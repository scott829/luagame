.. _networking_api:

**网络API**
========================

**URL**
------------------------------

**function lg.parse_url(url)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

解析url，将 url 的段保存到table中返回。

**function lg.url_encode(text)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

编码字符串

**function lg.url_decode(text)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

解码字符串

**HTTP**
------------------------------

**function lg.http_get(url, targetFile, callback, [requestFields])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

请求指定 url 的数据。

如果指定了 targetFile，则将请求到的数据保存到指定文件。

callback 是一个回调函数，函数定义为: function(type, value)，type 的值可能为 "data", "loading", "error"。

如果 type 等于 "data"，则 value 保存请求到的数据（如果没有设置targetFile）。

如果 type 等于 "loading"，则 value 为当前下载数据的比率，为一个 0 - 1 的浮点数。

如果 type 等于 "error"，则 value 保存错误代码。

requestFields 是一个 table，用于描述 http 头中的请求字段。

.. code-block:: lua

    -- 请求http://www.baidu.com的内容，并且 print 出数据
    lg.http_get("http://www.baidu.com", nil, 
    	function(type, value)
    		if type == "data" then
    			print(value)
    		end
      	end)

**function lg.http_post(url, postData, targetFile, callback, [requestFields])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

发送数据到指定 url。

postData 是一个字符串，保存需要发送的数据。

如果指定了 targetFile，则将请求到的数据保存到指定文件。

callback 说明请参考 lg.http_get。

**网络客户端**
------------------------------

**function lg.net_open(url, function callback())**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

pomelo客户端url格式:

	pomelo://<ip>:<port>?[clientType=客户端类型][version=客户端版本]
	
lws客户端url格式:

	lws://<ip>:<port>/path?[clientType=客户端类型][version=客户端版本]

当连接成功后，调用 callback。

如果当前已经连接，则同时关闭上一个连接。

**function lg.net_close()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

关闭当前网络连接。

**function lg.net_request(route, msg, function callback(response))**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

发送请求，当得到响应以后调用 callback，成功则返回 true。

**function lg.net_notify(route, msg)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

发送通知，成功则返回 true。

**EVT function lg.net_set_error(function handler(err_code))**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置错误处理函数，请参考 :ref:`网络错误类型<net-err-type>`。

**EVT function lg.net_set_push(function handler(route, msg))**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置服务端推送消息的处理函数。
