﻿.. _constant:

**常量**
============================

**平台相关**
---------------------

* lg.platform **平台类型**

.. _keycode:

**按键代码**
-----------------------------

* lg.key.up **上方向键**

* lg.key.down **下方向键**

* lg.key.left **左方向键**

* lg.key.right **右方向键**

* lg.key.page_up **向上翻页**

* lg.key.page_down **向下翻页**

* lg.key.enter **回车(确定)**

* lg.key.back **后退**

* lg.key.options **选项**

* lg.key.f1 **F1**

* lg.key.f2 **F2**

* lg.key.f3 **F3**

* lg.key.f4 **F4**

* lg.key.f5 **F5**

* lg.key.f6 **F6**

* lg.key.f7 **F7**

* lg.key.f8 **F8**

* lg.key.f9 **F9**

* lg.key.f10 **F10**

* lg.key.f11 **F11**

* lg.key.f12 **F12**

.. _touch-type:
  
**触摸类型**
---------------------------

* lg.touch_type.began

  开始触摸

* lg.touch_type.moved

  触摸点改变

* lg.touch_type.ended

  触摸结束

* lg.touch_type.cancelled

  触摸取消
  
.. _gesture-type:
  
**手势类型**
---------------------------

* lg.gesture_type.tap

  轻触

* lg.gesture_type.longpress

  在一点上长按

* lg.gesture_type.pinch

  两个指头捏或者放的操作

.. _swipe-direction:

**手势滑动方向**
---------------------------

* lg.swipe_direction.left

  向左滑动

* lg.swipe_direction.right

  向右滑动

* lg.swipe_direction.up

  向上滑动

* lg.swipe_direction.down

  向下滑动

**字符串绘制方法**
-------------------------------

* lg.draw_string_flags.baseline

  基线对齐。
  
.. _animation-evt-type:
  
**动画事件类型**
-----------------------

* lg.animation_event_type.start

  动画开始事件。
  
* lg.animation_event_type.end

  动画结束事件。
  
* lg.animation_event_type.complete

  动画播放完成事件。
  
* lg.animation_event_type.custom

  自定义事件。

.. _res-type:

**资源类型**
-------------------------------

* lg.res_type.image

  图片资源。

* lg.res_type.sound

  音效资源。

.. _mapobject-type:

**地图对象类型**
---------------------

* lg.mapobject_type.rectangle

  矩形。

* lg.mapobject_type.ellipse

  椭圆形。

* lg.mapobject_type.polygon

  多边形。

* lg.mapobject_type.polyline

  折线。

* lg.mapobject_type.tile

  图块。
  
.. _net-err-type:

**网络错误类型**
-------------------------------

* lg.net_error.bad_response

  错误的响应数据。
   
* lg.net_error.handshake_version

  客户端版本号不符合要求。

* lg.net_error.handshake_unknown

  握手阶段未知错误。
  
* lg.net_error.kick_by_server

  被服务端踢掉。
  
* lg.net_error.timeout

  读/写超时。
  
* lg.net_error.reading

  读数据出错。
  
* lg.net_error.writing

  写数据出错。
  
.. _payment-event-type:

**支付事件类型**
---------------------------

* lg.payment.purchased

  支付成功。

* lg.payment.failed

  支付失败。

* lg.payment.not_found

  没有找到指定商品。
