.. _misc_api:

**其他API**
========================

**字符串**
--------------------------

**function lg.strtrim(str, [del])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

删除字符串两边的指定字符，del 可以是多多个字符，如果 del 未指定，则为 "\t\n\r "。

**function lg.strsplit(str, sep, [limit])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

用 sep 保存的符号列表分割字符串 str，结果用一个 table返回。limit 表示最大返回的字符串段个数，默认为不限制。

.. code-block:: lua

    -- 分割字符串
	local result = lg.string_split("a,bbb,,c", ",")
    for i = 1, #result do
        print(result[i])
    end

**function lg.strjoin(seq, str1, str2 ... strN)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

以 seq 作为分隔符连接字符串。

**function lg.strconcat(str1, str2 ... strN)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

连接字符串。

**function lg.strreplace(str, search, replace)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

替换字符串。
	
.. _new-gesture-recognizer:

**手势识别器**
-------------------------

**function lg.gr.tap([numberOfTapsRequired])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

创建一个轻触手势识别器。numberOfTapsRequired 表示需要轻触多少下。手势事件回调函数第二个参数是一个 point，表示按下的坐标。

**function lg.gr.longpress()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

创建一个长按手势识别器。手势事件回调函数第二个参数是一个 point，表示按下的坐标。

**function lg.gr.pinch()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

创建一个两个指头捏或者放的识别器。

当发生 pinch 事件时，手势事件回调函数第二个参数是一个 table，其中 start_distance 和 current_distance 分别表示起始距离和当前距离。

**function lg.gr.swipe()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

创建一个指头滑动的识别器。

当发生 swipe 事件时，手势事件回调函数第二个参数是一个枚举，表示滑动的方向。参考 :ref:`滑动方向<swipe-direction>`。

**应用内支付**
---------------------

**function lg.can_make_payments()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

判断用户当前是否允许支付。

**function purchase(product_id)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

购买一个产品。

**function payment_set_event(function callback(type, receipt))**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置支付事件回调函数，当 type 为 lg.payment.purchased 或者 lg.payment.restored 时，receipt 为购买的收据。参考 :ref:`支付事件类型<payment-event-type>`。
