.. _resource_api:

**资源API**
========================

**资源包**
--------------------------------------

**function lg.add_resource(filename, [head])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

添加 filename 指定的路径到资源包列表，如果 head 为 true，则添加到列表的头部，否则添加到尾部。

**function lg.remove_resource(filename)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

删除指定的资源包。

**function lg.resource_list()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

返回当前资源包列表的数组。

**function lg.cache_resource(filename, type, [function callback(filename, succeeded, time)])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

异步缓存指定的 :ref:`资源类型<res-type>`，如果缓存成功，则回调函数的 succeeded 为 true。

回调函数的 time 为加载资源的耗时秒数。

