.. _node_api:

**节点API**
========================

**场景管理**
--------------------------------------

**function lg.push_scene(scene)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

压入一个场景到场景栈。

**function lg.pop_scene()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

弹出栈顶的场景。

**function lg.replace_scene(scene)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

替换栈顶的场景。

**function lg.current_scene()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取栈顶的场景，即当前场景。

**function lg.set_viewsize(width, height)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置获取视图的大小。

**function lg.viewsize()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取视图的大小 width, height。

**function lg.set_speed(speed)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置游戏的速度，1 为原始速度。

**function lg.speed()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取游戏的当前速度。

**function lg.pause()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

暂停游戏。

**function lg.resume()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

恢复被暂停的游戏。

**function lg.is_paused()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

判断游戏是否已经被暂停。

**function lg.set_camera(center_x, center_y, scale)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置摄像机位置和缩放。

**节点**
--------------------------------------

**function lg.new_node()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

创建一个新节点。

一般使用该函数创建一个容器节点，或者从该节点对象继承，创建一个自定义节点类型。

锚点和变换锚点都在 (0, 0) 位置。

**function <node>:equal(other_node)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

比较两个节点是否相同。

**function <node>:set_cxform(cxform)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置节点的 :ref:`颜色变换矩阵<cxform>`。

**function <node>:cxform()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取节点的 :ref:`颜色变换矩阵<cxform>`。

**function <node>:set_position([x, y]|[pt])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置节点的位置，该位置和节点锚点的位置有关。例如，一个 100 宽高得图像精灵节点，锚点位置为 (50, 50)，如果设置节点的位置为 (0, 0)，则图像精灵的左上角应该在 (-50, -50)。

**function <node>:position()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取表示节点的位置点对象。

**function <node>:set_anchor([x, y]|[pt])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置节点锚点位置。

**function <node>:anchor()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取节点锚点的位置点对象。

**function <node>:set_scale([sx, [sy]]|[pt])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置节点的缩放比例，如果省略掉参数 sy，则纵向缩放比等于 sx，如果参数是一个点对象，则 x 和 y 分别表示对应轴的缩放。

**function <node>:scale()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取节点的缩放比例的点对象。

**function <node>:set_rotate(angle)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置节点的旋转角度。

**function <node>:rotate()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取节点的缩放角度。

**function <node>:set_transform_anchor([x, y]|[pt])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置节点变换锚点的位置。

**function <node>:transform_anchor()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取节点变换锚点的点对象。

**function <node>:set_zorder(z)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置节点的Z轴位置，参数 z 是一个整数，同一层次节点的 z 值越大，则节点优先绘制，默认节点的 z 值为 0，如果不做特别设置的情况下，按添加的先后顺序绘制。

**function <node>:zorder()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取节点的Z轴位置。

**function <node>:add_child(child)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

添加子节点。父节点会比子节点更加优先绘制，并且，所有子节点的变换参数（平移，缩放，旋转）都会继承于根节点。

**function <node>:remove()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

删除该节点。

**function <node>:remove_all_children()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

删除所有子节点。

**function <node>:is_visible()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

返回布尔值，表示节点当前是否可见。

如果节点不可见，可能有三种原因:

#. 该节点可能并未直接或者间接添加到场景中。
#. 该节点所在的场景不是当前场景。
#. 该节点被 set_visible 函数隐藏了。

**function <node>:set_visible(v)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置节点是否可见。

**function <node>:do_action(action)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

在节点上执行一个 :doc:`动作<actions_api>`。

**function <node>:stop_actions()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

停止节点上已经执行的所有动作。

**function <node>:pause_actions()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

暂停节点上所有已经执行的动作。

**function <node>:resume_actions()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

恢复运行节点上所有已经暂停的动作。

**function <node>:parent()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取父节点，如果返回 nil，则表示当前是一个根节点。

**function <node>:root()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取根节点，通常是一个场景节点。

**function <node>:set_opacity(opacity)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置节点的透明度。

该函数的内部实现事实上是调用 node:set_cxform(lg.cxform(opacity, 1, 1, 1, 0, 0, 0, 0))，只是一个便捷函数。

**function <node>:enable_touch(value)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

开启或关闭节点触摸功能。

**function <node>:touch_is_enabled()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

判断节点的触摸测试功能是否开启。

**function <node>:children_count()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取子节点的总数。

**function <node>:child(i)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取第 i 个子节点。

**function <node>:size()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取点对象，表示节点占用的绘图区域大小 width 和 height。

**function <node>:content_box()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取节点的内容区域 x, y, width, height。

**function <node>:actions_count()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取节点当前正在运行的动作总数。

**function <node>:local_to_world([x, y]|[pt])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

将节点内坐标转换为世界坐标，如果输入是一个点对象，则返回同样为点对象。

**function <node>:world_to_local([x, y]|[pt])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

将世界坐标转换为节点内坐标，如果输入是一个点对象，则返回同样为点对象。

**function <node>:center_anchor()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

将节点的锚点设置到节点内容的正中间。

**function <node>:center_transform_anchor()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

将节点的变换锚点设置到节点内容的正中间。

**function <node>:set_touch_bubble(value)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置触摸事件是否传递到上层节点。

**function <node>:is_touch_bubble()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

判断触摸事件是否传递到上层节点。

**function <node>:add_gesture_recognizer(gr)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

添加一个手势识别器（每个节点必须单独创建，不能够共用同一个识别器）。参考 :ref:`创建手势识别器<new-gesture-recognizer>`:。

**function <node>:add_class(name)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

给节点添加一个 class。

**function <node>:remove_class(name)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

从节点移除一个 class。

**function <node>:toggle_class(name)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

切换节点的 class，如果该 class 存在，则删除，否则添加。

**function <node>:has_class(name)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

判断节点是否有名称为 name 的 class。

**function <node>:clear_classes()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

清除节点的所有 class。

**function <node>:classes()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取节点的所有 class。

**function <node>:select(name, [recursive])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

查找 class 名称为 name 的子节点，recursive 表示是否递归查找后代节点，默认为 false。

**function <node>:set_id(id)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置节点的id，id 必须为字符串。

**function <node>:id()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取节点的id。

**function <node>:bounds()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

返回节点相对于屏幕的包围矩形。

**function <node>:enable_rtree(value)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

打开或关闭节点的RTree节点查找支持，默认为关闭。

**function <node>:rtree_search_by_rect(rt, [in_rect])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

查找范围内所有的子节点，in_rect 如果为 true，则所有子节点必须完全包含于 rt 中，默认为 false。

**function <node>:rtree_search_by_radius(center_point, radius)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

查找指定位置半径范围的所有子节点。

查找范围内所有的子节点，in_rect 如果为 true，则所有子节点必须完全包含于 rt 中，默认为 false。

**function <node>:touches()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取所有当前节点上的触摸对象。

**function <node>:enable_multiple_touch(value)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

打开或关闭节点的多点触摸支持，默认为关闭。

**EVT function <node>:set_touch(function handler(type, touch))**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置节点的触摸事件处理函数，type 是 :ref:`触摸类型<touch-type>`，touch 是一个 :ref:`触摸对象<touch>`

**EVT <node>:set_enter(function handler())**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置节点的进入事件处理函数。当节点开始运行时（即节点所在的场景成为当前场景时）调用该函数。

**EVT <node>:set_exit(function handler())**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置节点的退出事件处理函数。当节点停止运行时（即节点所在的场景不在成为当前场景时）调用该函数。

**EVT <node>:set_gesture(function handler(type, gesture_data))**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置节点的手势事件处理函数，参数 gesture_data 的内容根据不同手势类型而定。参考 :ref:`手势类型<gesture-type>`，:ref:`创建手势识别器<new-gesture-recognizer>`:。

**场景**
---------------------------

**function lg.new_scene()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

创建一个新场景节点。

一般情况下，场景节点都是根节点（这不是必须的，基于某些特殊的需求，场景节点也可能成为其他节点的子节点）。

锚点在 (0, 0) 位置，变换锚点都在屏幕正中间。

**图层**
-----------------------

**function lg.new_layer()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

创建一个图层。

**EVT function <layer>:set_key_down(function (keycode))**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置键盘按下事件处理函数，按键代码请参考 :ref:`键盘代码<keycode>`:

**EVT function <layer>:set_key_up(function (keycode))**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置键盘弹起事件处理函数，按键代码请参考 :ref:`键盘代码<keycode>`:

**单色图层**
--------------------------

**function lg.new_colorlayer(color)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

创建一个单色图层。参数 color 指定了图层的颜色，请使用 :ref:`lg.rgb<rgb>` 或者 :ref:`lg.argb<argb>` 函数来生成它。

锚点在 (0, 0) 位置，变换锚点都在屏幕正中间。

**function <colorlayer>:set_color(color)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置单色图层的 :ref:`颜色<color>`。

**function <colorlayer>:color()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取单色图层的 :ref:`颜色<color>`。

**渐变色图层**
-------------------------

**function lg.new_gradientlayer(begin_color, end_color, vx, vy)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

创建以 begin_color 为起始颜色，end_color 为结束颜色的渐变色图层，vx 和 vy 是一个二维向量，指定渐变的方向。

锚点在 (0, 0) 位置，变换锚点都在屏幕正中间。

下面的例子说明 vx 和 vy 的用法：

.. code-block:: lua

   -- 创建一个从上面是红色，下面是蓝色的渐变图层
   lg.new_gradientlayer(lg.rgb(1, 0, 0), lg.rgb(0, 0, 1), 0, 1)
   
   -- 创建一个从左上是红色，右下是蓝色的渐变图层
   lg.new_gradientlayer(lg.rgb(1, 0, 0), lg.rgb(0, 0, 1), 1, 1)

**图块地图图层**
---------------------------

**function lg.new_tilemaplayer(filename)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

创建一个图块地图图层。

锚点在 (0, 0) 位置，变换锚点都在屏幕正中间。

**function <tilemaplayer>:size()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取图块地图像素宽度和高度。

**function <tilemaplayer>:grid_size()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取图块大小。

**function <tilemaplayer>:set_scroll([x, y]|pt)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置地图左上角偏移坐标。

**function <tilemaplayer>:scroll()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取地图左上角偏移坐标。

**function <tilemaplayer>:center_scroll([x, y]|pt)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取居中于某个图块的偏移坐标。

**function <tilemaplayer>:center_to([x, y]|pt)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

使地图居中于某个图块。

**function <tilemaplayer>:local_to_grid([x, y]| pt)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

屏幕坐标转换为图块坐标。

**function <tilemaplayer>:grid_to_local([x, y]| pt)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

图块坐标转换为屏幕坐标。

**function <tilemaplayer>:global_to_grid([x, y]| pt)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

像素坐标转换为图块坐标（不包含偏移）。

**function <tilemaplayer>:grid_to_global([x, y]| pt)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

图块坐标转换为像素坐标（不包含偏移）。

**function <tilemaplayer>:show_layer(name)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

显示名称为 name 的图层。

**function <tilemaplayer>:hide_layer(name)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

隐藏名称为 name 的图层。

**function <tilemaplayer>:prop(name)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取名称为 name 的地图属性。

**function <tilemaplayer>:layer_prop(layer_name, prop_name)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取图层的属性。

**function <tilemaplayer>:tile_prop([layer_name], x, y, prop_name)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取指定图层某个位置图块的属性，如果省略掉 layer_name 参数，则按图层顺序依次查找。

**function <tilemaplayer>:find_path(start_x, start_y, end_x, end_y, bias)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

查找 start 到 end 的路径（包含end），bias 如果为 true 则表示允许斜着搜索。

**function <tilemaplayer>:tileid_by_alias(alias)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

以图块别名获取图块的 id。

**function <tilemaplayer>:tileid(layer_name, x, y)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取指定图层某个位置的图块 id。

**function <tilemaplayer>:set_tileid(layer_name, x, y, id)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置指定图层某个位置的图块 id。

**function <tilemaplayer>:set_child_at(child, layer_name, y)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置地图内的精灵位于某个图层的什么位置，用于调整精灵相对于地图的Z轴。

**function <tilemaplayer>:object(layer_name, object_name)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取指定图层上的对象。

返回的是一个 table，其中 object_type 字段表示 :ref:`地图对象类型<mapobject-type>`。类型一共有五种，每种对象都有不同的属性。

对象的通用属性有 name（对象的名称），type（自定义类型），visible（是否可见），x 和 y（坐标）。

当类型为 rectangle 或者 ellipse 时，有 width 和 height 指定大小。

当类型为 polygon 或者 polyline 时，有 points 保存点序列。

当类型为 tile 时，有 gid 指定图块的 id。

**图像精灵**
---------------------------------

**function lg.new_sprite(location, [width, height], [inset_left], [inset_top], [inset_right], [inset_bottom])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

以 localtion 创建一个图像精灵节点，width 和 height 是可选参数，如果忽略它们或者它们其中一个值为0 ，则使用图片的默认宽度和高度。

location 可以是一个字符串，表示图片文件的路径。或者是一个 table，table 字段 atlas 表示 atlas文件名，name 表示图块名。

inset_XXX 表示希望sprite作为9宫格方式缩放时使用的边界宽度。

锚点和变换锚点都在(0, 0)，为图像正中间。

.. code-block:: lua

   -- 用图片文件创建图像精灵
   lg.new_sprite("media/sprite.png")
   
   -- 基于 atlas 创建图片精灵
   lg.new_sprite({atlas = "media/goblins.atlas", name = "goblin/head"})

**function <sprite>:set_size(width, height)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置图像精灵节点的大小，并且把锚点和变换锚点设置到正中心。

**文本标签**
--------------------------

**function lg.new_label()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

创建一个文本标签节点，用于显示一行文本（只支持单行文本）。

锚点和变换锚点都在 (0, 0) 位置。

**function <label>:set_fontname(name)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置文本标签节点的字体，字体必须是 :ref:`字体列表<fontlist>` 中已经定义的字体。

**function <label>:fontname()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取文本标签节点当前的字体。

**function <label>:set_fontsize(name)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置文本标签节点的字号。

**function <label>:fontsize()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取文本标签节点当前的字号。

**function <label>:set_color(color)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置文本标签节点的文本 :ref:`颜色<color>`。

**function <label>:color()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取文本标签节点的当前文本  :ref:`颜色<color>`。

**function <label>:set_text(text)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置文本标签节点的文本，文本必须以 utf-8 编码，并确保当前字体包含文本编码的字形。

**function <label>:text()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取文本标签节点的当前文本。

**function <label>:set_draw_flags(flags)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置文本标签字符串绘制方法。

**function <label>:draw_flags()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取文本标签字符串绘制方法。

**可编辑文本**
--------------------------

**function lg.new_textfield()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

创建一个可编辑文本节点（只支持单行文本），当用户点击它之后，显示虚拟键盘用于编辑文本内容。

锚点在 (0, 0) 位置，变换锚点在中心位置。

**function <textfield>:set_fontname(name)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置可编辑文本节点的字体，字体必须是 :ref:`字体列表<fontlist>` 中已经定义的字体。

**function <textfield>:fontname()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取可编辑文本节点当前的字体。

**function <textfield>:set_fontsize(name)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置可编辑文本节点的字号。

**function <textfield>:fontsize()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取可编辑文本节点当前的字号。

**function <textfield>:set_color(color)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置可编辑文本节点的文本 :ref:`颜色<color>`。

**function <textfield>:color()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取可编辑文本节点的当前文本  :ref:`颜色<color>`。

**function <textfield>:set_cursor_color(color)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置可编辑文本节点的光标 :ref:`颜色<color>`。

**function <textfield>:cursor_color(color)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取可编辑文本节点光标  :ref:`颜色<color>`。

**function <textfield>:set_comp_color(color)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置可编辑文本节点正在完成中的文本 :ref:`颜色<color>`。

**function <textfield>:comp_color(color)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取可编辑文本节点正在完成中的文本  :ref:`颜色<color>`。

**function <textfield>:set_placeholder_color(color)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置可编辑文本节点提示的文本 :ref:`颜色<color>`。

**function <textfield>:placeholder_color(color)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取可编辑文本节点提示的文本  :ref:`颜色<color>`。

**function <textfield>:set_text(text)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置可编辑文本节点的文本，文本必须以 utf-8 编码，并确保当前字体包含文本编码的字形。

**function <textfield>:text()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**EVT function <textfield>:set_change(function handler())**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置文本改变函数。当可编辑文本节点内文本改变时调用该函数。

**绘图节点**
---------------------------

**function lg.new_drawnode()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

创建一个绘图节点。

锚点和变换锚点都在 (0, 0) 位置。

**function <drawnode>:set_color(color)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置绘图节点的当前绘图 :ref:`颜色<color>`。

**function <label>:color()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取绘图节点的当前文本  :ref:`颜色<color>`。

**function <drawnode>:clear()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

清除当前绘图节点的内容

**function <drawnode>:rectangle(x, y, width, height)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

用当前颜色填充左上角为 (x, y) 大小为 (width, height) 的矩形

**function <drawnode>:ellipse(x, y, width, height)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

用当前颜色填充左上角为 (x, y) 大小为 (width, height) 的圆形，圆心为 (x + width / 2, y + height / 2)

**function <drawnode>:ellipse(x, y, width, height, start, end)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

用当前颜色填充左上角为 (x, y) 大小为 (width, height) 的扇形，圆心为 (x + width / 2, y + height / 2)，起始角度为 start ，结束角度为 end。

**function <drawnode>:triangle(x1, y1, x2, y2, x3, y3)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

用当前颜色绘制三角形，参数为三角形的三个顶点坐标。

**裁剪节点**
---------------------------

**function lg.new_clipnode()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

创建一个裁剪节点。

锚点和变换锚点都在 (0, 0) 位置。

裁剪节点可以看做是一个容器节点，它之下的所有节点都被 stencil 的绘图区域裁剪。

**function <clipnode>:set_stencil(node)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置裁剪模板，裁剪节点下面的所有子节点都被该节点的绘图区域裁剪。

.. code-block:: lua

   -- 创建一个裁剪节点，并且用 drawnode 作为裁剪模板裁剪下面的图像精灵
   clipnode = lg.new_clipnode()
   drawnode = lg.new_drawnode()
   drawnode:ellipse(0, 0, 100, 100)
   clipnode:set_stencil(drawnode)
   
   sprite = lg.new_sprite("media/koala.jpg", 100, 100)
   clipnode:add_child(sprite)

**function <clipnode>:stencil()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取裁剪模板节点。

**function <clipnode>:set_invert(invert)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置裁剪方式，当 invert 为 false 时，以 stencil 的绘图区域裁剪，否则以非绘图区域裁剪。

**function <clipnode>:invert()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取裁剪方式。

**粒子系统节点**
---------------------------

**function lg.new_particle_system(filename)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

创建一个粒子系统节点。

锚点和变换锚点都在 (0, 0) 位置。

**function <particle_system>:set_active(value)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置粒子系统是否为激活状态，默认为激活。

**function <particle_system>:.set_in_world(value)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置粒子系统坐标是相对于世界坐标，还是节点坐标。默认为相对于世界坐标。

**批处理渲染节点**
---------------------------

**function lg.new_batchnode()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

创建一个批处理渲染节点。

**动画节点**
---------------------------

**function lg.new_animation(filename)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

创建一个动画节点，动画文件由 `Spine <http://esotericsoftware.com/>`_ 生成。

**function <animation>:set_animation(name, [loop])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置当前播放的动画名称，loop 是一个可选参数，表示是否循环播放，默认为 true。这个函数会清除当前的所有动画播放队列。

**function <animation>:add_animation(name, [loop], [delay])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

添加一个动画到队列末尾，loop 是一个可选参数，表示是否循环播放，默认为 true。delay 表示延迟多少秒之后播放。

**function <animation>:stop_animation()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

停止当前播放的所有动画。

**function <animation>:duration(name)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

获取动画的持续时间。

**function <animation>:set_skin(name)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置当前骨骼动画的皮肤。

**function <animation>:set_exact_touch(value)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置是否开启精密触摸测试模式。

**function <animation>:is_exact_touch()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

判断当前是否开启了精密触摸模式。

**EVT function <animation>:set_event(function handler(type, data))**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置动画播放事件处理函数，type 为 :ref:`动画事件类型<animation-evt-type>`。data 是一个 table，根据事件类型的不同容纳不同的值。

当 type 为 lg.animation_event_type.start 或者 lg.animation_event_type.end 时，data 有属性 animation_name。

当 type 为 lg.animation_event_type.complete 时，data 有属性 animation_name 和 loop_count。

当 type 为 lg.animation_event_type.custom 时，data 有属性 name，int_value，float_value，string_value。
