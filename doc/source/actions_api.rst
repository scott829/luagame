﻿.. _actions_api:

**动作API**
===================

**基本**
--------------------------------------

**function <action>:is_paused()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

判断动作是否已经暂停。

**function <action>:is_running()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

判断动作是否正在运行。

**function <action>:pause()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

暂停正在运行的动作。

**function <action>:resume()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

恢复执行已经暂停的动作。

**function <action>:stop()**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

停止正在运行的动作。

**function lg.actions.clone(action)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

克隆动作（不会克隆其运行状态）。

**组合动作**
--------------------------------------

**function lg.actions.sequence([action...])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

创建按照顺序执行的动作序列

**function lg.actions.spawn([action...])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

创建组合执行的动作

**间隔动作**
--------------------------------------

**function lg.actions.moveto(duration, x, y)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

在 持续 duration 秒内,将当前节点移动到 x , y

**function lg.actions.moveby(duration, offsetx, offsety)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

在 持续 duration 秒内,相对于当前节点位置移动 offsetx , offsety

**function lg.actions.scaleto(duration, x, y)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

在 持续 duration 秒内,相对当前节点原始宽度和高度分别缩放 x ,y 倍

**function lg.actions.scaleby(duration, x, y)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

在 持续 duration 秒内,相对当前节点宽度和高度分别缩放 x ,y 倍

**function lg.actions.rotateto(duration, d)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

在 持续 duration 秒内,当前节点旋转到 d 角度

**function lg.actions.rotateby(duration, d)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

在 持续 duration 秒内,相对当前节点角度旋转 d 角度

**function lg.actions.bezierto(duration, x1, y1, x2, y2, x3, y3)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

在 持续 duration 秒内,当前节点按照控制点为 x1, y1 , x2, y2, 终点为 x3 , y3 的贝塞尔曲线移动

**function lg.actions.bezierby(duration, x1, y1, x2, y2, x3, y3)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

在 持续 duration 秒内,相对当前节点位置,节点按照控制点为 x1, y1 , x2, y2, 终点为 x3 , y3 的贝塞尔曲线移动

**function lg.actions.blink(duration, blinks)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

在 持续 duration 秒内,将当前节点闪烁 blinks 下

**function lg.actions.fadein(duration)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

在 持续 duration 秒内,当前节点淡入

**function lg.actions.fadeout(duration)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

在 持续 duration 秒内,当前节点淡出

**function lg.jump(duration, offsetx, offsety, height, jumps)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

在 持续 duration 秒内,相对节点位置当前节点偏移 offsetx, offsety, 按照 height 高度,跳 jumps 次

**function lg.actions.delay(duration)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

当前节点动作延时 duration 秒执行

**function lg.actions.wave3d(duration, [cols, rows, waves, amplitude])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

在 持续 duration 秒内,当前节点执行 wave3d 动作

**function lg.actions.lens3d(duration, [cols, rows, xcenter, ycenter, radius, lensEffect])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

在 持续 duration 秒内,当前节点执行 lens3d 动作

**function lg.actions.ripple3d(duration, [cols, rows, xcenter, ycenter, radius, waves, amplitude])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

在 持续 duration 秒内,当前节点执行 ripple3d 动作

**function lg.actions.liquid3d(duration, [cols, rows, waves, amplitude])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

在 持续 duration 秒内,当前节点执行 liquid3d 动作

**function lg.actions.shaky(duration, [cols, rows, range, shakeZ])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

在 持续 duration 秒内,当前节点执行 shaky 动作

**function lg.actions.fadeout_lt(duration, [cols, rows])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

在 持续 duration 秒内,当前节点执行 fadeout_lt 动作

**function lg.actions.fadeout_rb(duration, [cols, rows])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

在 持续 duration 秒内,当前节点执行 fadeout_rb 动作

**function lg.actions.fadeout_up(duration, [cols, rows])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

在 持续 duration 秒内,当前节点执行 fadeout_up 动作

**function lg.actions.fadeout_down(duration, [cols, rows])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

在 持续 duration 秒内,当前节点执行 fadeout_down 动作

**function lg.actions.call(function handler())**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

创建一个函数调用动作

**function lg.actions.interval(duration, callbacks, context)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

创建一个间隔动作，callbacks 是一个字典，可以设置对应动作状态的回调函数，context 是传给回调函数的参数，可以为任意值。

回调函数:

function reset(context) 当动作重置的时候调用

function step(context, ratio) 当动作更新的时候调用

function stop(context) 当动作停止的时候调用

**过渡动作**
--------------------------------------

**function lg.actions.speed(action, speed)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

调整动作 action 执行速度相对于当前 speed 倍

**function lg.actions.range(action, start, end)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

调整动作 action 的范围为 start 到 end

**function lg.actions.loop(action, time)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

动作 action 循环执行 time 秒

**function lg.actions.loop_forever(action)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

动作 action 永久的循环执行

**function lg.actions.ease_in(action, [rate])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

动作 action 按照 rate 比例因子缓入,先快后慢

**function lg.actions.ease_out(action, [rate])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

动作 action 按照 rate 比例因子缓出,先慢后快

**function lg.actions.ease_inout(action, [rate])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

动作 action 按照 rate 比例因子缓入在缓出,先快后慢,在慢在快

**function lg.actions.easeexp_in(action, [rate])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

动作 action 按照 rate 比例因子急入,先慢后越来越快

**function lg.actions.easeexp_out(action, [rate])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

动作 action 按照 rate 比例因子缓出,先快后越来越慢

**function lg.actions.easeexp_inout(action, [rate])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

动作 action 按照 rate 比例因子急入缓出, 先慢后越来越快然后先快后越来越慢

**function lg.actions.easesine_in(action, [rate])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

动作 action 按照 rate 比例因子进行sine函数的缓入

**function lg.actions.easesine_out(action, [rate])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

动作 action 按照 rate 比例因子进行sine函数的缓出

**function lg.actions.easesine_inout(action, [rate])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

动作 action 按照 rate 比例因子进行sine函数的缓入缓出

**function lg.actions.easeelastic_in(action, [rate])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

动作 action 按照 rate 比例因子进行弹性的缓入,先慢在更慢然后在快在更快

**function lg.actions.easeelastic_out(action, [rate])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

动作 action 按照 rate 比例因子进行弹性的缓出,先快在更快然后在慢在更慢

**function lg.actions.easeelastic_inout(action, [rate])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

动作 action 按照 rate 比例因子进行弹性的缓入缓出

**function lg.actions.easebounce_in(action, [rate])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

动作 action 按照 rate 比例因子进行弹跳的缓入, 先慢在快

**function lg.actions.easebounce_out(action, [rate])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

动作 action 按照 rate 比例因子进行弹跳的缓出, 先快在慢

**function lg.actions.easebounce_inout(action, [rate])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

动作 action 按照 rate 比例因子进行弹跳的缓入缓出, 先快在慢,在慢在快

**function lg.actions.easeback_in(action, [rate])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

动作 action 按照 rate 比例因子进行倒退的缓入

**function lg.actions.easeback_out(action, [rate])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

动作 action 按照 rate 比例因子进行倒退的缓出

**function lg.actions.easeback_inout(action, [rate])**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

动作 action 按照 rate 比例因子进行倒退的缓入缓出


**摄像机**
--------------------------------------

**function lg.actions.camera(duration, center_x, center_y, scale)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

创建摄像机镜头拉升或移动动作，center_x 和 center_y 是摄像机的中心点，scale 是缩放比例。

















