LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)   
LOCAL_MODULE    := luagame
LOCAL_SRC_FILES := ../../../bin/android/libluagame.a
include $(PREBUILT_STATIC_LIBRARY)  

include $(CLEAR_VARS)

LOCAL_MODULE    := native-activity
LOCAL_SRC_FILES := ../../dummy.c
LOCAL_LDLIBS    := -llog -landroid -lEGL -lGLESv2 -lOpenSLES
LOCAL_WHOLE_STATIC_LIBRARIES := luagame

include $(BUILD_SHARED_LIBRARY)
