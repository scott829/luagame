#!/bin/sh
platform=$1
if [ "$platform" = "android" ]; then
	echo build for $platform
	mkdir -p build.android
	cd build.android
	ndk_platform="unknown"
	if [ -d "$ANDROID_NDK/prebuilt/darwin-x86_64" ]; then
		ndk_platform="darwin-x86_64"
	fi
	if [ "$ndk_platform" = "unknown" ]; then
		echo Android NDK is not found!
		exit 1
	fi
	echo ndk platform: $ndk_platform
	cmake .. -DCMAKE_TOOLCHAIN_FILE=../makefiles/toolchain/android.cmake
	make
	cd ../main/android/jni
	$ANDROID_NDK/ndk-build
	exit 0
elif [ "$platform" = "ios" ]; then
	echo build for $platform
	mkdir -p build.ios
	cd build.ios
	cmake .. -DCMAKE_TOOLCHAIN_FILE=../makefiles/toolchain/iOS.cmake -GXcode
	exit 0
fi

echo unknown platform
exit 1