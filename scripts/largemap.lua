require 'yaci'

local LargeMap = newclass("LargeMap")

-- ortho地图驱动
local OrthoMapDriver = {}

function OrthoMapDriver.pixelSize(map)
	return map.numColumns * map.tileWidth, map.numRows * map.tileHeight
end

function OrthoMapDriver.pixelCroodToGrid(map, x, y)
	return x / map.tileWidth + 1, y / map.tileHeight + 1
end

function OrthoMapDriver.gridToPixelCrood(map, x, y)
	return (x - 1) * map.tileWidth, (y - 1) * map.tileHeight
end

-- iso地图驱动
local IsoMapDriver = {}

function IsoMapDriver.pixelSize(map)
	return map.numColumns * map.tileWidth, map.numRows * map.tileHeight
end

function IsoMapDriver.pixelCroodToGrid(map, x, y)
	local px, py = x, y
	local tileX, tileY
	px = px - map.numRows * map.tileWidth / 2
	tileX = px / map.tileWidth
	tileY = py / map.tileHeight
	return tileY + tileX + 1, tileY - tileX + 1
end

function IsoMapDriver.gridToPixelCrood(map, x, y)
	local originX = map.numRows * map.tileWidth / 2
	local gx, gy = x - 1, y - 1
	return (gx - gy) * map.tileWidth / 2 + originX, (gx + gy) * map.tileHeight / 2
end

-- 构造函数
function LargeMap:init(mapType, numColumns, numRows, tileWidth, tileHeight)
	-- 设置地图驱动
	if mapType == "ortho" then
		self.driver = OrthoMapDriver
	elseif mapType == "iso" then
		self.driver = IsoMapDriver
	else
		error("map type")
	end

	-- 地图和图块大小
	self.numColumns = numColumns
	self.numRows = numRows
	self.tileWidth = tileWidth
	self.tileHeight = tileHeight

	-- 创建显示节点
	self.layerNode = lg.new_layer()
	self.scrollViewNode = lg.ui.new_scrollview()
	local layerSize = self.layerNode:size()
	self.layerNode:add_child(self.scrollViewNode)
	self.scrollViewNode:set_size(layerSize.x, layerSize.y)
	self.scrollViewNode:set_position(0, 0)
	local pwidth, pheight = self.driver.pixelSize(self)
	self.scrollViewNode:set_content_size(pwidth, pheight)
	self.scrollViewNode:set_onscroll(function()
		-- 当滚动视图滚动后，请求地图数据
		self:requestMap()
		end)
	self.contentView = self.scrollViewNode:content_view()

	-- 图层
	self.layers = {}
end

-- 添加图层
function LargeMap:appendLayer(name)
	self.layers[#self.layers + 1] = name
end

-- 请求设置视图位置，x 和 y 是地图坐标，该函数会把指定地图坐标居中到视图
function LargeMap:setViewPosition(x, y)
	local px, py = self.driver.gridToPixelCrood(self, x, y)
	local viewsize = self.scrollViewNode:size()
	px = px - viewsize.x
	py = py - viewsize.y
	self.scrollViewNode:set_scroll(px, py)
	self:requestMap()
end

-- 请求地图数据，返回region对象
LargeMap:virtual("doRequestMap")

-- 当请求地图失败之后调用
LargeMap:virtual("onRequestMapDataError")

-- 请求当前位置地图，如果当前视图在当前区域内，则该函数不做任何事
function LargeMap:requestMap()
	local viewRect = self.scrollViewNode:view_rect()
	local x1, y1 = self.driver.pixelCroodToGrid(self, viewRect.x, viewRect.y)
	local x2, y2 = self.driver.pixelCroodToGrid(self, viewRect.x + viewRect.width, viewRect.y)
	local x3, y3 = self.driver.pixelCroodToGrid(self, viewRect.x + viewRect.width, viewRect.y + viewRect.height)
	local x4, y4 = self.driver.pixelCroodToGrid(self, viewRect.x, viewRect.y + viewRect.height)
	local x5, y5 = self.driver.pixelCroodToGrid(self, viewRect.x + viewRect.width / 2, viewRect.y)
	local x6, y6 = self.driver.pixelCroodToGrid(self, viewRect.x + viewRect.width, viewRect.y + viewRect.height / 2)
	local x7, y7 = self.driver.pixelCroodToGrid(self, viewRect.x + viewRect.width / 2, viewRect.y + viewRect.height)
	local x8, y8 = self.driver.pixelCroodToGrid(self, viewRect.x, viewRect.y + viewRect.height / 2)
	local minx, miny, maxx, maxy = 9999999, 9999999, -9999999, -9999999
	local needRequest = false
	local points = {
		{x1, y1}, {x2, y2}, {x3, y3}, {x4, y4},
		{x5, y5}, {x6, y6}, {x7, y7}, {x8, y8},
	}

	for _, v in ipairs(points) do
		local x, y = math.floor(v[1]), math.floor(v[2])
		if x > maxx then maxx = x end
		if y > maxy then maxy = y end
		if x < minx then minx = x end
		if y < miny then miny = y end
	end

	minx = math.max(1, minx)
	maxx = math.min(self.numColumns - 1, maxx)
	miny = math.max(1, miny)
	maxy = math.min(self.numRows - 1, maxy)

	local function ptInRect(x, y, w, h, ptx, pty)
		return ptx >= x and pty >= y and ptx < x + w and pty < y + h
	end

	if self.region then
		-- 当前有区域
		if not ptInRect(self.region.x, self.region.y, self.region.width, self.region.height, minx, miny) or
			not ptInRect(self.region.x, self.region.y, self.region.width, self.region.height, maxx, maxy) then
			needRequest = true
		end
	else
		needRequest = true
	end

	if needRequest then
		-- 请求地图
		local rx1, ry1, rx2, ry2 = minx, miny, maxx, maxy
		
		self:doRequestMap(rx1, ry1, rx2 - rx1 + 1, ry2 - ry1 + 1, function(succeeded, region)
			if succeeded then
				-- 当区域请求成功
				self.region = region
				self.region.x = rx1
				self.region.y = ry1
				self.region.width = rx2 - rx1 + 1
				self.region.height = ry2 - ry1 + 1
				-- 重绘地图
				self:redraw()
			else
				self:onRequestMapDataError()
			end
		end)
	end
end

-- 获取图块属性，如果当前没有 region，或者超出 region 范围，则返回nil
function LargeMap:tile(x, y)
	if not self.region then
		return nil
	end

	if x < self.region.x or x > self.region.width - 1 or
		y < self.region.y or y > self.region.height - 1 then
		return nil
	end

	local rx, ry = x - self.region.x, y - self.region.y
	return self.region.tiles[rx + ry * self.region.height + 1]
end

-- 绘制地图
function LargeMap:redraw()
	if self.region then
		self.contentView:remove_all_children()
		self:drawTiles()
		self:drawObjects()
	end
end

-- 创建图块sprite对象
LargeMap:virtual("createTileSprite")

-- 绘制图块
function LargeMap:drawTiles()
	-- 创建图层
	local layerToNode = {}
	for _, layerName in pairs(self.layers) do
		local layerNode = lg.new_batchnode()
		layerToNode[layerName] = layerNode
		self.contentView:add_child(layerNode)
	end

	for i, tileInfo in ipairs(self.region.tiles) do
		local idx = i - 1
		local x = self.region.x + math.floor(idx % self.region.width)
		local y = self.region.y + math.floor(idx / self.region.width)
		local px, py = self.driver.gridToPixelCrood(self, x, y)
		local sprites = self:createTileSprite(tileInfo)
		for layerName, sprite in pairs(sprites) do
			local layer = layerToNode[layerName]

			if layer then
				-- 有这个图层
				local spriteSize = sprite:size()
				sprite:set_anchor(-spriteSize.x / 2, -spriteSize.y / 2)
				sprite:set_position(px, py)
				layer:add_child(sprite)
			end
		end
	end
end
LargeMap:virtual("drawTiles")

-- 绘制对象
function LargeMap:drawObjects()
end
LargeMap:virtual("drawObjects")

-- 测试 -----------------------------------------------

LargeMap.static.test = function()
	lg.set_viewsize(640, 480)

	local scene = lg.new_scene()
	lg.push_scene(scene)

	local mapData = {}
	for i = 1, 1000 * 1000 do
		mapData[i] = 0
	end
	for i = 0, 1000 - 1 do
		mapData[i * 1000 + 10 + 1] = 1
	end

	function getMapData(x, y, width, height)
		lg.log_info("request %d, %d, %d, %d", x, y, width, height)
		local result = {}
		for row = 0, height - 1 do
			for col = 0, width - 1 do
				local gx, gy = (x - 1) + col, (y - 1) + row
				local gidx = gx + gy * 1000 + 1
				local lidx = col + row * width + 1
				local tileId = mapData[gidx]
				result[lidx] = tileId
			end
		end
		return result
	end

	local MyMap = LargeMap:subclass("MyMap")

	function MyMap:init(mapType, numColumns, numRows, tileWidth, tileHeight)
		self.super:init(mapType, numColumns, numRows, tileWidth, tileHeight)
	end

	function MyMap:createTileSprite(tileInfo)
		return {tileLayer = lg.new_sprite("media/t" .. tileInfo .. ".png")}
	end

	function MyMap:doRequestMap(x, y, width, height, callback)
		callback(true, {
			tiles = getMapData(x, y, width, height)
		})
	end

	local map = MyMap:new("iso", 1000, 1000, 64, 32)
	map:appendLayer("tileLayer")
	scene:add_child(map.layerNode)
	map:setViewPosition(20, 20)
end


return LargeMap
