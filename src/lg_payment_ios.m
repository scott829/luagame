#include "lg_payment.h"
#import <StoreKit/StoreKit.h>

static LgSlot slotPayment = {NULL};

static void fireNotfoundEvent(const char *productId) {
	struct LgPaymentEvent evt;
	
	evt.type = LgPaymentEventPurchased;
	evt.productId = productId;
	evt.receipt = NULL;
	
	LgSlotCall(&slotPayment, &evt);
}

static void firePaymentEvent(enum LgPaymentEventType type, SKPaymentTransaction *transaction) {
	struct LgPaymentEvent evt;
	
	evt.type = LgPaymentEventPurchased;
	evt.productId = [transaction.payment.productIdentifier cStringUsingEncoding:NSUTF8StringEncoding];
	evt.receipt = NULL;
	
	if (transaction.transactionState == SKPaymentTransactionStatePurchased) {
		NSString * receipt = [transaction.transactionReceipt base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
		evt.receipt = cJSON_CreateString([receipt cStringUsingEncoding:NSUTF8StringEncoding]);
	}
	
	LgSlotCall(&slotPayment, &evt);
	if (evt.receipt) cJSON_Delete(evt.receipt);
}

#pragma mark - PaymentProcess

@interface PaymentProcess : NSObject<SKProductsRequestDelegate, SKPaymentTransactionObserver> {
	
}

@end

@implementation PaymentProcess

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
	// 产品查询回调函数
	NSArray *products = response.products;
	
	if (products.count == 0) {
		// 没有找到商品
		fireNotfoundEvent([(NSString *)response.invalidProductIdentifiers[0] cStringUsingEncoding:NSUTF8StringEncoding]);
		firePaymentEvent(LgPaymentEventNotFound, NULL);
		return;
	}
	
	// 找到该商品了，开始购买流程
	SKPayment * payment = [SKPayment paymentWithProduct:products[0]];
	[[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
	for (SKPaymentTransaction *transaction in transactions) {
		switch (transaction.transactionState) {
			case SKPaymentTransactionStatePurchased: // 交易完成
				[self completeTransaction:transaction];
				break;
			case SKPaymentTransactionStateFailed: // 交易失败
				[self failedTransaction:transaction];
				break;
			case SKPaymentTransactionStatePurchasing: // 商品添加进队列
				break;
		}
	}
}

- (void)completeTransaction:(SKPaymentTransaction *)transaction {
	firePaymentEvent(LgPaymentEventPurchased, transaction);
	[[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction {
	if (transaction.error.code != SKErrorPaymentCancelled) {
		// 用户没有取消交易
		firePaymentEvent(LgPaymentEventFailed, transaction);
	}
	[[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

@end

#pragma mark LgPayment

static PaymentProcess *g_paymentProc = NULL;

LgBool LgPaymentOpen() {
	g_paymentProc = [PaymentProcess alloc];
	[[SKPaymentQueue defaultQueue] addTransactionObserver:g_paymentProc];
	return LgTrue;
}

void LgPaymentClose() {
	[[SKPaymentQueue defaultQueue] removeTransactionObserver:g_paymentProc];
	[g_paymentProc release];
	LgSlotClear(&slotPayment);
}

LgBool LgPaymentCanMakePayments() {
	return [SKPaymentQueue canMakePayments];
}

LgBool LgPaymentPurchase(const char *productId) {
	NSString *sProductId = [NSString stringWithUTF8String:productId];
	NSSet *set = [NSSet setWithArray:@[sProductId]];
	SKProductsRequest *request = [[SKProductsRequest alloc] initWithProductIdentifiers: set];
	request.delegate = g_paymentProc;
	[request start];
	return LgTrue;
}

LgSlot *LgPaymentSlot() {
	return &slotPayment;
}
