#ifndef _LG_LRU_H
#define _LG_LRU_H

#include "lg_basetypes.h"

typedef struct LgLru *LgLruP;

LgLruP LgLruCreate(int maxcount, LgCompare compareKey, LgDestroy destroyKey, LgDestroy destroyData);

void LgLruDestroy(LgLruP lru);

LgBool LgLruInsert(LgLruP lru, void *key, void *value);

void *LgLruLookup(LgLruP lru, void *key);

void LgLruClear(LgLruP lru);

int LgLruCount(LgLruP lru);

#endif