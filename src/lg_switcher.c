#include "lg_switcher.h"
#include "lg_sprite.h"
#include "lg_action.h"

static void switcherEventProc(LgEventType type, LgNodeP node, void *param) {
	LgSwitcherDataP sd = (LgSwitcherDataP)LgNodeUserData(node);

	switch(type) {
	case LG_NODE_EVENT_ClearSlots:
		LgSlotClear(&sd->slotChange);
		break;
	}

	LgControlEventProc(type, node, param);
}

LgNodeP LgSwitcherCreateR(const LgVector2P size) {
	return LgControlNodeCreateSubClassR(sizeof(struct LgSwitcherData), (LgEvent)switcherEventProc, size);
}

LgBool LgNodeIsSwitcher(LgNodeP node) {
	return LgNodeEventProc(node) == (LgEvent)switcherEventProc;
}

LgBool LgSwitcherValue(LgNodeP node) {
	LgSwitcherDataP sd = (LgSwitcherDataP)LgNodeUserData(node);
	return sd->value;
}

static void updateButtonPosition(LgSwitcherDataP sd) {
	if (sd->buttonNode) {
		struct LgVector2 pos;
		LgActionP move, ease;

		pos.x = sd->value ? sd->base.size.x : 0;
		pos.y = sd->base.size.y / 2;

		move = LgActionMoveToCreate(0.1, &pos);
		ease = LgActionEaseInCreate(move, 0.3f);
		LgNodeDoAction(sd->buttonNode, ease);
		LgActionRelease(ease);
		LgActionRelease(move);
	}
}

void LgSwitcherSetValue(LgNodeP node, LgBool value) {
	LgSwitcherDataP sd = (LgSwitcherDataP)LgNodeUserData(node);
	sd->value = value;
	updateButtonPosition(sd);
}

static void slotButtonTouch(void *param, void *userdata) {
	LgNodeTouchEventP evt = (LgNodeTouchEventP)param;
	LgNodeP switcher = (LgNodeP)userdata;
	LgSwitcherDataP sd = (LgSwitcherDataP)LgNodeUserData(switcher);

	if (evt->type == LgNodeTouchBegan) {
		LgSwitcherSetValue(switcher, !LgSwitcherValue(switcher));
		LgSlotCall(&sd->slotChange, NULL);
	}
}

void LgSwitcherSetButton(LgNodeP node, LgNodeP buttonNode) {
	if (buttonNode && LgNodeIsSprite(buttonNode)) {
		LgSwitcherDataP sd = (LgSwitcherDataP)LgNodeUserData(node);
		if (sd->buttonNode) LgNodeRemove(sd->buttonNode);
		sd->buttonNode = buttonNode;
		LgNodeAddChild(node, buttonNode);
		LgNodeSetZorder(node, LG_UILAYER_CHILD);
		updateButtonPosition(sd);
		LgSlotAssign(LgNodeSlotTouch(buttonNode), slotButtonTouch, NULL, NULL, node);
		LgNodeEnableHitTest(buttonNode, LgTrue);
	}
}

LgSlot *LgSwitcherSlotChange(LgNodeP node) {
	LgSwitcherDataP sd = (LgSwitcherDataP)LgNodeUserData(node);
	return &sd->slotChange;
}
