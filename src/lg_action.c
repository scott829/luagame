﻿#include "lg_action.h"
#include "lg_memory.h"
#include "lg_array.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "lg_scriptenv.h"
#include "lg_actionmanager.h"
#include "lg_director.h"

#ifndef M_PI_X_2
#	define M_PI_X_2 (float)M_PI * 2.0f
#endif

LG_REFCNT_FUNCTIONS(LgAction);

static int g_actionCount = 0;

int LgActionCount() {
	return g_actionCount;
}

void LgActionDoStop(LgActionP action) {
	if (action->node) {
		LgActionManagerStop(action->node, action);
	}
}

static void actionObjDestroy(LgActionP action) {
	action->impl->destroy(action);
	g_actionCount--;
}

void *createAction(size_t size) {
	LgActionP action = (LgActionP)LgMallocZ(size);
	LG_REFCNT_INIT(action, actionObjDestroy);
	g_actionCount++;
	return action;
}

// base action ---------------------------------------------------------------------------------------------------------------------

static void actionDestroy(LgActionP action) {
	if (action->node) LgNodeRelease(action->node);
	LgFree(action);
}

static void actionAttachNode(LgActionP action, LgNodeP node) {
	if (action->node) LgNodeRelease(action->node);
	action->node = node;
	LgNodeRetain(node);
}

// loop ---------------------------------------------------------------------------------------------------------------------
struct LgActionLoop {
	struct LgAction	base;
	LgActionP		action;
	int				times, totalTimes;
};

static void loopDestroy(LgActionP self) {
	struct LgActionLoop *loop = (struct LgActionLoop *)self;
	LgActionRelease(loop->action);
	actionDestroy((LgActionP)loop);
}

static LgBool loopUpdate(LgActionP self, LgTime delta) {
	struct LgActionLoop *loop = (struct LgActionLoop *)self;
	if (!LgActionUpdate(loop->action, delta)) {
		loop->times++;
		if (loop->times == loop->totalTimes) return LgFalse;
		LgActionStop(loop->action);
		LgActionReset(loop->action);
	}

	return LgTrue;
}

static void loopReset(LgActionP self) {
	struct LgActionLoop *loop = (struct LgActionLoop *)self;
	LgActionReset(loop->action);
	loop->times = 0;
}

static void loopStop(LgActionP self) {
	struct LgActionLoop *loop = (struct LgActionLoop *)self;
	LgActionStop(loop->action);
}

static void loopAttachNode(LgActionP self, LgNodeP node) {
	struct LgActionLoop *loop = (struct LgActionLoop *)self;
	LgActionAttachNode(loop->action, node);
	actionAttachNode(self, node);
}

static LgActionP loopClone(LgActionP self) {
	struct LgActionLoop *loop = (struct LgActionLoop *)self;
	return LgActionLoopCreate(loop->action, loop->totalTimes);
}

static struct LgActionImpl loopImpl = {
	loopDestroy,
	loopUpdate,
	loopReset,
	loopStop,
	loopAttachNode,
	loopClone,
};

LgActionP LgActionLoopCreate(LgActionP action, int times) {
	struct LgActionLoop *loop = (struct LgActionLoop *)createAction(sizeof(struct LgActionLoop));
	loop->base.impl = &loopImpl;
	loop->action = action;
	LgActionRetain(action);
	loop->totalTimes = times;
	loop->times = 0;
	return (LgActionP)loop;
}

// loop forever ---------------------------------------------------------------------------------------------------------------------
struct LgActionLoopForever {
	struct LgAction	base;
	LgActionP		action;
};

static void loopForeverDestroy(LgActionP self) {
	struct LgActionLoopForever *loop = (struct LgActionLoopForever *)self;
	LgActionRelease(loop->action);
	actionDestroy((LgActionP)loop);
}

static LgBool loopForeverUpdate(LgActionP self, LgTime delta) {
	struct LgActionLoopForever *loop = (struct LgActionLoopForever *)self;
	if (!LgActionUpdate(loop->action, delta)) {
		LgActionStop(loop->action);
		LgActionReset(loop->action);
	}
	return LgTrue;
}

static void loopForeverReset(LgActionP self) {
	struct LgActionLoopForever *loop = (struct LgActionLoopForever *)self;
	LgActionReset(loop->action);
}

static void loopForeverStop(LgActionP self) {
	struct LgActionLoopForever *loop = (struct LgActionLoopForever *)self;
	LgActionStop(loop->action);
}

static void loopForeverAttachNode(LgActionP self, LgNodeP node) {
	struct LgActionLoopForever *loop = (struct LgActionLoopForever *)self;
	LgActionAttachNode(loop->action, node);
	actionAttachNode(self, node);
}

static LgActionP loopForeverClone(LgActionP self) {
	struct LgActionLoopForever *loop = (struct LgActionLoopForever *)self;
	return LgActionLoopForeverCreate(loop->action);
}

static struct LgActionImpl loopForeverImpl = {
	loopForeverDestroy,
	loopForeverUpdate,
	loopForeverReset,
	loopForeverStop,
	loopForeverAttachNode,
	loopForeverClone,
};

LgActionP LgActionLoopForeverCreate(LgActionP action) {
	struct LgActionLoopForever *loop = (struct LgActionLoopForever *)createAction(sizeof(struct LgActionLoopForever));
	loop->base.impl = &loopForeverImpl;
	loop->action = action;
	LgActionRetain(action);
	return (LgActionP)loop;
}

// sequence ---------------------------------------------------------------------------------------------------------------------
struct LgActionSequence {
	struct LgAction	base;
	LgArrayP		actions;
	int				current;
};

static void sequenceDestroy(LgActionP self) {
	struct LgActionSequence *sequence = (struct LgActionSequence *)self;
	LgArrayDestroy(sequence->actions);
	actionDestroy((LgActionP)sequence);
}

static LgBool sequenceUpdate(LgActionP self, LgTime delta) {
	struct LgActionSequence *sequence = (struct LgActionSequence *)self;
	LgActionP currentAction;

	currentAction = (LgActionP)LgArrayGet(sequence->actions, sequence->current);

	if (!LgActionUpdate(currentAction, delta)) {
		// 下一个动作
		LgActionStop(currentAction);
		sequence->current++;

		while(sequence->current < LgArrayLen(sequence->actions)) {
			currentAction = (LgActionP)LgArrayGet(sequence->actions, sequence->current);
			LgActionReset(currentAction);
			if (LgActionUpdate(currentAction, 0)) break;
			LgActionStop(currentAction);
			sequence->current++;
		}

		if (sequence->current >= LgArrayLen(sequence->actions)) {
			return LgFalse;
		}
	}

	return LgTrue;
}

static void sequenceReset(LgActionP self) {
	struct LgActionSequence *sequence = (struct LgActionSequence *)self;
	sequence->current = 0;
	if (LgArrayLen(sequence->actions) > 0)
		LgActionReset((LgActionP)LgArrayGet(sequence->actions, 0));
}

static void sequenceStop(LgActionP self) {
	struct LgActionSequence *sequence = (struct LgActionSequence *)self;
	int i;
	for (i = 0; i < LgArrayLen(sequence->actions); i++) {
		LgActionStop((LgActionP)LgArrayGet(sequence->actions, i));
	}
}

static void sequenceAttachNode(LgActionP self, LgNodeP node) {
	struct LgActionSequence *sequence = (struct LgActionSequence *)self;
	int i;
	for (i = 0; i < LgArrayLen(sequence->actions); i++)
		LgActionAttachNode((LgActionP)LgArrayGet(sequence->actions, i), node);
	actionAttachNode(self, node);
}

static LgActionP sequenceClone(LgActionP self) {
	struct LgActionSequence *sequence = (struct LgActionSequence *)self;
	return LgActionSequenceCreate((LgActionP *)LgArrayData(sequence->actions), LgArrayLen(sequence->actions));
}

static struct LgActionImpl sequenceImpl = {
	sequenceDestroy,
	sequenceUpdate,
	sequenceReset,
	sequenceStop,
	sequenceAttachNode,
	sequenceClone,
};

LgActionP LgActionSequenceCreate(LgActionP *actions, int count) {
	struct LgActionSequence *sequence = (struct LgActionSequence *)createAction(sizeof(struct LgActionSequence));
	int i;

	sequence->base.impl = &sequenceImpl;
	sequence->actions = LgArrayCreate((LgDestroy)LgActionRelease);
	for (i = 0; i < count; i++) {
		LgActionRetain(actions[i]);
		LgArrayAppend(sequence->actions, actions[i]);
	}
	return (LgActionP)sequence;
}

// call ---------------------------------------------------------------------------------------------------------------------
struct LgActionCall {
	struct LgAction	base;
	LgSlot			slot;
};

static void callDestroy(LgActionP action) {
	struct LgActionCall *call = (struct LgActionCall *)action;
	LgSlotClear(&call->slot);
	actionDestroy(action);
}

static LgActionP callClone(LgActionP self) {
	struct LgActionCall *call = (struct LgActionCall *)self;
	LgSlot slot;

	LgZeroMemory(&slot, sizeof(slot));
	LgSlotClone(&slot, &call->slot);
	return LgActionCallCreate(SLOT_PARAMS_VALUE_REF(slot));
}

static LgBool callUpdate(LgActionP self, LgTime delta) {
	struct LgActionCall *call = (struct LgActionCall *)self;
	LgSlotCall(&call->slot, NULL);
	return LgFalse;
}

void callStop(LgActionP self) {
	struct LgActionCall *call = (struct LgActionCall *)self;
	LgSlotClear(&call->slot);
}

static struct LgActionImpl callImpl = {
	callDestroy,
	callUpdate,
	NULL,
	callStop,
	NULL,
	callClone,
};

LgActionP LgActionCallCreate(SLOT_PARAMS) {
	struct LgActionCall *call = (struct LgActionCall *)createAction(sizeof(struct LgActionCall));
	((LgActionP)call)->impl = (struct LgActionImpl *)&callImpl;
	LgSlotAssign(&call->slot, SLOT_PARAMS_VALUE);
	return (LgActionP)call;
}

// interval ---------------------------------------------------------------------------------------------------------------------
#define LgActionIntervalStep(action, ratio) \
	if (((struct LgIntervalImpl *)(((LgActionP)action)->impl))->step) \
		((struct LgIntervalImpl *)(((LgActionP)action)->impl))->step((LgActionP)action, ratio);


struct LgIntervalImpl {
	struct LgActionImpl base;
	void (* step)(LgActionP action, float ratio);
};

typedef struct LgActionInterval {
	struct LgAction	base;
	LgTime			duration, time;
}*LgActionIntervalP;

#define DURATION(action) (((struct LgActionInterval *)action)->duration)

static void intervalDestroy(LgActionP action) {
	actionDestroy(action);
}

static LgBool intervalUpdate(LgActionP self, LgTime delta) {
	struct LgActionInterval *interval = (struct LgActionInterval *)self;
	float ratio;

	interval->time += delta;
	ratio = (float)(interval->time / interval->duration);
	if (ratio >= 1) {
		LgActionIntervalStep(interval, 1);
		return LgFalse;
	} else {
		LgActionIntervalStep(interval, ratio);
		return LgTrue;
	}
}

static void intervalReset(LgActionP self) {
	struct LgActionInterval *interval = (struct LgActionInterval *)self;
	interval->time = 0;
}

// lua interval ---------------------------------------------------------------------------------------------------------------------
#define LUA_INTERVAL_RESET "reset"
#define LUA_INTERVAL_STEP "step"
#define LUA_INTERVAL_STOP "stop"

struct LgActionLuaInterval {
	struct LgActionInterval	base;
	int						func;
	int						context;
};

static void luaIntervalDestroy(LgActionP self) {
	struct LgActionLuaInterval *luaInterval = (struct LgActionLuaInterval *)self;
	lua_State *L = LgScriptEnvState();
	if (luaInterval->func) luaL_unref(L, LUA_REGISTRYINDEX, luaInterval->func);
	if (luaInterval->context) luaL_unref(L, LUA_REGISTRYINDEX, luaInterval->context);
	intervalDestroy(self);
}

static void luaIntervalPushContext(struct LgActionLuaInterval *luaInterval) {
	lua_State *L = LgScriptEnvState();
	if (luaInterval->context) lua_rawgeti(L, LUA_REGISTRYINDEX, luaInterval->context);
	else lua_pushnil(L);
}

static void luaIntervalReset(LgActionP self) {
	struct LgActionLuaInterval *luaInterval = (struct LgActionLuaInterval *)self;
	lua_State *L = LgScriptEnvState();
	lua_rawgeti(L, LUA_REGISTRYINDEX, luaInterval->func);
	lua_getfield(L, -1, LUA_INTERVAL_RESET);

	if (!lua_isnil(L, -1)) {
		luaIntervalPushContext(luaInterval);
		if (lua_pcall(L, 1, 0, 0)) {
			LgScriptShowError();
		}
		lua_pop(L, 1);
	} else {
		lua_pop(L, 2);
	}

	intervalReset(self);
}

static LgActionP luaIntervalClone(LgActionP self) {
	struct LgActionLuaInterval *luaInterval = (struct LgActionLuaInterval *)self;
	return LgActionLuaIntervalCreate(DURATION(luaInterval), luaInterval->func, luaInterval->context);
}

void luaIntervalStep(LgActionP self, float ratio) {
	struct LgActionLuaInterval *luaInterval = (struct LgActionLuaInterval *)self;
	lua_State *L = LgScriptEnvState();
	if (luaInterval->func) {
		lua_rawgeti(L, LUA_REGISTRYINDEX, luaInterval->func);
		lua_getfield(L, -1, LUA_INTERVAL_STEP);

		if (!lua_isnil(L, -1)) {
			luaIntervalPushContext(luaInterval);
			lua_pushnumber(L, ratio);
			if (lua_pcall(L, 2, 0, 0)) {
				LgScriptShowError();
			}
			lua_pop(L, 1);
		} else {
			lua_pop(L, 2);
		}
	}
}

void luaIntervalStop(LgActionP self) {
	struct LgActionLuaInterval *luaInterval = (struct LgActionLuaInterval *)self;
	lua_State *L = LgScriptEnvState();

	if (luaInterval->func) {
		lua_rawgeti(L, LUA_REGISTRYINDEX, luaInterval->func);
		lua_getfield(L, -1, LUA_INTERVAL_STOP);

		if (!lua_isnil(L, -1)) {
			luaIntervalPushContext(luaInterval);
			if (lua_pcall(L, 1, 0, 0)) {
				LgScriptShowError();
			}
			lua_pop(L, 1);
		} else {
			lua_pop(L, 2);
		}

		if (luaInterval->func) luaL_unref(L, LUA_REGISTRYINDEX, luaInterval->func);
		if (luaInterval->context) luaL_unref(L, LUA_REGISTRYINDEX, luaInterval->context);
		luaInterval->func = luaInterval->context = 0;
	}
}

static struct LgIntervalImpl luaIntervalImpl = {
	{
		luaIntervalDestroy,
		intervalUpdate,
		luaIntervalReset,
		luaIntervalStop,
		actionAttachNode,
		luaIntervalClone,
	},
	luaIntervalStep,
};

LgActionP LgActionLuaIntervalCreate(LgTime duration, int func, int context) {
	struct LgActionLuaInterval *luaInterval = (struct LgActionLuaInterval *)createAction(sizeof(struct LgActionLuaInterval));
	((LgActionP)luaInterval)->impl = (struct LgActionImpl *)&luaIntervalImpl;
	DURATION(luaInterval) = duration;
	luaInterval->func = func;
	luaInterval->context = context;
	return (LgActionP)luaInterval;
}

// camera ---------------------------------------------------------------------------------------------------------------------
struct LgActionCamera {
	struct LgActionInterval	base;
	struct LgVector2		orgCenter, destCenter;
	float					orgScale, destScale;
};

static void cameraReset(LgActionP self) {
	struct LgActionCamera *camera = (struct LgActionCamera *)self;
	LgDirectorCamera(&camera->orgCenter.x, &camera->orgCenter.y, &camera->orgScale);
	intervalReset(self);
}

static LgActionP cameraClone(LgActionP self) {
	struct LgActionCamera *camera = (struct LgActionCamera *)self;
	return LgActionCameraCreate(DURATION(camera), camera->destCenter.x, camera->destCenter.y, camera->destScale);
}

void cameraStep(LgActionP self, float ratio) {
	struct LgActionCamera *camera = (struct LgActionCamera *)self;
	float newCenterX, newCenterY, newScale;
	newCenterX = camera->orgCenter.x + (camera->destCenter.x - camera->orgCenter.x) * ratio;
	newCenterY = camera->orgCenter.y + (camera->destCenter.y - camera->orgCenter.y) * ratio;
	newScale = camera->orgScale + (camera->destScale - camera->orgScale) * ratio;
	LgDirectorSetCamera(newCenterX, newCenterY, newScale);
}

static struct LgIntervalImpl cameraImpl = {
	{
		intervalDestroy,
		intervalUpdate,
		cameraReset,
		NULL,
		actionAttachNode,
		cameraClone,
	},
	cameraStep,
};

LgActionP LgActionCameraCreate(LgTime duration, float centerX, float centerY, float scale) {
	struct LgActionCamera *camera = (struct LgActionCamera *)createAction(sizeof(struct LgActionCamera));
	((LgActionP)camera)->impl = (struct LgActionImpl *)&cameraImpl;
	DURATION(camera) = duration;
	camera->destCenter.x = centerX;
	camera->destCenter.y = centerY;
	camera->destScale = scale;
	return (LgActionP)camera;
}

// moveto ---------------------------------------------------------------------------------------------------------------------
struct LgActionMoveTo {
	struct LgActionInterval	base;
	struct LgVector2		org, dest;
};

static void movetoReset(LgActionP self) {
	struct LgActionMoveTo *moveto = (struct LgActionMoveTo *)self;
	LgNodePosition(LgActionNode(moveto), &moveto->org);
	intervalReset(self);
}

static LgActionP movetoClone(LgActionP self) {
	struct LgActionMoveTo *moveto = (struct LgActionMoveTo *)self;
	return LgActionMoveToCreate(DURATION(moveto), &moveto->dest);
}

void movetoStep(LgActionP self, float ratio) {
	struct LgActionMoveTo *moveto = (struct LgActionMoveTo *)self;
	struct LgVector2 newPosition;
	newPosition.x = moveto->org.x + (moveto->dest.x - moveto->org.x) * ratio;
	newPosition.y = moveto->org.y + (moveto->dest.y - moveto->org.y) * ratio;
	LgNodeSetPosition(LgActionNode(moveto), &newPosition);
}

static struct LgIntervalImpl movetoImpl = {
	{
		intervalDestroy,
		intervalUpdate,
		movetoReset,
		NULL,
		actionAttachNode,
		movetoClone,
	},
	movetoStep,
};

LgActionP LgActionMoveToCreate(LgTime duration, const LgVector2P position) {
	struct LgActionMoveTo *moveto = (struct LgActionMoveTo *)createAction(sizeof(struct LgActionMoveTo));
	((LgActionP)moveto)->impl = (struct LgActionImpl *)&movetoImpl;
	DURATION(moveto) = duration;
	LgCopyMemory(&moveto->dest, position, sizeof(struct LgVector2));
	return (LgActionP)moveto;
}

// moveby ---------------------------------------------------------------------------------------------------------------------
struct LgActionMoveBy {
	struct LgActionMoveTo	base;
	struct LgVector2		delta;
};

static void movebyReset(LgActionP self) {
	struct LgActionMoveBy *moveby = (struct LgActionMoveBy *)self;
	LgNodePosition(LgActionNode(moveby), &moveby->base.org);
	moveby->base.dest.x = moveby->base.org.x + moveby->delta.x;
	moveby->base.dest.y = moveby->base.org.y + moveby->delta.y;
	intervalReset(self);
}

static LgActionP movebyClone(LgActionP self) {
	struct LgActionMoveBy *moveby = (struct LgActionMoveBy *)self;
	return LgActionMoveByCreate(DURATION(moveby), &moveby->delta);
}

static struct LgIntervalImpl movebyImpl = {
	{
		intervalDestroy,
		intervalUpdate,
		movebyReset,
		NULL,
		actionAttachNode,
		movebyClone,
	},
	movetoStep,
};

LgActionP LgActionMoveByCreate(LgTime duration, const LgVector2P delta) {
	struct LgActionMoveBy *moveby = (struct LgActionMoveBy *)createAction(sizeof(struct LgActionMoveBy));
	((LgActionP)moveby)->impl = (struct LgActionImpl *)&movebyImpl;
	DURATION(moveby) = duration;
	LgCopyMemory(&moveby->delta, delta, sizeof(struct LgVector2));
	return (LgActionP)moveby;
}

// bezierto ---------------------------------------------------------------------------------------------------------------------
struct LgActionBezierTo {
	struct LgActionInterval	base;
	struct LgVector2		org, dest;
	struct LgVector2		cp1, cp2;
	struct LgVector2		prevPosition;
};

static void beziertoReset(LgActionP self) {
	struct LgActionBezierTo *bezierto = (struct LgActionBezierTo *)self;
	LgNodePosition(LgActionNode(bezierto), &bezierto->org);
	LgNodePosition(LgActionNode(bezierto), &bezierto->prevPosition);
	intervalReset(self);
}

static float bezierAt(float a, float b, float c, float d, float t) {
	return (powf(1 - t, 3) * a + 
		3 * t * (powf(1 - t, 2)) * b + 
		3 * powf(t, 2) * (1 - t) * c +
		powf(t, 3) * d);
}

static LgActionP beziertoClone(LgActionP self) {
	struct LgActionBezierTo *bezierto = (struct LgActionBezierTo *)self;
	return LgActionBezierToCreate(DURATION(bezierto), &bezierto->cp1, &bezierto->cp2, &bezierto->dest);
}

void beziertoStep(LgActionP self, float ratio) {
	struct LgActionBezierTo *bezierto = (struct LgActionBezierTo *)self;

	float xa = 0;
	float xb = bezierto->cp1.x;
	float xc = bezierto->cp2.x;
	float xd = bezierto->dest.x;

	float ya = 0;
	float yb = bezierto->cp1.y;
	float yc = bezierto->cp2.y;
	float yd = bezierto->dest.y;

	float x = bezierAt(xa, xb, xc, xd, ratio);
	float y = bezierAt(ya, yb, yc, yd, ratio);
	struct LgVector2 diff, newPos;

	LgNodePosition(LgActionNode(self), &diff);
	LgVector2Sub(&diff, &bezierto->prevPosition);
	LgVector2Add(&bezierto->org, &diff);
	LgCopyMemory(&newPos, &bezierto->org, sizeof(struct LgVector2));
	newPos.x += x, newPos.y += y;
	LgNodeSetPosition(LgActionNode(self), &newPos);
	LgCopyMemory(&bezierto->prevPosition, &newPos, sizeof(struct LgVector2));
}

static struct LgIntervalImpl beziertoImpl = {
	{
		intervalDestroy,
		intervalUpdate,
		beziertoReset,
		NULL,
		actionAttachNode,
		beziertoClone,
	},
	beziertoStep,
};

LgActionP LgActionBezierToCreate(LgTime duration, const LgVector2P cp1, const LgVector2P cp2, const LgVector2P endPoint) {
	struct LgActionBezierTo *bezierto = (struct LgActionBezierTo *)createAction(sizeof(struct LgActionBezierTo));
	((LgActionP)bezierto)->impl = (struct LgActionImpl *)&beziertoImpl;
	DURATION(bezierto) = duration;
	LgCopyMemory(&bezierto->cp1, cp1, sizeof(struct LgVector2));
	LgCopyMemory(&bezierto->cp2, cp2, sizeof(struct LgVector2));
	LgCopyMemory(&bezierto->dest, endPoint, sizeof(struct LgVector2));
	return (LgActionP)bezierto;
}

// bezierby ---------------------------------------------------------------------------------------------------------------------
struct LgActionBezierBy {
	struct LgActionBezierTo	base;
	struct LgVector2		Rcp1, Rcp2, Rdest;
};

static void bezierbyReset(LgActionP self) {
	struct LgActionBezierBy *bezierby = (struct LgActionBezierBy *)self;
	beziertoReset(self);

	LgCopyMemory(&bezierby->base.dest, &bezierby->base.org, sizeof(struct LgVector2));
	LgVector2Add(&bezierby->base.dest, &bezierby->Rdest);

	LgCopyMemory(&bezierby->base.cp1, &bezierby->base.org, sizeof(struct LgVector2));
	LgVector2Add(&bezierby->base.cp1, &bezierby->Rcp1);

	LgCopyMemory(&bezierby->base.cp2, &bezierby->base.org, sizeof(struct LgVector2));
	LgVector2Add(&bezierby->base.cp2, &bezierby->Rcp2);
}

static LgActionP bezierbyClone(LgActionP self) {
	struct LgActionBezierBy *bezierby = (struct LgActionBezierBy *)self;
	return LgActionBezierByCreate(DURATION(bezierby), &bezierby->Rcp1, &bezierby->Rcp2, &bezierby->Rdest);
}

static struct LgIntervalImpl bezierbyImpl = {
	{
		intervalDestroy,
		intervalUpdate,
		bezierbyReset,
		NULL,
		actionAttachNode,
		bezierbyClone,
	},
	beziertoStep,
};

LgActionP LgActionBezierByCreate(LgTime duration, const LgVector2P Rcp1, const LgVector2P Rcp2, const LgVector2P RendPoint) {
	struct LgActionBezierBy *bezierby = (struct LgActionBezierBy *)createAction(sizeof(struct LgActionBezierBy));
	((LgActionP)bezierby)->impl = (struct LgActionImpl *)&bezierbyImpl;
	DURATION(bezierby) = duration;
	LgCopyMemory(&bezierby->Rcp1, Rcp1, sizeof(struct LgVector2));
	LgCopyMemory(&bezierby->Rcp2, Rcp2, sizeof(struct LgVector2));
	LgCopyMemory(&bezierby->Rdest, RendPoint, sizeof(struct LgVector2));
	return (LgActionP)bezierby;
}

// scaleto ---------------------------------------------------------------------------------------------------------------------
struct LgActionScaleTo {
	struct LgActionInterval	base;
	struct LgVector2		org, dest;
};

static void scaletoReset(LgActionP self) {
	struct LgActionScaleTo *scaleto = (struct LgActionScaleTo *)self;
	LgNodeScale(LgActionNode(scaleto), &scaleto->org);
	intervalReset(self);
}

static LgActionP scaletoClone(LgActionP self) {
	struct LgActionScaleTo *scaleto = (struct LgActionScaleTo *)self;
	return LgActionScaleToCreate(DURATION(scaleto), &scaleto->dest);
}

void scaletoStep(LgActionP self, float ratio) {
	struct LgActionScaleTo *scaleto = (struct LgActionScaleTo *)self;
	struct LgVector2 newScale;
	newScale.x = scaleto->org.x + (scaleto->dest.x - scaleto->org.x) * ratio;
	newScale.y = scaleto->org.y + (scaleto->dest.y - scaleto->org.y) * ratio;
	LgNodeSetScale(LgActionNode(scaleto), &newScale);
}

static struct LgIntervalImpl scaletoImpl = {
	{
		intervalDestroy,
		intervalUpdate,
		scaletoReset,
		NULL,
		actionAttachNode,
		scaletoClone,
	},
	scaletoStep,
};

LgActionP LgActionScaleToCreate(LgTime duration, const LgVector2P scale) {
	struct LgActionScaleTo *scaleto = (struct LgActionScaleTo *)createAction(sizeof(struct LgActionScaleTo));
	((LgActionP)scaleto)->impl = (struct LgActionImpl *)&scaletoImpl;
	DURATION(scaleto) = duration;
	LgCopyMemory(&scaleto->dest, scale, sizeof(struct LgVector2));
	return (LgActionP)scaleto;
}

// scaleby ---------------------------------------------------------------------------------------------------------------------
struct LgActionScaleBy {
	struct LgActionScaleTo	base;
	struct LgVector2		delta;
};

static void scalebyReset(LgActionP self) {
	struct LgActionScaleBy *scaleby = (struct LgActionScaleBy *)self;
	LgNodeScale(LgActionNode(scaleby), &scaleby->base.org);
	scaleby->base.dest.x = scaleby->base.org.x + scaleby->delta.x;
	scaleby->base.dest.y = scaleby->base.org.y + scaleby->delta.y;
	intervalReset(self);
}

static LgActionP scalebyClone(LgActionP self) {
	struct LgActionScaleBy *scaleby = (struct LgActionScaleBy *)self;
	return LgActionScaleByCreate(DURATION(scaleby), &scaleby->delta);
}

static struct LgIntervalImpl scalebyImpl = {
	{
		intervalDestroy,
		intervalUpdate,
		scalebyReset,
		NULL,
		actionAttachNode,
		scalebyClone,
	},
	scaletoStep,
};

LgActionP LgActionScaleByCreate(LgTime duration, const LgVector2P delta) {
	struct LgActionScaleBy *scaleby = (struct LgActionScaleBy *)createAction(sizeof(struct LgActionScaleBy));
	((LgActionP)scaleby)->impl = (struct LgActionImpl *)&scalebyImpl;
	DURATION(scaleby) = duration;
	LgCopyMemory(&scaleby->delta, delta, sizeof(struct LgVector2));
	return (LgActionP)scaleby;
}

// rotateto ---------------------------------------------------------------------------------------------------------------------
struct LgActionRotateTo {
	struct LgActionInterval	base;
	float					d, od;
};

static void rotatetoReset(LgActionP self) {
	struct LgActionRotateTo *rotateto = (struct LgActionRotateTo *)self;
	rotateto->od = LgNodeRotate(LgActionNode(rotateto));
	intervalReset(self);
}

static LgActionP rotatetoClone(LgActionP self) {
	struct LgActionRotateTo *rotateto = (struct LgActionRotateTo *)self;
	return LgActionRotateToCreate(DURATION(rotateto), rotateto->d);
}

void rotatetoStep(LgActionP self, float ratio) {
	struct LgActionRotateTo *rotateto = (struct LgActionRotateTo *)self;
	float newd = rotateto->od + (rotateto->d - rotateto->od) * ratio;
	LgNodeSetRotate(LgActionNode(rotateto), newd);
}

static struct LgIntervalImpl rotatetoImpl = {
	{
		intervalDestroy,
		intervalUpdate,
		rotatetoReset,
		NULL,
		actionAttachNode,
		rotatetoClone,
	},
	rotatetoStep,
};

LgActionP LgActionRotateToCreate(LgTime duration, float d) {
	struct LgActionRotateTo *rotateto = (struct LgActionRotateTo *)createAction(sizeof(struct LgActionRotateTo));
	((LgActionP)rotateto)->impl = (struct LgActionImpl *)&rotatetoImpl;
	DURATION(rotateto) = duration;
	rotateto->d = d;
	return (LgActionP)rotateto;
}

// rotateby ---------------------------------------------------------------------------------------------------------------------
struct LgActionRotateBy {
	struct LgActionRotateTo	base;
	float					delta;
};

static void rotatebyReset(LgActionP self) {
	struct LgActionRotateBy *rotateby = (struct LgActionRotateBy *)self;
	rotateby->base.od = LgNodeRotate(LgActionNode(rotateby));
	rotateby->base.d = rotateby->base.od + rotateby->delta;
	intervalReset(self);
}

static LgActionP rotatebyClone(LgActionP self) {
	struct LgActionRotateBy *rotateby = (struct LgActionRotateBy *)self;
	return LgActionRotateByCreate(DURATION(rotateby), rotateby->delta);
}

static struct LgIntervalImpl rotatebyImpl = {
	{
		intervalDestroy,
		intervalUpdate,
		rotatebyReset,
		NULL,
		actionAttachNode,
		rotatebyClone,
	},
	rotatetoStep,
};

LgActionP LgActionRotateByCreate(LgTime duration, float delta) {
	struct LgActionRotateBy *rotateby = (struct LgActionRotateBy *)createAction(sizeof(struct LgActionRotateBy));
	((LgActionP)rotateby)->impl = (struct LgActionImpl *)&rotatebyImpl;
	DURATION(rotateby) = duration;
	rotateby->delta = delta;
	return (LgActionP)rotateby;
}

// blink ---------------------------------------------------------------------------------------------------------------------
struct LgActionBlink {
	struct LgActionInterval	base;
	LgBool					visible;
	int						blinks;
};

static void blinkReset(LgActionP self) {
	struct LgActionBlink *blink = (struct LgActionBlink *)self;
	blink->visible = LgNodeIsVisible(LgActionNode(blink));
	intervalReset(self);
}

static LgActionP blinkClone(LgActionP self) {
	struct LgActionBlink *blink = (struct LgActionBlink *)self;
	return LgActionBlinkCreate(DURATION(blink), blink->blinks);
}

void blinkStep(LgActionP self, float ratio) {
	struct LgActionBlink *blink = (struct LgActionBlink *)self;
	if (ratio == 1) {
		LgNodeSetVisible(LgActionNode(blink), blink->visible);
	} else {
		float slice = 1.0f / blink->blinks;
		float m = fmodf(ratio, slice);
		LgNodeSetVisible(LgActionNode(blink), m > slice / 2 ? LgTrue : LgFalse);
	}
}

static struct LgIntervalImpl blinkImpl = {
	{
		intervalDestroy,
		intervalUpdate,
		blinkReset,
		NULL,
		actionAttachNode,
		blinkClone,
	},
	blinkStep,
};

LgActionP LgActionBlinkCreate(LgTime duration, int blinks) {
	struct LgActionBlink *blink = (struct LgActionBlink *)createAction(sizeof(struct LgActionBlink));
	((LgActionP)blink)->impl = (struct LgActionImpl *)&blinkImpl;
	DURATION(blink) = duration;
	blink->blinks = blinks;
	return (LgActionP)blink;
}

// fadein ---------------------------------------------------------------------------------------------------------------------
static LgActionP fadeinClone(LgActionP self) {
	struct LgActionInterval *fadein = (struct LgActionInterval *)self;
	return LgActionFadeInCreate(fadein->duration);
}

void fadeinStep(LgActionP self, float ratio) {
	struct LgActionInterval *fadein = (struct LgActionInterval *)self;
	LgNodeSetOpacity(LgActionNode(fadein), ratio);
}

static struct LgIntervalImpl fadeinImpl = {
	{
		intervalDestroy,
		intervalUpdate,
		intervalReset,
		NULL,
		actionAttachNode,
		fadeinClone,
	},
	fadeinStep,
};

LgActionP LgActionFadeInCreate(LgTime duration) {
	struct LgActionInterval *fadein = (struct LgActionInterval *)createAction(sizeof(struct LgActionInterval));
	((LgActionP)fadein)->impl = (struct LgActionImpl *)&fadeinImpl;
	fadein->duration = duration;
	return (LgActionP)fadein;
}

// fadeout ---------------------------------------------------------------------------------------------------------------------
static LgActionP fadeoutClone(LgActionP self) {
	struct LgActionInterval *fadeout = (struct LgActionInterval *)self;
	return LgActionFadeInCreate(fadeout->duration);
}

void fadeoutStep(LgActionP self, float ratio) {
	struct LgActionInterval *fadeout = (struct LgActionInterval *)self;
	LgNodeSetOpacity(LgActionNode(fadeout), 1.0f - ratio);
}

static struct LgIntervalImpl fadeoutImpl = {
	{
		intervalDestroy,
		intervalUpdate,
		intervalReset,
		NULL,
		actionAttachNode,
		fadeoutClone,
	},
	fadeoutStep,
};

LgActionP LgActionFadeOutCreate(LgTime duration) {
	struct LgActionInterval *fadeout = (struct LgActionInterval *)createAction(sizeof(struct LgActionInterval));
	((LgActionP)fadeout)->impl = (struct LgActionImpl *)&fadeoutImpl;
	fadeout->duration = duration;
	return (LgActionP)fadeout;
}

// delay ---------------------------------------------------------------------------------------------------------------------
static LgActionP delayClone(LgActionP self) {
	struct LgActionInterval *fadeout = (struct LgActionInterval *)self;
	return LgActionDelayCreate(fadeout->duration);
}

static struct LgIntervalImpl delayImpl = {
	{
		intervalDestroy,
		intervalUpdate,
		intervalReset,
		NULL,
		actionAttachNode,
		delayClone,
	},
	NULL,
};

LgActionP LgActionDelayCreate(LgTime duration) {
	struct LgActionInterval *delay = (struct LgActionInterval *)createAction(sizeof(struct LgActionInterval));
	((LgActionP)delay)->impl = (struct LgActionImpl *)&delayImpl;
	delay->duration = duration;
	return (LgActionP)delay;
}

// jumpto ---------------------------------------------------------------------------------------------------------------------
struct LgActionJump {
	struct LgActionInterval	base;
	struct LgVector2		org, offset;
	float					height;
	int						jumps;
};

static void jumpReset(LgActionP self) {
	struct LgActionJump *jump = (struct LgActionJump *)self;
	LgNodePosition(LgActionNode(jump), &jump->org);
	intervalReset(self);
}

static LgActionP jumpClone(LgActionP self) {
	struct LgActionJump *jump = (struct LgActionJump *)self;
	return LgActionJumpCreate(DURATION(jump), &jump->offset, jump->height, jump->jumps);
}

void jumpStep(LgActionP self, float ratio) {
	struct LgActionJump *jump = (struct LgActionJump *)self;
	struct LgVector2 xy;
	xy.y = jump->height * fabsf(sinf(ratio * (float)M_PI * jump->jumps));
	xy.y += ratio * jump->offset.y;
	xy.x = ratio * jump->offset.x;
	xy.x += jump->org.x;
	xy.y += jump->org.y;
	LgNodeSetPosition(LgActionNode(self), &xy);
}

static struct LgIntervalImpl jumpImpl = {
	{
		intervalDestroy,
		intervalUpdate,
		jumpReset,
		NULL,
		actionAttachNode,
		jumpClone,
	},
	jumpStep,
};

LgActionP LgActionJumpCreate(LgTime duration, const LgVector2P offset, float height, int jumps) {
	struct LgActionJump *jump = (struct LgActionJump *)createAction(sizeof(struct LgActionJump));
	((LgActionP)jump)->impl = (struct LgActionImpl *)&jumpImpl;
	DURATION(jump) = duration;
	LgCopyMemory(&jump->offset, offset, sizeof(struct LgVector2));
	jump->height = -height;
	jump->jumps = jumps;
	return (LgActionP)jump;
}

// speed ---------------------------------------------------------------------------------------------------------------------
struct LgActionSpeed {
	struct LgActionInterval	base;
	LgActionP				action;
	float					speed;
};

static void speedDestroy(LgActionP action) {
	struct LgActionSpeed *speed = (struct LgActionSpeed *)action;
	LgActionRelease(speed->action);
	intervalDestroy(action);
}

static LgActionP speedClone(LgActionP self) {
	struct LgActionSpeed *speed = (struct LgActionSpeed *)self;
	return LgActionSpeedCreate(speed->action, speed->speed);
}

static void speedStep(LgActionP self, float ratio) {
	struct LgActionSpeed *speed = (struct LgActionSpeed *)self;
	LgActionIntervalStep(speed->action, ratio);
}

static void speedStop(LgActionP self) {
	struct LgActionSpeed *speed = (struct LgActionSpeed *)self;
	LgActionStop(speed->action);
}

static void speedReset(LgActionP self) {
	struct LgActionSpeed *speed = (struct LgActionSpeed *)self;
	LgActionReset(speed->action);
	intervalReset(self);
}

static void speedAttachNode(LgActionP self, LgNodeP node) {
	struct LgActionSpeed *speed = (struct LgActionSpeed *)self;
	LgActionAttachNode(speed->action, node);
	actionAttachNode(self, node);
}

static struct LgIntervalImpl speedImpl = {
	{
		speedDestroy,
		intervalUpdate,
		speedReset,
		speedStop,
		speedAttachNode,
		speedClone,
	},
	speedStep,
};

LgActionP LgActionSpeedCreate(LgActionP action, float speed) {
	struct LgActionSpeed *speedact = (struct LgActionSpeed *)createAction(sizeof(struct LgActionSpeed));
	((LgActionP)speedact)->impl = (struct LgActionImpl *)&speedImpl;
	DURATION(speedact) = ((LgActionIntervalP)action)->duration / speed;
	speedact->action = action;
	LgActionRetain(action);
	return (LgActionP)speedact;
}

// range ---------------------------------------------------------------------------------------------------------------------
struct LgActionRange {
	struct LgActionInterval	base;
	LgActionP				action;
	float					start, end;
};

static void rangeDestroy(LgActionP action) {
	struct LgActionRange *range = (struct LgActionRange *)action;
	LgActionRelease(range->action);
	intervalDestroy(action);
}

static LgActionP rangeClone(LgActionP self) {
	struct LgActionRange *range = (struct LgActionRange *)self;
	return LgActionRangeCreate(range->action, range->start, range->end);
}

static void rangeStep(LgActionP self, float ratio) {
	struct LgActionRange *range = (struct LgActionRange *)self;
	float r;

	r = range->start + (range->end - range->start) * ratio;
	LgActionIntervalStep(range->action, r);
}

static void rangeStop(LgActionP self) {
	struct LgActionRange *range = (struct LgActionRange *)self;
	LgActionStop(range->action);
}

static void rangeReset(LgActionP self) {
	struct LgActionRange *range = (struct LgActionRange *)self;
	LgActionReset(range->action);
	intervalReset(self);
}

static void rangeAttachNode(LgActionP self, LgNodeP node) {
	struct LgActionRange *range = (struct LgActionRange *)self;
	LgActionAttachNode(range->action, node);
	actionAttachNode(self, node);
}

static struct LgIntervalImpl rangeImpl = {
	{
		rangeDestroy,
		intervalUpdate,
		rangeReset,
		rangeStop,
		rangeAttachNode,
		rangeClone,
	},
	rangeStep,
};

LgActionP LgActionRangeCreate(LgActionP action, float start, float end) {
	struct LgActionRange *rangeact = (struct LgActionRange *)createAction(sizeof(struct LgActionRange));
	((LgActionP)rangeact)->impl = (struct LgActionImpl *)&rangeImpl;
	DURATION(rangeact) = ((LgActionIntervalP)action)->duration * (end - start);
	rangeact->action = action;
	rangeact->start = start;
	rangeact->end = end;
	LgActionRetain(action);
	return (LgActionP)rangeact;
}

// spawn ---------------------------------------------------------------------------------------------------------------------
struct LgActionSpawn {
	struct LgActionInterval	base;
	LgArrayP				actions;
};

static void spawnDestroy(LgActionP action) {
	struct LgActionSpawn *spawn = (struct LgActionSpawn *)action;
	LgArrayDestroy(spawn->actions);
	intervalDestroy(action);
}

static LgActionP spawnClone(LgActionP self) {
	struct LgActionSpawn *spawn = (struct LgActionSpawn *)self;
	return LgActionSpawnCreate((LgActionP *)LgArrayData(spawn->actions), LgArrayLen(spawn->actions));
}

static LgBool spawnUpdate(LgActionP self, LgTime delta) {
	struct LgActionSpawn *spawn = (struct LgActionSpawn *)self;
	float ratio;
	int i;

	spawn->base.time += delta;
	ratio = (float)(spawn->base.time / DURATION(spawn));
	if (ratio >= 1) {
		for (i = 0; i < LgArrayLen(spawn->actions); i++)
			LgActionUpdate((LgActionP)LgArrayGet(spawn->actions, i), delta);
		return LgFalse;
	} else {
		for (i = 0; i < LgArrayLen(spawn->actions); i++)
			LgActionUpdate((LgActionP)LgArrayGet(spawn->actions, i), delta);
		return LgTrue;
	}
}

static void spawnReset(LgActionP self) {
	struct LgActionSpawn *spawn = (struct LgActionSpawn *)self;
	int i;
	for (i = 0; i < LgArrayLen(spawn->actions); i++)
		LgActionReset((LgActionP)LgArrayGet(spawn->actions, i));
	intervalReset(self);
}

static void spawnStop(LgActionP self) {
	struct LgActionSpawn *spawn = (struct LgActionSpawn *)self;
	int i;
	for (i = 0; i < LgArrayLen(spawn->actions); i++)
		LgActionStop((LgActionP)LgArrayGet(spawn->actions, i));
}

static void spawnAttachNode(LgActionP self, LgNodeP node) {
	struct LgActionSpawn *spawn = (struct LgActionSpawn *)self;
	int i;
	for (i = 0; i < LgArrayLen(spawn->actions); i++)
		LgActionAttachNode((LgActionP)LgArrayGet(spawn->actions, i), node);
	actionAttachNode(self, node);
}

static struct LgIntervalImpl spawnImpl = {
	{
		spawnDestroy,
		spawnUpdate,
		spawnReset,
		spawnStop,
		spawnAttachNode,
		spawnClone,
	},
	NULL,
};

LgActionP LgActionSpawnCreate(LgActionP *actions, int count) {
	struct LgActionSpawn *spawn = (struct LgActionSpawn *)createAction(sizeof(struct LgActionSpawn));
	int i;
	LgTime maxDuration = 0;

	((LgActionP)spawn)->impl = (struct LgActionImpl *)&spawnImpl;
	spawn->actions = LgArrayCreate((LgDestroy)LgActionRelease);

	for (i = 0; i < count; i++)
		maxDuration = LgMax(maxDuration, ((struct LgActionInterval *)actions[i])->duration);

	for (i = 0; i < count; i++) {
		LgActionP child = actions[i];
		LgTime delay = maxDuration - ((struct LgActionInterval *)child)->duration;
		LgActionP seq;

		if (delay > 0) {
			LgActionP seqActions[2];
			seqActions[0] = LgActionDelayCreate(delay);
			seqActions[1] = child;
			seq = LgActionSequenceCreate(seqActions, 2);
			LgActionRelease(seqActions[0]);
		} else {
			seq = LgActionSequenceCreate(&child, 1);
		}
		
		LgArrayAppend(spawn->actions, seq);
	}

	DURATION(spawn) = maxDuration;
	return (LgActionP)spawn;
}

// ease ---------------------------------------------------------------------------------------------------------------------
struct LgActionEase {
	struct LgActionInterval	base;
	LgActionP				action;
};

#define EASE_SUB_ACTION(eaction) (((struct LgActionEase *)eaction)->action)

#define EASE_SET_SUB_ACTION(eaction, sub_action) ((struct LgActionEase *)eaction)->action = LgActionRetain((LgActionP)sub_action)

static void easeDestroy(LgActionP action) {
	struct LgActionEase *ease = (struct LgActionEase *)action;
	LgActionRelease(ease->action);
	intervalDestroy(action);
}

static void easeAttachNode(LgActionP self, LgNodeP node) {
	struct LgActionEase *ease = (struct LgActionEase *)self;
	LgActionAttachNode(ease->action, node);
	actionAttachNode(self, node);
}

static void easeReset(LgActionP self) {
	struct LgActionEase *ease = (struct LgActionEase *)self;
	LgActionReset(ease->action);
	intervalReset(self);
}

static void easeStop(LgActionP self) {
	struct LgActionEase *ease = (struct LgActionEase *)self;
	LgActionStop(ease->action);
}

// easein ---------------------------------------------------------------------------------------------------------------------
struct LgActiondzEaseIn {
	struct LgActionEase	base;
	float				rate;
};

static LgActionP easeinClone(LgActionP self) {
	struct LgActiondzEaseIn *easein = (struct LgActiondzEaseIn *)self;
	return LgActionEaseInCreate(EASE_SUB_ACTION(easein), easein->rate);
}

static void easeinStep(LgActionP self, float ratio) {
	struct LgActiondzEaseIn *easein = (struct LgActiondzEaseIn *)self;
	LgActionIntervalStep(EASE_SUB_ACTION(easein), powf(ratio, easein->rate));
}

static struct LgIntervalImpl easeinImpl = {
	{
		easeDestroy,
		intervalUpdate,
		easeReset,
		easeStop,
		easeAttachNode,
		easeinClone,
	},
	easeinStep,
};

LgActionP LgActionEaseInCreate(LgActionP action, float rate) {
	struct LgActiondzEaseIn *easein = (struct LgActiondzEaseIn *)createAction(sizeof(struct LgActiondzEaseIn));
	((LgActionP)easein)->impl = (struct LgActionImpl *)&easeinImpl;
	DURATION(easein) = ((LgActionIntervalP)action)->duration;
	EASE_SET_SUB_ACTION(easein, action);
	easein->rate = rate;
	return (LgActionP)easein;
}

// easeout ---------------------------------------------------------------------------------------------------------------------
struct LgActiondzEaseOut {
	struct LgActionEase	base;
	float				rate;
};

static LgActionP easeoutClone(LgActionP self) {
	struct LgActiondzEaseOut *easeout = (struct LgActiondzEaseOut *)self;
	return LgActionEaseOutCreate(EASE_SUB_ACTION(easeout), easeout->rate);
}

static void easeoutStep(LgActionP self, float ratio) {
	struct LgActiondzEaseOut *easeout = (struct LgActiondzEaseOut *)self;
	LgActionIntervalStep(EASE_SUB_ACTION(easeout), powf(ratio, 1.0f / easeout->rate));
}

static struct LgIntervalImpl easeoutImpl = {
	{
		easeDestroy,
		intervalUpdate,
		easeReset,
		easeStop,
		easeAttachNode,
		easeoutClone,
	},
	easeoutStep,
};

LgActionP LgActionEaseOutCreate(LgActionP action, float rate) {
	struct LgActiondzEaseOut *easeout = (struct LgActiondzEaseOut *)createAction(sizeof(struct LgActiondzEaseOut));
	((LgActionP)easeout)->impl = (struct LgActionImpl *)&easeoutImpl;
	DURATION(easeout) = ((LgActionIntervalP)action)->duration;
	EASE_SET_SUB_ACTION(easeout, action);
	easeout->rate = rate;
	return (LgActionP)easeout;
}

// easeinout ---------------------------------------------------------------------------------------------------------------------
struct LgActiondzEaseInOut {
	struct LgActionEase	base;
	float				rate;
};

static LgActionP easeinoutClone(LgActionP self) {
	struct LgActiondzEaseInOut *easeinout = (struct LgActiondzEaseInOut *)self;
	return LgActionEaseInOutCreate(EASE_SUB_ACTION(easeinout), easeinout->rate);
}

static void easeinoutStep(LgActionP self, float ratio) {
	struct LgActiondzEaseInOut *easeinout = (struct LgActiondzEaseInOut *)self;
	float cratio;

	ratio *= 2;
	if (ratio < 1) {
		cratio = 0.5f * powf(ratio, easeinout->rate);
	} else {
		cratio = 1.0f - 0.5f * powf(2.0f - ratio, easeinout->rate);
	}

	LgActionIntervalStep(EASE_SUB_ACTION(easeinout), cratio);
}

static struct LgIntervalImpl easeinoutImpl = {
	{
		easeDestroy,
		intervalUpdate,
		easeReset,
		easeStop,
		easeAttachNode,
		easeinoutClone,
	},
	easeinoutStep,
};

LgActionP LgActionEaseInOutCreate(LgActionP action, float rate) {
	struct LgActiondzEaseInOut *easeinout = (struct LgActiondzEaseInOut *)createAction(sizeof(struct LgActiondzEaseInOut));
	((LgActionP)easeinout)->impl = (struct LgActionImpl *)&easeinoutImpl;
	DURATION(easeinout) = ((LgActionIntervalP)action)->duration;
	EASE_SET_SUB_ACTION(easeinout, action);
	easeinout->rate = rate;
	return (LgActionP)easeinout;
}

// easeexpin ---------------------------------------------------------------------------------------------------------------------
struct LgActionEaseExponentialIn {
	struct LgActionEase	base;
	float				rate;
};

static LgActionP easeexpinClone(LgActionP self) {
	struct LgActionEaseExponentialIn *easeexpin = (struct LgActionEaseExponentialIn *)self;
	return LgActionEaseExponentialInCreate(EASE_SUB_ACTION(easeexpin), easeexpin->rate);
}

static void easeexpinStep(LgActionP self, float ratio) {
	struct LgActionEaseExponentialIn *easeexpin = (struct LgActionEaseExponentialIn *)self;
	LgActionIntervalStep(EASE_SUB_ACTION(easeexpin), ratio == 0 ? 0 : powf(2, 10 * (ratio / 1 - 1)) - 1 * 0.001f);
}

static struct LgIntervalImpl easeexpinImpl = {
	{
		easeDestroy,
		intervalUpdate,
		easeReset,
		easeStop,
		easeAttachNode,
		easeexpinClone,
	},
	easeexpinStep,
};

LgActionP LgActionEaseExponentialInCreate(LgActionP action, float rate) {
	struct LgActionEaseExponentialIn *easeexpin = (struct LgActionEaseExponentialIn *)createAction(sizeof(struct LgActionEaseExponentialIn));
	((LgActionP)easeexpin)->impl = (struct LgActionImpl *)&easeexpinImpl;
	DURATION(easeexpin) = ((LgActionIntervalP)action)->duration;
	EASE_SET_SUB_ACTION(easeexpin, action);
	easeexpin->rate = rate;
	return (LgActionP)easeexpin;
}

// easeexpout ---------------------------------------------------------------------------------------------------------------------
struct LgActionEaseExponentialOut {
	struct LgActionEase	base;
	float				rate;
};

static LgActionP easeexpoutClone(LgActionP self) {
	struct LgActionEaseExponentialOut *easeexpout = (struct LgActionEaseExponentialOut *)self;
	return LgActionEaseExponentialOutCreate(EASE_SUB_ACTION(easeexpout), easeexpout->rate);
}

static void easeexpoutStep(LgActionP self, float ratio) {
	struct LgActionEaseExponentialOut *easeexpout = (struct LgActionEaseExponentialOut *)self;
	LgActionIntervalStep(EASE_SUB_ACTION(easeexpout), ratio == 1 ? 1 : (-powf(2, -10 * ratio / 1) + 1));
}

static struct LgIntervalImpl easeexpoutImpl = {
	{
		easeDestroy,
		intervalUpdate,
		easeReset,
		easeStop,
		easeAttachNode,
		easeexpoutClone,
	},
	easeexpoutStep,
};

LgActionP LgActionEaseExponentialOutCreate(LgActionP action, float rate) {
	struct LgActionEaseExponentialOut *easeexpout = (struct LgActionEaseExponentialOut *)createAction(sizeof(struct LgActionEaseExponentialOut));
	((LgActionP)easeexpout)->impl = (struct LgActionImpl *)&easeexpoutImpl;
	DURATION(easeexpout) = ((LgActionIntervalP)action)->duration;
	EASE_SET_SUB_ACTION(easeexpout, action);
	easeexpout->rate = rate;
	return (LgActionP)easeexpout;
}

// easeexpinout ---------------------------------------------------------------------------------------------------------------------
struct LgActionEaseExponentialInOut {
	struct LgActionEase	base;
	float				rate;
};

static LgActionP easeexpinoutClone(LgActionP self) {
	struct LgActionEaseExponentialInOut *easeexpinout = (struct LgActionEaseExponentialInOut *)self;
	return LgActionEaseExponentialInOutCreate(EASE_SUB_ACTION(easeexpinout), easeexpinout->rate);
}

static void easeexpinoutStep(LgActionP self, float ratio) {
	struct LgActionEaseExponentialInOut *easeexpinout = (struct LgActionEaseExponentialInOut *)self;

	ratio /= 0.5f;
	if (ratio < 1) {
		ratio = 0.5f * powf(2, 10 * (ratio - 1));
	}
	else {
		ratio = 0.5f * (-powf(2, -10 * (ratio - 1)) + 2);
	}

	LgActionIntervalStep(EASE_SUB_ACTION(easeexpinout), ratio);
}

static struct LgIntervalImpl easeexpinoutImpl = {
	{
		easeDestroy,
		intervalUpdate,
		easeReset,
		easeStop,
		easeAttachNode,
		easeexpinoutClone,
	},
	easeexpinoutStep,
};

LgActionP LgActionEaseExponentialInOutCreate(LgActionP action, float rate) {
	struct LgActionEaseExponentialInOut *easeexpinout = (struct LgActionEaseExponentialInOut *)createAction(sizeof(struct LgActionEaseExponentialInOut));
	((LgActionP)easeexpinout)->impl = (struct LgActionImpl *)&easeexpinoutImpl;
	DURATION(easeexpinout) = ((LgActionIntervalP)action)->duration;
	EASE_SET_SUB_ACTION(easeexpinout, action);
	easeexpinout->rate = rate;
	return (LgActionP)easeexpinout;
}

// easesinein ---------------------------------------------------------------------------------------------------------------------
struct LgActionEaseSineIn {
	struct LgActionEase	base;
	float				rate;
};

static LgActionP easesineinClone(LgActionP self) {
	struct LgActionEaseSineIn *easesinein = (struct LgActionEaseSineIn *)self;
	return LgActionEaseSineInCreate(EASE_SUB_ACTION(easesinein), easesinein->rate);
}

static void easesineinStep(LgActionP self, float ratio) {
	struct LgActionEaseSineIn *easesinein = (struct LgActionEaseSineIn *)self;
	LgActionIntervalStep(EASE_SUB_ACTION(easesinein), -1 * cosf(ratio * (float)M_PI_2) + 1);
}

static struct LgIntervalImpl easesineinImpl = {
	{
		easeDestroy,
		intervalUpdate,
		easeReset,
		easeStop,
		easeAttachNode,
		easesineinClone,
	},
	easesineinStep,
};

LgActionP LgActionEaseSineInCreate(LgActionP action, float rate) {
	struct LgActionEaseSineIn *easesinein = (struct LgActionEaseSineIn *)createAction(sizeof(struct LgActionEaseSineIn));
	((LgActionP)easesinein)->impl = (struct LgActionImpl *)&easesineinImpl;
	DURATION(easesinein) = ((LgActionIntervalP)action)->duration;
	EASE_SET_SUB_ACTION(easesinein, action);
	easesinein->rate = rate;
	return (LgActionP)easesinein;
}

// easesineout ---------------------------------------------------------------------------------------------------------------------
struct LgActionEaseSineOut {
	struct LgActionEase	base;
	float				rate;
};

static LgActionP easesineoutClone(LgActionP self) {
	struct LgActionEaseSineOut *easesineout = (struct LgActionEaseSineOut *)self;
	return LgActionEaseSineOutCreate(EASE_SUB_ACTION(easesineout), easesineout->rate);
}

static void easesineoutStep(LgActionP self, float ratio) {
	struct LgActionEaseSineOut *easesineout = (struct LgActionEaseSineOut *)self;
	LgActionIntervalStep(EASE_SUB_ACTION(easesineout), sinf(ratio * (float)M_PI_2));
}

static struct LgIntervalImpl easesineoutImpl = {
	{
		easeDestroy,
		intervalUpdate,
		easeReset,
		easeStop,
		easeAttachNode,
		easesineoutClone,
	},
	easesineoutStep,
};

LgActionP LgActionEaseSineOutCreate(LgActionP action, float rate) {
	struct LgActionEaseSineOut *easesineout = (struct LgActionEaseSineOut *)createAction(sizeof(struct LgActionEaseSineOut));
	((LgActionP)easesineout)->impl = (struct LgActionImpl *)&easesineoutImpl;
	DURATION(easesineout) = ((LgActionIntervalP)action)->duration;
	EASE_SET_SUB_ACTION(easesineout, action);
	easesineout->rate = rate;
	return (LgActionP)easesineout;
}

// easesineinout ---------------------------------------------------------------------------------------------------------------------
struct LgActionEaseSineInOut {
	struct LgActionEase	base;
	float				rate;
};

static LgActionP easesineinoutClone(LgActionP self) {
	struct LgActionEaseSineInOut *easesineinout = (struct LgActionEaseSineInOut *)self;
	return LgActionEaseSineInOutCreate(EASE_SUB_ACTION(easesineinout), easesineinout->rate);
}

static void easesineinoutStep(LgActionP self, float ratio) {
	struct LgActionEaseSineInOut *easeexpinout = (struct LgActionEaseSineInOut *)self;
	LgActionIntervalStep(EASE_SUB_ACTION(easeexpinout), -0.5f * (cosf((float)M_PI * ratio) - 1));
}

static struct LgIntervalImpl easesineinoutImpl = {
	{
		easeDestroy,
		intervalUpdate,
		easeReset,
		easeStop,
		easeAttachNode,
		easesineinoutClone,
	},
	easesineinoutStep,
};

LgActionP LgActionEaseSineInOutCreate(LgActionP action, float rate) {
	struct LgActionEaseSineInOut *easesineinout = (struct LgActionEaseSineInOut *)createAction(sizeof(struct LgActionEaseSineInOut));
	((LgActionP)easesineinout)->impl = (struct LgActionImpl *)&easesineinoutImpl;
	DURATION(easesineinout) = ((LgActionIntervalP)action)->duration;
	EASE_SET_SUB_ACTION(easesineinout, action);
	easesineinout->rate = rate;
	return (LgActionP)easesineinout;
}

// easeelasticin ---------------------------------------------------------------------------------------------------------------------
struct LgActionEaseElasticIn {
	struct LgActionEase	base;
	float				period;
};

static LgActionP easeelasticinClone(LgActionP self) {
	struct LgActionEaseElasticIn *easeelasticin = (struct LgActionEaseElasticIn *)self;
	return LgActionEaseElasticInCreate(EASE_SUB_ACTION(easeelasticin), easeelasticin->period);
}

static void easeelasticinStep(LgActionP self, float ratio) {
	struct LgActionEaseElasticIn *easeelasticin = (struct LgActionEaseElasticIn *)self;

	float newT = 0;
	if (ratio == 0 || ratio == 1) {
		newT = ratio;
	} else {
		float s = easeelasticin->period / 4;
		ratio = ratio - 1;
		newT = -powf(2, 10 * ratio) * sinf((ratio - s) * M_PI_X_2 / easeelasticin->period);
	}

	LgActionIntervalStep(EASE_SUB_ACTION(easeelasticin), newT);
}

static struct LgIntervalImpl easeelasticinImpl = {
	{
		easeDestroy,
		intervalUpdate,
		easeReset,
		easeStop,
		easeAttachNode,
		easeelasticinClone,
	},
	easeelasticinStep,
};

LgActionP LgActionEaseElasticInCreate(LgActionP action, float period) {
	struct LgActionEaseElasticIn *easeelasticin = (struct LgActionEaseElasticIn *)createAction(sizeof(struct LgActionEaseElasticIn));
	((LgActionP)easeelasticin)->impl = (struct LgActionImpl *)&easeelasticinImpl;
	DURATION(easeelasticin) = ((LgActionIntervalP)action)->duration;
	EASE_SET_SUB_ACTION(easeelasticin, action);
	easeelasticin->period = period;
	return (LgActionP)easeelasticin;
}

// easeelasticout ---------------------------------------------------------------------------------------------------------------------
struct LgActionEaseElasticOut {
	struct LgActionEase	base;
	float				period;
};

static LgActionP easeelasticoutClone(LgActionP self) {
	struct LgActionEaseElasticOut *easeelasticout = (struct LgActionEaseElasticOut *)self;
	return LgActionEaseElasticOutCreate(EASE_SUB_ACTION(easeelasticout), easeelasticout->period);
}

static void easeelasticoutStep(LgActionP self, float ratio) {
	struct LgActionEaseElasticOut *easeelasticout = (struct LgActionEaseElasticOut *)self;

	float newT = 0;
	if (ratio == 0 || ratio == 1) {
		newT = ratio;
	} else {
		float s = easeelasticout->period / 4;
		newT = powf(2, -10 * ratio) * sinf((ratio - s) * M_PI_X_2 / easeelasticout->period) + 1;
	}

	LgActionIntervalStep(EASE_SUB_ACTION(easeelasticout), newT);
}

static struct LgIntervalImpl easeelasticoutImpl = {
	{
		easeDestroy,
		intervalUpdate,
		easeReset,
		easeStop,
		easeAttachNode,
		easeelasticoutClone,
	},
	easeelasticoutStep,
};

LgActionP LgActionEaseElasticOutCreate(LgActionP action, float period) {
	struct LgActionEaseElasticOut *easeelasticout = (struct LgActionEaseElasticOut *)createAction(sizeof(struct LgActionEaseElasticOut));
	((LgActionP)easeelasticout)->impl = (struct LgActionImpl *)&easeelasticoutImpl;
	DURATION(easeelasticout) = ((LgActionIntervalP)action)->duration;
	EASE_SET_SUB_ACTION(easeelasticout, action);
	easeelasticout->period = period;
	return (LgActionP)easeelasticout;
}

// easeelastinout ---------------------------------------------------------------------------------------------------------------------
struct LgActionEaseElasticInOut {
	struct LgActionEase	base;
	float				period;
};

static LgActionP easeelasticinoutClone(LgActionP self) {
	struct LgActionEaseElasticInOut *easeelastinout = (struct LgActionEaseElasticInOut *)self;
	return LgActionEaseElasticOutCreate(EASE_SUB_ACTION(easeelastinout), easeelastinout->period);
}

static void easeelasticinoutStep(LgActionP self, float ratio) {
	struct LgActionEaseElasticInOut *easeelastinout = (struct LgActionEaseElasticInOut *)self;

	float newT = 0;

	if (ratio == 0 || ratio == 1) {
		newT = ratio;
	} else {
		float s;

		ratio = ratio * 2;
		s = easeelastinout->period / 4;
		ratio = ratio - 1;

		if (ratio < 0) {
			newT = -0.5f * powf(2, 10 * ratio) * sinf((ratio -s) * M_PI_X_2 / easeelastinout->period);
		} else {
			newT = powf(2, -10 * ratio) * sinf((ratio - s) * M_PI_X_2 / easeelastinout->period) * 0.5f + 1;
		}
	}


	LgActionIntervalStep(EASE_SUB_ACTION(easeelastinout), newT);
}

static struct LgIntervalImpl easeelasticinoutImpl = {
	{
		easeDestroy,
		intervalUpdate,
		easeReset,
		easeStop,
		easeAttachNode,
		easeelasticinoutClone,
	},
	easeelasticinoutStep,
};

LgActionP LgActionEaseElasticInOutCreate(LgActionP action, float period) {
	struct LgActionEaseElasticInOut *easeelasticinout = (struct LgActionEaseElasticInOut *)createAction(sizeof(struct LgActionEaseElasticInOut));
	((LgActionP)easeelasticinout)->impl = (struct LgActionImpl *)&easeelasticinoutImpl;
	DURATION(easeelasticinout) = ((LgActionIntervalP)action)->duration;
	EASE_SET_SUB_ACTION(easeelasticinout, action);
	easeelasticinout->period = period;
	return (LgActionP)easeelasticinout;
}

// easebouncein ---------------------------------------------------------------------------------------------------------------------
struct LgActionEaseBounceIn {
	struct LgActionEase	base;
};

static float bounceTime(float time) {
	if (time < 1 / 2.75) {
		return 7.5625f * time * time;
	} else if (time < 2 / 2.75) {
		time -= 1.5f / 2.75f;
		return 7.5625f * time * time + 0.75f;
	} else if(time < 2.5 / 2.75) {
		time -= 2.25f / 2.75f;
		return 7.5625f * time * time + 0.9375f;
	}

	time -= 2.625f / 2.75f;
	return 7.5625f * time * time + 0.984375f;
}

static LgActionP easebounceinClone(LgActionP self) {
	struct LgActionEaseBounceIn *easebouncein = (struct LgActionEaseBounceIn *)self;
	return LgActionEaseBounceInCreate(EASE_SUB_ACTION(easebouncein));
}

static void easebounceinStep(LgActionP self, float ratio) {
	struct LgActionEaseBounceIn *easebouncein = (struct LgActionEaseBounceIn *)self;
	LgActionIntervalStep(EASE_SUB_ACTION(easebouncein), 1 - bounceTime(1 - ratio));
}

static struct LgIntervalImpl easebounceinImpl = {
	{
		easeDestroy,
		intervalUpdate,
		easeReset,
		easeStop,
		easeAttachNode,
		easebounceinClone,
	},
	easebounceinStep,
};

LgActionP LgActionEaseBounceInCreate(LgActionP action) {
	struct LgActionEaseBounceIn *easebouncein = (struct LgActionEaseBounceIn *)createAction(sizeof(struct LgActionEaseBounceIn));
	((LgActionP)easebouncein)->impl = (struct LgActionImpl *)&easebounceinImpl;
	DURATION(easebouncein) = ((LgActionIntervalP)action)->duration;
	EASE_SET_SUB_ACTION(easebouncein, action);
	return (LgActionP)easebouncein;
}

// easebounceout ---------------------------------------------------------------------------------------------------------------------
struct LgActionEaseBounceOut {
	struct LgActionEase	base;
};

static LgActionP easebounceoutClone(LgActionP self) {
	struct LgActionEaseBounceOut *easebounceout = (struct LgActionEaseBounceOut *)self;
	return LgActionEaseBounceOutCreate(EASE_SUB_ACTION(easebounceout));
}

static void easebounceoutStep(LgActionP self, float ratio) {
	struct LgActionEaseBounceOut *easebounceout = (struct LgActionEaseBounceOut *)self;
	LgActionIntervalStep(EASE_SUB_ACTION(easebounceout), bounceTime(ratio));
}

static struct LgIntervalImpl easebounceoutImpl = {
	{
		easeDestroy,
		intervalUpdate,
		easeReset,
		easeStop,
		easeAttachNode,
		easebounceoutClone,
	},
	easebounceoutStep,
};

LgActionP LgActionEaseBounceOutCreate(LgActionP action) {
	struct LgActionEaseBounceOut *easebounceout = (struct LgActionEaseBounceOut *)createAction(sizeof(struct LgActionEaseBounceOut));
	((LgActionP)easebounceout)->impl = (struct LgActionImpl *)&easebounceoutImpl;
	DURATION(easebounceout) = ((LgActionIntervalP)action)->duration;
	EASE_SET_SUB_ACTION(easebounceout, action);
	return (LgActionP)easebounceout;
}

// easebounceinout ---------------------------------------------------------------------------------------------------------------------
struct LgActionEaseBounceInOut {
	struct LgActionEase	base;
};

static LgActionP easebounceinoutClone(LgActionP self) {
	struct LgActionEaseBounceInOut *easebounceinout = (struct LgActionEaseBounceInOut *)self;
	return LgActionEaseBounceInOutCreate(EASE_SUB_ACTION(easebounceinout));
}

static void easebounceinoutStep(LgActionP self, float ratio) {
	struct LgActionEaseBounceInOut *easebounceinout = (struct LgActionEaseBounceInOut *)self;
	float newT = 0;

	if (ratio < 0.5f) {
		ratio = ratio * 2;
		newT = (1 - bounceTime(1 - ratio)) * 0.5f;
	} else {
		newT = bounceTime(ratio * 2 - 1) * 0.5f + 0.5f;
	}
	LgActionIntervalStep(EASE_SUB_ACTION(easebounceinout), newT);
}

static struct LgIntervalImpl easebounceinoutImpl = {
	{
		easeDestroy,
		intervalUpdate,
		easeReset,
		easeStop,
		easeAttachNode,
		easebounceinoutClone,
	},
	easebounceinoutStep,
};

LgActionP LgActionEaseBounceInOutCreate(LgActionP action) {
	struct LgActionEaseBounceInOut *easebounceinout = (struct LgActionEaseBounceInOut *)createAction(sizeof(struct LgActionEaseBounceInOut));
	((LgActionP)easebounceinout)->impl = (struct LgActionImpl *)&easebounceinoutImpl;
	DURATION(easebounceinout) = ((LgActionIntervalP)action)->duration;
	EASE_SET_SUB_ACTION(easebounceinout, action);
	return (LgActionP)easebounceinout;
}

// easebackin ---------------------------------------------------------------------------------------------------------------------
struct LgActionEaseBackIn {
	struct LgActionEase	base;
};

static LgActionP easebackinClone(LgActionP self) {
	struct LgActionEaseBackIn *easebackin = (struct LgActionEaseBackIn *)self;
	return LgActionEaseBackInCreate(EASE_SUB_ACTION(easebackin));
}

static void easebackinStep(LgActionP self, float ratio) {
	struct LgActionEaseBackIn *easebackin = (struct LgActionEaseBackIn *)self;
	const float overshoot = 1.70158f;
	LgActionIntervalStep(EASE_SUB_ACTION(easebackin), ratio * ratio * ((overshoot + 1) * ratio - overshoot));
}

static struct LgIntervalImpl easebackinImpl = {
	{
		easeDestroy,
		intervalUpdate,
		easeReset,
		easeStop,
		easeAttachNode,
		easebackinClone,
	},
	easebackinStep,
};

LgActionP LgActionEaseBackInCreate(LgActionP action) {
	struct LgActionEaseBackIn *easebackin = (struct LgActionEaseBackIn *)createAction(sizeof(struct LgActionEaseBackIn));
	((LgActionP)easebackin)->impl = (struct LgActionImpl *)&easebackinImpl;
	DURATION(easebackin) = ((LgActionIntervalP)action)->duration;
	EASE_SET_SUB_ACTION(easebackin, action);
	return (LgActionP)easebackin;
}

// easebackout ---------------------------------------------------------------------------------------------------------------------
struct LgActionEaseBackOut {
	struct LgActionEase	base;
};

static LgActionP easebackoutClone(LgActionP self) {
	struct LgActionEaseBackOut *easebackout = (struct LgActionEaseBackOut *)self;
	return LgActionEaseBackOutCreate(EASE_SUB_ACTION(easebackout));
}

static void easebackoutStep(LgActionP self, float ratio) {
	struct LgActionEaseBackOut *easebackout = (struct LgActionEaseBackOut *)self;
	const float overshoot = 1.70158f;
	ratio = ratio - 1;
	LgActionIntervalStep(EASE_SUB_ACTION(easebackout), ratio * ratio * ((overshoot + 1) * ratio + overshoot) + 1);
}

static struct LgIntervalImpl easebackoutImpl = {
	{
		easeDestroy,
		intervalUpdate,
		easeReset,
		easeStop,
		easeAttachNode,
		easebackoutClone,
	},
	easebackoutStep,
};

LgActionP LgActionEaseBackOutCreate(LgActionP action) {
	struct LgActionEaseBackOut *easebackout = (struct LgActionEaseBackOut *)createAction(sizeof(struct LgActionEaseBackOut));
	((LgActionP)easebackout)->impl = (struct LgActionImpl *)&easebackoutImpl;
	DURATION(easebackout) = ((LgActionIntervalP)action)->duration;
	EASE_SET_SUB_ACTION(easebackout, action);
	return (LgActionP)easebackout;
}

// easebackinout ---------------------------------------------------------------------------------------------------------------------
struct LgActionEaseBackInOut {
	struct LgActionEase	base;
};

static LgActionP easebackinoutClone(LgActionP self) {
	struct LgActionEaseBackInOut *easebackinout = (struct LgActionEaseBackInOut *)self;
	return LgActionEaseBackInOutCreate(EASE_SUB_ACTION(easebackinout));
}

static void easebackinoutStep(LgActionP self, float ratio) {
	struct LgActionEaseBackInOut *easebackinout = (struct LgActionEaseBackInOut *)self;
	const float overshoot = 1.70158f * 1.525f;

	ratio = ratio * 2;
	if (ratio < 1) {
		LgActionIntervalStep(EASE_SUB_ACTION(easebackinout), (ratio * ratio * ((overshoot + 1) * ratio - overshoot)) / 2);
	}
	else {
		ratio = ratio - 2;
		LgActionIntervalStep(EASE_SUB_ACTION(easebackinout), (ratio * ratio * ((overshoot + 1) * ratio + overshoot)) / 2 + 1);
	}
}

static struct LgIntervalImpl easebackinoutImpl = {
	{
		easeDestroy,
		intervalUpdate,
		easeReset,
		easeStop,
		easeAttachNode,
		easebackinoutClone,
	},
	easebackinoutStep,
};

LgActionP LgActionEaseBackInOutCreate(LgActionP action) {
	struct LgActionEaseBounceInOut *easebounceinout = (struct LgActionEaseBounceInOut *)createAction(sizeof(struct LgActionEaseBounceInOut));
	((LgActionP)easebounceinout)->impl = (struct LgActionImpl *)&easebackinoutImpl;
	DURATION(easebounceinout) = ((LgActionIntervalP)action)->duration;
	EASE_SET_SUB_ACTION(easebounceinout, action);
	return (LgActionP)easebounceinout;
}

// grid3d ---------------------------------------------------------------------------------------------------------------------
struct LgActionGrid {
	struct LgActionInterval	base;
	LgBool					tileGrid;
	LgGridP					grid;
	int						cols, rows;
};

#define GRID(action) (((struct LgActionGrid *)action)->grid)
#define GRID_IS_TILE(action) (((struct LgActionGrid *)action)->tileGrid)
#define GRID_COLS(action) (((struct LgActionGrid *)action)->cols)
#define GRID_ROWS(action) (((struct LgActionGrid *)action)->rows)
#define IS_VALID_GRID(action) (GRID(action) && LgNodeGrid(LgActionNode(action)) == GRID(action))

static void grid3dActionReset(LgActionP self) {
	if (!LgNodeGrid(LgActionNode(self))) {
		if (!GRID_IS_TILE(self)) GRID(self) = LgGrid3DCreate(GRID_COLS(self), GRID_ROWS(self));
		else GRID(self) = LgTileGrid3DCreate(GRID_COLS(self), GRID_ROWS(self));
		LgNodeSetGrid(LgActionNode(self), GRID(self));
	} else {
		GRID(self) = NULL;
	}

	intervalReset(self);
}

static void grid3dStop(LgActionP action) {
	if (IS_VALID_GRID(action)) {
		LgNodeSetGrid(LgActionNode(action), NULL);
		GRID(action) = NULL;
	}
}

// wave3d ---------------------------------------------------------------------------------------------------------------------
struct LgActionWave3D {
	struct LgActionGrid	base;
	int					waves;
	float				amplitude;
};

static LgActionP wave3dClone(LgActionP self) {
	struct LgActionWave3D *wave3d = (struct LgActionWave3D *)self;
	return LgActionWave3DCreate(DURATION(wave3d), GRID_COLS(wave3d), GRID_ROWS(wave3d), wave3d->waves, wave3d->amplitude);
}

static void wave3dStep(LgActionP self, float ratio) {
	struct LgActionWave3D *wave3d = (struct LgActionWave3D *)self;

	if (IS_VALID_GRID(self)) {
		int i, j;
		struct LgSize gridsize;

		LgGridSize(GRID(self), &gridsize);

		for (i = 0; i <= gridsize.width; i++) {
			for (j = 0; j <= gridsize.height; j++) {
				struct LgVector3 vertex;
				LgGrid3DOrgPosition(GRID(self), i, j, &vertex);
				vertex.z += sinf((float)(ratio * M_PI * wave3d->waves * 2) + (vertex.x + vertex.y) * 0.01f) * wave3d->amplitude;
				LgGrid3DSetPosition(GRID(self), i, j, &vertex);
			}
		}
	}
}

static struct LgIntervalImpl wave3dImpl = {
	{
		intervalDestroy,
		intervalUpdate,
		grid3dActionReset,
		grid3dStop,
		actionAttachNode,
		wave3dClone,
	},
	wave3dStep,
};

LgActionP LgActionWave3DCreate(LgTime duration, int cols, int rows, int waves, float amplitude) {
	struct LgActionWave3D *wave3d = (struct LgActionWave3D *)createAction(sizeof(struct LgActionWave3D));
	((LgActionP)wave3d)->impl = (struct LgActionImpl *)&wave3dImpl;
	DURATION(wave3d) = duration;
	GRID_COLS(wave3d) = cols, GRID_ROWS(wave3d) = rows;
	wave3d->waves = waves;
	wave3d->amplitude = amplitude;
	return (LgActionP)wave3d;
}

// lens3d ---------------------------------------------------------------------------------------------------------------------
struct LgActionLens3D {
	struct LgActionGrid	base;
	struct LgVector2	center;
	float				radius, lensEffect;
};

static LgActionP lens3dClone(LgActionP self) {
	struct LgActionLens3D *lens3d = (struct LgActionLens3D *)self;
	return LgActionLens3DCreate(DURATION(lens3d), GRID_COLS(lens3d), GRID_ROWS(lens3d), &lens3d->center, lens3d->radius, lens3d->lensEffect);
}

static void lens3dStep(LgActionP self, float ratio) {
	struct LgActionLens3D *lens3d = (struct LgActionLens3D *)self;

	if (IS_VALID_GRID(self)) {
		int i, j;
		struct LgSize gridsize;

		LgGridSize(GRID(self), &gridsize);

		for (i = 0; i <= gridsize.width; i++) {
			for (j = 0; j <= gridsize.height; j++) {
				struct LgVector3 vertex;
				struct LgVector2 p, vect;
				float r;

				LgGrid3DOrgPosition(GRID(self), i, j, &vertex);
				
				p.x = vertex.x, p.y = vertex.y;
				LgCopyMemory(&vect, &lens3d->center, sizeof(struct LgVector2));
				LgVector2Sub(&vect, &p);
				r = LgVector2Length(&vect);

				if (r < lens3d->radius) {
					float pre_log, l, new_r;
					r = lens3d->radius - r;
					pre_log = r / lens3d->radius;
					if (pre_log == 0) pre_log = 0.001f;
					l = logf(pre_log) * lens3d->lensEffect;
					new_r = expf(l) * lens3d->radius;
					LgVector2Normalize(&vect);
					LgVector2MulFloat(&vect, new_r);
					vertex.z += LgVector2Length(&vect) * lens3d->lensEffect;
				}

				LgGrid3DSetPosition(GRID(self), i, j, &vertex);
			}
		}
	}
}

static struct LgIntervalImpl lens3dImpl = {
	{
		intervalDestroy,
		intervalUpdate,
		grid3dActionReset,
		grid3dStop,
		actionAttachNode,
		lens3dClone,
	},
	lens3dStep,
};

LgActionP LgActionLens3DCreate(LgTime duration, int cols, int rows, const LgVector2P center, float radius, float lensEffect) {
	struct LgActionLens3D *lens3d = (struct LgActionLens3D *)createAction(sizeof(struct LgActionLens3D));
	((LgActionP)lens3d)->impl = (struct LgActionImpl *)&lens3dImpl;
	DURATION(lens3d) = duration;
	GRID_COLS(lens3d) = cols, GRID_ROWS(lens3d) = rows;
	LgCopyMemory(&lens3d->center, center, sizeof(struct LgVector2));
	lens3d->radius = radius;
	lens3d->lensEffect = lensEffect;
	return (LgActionP)lens3d;
}

// ripple3d ---------------------------------------------------------------------------------------------------------------------
struct LgActionRipple3D {
	struct LgActionGrid	base;
	int					waves;
	struct LgVector2	center;
	float				radius, amplitude;
};

static LgActionP ripple3dClone(LgActionP self) {
	struct LgActionRipple3D *ripple3d = (struct LgActionRipple3D *)self;
	return LgActionRipple3DCreate(DURATION(ripple3d), GRID_COLS(ripple3d), GRID_ROWS(ripple3d), 
		&ripple3d->center, ripple3d->radius, ripple3d->waves, ripple3d->amplitude);
}

static void ripple3dStep(LgActionP self, float ratio) {
	struct LgActionRipple3D *ripple3d = (struct LgActionRipple3D *)self;

	if (IS_VALID_GRID(self)) {
		int i, j;
		struct LgSize gridsize;

		LgGridSize(GRID(self), &gridsize);

		for (i = 0; i <= gridsize.width; i++) {
			for (j = 0; j <= gridsize.height; j++) {
				struct LgVector3 vertex;
				struct LgVector2 p, vect;
				float r;

				LgGrid3DOrgPosition(GRID(self), i, j, &vertex);

				p.x = vertex.x, p.y = vertex.y;
				LgCopyMemory(&vect, &ripple3d->center, sizeof(struct LgVector2));
				LgVector2Sub(&vect, &p);
				r = LgVector2Length(&vect);

				if (r < ripple3d->radius) {
					float rate;
					r = ripple3d->radius - r;
					rate = powf(r / ripple3d->radius, 2);
					vertex.z += (sinf((float)(ratio * M_PI * ripple3d->waves * 2) + r * 0.1f) * ripple3d->amplitude * rate);
				}

				LgGrid3DSetPosition(GRID(self), i, j, &vertex);
			}
		}
	}
}

static struct LgIntervalImpl ripple3dImpl = {
	{
		intervalDestroy,
		intervalUpdate,
		grid3dActionReset,
		grid3dStop,
		actionAttachNode,
		ripple3dClone,
	},
	ripple3dStep,
};

LgActionP LgActionRipple3DCreate(LgTime duration, int cols, int rows, const LgVector2P center, float radius, int waves, float amplitude) {
	struct LgActionRipple3D *ripple3d = (struct LgActionRipple3D *)createAction(sizeof(struct LgActionRipple3D));
	((LgActionP)ripple3d)->impl = (struct LgActionImpl *)&ripple3dImpl;
	DURATION(ripple3d) = duration;
	GRID_COLS(ripple3d) = cols, GRID_ROWS(ripple3d) = rows;
	LgCopyMemory(&ripple3d->center, center, sizeof(struct LgVector2));
	ripple3d->radius = radius;
	ripple3d->waves = waves;
	ripple3d->amplitude = amplitude;
	return (LgActionP)ripple3d;
}

// liquid3d ---------------------------------------------------------------------------------------------------------------------
struct LgActionLiquid3D {
	struct LgActionGrid	base;
	int					waves;
	float				amplitude;
};

static LgActionP liquid3dClone(LgActionP self) {
	struct LgActionLiquid3D *liquid3d = (struct LgActionLiquid3D *)self;
	return LgActionLiquid3DCreate(DURATION(liquid3d), GRID_COLS(liquid3d), GRID_ROWS(liquid3d), liquid3d->waves, liquid3d->amplitude);
}

static void liquid3dStep(LgActionP self, float ratio) {
	struct LgActionLiquid3D *liquid3d = (struct LgActionLiquid3D *)self;

	if (IS_VALID_GRID(self)) {
		int i, j;
		struct LgSize gridsize;

		LgGridSize(GRID(self), &gridsize);

		for (i = 0; i <= gridsize.width; i++) {
			for (j = 0; j <= gridsize.height; j++) {
				struct LgVector3 vertex;
				LgGrid3DOrgPosition(GRID(self), i, j, &vertex);
				vertex.x = (float)(vertex.x + (sin(ratio * M_PI * liquid3d->waves * 2 + vertex.x * 0.01f) * liquid3d->amplitude));
				vertex.y = (float)(vertex.y + (sin(ratio * M_PI * liquid3d->waves * 2 + vertex.y * 0.01f) * liquid3d->amplitude));
				LgGrid3DSetPosition(GRID(self), i, j, &vertex);
			}
		}
	}
}

static struct LgIntervalImpl liquid3dImpl = {
	{
		intervalDestroy,
		intervalUpdate,
		grid3dActionReset,
		grid3dStop,
		actionAttachNode,
		liquid3dClone,
	},
	liquid3dStep,
};

LgActionP LgActionLiquid3DCreate(LgTime duration, int cols, int rows, int waves, float amplitude) {
	struct LgActionLiquid3D *liquid3d = (struct LgActionLiquid3D *)createAction(sizeof(struct LgActionLiquid3D));
	((LgActionP)liquid3d)->impl = (struct LgActionImpl *)&liquid3dImpl;
	DURATION(liquid3d) = duration;
	GRID_COLS(liquid3d) = cols, GRID_ROWS(liquid3d) = rows;
	liquid3d->waves = waves;
	liquid3d->amplitude = amplitude;
	return (LgActionP)liquid3d;
}

// shaky ---------------------------------------------------------------------------------------------------------------------
struct LgActionShakyY {
	struct LgActionGrid	base;
	int					range;
	LgBool				shakeZ;
};

static LgActionP shakyClone(LgActionP self) {
	struct LgActionShakyY *shaky = (struct LgActionShakyY *)self;
	return LgActionShakyYCreate(DURATION(shaky), GRID_COLS(shaky), GRID_ROWS(shaky), shaky->range, shaky->shakeZ);
}

static void shakyStep(LgActionP self, float ratio) {
	struct LgActionShakyY *shaky = (struct LgActionShakyY *)self;

	if (IS_VALID_GRID(self)) {
		int col, row;
		struct LgSize gridsize;

		LgGridSize(GRID(self), &gridsize);

		for (col = 0; col < gridsize.width; col++) {
			for (row = 0; row < gridsize.height; row++) {
				struct LgVector3 lt, lb, rt, rb;

				LgTileGrid3DOrgPositionWithAnchor(GRID(self), col, row, LgGridAnchorLT, &lt);
				LgTileGrid3DOrgPositionWithAnchor(GRID(self), col, row, LgGridAnchorLB, &lb);
				LgTileGrid3DOrgPositionWithAnchor(GRID(self), col, row, LgGridAnchorRT, &rt);
				LgTileGrid3DOrgPositionWithAnchor(GRID(self), col, row, LgGridAnchorRB, &rb);

				lb.x += (rand() % shaky->range * 2) - shaky->range;
				rb.x += (rand() % shaky->range * 2) - shaky->range;
				lt.x += (rand() % shaky->range * 2) - shaky->range;
				rt.x += (rand() % shaky->range * 2) - shaky->range;

				lb.y += (rand() % shaky->range * 2) - shaky->range;
				rb.y += (rand() % shaky->range * 2) - shaky->range;
				lt.y += (rand() % shaky->range * 2) - shaky->range;
				rt.y += (rand() % shaky->range * 2) - shaky->range;

				if (shaky->shakeZ) {
					lb.z += (rand() % shaky->range * 2) - shaky->range;
					rb.z += (rand() % shaky->range * 2) - shaky->range;
					lt.z += (rand() % shaky->range * 2) - shaky->range;
					rt.z += (rand() % shaky->range * 2) - shaky->range;
				}

				LgTileGrid3DSetPositionWithAnchor(GRID(self), col, row, LgGridAnchorLT, &lt);
				LgTileGrid3DSetPositionWithAnchor(GRID(self), col, row, LgGridAnchorLB, &lb);
				LgTileGrid3DSetPositionWithAnchor(GRID(self), col, row, LgGridAnchorRT, &rt);
				LgTileGrid3DSetPositionWithAnchor(GRID(self), col, row, LgGridAnchorRB, &rb);
			}
		}
	}
}

static struct LgIntervalImpl shakyImpl = {
	{
		intervalDestroy,
		intervalUpdate,
		grid3dActionReset,
		grid3dStop,
		actionAttachNode,
		shakyClone,
	},
	shakyStep,
};

LgActionP LgActionShakyYCreate(LgTime duration, int cols, int rows, int range, LgBool shakeZ) {
	struct LgActionShakyY *shaky = (struct LgActionShakyY *)createAction(sizeof(struct LgActionShakyY));
	((LgActionP)shaky)->impl = (struct LgActionImpl *)&shakyImpl;
	DURATION(shaky) = duration;
	GRID_COLS(shaky) = cols, GRID_ROWS(shaky) = rows;
	GRID_IS_TILE(shaky) = LgTrue;
	shaky->range = range;
	shaky->shakeZ = shakeZ;
	return (LgActionP)shaky;
}

// fadeoutrb ---------------------------------------------------------------------------------------------------------------------
struct LgActionFadeoutRB {
	struct LgActionGrid	base;
	float (* testFunc)(LgGridP grid, int col, int row, float ratio);
	void (* transformTile)(LgGridP grid, int col, int row, float distance);
};

static LgActionP fadeoutrbClone(LgActionP self) {
	struct LgActionFadeoutRB *fadeoutrb = (struct LgActionFadeoutRB *)self;
	return LgActionFadeOutRBCreate(DURATION(fadeoutrb), GRID_COLS(fadeoutrb), GRID_ROWS(fadeoutrb));
}

static void turnOnTile(LgGridP grid, int col, int row) {
	struct LgVector3 lt, lb, rt, rb;

	LgTileGrid3DOrgPositionWithAnchor(grid, col, row, LgGridAnchorLT, &lt);
	LgTileGrid3DOrgPositionWithAnchor(grid, col, row, LgGridAnchorLB, &lb);
	LgTileGrid3DOrgPositionWithAnchor(grid, col, row, LgGridAnchorRT, &rt);
	LgTileGrid3DOrgPositionWithAnchor(grid, col, row, LgGridAnchorRB, &rb);

	LgTileGrid3DSetPositionWithAnchor(grid, col, row, LgGridAnchorLT, &lt);
	LgTileGrid3DSetPositionWithAnchor(grid, col, row, LgGridAnchorLB, &lb);
	LgTileGrid3DSetPositionWithAnchor(grid, col, row, LgGridAnchorRT, &rt);
	LgTileGrid3DSetPositionWithAnchor(grid, col, row, LgGridAnchorRB, &rb);
}

static void turnOffTile(LgGridP grid, int col, int row) {
	struct LgVector3 zero = {0, 0, 0};

	LgTileGrid3DSetPositionWithAnchor(grid, col, row, LgGridAnchorLT, &zero);
	LgTileGrid3DSetPositionWithAnchor(grid, col, row, LgGridAnchorLB, &zero);
	LgTileGrid3DSetPositionWithAnchor(grid, col, row, LgGridAnchorRT, &zero);
	LgTileGrid3DSetPositionWithAnchor(grid, col, row, LgGridAnchorRB, &zero);
}

void fadeoutrbTransformTile(LgGridP grid, int col, int row, float distance) {
	struct LgVector3 lt, lb, rt, rb;
	struct LgVector2 cellSize;

	LgTileGrid3DOrgPositionWithAnchor(grid, col, row, LgGridAnchorLT, &lt);
	LgTileGrid3DOrgPositionWithAnchor(grid, col, row, LgGridAnchorLB, &lb);
	LgTileGrid3DOrgPositionWithAnchor(grid, col, row, LgGridAnchorRT, &rt);
	LgTileGrid3DOrgPositionWithAnchor(grid, col, row, LgGridAnchorRB, &rb);
	
	LgGridCellSize(grid, &cellSize);

	lb.x += (cellSize.x / 2) * (1.0f - distance);
	lb.y -= (cellSize.y / 2) * (1.0f - distance);

	rb.x -= (cellSize.x / 2) * (1.0f - distance);
	rb.y -= (cellSize.y / 2) * (1.0f - distance);

	lt.x += (cellSize.x / 2) * (1.0f - distance);
	lt.y += (cellSize.y / 2) * (1.0f - distance);

	rt.x -= (cellSize.x / 2) * (1.0f - distance);
	rt.y += (cellSize.y / 2) * (1.0f - distance);
	
	LgTileGrid3DSetPositionWithAnchor(grid, col, row, LgGridAnchorLT, &lt);
	LgTileGrid3DSetPositionWithAnchor(grid, col, row, LgGridAnchorLB, &lb);
	LgTileGrid3DSetPositionWithAnchor(grid, col, row, LgGridAnchorRT, &rt);
	LgTileGrid3DSetPositionWithAnchor(grid, col, row, LgGridAnchorRB, &rb);
}

float fadeoutrbTestFunc(LgGridP grid, int col, int row, float ratio) {
	struct LgSize size;
	float x, y;

	LgGridSize(grid, &size);
	x = size.width * ratio, y = size.height * ratio;
	if (x + y == 0.0f) return 1.0f;
	return powf((float)(col + row) / (float)(x + y), 6);
}

static void fadeoutrbStep(LgActionP self, float ratio) {
	struct LgActionFadeoutRB *fadeoutrb = (struct LgActionFadeoutRB *)self;

	if (IS_VALID_GRID(self)) {
		int col, row;
		struct LgSize gridsize;

		LgGridSize(GRID(self), &gridsize);

		for (col = 0; col < gridsize.width; col++) {
			for (row = 0; row < gridsize.height; row++) {
				float distance = fadeoutrb->testFunc(GRID(fadeoutrb), col, row, ratio);
				if (distance == 0) turnOffTile(GRID(fadeoutrb), col, row);
				else if (distance < 1) fadeoutrb->transformTile(GRID(fadeoutrb), col, row, distance);
				else turnOnTile(GRID(fadeoutrb), col, row);
			}
		}
	}
}

static struct LgIntervalImpl fadeoutrbImpl = {
	{
		intervalDestroy,
		intervalUpdate,
		grid3dActionReset,
		grid3dStop,
		actionAttachNode,
		fadeoutrbClone,
	},
	fadeoutrbStep,
};

LgActionP LgActionFadeOutRBCreate(LgTime duration, int cols, int rows) {
	struct LgActionFadeoutRB *fadeoutrb = (struct LgActionFadeoutRB *)createAction(sizeof(struct LgActionFadeoutRB));
	((LgActionP)fadeoutrb)->impl = (struct LgActionImpl *)&fadeoutrbImpl;
	DURATION(fadeoutrb) = duration;
	GRID_COLS(fadeoutrb) = cols, GRID_ROWS(fadeoutrb) = rows;
	GRID_IS_TILE(fadeoutrb) = LgTrue;
	fadeoutrb->testFunc = fadeoutrbTestFunc;
	fadeoutrb->transformTile = fadeoutrbTransformTile;
	return (LgActionP)fadeoutrb;
}

// fadeoutlt ---------------------------------------------------------------------------------------------------------------------
float fadeoutltTestFunc(LgGridP grid, int col, int row, float ratio) {
	struct LgSize size;
	float x, y;

	LgGridSize(grid, &size);
	x = size.width * (1.0f - ratio), y = size.height * (1.0f - ratio);
	if (col + row == 0) return 1.0f;
	return powf((float)(x + y) / (float)(col + row), 6);
}

LgActionP LgActionFadeOutLTCreate(LgTime duration, int cols, int rows) {
	struct LgActionFadeoutRB *fadeoutlt = (struct LgActionFadeoutRB *)createAction(sizeof(struct LgActionFadeoutRB));
	((LgActionP)fadeoutlt)->impl = (struct LgActionImpl *)&fadeoutrbImpl;
	DURATION(fadeoutlt) = duration;
	GRID_COLS(fadeoutlt) = cols, GRID_ROWS(fadeoutlt) = rows;
	GRID_IS_TILE(fadeoutlt) = LgTrue;
	fadeoutlt->testFunc = fadeoutltTestFunc;
	fadeoutlt->transformTile = fadeoutrbTransformTile;
	return (LgActionP)fadeoutlt;
}

// fadeoutup ---------------------------------------------------------------------------------------------------------------------
float fadeoutupTestFunc(LgGridP grid, int col, int row, float ratio) {
	struct LgSize size;
	float y;

	LgGridSize(grid, &size);
	y = size.height * (1.0f - ratio);
	if (row == 0) return 1.0f;
	return powf((float)y / row, 6);
}

void fadeoutupTransformTile(LgGridP grid, int col, int row, float distance) {
	struct LgVector3 lt, lb, rt, rb;
	struct LgVector2 cellSize;

	LgTileGrid3DOrgPositionWithAnchor(grid, col, row, LgGridAnchorLT, &lt);
	LgTileGrid3DOrgPositionWithAnchor(grid, col, row, LgGridAnchorLB, &lb);
	LgTileGrid3DOrgPositionWithAnchor(grid, col, row, LgGridAnchorRT, &rt);
	LgTileGrid3DOrgPositionWithAnchor(grid, col, row, LgGridAnchorRB, &rb);

	LgGridCellSize(grid, &cellSize);

	lb.y -= (cellSize.y / 2) * (1.0f - distance);
	rb.y -= (cellSize.y / 2) * (1.0f - distance);
	lt.y += (cellSize.y / 2) * (1.0f - distance);
	rt.y += (cellSize.y / 2) * (1.0f - distance);

	LgTileGrid3DSetPositionWithAnchor(grid, col, row, LgGridAnchorLT, &lt);
	LgTileGrid3DSetPositionWithAnchor(grid, col, row, LgGridAnchorLB, &lb);
	LgTileGrid3DSetPositionWithAnchor(grid, col, row, LgGridAnchorRT, &rt);
	LgTileGrid3DSetPositionWithAnchor(grid, col, row, LgGridAnchorRB, &rb);
}

LgActionP LgActionFadeOutUPCreate(LgTime duration, int cols, int rows) {
	struct LgActionFadeoutRB *fadeoutup = (struct LgActionFadeoutRB *)createAction(sizeof(struct LgActionFadeoutRB));
	((LgActionP)fadeoutup)->impl = (struct LgActionImpl *)&fadeoutrbImpl;
	DURATION(fadeoutup) = duration;
	GRID_COLS(fadeoutup) = cols, GRID_ROWS(fadeoutup) = rows;
	GRID_IS_TILE(fadeoutup) = LgTrue;
	fadeoutup->testFunc = fadeoutupTestFunc;
	fadeoutup->transformTile = fadeoutupTransformTile;
	return (LgActionP)fadeoutup;
}

// fadeoutdown ---------------------------------------------------------------------------------------------------------------------
float fadeoutdownTestFunc(LgGridP grid, int col, int row, float ratio) {
	struct LgSize size;
	float y;

	LgGridSize(grid, &size);
	y = size.height * ratio;
	if (y == 0.0f) return 1.0f;
	return powf((float)row / y, 6);
}

LgActionP LgActionFadeOutDOWNCreate(LgTime duration, int cols, int rows) {
	struct LgActionFadeoutRB *fadeoutdown = (struct LgActionFadeoutRB *)createAction(sizeof(struct LgActionFadeoutRB));
	((LgActionP)fadeoutdown)->impl = (struct LgActionImpl *)&fadeoutrbImpl;
	DURATION(fadeoutdown) = duration;
	GRID_COLS(fadeoutdown) = cols, GRID_ROWS(fadeoutdown) = rows;
	GRID_IS_TILE(fadeoutdown) = LgTrue;
	fadeoutdown->testFunc = fadeoutdownTestFunc;
	fadeoutdown->transformTile = fadeoutupTransformTile;
	return (LgActionP)fadeoutdown;
}

// splitcols ---------------------------------------------------------------------------------------------------------------------
struct LgActionSplitCols {
	struct LgActionGrid	base;
};

static LgActionP splitColsClone(LgActionP self) {
	struct LgActionSplitCols *splitCols = (struct LgActionSplitCols *)self;
	return LgActionSplitColsCreate(DURATION(splitCols), GRID_COLS(splitCols));
}

static void splitColsStep(LgActionP self, float ratio) {
	if (IS_VALID_GRID(self)) {
		int col, row;
		struct LgSize gridsize;
		struct LgVector2 cellsize;

		LgGridSize(GRID(self), &gridsize);
		LgGridCellSize(GRID(self), &cellsize);

		LgGridSize(GRID(self), &gridsize);

		for (col = 0; col < gridsize.width; col++) {
			for (row = 0; row < gridsize.height; row++) {
				struct LgVector3 lt, lb, rt, rb;
				float direction = 1;

				LgTileGrid3DOrgPositionWithAnchor(GRID(self), col, row, LgGridAnchorLT, &lt);
				LgTileGrid3DOrgPositionWithAnchor(GRID(self), col, row, LgGridAnchorLB, &lb);
				LgTileGrid3DOrgPositionWithAnchor(GRID(self), col, row, LgGridAnchorRT, &rt);
				LgTileGrid3DOrgPositionWithAnchor(GRID(self), col, row, LgGridAnchorRB, &rb);

				if ((col % 2) == 0) direction = -1;

				lt.y += direction * cellsize.y * ratio;
				lb.y += direction * cellsize.y * ratio;
				rt.y += direction * cellsize.y * ratio;
				rb.y += direction * cellsize.y * ratio;

				LgTileGrid3DSetPositionWithAnchor(GRID(self), col, row, LgGridAnchorLT, &lt);
				LgTileGrid3DSetPositionWithAnchor(GRID(self), col, row, LgGridAnchorLB, &lb);
				LgTileGrid3DSetPositionWithAnchor(GRID(self), col, row, LgGridAnchorRT, &rt);
				LgTileGrid3DSetPositionWithAnchor(GRID(self), col, row, LgGridAnchorRB, &rb);
			}
		}
	}
}

static struct LgIntervalImpl splitColsImpl = {
	{
		intervalDestroy,
		intervalUpdate,
		grid3dActionReset,
		grid3dStop,
		actionAttachNode,
		splitColsClone,
	},
	splitColsStep,
};

LgActionP LgActionSplitColsCreate(LgTime duration, int cols) {
	struct LgActionSplitCols *splitCols = (struct LgActionSplitCols *)createAction(sizeof(struct LgActionSplitCols));
	((LgActionP)splitCols)->impl = (struct LgActionImpl *)&splitColsImpl;
	DURATION(splitCols) = duration;
	GRID_COLS(splitCols) = cols, GRID_ROWS(splitCols) = 1;
	GRID_IS_TILE(splitCols) = LgTrue;
	return (LgActionP)splitCols;
}

// splitrows ---------------------------------------------------------------------------------------------------------------------
struct LgActionSplitRows {
	struct LgActionGrid	base;
};

static LgActionP splitRowsClone(LgActionP self) {
	struct LgActionSplitRows *splitRows = (struct LgActionSplitRows *)self;
	return LgActionSplitColsCreate(DURATION(splitRows), GRID_ROWS(splitRows));
}

static void splitRowsStep(LgActionP self, float ratio) {
	if (IS_VALID_GRID(self)) {
		int col, row;
		struct LgSize gridsize;
		struct LgVector2 cellsize;

		LgGridSize(GRID(self), &gridsize);
		LgGridCellSize(GRID(self), &cellsize);

		for (col = 0; col < gridsize.width; col++) {
			for (row = 0; row < gridsize.height; row++) {
				struct LgVector3 lt, lb, rt, rb;
				float direction = 1;

				LgTileGrid3DOrgPositionWithAnchor(GRID(self), col, row, LgGridAnchorLT, &lt);
				LgTileGrid3DOrgPositionWithAnchor(GRID(self), col, row, LgGridAnchorLB, &lb);
				LgTileGrid3DOrgPositionWithAnchor(GRID(self), col, row, LgGridAnchorRT, &rt);
				LgTileGrid3DOrgPositionWithAnchor(GRID(self), col, row, LgGridAnchorRB, &rb);

				if ((row % 2) == 0) direction = -1;

				lt.x += direction * cellsize.x * ratio;
				lb.x += direction * cellsize.x * ratio;
				rt.x += direction * cellsize.x * ratio;
				rb.x += direction * cellsize.x * ratio;

				LgTileGrid3DSetPositionWithAnchor(GRID(self), col, row, LgGridAnchorLT, &lt);
				LgTileGrid3DSetPositionWithAnchor(GRID(self), col, row, LgGridAnchorLB, &lb);
				LgTileGrid3DSetPositionWithAnchor(GRID(self), col, row, LgGridAnchorRT, &rt);
				LgTileGrid3DSetPositionWithAnchor(GRID(self), col, row, LgGridAnchorRB, &rb);
			}
		}
	}
}

static struct LgIntervalImpl splitRowsImpl = {
	{
		intervalDestroy,
		intervalUpdate,
		grid3dActionReset,
		grid3dStop,
		actionAttachNode,
		splitRowsClone,
	},
	splitRowsStep,
};

LgActionP LgActionSplitRowsCreate(LgTime duration, int rows) {
	struct LgActionSplitCols *splitRows = (struct LgActionSplitCols *)createAction(sizeof(struct LgActionSplitCols));
	((LgActionP)splitRows)->impl = (struct LgActionImpl *)&splitRowsImpl;
	DURATION(splitRows) = duration;
	GRID_COLS(splitRows) = 1, GRID_ROWS(splitRows) = rows;
	GRID_IS_TILE(splitRows) = LgTrue;
	return (LgActionP)splitRows;
}