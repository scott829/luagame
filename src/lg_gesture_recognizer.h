﻿#ifndef _GESTURE_RECOGNIZER_H
#define _GESTURE_RECOGNIZER_H

#include "lg_node.h"

enum LgGestureType {
	LgGestureTap,
	LgGestureLongPress,
	LgGesturePinch,
	LgGestureSwipe,
};

typedef struct LgGestureEvent {
	enum LgGestureType	type;
	void *				userdata;
}*LgGestureEventP;

typedef struct LgGestureRecognizer *LgGestureRecognizerP;

LG_REFCNT_FUNCTIONS_DECL(LgGestureRecognizer);

void LgGestureRecognizerHandleTouchEvent(LgGestureRecognizerP gr, LgNodeTouchEventP evt);

void LgGestureRecognizerUpdate(LgGestureRecognizerP gr, float delta);

LgBool LgGestureRecognizerAttachNode(LgGestureRecognizerP gr, LgNodeP node);

// tap --------------------------------------------------------------------------------------------------------
LgGestureRecognizerP LgGestureRecognizerTapCreate(int numberOfTapsRequired);

// long press --------------------------------------------------------------------------------------------------------
LgGestureRecognizerP LgGestureRecognizerLongPressCreate();

// pinch --------------------------------------------------------------------------------------------------------
typedef struct LgGesturePinchEvent {
	float startDistance, currentDistance;
}*LgGesturePinchEventP;

LgGestureRecognizerP LgGestureRecognizerPinchCreate();

// swipe --------------------------------------------------------------------------------------------------------

enum LgGestureSwipeDirection {
	LgGestureSwipeLeft,
	LgGestureSwipeRight,
	LgGestureSwipeUp,
	LgGestureSwipeDown,
};

LgGestureRecognizerP LgGestureRecognizerSwipeCreate();

#endif