﻿#include "lg_controlnode.h"
#include "lg_sprite.h"

void LgControlEventProc(LgEventType type, LgNodeP node, void *param) {
	switch(type) {
	case LG_NODE_EVENT_Destroy:
		{
			LgControlDataP cd = (LgControlDataP)LgNodeUserData(node);
			LgFree(cd);
		}
		break;
	case LG_NODE_EVENT_IsClipTouch:
		*((LgBool *)param) = LgTrue;
		return;
	case LG_NODE_EVENT_HandleTouch:
		{
			LgControlDataP cd = (LgControlDataP)LgNodeUserData(node);
			if (!cd->enabled) {
				// 如果节点是禁用状态，则无法点击
				return;
			}
		}
		break;
	case LG_NODE_EVENT_ContentBox:
		{
			LgControlDataP cd = (LgControlDataP)LgNodeUserData(node);
			LgAabbP contentBox = (LgAabbP)param;
			contentBox->x = 0, contentBox->y = 0;
			contentBox->width = cd->size.x, contentBox->height = cd->size.y;
		}
		return;
	case LG_CONTROLNODE_EVENT_SetEnable:
		{
			LgControlDataP cd = (LgControlDataP)LgNodeUserData(node);
			cd->enabled = (LgBool)param;
		}
		break;
	case LG_CONTROLNODE_EVENT_IsEnabled:
		{
			LgControlDataP cd = (LgControlDataP)LgNodeUserData(node);
			*((LgBool *)param) = cd->enabled;
		}
		break;
	case LG_CONTROLNODE_EVENT_SetHighlight:
		{
			LgControlDataP cd = (LgControlDataP)LgNodeUserData(node);
			cd->highlight = (LgBool)param;
		}
		break;
	case LG_CONTROLNODE_EVENT_IsHighlighted:
		{
			LgControlDataP cd = (LgControlDataP)LgNodeUserData(node);
			*((LgBool *)param) = cd->highlight;
		}
		break;
	case LG_CONTROLNODE_EVENT_SetSelect:
		{
			LgControlDataP cd = (LgControlDataP)LgNodeUserData(node);
			cd->selected = (LgBool)param;
		}
		break;
	case LG_CONTROLNODE_EVENT_IsSelected:
		{
			LgControlDataP cd = (LgControlDataP)LgNodeUserData(node);
			*((LgBool *)param) = cd->selected;
		}
		break;
	case LG_CONTROLNODE_EVENT_SetSize:
		{
			LgControlDataP cd = (LgControlDataP)LgNodeUserData(node);

			cd->size = *((LgVector2P)param);
			if (cd->backgroundNode) LgSpriteSetSize(cd->backgroundNode, &cd->size);
		}
		break;
	}

	LgNodeDefEventProc(type, node, param);
}

LgBool LgNodeIsControlNode(LgNodeP node) {
	return LgNodeEventProc(node) == (LgEvent)LgControlEventProc;
}

LgNodeP LgControlNodeCreateR(const LgVector2P size) {
	return LgControlNodeCreateSubClassR(sizeof(struct LgControlData), NULL, size);
}

LgNodeP LgControlNodeCreateSubClassR(size_t dataSize, LgEvent eventProc, const LgVector2P size) {
	LgNodeP node;
	LgControlDataP cd;

	if (!eventProc) eventProc = (LgEvent)LgControlEventProc;

	node = LgNodeCreateR(eventProc);
	LG_CHECK_ERROR(node);

	cd = (LgControlDataP)LgMallocZ(dataSize);
	LG_CHECK_ERROR(cd);
	LgNodeSetUserData(node, cd);
	cd->size = *size;
	cd->enabled = LgTrue;
	LgNodeCenterTransformAnchor(node);

	return node;

_error:
	if (node) LgNodeRelease(node);
	return NULL;
}

void LgControlNodeSetEnable(LgNodeP node, LgBool enable) {
	LgNodeDoEvent(LG_CONTROLNODE_EVENT_SetEnable, node, (void *)enable);
}

LgBool LgControlNodeIsEnabled(LgNodeP node) {
	LgBool ret;
	LgNodeDoEvent(LG_CONTROLNODE_EVENT_IsEnabled, node, &ret);
	return ret;
}

void LgControlNodeSetHighlight(LgNodeP node, LgBool enable) {
	LgNodeDoEvent(LG_CONTROLNODE_EVENT_SetHighlight, node, (void *)enable);
}

LgBool LgControlNodeIsHighlighted(LgNodeP node) {
	LgBool ret;
	LgNodeDoEvent(LG_CONTROLNODE_EVENT_IsHighlighted, node, &ret);
	return ret;
}

void LgControlNodeSetSelect(LgNodeP node, LgBool enable) {
	LgNodeDoEvent(LG_CONTROLNODE_EVENT_SetSelect, node, (void *)enable);
}

LgBool LgControlNodeIsSelected(LgNodeP node) {
	LgBool ret;
	LgNodeDoEvent(LG_CONTROLNODE_EVENT_IsSelected, node, &ret);
	return ret;
}

void LgControlNodeSetSize(LgNodeP node, const LgVector2P size) {
	LgNodeDoEvent(LG_CONTROLNODE_EVENT_SetSize, node, size);
}

void LgControlNodeSetBackground(LgNodeP node, LgNodeP backgroundNode) {
	LgControlDataP cd = (LgControlDataP)LgNodeUserData(node);
	
	// 只有sprite能作为背景
	if (backgroundNode && !LgNodeIsSprite(backgroundNode)) return;
	
	if (cd->backgroundNode) LgNodeRemove(cd->backgroundNode);
	LgNodeAddChild(node, backgroundNode);
	cd->backgroundNode = backgroundNode;
	LgNodeSetZorder(backgroundNode, LG_UILAYER_BACKGROUND);
	
	LgSpriteSetSize(backgroundNode, &cd->size);
	LgNodeCenter(backgroundNode);
}
