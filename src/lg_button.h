#ifndef _LG_BUTTON_H
#define _LG_BUTTON_H

#include "lg_controlnode.h"

typedef struct LgButtonData {
	struct LgControlData	base;
	LgNodeP					titleNode;
	LgActionP				pressAction, releaseAction;
	LgNodeP					faceNode, pressFaceNode;
	LgColor					titleColor, titlePressColor;
	LgBool					down;
	struct LgVector2		downPosition;
	LgSlot					slotClick;
}*LgButtonDataP;

LgNodeP LgButtonCreateR(const LgVector2P size);

LgBool LgNodeIsButton(LgNodeP node);

void LgButtonSetTitle(LgNodeP node, LgNodeP titleNode);

void LgButtonSetTitleAsText(LgNodeP node, const char *text, const char *fontname, int fontsize);

void LgButtonSetPressAction(LgNodeP node, LgActionP action);

void LgButtonSetReleaseAction(LgNodeP node, LgActionP action);

void LgButtonSetTitleTextColor(LgNodeP node, LgColor color);

void LgButtonSetTitlePressTextColor(LgNodeP node, LgColor color);

void LgButtonSetFace(LgNodeP node, LgNodeP faceNode);

void LgButtonSetPressFace(LgNodeP node, LgNodeP faceNode);

LgSlot *LgButtonSlotClick(LgNodeP node);

#endif