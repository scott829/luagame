#ifndef _LG_LABEL_H
#define _LG_LABEL_H

#include "lg_node.h"
#include "lg_fontsystem.h"

enum {
	LG_LABEL_EVENT_SetFontName = LG_NODE_EVENT_LAST,
	LG_LABEL_EVENT_FontName,
	LG_LABEL_EVENT_SetFontSize,
	LG_LABEL_EVENT_FontSize,
	LG_LABEL_EVENT_SetColor,
	LG_LABEL_EVENT_Color,
	LG_LABEL_EVENT_SetText,
	LG_LABEL_EVENT_Text,
	LG_LABEL_EVENT_TextLength,
	LG_LABEL_EVENT_SetDrawFlags,
	LG_LABEL_EVENT_DrawFlags,
	LG_LABEL_EVENT_Font,
	LG_LABEL_EVENT_LAST,
};

LgNodeP LgLabelCreateR(const char *text);

typedef struct LgLabelEventGetText {
	char *	buf;
	int		bufLength;
}*LgLabelEventGetTextP;

LgBool LgNodeIsLabel(LgNodeP node);

void LgLabelSetFontName(LgNodeP node, const char *fontname);

const char *LgLabelFontName(LgNodeP node);

void LgLabelSetFontSize(LgNodeP node, int fontsize);

int LgLabelFontSize(LgNodeP node);

void LgLabelSetColor(LgNodeP node, LgColor color);

LgColor LgLabelColor(LgNodeP node);

void LgLabelSetText(LgNodeP node, const char *text);

int LgLabelTextLength(LgNodeP node);

void LgLabelText(LgNodeP node, char *buf, int bufLen);

void LgLabelSetDrawFlag(LgNodeP node, int flags);

int LgLabelDrawFlag(LgNodeP node);

LgFontP LgLabelFont(LgNodeP node);

#endif