#ifndef _LG_WINDOW_IOS_H_
#define _LG_WINDOW_IOS_H_

#include "lg_window.h"
#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>

UIWindow *LgWindowUIWindow();

void LgWindowBeginDraw();

void LgWindowEndDraw();

#endif
