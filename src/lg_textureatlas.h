#ifndef _LG_TEXTURE_ATLAS_H
#define _LG_TEXTURE_ATLAS_H

#include "lg_aabb.h"
#include "lg_rendersystem.h"
#include "lg_refcnt.h"

#define LG_TEXTUREATLAS_FIRSTTEXTURE 1

typedef struct LgTextAtlasRecord {
	int				textureId;
	struct LgPoint	pt;
	struct LgSize	size;
}*LgTextAtlasRecordP;

typedef struct LgTextureAtlas *LgTextureAtlasP;

typedef struct LgAtlasDrawBuffer *LgAtlasDrawBufferP;

LG_REFCNT_FUNCTIONS_DECL(LgTextureAtlas)

LgTextureAtlasP LgTextureAtlasCreateR();

int LgTextureAtlasAddEmptyTexture(LgTextureAtlasP ta, enum LgImageFormat_ format, const LgSizeP size);

int LgTextureAtlasAddTextureFromFile(LgTextureAtlasP ta, const char *filename);

int LgTextureAtlasAddTexture(LgTextureAtlasP ta, LgTextureP texture);

void LgTextureAtlasRemoveAllTextures(LgTextureAtlasP ta);

void LgTextureAtlasRemoveAllRecords(LgTextureAtlasP ta);

void LgTextureAtlasRemoveAll(LgTextureAtlasP ta);

int LgTextureAtlasCountTextures(LgTextureAtlasP ta);

int LgTextureAtlasCountRecord(LgTextureAtlasP ta);

LgTextureP LgTextureAtlasTexture(LgTextureAtlasP ta, int i);

void LgTextureAtlasAddRecord(LgTextureAtlasP ta, int characterId, const LgTextAtlasRecordP record);

LgBool LgTextureAtlasRecord(LgTextureAtlasP ta, int characterId, LgTextAtlasRecordP record);

LgAtlasDrawBufferP LgTextureAtlasCreateDrawBufferR(LgTextureAtlasP ta);

// ----------------------------------------------------------------------------------------------------------------------------------

LG_REFCNT_FUNCTIONS_DECL(LgAtlasDrawBuffer)

LgBool LgAtlasBufferDrawRect(LgAtlasDrawBufferP buf, int characterId, const LgAabbP aabb);

LgBool LgAtlasBufferDrawQuad(LgAtlasDrawBufferP buf, int characterId,
							 const LgVector2P lt, const LgVector2P rt, const LgVector2P lb, const LgVector2P rb);

void LgAtlasBufferClear(LgAtlasDrawBufferP buf);

void LgAtlasBufferDraw(LgAtlasDrawBufferP buf);

int LgAtlasBufferNumberOfBatch(LgAtlasDrawBufferP buf);

#endif