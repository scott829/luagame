#include "lg_vector3.h"

float LgVector3Length(const LgVector3P vec) {
	return sqrtf(vec->x * vec->x + vec->y * vec->y +  vec->z * vec->z);
}

float LgVector3SquaredLength(const LgVector3P vec) {
	return vec->x * vec->x + vec->y * vec->y +  vec->z * vec->z;
}

float LgVector3Distance(const LgVector3P lhs, const LgVector3P rhs) {
	struct LgVector3 t = {lhs->x - rhs->x, lhs->y - rhs->y, lhs->z - rhs->z};
	return LgVector3Length(&t);
}

float LgVector3SquaredDistance(const LgVector3P lhs, const LgVector3P rhs) {
	struct LgVector3 t = {lhs->x - rhs->x, lhs->y - rhs->y};
	return LgVector3SquaredLength(&t);
}

float LgVector3Dot(const LgVector3P lhs, const LgVector3P rhs) {
	return lhs->x * rhs->x + lhs->y * rhs->y +  lhs->z * rhs->z;
}

void LgVector3Cross(LgVector3P lhs, const LgVector3P rhs) {
	float _x = lhs->y * rhs->z - lhs->z * rhs->y,
		_y = -lhs->x * rhs->z + lhs->z * rhs->x,
		_z = lhs->x * rhs->y - lhs->y * rhs->x;
	lhs->x = _x, lhs->y = _y, lhs->z = _z;
}

void LgVector3MidPoint(LgVector3P lhs, const LgVector3P rhs) {
	lhs->x = (lhs->x + rhs->x) / 2;
	lhs->y = (lhs->y + rhs->y) / 2;
	lhs->z = (lhs->z + rhs->z) / 2;
}

void LgVector3Normalize(LgVector3P vec) {
	float d = sqrtf(vec->x * vec->x + vec->y * vec->y + vec->z * vec->z);
	vec->x /= d, vec->y /= d, vec->z /= d;
}

LgBool LgVector3IsZero(const LgVector3P vec) {
	return LgFloatEqual(vec->x, 0.0f) && LgFloatEqual(vec->y, 0.0f) && LgFloatEqual(vec->z, 0.0f);
}

void LgVector3Add(LgVector3P lhs, const LgVector3P rhs) {
	lhs->x = lhs->x + rhs->x;
	lhs->y = lhs->y + rhs->y;
	lhs->z = lhs->z + rhs->z;
}

void LgVector3Sub(LgVector3P lhs, const LgVector3P rhs) {
	lhs->x = lhs->x - rhs->x;
	lhs->y = lhs->y - rhs->y;
	lhs->z = lhs->z - rhs->z;
}

void LgVector3Mul(LgVector3P lhs, const LgVector3P rhs) {
	lhs->x = lhs->x * rhs->x;
	lhs->y = lhs->y * rhs->y;
	lhs->z = lhs->z * rhs->z;
}

void LgVector3Div(LgVector3P lhs, const LgVector3P rhs) {
	lhs->x = lhs->x / rhs->x;
	lhs->y = lhs->y / rhs->y;
	lhs->z = lhs->z / rhs->z;
}

LgBool LgVector3Equal(LgVector3P lhs, const LgVector3P rhs) {
	return LgFloatEqual(lhs->x, rhs->x) && LgFloatEqual(lhs->y, rhs->y) && LgFloatEqual(lhs->z, rhs->z);
}

void LgVector3AddFloat(LgVector3P lhs, float rhs) {
	lhs->x = lhs->x + rhs;
	lhs->y = lhs->y + rhs;
	lhs->z = lhs->z + rhs;
}

void LgVector3SubFloat(LgVector3P lhs, float rhs) {
	lhs->x = lhs->x - rhs;
	lhs->y = lhs->y - rhs;
	lhs->z = lhs->z - rhs;
}

void LgVector3MulFloat(LgVector3P lhs, float rhs) {
	lhs->x = lhs->x * rhs;
	lhs->y = lhs->y * rhs;
	lhs->z = lhs->z * rhs;
}

void LgVector3DivFloat(LgVector3P lhs, float rhs) {
	lhs->x = lhs->x / rhs;
	lhs->y = lhs->y / rhs;
	lhs->z = lhs->z / rhs;
}
