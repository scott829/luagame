#include "lg_archive.h"
#include "lg_dict.h"
#include "lg_list.h"
#include <string.h>
#include "lg_memory.h"
#include "lg_zipheaders.h"
#include "lg_platform.h"
#include "lg_zlib.h"
#include "lg_fileinputstream.h"
#include "lg_init.h"
#include <stdio.h>
#include <sys/stat.h>
#include "lg_log.h"
#include "lg_window.h"

// LgZipInputStream ----------------------------------------------------------------------------

struct LgZipInputStream {
	struct LgInputStream		base;
	LgZlibP						zlib;
	LgInputStreamP				srcInput;
	struct LgZipFileArchiveHead	ah;
};

static void destroy(void *stream) {
	struct LgZipInputStream *s = (struct LgZipInputStream *)stream;
	if (s->srcInput) LgInputStreamDestroy(s->srcInput);
	if (s->zlib) LgZlibDestroy(s->zlib);
	LgFree(stream);
}

static int available(LgInputStreamP stream) {
	struct LgZipInputStream *s = (struct LgZipInputStream *)stream;
	if (s->ah.method) return s->ah.uncomprsize - LgZlibTotalRead(s->zlib);
	else return LgInputStreamAvailable(s->srcInput);
}

static int readBytes(LgInputStreamP stream, void *data, int size) {
	struct LgZipInputStream *s = (struct LgZipInputStream *)stream;
	if (s->ah.method) {
		return LgZlibReadBytes(s->zlib, data, size);
	} else {
		return LgInputStreamReadBytes(s->srcInput, data, size);
	}
}

static LgBool markSupported(LgInputStreamP stream) {
	return LgFalse;
}

static void reset(LgInputStreamP stream) {
	struct LgZipInputStream *s = (struct LgZipInputStream *)stream;
	LgZlibDestroy(s->zlib);
	LgInputStreamReset(s->srcInput);
	s->zlib = LgZlibCreate(LgFalse, s->srcInput);
}

struct LgInputStreamImpl zipInputStream = {
	destroy,
	available,
	readBytes,
	NULL,
	markSupported,
	reset
};

// LgArchive ----------------------------------------------------------------------------

enum LgResourceType {
	LgResourceTypeDirectory,
	LgResourceTypeZip,
};

typedef struct LgResource {
	enum LgResourceType	type;
	
	// for directory
	char				filename[512];
	
	// for zip
	LgDictP				zipEntrties;
}*LgResourceP;

LgListP g_resources = NULL;

static void destroyResource(LgResourceP resource) {
	if (resource->type == LgResourceTypeZip) {
		if (resource->zipEntrties) LgDictDestroy(resource->zipEntrties);
	}
	LgFree(resource);
}

static void readZipFiles(FILE *fp, LgDictP entrties) {
	int sign;
	struct LgZipFileHead fh;
	struct LgZipCentralDirRecord cdr;
	struct LgZipCentralDirLocator cdl;
	struct LgZipEndCentralDirRecord ecdr;
	struct LgZipDigitDesc dd;
	
	while(LgTrue) {
		if (!fread(&sign, sizeof(sign), 1, fp)) {
			break;
		}
		
		if (sign == LGZIP_LOCALFILE_SIGNATURE) {
			fread(&fh, sizeof(fh), 1, fp);
			fseek(fp, fh.namelen + fh.extralen + fh.comprsize, SEEK_CUR);
			
			if (fh.purepose & 0x10) {
				fread(&dd, sizeof(dd), 1, fp);
			}
		}
		else if (sign == LGZIP_ARCHIVE_SIGNATURE) {
			char *name;
			struct LgZipFileArchiveHead *ah =
			(struct LgZipFileArchiveHead *)LgMallocZ(sizeof(struct LgZipFileArchiveHead));
			
			fread(ah, sizeof(struct LgZipFileArchiveHead), 1, fp);
			name = (char *)LgMallocZ(ah->namelen + 1);
			fread(name, ah->namelen, 1, fp);
			name[ah->namelen] = 0;
			
			LgDictReplace(entrties, name, ah);
			fseek(fp, ah->extralen + ah->commlen, SEEK_CUR);
		}
		else if (sign == LGZIP_ARCHIVE_EXTRA_SIGNATURE) {
			unsigned int fieldlen;
			fread(&fieldlen, sizeof(fieldlen), 1, fp);
			fseek(fp, fieldlen, SEEK_CUR);
		}
		else if (sign == LGZIP_DIGITAL_SIGNATURE) {
			unsigned short size;
			fread(&size, sizeof(size), 1, fp);
			fseek(fp, size, SEEK_CUR);
		}
		else if (sign == LGZIP_CENTRAL_DIR_RECORD_SIGNATURE) {
			fread(&cdr, sizeof(cdr), 1, fp);
			fseek(fp, (int)cdr.size, SEEK_CUR);
		}
		else if (sign == LGZIP_CENTRAL_DIR_LOCATOR_SIGNATURE) {
			fread(&cdl, sizeof(cdl), 1, fp);
		}
		else if (sign == LGZIP_END_CENTRAL_DIR_RECORD_SIGNATURE) {
			fread(&ecdr, sizeof(ecdr), 1, fp);
			fseek(fp, ecdr.commlen, SEEK_CUR);
		}
	}
}

static LgBool addSource(const char *filename, LgBool head) {
	LgResourceP resource = NULL;
	FILE *fp = NULL;
	LgBool isDir;
	
#ifdef WIN32
	struct _stat s;
	char path[260]; // win32下面必须删除最后一个 '\'
	strlcpy(path, filename, sizeof(path));
	if (path[strlen(path) - 1] == '\\') path[strlen(path) - 1] = 0;
	LG_CHECK_ERROR(_stat(path, &s) == 0);
#else
	struct stat s;
	LG_CHECK_ERROR(lstat(filename, &s) == 0);
#endif

	resource = (LgResourceP)LgMallocZ(sizeof(struct LgResource));
	LG_CHECK_ERROR(resource);

#ifdef WIN32
	isDir = ((s.st_mode & _S_IFDIR) != 0);
#else
	isDir = S_ISDIR(s.st_mode);
#endif
	
	if (isDir) {
		// 是目录
		resource->type = LgResourceTypeDirectory;
		strcpy(resource->filename, filename);
		strcat(resource->filename, LgPlatformPathSeparator());
	} else {
		// 是Zip文件
		resource->type = LgResourceTypeZip;
		strcpy(resource->filename, filename);
		
		resource->zipEntrties = LgDictCreate((LgCompare)strnocasecmp, (LgDestroy)LgFree, (LgDestroy)LgFree);
		LG_CHECK_ERROR(resource->zipEntrties);
		
		fp = fopen(filename, "rb");
		LG_CHECK_ERROR(fp);
		readZipFiles(fp, resource->zipEntrties);
		fclose(fp);
	}
	
	if (head) LgListInsertBefore(g_resources, LgListFirstNode(g_resources), resource);
	else LgListAppend(g_resources, resource);
	return LgTrue;
	
_error:
	if (resource) destroyResource(resource);
	if (fp) fclose(fp);
	return LgFalse;
}

LgBool addSource2(const char *filename, LgBool head, LgBool isHD) {
	char pathZip[LG_PATH_MAX], pathDirectory[LG_PATH_MAX];

	if (isHD) {
		sprintf(pathZip, "%s-hd.zip", LgResourceFilename(filename));
		sprintf(pathDirectory, "%s-hd", LgResourceDirname(filename));
	} else {
		sprintf(pathZip, "%s.zip", LgResourceFilename(filename));
		sprintf(pathDirectory, "%s", LgResourceDirname(filename));
	}
	
	if (addSource(pathDirectory, head)) {
		LgLogInfo("add resource directory: ", filename);
		return LgTrue;
	}
	
	if (addSource(pathZip, head)) {
		LgLogInfo("add resource package: ", filename);
		return LgTrue;
	}
	
	return LgFalse;
}

LgBool LgArchiveAddSource(const char *filename, LgBool head) {
	if (LgWindowIsHD()) {
		if (!addSource2(filename, head, LgTrue))
			LG_CHECK_ERROR(addSource2(filename, head, LgFalse));
	} else {
		LG_CHECK_ERROR(addSource2(filename, head, LgFalse));
	}
	
	return LgTrue;
	
_error:
	LgLogError("failed to add resource '%s'", filename);
	return LgFalse;
}

LgBool LgArchiveRemoveSource(const char *filename) {
	LgListNodeP node = LgListFirstNode(g_resources);
	while(node) {
		LgResourceP resource = (LgResourceP)LgListNodeValue(node);
		if (strcmp(resource->filename, filename) == 0) {
			LgListRemove(g_resources, node);
			return LgTrue;
		}
		node = LgListNextNode(node);
	}
	return LgFalse;
}

LgBool LgArchiveOpen() {
	g_resources = LgListCreate((LgDestroy)destroyResource);
	LgArchiveAddSource("resource", LgFalse);
	return LgTrue;
}

void LgArchiveClose() {
	if (g_resources) {
		LgListDestroy(g_resources);
		g_resources = NULL;
	}
}

static LgInputStreamP openFile(LgResourceP resource, const char *filename) {
	if (resource->type == LgResourceTypeDirectory) {
		char path[512];
		
		strcpy(path, resource->filename);
		strcat(path, filename);
		return LgFileInputStreamCreate(path, NULL);
	} else if (resource->type == LgResourceTypeZip) {
		struct LgZipInputStream *input = NULL;
		struct LgZipFileArchiveHead *ah;
		struct LgRange range;
		
		ah = (struct LgZipFileArchiveHead *)LgDictFind(resource->zipEntrties, (void *)filename);
		LG_CHECK_ERROR(ah);
		
		range.begin = ah->offset + 30 + ah->namelen;
		range.size = ah->method ? ah->comprsize : ah->uncomprsize;
		input = (struct LgZipInputStream *)LgMallocZ(sizeof(struct LgZipInputStream));
		LG_CHECK_ERROR(input);
		
		input->base.impl = &zipInputStream;
		
		LgCopyMemory(&input->ah, ah, sizeof(struct LgZipFileArchiveHead));
		input->srcInput = LgFileInputStreamCreate(resource->filename, &range);
		LG_CHECK_ERROR(input->srcInput);
		if (input->ah.method) {
			input->zlib = LgZlibCreate(LgFalse, input->srcInput);
			LG_CHECK_ERROR(input->srcInput);
		}
		return (LgInputStreamP)input;
		
	_error:
		if (input) LgInputStreamDestroy(input);
		return NULL;
	} else {
		return NULL;
	}
}

LgInputStreamP LgArchiveOpenFile(const char *filename) {
	LgListNodeP node;
	
	for (node = LgListFirstNode(g_resources); node != NULL; node = LgListNextNode(node)) {
		LgInputStreamP ret = openFile((LgResourceP)LgListNodeValue(node), filename);
		if (ret) return ret;
	}
	return NULL;
}

LgBool LgArchiveNextSource(LgListNodeP *node, const char **filename) {
	LgResourceP resource;

	if (!*node) *node = LgListFirstNode(g_resources);
	if (!*node) return LgFalse;

	resource = (LgResourceP)LgListNodeValue(*node);
	*filename = resource->filename;
	*node = LgListNextNode(*node);
	return *node != NULL;
}
