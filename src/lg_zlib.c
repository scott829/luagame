﻿#include "lg_zlib.h"
#include "zlib/zlib.h"
#include "lg_memory.h"

#define ZLIB_BUFFER_SIZE 8192

struct LgZlib {
	z_stream		z;
	char			inputBuffer[ZLIB_BUFFER_SIZE];
	char			outputBuffer[ZLIB_BUFFER_SIZE];
	int				srcLen;
	LgBool			compress;
	int				pos, len, readSize, windowBits;
	LgInputStreamP	src;
};

LgZlibP LgZlibCreate(LgBool compress, LgInputStreamP input) {
	LgZlibP zlib;

	zlib = (LgZlibP)LgMallocZ(sizeof(struct LgZlib));
	LG_CHECK_ERROR(zlib);
	zlib->compress = compress;
	zlib->windowBits = -15;
	zlib->z.next_in = (Bytef *)zlib->inputBuffer;
	zlib->src = input;
	
	if (compress) deflateInit(&zlib->z, Z_DEFAULT_COMPRESSION);
	else inflateInit2(&zlib->z, zlib->windowBits);

	zlib->pos = zlib->len = 0;
	zlib->readSize = 0;
	return zlib;

_error:
	if (zlib) LgZlibDestroy(zlib);
	return NULL;
}

void LgZlibDestroy(LgZlibP zlib) {
	if (zlib->compress) deflateEnd(&zlib->z);
	else inflateEnd(&zlib->z);
	LgFree(zlib);
}

static LgBool zlibProcess(LgZlibP zlib) {
	int ret;

	zlib->z.avail_out = ZLIB_BUFFER_SIZE;
	zlib->z.next_out = (Bytef *)zlib->outputBuffer;

	if (zlib->z.avail_in == 0) {
		// 补充数据
		int sz;

		zlib->z.next_in = (Bytef *)zlib->inputBuffer;
		sz = ZLIB_BUFFER_SIZE - (int)zlib->z.avail_in;
		sz = LgInputStreamReadBytes(zlib->src, zlib->z.next_in + zlib->z.avail_in, sz);
		zlib->z.avail_in = sz;
	}

	if (zlib->compress) ret = deflate(&zlib->z, zlib->z.avail_in == 0 ? Z_FINISH : Z_NO_FLUSH);
	else ret = inflate(&zlib->z, zlib->z.avail_in == 0 ? Z_FINISH : Z_NO_FLUSH);

	if (ret < 0) return LgFalse;
	zlib->pos = 0;
	zlib->len = ZLIB_BUFFER_SIZE - zlib->z.avail_out;
	return ret != Z_STREAM_END;
}

int LgZlibReadBytes(LgZlibP zlib, void *data, int len) {
	int sz = 0;

	while(sz < len) {
		if (zlib->len > 0) {
			int rsz = LgMin(len - sz, zlib->len);
			LgCopyMemory((char *)data + sz, zlib->outputBuffer + zlib->pos, rsz);
			zlib->pos += rsz, zlib->len -= rsz;
			sz += rsz;
			zlib->readSize += rsz;
		} else {
			if (!zlibProcess(zlib) && zlib->len == 0) break;
		}
	}

	return sz;
}

int LgZlibTotalRead(LgZlibP zlib) {
	return zlib->readSize;
}