#include "lg_glcontext_osx.h"
#include "lg_window_osx.h"
#include <Cocoa/Cocoa.h>

LgBool LgGlContextOpen() {
	return LgTrue;
}

void LgGlContextClose() {
}

void LgGlContextBegin() {
	NSOpenGLView *view = LgWindowNSOpenGLView();
	NSOpenGLContext *context = [view openGLContext];
	[context makeCurrentContext];
}

void LgGlContextEnd() {
	NSOpenGLView *view = LgWindowNSOpenGLView();
	NSOpenGLContext *context = [view openGLContext];
	[context flushBuffer];
	[NSOpenGLContext clearCurrentContext];
}

