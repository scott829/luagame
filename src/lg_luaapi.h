#ifndef _LG_LUAAPI_H
#define _LG_LUAAPI_H

#include "lg_basetypes.h"
#include "lua/lauxlib.h"

static const char *LGCOLOR_HANDLE = "LuaGame::LgColor";

static const char *LGCXFORM_HANDLE = "LuaGame::LgCxform";

static const char *LGPOINT_HANDLE = "LuaGame::LgPoint";

static const char *LGRECT_HANDLE = "LuaGame::LgRect";

static const char *LGACTION_HANDLE = "LuaGame::LgActionP";

static const char *LGACTIONINTERVAL_HANDLE = "LuaGame::LgActionIntervalP";

static const char *LGTOUCH_HANDLE = "LuaGame::LgTouchP";

static const char *LGKEYEVENT_HANDLE = "LuaGame::LgKeyEventP";

static const char *LGNODE_HANDLE = "LuaGame::LgNodeP";

static const char *LGSCENE_HANDLE = "LuaGame::LgSceneP";

static const char *LGLAYER_HANDLE = "LuaGame::LgLayerP";

static const char *LGCOLORLAYER_HANDLE = "LuaGame::LgColorLayerP";

static const char *LGGRADIENTLAYER_HANDLE = "LuaGame::LgGradientLayerP";

static const char *LGTILEMAPLAYER_HANDLE = "LuaGame::LgTileMapLayerP";

static const char *LGPARTICLESYSTEM_HANDLE = "LuaGame::LgParticleSystemP";

static const char *LGSPRITE_HANDLE = "LuaGame::LgSpriteP";

static const char *LGLABEL_HANDLE = "LuaGame::LgLabelP";

static const char *LGTEXTFIELD_HANDLE = "LuaGame::LgTextFieldP";

static const char *LGDRAWNODE_HANDLE = "LuaGame::LgDrawNodeP";

static const char *LGCLIPNODE_HANDLE = "LuaGame::LgClipNodeP";

static const char *LGBATCHNODE_HANDLE = "LuaGame::LgBatchNodeP";

static const char *LGANIMATION_HANDLE = "LuaGame::LgAnimationP";

static const char *LGSCHEDULEITEM_HANDLE = "LuaGame::LgScheduleItemP";

static const char *LGGESTURERECOGNIZER_HANDLE = "LuaGame::LgGestureRecognizerP";

// ui
static const char *LGCONTROLNODE_HANDLE = "LuaGame::LgControlNodeP";

static const char *LGBUTTON_HANDLE = "LuaGame::LgButtonP";

static const char *LGSCROLLVIEW_HANDLE = "LuaGame::LgScrollViewP";

static const char *LGSLIDER_HANDLE = "LuaGame::LgSliderP";

static const char *LGSWITCHER_HANDLE = "LuaGame::LgSwitcherP";

LgBool LgLuaApiInstall();

LgBool LgLuaIsObject(lua_State *L, int idx, const char *base);

void *LgLuaCheckObject(lua_State *L, int idx, const char *tname);

#endif