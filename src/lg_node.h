#ifndef _LG_NODE_H
#define _LG_NODE_H

#include "lg_basetypes.h"
#include "lg_refcnt.h"
#include "lg_cxform.h"
#include "lg_vector2.h"
#include "lg_affinetrans.h"
#include "lg_rendersystem.h"
#include "lg_radians.h"
#include "lg_touch.h"
#include "lg_grid.h"
#include "lg_avltree.h"
#include "lg_list.h"
#include "lg_rtree.h"

typedef struct LgNode *LgNodeP;
typedef struct LgAction *LgActionP;
typedef struct LgGestureRecognizer *LgGestureRecognizerP;

enum {
	LG_NODE_EVENT_SetCxform = 1,
	LG_NODE_EVENT_Cxform,
	LG_NODE_EVENT_SetPosition,
	LG_NODE_EVENT_Position,
	LG_NODE_EVENT_SetAnchor,
	LG_NODE_EVENT_Anchor,
	LG_NODE_EVENT_SetScale,
	LG_NODE_EVENT_Scale,
	LG_NODE_EVENT_SetRotate,
	LG_NODE_EVENT_Rotate,
	LG_NODE_EVENT_SetTransformAnchor,
	LG_NODE_EVENT_TransformAnchor,
	LG_NODE_EVENT_SetZorder,
	LG_NODE_EVENT_Zorder,
	LG_NODE_EVENT_AddChild,
	LG_NODE_EVENT_Remove,
	LG_NODE_EVENT_RemoveAllChildren,
	LG_NODE_EVENT_Visit,
	LG_NODE_EVENT_IsRunning,
	LG_NODE_EVENT_IsVisible,
	LG_NODE_EVENT_SetVisible,
	LG_NODE_EVENT_DoAction,
	LG_NODE_EVENT_StopActions,
	LG_NODE_EVENT_PauseActions,
	LG_NODE_EVENT_ResumeActions,
	LG_NODE_EVENT_LocalTransform,
	LG_NODE_EVENT_WorldTransform,
	LG_NODE_EVENT_PointToWorld,
	LG_NODE_EVENT_PointToLocal,
	LG_NODE_EVENT_Parent,
	LG_NODE_EVENT_Root,
	LG_NODE_EVENT_ChildByIndex,
	LG_NODE_EVENT_ChildrenCount,
	LG_NODE_EVENT_SetOpacity,
	LG_NODE_EVENT_SetGrid,
	LG_NODE_EVENT_Grid,
	LG_NODE_EVENT_ContentBox,
	LG_NODE_EVENT_Enter,
	LG_NODE_EVENT_Exit,
	LG_NODE_EVENT_Transform,
	LG_NODE_EVENT_DoHitTest,
	LG_NODE_EVENT_HitTest,
	LG_NODE_EVENT_Draw,
	LG_NODE_EVENT_AfterDraw,
	LG_NODE_EVENT_Create,
	LG_NODE_EVENT_Destroy,
	LG_NODE_EVENT_ActionsCount,
	LG_NODE_EVENT_IsLayer,
	LG_NODE_EVENT_ClearSlots,
	LG_NODE_EVENT_FireTouchEvent,
	LG_NODE_EVENT_HandleTouch,
	LG_NODE_EVENT_SetVertexZ,
	LG_NODE_EVENT_VisitChildren,
	LG_NODE_EVENT_AddClass,
	LG_NODE_EVENT_RemoveClass,
	LG_NODE_EVENT_ToggleClass,
	LG_NODE_EVENT_HasClass,
	LG_NODE_EVENT_ClearClasses,
	LG_NODE_EVENT_Classes,
	LG_NODE_EVENT_Select,
	LG_NODE_EVENT_Bounds,
	LG_NODE_EVENT_EnableRTree,
	LG_NODE_EVENT_RTreeSearchByRect,
	LG_NODE_EVENT_RTreeSearchByRadius,
	LG_NODE_EVENT_SetId,
	LG_NODE_EVENT_Id,
	LG_NODE_EVENT_BatchInfo,
	LG_NODE_EVENT_IsClipTouch,

	LG_NODE_EVENT_RemoveChildNotify,
	LG_NODE_EVENT_AddChildNotify,
	
	LG_NODE_EVENT_LAST,
};

enum LgNodeHitTestMode {
	LgNodeHitTestNone,
	LgNodeHitTestSimple,
	LgNodeHitTestExact,
};

enum LgNodeType_ {
	LgNodeTypeEntry,
	LgNodeTypeGroup,
};

LG_REFCNT_FUNCTIONS_DECL(LgNode)

LgNodeP LgNodeCreateR(LgEvent eventProc);
	
typedef struct LgNodeEventChildByIndex {
	int		i;
	LgNodeP node;
}*LgNodeEventChildByIndexP;

typedef struct LgNodeEventHitTest {
	LgNodeP				node;
	struct LgVector2	pt, localPosition;
}*LgNodeEventHitTestP;

typedef struct LgNodeEventDoHitTest {
	LgNodeP				node;
	struct LgVector2	localPosition;
	LgBool				hitted;
}*LgNodeEventDoHitTestP;

typedef struct LgNodeEventHasClass {
	const char *	className;
	LgBool			result;
}*LgNodeEventHasClassP;

enum LgNodeTouchType {
	LgNodeTouchBegan,
	LgNodeTouchMoved,
	LgNodeTouchEnded,
	LgNodeTouchCancelled,
};

typedef struct LgNodeTouchEvent {
	enum LgNodeTouchType	type;
	int						num;
	LgTouchP *				touches;
}*LgNodeTouchEventP;

typedef struct LgNodeEventSelect {
	const char *className;
	LgBool recursive;
	LgListP result;
}*LgNodeEventSelectP;

typedef struct LgNodeEventRTreeSearchByRect {
	LgAabbP aabb;
	LgBool inRect;
	LgRTreeSearchCallback callback;
	void *userdata;
	int result;
}*LgNodeEventRTreeSearchByRectP;

typedef struct LgNodeEventRTreeSearchByRadius {
	LgVector2P center;
	float radius;
	LgRTreeSearchCallback callback;
	void *userdata;
	int result;
}*LgNodeEventRTreeSearchByRadiusP;

typedef struct LgBatchInfo {
	LgTextureP texture;
	struct {
		struct LgVector2 lt, rt, lb, rb;
	}drawQuad;
	struct LgRect textureRt;
}*LgBatchInfoP;

typedef struct LgNodeEventBatchInfo {
	LgBool				supported;
	struct LgBatchInfo	batchInfo;
}*LgNodeEventBatchInfoP;

int LgNodeCount();

void LgNodeDoEvent(LgEventType type, LgNodeP node, void *param);

void LgNodeDefEventProc(LgEventType type, LgNodeP node, void *param);

LgEvent LgNodeEventProc(LgNodeP node);

void LgNodeSetEventProc(LgNodeP node, LgEvent eventProc);

void *LgNodeUserData(LgNodeP node);

void LgNodeSetUserData(LgNodeP node, void *userdata);

void LgNodeSetCxform(LgNodeP node, const LgCxformP cxform);

void LgNodeCxform(LgNodeP node, LgCxformP cxform);

void LgNodeSetPosition(LgNodeP node, const LgVector2P position);

void LgNodePosition(LgNodeP node, LgVector2P position);

void LgNodeSetAnchor(LgNodeP node, const LgVector2P anchor);

void LgNodeAnchor(LgNodeP node, LgVector2P anchor);

void LgNodeSetScale(LgNodeP node, const LgVector2P scale);

void LgNodeScale(LgNodeP node, LgVector2P scale);

void LgNodeSetRotate(LgNodeP node, float rotateRadians);

float LgNodeRotate(LgNodeP node);

void LgNodeSetTransformAnchor(LgNodeP node, const LgVector2P transformAnchor);

void LgNodeTransformAnchor(LgNodeP node, LgVector2P transformAnchor);

void LgNodeSetZorder(LgNodeP node, int zorder);

int LgNodeZorder(LgNodeP node);

void LgNodeAddChild(LgNodeP node, LgNodeP child);

void LgNodeRemove(LgNodeP node);

void LgNodeRemoveAllChildren(LgNodeP node);

void LgNodeVisit(LgNodeP node, float delta);

LgBool LgNodeIsRunning(LgNodeP node);

LgBool LgNodeIsVisible(LgNodeP node);

void LgNodeSetVisible(LgNodeP node, LgBool visible);

void LgNodeDoAction(LgNodeP node, LgActionP action);

void LgNodeDoActionAndRelease(LgNodeP node, LgActionP action);

void LgNodeStopActions(LgNodeP node);

void LgNodePauseActions(LgNodeP node);

void LgNodeResumeActions(LgNodeP node);

void LgNodeLocalTransform(LgNodeP node, LgAffineTransformP af);

void LgNodeWorldTransform(LgNodeP node, LgAffineTransformP af);

void LgNodePointToWorld(LgNodeP node, LgVector2P pt);

void LgNodePointToLocal(LgNodeP node, LgVector2P pt);

LgNodeP LgNodeParent(LgNodeP node);

LgNodeP LgNodeRoot(LgNodeP node);

LgNodeP LgNodeChildByIndex(LgNodeP node, int index);

int LgNodeChildrenCount(LgNodeP node);

void LgNodeSetOpacity(LgNodeP node, float opacity);

void LgNodeSetGrid(LgNodeP node, LgGridP grid);

LgGridP LgNodeGrid(LgNodeP node);

LgBool LgNodeHitTestIsEnabled(LgNodeP node);

void LgNodeEnableHitTest(LgNodeP node, LgBool value);

void LgNodeContentBox(LgNodeP node, LgAabbP aabb);

LgNodeP LgNodeHitTest(LgNodeP node, const LgVector2P pt, LgVector2P localPosition);

int LgNodeActionsCount(LgNodeP node);

LgBool LgNodeIsLayer(LgNodeP node);

LgSlot *LgNodeSlotTouch(LgNodeP node);

LgSlot *LgNodeSlotEnter(LgNodeP node);

LgSlot *LgNodeSlotExit(LgNodeP node);

LgSlot *LgNodeSlotGesture(LgNodeP node);

void LgNodeHandleTouch(LgNodeP node, const LgNodeTouchEventP evt);

void LgNodeAddGestureRecognizer(LgNodeP node, LgGestureRecognizerP gr);

void LgNodeSetVertexZ(LgNodeP node, float z);

void LgNodeAddClass(LgNodeP node, const char *className);

void LgNodeRemoveClass(LgNodeP node, const char *className);

void LgNodeToggleClass(LgNodeP node, const char *className);

LgBool LgNodeHasClass(LgNodeP node, const char *className);

void LgNodeClearClasses(LgNodeP node);

LgAvlTreeP LgNodeClasses(LgNodeP node);

void LgNodeSelect(LgNodeP node, const char *className, LgBool recursive, LgListP result);

void LgNodeBounds(LgNodeP node, LgAabbP aabb);

void LgNodeEnableRTree(LgNodeP node, LgBool enable);

int LgNodeRTreeSearchByRect(LgNodeP node, const LgAabbP aabb, LgBool inRect, LgRTreeSearchCallback callback, void *userdata);

int LgNodeRTreeSearchByRadius(LgNodeP node, const LgVector2P center, float radius, LgRTreeSearchCallback callback, void *userdata);

void LgNodeSetId(LgNodeP node, const char *id);

const char *LgNodeId(LgNodeP node);

LgAvlTreeP LgNodeTouches(LgNodeP node);

int LgNodeTouchesCount(LgNodeP node);

void LgNodeEnableMultipleTouch(LgNodeP node, LgBool enable);

LgBool LgNodeBatchInfo(LgNodeP node, LgBatchInfoP batchInfo);

void LgNodeCenterAnchor(LgNodeP node);

void LgNodeCenterTransformAnchor(LgNodeP node);

void LgNodeCenter(LgNodeP node);

void LgNodeSetTouchBubble(LgNodeP node, LgBool value);

LgBool LgNodeIsTouchBubble(LgNodeP node);

#endif
