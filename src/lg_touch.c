#include "lg_touch.h"
#include "lg_memory.h"
#include "lg_node.h"

struct LgTouch {
	LG_REFCNT_HEAD
	int					id;
	LgNodeP				downNode, currentNode;
	struct LgVector2	point, prevPoint;
	LgTime				downTime;
};

LG_REFCNT_FUNCTIONS(LgTouch)

static void destroy(LgTouchP touch) {
	if (touch->downNode) LgNodeRelease(touch->downNode);
	if (touch->currentNode) LgNodeRelease(touch->currentNode);
	LgFree(touch);
}

static void init(LgTouchP touch, LgNodeP node, const LgVector2P pt) {
	if (touch->downNode) LgNodeRelease(touch->downNode);
	if (touch->currentNode) LgNodeRelease(touch->currentNode);
	touch->downNode = LgNodeRetain(node);
	touch->currentNode = LgNodeRetain(node);
	touch->downTime = LgGetTimeOfDay();
	if (pt) {
		LgCopyMemory(&touch->point, pt, sizeof(struct LgVector2));
		LgCopyMemory(&touch->prevPoint, pt, sizeof(struct LgVector2));
	} else {
		touch->point.x = touch->point.y = 0;
		touch->prevPoint.x = touch->prevPoint.y = 0;
	}
}

LgTouchP LgTouchCreateR(int id, LgNodeP node, const LgVector2P pt) {
	LgTouchP touch = (LgTouchP)LgMallocZ(sizeof(struct LgTouch));
	LG_REFCNT_INIT(touch, destroy);
	touch->id = id;
	init(touch, node, pt);
	return touch;
}

void LgTouchReplace(LgTouchP touch, LgNodeP node, const LgVector2P pt) {
	if (touch->currentNode) LgNodeRelease(touch->currentNode);
	touch->currentNode = LgNodeRetain(node);
	LgCopyMemory(&touch->prevPoint, &touch->point, sizeof(struct LgVector2));
	LgCopyMemory(&touch->point, pt, sizeof(struct LgVector2));
}

void LgTouchLocalPosition(LgTouchP touch, LgVector2P pt) {
	LgCopyMemory(pt, &touch->point, sizeof(struct LgVector2));
	LgNodePointToLocal(touch->downNode, pt);
}

void LgTouchLocalPreviousPosition(LgTouchP touch, LgVector2P pt) {
	LgCopyMemory(pt, &touch->prevPoint, sizeof(struct LgVector2));
	LgNodePointToLocal(touch->downNode, pt);
}

void LgTouchWorldPosition(LgTouchP touch, LgVector2P pt) {
	LgCopyMemory(pt, &touch->point, sizeof(struct LgVector2));
}

void LgTouchWorldPreviousPosition(LgTouchP touch, LgVector2P pt) {
	LgCopyMemory(pt, &touch->prevPoint, sizeof(struct LgVector2));
}

LgNodeP LgTouchDownNode(LgTouchP touch) {
	return touch->downNode;
}

LgNodeP LgTouchCurrentNode(LgTouchP touch) {
	return touch->currentNode;
}

void LgTouchDelta(LgTouchP touch, LgVector2P delta) {
	LgCopyMemory(delta, &touch->point, sizeof(struct LgVector2));
	LgVector2Sub(delta, &touch->prevPoint);
}

int LgTouchId(LgTouchP touch) {
	return touch->id;
}

LgTime LgTouchDownTime(LgTouchP touch) {
	return LgGetTimeOfDay() - touch->downTime;
}
