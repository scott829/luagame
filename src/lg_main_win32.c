#include "lg_init.h"
#include <windows.h>

int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	char *argv[256], *token;
	int argc = 0;
	MSG msg;

	// 转换命令行参数为标准形式
	token = strtok(lpCmdLine, " ");
	while(token) {
		argv[argc++] = strdup(token);
		token = strtok(NULL, " ");
	}

	if (!LgInit(argc, argv)) return -1;

	// 释放命令行参数
	while(argc > 0) {
		free(argv[argc - 1]);
		argc--;
	}
	
	while(LgTrue) {
		if (!PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (!LgMainLoop()) break;
		}
		else {
			if (msg.message == WM_QUIT) break;
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	LgCleanup();
	return 0;
}