#include "lg_memory.h"

void *LgMalloc(size_t size) {
	return malloc(size);
}

void *LgMallocZ(size_t size) {
	void *p = malloc(size);
	memset(p, 0, size);
	return p;
}

void LgFree(void *p) {
	free(p);
}

void *LgCalloc(size_t size, int n) {
	return LgMalloc(size * n);
}

void *LgRealloc(void *p, size_t size) {
	return realloc(p, size);
}
