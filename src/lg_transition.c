#include "lg_transition.h"
#include "lg_memory.h"
#include "lg_layer.h"
#include "lg_director.h"
#include "lg_action.h"
#include "lg_scheduler.h"

static void transitionEventProc(LgEventType type, LgNodeP node, void *param) {
	if (type == LG_NODE_EVENT_HitTest) {
		// 切换场景不允许点击测试
		return;
	}

	LgSceneEventProc(type, node, param);
}

static void endTransition(void *param, void *userdata) {
	LgNodeP inScene = (LgNodeP)userdata;
	LgNodeDoEvent(LG_NODE_EVENT_Enter, inScene, NULL);
	LgDirectorReplace(inScene);
	LgNodeDoEvent(LG_NODE_EVENT_Exit, inScene, NULL);
}

// 创建切换场景，切换动作必须是interval类型
void LgTransition(LgNodeP scene, LgActionP inAction, LgActionP outAction) {
	LgNodeP transScene, inScene, outScene, inLayer, outLayer;

	if (!inAction && !outAction) return;
	
	// 创建进入图层
	inScene = scene;
	
	inLayer = LgLayerCreateR();
	LgNodeAddChild(inLayer, scene);
	LgNodeSetZorder(inLayer, 0);
	
	outScene = LgDirectorCurrentScene();
	
	outLayer = LgLayerCreateR();
	LgNodeAddChild(outLayer, outScene);
	LgNodeSetZorder(outLayer, 1);
	
	// 创建切换场景
	transScene = LgNodeCreateR((LgEvent)transitionEventProc);
	LgNodeAddChild(transScene, inLayer);
	LgNodeRelease(inLayer);
	LgNodeAddChild(transScene, outLayer);
	LgNodeRelease(outLayer);

	// 执行切换动作
	if (inAction) {
		LgActionP inseq[2];
		LgActionP seq;
		inseq[0] = inAction;
		inseq[1] = LgActionCallCreate(endTransition, NULL, NULL, inScene);
		seq = LgActionSequenceCreate(inseq, 2);
		LgActionRelease(inseq[1]);
		LgNodeDoAction(inLayer, seq);
		LgActionRelease(seq);
		if (outAction) LgNodeDoAction(outLayer, outAction);
	} else {
		LgActionP outseq[2];
		LgActionP seq;
		outseq[0] = outAction;
		outseq[1] = LgActionCallCreate(endTransition, NULL, NULL, inScene);
		seq = LgActionSequenceCreate(outseq, 2);
		LgActionRelease(outseq[1]);
		LgNodeDoAction(outLayer, seq);
		LgActionRelease(seq);
	}

	LgDirectorReplace(transScene);
	LgNodeRelease(transScene);
}
