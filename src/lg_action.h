﻿#ifndef _LG_ACTION_H
#define _LG_ACTION_H

#include "lg_time.h"
#include "lg_node.h"
#include "lg_list.h"

typedef struct LgAction *LgActionP;

struct LgActionImpl {
	void (* destroy)(LgActionP action);
	LgBool (* update)(LgActionP action, LgTime delta);
	void (* reset)(LgActionP action);
	void (* stop)(LgActionP action);
	void (* attachNode)(LgActionP action, LgNodeP node);
	LgActionP (* clone)(LgActionP action);
};

struct LgAction {
	LG_REFCNT_HEAD
	struct LgActionImpl *	impl;
	LgBool					paused, running;
	LgNodeP					node;
	LgBool					deleted;
	LgListNodeP				newItemsNode; // 如果在新建项列表内，则此处保存新建项节点
};

LG_REFCNT_FUNCTIONS_DECL(LgAction);

#define LgActionIsPaused(action) (((LgActionP)action)->paused)

#define LgActionPause(action) ((LgActionP)action)->paused = LgTrue

#define LgActionResume(action) ((LgActionP)action)->paused = LgFalse

#define LgActionIsRunning(action) (((LgActionP)action)->running)

#define LgActionAttachNode(action, node) if (((LgActionP)action)->impl->attachNode) (((LgActionP)action)->impl->attachNode((LgActionP)action, node))

#define LgActionUpdate(action, delta) (((LgActionP)action)->impl->update((LgActionP)action, delta))

#define LgActionStop(action) if (((LgActionP)action)->impl->stop) ((LgActionP)action)->impl->stop((LgActionP)action)

#define LgActionReset(action) if (((LgActionP)action)->impl->reset) ((LgActionP)action)->impl->reset((LgActionP)action)

#define LgActionClone(action) (((LgActionP)action)->impl->clone((LgActionP)action))

#define LgActionNode(action) (((LgActionP)action)->node)

#define LgActionDeleted(action)(((LgActionP)action)->deleted)

#define LgActionNewItemNode(action) (((LgActionP)action)->newItemsNode)

void LgActionDoStop(LgActionP action);

int LgActionCount();

// ------------------------------------------------------------------------------------------------------------

LgActionP LgActionLoopCreate(LgActionP action, int times);

LgActionP LgActionLoopForeverCreate(LgActionP action);

LgActionP LgActionSequenceCreate(LgActionP *actions, int count);

LgActionP LgActionCallCreate(SLOT_PARAMS);

LgActionP LgActionLuaIntervalCreate(LgTime duration, int func, int context);

LgActionP LgActionCameraCreate(LgTime duration, float centerX, float centerY, float scale);

LgActionP LgActionMoveToCreate(LgTime duration, const LgVector2P position);

LgActionP LgActionMoveByCreate(LgTime duration, const LgVector2P delta);

LgActionP LgActionBezierToCreate(LgTime duration, const LgVector2P cp1, const LgVector2P cp2, const LgVector2P endPoint);

LgActionP LgActionBezierByCreate(LgTime duration, const LgVector2P Rcp1, const LgVector2P Rcp2, const LgVector2P RendPoint);

LgActionP LgActionScaleToCreate(LgTime duration, const LgVector2P scale);

LgActionP LgActionScaleByCreate(LgTime duration, const LgVector2P delta);

LgActionP LgActionRotateToCreate(LgTime duration, float d);

LgActionP LgActionRotateByCreate(LgTime duration, float delta);

LgActionP LgActionBlinkCreate(LgTime duration, int blinks);

LgActionP LgActionFadeInCreate(LgTime duration);

LgActionP LgActionFadeOutCreate(LgTime duration);

LgActionP LgActionDelayCreate(LgTime duration);

LgActionP LgActionJumpCreate(LgTime duration, const LgVector2P offset, float height, int jumps);

LgActionP LgActionSpeedCreate(LgActionP action, float speed);

LgActionP LgActionRangeCreate(LgActionP action, float start, float end);

LgActionP LgActionSpawnCreate(LgActionP *actions, int count);

LgActionP LgActionEaseInCreate(LgActionP action, float rate);

LgActionP LgActionEaseOutCreate(LgActionP action, float rate);

LgActionP LgActionEaseInOutCreate(LgActionP action, float rate);

LgActionP LgActionEaseExponentialInCreate(LgActionP action, float rate);

LgActionP LgActionEaseExponentialOutCreate(LgActionP action, float rate);

LgActionP LgActionEaseExponentialInOutCreate(LgActionP action, float rate);

LgActionP LgActionEaseSineInCreate(LgActionP action, float rate);

LgActionP LgActionEaseSineOutCreate(LgActionP action, float rate);

LgActionP LgActionEaseSineInOutCreate(LgActionP action, float rate);

LgActionP LgActionEaseElasticInCreate(LgActionP action, float period);

LgActionP LgActionEaseElasticOutCreate(LgActionP action, float period);

LgActionP LgActionEaseElasticInOutCreate(LgActionP action, float period);

LgActionP LgActionEaseBounceInCreate(LgActionP action);

LgActionP LgActionEaseBounceOutCreate(LgActionP action);

LgActionP LgActionEaseBounceInOutCreate(LgActionP action);

LgActionP LgActionEaseBackInCreate(LgActionP action);

LgActionP LgActionEaseBackOutCreate(LgActionP action);

LgActionP LgActionEaseBackInOutCreate(LgActionP action);

LgActionP LgActionWave3DCreate(LgTime duration, int cols, int rows, int waves, float amplitude);

LgActionP LgActionLens3DCreate(LgTime duration, int cols, int rows, const LgVector2P center, float radius, float lensEffect);

LgActionP LgActionRipple3DCreate(LgTime duration, int cols, int rows, const LgVector2P center, float radius, int waves, float amplitude);

LgActionP LgActionLiquid3DCreate(LgTime duration, int cols, int rows, int waves, float amplitude);

LgActionP LgActionShakyYCreate(LgTime duration, int cols, int rows, int range, LgBool shakeZ);

LgActionP LgActionFadeOutRBCreate(LgTime duration, int cols, int rows);

LgActionP LgActionFadeOutLTCreate(LgTime duration, int cols, int rows);

LgActionP LgActionFadeOutUPCreate(LgTime duration, int cols, int rows);

LgActionP LgActionFadeOutDOWNCreate(LgTime duration, int cols, int rows);

LgActionP LgActionSplitColsCreate(LgTime duration, int cols);

LgActionP LgActionSplitRowsCreate(LgTime duration, int rows);

#endif
