#ifndef _LG_MEMORY_H
#define _LG_MEMORY_H

#include "lg_basetypes.h"
#include <string.h>

void *LgMalloc(size_t size);

void *LgMallocZ(size_t size);

void LgFree(void *p);

void *LgCalloc(size_t size, int n);

void *LgRealloc(void *p, size_t size);

#define LgZeroMemory(p, size) memset(p, 0, size)

#define LgCopyMemory(dest, src, size) memcpy(dest, src, size)

#define LgMoveMemory(dest, src, size) memmove(dest, src, size)

#endif