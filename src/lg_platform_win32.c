#include "lg_platform.h"
#include <windows.h>
#include <shlobj.h>

const char *LgPlatformResourceDirectory() {
	static char path[MAX_PATH];
	GetModuleFileName(GetModuleHandle(NULL), path, sizeof(path));
	*(strrchr(path, '\\') + 1) = 0;
	return path;
}

const char *LgPlatformPathSeparator() {
	return "\\";
}

const char *LgPlatformDocumentDirectory() {
	static char path[MAX_PATH];

	SHGetSpecialFolderPath(NULL, path, CSIDL_PROFILE, TRUE);
	strcat(path, "\\");
	return path;
}

const char *LgCurrentPlatform() {
	return "win32";
}
