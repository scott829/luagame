﻿#include "lg_label.h"
#include "lg_fontsystem.h"
#include "lg_platform.h"
#include "lg_director.h"
#include "lg_util.h"

typedef struct LgLabelData {
	char *					text;		// utf8
	wchar_t *				wideText;
	LgFontP					font;
	int						fontSize;
	enum LgDrawStringFlag	drawFlags;
	float					width;
	LgColor					color;
	LgDrawBufferP			drawBuffer;
}*LgLabelDataP;

static void clearBuffer(LgLabelDataP ld) {
	if (ld->drawBuffer) {
		LgFontGetDriver(ld->font)->destroyStringBuffer(ld->drawBuffer);
		ld->drawBuffer = NULL;
		ld->width = 0;
	}
}

static void labelEventProc(LgEventType type, LgNodeP node, void *param) {
	switch(type) {
	case LG_NODE_EVENT_Destroy:
		{
			LgLabelDataP ld = (LgLabelDataP)LgNodeUserData(node);
			clearBuffer(ld);
			if (ld->text) LgFree(ld->text);
			if (ld->wideText) LgFree(ld->wideText);
			LgFree(ld);
		}
		break;
	case LG_NODE_EVENT_Draw:
		{
			LgLabelDataP ld = (LgLabelDataP)LgNodeUserData(node);

			if (!ld->drawBuffer && ld->font && ld->text) {
				ld->drawBuffer = LgFontGetDriver(ld->font)->createStringBuffer(LgFontGetData(ld->font), ld->fontSize, ld->wideText, -1);
				ld->width = LgFontGetDriver(ld->font)->stringWidth(LgFontGetData(ld->font), ld->fontSize, ld->wideText, -1);
			}

			if (ld->drawBuffer) {
				LgFontGetDriver(ld->font)->draw(ld->drawBuffer, 0, 0, ld->color, ld->drawFlags);
				LgDirectorUpdateBatch(LgFontGetDriver(ld->font)->numberOfbatch(ld->drawBuffer));
			}
		}
		break;
	case LG_NODE_EVENT_Exit:
		{
			LgLabelDataP ld = (LgLabelDataP)LgNodeUserData(node);
			clearBuffer(ld);
		}
		break;
	case LG_NODE_EVENT_ContentBox:
		{
			LgLabelDataP ld = (LgLabelDataP)LgNodeUserData(node);
			struct LgAabb contentBox;

			// 在获取之前更新contentBox
			contentBox.x = contentBox.y = 0;
			contentBox.width = 0;
			if (ld->drawBuffer) {
				contentBox.width = ld->width;
			} else if (ld->font) {
				contentBox.width = ld->width = LgFontGetDriver(ld->font)->stringWidth(LgFontGetData(ld->font), ld->fontSize, ld->wideText, -1);
			}
			contentBox.height = ld->font ? LgFontGetDriver(ld->font)->height(LgFontGetData(ld->font), ld->fontSize) : 0;
			*((LgAabbP)param) = contentBox;
		}
		return;
	case LG_LABEL_EVENT_SetFontName:
		{
			LgLabelDataP ld = (LgLabelDataP)LgNodeUserData(node);
			ld->font = LgFontSystemFindFont((const char *)param);
			clearBuffer(ld);
		}
		break;
	case LG_LABEL_EVENT_FontName:
		{
			LgLabelDataP ld = (LgLabelDataP)LgNodeUserData(node);
			*((const char **)param) = LgFontGetAlias(ld->font);
		}
		break;
	case LG_LABEL_EVENT_SetFontSize:
		{
			LgLabelDataP ld = (LgLabelDataP)LgNodeUserData(node);
			if (ld->fontSize != *((int *)param)) {
				ld->fontSize = *((int *)param);
				clearBuffer(ld);
			}
		}
		break;
	case LG_LABEL_EVENT_FontSize:
		{
			LgLabelDataP ld = (LgLabelDataP)LgNodeUserData(node);
			*((int *)param) = ld->fontSize;
		}
		break;
	case LG_LABEL_EVENT_SetColor:
		{
			LgLabelDataP ld = (LgLabelDataP)LgNodeUserData(node);
			ld->color = *((LgColor *)param);
		}
		break;
	case LG_LABEL_EVENT_Color:
		{
			LgLabelDataP ld = (LgLabelDataP)LgNodeUserData(node);
			*((LgColor *)param) = ld->color;
		}
		break;
	case LG_LABEL_EVENT_SetText:
		{
			LgLabelDataP ld = (LgLabelDataP)LgNodeUserData(node);

			if (!param) param = "";
			if (!ld->text || strcmp(ld->text, (const char *)param) != 0) {
				if (ld->text) LgFree(ld->text);
				ld->text = strdup((const char *)param);
				if (ld->wideText) LgFree(ld->wideText);
				ld->wideText = LgUtf8ToWide(ld->text, -1);
				clearBuffer(ld);
			}
		}
		break;
	case LG_LABEL_EVENT_TextLength:
		{
			LgLabelDataP ld = (LgLabelDataP)LgNodeUserData(node);
			*((int *)param) = ld->text ? strlen(ld->text) : 0;
		}
		break;
	case LG_LABEL_EVENT_Text:
		{
			LgLabelDataP ld = (LgLabelDataP)LgNodeUserData(node);
			LgLabelEventGetTextP getText = (LgLabelEventGetTextP)param;
			strlcpy(getText->buf, ld->text, getText->bufLength);
		}
		break;
	case LG_LABEL_EVENT_SetDrawFlags:
		{
			LgLabelDataP ld = (LgLabelDataP)LgNodeUserData(node);
			ld->drawFlags = (enum LgDrawStringFlag)param;
		}
		break;
	case LG_LABEL_EVENT_DrawFlags:
		{
			LgLabelDataP ld = (LgLabelDataP)LgNodeUserData(node);
			*((int *)param) = (int)ld->drawFlags;
		}
		break;
	case LG_LABEL_EVENT_Font:
		{
			LgLabelDataP ld = (LgLabelDataP)LgNodeUserData(node);
			*((LgFontP *)param) = ld->font;
		}
		break;
	}

	LgNodeDefEventProc(type, node, param);
}

LgNodeP LgLabelCreateR(const char *text) {
	LgNodeP node;
	LgLabelDataP ld;

	node = LgNodeCreateR((LgEvent)labelEventProc);
	LG_CHECK_ERROR(node);

	ld = (LgLabelDataP)LgMallocZ(sizeof(struct LgLabelData));
	LG_CHECK_ERROR(ld);
	LgNodeSetUserData(node, ld);

	ld->font = LgFontSystemFindFont("_default");
	ld->fontSize = 24;
	ld->color = LgColorXRGB(255, 255, 255);
	
	if (text && strlen(text) > 0) LgLabelSetText(node, text);

	return node;

_error:
	if (node) LgNodeRelease(node);
	return NULL;
}

void LgLabelSetFontName(LgNodeP node, const char *fontname) {
	LgNodeDoEvent(LG_LABEL_EVENT_SetFontName, node, (void *)fontname);
}

const char *LgLabelFontName(LgNodeP node) {
	char *name;
	LgNodeDoEvent(LG_LABEL_EVENT_FontName, node, (void *)&name);
	return name;
}

void LgLabelSetFontSize(LgNodeP node, int fontsize) {
	LgNodeDoEvent(LG_LABEL_EVENT_SetFontSize, node, &fontsize);
}

int LgLabelFontSize(LgNodeP node) {
	int size;
	LgNodeDoEvent(LG_LABEL_EVENT_FontSize, node, &size);
	return size;
}

void LgLabelSetColor(LgNodeP node, LgColor color) {
	LgNodeDoEvent(LG_LABEL_EVENT_SetColor, node, &color);
}

LgColor LgLabelColor(LgNodeP node) {
	LgColor color;
	LgNodeDoEvent(LG_LABEL_EVENT_Color, node, &color);
	return color;
}

void LgLabelSetText(LgNodeP node, const char *text) {
	LgNodeDoEvent(LG_LABEL_EVENT_SetText, node, (void *)text);
}

int LgLabelTextLength(LgNodeP node) {
	int len;
	LgNodeDoEvent(LG_LABEL_EVENT_TextLength, node, &len);
	return len;
}

void LgLabelText(LgNodeP node, char *buf, int bufLen) {
	struct LgLabelEventGetText getText;
	getText.buf = buf;
	getText.bufLength = bufLen;
	LgNodeDoEvent(LG_LABEL_EVENT_Text, node, &getText);
}

void LgLabelSetDrawFlag(LgNodeP node, int flags) {
	LgNodeDoEvent(LG_LABEL_EVENT_SetDrawFlags, node, (void *)flags);
}

int LgLabelDrawFlag(LgNodeP node) {
	int flags;
	LgNodeDoEvent(LG_LABEL_EVENT_DrawFlags, node, &flags);
	return flags;
}

LgFontP LgLabelFont(LgNodeP node) {
	LgFontP font;
	LgNodeDoEvent(LG_LABEL_EVENT_Font, node, &font);
	return font;
}

LgBool LgNodeIsLabel(LgNodeP node) {
	return LgNodeEventProc(node) == (LgEvent)labelEventProc;
}
