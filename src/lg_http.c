﻿#include "lg_http.h"
#include "lg_dict.h"
#include "lg_memory.h"
#include "lg_platform.h"
#include "lg_url.h"
#include "regexp/pcre.h"
#include <stdio.h>
#include "event2/event.h"
#include "event2/util.h"
#include "event2/bufferevent.h"
#include "event2/buffer.h"
#include "lg_init.h"

typedef struct LgDecoder *LgDecoderP;

struct LgHttp {
	LG_REFCNT_HEAD
	evutil_socket_t		s;
	struct bufferevent *bev;
	LgUrlP				urlComponent;
	enum LgHttpMethod	method;
	void *				postData;
	int					postLength, postPosition;
	enum LgHttpState	state;
	LgDictP				requestFields, responseFields;
	int					responseCode;
	LgSlot				slotStateChanged, slotResponseData, slotError;
	int					contentLength, needResponseDataLength;
	LgDecoderP			decoder;
};

LG_REFCNT_FUNCTIONS(LgHttp)

// base decoder ------------------------------------------------------------------------
struct LgDecoder {
	LgHttpP http;
	LgBool (* decode)(struct LgDecoder *decoder, const char *data, int size);
	LgDestroy destroy;
};

static void LgDecoderDestroy(LgDecoderP decoder) {
	if (decoder->destroy) decoder->destroy(decoder);
	LgFree(decoder);
}

// normal decoder ------------------------------------------------------------------------

static LgBool normalDecode(struct LgDecoder *decoder, const char *data, int size) {
	struct LgHttpResponseDataEvent responseDataEvt;
	LgHttpP http = decoder->http;

	if (http->needResponseDataLength == -1) {
		responseDataEvt.finished = LgFalse;
		responseDataEvt.data = data;
		responseDataEvt.size = size;
		responseDataEvt.ratio = 0;
		LgSlotCall(&http->slotResponseData, &responseDataEvt);
	} else {
		int rsz = LgMin(http->needResponseDataLength, size);
		responseDataEvt.data = data;
		responseDataEvt.size = rsz;
		http->needResponseDataLength -= rsz;
		responseDataEvt.finished = http->needResponseDataLength == 0;
		responseDataEvt.ratio = ((float)(http->contentLength - http->needResponseDataLength)) / http->contentLength;
		LgSlotCall(&http->slotResponseData, &responseDataEvt);
	}

	return responseDataEvt.finished;
}

static LgDecoderP createNormalDecoder(LgHttpP http) {
	LgDecoderP decoder = (LgDecoderP)LgMallocZ(sizeof(struct LgDecoder));
	decoder->http = http;
	decoder->decode = normalDecode;
	return decoder;
}

// chunked decoder ------------------------------------------------------------------------
enum LgChunkedDecoderState {
	LgChunkedDecoderReadingHeader,
	LgChunkedDecoderReadingBody,
	LgChunkedDecoderReadingBodyTail,
};

typedef struct LgChunkedDecoder {
	struct LgDecoder			base;
	enum LgChunkedDecoderState	state;
	int							chunkSize, totalChunkSize;
	char						chunked[64];
	int							chunkedLen;
}*LgChunkedDecoderP;

static LgBool chunkedDecode(struct LgDecoder *decoder, const char *data, int size) {
	const char *p = data;
	LgHttpP http = decoder->http;
	LgChunkedDecoderP chunkedDecoder = (LgChunkedDecoderP)decoder;

	while(p < data + size) {
		if (chunkedDecoder->state == LgChunkedDecoderReadingHeader) {
			if (*p == '\n') {
				chunkedDecoder->state = LgChunkedDecoderReadingBody;
				sscanf(chunkedDecoder->chunked, "%x", &chunkedDecoder->chunkSize);
				chunkedDecoder->totalChunkSize = chunkedDecoder->chunkSize;
				chunkedDecoder->chunkedLen = 0;
				if (chunkedDecoder->chunkSize == 0) {
					// 没有了
					struct LgHttpResponseDataEvent responseDataEvt;
					responseDataEvt.data = NULL;
					responseDataEvt.size = 0;
					responseDataEvt.finished = LgTrue;
					responseDataEvt.ratio = 1.0f;
					LgSlotCall(&http->slotResponseData, &responseDataEvt);
					return LgTrue;
				}
				p++;
			} else if (*p == '\r') {
				p++;
			} else {
				chunkedDecoder->chunked[chunkedDecoder->chunkedLen] = *p;
				chunkedDecoder->chunked[chunkedDecoder->chunkedLen + 1] = 0;
				chunkedDecoder->chunkedLen++, p++;
			}
		} else if (chunkedDecoder->state == LgChunkedDecoderReadingBody) {
			int wsz;
			struct LgHttpResponseDataEvent responseDataEvt;

			wsz = LgMin(chunkedDecoder->chunkSize, (int)(data + size - p));
			responseDataEvt.data = p;
			responseDataEvt.size = wsz;
			responseDataEvt.finished = LgFalse;
			responseDataEvt.ratio = (((float)(chunkedDecoder->totalChunkSize - chunkedDecoder->chunkSize)) / chunkedDecoder->totalChunkSize);
			LgSlotCall(&http->slotResponseData, &responseDataEvt);
			chunkedDecoder->chunkSize -= wsz;
			p += wsz;
			if (chunkedDecoder->chunkSize == 0) {
				chunkedDecoder->chunkedLen = 2;
				chunkedDecoder->state = LgChunkedDecoderReadingBodyTail;
			}
		} else if (chunkedDecoder->state == LgChunkedDecoderReadingBodyTail) {
			int wsz = LgMin(chunkedDecoder->chunkedLen, (int)(data + size - p));
			p += wsz;
			chunkedDecoder->chunkedLen -= wsz;
			if (chunkedDecoder->chunkedLen == 0) {
				chunkedDecoder->state = LgChunkedDecoderReadingHeader;
			}
		}
	}

	return LgFalse;
}

static LgDecoderP createChunkedDecoder(LgHttpP http) {
	LgChunkedDecoderP decoder = (LgChunkedDecoderP)LgMallocZ(sizeof(struct LgChunkedDecoder));
	decoder->base.http = http;
	decoder->base.decode = chunkedDecode;
	decoder->state = LgChunkedDecoderReadingHeader;
	return (LgDecoderP)decoder;
}

// decoder factory ------------------------------------------------------------------------

static LgDecoderP createDecoder(LgHttpP http) {
	const char *transferEncoding = (const char *)LgDictFind(http->responseFields, "Transfer-Encoding");
	if (transferEncoding && strcmp(transferEncoding, "chunked") == 0) return createChunkedDecoder(http);
	else return createNormalDecoder(http);
}

// http ------------------------------------------------------------------------

static LgBool openUrl(LgHttpP http, const char *url, enum LgHttpMethod method, const void *postData, int postLength);

static void changeState(LgHttpP http, enum LgHttpState newState) {
	if (http->state != newState) {
		struct LgHttpStateChangedEvent event;
		event.newState = newState;
		event.oldState = http->state;
		http->state = newState;
		LgSlotCall(&http->slotStateChanged, &event);
	}
}

static void close(LgHttpP http, enum LgHttpErrorType err) {
	if (err != LgHttpErrorNone) LgSlotCall(&http->slotError, (void *)err);

	if (http->urlComponent) {
		LgUrlDestroy(http->urlComponent);
		http->urlComponent = NULL;
	}
	if (http->bev) {
		bufferevent_free(http->bev);
		http->bev = NULL;
	}
	if (http->s) {
		evutil_closesocket(http->s);
		http->s = 0;
	}
	LgDictClear(http->responseFields);
	http->responseCode = 0;
	if (http->decoder) {
		LgDecoderDestroy(http->decoder);
		http->decoder = NULL;
	}
	changeState(http, LgHttpStateDisconnected);
}

static void destroyHttp(LgHttpP http) {
	close(http, LgHttpErrorNone);
	if (http->requestFields) LgDictDestroy(http->requestFields);
	if (http->responseFields) LgDictDestroy(http->responseFields);
	LgSlotClear(&http->slotError);
	LgSlotClear(&http->slotResponseData);
	LgSlotClear(&http->slotStateChanged);
	if (http->postData) LgFree(http->postData);
	LgFree(http);
}

LgHttpP LgHttpCreateR() {
	LgHttpP http = (LgHttpP)LgMallocZ(sizeof(struct LgHttp));
	LG_CHECK_ERROR(http);
	LG_REFCNT_INIT(http, destroyHttp);
	http->state = LgHttpStateDisconnected;
	http->requestFields = LgDictCreate((LgCompare)strnocasecmp, (LgDestroy)LgFree, (LgDestroy)LgFree);
	LG_CHECK_ERROR(http->requestFields);
	http->responseFields = LgDictCreate((LgCompare)strnocasecmp, (LgDestroy)LgFree, (LgDestroy)LgFree);
	LG_CHECK_ERROR(http->responseFields);
	return http;

_error:
	if (http) LgHttpRelease(http);
	return NULL;
}

static const char *methodStr(enum LgHttpMethod method) {
	switch(method) {
	case LgHttpMethodGet: return "GET";
	case LgHttpMethodPost: return "POST";
	default: return "GET";
	}
}

static void sendRequest(LgHttpP http) {
	char temp[512];
	changeState(http, LgHttpStateSendingRequestHeaders);

	if (http->method == LgHttpMethodGet) {
		sprintf(temp, "%s /%s HTTP/1.1\r\nHost: %s:%d\r\n\r\n",
			methodStr(http->method), http->urlComponent->path,
			http->urlComponent->host, http->urlComponent->port);
	} else {
		sprintf(temp, "%s /%s HTTP/1.1\r\nHost: %s:%d\r\nContent-Length: %d\r\n\r\n",
			methodStr(http->method), http->urlComponent->path,
			http->urlComponent->host, http->urlComponent->port,
			http->postLength);
	}

	bufferevent_write(http->bev, temp, strlen(temp));
}

static void socketEvent(struct bufferevent *bev, short what, void *arg) {
	LgHttpP http = (LgHttpP)arg;

	if (what == BEV_EVENT_CONNECTED) {
		// 连接成功，发送请求
		sendRequest(http);
	} else if (what == BEV_EVENT_TIMEOUT) {
		// 超时了
		close(http, LgHttpErrorTimeout);
	} else if (what == BEV_EVENT_EOF) {
		// 服务端断开了连接
		if (http->needResponseDataLength == -1) {
			struct LgHttpResponseDataEvent responseDataEvt;
			responseDataEvt.finished = LgTrue;
			responseDataEvt.data = NULL;
			responseDataEvt.size = 0;
			responseDataEvt.ratio = 1.0f;
			LgSlotCall(&http->slotResponseData, &responseDataEvt);
			close(http, LgHttpErrorNone);
		} else {
			close(http, LgHttpErrorClose);
		}
	} else if (what == BEV_EVENT_ERROR) {
		// 出错了
		close(http, LgHttpErrorReadData);
	}
}

static LgBool postData(LgHttpP http) {
	// 每次发送8k数据
	const char *p = (const char *)http->postData + http->postPosition;
	int len = LgMin(8192, http->postLength - http->postPosition);
	if (len > 0) {
		bufferevent_write(http->bev, p, len);
		http->postPosition += len;
		return LgTrue;
	} else {
		return LgFalse;
	}
}

static void socketWrite(struct bufferevent *bev, void *arg) {
	// 写完成
	LgHttpP http = (LgHttpP)arg;

	if (http->state == LgHttpStateSendingRequestHeaders) {
		// 请求头发送完成，如果为Post，则开始发送Post数据
		if (http->method == LgHttpMethodPost) {
			changeState(http, LgHttpStateSendingRequestBody);
			postData(http);
		} else {
			changeState(http, LgHttpStateReadingFirstLine);
		}
	} else if (http->state == LgHttpStateSendingRequestBody) {
		if (!postData(http)) {
			// 没有需要发送的数据了
			changeState(http, LgHttpStateReadingFirstLine);
		}
	}
}

static LgBool handleResponse(LgHttpP http) {
	if (http->responseCode == 100) {
		// reset
		sendRequest(http);
		return LgFalse;
	} else if (http->responseCode == 404) {
		close(http, LgHttpErrorNotFound);
		return LgFalse;
	} else if (http->responseCode == 403) {
		close(http, LgHttpErrorForbidden);
		return LgFalse;
	} else if (http->responseCode == 302 || http->responseCode == 301) {
		const char *cookie = (const char *)LgDictFind(http->responseFields, "Cookie");
		const char *location = (const char *)LgDictFind(http->responseFields, "Location");

		if (!location) {
			close(http, LgHttpErrorBadResponse);
		} else {
			char *_cookie = NULL, *_location;
			if (cookie) _cookie = strdup(cookie);
			_location = strdup(location);
			close(http, LgHttpErrorNone);
			if (_cookie) LgHttpSetRequestField(http, "Cookie", _cookie);
			if (!openUrl(http, _location, http->method, http->postData, http->postLength)) {
				close(http, LgHttpErrorInternal);
			}
			if (_cookie) LgFree(_cookie);
			LgFree(_location);
		}
		return LgFalse;
	} else {
		const char *contentLength = (const char *)LgDictFind(http->responseFields, "Content-Length");
		const char *transferEncoding = (const char *)LgDictFind(http->responseFields, "Transfer-Encoding");

		http->contentLength = http->needResponseDataLength = contentLength ? atoi(contentLength) : -1;
		if (http->contentLength > 0 || strcmp(transferEncoding, "chunked") == 0) {
			http->decoder = createDecoder(http);
			changeState(http, LgHttpStateReadingBody);
		} else {
			struct LgHttpResponseDataEvent responseDataEvt;
			responseDataEvt.data = NULL;
			responseDataEvt.finished = LgTrue;
			responseDataEvt.size = 0;
			responseDataEvt.ratio = 1.0f;
			LgSlotCall(&http->slotResponseData, &responseDataEvt);
			close(http, LgHttpErrorNone);
			return LgFalse;
		}
	}

	return LgTrue;
}

static void socketRead(struct bufferevent *bev, void *arg) {
	// 有数据可读
	LgHttpP http = (LgHttpP)arg;
	struct evbuffer *input = bufferevent_get_input(bev);
	char *line;
	size_t lineSize;

	while(evbuffer_get_length(input) > 0) {
		if (http->state == LgHttpStateReadingFirstLine) {
			// 读第一行
			if ((line = evbuffer_readln(input, &lineSize, EVBUFFER_EOL_CRLF))) {
				const char *error;
				int erroffset;
				pcre *re = pcre_compile("HTTP/.\\.. (?P<code>\\d+) .+", PCRE_EXTRA, &error, &erroffset, NULL);
				int vec[64], codeIdx;

				pcre_exec(re, NULL, line, lineSize, 0, 0, vec, sizeof(vec) / sizeof(int));
				codeIdx = pcre_get_stringnumber(re, "code");

				if (codeIdx >= 0) {
					http->responseCode = atoi(line + vec[codeIdx * 2]);
					pcre_free(re);
					free(line);
					changeState(http, LgHttpStateReadingHeaders);
				} else {
					// 错误的反馈头
					pcre_free(re);
					free(line);
					close(http, LgHttpErrorBadResponse);
					break;
				}
			}
		} else if (http->state == LgHttpStateReadingHeaders) {
			// 读一行
			if ((line = evbuffer_readln(input, &lineSize, EVBUFFER_EOL_CRLF))) {
				if (strlen(line) == 0) {
					// 反馈头读取完毕
					free(line);
					if (!handleResponse(http)) {
						break;
					}
				} else {
					char *p = strchr(line, ':');
					char *name, *value;

					*p = 0;
					name = strdup(line);
					*p = ':';
					p++;
					while(*p == ' ') p++;
					value = strdup(p);
					free(line);

					LgDictReplace(http->responseFields, name, value);
				}
			}
		} else if (http->state == LgHttpStateReadingBody) {
			char temp[512];
			int n = evbuffer_remove(input, temp, sizeof(temp));
			LgBool ret = http->decoder->decode(http->decoder, temp, n);
			// 读完了
			if (ret) {
				close(http, LgHttpErrorNone);
				break;
			}
		} else {
			break;
		}
	}
}

static LgBool openUrl(LgHttpP http, const char *url, enum LgHttpMethod method,
					  const void *postData, int postLength) {
	if (http->s) {
		// 已经连接了
		return LgFalse;
	}

	http->urlComponent = LgUrlParse(url);
	LG_CHECK_ERROR(http->urlComponent);
	http->method = method;
	http->postLength = postLength;
	http->postData = LgMalloc(postLength);
	LgCopyMemory(http->postData, postData, postLength);

	// 创建socket连接
	http->s = socket(AF_INET, SOCK_STREAM, 0);
	LG_CHECK_ERROR(http->s);
	evutil_make_socket_nonblocking(http->s);
	http->bev = bufferevent_socket_new(g_evtbase, http->s, BEV_OPT_CLOSE_ON_FREE);
	bufferevent_setcb(http->bev, socketRead, socketWrite, socketEvent, http);
	bufferevent_enable(http->bev, EV_READ | EV_PERSIST);

	// 开始连接
	changeState(http, LgHttpStateConnecting);
	LG_CHECK_ERROR(bufferevent_socket_connect_hostname(http->bev, NULL, AF_UNSPEC, http->urlComponent->host, http->urlComponent->port) == 0);
	return LgTrue;

_error:
	close(http, LgHttpErrorNone);
	return LgFalse;
}

LgBool LgHttpGet(LgHttpP http, const char *url) {
	return openUrl(http, url, LgHttpMethodGet, NULL, 0);
}

LgBool LgHttpPost(LgHttpP http, const char *url, const void *postData, int postDataLength) {
	return openUrl(http, url, LgHttpMethodPost, postData, postDataLength);
}

void LgHttpSetRequestField(LgHttpP http, const char *name, const char *value) {
	LgDictReplace(http->requestFields, (void *)strdup(name), (void *)strdup(value));
}

const char *LgHttpResponseField(LgHttpP http, const char *name) {
	return (const char *)LgDictFind(http->responseFields, (void *)name);
}

int LgHttpResponseCode(LgHttpP http) {
	return http->responseCode;
}

LgSlot *LgHttpSlotResponseData(LgHttpP http) {
	return &http->slotResponseData;
}

LgSlot *LgHttpSlotStateChanged(LgHttpP http) {
	return &http->slotStateChanged;
}

LgSlot *LgHttpSlotError(LgHttpP http) {
	return &http->slotError;
}
