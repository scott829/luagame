#ifndef _LG_WINDOW_WIN32_H
#define _LG_WINDOW_WIN32_H

#include "lg_window.h"
#include <windows.h>

LgBool LgWindowAttach(HWND hwnd, LgBool destroy);

HWND LgWindowHWND();

#endif