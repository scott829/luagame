#ifndef _LG_PNGLOADER_H
#define _LG_PNGLOADER_H

#include "lg_inputstream.h"
#include "lg_image.h"

LgBool pngCheckType(const unsigned char *data, int size);

LgImageP pngLoad(LgInputStreamP inputStream);

#endif