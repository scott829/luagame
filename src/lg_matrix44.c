#include "lg_matrix44.h"

void LgMatrix44Indentity(LgMatrix44P m) {
	m->m44[0][0] = 1, m->m44[0][1] = 0, m->m44[0][2] = 0, m->m44[0][3] = 0;
	m->m44[1][0] = 0, m->m44[1][1] = 1, m->m44[1][2] = 0, m->m44[1][3] = 0;
	m->m44[2][0] = 0, m->m44[2][1] = 0, m->m44[2][2] = 1, m->m44[2][3] = 0;
	m->m44[3][0] = 0, m->m44[3][1] = 0, m->m44[3][2] = 0, m->m44[3][3] = 1;
}

void LgMatrix44InitScale(LgMatrix44P m, float sx, float sy, float sz) {
	m->m44[0][0] = sx,	m->m44[0][1] = 0,	m->m44[0][2] = 0,	m->m44[0][3] = 0;
	m->m44[1][0] = 0,	m->m44[1][1] = sy,	m->m44[1][2] = 0,	m->m44[1][3] = 0;
	m->m44[2][0] = 0,	m->m44[2][1] = 0,	m->m44[2][2] = sz,	m->m44[2][3] = 0;
	m->m44[3][0] = 0,	m->m44[3][1] = 0,	m->m44[3][2] = 0,	m->m44[3][3] = 1;
}

void LgMatrix44InitTranslate(LgMatrix44P m, float tx, float ty, float tz) {
	m->m44[0][0] = 1,	m->m44[0][1] = 0,	m->m44[0][2] = 0,	m->m44[0][3] = 0;
	m->m44[1][0] = 0,	m->m44[1][1] = 1,	m->m44[1][2] = 0,	m->m44[1][3] = 0;
	m->m44[2][0] = 0,	m->m44[2][1] = 0,	m->m44[2][2] = 1,	m->m44[2][3] = 0;
	m->m44[3][0] = tx,	m->m44[3][1] = ty,	m->m44[3][2] = tz,	m->m44[3][3] = 1;
}

void LgMatrix44InitRotateX(LgMatrix44P m, float radians) {
	float s, c;
	s = sinf(radians);
	c = cosf(radians);
	m->m44[0][0] = 1,	m->m44[0][1] = 0,	m->m44[0][2] = 0,	m->m44[0][3] = 0;
	m->m44[1][0] = 0,	m->m44[1][1] = c,	m->m44[1][2] = s,	m->m44[1][3] = 0;
	m->m44[2][0] = 0,	m->m44[2][1] = -s,	m->m44[2][2] = c,	m->m44[2][3] = 0;
	m->m44[3][0] = 0,	m->m44[3][1] = 0,	m->m44[3][2] = 0,	m->m44[3][3] = 1;
}

void LgMatrix44InitRotateY(LgMatrix44P m, float radians) {
	float s, c;
	s = sinf(radians);
	c = cosf(radians);
	m->m44[0][0] = c,	m->m44[0][1] = 0,	m->m44[0][2] = -s,	m->m44[0][3] = 0;
	m->m44[1][0] = 0,	m->m44[1][1] = 1,	m->m44[1][2] = 0,	m->m44[1][3] = 0;
	m->m44[2][0] = s,	m->m44[2][1] = 0,	m->m44[2][2] = c,	m->m44[2][3] = 0;
	m->m44[3][0] = 0,	m->m44[3][1] = 0,	m->m44[3][2] = 0,	m->m44[3][3] = 1;
}

void LgMatrix44InitRotateZ(LgMatrix44P m, float radians) {
	float s, c;
	s = sinf(radians);
	c = cosf(radians);
	m->m44[0][0] = c,	m->m44[0][1] = s,	m->m44[0][2] = 0,	m->m44[0][3] = 0;
	m->m44[1][0] = -s,	m->m44[1][1] = c,	m->m44[1][2] = 0,	m->m44[1][3] = 0;
	m->m44[2][0] = 0,	m->m44[2][1] = 0,	m->m44[2][2] = 1,	m->m44[2][3] = 0;
	m->m44[3][0] = 0,	m->m44[3][1] = 0,	m->m44[3][2] = 0,	m->m44[3][3] = 1;
}

void LgMatrix44Transpose(LgMatrix44P m) {
	struct LgMatrix44 result;

	result.m44[0][0] = m->m44[0][0], result.m44[0][1] = m->m44[1][0], result.m44[0][2] = m->m44[2][0], result.m44[0][3] = m->m44[3][0];
	result.m44[1][0] = m->m44[0][1], result.m44[1][1] = m->m44[1][1], result.m44[1][2] = m->m44[2][1], result.m44[1][3] = m->m44[3][1];
	result.m44[2][0] = m->m44[0][2], result.m44[2][1] = m->m44[1][2], result.m44[2][2] = m->m44[2][2], result.m44[2][3] = m->m44[3][2];
	result.m44[3][0] = m->m44[0][3], result.m44[3][1] = m->m44[1][3], result.m44[3][2] = m->m44[2][3], result.m44[3][3] = m->m44[3][3];
	LgCopyMemory(m, &result, sizeof(struct LgMatrix44));
}

void LgMatrix44Mul(LgMatrix44P lhs, const LgMatrix44P rhs) {
	struct LgMatrix44 tmp, result;

	LgCopyMemory(&tmp, rhs, sizeof(struct LgMatrix44));
	LgMatrix44Transpose(&tmp);

	result.m44[0][0] = lhs->m44[0][0] * tmp.m44[0][0] + lhs->m44[0][1] * tmp.m44[0][1] + lhs->m44[0][2] * tmp.m44[0][2] + lhs->m44[0][3] * tmp.m44[0][3];
	result.m44[0][1] = lhs->m44[0][0] * tmp.m44[1][0] + lhs->m44[0][1] * tmp.m44[1][1] + lhs->m44[0][2] * tmp.m44[1][2] + lhs->m44[0][3] * tmp.m44[1][3];
	result.m44[0][2] = lhs->m44[0][0] * tmp.m44[2][0] + lhs->m44[0][1] * tmp.m44[2][1] + lhs->m44[0][2] * tmp.m44[2][2] + lhs->m44[0][3] * tmp.m44[2][3];
	result.m44[0][3] = lhs->m44[0][0] * tmp.m44[3][0] + lhs->m44[0][1] * tmp.m44[3][1] + lhs->m44[0][2] * tmp.m44[3][2] + lhs->m44[0][3] * tmp.m44[3][3];

	result.m44[1][0] = lhs->m44[1][0] * tmp.m44[0][0] + lhs->m44[1][1] * tmp.m44[0][1] + lhs->m44[1][2] * tmp.m44[0][2] + lhs->m44[1][3] * tmp.m44[0][3];
	result.m44[1][1] = lhs->m44[1][0] * tmp.m44[1][0] + lhs->m44[1][1] * tmp.m44[1][1] + lhs->m44[1][2] * tmp.m44[1][2] + lhs->m44[1][3] * tmp.m44[1][3];
	result.m44[1][2] = lhs->m44[1][0] * tmp.m44[2][0] + lhs->m44[1][1] * tmp.m44[2][1] + lhs->m44[1][2] * tmp.m44[2][2] + lhs->m44[1][3] * tmp.m44[2][3];
	result.m44[1][3] = lhs->m44[1][0] * tmp.m44[3][0] + lhs->m44[1][1] * tmp.m44[3][1] + lhs->m44[1][2] * tmp.m44[3][2] + lhs->m44[1][3] * tmp.m44[3][3];

	result.m44[2][0] = lhs->m44[2][0] * tmp.m44[0][0] + lhs->m44[2][1] * tmp.m44[0][1] + lhs->m44[2][2] * tmp.m44[0][2] + lhs->m44[2][3] * tmp.m44[0][3];
	result.m44[2][1] = lhs->m44[2][0] * tmp.m44[1][0] + lhs->m44[2][1] * tmp.m44[1][1] + lhs->m44[2][2] * tmp.m44[1][2] + lhs->m44[2][3] * tmp.m44[1][3];
	result.m44[2][2] = lhs->m44[2][0] * tmp.m44[2][0] + lhs->m44[2][1] * tmp.m44[2][1] + lhs->m44[2][2] * tmp.m44[2][2] + lhs->m44[2][3] * tmp.m44[2][3];
	result.m44[2][3] = lhs->m44[2][0] * tmp.m44[3][0] + lhs->m44[2][1] * tmp.m44[3][1] + lhs->m44[2][2] * tmp.m44[3][2] + lhs->m44[2][3] * tmp.m44[3][3];

	result.m44[3][0] = lhs->m44[3][0] * tmp.m44[0][0] + lhs->m44[3][1] * tmp.m44[0][1] + lhs->m44[3][2] * tmp.m44[0][2] + lhs->m44[3][3] * tmp.m44[0][3];
	result.m44[3][1] = lhs->m44[3][0] * tmp.m44[1][0] + lhs->m44[3][1] * tmp.m44[1][1] + lhs->m44[3][2] * tmp.m44[1][2] + lhs->m44[3][3] * tmp.m44[1][3];
	result.m44[3][2] = lhs->m44[3][0] * tmp.m44[2][0] + lhs->m44[3][1] * tmp.m44[2][1] + lhs->m44[3][2] * tmp.m44[2][2] + lhs->m44[3][3] * tmp.m44[2][3];
	result.m44[3][3] = lhs->m44[3][0] * tmp.m44[3][0] + lhs->m44[3][1] * tmp.m44[3][1] + lhs->m44[3][2] * tmp.m44[3][2] + lhs->m44[3][3] * tmp.m44[3][3];
	LgCopyMemory(lhs, &result, sizeof(struct LgMatrix44));
}

void LgMatrix44Scale(LgMatrix44P m, float sx, float sy, float sz) {
	struct LgMatrix44 tmp;
	LgMatrix44InitScale(&tmp, sx, sy, sz);
	LgMatrix44Mul(m, &tmp);
}

void LgMatrix44Translate(LgMatrix44P m, float tx, float ty, float tz) {
	struct LgMatrix44 tmp;
	LgMatrix44InitTranslate(&tmp, tx, ty, tz);
	LgMatrix44Mul(m, &tmp);
}

void LgMatrix44RotateX(LgMatrix44P m, float radians) {
	struct LgMatrix44 tmp;
	LgMatrix44InitRotateX(&tmp, radians);
	LgMatrix44Mul(m, &tmp);
}

void LgMatrix44RotateY(LgMatrix44P m, float radians) {
	struct LgMatrix44 tmp;
	LgMatrix44InitRotateY(&tmp, radians);
	LgMatrix44Mul(m, &tmp);
}

void LgMatrix44RotateZ(LgMatrix44P m, float radians) {
	struct LgMatrix44 tmp;
	LgMatrix44InitRotateZ(&tmp, radians);
	LgMatrix44Mul(m, &tmp);

}

float LgMatrix44Determinant(const LgMatrix44P m) {
	float _3142_3241 = (m->m44[2][0] * m->m44[3][1] - m->m44[2][1] * m->m44[3][0]);
	float _3143_3341 = (m->m44[2][0] * m->m44[3][2] - m->m44[2][2] * m->m44[3][0]);
	float _3144_3441 = (m->m44[2][0] * m->m44[3][3] - m->m44[2][3] * m->m44[3][0]);
	float _3243_3342 = (m->m44[2][1] * m->m44[3][2] - m->m44[2][2] * m->m44[3][1]);
	float _3244_3442 = (m->m44[2][1] * m->m44[3][3] - m->m44[2][3] * m->m44[3][1]);
	float _3344_3443 = (m->m44[2][2] * m->m44[3][3] - m->m44[2][3] * m->m44[3][2]);

	return m->m44[0][0] * (m->m44[1][1] * _3344_3443 - m->m44[1][2] * _3244_3442 + m->m44[1][3] * _3243_3342)
		- m->m44[0][1] * (m->m44[1][0] * _3344_3443 - m->m44[1][2] * _3144_3441 + m->m44[1][3] * _3143_3341)
		+ m->m44[0][2] * (m->m44[1][0] * _3244_3442 - m->m44[1][1] * _3144_3441 + m->m44[1][3] * _3142_3241)
		- m->m44[0][3] * (m->m44[1][0] * _3243_3342 - m->m44[1][1] * _3143_3341 + m->m44[1][2] * _3142_3241);
}

void LgMatrix44Inverse(LgMatrix44P m) {
	struct LgMatrix44 result;
	float d = LgMatrix44Determinant(m);
	float invDet;

	invDet = 1.0f / d;

	{
		float _2132_2231 = (m->m44[1][0] * m->m44[2][1] - m->m44[1][1] * m->m44[2][0]);
		float _2133_2331 = (m->m44[1][0] * m->m44[2][2] - m->m44[1][2] * m->m44[2][0]);
		float _2134_2431 = (m->m44[1][0] * m->m44[2][3] - m->m44[1][3] * m->m44[2][0]);
		float _2142_2241 = (m->m44[1][0] * m->m44[3][1] - m->m44[1][1] * m->m44[3][0]);
		float _2143_2341 = (m->m44[1][0] * m->m44[3][2] - m->m44[1][2] * m->m44[3][0]);
		float _2144_2441 = (m->m44[1][0] * m->m44[3][3] - m->m44[1][3] * m->m44[3][0]);
		float _2233_2332 = (m->m44[1][1] * m->m44[2][2] - m->m44[1][2] * m->m44[2][1]);
		float _2234_2432 = (m->m44[1][1] * m->m44[2][3] - m->m44[1][3] * m->m44[2][1]);
		float _2243_2342 = (m->m44[1][1] * m->m44[3][2] - m->m44[1][2] * m->m44[3][1]);
		float _2244_2442 = (m->m44[1][1] * m->m44[3][3] - m->m44[1][3] * m->m44[3][1]);
		float _2334_2433 = (m->m44[1][2] * m->m44[2][3] - m->m44[1][3] * m->m44[2][2]);
		float _2344_2443 = (m->m44[1][2] * m->m44[3][3] - m->m44[1][3] * m->m44[3][2]);
		float _3142_3241 = (m->m44[2][0] * m->m44[3][1] - m->m44[2][1] * m->m44[3][0]);
		float _3143_3341 = (m->m44[2][0] * m->m44[3][2] - m->m44[2][2] * m->m44[3][0]);
		float _3144_3441 = (m->m44[2][0] * m->m44[3][3] - m->m44[2][3] * m->m44[3][0]);
		float _3243_3342 = (m->m44[2][1] * m->m44[3][2] - m->m44[2][2] * m->m44[3][1]);
		float _3244_3442 = (m->m44[2][1] * m->m44[3][3] - m->m44[2][3] * m->m44[3][1]);
		float _3344_3443 = (m->m44[2][2] * m->m44[3][3] - m->m44[2][3] * m->m44[3][2]);

		result.m44[0][0] = +invDet * (m->m44[1][1] * _3344_3443 - m->m44[1][2] * _3244_3442 + m->m44[1][3] * _3243_3342);
		result.m44[0][1] = -invDet * (m->m44[0][1] * _3344_3443 - m->m44[0][2] * _3244_3442 + m->m44[0][3] * _3243_3342);
		result.m44[0][2] = +invDet * (m->m44[0][1] * _2344_2443 - m->m44[0][2] * _2244_2442 + m->m44[0][3] * _2243_2342);
		result.m44[0][3] = -invDet * (m->m44[0][1] * _2334_2433 - m->m44[0][2] * _2234_2432 + m->m44[0][3] * _2233_2332);

		result.m44[1][0] = -invDet * (m->m44[1][0] * _3344_3443 - m->m44[1][2] * _3144_3441 + m->m44[1][3] * _3143_3341);
		result.m44[1][1] = +invDet * (m->m44[0][0] * _3344_3443 - m->m44[0][2] * _3144_3441 + m->m44[0][3] * _3143_3341);
		result.m44[1][2] = -invDet * (m->m44[0][0] * _2344_2443 - m->m44[0][2] * _2144_2441 + m->m44[0][3] * _2143_2341);
		result.m44[1][3] = +invDet * (m->m44[0][0] * _2334_2433 - m->m44[0][2] * _2134_2431 + m->m44[0][3] * _2133_2331);

		result.m44[2][0] = +invDet * (m->m44[1][0] * _3244_3442 - m->m44[1][1] * _3144_3441 + m->m44[1][3] * _3142_3241);
		result.m44[2][1] = -invDet * (m->m44[0][0] * _3244_3442 - m->m44[0][1] * _3144_3441 + m->m44[0][3] * _3142_3241);
		result.m44[2][2] = +invDet * (m->m44[0][0] * _2244_2442 - m->m44[0][1] * _2144_2441 + m->m44[0][3] * _2142_2241);
		result.m44[2][3] = -invDet * (m->m44[0][0] * _2234_2432 - m->m44[0][1] * _2134_2431 + m->m44[0][3] * _2132_2231);

		result.m44[3][0] = -invDet * (m->m44[1][0] * _3243_3342 - m->m44[1][1] * _3143_3341 + m->m44[1][2] * _3142_3241);
		result.m44[3][1] = +invDet * (m->m44[0][0] * _3243_3342 - m->m44[0][1] * _3143_3341 + m->m44[0][2] * _3142_3241);
		result.m44[3][2] = -invDet * (m->m44[0][0] * _2243_2342 - m->m44[0][1] * _2143_2341 + m->m44[0][2] * _2142_2241);
		result.m44[3][3] = +invDet * (m->m44[0][0] * _2233_2332 - m->m44[0][1] * _2133_2331 + m->m44[0][2] * _2132_2231);

		LgCopyMemory(m, &result, sizeof(struct LgMatrix44));
	}
}

void LgMatrix44LhToRh(LgMatrix44P m) {
	m->m44[2][0] = -m->m44[2][0];
	m->m44[2][1] = -m->m44[2][1];
	m->m44[2][2] = -m->m44[2][2];
	m->m44[2][3] = -m->m44[2][3];
}

void LgMatrix44OrthoOffsetCenterLH(LgMatrix44P m, float left, float right, float bottom, float top, float nearPlane, float farPlane) {
	float q = 1 / (farPlane - nearPlane);
	float invWidth = 1 / (right - left);
	float invHeight = 1 / (top - bottom);

	m->m44[0][0] = invWidth * 2,				m->m44[0][1] = 0,							m->m44[0][2] = 0,				m->m44[0][3] = 0;
	m->m44[1][0] = 0,							m->m44[1][1] = invHeight * 2,				m->m44[1][2] = 0,				m->m44[1][3] = 0;
	m->m44[2][0] = 0,							m->m44[2][1] = 0,							m->m44[2][2] = q,				m->m44[2][3] = 0;
	m->m44[3][0] = -(left + right) * invWidth,	m->m44[3][1] = -(top + bottom) * invHeight,	m->m44[3][2] = -nearPlane * q,	m->m44[3][3] = 1;
}

void LgMatrix44OrthoOffsetCenterRH(LgMatrix44P m, float left, float right, float bottom, float top, float nearPlane, float farPlane) {
	LgMatrix44OrthoOffsetCenterLH(m, left, right, bottom, top, nearPlane, farPlane);
	LgMatrix44LhToRh(m);
}

void LgMatrix44OrthoLH(LgMatrix44P m, float width, float height, float nearPlane, float farPlane) {
	float w_2 = width / 2, h_2 = height / 2;
	LgMatrix44OrthoOffsetCenterLH(m, -w_2, w_2, -h_2, h_2, nearPlane, farPlane);
}

void LgMatrix44OrthoRH(LgMatrix44P m, float width, float height, float nearPlane, float farPlane) {
	LgMatrix44OrthoLH(m, width, height, nearPlane, farPlane);
	LgMatrix44LhToRh(m);
}

void LgMatrix44PerspectiveOffsetCenterLH(LgMatrix44P m, float left, float right, float bottom, float top, float nearPlane, float farPlane) {
	float q = farPlane / (farPlane - nearPlane);
	float invWidth = 1 / (right - left);
	float invHeight = 1 / (top - bottom);

	m->m44[0][0] = invWidth + invWidth,			m->m44[0][1] = 0,								m->m44[0][2] = 0,				m->m44[0][3] = 0;
	m->m44[1][0] = 0,							m->m44[1][1] = invHeight + invHeight,			m->m44[1][2] = 0,				m->m44[1][3] = 0;
	m->m44[2][0] = 0,							m->m44[2][1] = 0,								m->m44[2][2] = q,				m->m44[2][3] = 0;
	m->m44[3][0] = -(left + right) * invWidth,	m->m44[3][1] = -(top + bottom) * invHeight,		m->m44[3][2] = -nearPlane * q,	m->m44[3][3] = 1;
}

void LgMatrix44PerspectiveOffsetCenterRH(LgMatrix44P m, float left, float right, float bottom, float top, float nearPlane, float farPlane) {
	LgMatrix44PerspectiveOffsetCenterLH(m, left, right, bottom, top, nearPlane, farPlane);
	LgMatrix44LhToRh(m);
}

void LgMatrix44PerspectiveLH(LgMatrix44P m, float width, float height, float nearPlane, float farPlane) {
	float q = farPlane / (farPlane - nearPlane);
	float near2 = nearPlane * 2;

	m->m44[0][0] = near2 / width,	m->m44[0][1] = 0,				m->m44[0][2] = 0,				m->m44[0][3] = 0;
	m->m44[1][0] = 0,				m->m44[1][1] = near2 / height,	m->m44[1][2] = 0,				m->m44[1][3] = 0;
	m->m44[2][0] = 0,				m->m44[2][1] = 0,				m->m44[2][2] = q,				m->m44[2][3] = 1;
	m->m44[3][0] = 0,				m->m44[3][1] = 0,				m->m44[3][2] = -nearPlane * q,	m->m44[3][3] = 0;
}

void LgMatrix44PerspectiveRH(LgMatrix44P m, float width, float height, float nearPlane, float farPlane) {
	LgMatrix44PerspectiveLH(m, width, height, nearPlane, farPlane);
	LgMatrix44LhToRh(m);
}

void LgMatrix44PerspectiveFovLH(LgMatrix44P m, float fov, float aspect, float nearPlane, float farPlane) {
	float h = 1 / tanf(fov / 2);
	float w = h / aspect;
	float q = farPlane / (farPlane - nearPlane);

	m->m44[0][0] = w,	m->m44[0][1] = 0,	m->m44[0][2] = 0,				m->m44[0][3] = 0;
	m->m44[1][0] = 0,	m->m44[1][1] = h,	m->m44[1][2] = 0,				m->m44[1][3] = 0;
	m->m44[2][0] = 0,	m->m44[2][1] = 0,	m->m44[2][2] = q,				m->m44[2][3] = 1;
	m->m44[3][0] = 0,	m->m44[3][1] = 0,	m->m44[3][2] = -nearPlane * q,	m->m44[3][3] = 0;
}

void LgMatrix44PerspectiveFovRH(LgMatrix44P m, float fov, float aspect, float nearPlane, float farPlane) {
	LgMatrix44PerspectiveFovLH(m, fov, aspect, nearPlane, farPlane);
	LgMatrix44LhToRh(m);
}

void LgMatrix44LookAtLH(LgMatrix44P m, const LgVector3P eye, const LgVector3P at, const LgVector3P up) {
	struct LgVector3 zaxis, xaxis, yaxis;

	LgCopyMemory(&zaxis, at, sizeof(struct LgVector3));
	LgVector3Sub(&zaxis, eye);
	LgVector3Normalize(&zaxis);

	LgCopyMemory(&xaxis, up, sizeof(struct LgVector3));
	LgVector3Cross(&xaxis, &zaxis);
	LgVector3Normalize(&xaxis);

	LgCopyMemory(&yaxis, &zaxis, sizeof(struct LgVector3));
	LgVector3Cross(&yaxis, &xaxis);

	m->m44[0][0] = xaxis.x,						m->m44[0][1] = yaxis.x,						m->m44[0][2] = zaxis.x,						m->m44[0][3] = 0;
	m->m44[1][0] = xaxis.y,						m->m44[1][1] = yaxis.y,						m->m44[1][2] = zaxis.y,						m->m44[1][3] = 0;
	m->m44[2][0] = xaxis.z,						m->m44[2][1] = yaxis.z,						m->m44[2][2] = zaxis.z,						m->m44[2][3] = 0;
	m->m44[3][0] = -LgVector3Dot(&xaxis, eye),	m->m44[3][1] = -LgVector3Dot(&yaxis, eye),	m->m44[3][2] = -LgVector3Dot(&zaxis, eye),	m->m44[3][3] = 1;
}

void LgMatrix44LookAtRH(LgMatrix44P m, const LgVector3P eye, const LgVector3P at, const LgVector3P up) {
	struct LgVector3 zaxis, xaxis, yaxis;

	LgCopyMemory(&zaxis, eye, sizeof(struct LgVector3));
	LgVector3Sub(&zaxis, at);
	LgVector3Normalize(&zaxis);

	LgCopyMemory(&xaxis, up, sizeof(struct LgVector3));
	LgVector3Cross(&xaxis, &zaxis);
	LgVector3Normalize(&xaxis);

	LgCopyMemory(&yaxis, &zaxis, sizeof(struct LgVector3));
	LgVector3Cross(&yaxis, &xaxis);

	m->m44[0][0] = xaxis.x,						m->m44[0][1] = yaxis.x,						m->m44[0][2] = zaxis.x,						m->m44[0][3] = 0;
	m->m44[1][0] = xaxis.y,						m->m44[1][1] = xaxis.y,						m->m44[1][2] = xaxis.y,						m->m44[1][3] = 0;
	m->m44[2][0] = xaxis.z,						m->m44[2][1] = xaxis.z,						m->m44[2][2] = xaxis.z,						m->m44[2][3] = 0;
	m->m44[3][0] = -LgVector3Dot(&xaxis, eye),	m->m44[3][1] = -LgVector3Dot(&yaxis, eye),	m->m44[3][2] = -LgVector3Dot(&zaxis, eye),	m->m44[3][3] = 1;
}

void LgMatrix44MulVector2(const LgMatrix44P m, LgVector3P vec) {
	float _x, _y, _z, _w;

	_x = vec->x * m->m44[0][0] + vec->y * m->m44[1][0] + vec->z * m->m44[2][0] + m->m44[3][0];
	_y = vec->x * m->m44[0][1] + vec->y * m->m44[1][1] + vec->z * m->m44[2][1] + m->m44[3][1];
	_z = vec->x * m->m44[0][2] + vec->y * m->m44[1][2] + vec->z * m->m44[2][2] + m->m44[3][2];
	_w = vec->x * m->m44[0][3] + vec->y * m->m44[1][3] + vec->z * m->m44[2][3] + m->m44[3][3];

	if (!LgFloatEqual(_w, 0)) {
		_x /= _w;
		_y /= _w;
		_z /= _w;
	}

	vec->x = _x, vec->y = _y, vec->z = _z;
}