#include "lg_statistics.h"
#include "lg_time.h"
#include "lg_label.h"
#include "lg_director.h"
#include <stdio.h>
#include "lg_fontsystem.h"
#include "lg_drawnode.h"
#include "lg_resourcemanager.h"
#include "lg_action.h"
#include "lg_array.h"
#include "lg_scriptenv.h"
#include "lg_scheduler.h"

#define FONTNAME "_default"
#define FONTSIZE 16

typedef struct LgStatisticeData {
	LgTime			prevTime;
	unsigned long	frameCount;
	LgTime			timeElapsed;
	float			fps;
	int				batch;
	LgNodeP			nodeRSLabel, nodeRSValue;
	LgNodeP			nodeResolutionLabel, nodeResolutionValue;
	LgNodeP			nodeFPSLabel, nodeFPSValue;
	LgNodeP			nodeBatchLabel, nodeBatchValue;
	LgNodeP			nodeSSSizeLabel, nodeSSSizeValue;
	LgNodeP			nodeRCLabel, nodeRCValue;
	LgNodeP			nodeANLabel, nodeANValue;
	LgNodeP			nodeAALabel, nodeAAValue;
	LgNodeP			nodeSchLabel, nodeSchValue;
	LgNodeP			nodeLuaMLabel, nodeLuaMValue;
}*LgStatisticeDataP;

static void spriteEventProc(LgEventType type, LgNodeP node, void *param) {
	switch(type) {
	case LG_NODE_EVENT_Destroy:
		{
			LgStatisticeDataP sd = (LgStatisticeDataP)LgNodeUserData(node);
			LgFree(sd);
		}
		break;
	}

	LgNodeDefEventProc(type, node, param);
}

static void createLabel(const char *name, float y, LgNodeP *labelNode, LgNodeP *valueNode) {
	struct LgVector2 vec;

	*labelNode = LgLabelCreateR(NULL);
	*valueNode = LgLabelCreateR(NULL);
	LgLabelSetFontName(*labelNode,FONTNAME);
	LgLabelSetFontName(*valueNode,FONTNAME);
	LgLabelSetFontSize(*labelNode, FONTSIZE);
	LgLabelSetFontSize(*valueNode, FONTSIZE);
	LgLabelSetColor(*labelNode, LgColorXRGB(255, 255, 255));
	LgLabelSetColor(*valueNode, LgColorXRGB(160, 160, 160));
	LgLabelSetText(*labelNode, name);

	vec.x = 5, vec.y = y;
	LgNodeSetPosition(*labelNode, &vec);
	vec.x = 150, vec.y = y;
	LgNodeSetPosition(*valueNode, &vec);
}

static void updateUi(LgNodeP node) {
	LgStatisticeDataP sd = (LgStatisticeDataP)LgNodeUserData(node);
	struct LgSize viewSize;
	char temp[512];

	LgLabelSetText(sd->nodeRSValue, LgRenderSystemName());

	LgDirectorViewSize(&viewSize);
	sprintf(temp, "(%d, %d)", viewSize.width, viewSize.height);
	LgLabelSetText(sd->nodeResolutionValue, temp);

	sprintf(temp, "%.2f", sd->fps);
	LgLabelSetText(sd->nodeFPSValue, temp);

	sprintf(temp, "%d", sd->batch);
	LgLabelSetText(sd->nodeBatchValue, temp);

	sprintf(temp, "%d", LgNodeCount());
	LgLabelSetText(sd->nodeANValue, temp);

	sprintf(temp, "%d", LgActionCount());
	LgLabelSetText(sd->nodeAAValue, temp);

	sprintf(temp, "%d", LgSchedulerCount());
	LgLabelSetText(sd->nodeSchValue, temp);

	sprintf(temp, "%d", LgDirectorSceneStackSize());
	LgLabelSetText(sd->nodeSSSizeValue, temp);

	sprintf(temp, "%d/%d", LgResourceManagerCachedCount(), LgResourceManagerMaxCount());
	LgLabelSetText(sd->nodeRCValue, temp);

	sprintf(temp, "%dk", lua_gc(LgScriptEnvState(), LUA_GCCOUNT, 0));
	LgLabelSetText(sd->nodeLuaMValue, temp);
}

LgNodeP LgStatisticeCreateR() {
	LgNodeP node;
	LgStatisticeDataP sd;
	LgFontP font;
	float height;
	LgNodeP bkg;
	float y = 0;
	const int itemCount = 10;

	node = LgNodeCreateR((LgEvent)spriteEventProc);
	LG_CHECK_ERROR(node);

	sd = (LgStatisticeDataP)LgMallocZ(sizeof(struct LgStatisticeData));
	LG_CHECK_ERROR(sd);
	LgNodeSetUserData(node, sd);
	sd->prevTime = LgGetTimeOfDay();

	font = LgFontSystemFindFont(FONTNAME);
	LG_CHECK_ERROR(font);
	height = LgFontGetDriver(font)->height(LgFontGetData(font), FONTSIZE);

	bkg = LgDrawNodeCreateR();
	LgDrawNodeSetColor(bkg, LgColorARGB(160, 0, 0, 0));
	LgDrawNodeRectangle(bkg, 0, 0, 240, height * itemCount + 5);
	LgNodeAddChild(node, bkg); LgNodeRelease(bkg);

	createLabel("RenderSystem:", y, &sd->nodeRSLabel, &sd->nodeRSValue);
	LgNodeAddChild(node, sd->nodeRSLabel); LgNodeRelease(sd->nodeRSLabel);
	LgNodeAddChild(node, sd->nodeRSValue); LgNodeRelease(sd->nodeRSValue);
	y += height;

	createLabel("Resolution:", y, &sd->nodeResolutionLabel, &sd->nodeResolutionValue);
	LgNodeAddChild(node, sd->nodeResolutionLabel); LgNodeRelease(sd->nodeResolutionLabel);
	LgNodeAddChild(node, sd->nodeResolutionValue); LgNodeRelease(sd->nodeResolutionValue);
	y += height;

	createLabel("FPS:", y, &sd->nodeFPSLabel, &sd->nodeFPSValue);
	LgNodeAddChild(node, sd->nodeFPSLabel); LgNodeRelease(sd->nodeFPSLabel);
	LgNodeAddChild(node, sd->nodeFPSValue); LgNodeRelease(sd->nodeFPSValue);
	y += height;

	createLabel("Batch:", y, &sd->nodeBatchLabel, &sd->nodeBatchValue);
	LgNodeAddChild(node, sd->nodeBatchLabel); LgNodeRelease(sd->nodeBatchLabel);
	LgNodeAddChild(node, sd->nodeBatchValue); LgNodeRelease(sd->nodeBatchValue);
	y += height;

	createLabel("Allocated Node:", y, &sd->nodeANLabel, &sd->nodeANValue);
	LgNodeAddChild(node, sd->nodeANLabel); LgNodeRelease(sd->nodeANLabel);
	LgNodeAddChild(node, sd->nodeANValue); LgNodeRelease(sd->nodeANValue);
	y += height;

	createLabel("Allocated Action:", y, &sd->nodeAALabel, &sd->nodeAAValue);
	LgNodeAddChild(node, sd->nodeAALabel); LgNodeRelease(sd->nodeAALabel);
	LgNodeAddChild(node, sd->nodeAAValue); LgNodeRelease(sd->nodeAAValue);
	y += height;

	createLabel("Scheduled:", y, &sd->nodeSchLabel, &sd->nodeSchValue);
	LgNodeAddChild(node, sd->nodeSchLabel); LgNodeRelease(sd->nodeSchLabel);
	LgNodeAddChild(node, sd->nodeSchValue); LgNodeRelease(sd->nodeSchValue);
	y += height;

	createLabel("Scene Stack:", y, &sd->nodeSSSizeLabel, &sd->nodeSSSizeValue);
	LgNodeAddChild(node, sd->nodeSSSizeLabel); LgNodeRelease(sd->nodeSSSizeLabel);
	LgNodeAddChild(node, sd->nodeSSSizeValue); LgNodeRelease(sd->nodeSSSizeValue);
	y += height;

	createLabel("Res Cache:", y, &sd->nodeRCLabel, &sd->nodeRCValue);
	LgNodeAddChild(node, sd->nodeRCLabel); LgNodeRelease(sd->nodeRCLabel);
	LgNodeAddChild(node, sd->nodeRCValue); LgNodeRelease(sd->nodeRCValue);
	y += height;

	createLabel("Lua Heap:", y, &sd->nodeLuaMLabel, &sd->nodeLuaMValue);
	LgNodeAddChild(node, sd->nodeLuaMLabel); LgNodeRelease(sd->nodeLuaMLabel);
	LgNodeAddChild(node, sd->nodeLuaMValue); LgNodeRelease(sd->nodeLuaMValue);
	y += height;

	return node;

_error:
	if (node) LgNodeRelease(node);
	return NULL;
}

void LgStatisticeLoopStart(LgNodeP node){
	LgStatisticeDataP sd = (LgStatisticeDataP)LgNodeUserData(node);
	sd->batch = 0;
}

void LgStatisticeLoopEnd(LgNodeP node){
	LgTime currentTime;
	LgTime delta;
	LgStatisticeDataP sd = (LgStatisticeDataP)LgNodeUserData(node);

	currentTime = LgGetTimeOfDay();
	delta = currentTime - sd->prevTime;
	sd->timeElapsed += delta;
	sd->frameCount++;
	sd->fps = (float)(sd->frameCount / sd->timeElapsed);
	sd->prevTime = currentTime;

	updateUi(node);
}

void LgStatisticeUpdateBatch(LgNodeP node, int batch) {
	LgStatisticeDataP sd = (LgStatisticeDataP)LgNodeUserData(node);
	sd->batch += batch;
}
