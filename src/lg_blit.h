#ifndef _LG_BLIT_H
#define _LG_BLIT_H

typedef void (* LgBlit)(unsigned char *dest, const unsigned char *src);

void LgBlit_A8R8G8B8_To_L8(unsigned char *dest, const unsigned char *src);

void LgBlit_A8R8G8B8_To_A8L8(unsigned char *dest, const unsigned char *src);

void LgBlit_A8R8G8B8_To_A8R8G8B8(unsigned char *dest, const unsigned char *src);

void LgBlit_A8L8_To_L8(unsigned char *dest, const unsigned char *src);

void LgBlit_A8L8_To_A8L8(unsigned char *dest, const unsigned char *src);

void LgBlit_A8L8_To_A8R8G8B8(unsigned char *dest, const unsigned char *src);

void LgBlit_L8_To_L8(unsigned char *dest, const unsigned char *src);

void LgBlit_L8_To_A8L8(unsigned char *dest, const unsigned char *src);

void LgBlit_L8_To_A8R8G8B8(unsigned char *dest, const unsigned char *src);

#endif