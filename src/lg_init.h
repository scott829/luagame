#ifndef _LG_INIT_H
#define _LG_INIT_H

#include "lg_basetypes.h"
#include "jansson/jansson.h"
#include "event2/event.h"
#include "event2/thread.h"

extern json_t *g_globalConfig;

extern struct event_base *g_evtbase;

LgBool LgInit(int argc, char **argv);

void LgCleanup();

LgBool LgMainLoop();

const char *LgResourceDirectory();

const char *LgResourceFilename(const char *filename);

const char *LgResourceDirname(const char *filename);

void LgSetResourceDirectory(const char *path);

void LgExit();

#endif
