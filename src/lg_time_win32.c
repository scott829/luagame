#include "lg_time.h"
#include <windows.h>

LgTime LgGetTimeOfDay() {
	LARGE_INTEGER liTime, liFreq;
	long sec, usec;

	QueryPerformanceFrequency(&liFreq);
	QueryPerformanceCounter(&liTime);
	sec = (long)(liTime.QuadPart / liFreq.QuadPart);
	usec = (long)(liTime.QuadPart * 1000000.0 / liFreq.QuadPart - sec * 1000000.0);
	return (LgTime)(sec + usec / 1000000.0);
}