#include "lg_keyevent.h"
#include "lg_memory.h"

struct LgKeyEvent {
	enum LgKey key;
	LgTime downTime;
};

LgKeyEventP LgKeyEventCreate() {
	LgKeyEventP event = (LgKeyEventP)LgMallocZ(sizeof(struct LgKeyEvent));
	return event;
}

void LgKeyEventDestroy(LgKeyEventP event) {
	LgFree(event);
}

void LgKeyEventDown(LgKeyEventP event, enum LgKey key) {
	event->key = key;
	event->downTime = LgGetTimeOfDay();
};

void LgKeyEventUp(LgKeyEventP event, enum LgKey key) {
	event->key = key;
}

enum LgKey LgKeyEventKey(LgKeyEventP event) {
	return event->key;
};

LgTime LgKeyEventDownTime(LgKeyEventP event) {
	return LgGetTimeOfDay() - event->downTime;
}