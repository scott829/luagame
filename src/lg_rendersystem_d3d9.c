﻿#include "lg_init.h"
#include "lg_rendersystem.h"
#include "lg_memory.h"
#include <d3dx9.h>
#include <d3dx9shader.h>
#include "shaders/d3d9_shaders.h"
#include "lg_blit.h"
#include "lg_imagecopy.h"
#include "lg_window_win32.h"

typedef struct LgD3dShader {
	ID3DXBuffer *			shaderBuf;
	IDirect3DPixelShader9 *	shader;
	ID3DXConstantTable *	constantTable;
	D3DXHANDLE				cxformMul, cxformAdd;
}*LgD3dShaderP;

struct LgVertexBufferD3d9 {
	IDirect3DVertexBuffer9 *vb;
};

struct LgIndexBufferD3d9 {
	IDirect3DIndexBuffer9 *ib;
};

struct LgTextureD3d9 {
	LG_REFCNT_HEAD
	IDirect3DTexture9 *	texture;
	enum LgImageFormat_	format;
};

struct LgRenderTargetD3d9 {
	struct LgTextureD3d9 *texture;
};

static IDirect3D9 *			g_d3d9 = NULL;
static IDirect3DDevice9 *	g_d3dDevice = NULL;
static IDirect3DSurface9 *	g_defaultTarget = NULL;
static D3DCAPS9				g_caps;
static struct LgCxform		g_cxform;
static struct LgD3dShader	g_shaderDiffuseCxform, g_shaderTextureCxform,
	g_shaderDiffuseTextureCxform, g_shaderCopyRenderTarget;
static LgBool				g_renderInTexture;

static void d3d9SetCxform(const LgCxformP cxform);
static void d3d9VertexBufferDestroy(LgVertexBufferP vb);
static void d3d9IndexBufferDestroy(LgIndexBufferP ib);
static void d3d9RenderTargetDestroy(LgRenderTargetP target);

// 初始化着色器
static void destroyShader(LgD3dShaderP shader) {
	if (shader->constantTable) shader->constantTable->lpVtbl->Release(shader->constantTable);
	if (shader->shader) shader->shader->lpVtbl->Release(shader->shader);
	if (shader->shaderBuf) shader->shaderBuf->lpVtbl->Release(shader->shaderBuf);
	shader->constantTable = NULL;
	shader->shader = NULL;
	shader->shaderBuf = NULL;
}

static LgBool initShader(LgD3dShaderP shader, const char *src) {
	LgZeroMemory(shader, sizeof(struct LgD3dShader));
	LG_CHECK_ERROR(SUCCEEDED(D3DXCompileShader(src, strlen(src), NULL, NULL, "ps_main", "ps_2_0", 0, 
		&shader->shaderBuf, NULL, NULL)));
	LG_CHECK_ERROR(SUCCEEDED(IDirect3DDevice9_CreatePixelShader(g_d3dDevice, 
		shader->shaderBuf->lpVtbl->GetBufferPointer(shader->shaderBuf), &shader->shader)));
	LG_CHECK_ERROR(SUCCEEDED(D3DXGetShaderConstantTable(shader->shaderBuf->lpVtbl->GetBufferPointer(shader->shaderBuf), 
		&shader->constantTable)));

	shader->cxformMul = shader->constantTable->lpVtbl->GetConstantByName(shader->constantTable, NULL, "cxform_mul");
	shader->cxformAdd = shader->constantTable->lpVtbl->GetConstantByName(shader->constantTable, NULL, "cxform_add");
	return LgTrue;

_error:
	destroyShader(shader);
	return LgFalse;
}

static void setShaderCxform(LgD3dShaderP shader, LgCxformP cxform) {
	if (shader->cxformMul) shader->constantTable->lpVtbl->SetFloatArray(shader->constantTable,
		g_d3dDevice, shader->cxformMul, cxform->mul, 4);
	if (shader->cxformAdd) shader->constantTable->lpVtbl->SetFloatArray(shader->constantTable,
		g_d3dDevice, shader->cxformAdd, cxform->add, 4);
}

static LgBool initShaders() {
	return initShader(&g_shaderDiffuseCxform, d3d9_shaderDiffuseCxform_fsh) &&
		initShader(&g_shaderTextureCxform, d3d9_shaderTextureCxform_fsh) &&
		initShader(&g_shaderDiffuseTextureCxform, d3d9_shaderDiffuseTextureCxform_fsh) &&
		initShader(&g_shaderCopyRenderTarget, d3d9_shaderCopyRenderTarget_fsh);
}

static void d3d9Close() {
	destroyShader(&g_shaderDiffuseCxform);
	destroyShader(&g_shaderDiffuseTextureCxform);
	destroyShader(&g_shaderTextureCxform);
	destroyShader(&g_shaderCopyRenderTarget);
	if (g_defaultTarget) {
		IDirect3DSurface9_Release(g_defaultTarget);
		g_defaultTarget = NULL;
	}
	if (g_d3dDevice) {
		IDirect3DDevice9_Release(g_d3dDevice);
		g_d3dDevice = NULL;
	}
	if (g_d3d9) {
		IDirect3D9_Release(g_d3d9);
		g_d3d9 = NULL;
	}
}

static void checkMultiSample(D3DMULTISAMPLE_TYPE *type, DWORD *quality) {
	int t;

	*type = D3DMULTISAMPLE_NONE;
	*quality = 0;

	for (t = D3DMULTISAMPLE_16_SAMPLES; t > 0; t--) {
		if (SUCCEEDED(IDirect3D9_CheckDeviceMultiSampleType(g_d3d9, D3DADAPTER_DEFAULT, 
			D3DDEVTYPE_HAL, D3DFMT_X8R8G8B8, TRUE, t, quality))) {
			*type = (D3DMULTISAMPLE_TYPE)t;
			(*quality)--;
			break;
		}
	}
}

static LgBool d3d9Open() {
	D3DPRESENT_PARAMETERS d3dpp;
	struct LgCxform defaultCxform;
	LgBool vsync = LsRenderSystemIsVSync();
	D3DMULTISAMPLE_TYPE multiSampleType = D3DMULTISAMPLE_NONE;
	DWORD multiSampleQuality = 0;

	g_d3d9 = Direct3DCreate9(D3D_SDK_VERSION);
	LG_CHECK_ERROR(g_d3d9);

	// 获得设备能力g_caps
	IDirect3D9_GetDeviceCaps(g_d3d9, D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &g_caps);

	if (g_caps.DevCaps && D3DDEVCAPS_HWTRANSFORMANDLIGHT) {
		checkMultiSample(&multiSampleType, &multiSampleQuality);
	}
	
	LgZeroMemory(&d3dpp, sizeof(d3dpp));
	d3dpp.Windowed = TRUE;
	d3dpp.hDeviceWindow = LgWindowHWND();
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D24S8;
	d3dpp.PresentationInterval = !vsync ? D3DPRESENT_INTERVAL_IMMEDIATE : 0;
	d3dpp.MultiSampleQuality = multiSampleQuality;
	d3dpp.MultiSampleType = multiSampleType;
	
	LG_CHECK_ERROR(SUCCEEDED(IDirect3D9_CreateDevice(g_d3d9, D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, NULL, 
		(g_caps.DevCaps && D3DDEVCAPS_HWTRANSFORMANDLIGHT) ? D3DCREATE_HARDWARE_VERTEXPROCESSING : D3DCREATE_SOFTWARE_VERTEXPROCESSING, 
		&d3dpp, &g_d3dDevice)));

	// 获取默认渲染目标
	IDirect3DDevice9_GetRenderTarget(g_d3dDevice, 0, &g_defaultTarget);

	// 初始化设备
	// 关闭深度测试
	IDirect3DDevice9_SetRenderState(g_d3dDevice, D3DRS_ZENABLE, FALSE);

	// 关闭模板测试
	IDirect3DDevice9_SetRenderState(g_d3dDevice, D3DRS_STENCILENABLE, FALSE);

	// 设置alpha混合方式
	IDirect3DDevice9_SetRenderState(g_d3dDevice, D3DRS_ALPHABLENDENABLE, TRUE);
	IDirect3DDevice9_SetRenderState(g_d3dDevice, D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	IDirect3DDevice9_SetRenderState(g_d3dDevice, D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	IDirect3DDevice9_SetRenderState(g_d3dDevice, D3DRS_BLENDOP, D3DBLENDOP_ADD);

	// 设置纹理过滤方式
	IDirect3DDevice9_SetSamplerState(g_d3dDevice, 0, D3DSAMP_MAGFILTER, D3DTEXF_ANISOTROPIC);
	IDirect3DDevice9_SetSamplerState(g_d3dDevice, 0, D3DSAMP_MINFILTER, D3DTEXF_ANISOTROPIC);
	IDirect3DDevice9_SetSamplerState(g_d3dDevice, 0, D3DSAMP_MAXANISOTROPY, 32);
	
	// 开启截取纹理寻址模式
	IDirect3DDevice9_SetSamplerState(g_d3dDevice, 0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
	IDirect3DDevice9_SetSamplerState(g_d3dDevice, 0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

	// 关闭灯光
	IDirect3DDevice9_SetRenderState(g_d3dDevice, D3DRS_LIGHTING, FALSE);

	// 关闭背面拣选
	IDirect3DDevice9_SetRenderState(g_d3dDevice, D3DRS_CULLMODE, D3DCULL_NONE);

	// 初始化着色器
	LG_CHECK_ERROR(initShaders());

	// 设置初始颜色变换矩阵
	LgCxformIndentity(&defaultCxform);
	d3d9SetCxform(&defaultCxform);

	g_renderInTexture = LgFalse;
	return LgTrue;

_error:
	d3d9Close();
	return LgFalse;
}

static void d3d9SetTransform(enum LgTransformType type, const LgMatrix44P m) {
	switch(type) {
	case LgTransformWorld:
		IDirect3DDevice9_SetTransform(g_d3dDevice, D3DTS_WORLD, (const D3DMATRIX *)m->m44);
		break;
	case LgTransformProjection:
		IDirect3DDevice9_SetTransform(g_d3dDevice, D3DTS_PROJECTION, (const D3DMATRIX *)m->m44);
		break;
	case LgTransformView:
		IDirect3DDevice9_SetTransform(g_d3dDevice, D3DTS_VIEW, (const D3DMATRIX *)m->m44);
		break;
	}
}

static void d3d9GetTransform(enum LgTransformType type, LgMatrix44P m) {
	switch(type) {
	case LgTransformWorld:
		IDirect3DDevice9_GetTransform(g_d3dDevice, D3DTS_WORLD, (D3DMATRIX *)m);
		break;
	case LgTransformProjection:
		IDirect3DDevice9_GetTransform(g_d3dDevice, D3DTS_PROJECTION, (D3DMATRIX *)m);
		break;
	case LgTransformView:
		IDirect3DDevice9_GetTransform(g_d3dDevice, D3DTS_VIEW, (D3DMATRIX *)m);
		break;
	}
}

static void d3d9SetCxform(const LgCxformP cxform) {
	if (cxform) LgCopyMemory(&g_cxform, cxform, sizeof(struct LgCxform));
	setShaderCxform(&g_shaderDiffuseCxform, cxform);
	setShaderCxform(&g_shaderTextureCxform, cxform);
	setShaderCxform(&g_shaderDiffuseTextureCxform, cxform);
	setShaderCxform(&g_shaderCopyRenderTarget, cxform);
}

static void d3d9GetCxform(LgCxformP cxform) {
	LgCopyMemory(cxform, &g_cxform, sizeof(struct LgCxform));
}

static LgVertexBufferP d3d9CreateVertexBuffer(int size) {
	struct LgVertexBufferD3d9 *vb;
	vb = (struct LgVertexBufferD3d9 *)LgMallocZ(sizeof(struct LgVertexBufferD3d9));
	LG_CHECK_ERROR(vb);
	LG_CHECK_ERROR(SUCCEEDED(IDirect3DDevice9_CreateVertexBuffer(g_d3dDevice, size, 0, 0, D3DPOOL_MANAGED, &vb->vb, NULL)));
	return vb;

_error:
	if (vb) d3d9VertexBufferDestroy(vb);
	return NULL;
}

static LgIndexBufferP d3d9CreateIndexBuffer(int count) {
	struct LgIndexBufferD3d9 *ib;
	ib = (LgIndexBufferP)LgMallocZ(sizeof(struct LgIndexBufferD3d9));
	LG_CHECK_ERROR(ib);
	LG_CHECK_ERROR(SUCCEEDED(IDirect3DDevice9_CreateIndexBuffer(g_d3dDevice, count * sizeof(unsigned short), 0, D3DFMT_INDEX16, D3DPOOL_MANAGED, &ib->ib, NULL)));
	return ib;

_error:
	if (ib) d3d9IndexBufferDestroy(ib);
	return NULL;
}

static D3DFORMAT d3dFormat(enum LgImageFormat_ format) {
	switch(format) {
	case LgImageFormatL8: return D3DFMT_L8;
	case LgImageFormatA8L8: return D3DFMT_A8L8;
	case LgImageFormatA8R8G8B8: return D3DFMT_A8R8G8B8;
	default: return D3DFMT_UNKNOWN;
	}
}

static void destroyTexture(LgTextureP texture) {
	struct LgTextureD3d9 *textured3d9 = (struct LgTextureD3d9 *)texture;
	if (textured3d9->texture) IDirect3DTexture9_Release(textured3d9->texture);
	LgFree(textured3d9);
}

static LgTextureP d3d9CreateTextureR(enum LgImageFormat_ format, const LgSizeP size) {
	struct LgTextureD3d9 *texture;
	texture = (struct LgTextureD3d9 *)LgMallocZ(sizeof(struct LgTextureD3d9));
	LG_CHECK_ERROR(texture);
	LG_REFCNT_INIT(texture, destroyTexture);
	texture->format = format;
	LG_CHECK_ERROR(SUCCEEDED(IDirect3DDevice9_CreateTexture(g_d3dDevice, size->width, size->height, 1, 0, d3dFormat(format), D3DPOOL_MANAGED, &texture->texture, NULL)));
	return texture;

_error:
	if (texture) LgTextureRelease(texture);
	return NULL;
}

static D3DPRIMITIVETYPE d3dPrimitiveType(enum LgPrimitiveType type) {
	switch(type) {
	case LgPrimitiveTriangleList: return D3DPT_TRIANGLELIST;
	case LgPrimitiveTriangleStrip: return D3DPT_TRIANGLESTRIP;
	case LgPrimitiveTriangleFan: return D3DPT_TRIANGLEFAN;
	default: return D3DPT_TRIANGLELIST;
	}
}

static int getStride(int fvf) {
	int size = 0;
	if (fvf & LgFvfXYZ) size += sizeof(float) * 3;
	if (fvf & LgFvfDiffuse) size += sizeof(LgColor);
	if (fvf & LgFvfTexture) size += sizeof(float) * 2;
	return size;
}

static int d3dFvf(int fvf) {
	DWORD d3dFvf = 0;

	if (fvf & LgFvfXYZ) d3dFvf |= D3DFVF_XYZ;
	if (fvf & LgFvfDiffuse) d3dFvf |= D3DFVF_DIFFUSE;
	if (fvf & LgFvfTexture) d3dFvf |= D3DFVF_TEX1;

	return d3dFvf;
}

static void d3d9DrawPrimitive(enum LgPrimitiveType type, int fvf, LgVertexBufferP vb, int primitiveCount) {
	struct LgVertexBufferD3d9 *vbd3d9 = (struct LgVertexBufferD3d9 *)vb;
	IDirect3DDevice9_SetFVF(g_d3dDevice, d3dFvf(fvf));
	IDirect3DDevice9_SetStreamSource(g_d3dDevice, 0, vbd3d9->vb, 0, getStride(fvf));
	IDirect3DDevice9_DrawPrimitive(g_d3dDevice, d3dPrimitiveType(type), 0, primitiveCount);
}

static int getVertexCount(enum LgPrimitiveType type, int count) {
	switch(type) {
	case LgPrimitiveTriangleList: return count * 3;
	case LgPrimitiveTriangleStrip: return count + 2;
	case LgPrimitiveTriangleFan: return count + 2;
	default: return 0;
	}
}

static void d3d9DrawIndexPrimitive(enum LgPrimitiveType type, int fvf, LgVertexBufferP vb, LgIndexBufferP ib, int primitiveCount, int vertexCount) {
	struct LgVertexBufferD3d9 *vbd3d9 = (struct LgVertexBufferD3d9 *)vb;
	struct LgIndexBufferD3d9 *ibd3d9 = (struct LgIndexBufferD3d9 *)ib;
	IDirect3DDevice9_SetFVF(g_d3dDevice, d3dFvf(fvf));
	IDirect3DDevice9_SetStreamSource(g_d3dDevice, 0, vbd3d9->vb, 0, getStride(fvf));
	IDirect3DDevice9_SetIndices(g_d3dDevice, ibd3d9->ib);
	IDirect3DDevice9_DrawIndexedPrimitive(g_d3dDevice, d3dPrimitiveType(type), 0, 0, vertexCount, 0, primitiveCount);
}

static void d3d9Begin(LgRenderTargetP target) {
	if (target) {
		struct LgRenderTargetD3d9 *targetd3d9 = (struct LgRenderTargetD3d9 *)target;
		IDirect3DSurface9 *surface;

		IDirect3DDevice9_EndScene(g_d3dDevice);
		IDirect3DDevice9_BeginScene(g_d3dDevice);
		IDirect3DTexture9_GetSurfaceLevel(targetd3d9->texture->texture, 0, &surface);
		IDirect3DDevice9_SetRenderTarget(g_d3dDevice, 0, surface);
		IDirect3DSurface9_Release(surface);
		g_renderInTexture = LgTrue;
	} else {
		IDirect3DDevice9_BeginScene(g_d3dDevice);
		IDirect3DDevice9_SetRenderTarget(g_d3dDevice, 0, g_defaultTarget);
		g_renderInTexture = LgFalse;
	}
}

static void d3d9End() {
	IDirect3DDevice9_EndScene(g_d3dDevice);

	if (!g_renderInTexture) {
		IDirect3DDevice9_Present(g_d3dDevice, NULL, NULL, NULL, NULL);
	} else {
		IDirect3DDevice9_BeginScene(g_d3dDevice);
		IDirect3DDevice9_SetRenderTarget(g_d3dDevice, 0, g_defaultTarget);
		g_renderInTexture = LgFalse;
	}
}

static void d3d9Clear(enum LgClearFlag flags, LgColor color, int stencil) {
	DWORD d3dClear = 0;

	if (flags & LgClearFlagColor) {
		d3dClear |= D3DCLEAR_TARGET;
		color = LgColorSwapRedBlue(color);
	}

	if (flags & LgClearFlagStencil) d3dClear |= D3DCLEAR_STENCIL;
	if (flags & LgClearFlagDepth) d3dClear |= D3DCLEAR_ZBUFFER;
	IDirect3DDevice9_Clear(g_d3dDevice, 0, NULL, d3dClear, color, 1024, stencil);
}

static void d3d9SetTexture(LgTextureP texture) {
	struct LgTextureD3d9 *textured3d9 = (struct LgTextureD3d9 *)texture;
	IDirect3DDevice9_SetTexture(g_d3dDevice, 0, texture ? (IDirect3DBaseTexture9 *)textured3d9->texture : NULL);
}

static void d3d9EnableStencil(LgBool value) {
	IDirect3DDevice9_SetRenderState(g_d3dDevice, D3DRS_STENCILENABLE, value);
}

static D3DCMPFUNC d3dCmp(enum LgStencilCompare cmp) {
	switch(cmp) {
	case LgStencilCompareNever: return D3DCMP_NEVER;
	case LgStencilCompareLess: return D3DCMP_LESS;
	case LgStencilCompareEqual: return D3DCMP_EQUAL;
	case LgStencilCompareLessEqual: return D3DCMP_LESSEQUAL;
	case LgStencilCompareGreater: return D3DCMP_GREATER;
	case LgStencilCompareNotEqual: return D3DCMP_NOTEQUAL;
	case LgStencilCompareGreaterEqual: return D3DCMP_GREATEREQUAL;
	case LgStencilCompareAlways: return D3DCMP_ALWAYS;
	default: return D3DCMP_NEVER;
	}
}

static D3DSTENCILOP d3dStencilOp(enum LgStencilOp op) {
	switch(op) {
	case LgStencilOpKeep: return D3DSTENCILOP_KEEP;
	case LgStencilOpZero: return D3DSTENCILOP_ZERO;
	case LgStencilOpReplace: return D3DSTENCILOP_REPLACE;
	case LgStencilOpIncrsat: return D3DSTENCILOP_INCRSAT;
	case LgStencilOpDecrsat: return D3DSTENCILOP_DECRSAT;
	case LgStencilOpInvert: return D3DSTENCILOP_INVERT;
	case LgStencilOpIncr: return D3DSTENCILOP_INCR;
	case LgStencilOpDecr: return D3DSTENCILOP_DECR;
	default: return D3DSTENCILOP_KEEP;
	}
}

static void d3d9ApplyStencil(int reference, enum LgStencilCompare cmp, enum LgStencilOp passOp, enum LgStencilOp failOp) {
	IDirect3DDevice9_SetRenderState(g_d3dDevice, D3DRS_STENCILREF, reference);
	IDirect3DDevice9_SetRenderState(g_d3dDevice, D3DRS_STENCILFUNC, d3dCmp(cmp));
	IDirect3DDevice9_SetRenderState(g_d3dDevice, D3DRS_STENCILPASS, d3dStencilOp(passOp));
	IDirect3DDevice9_SetRenderState(g_d3dDevice, D3DRS_STENCILFAIL, d3dStencilOp(failOp));
}

static int nearestPow2(int v) {
	v -= 1;
	v |= v >> 1;
	v |= v >> 2;
	v |= v >> 4;
	v |= v >> 8;
	v |= v >> 16;
	return v + 1;
}

static LgRenderTargetP d3d9CreateRenderTarget(const LgSizeP size) {
	struct LgRenderTargetD3d9 *target;
	int width, height;

	target = (struct LgRenderTargetD3d9 *)LgMallocZ(sizeof(struct LgRenderTargetD3d9));
	LG_CHECK_ERROR(target);
	width = LgMin(nearestPow2(size->width), 512);
	height = LgMin(nearestPow2(size->height), 512);

	target->texture = (struct LgTextureD3d9 *)LgMallocZ(sizeof(struct LgTextureD3d9));
	LG_CHECK_ERROR(target->texture);
	LG_REFCNT_INIT(target->texture, destroyTexture);
	target->texture->format = LgImageFormatA8R8G8B8;
	LG_CHECK_ERROR(SUCCEEDED(IDirect3DDevice9_CreateTexture(g_d3dDevice, width, height, 1, 
		D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &target->texture->texture, NULL)));
	return target;
	
_error:
	if (target) d3d9RenderTargetDestroy(target);
	return NULL;
}

static void d3d9SetPixelShader(enum LgPixelShaderType shaderType) {
	switch(shaderType) {
	case LgPixelShaderNone:
		IDirect3DDevice9_SetPixelShader(g_d3dDevice, NULL);
		break;
	case LgPixelShaderCxformDiffuse:
		IDirect3DDevice9_SetPixelShader(g_d3dDevice, g_shaderDiffuseCxform.shader);
		break;
	case LgPixelShaderCxformTexture:
		IDirect3DDevice9_SetPixelShader(g_d3dDevice, g_shaderTextureCxform.shader);
		break;
	case LgPixelShaderCxformDiffuseAndTexture:
		IDirect3DDevice9_SetPixelShader(g_d3dDevice, g_shaderDiffuseTextureCxform.shader);
		break;
	case LgPixelShaderCopyRenderTarget:
		IDirect3DDevice9_SetPixelShader(g_d3dDevice, g_shaderCopyRenderTarget.shader);
		break;
	}
}

static void d3d9VertexBufferDestroy(LgVertexBufferP vb) {
	struct LgVertexBufferD3d9 *vbd3d9 = (struct LgVertexBufferD3d9 *)vb;
	if (vbd3d9->vb) IDirect3DVertexBuffer9_Release(vbd3d9->vb);
	LgFree(vbd3d9);
}

static void *d3d9VertexBufferLock(LgVertexBufferP vb) {
	void *p = NULL;
	struct LgVertexBufferD3d9 *vbd3d9 = (struct LgVertexBufferD3d9 *)vb;
	IDirect3DVertexBuffer9_Lock(vbd3d9->vb, 0, 0, &p, 0);
	return p;
}

static void d3d9VertexBufferUnlock(LgVertexBufferP vb) {
	struct LgVertexBufferD3d9 *vbd3d9 = (struct LgVertexBufferD3d9 *)vb;
	IDirect3DVertexBuffer9_Unlock(vbd3d9->vb);
}

static int d3d9VertexBufferSize(LgVertexBufferP vb) {
	D3DVERTEXBUFFER_DESC desc;
	struct LgVertexBufferD3d9 *vbd3d9 = (struct LgVertexBufferD3d9 *)vb;
	IDirect3DVertexBuffer9_GetDesc(vbd3d9->vb, &desc);
	return (int)desc.Size;
}

static void d3d9IndexBufferDestroy(LgIndexBufferP ib) {
	struct LgIndexBufferD3d9 *ibd3d9 = (struct LgIndexBufferD3d9 *)ib;
	if (ibd3d9->ib) IDirect3DIndexBuffer9_Release(ibd3d9->ib);
	LgFree(ibd3d9);
}

static void *d3d9IndexBufferLock(LgIndexBufferP ib) {
	void *p = NULL;
	struct LgIndexBufferD3d9 *ibd3d9 = (struct LgIndexBufferD3d9 *)ib;
	IDirect3DIndexBuffer9_Lock(ibd3d9->ib, 0, 0, &p, 0);
	return p;
}

static void d3d9IndexBufferUnlock(LgIndexBufferP ib) {
	struct LgIndexBufferD3d9 *ibd3d9 = (struct LgIndexBufferD3d9 *)ib;
	IDirect3DIndexBuffer9_Unlock(ibd3d9->ib);
}

static int d3d9IndexBufferCount(LgIndexBufferP ib) {
	D3DINDEXBUFFER_DESC desc;
	struct LgIndexBufferD3d9 *ibd3d9 = (struct LgIndexBufferD3d9 *)ib;
	IDirect3DIndexBuffer9_GetDesc(ibd3d9->ib, &desc);
	return (int)desc.Size / sizeof(unsigned short);
}

static void d3d9TextureSize(LgTextureP texture, LgSizeP size) {
	D3DSURFACE_DESC desc;
	struct LgTextureD3d9 *textured3d9 = (struct LgTextureD3d9 *)texture;
	IDirect3DTexture9_GetLevelDesc(textured3d9->texture, 0, &desc);
	size->width = (int)desc.Width;
	size->height = (int)desc.Height;
}

static enum LgImageFormat_ d3d9TextureFormat(LgTextureP texture) {
	struct LgTextureD3d9 *textured3d9 = (struct LgTextureD3d9 *)texture;
	return textured3d9->format;
}

static LgBool d3d9TextureUpdate(LgTextureP texture, int destX, int destY, int srcW, int srcH, LgImageP image) {
	D3DLOCKED_RECT r;
	struct LgRect destRt, srcRt;
	struct LgTextureD3d9 *textured3d9 = (struct LgTextureD3d9 *)texture;

	if (FAILED(IDirect3DTexture9_LockRect(textured3d9->texture, 0, &r, NULL, 0))) return LgFalse;

	destRt.x = destX, destRt.y = destY;
	destRt.width = srcW, destRt.height = srcH;

	srcRt.x = 0, srcRt.y = 0;
	srcRt.width = srcW, srcRt.height = srcH;

#define COPY(method) LgImageCopyData(r.pBits, r.Pitch, textured3d9->format, &destRt, \
	LgImageData(image), LgImageLineSize(image), LgImageFormat(image), &srcRt, method)

	switch(textured3d9->format) {
	case LgImageFormatA8R8G8B8:
		switch(LgImageFormat(image)) {
		case LgImageFormatA8R8G8B8: COPY(LgBlit_A8R8G8B8_To_A8R8G8B8); break;
		case LgImageFormatA8L8: COPY(LgBlit_A8L8_To_A8R8G8B8); break;
		case LgImageFormatL8: COPY(LgBlit_L8_To_A8R8G8B8); break;
		}
		break;
	case LgImageFormatA8L8:
		switch(LgImageFormat(image)) {
		case LgImageFormatA8R8G8B8: COPY(LgBlit_A8R8G8B8_To_A8L8); break;
		case LgImageFormatA8L8: COPY(LgBlit_A8L8_To_A8L8); break;
		case LgImageFormatL8: COPY(LgBlit_L8_To_A8L8); break;
		}
		break;
	case LgImageFormatL8:
		switch(LgImageFormat(image)) {
		case LgImageFormatA8R8G8B8: COPY(LgBlit_A8R8G8B8_To_L8); break;
		case LgImageFormatA8L8: COPY(LgBlit_A8L8_To_L8); break;
		case LgImageFormatL8: COPY(LgBlit_L8_To_L8); break;
		}
		break;
	}

	IDirect3DTexture9_UnlockRect(textured3d9->texture, 0);
	return LgTrue;
}

static void d3d9RenderTargetDestroy(LgRenderTargetP target) {
	struct LgRenderTargetD3d9 *targetd3d9 = (struct LgRenderTargetD3d9 *)target;
	if (targetd3d9->texture) LgTextureRelease(targetd3d9->texture);
	LgFree(targetd3d9);
}

static void d3d9RenderTargetSize(LgRenderTargetP target, LgSizeP size) {
	struct LgRenderTargetD3d9 *targetd3d9 = (struct LgRenderTargetD3d9 *)target;
	d3d9TextureSize(targetd3d9->texture, size);
}

static LgTextureP d3d9RenderTargetTexture(LgRenderTargetP target) {
	struct LgRenderTargetD3d9 *targetd3d9 = (struct LgRenderTargetD3d9 *)target;
	return targetd3d9->texture;
}

struct LgRenderSystemDriver d3d9Driver = {
	"Direct3D9",
	d3d9Open,
	d3d9Close,
	d3d9SetTransform,
	d3d9GetTransform,
	d3d9SetCxform,
	d3d9GetCxform,
	d3d9CreateVertexBuffer,
	d3d9CreateIndexBuffer,
	d3d9CreateTextureR,
	d3d9DrawPrimitive,
	d3d9DrawIndexPrimitive,
	d3d9Begin,
	d3d9End,
	d3d9Clear,
	d3d9SetTexture,
	d3d9EnableStencil,
	d3d9ApplyStencil,
	d3d9CreateRenderTarget,
	d3d9SetPixelShader,

	d3d9VertexBufferDestroy,
	d3d9VertexBufferLock,
	d3d9VertexBufferUnlock,
	d3d9VertexBufferSize,

	d3d9IndexBufferDestroy,
	d3d9IndexBufferLock,
	d3d9IndexBufferUnlock,
	d3d9IndexBufferCount,

	d3d9TextureSize,
	d3d9TextureFormat,
	d3d9TextureUpdate,

	d3d9RenderTargetDestroy,
	d3d9RenderTargetSize,
	d3d9RenderTargetTexture,
};