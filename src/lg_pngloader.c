#include "lg_pngloader.h"
#include "stdio.h"
#include "libpng/png.h"

LgBool pngCheckType(const unsigned char *data, int size) {
	return size > 8 && 
		data[0] == 0x89 && 
		data[1] == 0x50 &&
		data[2] == 0x4e &&
		data[3] == 0x47 &&
		data[4] == 0x0d &&
		data[5] == 0x0a &&
		data[6] == 0x1a &&
		data[7] == 0x0a;
}

static void read(png_structp ctx, png_bytep area, png_size_t size) {
	LgInputStreamP in = (LgInputStreamP)png_get_io_ptr(ctx);
	LgInputStreamReadBytes(in, area, size);
}

LgImageP pngLoad(LgInputStreamP inputStream) {
	png_structp png = NULL;
	png_infop pngInfo = NULL;
	png_uint_32 width, height;
	int bitDepth, colorType;
	LgImageP img = NULL;
	struct LgSize size;
	int numberPasses, pass, row;

	png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	LG_CHECK_ERROR(png);
	pngInfo = png_create_info_struct(png);
	LG_CHECK_ERROR(png);

	png_set_read_fn(png, inputStream, read);
	png_read_info(png, pngInfo);
	png_get_IHDR(png, pngInfo, &width, &height, &bitDepth,
		&colorType, NULL, NULL, NULL);

	if (bitDepth == 16) png_set_strip_16(png);
	if (colorType == PNG_COLOR_TYPE_PALETTE) png_set_palette_to_rgb(png);

	if (bitDepth < 8) {
		if (colorType == PNG_COLOR_TYPE_GRAY || colorType == PNG_COLOR_TYPE_GRAY_ALPHA)
			png_set_gray_1_2_4_to_8(png);
		else
			png_set_packing(png);
	}

	png_set_add_alpha(png, 0xff, PNG_FILLER_AFTER);

	if (png_get_valid(png, pngInfo, PNG_INFO_tRNS)) png_set_tRNS_to_alpha(png);
	if (colorType & PNG_COLOR_TYPE_PALETTE) png_set_palette_to_rgb(png);
	if (colorType == PNG_COLOR_TYPE_GRAY || colorType == PNG_COLOR_TYPE_GRAY_ALPHA) png_set_gray_to_rgb(png);

	png_read_update_info(png, pngInfo);

	size.width = width, size.height = height;
	img = LgImageCreateR(&size, LgImageFormatA8R8G8B8);
	LG_CHECK_ERROR(img);

	numberPasses = png_set_interlace_handling(png);

	png_start_read_image(png);

	for (pass = 0; pass < numberPasses; pass++) {
		for (row = 0; row < (int)height; row++) {
			png_read_row(png, (png_bytep)LgImageLineData(img, row), NULL);
		}
	}

	png_read_end(png, pngInfo);
	png_destroy_read_struct(&png, &pngInfo, NULL);

	return img;

_error:
	if (img) LgImageRelease(img);
	if (png) png_destroy_read_struct(&png, &pngInfo, NULL);
	return NULL;
}
