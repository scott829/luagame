﻿#include "lg_gesture_recognizer.h"

struct LgGestureRecognizer {
	LG_REFCNT_HEAD
	LgNodeP node;
	void (* handleTouchEvent)(LgGestureRecognizerP gr, LgNodeTouchEventP evt);
	void (* update)(LgGestureRecognizerP gr, float delta);
};

LG_REFCNT_FUNCTIONS(LgGestureRecognizer)

void LgGestureRecognizerHandleTouchEvent(LgGestureRecognizerP gr, LgNodeTouchEventP evt) {
	gr->handleTouchEvent(gr, evt);
}

void LgGestureRecognizerUpdate(LgGestureRecognizerP gr, float delta) {
	if (gr->update) gr->update(gr, delta);
}

static void destroyGestureRecognizer(LgGestureRecognizerP gr) {
	LgFree(gr);
}

LgBool LgGestureRecognizerAttachNode(LgGestureRecognizerP gr, LgNodeP node) {
	if (gr->node) return LgFalse;
	gr->node = node;
	return LgTrue;
}

// tap ------------------------------------------------------------------------------------
#define TAP_INTERVAL 0.3f
#define TAP_DOWNTIME 0.2f

struct LgGestureRecognizerTap {
	struct LgGestureRecognizer base;
	int numberOfTapsRequired;
	int currentTap;
	LgTime downTime, previousTapTime;
	LgBool down;
	struct LgVector2 downPosition;
};

static void handleTouchEventTap(LgGestureRecognizerP gr, LgNodeTouchEventP evt) {
	struct LgGestureRecognizerTap *tap = (struct LgGestureRecognizerTap *)gr;
	
	if (evt->type == LgNodeTouchBegan) {
		if (evt->num == 1 &&
			LgNodeTouchesCount(tap->base.node) == 1) {
			// 触摸开始，并且触摸点个数等于1，而且节点内只有这一个触摸点
			tap->down = LgTrue;
			tap->downTime = LgGetTimeOfDay();
			LgTouchLocalPosition(evt->touches[0], &tap->downPosition);
		} else {
			tap->down = LgFalse;
		}
	} else if (evt->type == LgNodeTouchEnded) {
		if (tap->down) {
			LgTime now = LgGetTimeOfDay();
			struct LgVector2 downPosition;
			
			if (now - tap->downTime > TAP_DOWNTIME) {
				// 抬起超时了
				tap->down = LgFalse;
				tap->currentTap = 0;
				return;
			}
			
			LgTouchLocalPosition(evt->touches[0], &downPosition);
			
			if (LgVector2Distance(&tap->downPosition, &downPosition) > 10) {
				tap->down = LgFalse;
				tap->currentTap = 0;
				return;
			}

			if (tap->currentTap == 0) {
				// 是第一次
				tap->currentTap = 1;
				tap->previousTapTime = now;
			} else {
				// 是后面几次
				float interval = (float)(now - tap->previousTapTime);

				if (interval < TAP_INTERVAL) {
					tap->currentTap++;
					tap->previousTapTime = now;
				} else {
					tap->currentTap = 1;
					tap->previousTapTime = now;
				}
			}
			
			tap->down = LgFalse;
		}

		if (tap->currentTap == tap->numberOfTapsRequired) {
			// 触发消息
			struct LgGestureEvent evt;
			evt.type = LgGestureTap;
			evt.userdata = &tap->downPosition;
			LgSlotCall(LgNodeSlotGesture(gr->node), &evt);

			tap->currentTap = 0;
		}
	}
}

LgGestureRecognizerP LgGestureRecognizerTapCreate(int numberOfTapsRequired) {
	struct LgGestureRecognizerTap *gr = (struct LgGestureRecognizerTap *)LgMallocZ(sizeof(struct LgGestureRecognizerTap));
	LG_REFCNT_INIT(gr, destroyGestureRecognizer);
	gr->base.handleTouchEvent = handleTouchEventTap;
	gr->numberOfTapsRequired = numberOfTapsRequired;
	return (LgGestureRecognizerP)gr;
}

// long press --------------------------------------------------------------------------------------------------------
#define LONG_PRESS_TIME 1.0f

struct LgGestureRecognizerLongPress {
	struct LgGestureRecognizer base;
	LgTime downTime;
	LgBool down;
	struct LgVector2 downPosition;
	float duration;
};

static void handleTouchEventLongPress(LgGestureRecognizerP gr, LgNodeTouchEventP evt) {
	struct LgGestureRecognizerLongPress *longpress = (struct LgGestureRecognizerLongPress *)gr;

	if (evt->type == LgNodeTouchBegan) {
		if (evt->num == 1 &&
			LgNodeTouchesCount(longpress->base.node) == 1) {
				// 触摸开始，并且触摸点个数等于1，而且节点内只有这一个触摸点
				longpress->down = LgTrue;
				longpress->downTime = LgGetTimeOfDay();
				LgTouchLocalPosition(evt->touches[0], &longpress->downPosition);
		} else {
			longpress->down = LgFalse;
		}
	} else if (evt->type == LgNodeTouchEnded) {
		if (longpress->down) {
			LgTime now = LgGetTimeOfDay();
			struct LgVector2 downPosition;

			if (now - longpress->downTime < LONG_PRESS_TIME) {
				// 没有达到规定的长按时间
				longpress->down = LgFalse;
				return;
			}

			LgTouchLocalPosition(evt->touches[0], &downPosition);

			if (LgVector2Distance(&longpress->downPosition, &downPosition) > 10) {
				// 偏移了
				longpress->down = LgFalse;
				return;
			} else {
				// 触发消息
				struct LgGestureEvent evt;
				evt.type = LgGestureLongPress;
				evt.userdata = &longpress->downPosition;
				LgSlotCall(LgNodeSlotGesture(gr->node), &evt);
			}
		}
	}
}

LgGestureRecognizerP LgGestureRecognizerLongPressCreate() {
	struct LgGestureRecognizerLongPress *gr = (struct LgGestureRecognizerLongPress *)LgMallocZ(sizeof(struct LgGestureRecognizerLongPress));
	LG_REFCNT_INIT(gr, destroyGestureRecognizer);
	gr->base.handleTouchEvent = handleTouchEventLongPress;
	return (LgGestureRecognizerP)gr;
}

// pinch --------------------------------------------------------------------------------------------------------
struct LgGestureRecognizerPinch {
	struct LgGestureRecognizer base;
	LgBool start;
	float startDistance; // 起始距离
	float currentDistance; // 当前距离
};

static void handleTouchEventPinch(LgGestureRecognizerP gr, LgNodeTouchEventP evt) {
	struct LgGestureRecognizerPinch *pinch = (struct LgGestureRecognizerPinch *)gr;

	if (evt->type == LgNodeTouchBegan) {
		if (LgNodeTouchesCount(pinch->base.node) == 2) {
			LgTouchP touch1 = evt->touches[0], touch2 = evt->touches[1];
			struct LgVector2 pt1, pt2;

			LgTouchLocalPosition(touch1, &pt1);
			LgTouchLocalPosition(touch2, &pt2);
			pinch->startDistance = pinch->currentDistance = LgVector2Distance(&pt1, &pt2);
			pinch->start = LgTrue;
		} else {
			pinch->start = LgFalse;
		}
	} else if (evt->type == LgNodeTouchMoved && pinch->start) {
		LgTouchP touch1 = evt->touches[0], touch2 = evt->touches[1];
		struct LgVector2 pt1, pt2;
		struct LgGesturePinchEvent evtData;
		struct LgGestureEvent evt;

		LgTouchLocalPosition(touch1, &pt1);
		LgTouchLocalPosition(touch2, &pt2);
		pinch->currentDistance = LgVector2Distance(&pt1, &pt2);

		evtData.startDistance = pinch->startDistance;
		evtData.currentDistance = pinch->currentDistance;

		evt.type = LgGesturePinch;
		evt.userdata = &evtData;
		LgSlotCall(LgNodeSlotGesture(gr->node), &evt);
	}
}

LgGestureRecognizerP LgGestureRecognizerPinchCreate() {
	struct LgGestureRecognizerLongPress *gr = (struct LgGestureRecognizerLongPress *)LgMallocZ(sizeof(struct LgGestureRecognizerLongPress));
	LG_REFCNT_INIT(gr, destroyGestureRecognizer);
	gr->base.handleTouchEvent = handleTouchEventPinch;
	return (LgGestureRecognizerP)gr;
}

// swpie --------------------------------------------------------------------------------------------------------

struct LgGestureRecognizerSwipe {
	struct LgGestureRecognizer base;
	struct LgVector2 startPosition;
	LgBool start;
};

static void handleTouchEventSwipe(LgGestureRecognizerP gr, LgNodeTouchEventP evt) {
	struct LgGestureRecognizerSwipe *swipe = (struct LgGestureRecognizerSwipe *)gr;
	
	if (evt->type == LgNodeTouchBegan) {
		if (LgNodeTouchesCount(swipe->base.node) == 1) {
			swipe->start = LgTrue;
			LgTouchLocalPosition(evt->touches[0], &swipe->startPosition);
		} else {
			swipe->start = LgFalse;
		}
	} else if (evt->type == LgNodeTouchMoved && swipe->start) {
		struct LgVector2 currentPosition;
		float distance;
		
		LgTouchLocalPosition(evt->touches[0], &currentPosition);
		distance = LgVector2Distance(&swipe->startPosition, &currentPosition);
		
		if (distance > 50) {
			enum LgGestureSwipeDirection d;
			LgBool r = LgFalse;
			float angle = LgVector2ToVector2Angle(&swipe->startPosition, &currentPosition);
			struct LgGestureEvent evt;

			if (angle > -20 && angle < 20) {
				d = LgGestureSwipeRight;
				r = LgTrue;
			} else if (angle > 70 && angle < 110) {
				d = LgGestureSwipeDown;
				r = LgTrue;
			} else if ((angle > 160 && angle < 180) || (angle > -180 && angle < -160)) {
				d = LgGestureSwipeLeft;
				r = LgTrue;
			} else if (angle < -70 && angle > -110) {
				d = LgGestureSwipeUp;
				r = LgTrue;
			}

			if (r) {
				evt.type = LgGestureSwipe;
				evt.userdata = (void *)d;
				LgSlotCall(LgNodeSlotGesture(gr->node), &evt);
			}

			swipe->start = LgFalse;
		}
	}
}

LgGestureRecognizerP LgGestureRecognizerSwipeCreate() {
	struct LgGestureRecognizerSwipe *gr = (struct LgGestureRecognizerSwipe *)LgMallocZ(sizeof(struct LgGestureRecognizerSwipe));
	LG_REFCNT_INIT(gr, destroyGestureRecognizer);
	gr->base.handleTouchEvent = handleTouchEventSwipe;
	return (LgGestureRecognizerP)gr;
}