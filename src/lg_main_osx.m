#import <Cocoa/Cocoa.h>
#include "lg_init.h"
#include "lg_window_osx.h"

@interface LgMainTimer : NSObject
{
}

- (void)onTimer : (NSTimer *)timer;

@end

@implementation LgMainTimer

- (void)onTimer : (NSTimer *)timer {
	if (!LgMainLoop()) [NSApp stop:nil];
}

@end


int main(int argc, char **argv) {
	@autoreleasepool {
		NSApp = [NSApplication sharedApplication];
		LgMainTimer *timerTarget;
	
		if (!LgInit(argc, argv)) {
			printf("Failed to initialize.\n");
			return -1;
		}
	
		timerTarget = [[LgMainTimer alloc] autorelease];
		NSTimer *timer = [NSTimer timerWithTimeInterval:1.0f / 60 target:timerTarget selector:@selector(onTimer:) userInfo:nil repeats:TRUE];
		[[NSRunLoop mainRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
		[LgWindowNSWindow() makeKeyAndOrderFront:nil];
		[LgWindowNSWindow() center];
		[NSApp run];
		LgCleanup();
	}
}
