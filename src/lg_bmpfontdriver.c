#include "lg_bmpfontdriver.h"
#include "lg_memory.h"
#include "lg_textureatlas.h"
#include "lg_dict.h"
#include "lg_platform.h"
#include "lg_archive.h"
#include "lg_image.h"
#include "lg_imagecopy.h"
#include "lg_meminputstream.h"
#include "lg_rendersystem.h"
#include <wchar.h>

struct LgBmpCharInfo {
	int xoffset, yoffset, xadvance;
};

typedef struct LgBtmapFontData {
	LgTextureAtlasP	atlas;
	int				lineHeight, baseline;
	LgDictP			charInfo;
}*LgBtmapFontDataP;

struct LineInfo {
	const char *typeStart, *typeEnd;
	int countAttrs;

	struct Attribute {
		const char *nameStart, *nameEnd;
		const char *valueStart, *valueEnd;
	}attrs[256];
};

struct LgBmFontDrawBuffer {
	LgAtlasDrawBufferP	drawBuffer;
	int					baseline;
};

static void skipSpace(const char **line) {
	while(**line == ' ' || **line == '\t') (*line)++;
}

static void parseAttrs(const char *line, struct LineInfo *lineInfo) {
	while(LgTrue) {
		skipSpace(&line);
		if (*line == 0) break;

		lineInfo->attrs[lineInfo->countAttrs].nameStart = line;
		while(*line && *line != '=') line++;
		if (*line != '=') break;
		lineInfo->attrs[lineInfo->countAttrs].nameEnd = line;

		line++;

		if (*line == '\"') {
			line++;
			lineInfo->attrs[lineInfo->countAttrs].valueStart = line;
			while(*line && *line != '\"') line++;
			if (*line != '\"') break;
			lineInfo->attrs[lineInfo->countAttrs].valueEnd = line;
			line++;
		} else {
			lineInfo->attrs[lineInfo->countAttrs].valueStart = line;
			while(*line && *line != ' ') line++;
			lineInfo->attrs[lineInfo->countAttrs].valueEnd = line;
		}

		lineInfo->countAttrs++;
	}
}

static void parseLine(const char *line, struct LineInfo *lineInfo) {
	lineInfo->countAttrs = 0;

	// 解析类型
	lineInfo->typeStart = line;
	while(*line && *line != ' ') line++;
	lineInfo->typeEnd = line;

	parseAttrs(line, lineInfo);
}

static LgBool getStringValue(struct LineInfo *lineInfo, const char *name, char *value, int len) {
	int i;
	for (i = 0; i < lineInfo->countAttrs; i++) {
		char c;
		LgBool eq;
		c = *((char *)lineInfo->attrs[i].nameEnd);
		*((char *)lineInfo->attrs[i].nameEnd) = 0;
		eq = strnocasecmp(lineInfo->attrs[i].nameStart, name) == 0;
		*((char *)lineInfo->attrs[i].nameEnd) = c;

		if (eq) {
			c = *((char *)lineInfo->attrs[i].valueEnd);
			*((char *)lineInfo->attrs[i].valueEnd) = 0;
			strlcpy(value, lineInfo->attrs[i].valueStart, len);
			*((char *)lineInfo->attrs[i].valueEnd) = c;
			return LgTrue;
		}
	}

	return LgFalse;
}

static LgBool getIntValue(struct LineInfo *lineInfo, const char *name, int *value) {
	int i;
	for (i = 0; i < lineInfo->countAttrs; i++) {
		char c;
		LgBool eq;
		c = *((char *)lineInfo->attrs[i].nameEnd);
		*((char *)lineInfo->attrs[i].nameEnd) = 0;
		eq = strnocasecmp(lineInfo->attrs[i].nameStart, name) == 0;
		*((char *)lineInfo->attrs[i].nameEnd) = c;

		if (eq) {
			c = *((char *)lineInfo->attrs[i].valueEnd);
			*((char *)lineInfo->attrs[i].valueEnd) = 0;
			*value = atoi(lineInfo->attrs[i].valueStart);
			*((char *)lineInfo->attrs[i].valueEnd) = c;
			return LgTrue;
		}
	}

	return LgFalse;
}

static void destroyFont(LgFontDataP font) {
	LgBtmapFontDataP bmfont = (LgBtmapFontDataP)font;
	if (bmfont->atlas) LgTextureAtlasRelease(bmfont->atlas);
	if (bmfont->charInfo) LgDictDestroy(bmfont->charInfo);
	LgFree(font);
}

static LgBool loadFont(LgBtmapFontDataP bmfont, const char *filename) {
	LgInputStreamP input;
	char fntPath[LG_PATH_MAX];
	char line[1024], c;
	int i, pageToTextureId[128];
	struct LineInfo info;

	strcpy(fntPath, "fonts/");
	strcat(fntPath, filename);

#define IS_VALID_PAGEID(id) (id >= 0 && id < sizeof(pageToTextureId) / sizeof(int))
	
	input = LgArchiveOpenFile(fntPath);
	if (!input) return LgFalse;

	for (i = 0; i < sizeof(pageToTextureId) / sizeof(int); i++) {
		pageToTextureId[i] = -1;
	}

	while(LgTrue) {
		LgInputStreamReadLine(input, line, sizeof(line));
		if (line[0] == 0) break;

		parseLine(line, &info);

		if (info.typeEnd > info.typeStart) {
			char type[32];
			
			c = *((char *)info.typeEnd);
			*((char *)info.typeEnd) = 0;
			strlcpy(type, info.typeStart, sizeof(type));
			*((char *)info.typeEnd) = c;
			
			if (strnocasecmp(type, "common") == 0) {
				getIntValue(&info, "lineHeight", &bmfont->lineHeight);
				getIntValue(&info, "base", &bmfont->baseline);
			} else if (strnocasecmp(type, "page") == 0) {
				char imagePath[LG_PATH_MAX], imageFilename[256];

				if (getStringValue(&info, "file", imageFilename, sizeof(imageFilename))) {
					LgImageP img;
					int id;

					strcpy(imagePath, "fonts/");
					strcat(imagePath, imageFilename);

					// 这里不使用LgTextureAtlasAddTextureFromFile是因为resourcemanager还未初始化
					img = LgImageCreateFromFileR(imagePath);
					if (img) {
						LgTextureP texture = LgImageCreateTextureR(img);
						if (texture) {
							if (getIntValue(&info, "id", &id) && IS_VALID_PAGEID(id)) {
								pageToTextureId[id] = LgTextureAtlasAddTexture(bmfont->atlas, texture);
							}
							LgTextureRelease(texture);
						}
						LgImageRelease(img);
					}
				}
			} else if (strnocasecmp(type, "char") == 0) {
				int charId, x, y, width, height, xoffset, yoffset, xadvance, page;
				struct LgTextAtlasRecord record;
				struct LgBmpCharInfo *charInfo;

				getIntValue(&info, "id", &charId);
				getIntValue(&info, "x", &x);
				getIntValue(&info, "y", &y);
				getIntValue(&info, "width", &width);
				getIntValue(&info, "height", &height);
				getIntValue(&info, "xoffset", &xoffset);
				getIntValue(&info, "yoffset", &yoffset);
				getIntValue(&info, "xadvance", &xadvance);
				getIntValue(&info, "page", &page);

				if (IS_VALID_PAGEID(page) && pageToTextureId[page] != -1) {
					record.textureId = pageToTextureId[page];
					record.pt.x = x, record.pt.y = y;
					record.size.width = width, record.size.height = height;
					LgTextureAtlasAddRecord(bmfont->atlas, charId, &record);

					charInfo = (struct LgBmpCharInfo *)LgMallocZ(sizeof(struct LgBmpCharInfo));
					charInfo->xoffset = xoffset;
					charInfo->yoffset = yoffset;
					charInfo->xadvance = xadvance;
					LgDictInsert(bmfont->charInfo, (void *)charId, charInfo);
				}
			}
		}
	}

	LgInputStreamDestroy(input);
	return LgTrue;
}

static LgFontDataP createFont(const char *filename) {
	LgBtmapFontDataP font = (LgBtmapFontDataP)LgMallocZ(sizeof(struct LgBtmapFontData));
	LG_CHECK_ERROR(font);
	font->charInfo = LgDictCreate(NULL, NULL, LgFree);
	LG_CHECK_ERROR(font->charInfo);
	font->atlas = LgTextureAtlasCreateR();
	LG_CHECK_ERROR(font->atlas);
	LG_CHECK_ERROR(loadFont(font, filename));
	return font;

_error:
	if (font) destroyFont(font);
	return NULL;
}

static void metrics(LgFontDataP font, int fontsize, LgFontMetricsP metrics) {
	LgBtmapFontDataP bmfont = (LgBtmapFontDataP)font;
	metrics->ascent = (float)bmfont->baseline;
	metrics->descent = (float)(bmfont->lineHeight - bmfont->baseline);
}

static float kerning(LgFontDataP font, int fontsize, int leftChar, int rightChar) {
	return 0;
}

static float height(LgFontDataP font, int fontsize) {
	LgBtmapFontDataP bmfont = (LgBtmapFontDataP)font;
	return (float)bmfont->lineHeight;
}

static float stringWidth(LgFontDataP font, int fontsize, const wchar_t *text, int textLen) {
	float width = 0;
	LgBtmapFontDataP bmfont = (LgBtmapFontDataP)font;

	if (text) {
		int i;
		if (textLen == -1) textLen = wcslen(text);
		
		for (i = 0; i < textLen; i++) {
			struct LgBmpCharInfo *charInfo;
			wchar_t wc = text[i];
			charInfo = (struct LgBmpCharInfo *)LgDictFind(bmfont->charInfo, (void *)wc);
			width += charInfo ? charInfo->xadvance : 0;
		}

		return width;
	} else {
		return 0;
	}
}

static void charsWidth(LgFontDataP font, int fontsize, const wchar_t *text, int textLen, int *width) {
	LgBtmapFontDataP bmfont = (LgBtmapFontDataP)font;
	if (text) {
		int i;
		if (textLen == -1) textLen = wcslen(text);

		for (i = 0; i < textLen; i++) {
			struct LgBmpCharInfo *charInfo;
			wchar_t wc = text[i];
			charInfo = (struct LgBmpCharInfo *)LgDictFind(bmfont->charInfo, (void *)wc);
			width[i] = charInfo ? charInfo->xadvance : 0;
		}
	}
}

static void destroyStringBuffer(LgDrawBufferP buffer) {
	struct LgBmFontDrawBuffer *dbuf = (struct LgBmFontDrawBuffer *)buffer;
	if (dbuf->drawBuffer) LgAtlasDrawBufferRelease(dbuf->drawBuffer);
	LgFree(dbuf);
}

static LgDrawBufferP createStringBuffer(LgFontDataP font, int fontsize, const wchar_t *text, int textLen) {
	LgBtmapFontDataP bmfont = (LgBtmapFontDataP)font;
	struct LgBmFontDrawBuffer *drawBuffer = NULL;
	float drawX = 0;
	int i;

	if (textLen == -1) textLen = wcslen(text);

	drawBuffer = (struct LgBmFontDrawBuffer *)LgMallocZ(sizeof(struct LgBmFontDrawBuffer));
	LG_CHECK_ERROR(drawBuffer);
	drawBuffer->baseline = bmfont->baseline;
	drawBuffer->drawBuffer = LgTextureAtlasCreateDrawBufferR(bmfont->atlas);
	LG_CHECK_ERROR(drawBuffer->drawBuffer);

	for (i = 0; i < textLen; i++){
		struct LgBmpCharInfo *charInfo;
		struct LgTextAtlasRecord record;
		struct LgAabb aabb;
		wchar_t wc = text[i];

		charInfo = (struct LgBmpCharInfo *)LgDictFind(bmfont->charInfo, (void *)wc);

		if (charInfo && LgTextureAtlasRecord(bmfont->atlas, wc, &record)) {
			aabb.x = drawX + (float)charInfo->xoffset, aabb.y = (float)charInfo->yoffset;
			aabb.width = (float)record.size.width, aabb.height = (float)record.size.height;
			LgAtlasBufferDrawRect(drawBuffer->drawBuffer, wc, &aabb);
			drawX += charInfo->xadvance;
		}
	}

	return drawBuffer;

_error:
	if (drawBuffer) destroyStringBuffer(drawBuffer);
	return NULL;
}

static void draw(LgDrawBufferP buffer, float x, float y, LgColor color, enum LgDrawStringFlag flags) {
	struct LgBmFontDrawBuffer *dbuf = (struct LgBmFontDrawBuffer *)buffer;
	if (dbuf->drawBuffer) {
		struct LgMatrix44 oldMatrix, newMatrix;

		LgRenderSystemGetTransform(LgTransformWorld, &oldMatrix);
		LgMatrix44InitTranslate(&newMatrix, x, y + ((flags & LgDrawStringBaseline) ? -dbuf->baseline : 0), 0);
		LgMatrix44Mul(&newMatrix, &oldMatrix);
		LgAtlasBufferDraw(dbuf->drawBuffer);
		LgRenderSystemSetTransform(LgTransformWorld, &oldMatrix);
	}
}

static int numberOfbatch(LgDrawBufferP buffer) {
	struct LgBmFontDrawBuffer *dbuf = (struct LgBmFontDrawBuffer *)buffer;
	return LgAtlasBufferNumberOfBatch(dbuf->drawBuffer);
}

struct LgFontDriver bmpFontDriver = {
	"bitmap",
	NULL,
	NULL,
	createFont,
	destroyFont,
	createStringBuffer,
	destroyStringBuffer,
	metrics,
	stringWidth,
	charsWidth,
	kerning,
	height,
	draw,
	numberOfbatch
};
