#include "lg_glescontext_android.h"
#include "lg_window_android.h"

static EGLDisplay g_display = NULL;
static EGLSurface g_surface = NULL;
static EGLContext g_context = NULL;

LgBool LgGlContextOpen() {
	const EGLint attribs[] = {
		EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
		EGL_BLUE_SIZE, 8,
		EGL_GREEN_SIZE, 8,
		EGL_RED_SIZE, 8,
		EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
		EGL_NONE
	};

	const EGLint aEGLContextAttributes[] = {
		EGL_CONTEXT_CLIENT_VERSION, 2,
		EGL_NONE
	};

	EGLint w, h, dummy, format;
	EGLint numConfigs;
	EGLConfig config;

	g_display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
	eglInitialize(g_display, 0, 0);
	eglChooseConfig(g_display, attribs, &config, 1, &numConfigs);
	eglGetConfigAttrib(g_display, config, EGL_NATIVE_VISUAL_ID, &format);
	ANativeWindow_setBuffersGeometry(LgWindowAWindow(), 0, 0, format);

	g_surface = eglCreateWindowSurface(g_display, config, LgWindowAWindow(), NULL);
	g_context = eglCreateContext(g_display, config, NULL, aEGLContextAttributes);
	if (eglMakeCurrent(g_display, g_surface, g_surface, g_context) == EGL_FALSE) {
		return LgFalse;
	}

	return LgTrue;
}

void LgGlContextClose() {
	if (g_context) eglDestroyContext(g_display, g_context);
	if (g_surface) eglDestroySurface(g_display, g_surface);
	if (g_display) eglTerminate(g_display);
}

void LgGlContextBegin() {
	eglMakeCurrent(g_display, g_surface, g_surface, g_context);
}

void LgGlContextEnd() {
	eglSwapBuffers(g_display, g_surface);
	eglMakeCurrent(g_display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
}
