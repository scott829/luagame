#include "lg_layer.h"
#include "lg_director.h"

LgSlot *LgLayerSlotKeyDown(LgNodeP node) {
	LgSlot *slot = NULL;
	LgNodeDoEvent(LG_LAYER_EVENT_KeyDownEvent, node, &slot);
	return slot;
}

LgSlot *LgLayerSlotKeyUp(LgNodeP node) {
	LgSlot *slot = NULL;
	LgNodeDoEvent(LG_LAYER_EVENT_KeyUpEvent, node, &slot);
	return slot;
}

void LgLayerEventProc(LgEventType type, LgNodeP node, void *param) {
	switch(type) {
	case LG_NODE_EVENT_Create:
		{
			struct LgSize viewSize;
			struct LgAabb aabb;
			struct LgVector2 anchor;

			LgDirectorViewSize(&viewSize);
			aabb.x = aabb.y = 0;
			aabb.width = (float)viewSize.width, aabb.height = (float)viewSize.height;

			LgAabbCenterPoint(&aabb, &anchor);
			LgNodeSetTransformAnchor(node, &anchor);
		}
		break;
	case LG_NODE_EVENT_Destroy:
		{
			LgLayerDataP ld = (LgLayerDataP)LgNodeUserData(node);
			LgLayerDataDestroy(ld);
		}
		break;
	case LG_NODE_EVENT_ContentBox:
		{
			struct LgSize viewSize;
			LgAabbP aabb = (LgAabbP)param;

			LgDirectorViewSize(&viewSize);
			aabb->x = aabb->y = 0;
			aabb->width = (float)viewSize.width, aabb->height = (float)viewSize.height;
		}
		return;
	case LG_LAYER_EVENT_KeyDownEvent:
		{
			LgLayerDataP ld = (LgLayerDataP)LgNodeUserData(node);
			*((LgSlot **)param) = &ld->slotKeyDown;
		}
		break;
	case LG_LAYER_EVENT_KeyUpEvent:
		{
			LgLayerDataP ld = (LgLayerDataP)LgNodeUserData(node);
			*((LgSlot **)param) = &ld->slotKeyUp;
		}
		break;
	case LG_NODE_EVENT_IsLayer:
		*((LgBool *)param) = LgTrue;
		return;
	case LG_NODE_EVENT_ClearSlots:
		{
			LgLayerDataP ld = (LgLayerDataP)LgNodeUserData(node);
			LgSlotClear(&ld->slotKeyDown);
			LgSlotClear(&ld->slotKeyUp);
		}
		break;
	}

	LgNodeDefEventProc(type, node, param);
}

void LgLayerDataDestroy(LgLayerDataP data) {
	LgSlotClear(&data->slotKeyDown);
	LgSlotClear(&data->slotKeyUp);
	LgFree(data);
}

LgLayerDataP LgLayerDataCreate(size_t size) {
	LgLayerDataP ld = NULL;

	if (size == 0) size = sizeof(struct LgLayerData);
	ld = (LgLayerDataP)LgMallocZ(size);
	return ld;
}

LgNodeP LgLayerCreateR() {
	LgNodeP node = NULL;
	LgLayerDataP ld = NULL;

	node = LgNodeCreateR((LgEvent)LgLayerEventProc);
	LG_CHECK_ERROR(node);

	ld = LgLayerDataCreate(0);
	LG_CHECK_ERROR(ld);
	LgNodeSetUserData(node, ld);

	return node;

_error:
	if (node) LgNodeRelease(node);
	return NULL;
}

LgBool LgNodeIsBaseLayer(LgNodeP node) {
	return LgNodeEventProc(node) == (LgEvent)LgLayerEventProc;
}
