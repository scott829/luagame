const char *opengl_shaderDiffuseCxform_vsh = "			\
attribute vec4 dz_position;						\
attribute vec4 dz_color;						\
varying vec4 v_fragmentColor;					\
uniform mat4 dz_MVPMatrix;						\
												\
void main()										\
{												\
	gl_Position = dz_MVPMatrix * dz_position;	\
	v_fragmentColor = dz_color;					\
}												\
";

const char *opengl_shaderDiffuseCxform_fsh = "			\
uniform vec4 dz_cxformMul;						\
uniform vec4 dz_cxformAdd;						\
varying vec4 v_fragmentColor;					\
												\
void main()										\
{												\
	gl_FragColor = v_fragmentColor;				\
	gl_FragColor *= dz_cxformMul;				\
	gl_FragColor += dz_cxformAdd;				\
}												\
";

const char *opengl_shaderTextureCxform_vsh = "			\
attribute vec4 dz_position;						\
attribute vec2 dz_texcrood;						\
uniform mat4 dz_MVPMatrix;						\
varying vec2 v_texcoord;						\
												\
void main()										\
{												\
	gl_Position = dz_MVPMatrix * dz_position;	\
	v_texcoord = dz_texcrood;					\
}												\
";

const char *opengl_shaderTextureCxform_fsh = "			\
uniform vec4 dz_cxformMul;						\
uniform vec4 dz_cxformAdd;						\
uniform sampler2D dz_texture;					\
varying vec2 v_texcoord;						\
												\
void main()										\
{												\
	gl_FragColor = texture2D(					\
		dz_texture, v_texcoord);				\
	gl_FragColor *= dz_cxformMul;				\
	gl_FragColor += dz_cxformAdd;				\
}												\
";

const char *opengl_shaderDiffuseTextureCxform_vsh = "	\
attribute vec4 dz_position;						\
attribute vec4 dz_color;						\
attribute vec2 dz_texcrood;						\
uniform mat4 dz_MVPMatrix;						\
varying vec2 v_texcoord;						\
varying vec4 v_fragmentColor;					\
												\
void main()										\
{												\
	gl_Position = dz_MVPMatrix * dz_position;	\
	v_texcoord = dz_texcrood;					\
	v_fragmentColor = dz_color;					\
}												\
";

const char *opengl_shaderDiffuseTextureCxform_fsh = "	\
uniform vec4 dz_cxformMul;						\
uniform vec4 dz_cxformAdd;						\
uniform sampler2D dz_texture;					\
varying vec2 v_texcoord;						\
varying vec4 v_fragmentColor;					\
												\
void main()										\
{												\
	gl_FragColor = v_fragmentColor * texture2D(	\
							 dz_texture, v_texcoord);				\
	gl_FragColor *= dz_cxformMul;				\
	gl_FragColor += dz_cxformAdd;				\
}												\
";

const char *opengl_shaderCopyRenderTarget_vsh = "		\
attribute vec4 dz_position;						\
attribute vec2 dz_texcrood;						\
uniform mat4 dz_MVPMatrix;						\
varying vec2 v_texcoord;						\
												\
void main()										\
{												\
	gl_Position = dz_MVPMatrix * dz_position;	\
	v_texcoord = vec2(dz_texcrood[0],			\
		1.0 - dz_texcrood[1]);					\
}												\
";

const char *opengl_shaderCopyRenderTarget_fsh = "		\
uniform sampler2D dz_texture;					\
varying vec2 v_texcoord;						\
												\
void main()										\
{												\
	gl_FragColor = texture2D(					\
		dz_texture, v_texcoord);				\
	gl_FragColor.a = 1.0;						\
}												\
";
