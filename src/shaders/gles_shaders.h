#ifndef _GLES_SHADER_H
#define _GLES_SHADER_H

const char *opengl_shaderDiffuseCxform_vsh = "			\
#ifdef GL_ES									\n\
precision lowp float;							\n\
#endif											\n\
attribute vec4 dz_position;						\
attribute vec4 dz_color;						\
attribute vec2 dz_texcrood;						\
varying vec4 v_fragmentColor;					\
uniform mat4 dz_MVPMatrix;						\
												\
void main()										\
{												\
	gl_Position = dz_MVPMatrix * dz_position;	\
	v_fragmentColor = dz_color;					\
}												\
";

const char *opengl_shaderDiffuseCxform_fsh = "			\
#ifdef GL_ES									\n\
precision lowp float;							\n\
#endif											\n\
uniform vec4 dz_cxformMul;						\
uniform vec4 dz_cxformAdd;						\
varying vec4 v_fragmentColor;					\
												\
void main()										\
{												\
	vec4 tcolor = v_fragmentColor;				\
	tcolor *= dz_cxformMul;						\
	tcolor += dz_cxformAdd;						\
	gl_FragColor = tcolor;						\
}												\
";

const char *opengl_shaderTextureCxform_vsh = "			\
#ifdef GL_ES									\n\
precision lowp float;							\n\
#endif											\n\
attribute vec4 dz_position;						\
attribute vec4 dz_color;						\
attribute vec2 dz_texcrood;						\
uniform sampler2D dz_texture;					\
uniform mat4 dz_MVPMatrix;						\
varying vec2 v_texcoord;						\
												\
void main()										\
{												\
	gl_Position = dz_MVPMatrix * dz_position;	\
	v_texcoord = dz_texcrood;					\
}												\
";

const char *opengl_shaderTextureCxform_fsh = "			\
#ifdef GL_ES									\n\
precision lowp float;							\n\
#endif											\n\
uniform vec4 dz_cxformMul;						\
uniform vec4 dz_cxformAdd;						\
uniform sampler2D dz_texture;					\
varying vec2 v_texcoord;						\
												\
void main()										\
{												\
	vec4 texColor = texture2D(					\
		dz_texture, v_texcoord);				\
	texColor *= dz_cxformMul;					\
	texColor += dz_cxformAdd;					\
	gl_FragColor = texColor;					\
}												\
";

const char *opengl_shaderDiffuseTextureCxform_vsh = "	\
#ifdef GL_ES									\n\
precision lowp float;							\n\
#endif											\n\
attribute vec4 dz_position;						\
attribute vec4 dz_color;						\
attribute vec2 dz_texcrood;						\
uniform mat4 dz_MVPMatrix;						\
varying vec2 v_texcoord;						\
varying vec4 v_fragmentColor;					\
												\
void main()										\
{												\
	gl_Position = dz_MVPMatrix * dz_position;	\
	v_texcoord = dz_texcrood;					\
	v_fragmentColor = dz_color;					\
}												\
";

const char *opengl_shaderDiffuseTextureCxform_fsh = "	\
#ifdef GL_ES									\n\
precision lowp float;							\n\
#endif											\n\
uniform vec4 dz_cxformMul;						\
uniform vec4 dz_cxformAdd;						\
uniform sampler2D dz_texture;					\
varying vec2 v_texcoord;						\
varying vec4 v_fragmentColor;					\
												\
void main()										\
{												\
	vec4 tcolor = v_fragmentColor * texture2D(	\
		dz_texture, v_texcoord);				\
	tcolor *= dz_cxformMul;						\
	tcolor += dz_cxformAdd;						\
	gl_FragColor = tcolor;						\
}												\
";

const char *opengl_shaderCopyRenderTarget_vsh = "		\
#ifdef GL_ES									\n\
precision lowp float;							\n\
#endif											\n\
attribute vec4 dz_position;						\
attribute vec4 dz_color;						\
attribute vec2 dz_texcrood;						\
uniform mat4 dz_MVPMatrix;						\
varying vec2 v_texcoord;						\
												\
void main()										\
{												\
	gl_Position = dz_MVPMatrix * dz_position;	\
	v_texcoord = vec2(dz_texcrood[0],			\
		1.0 - dz_texcrood[1]);					\
}												\
";

const char *opengl_shaderCopyRenderTarget_fsh = "		\
#ifdef GL_ES									\n\
precision lowp float;							\n\
#endif											\n\
uniform vec4 dz_cxformMul;						\
uniform vec4 dz_cxformAdd;						\
uniform sampler2D dz_texture;					\
varying vec2 v_texcoord;						\
												\
void main()										\
{												\
	gl_FragColor = texture2D(					\
		dz_texture, v_texcoord);				\
	gl_FragColor.a = 1.0;						\
	gl_FragColor *= dz_cxformMul;				\
	gl_FragColor += dz_cxformAdd;				\
}												\
";

#endif