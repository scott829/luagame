const char *d3d9_shaderDiffuseCxform_fsh = 
	"			\
	uniform extern float4 cxform_mul;				\
	uniform extern float4 cxform_add;				\
	\
	struct VS_INPUT									\
	{												\
	float4 inputColor : COLOR0;					\
	};												\
	\
	struct VS_OUTPUT								\
	{												\
	float4 outputColor : COLOR0;				\
	};												\
	\
	VS_OUTPUT ps_main(VS_INPUT Input)				\
	{												\
	VS_OUTPUT Output;							\
	float4 rgba;								\
	rgba = Input.inputColor;					\
	rgba[0] = Input.inputColor[2];				\
	rgba[1] = Input.inputColor[1];				\
	rgba[2] = Input.inputColor[0];				\
	rgba[3] = Input.inputColor[3];				\
	rgba *= cxform_mul;							\
	rgba += cxform_add;							\
	Output.outputColor = rgba;					\
	\
	return(Output);								\
	}												\
	";

const char *d3d9_shaderTextureCxform_fsh = 
	"			\
	texture Texture0;								\
	sampler Samp0 =	sampler_state					\
	{												\
	Texture = <Tex0>;							\
	MipFilter = LINEAR;							\
	MinFilter = LINEAR;							\
	MagFilter = LINEAR;							\
	};												\
	uniform extern float4 cxform_mul;				\
	uniform extern float4 cxform_add;				\
	\
	struct VS_INPUT									\
	{												\
	float2 uv : TEXCOORD0;						\
	};												\
	\
	struct VS_OUTPUT								\
	{												\
	float4 outputColor : COLOR0;				\
	};												\
	\
	VS_OUTPUT ps_main(VS_INPUT Input)				\
	{												\
	VS_OUTPUT Output;							\
	float4 rgba;								\
	\
	rgba = tex2D(Samp0, Input.uv);				\
	Output.outputColor[0] = rgba[2];			\
	Output.outputColor[1] = rgba[1];			\
	Output.outputColor[2] = rgba[0];			\
	Output.outputColor[3] = rgba[3];			\
	Output.outputColor *= cxform_mul;			\
	Output.outputColor += cxform_add;			\
	\
	return(Output);								\
	}												\
	";

const char *d3d9_shaderDiffuseTextureCxform_fsh = 
	"	\
	texture Texture0;								\
	sampler Samp0 =	sampler_state					\
	{												\
	Texture = <Tex0>;							\
	MipFilter = LINEAR;							\
	MinFilter = LINEAR;							\
	MagFilter = LINEAR;							\
	};												\
	uniform extern float4 cxform_mul;				\
	uniform extern float4 cxform_add;				\
	\
	struct VS_INPUT									\
	{												\
	float4 inputColor : COLOR0;					\
	float2 uv : TEXCOORD0;						\
	};												\
	\
	struct VS_OUTPUT								\
	{												\
	float4 outputColor : COLOR0;				\
	};												\
	\
	VS_OUTPUT ps_main(VS_INPUT Input)				\
	{												\
	VS_OUTPUT Output;							\
	float4 rgba;								\
	\
	rgba = Input.inputColor *					\
	tex2D(Samp0, Input.uv);					\
	Output.outputColor[0] = rgba[2];			\
	Output.outputColor[1] = rgba[1];			\
	Output.outputColor[2] = rgba[0];			\
	Output.outputColor[3] = rgba[3];			\
	Output.outputColor *= cxform_mul;			\
	Output.outputColor += cxform_add;			\
	\
	return(Output);								\
	}												\
	";

const char *d3d9_shaderCopyRenderTarget_fsh = 
	"		\
	texture Texture0;								\
	sampler Samp0 =	sampler_state					\
	{												\
	Texture = <Tex0>;							\
	MipFilter = LINEAR;							\
	MinFilter = LINEAR;							\
	MagFilter = LINEAR;							\
	};												\
	\
	struct VS_INPUT									\
	{												\
	float2 uv : TEXCOORD0;						\
	};												\
	\
	struct VS_OUTPUT								\
	{												\
	float4 outputColor : COLOR0;				\
	};												\
	\
	VS_OUTPUT ps_main(VS_INPUT Input)				\
	{												\
	VS_OUTPUT Output;							\
	\
	Output.outputColor = tex2D(Samp0, Input.uv);\
	Output.outputColor.a = 1.0f;				\
	\
	return(Output);								\
	}												\
	";
