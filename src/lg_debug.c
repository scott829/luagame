#include "lg_debug.h"
#include "lg_cmdline.h"
#include "event2/event.h"
#include "event2/util.h"
#include "event2/bufferevent.h"
#include "event2/buffer.h"
#include "lg_log.h"
#include "lg_init.h"
#include "lg_dict.h"
#include "lg_memory.h"
#include "jansson/jansson.h"
#include "lg_scriptenv.h"
#include "lg_platform.h"
#include "lg_avltree.h"
#include "lg_list.h"
#include "lg_luaapi.h"
#include "lg_node.h"
#include <math.h>
#include <float.h>
#include "lg_action.h"
#ifdef HAVE_NETINET_IN_H
#	include <netinet/in.h>
#endif
#ifdef HAVE_ARPA_INET_H
#	include <arpa/inet.h>
#endif

#define NumStr(name) char name[64]

#define LgDebugDumpVaraibleMaxDepth 5

enum LgDebuggerStepType {
	LgDebuggerStepNone,
	LgDebuggerStepOver,
	LgDebuggerStepInto,
	LgDebuggerStepReturn,
};

typedef struct LgDebugRequest {
	struct bufferevent *	bev;
	char *					json;
}*LgDebugRequestP;

static evutil_socket_t g_commandListener = 0;
static evutil_socket_t g_eventListener = 0;
static LgDictP g_commands = NULL;
static LgAvlTreeP g_breakpoints = NULL;
static LgListP g_eventClients = NULL;
static LgBool g_suspend = LgFalse;
static enum LgDebuggerStepType g_stepType;
static int g_stepStartLevel;
static LgBool g_requestSuspend = LgFalse;
static LgListP g_requests = NULL;
static LgBool g_requestStart = LgFalse, g_started = LgFalse;

typedef void (* LgDebugCommand)(struct bufferevent *bev, json_t *json);

void destroyDebugRequest(LgDebugRequestP request) {
	LgFree(request->json);
	LgFree(request);
}

static void sendReply(struct bufferevent *bev, json_t *json) {
	char *jsonText = json_dumps(json, JSON_COMPACT);
	bufferevent_write(bev, jsonText, strlen(jsonText) + 1);
	bufferevent_flush(bev, EV_WRITE, BEV_FLUSH);
	free(jsonText);
}

static void sendReplyBool(struct bufferevent *bev, LgBool succeeded) {
	json_t *reply = json_pack("{sb}", "succeeded", LgTrue);
	sendReply(bev, reply);
	json_delete(reply);
}

static void fireEvent(json_t *json) {
	LgListNodeP node = LgListFirstNode(g_eventClients);
	char *jsonText = json_dumps(json, JSON_COMPACT);
	while(node) {
		bufferevent_write(((struct bufferevent *)LgListNodeValue(node)),
			jsonText, strlen(jsonText) + 1);
		node = LgListNextNode(node);
	}
	free(jsonText);
}

static void commandStart(struct bufferevent *bev, json_t *json) {
	g_requestStart = LgTrue;
}

static void commandResume(struct bufferevent *bev, json_t *json) {
	if (g_suspend) {
		event_base_loopbreak(g_evtbase);
		g_suspend = LgFalse;
		sendReplyBool(bev, LgTrue);
	} else {
		sendReplyBool(bev, LgFalse);
	}
}

static void commandSuspend(struct bufferevent *bev, json_t *json) {
	if (!g_suspend) {
		g_requestSuspend = LgTrue;
		sendReplyBool(bev, LgTrue);
	} else {
		sendReplyBool(bev, LgFalse);
	}
}

static void commandTerminate(struct bufferevent *bev, json_t *json) {
	if (g_suspend) event_base_loopbreak(g_evtbase);
	LgExit();
}

static const char *getBreakpointString(const char *filename, int line) {
	static char temp[1024];
	sprintf(temp, "%s:%d", filename, line);
	return temp;
}

static void commandAddBreakpoint(struct bufferevent *bev, json_t *json) {
	json_t *filename = json_object_get(json, "filename");
	json_t *line = json_object_get(json, "line");

	if (filename && json_is_string(filename) &&
		line && json_is_integer(line)) {
		const char *bpstr = getBreakpointString(json_string_value(filename), (int)json_integer_value(line));
		LgAvlTreeReplace(g_breakpoints, strdup(bpstr));
		LgLogInfo("debug: add breakpoint: %s:%d", json_string_value(filename), (int)json_integer_value(line));
		sendReplyBool(bev, LgTrue);
	} else {
		sendReplyBool(bev, LgFalse);
	}
}

static void commandRemoveBreakpoint(struct bufferevent *bev, json_t *json) {
	json_t *filename = json_object_get(json, "filename");
	json_t *line = json_object_get(json, "line");

	if (filename && json_is_string(filename) &&
		line && json_is_integer(line)) {
		const char *bpstr = getBreakpointString(json_string_value(filename), (int)json_integer_value(line));
		LgAvlTreeRemove(g_breakpoints, (void *)bpstr);
		LgLogInfo("debug: remove breakpoint: %s:%d", json_string_value(filename), (int)json_integer_value(line));
		sendReplyBool(bev, LgTrue);
	} else {
		sendReplyBool(bev, LgFalse);
	}
}

static json_t *createUnknownVariable(const char *name) {
	return json_pack("{ssssss}",
					 "name", name,
					 "type", "unknown type",
					 "value", "(unknown type)");
}

const char *getNumberString(double d, char *str) {
	int intvalue = (int)d;

	if (fabs(((double)intvalue) - d) <= DBL_EPSILON && d <= INT_MAX && d >= INT_MIN) {
		sprintf(str, "%d", intvalue);
		return str;
	}
	else {
		if (str) {
			if (fabs(floor(d) - d) <= DBL_EPSILON && fabs(d) < 1.0e60) sprintf(str, "%.0f", d);
			else if (fabs(d) < 1.0e-6 || fabs(d) > 1.0e9) sprintf(str, "%e", d);
			else sprintf(str, "%f", d);
		}
	}
	return str;
}

static json_t *createNumberVariable(const char *name, double value) {
	NumStr(valueStr);
	return json_pack("{ssssss}",
					 "name", name,
					 "type", "number",
					 "value", getNumberString(value, valueStr));
}

static json_t *createStringVariable(const char *name, const char *value) {
	return json_pack("{ssssss}",
					 "name", name,
					 "type", "string",
					 "value", value);
}

static json_t *createBooleanVariable(const char *name, LgBool value) {
	return json_pack("{sssssb}",
					 "name", name,
					 "type", "string",
					 "value", value);
}

static json_t *createVector2Variable(const char *name, LgVector2P vec) {
	char temp[256];
	NumStr(xstr);
	NumStr(ystr);
	sprintf(temp, "(%s, %s)", getNumberString(vec->x, xstr), getNumberString(vec->y, ystr));

	return json_pack("{ssssss}",
					 "name", name,
					 "type", "lg.vector2",
					 "value", temp);
}

static json_t *createRectVariable(const char *name, LgAabbP rt) {
	char temp[256];
	NumStr(xstr);
	NumStr(ystr);
	NumStr(wstr);
	NumStr(hstr);

	sprintf(temp, "(x:%s, y:%s, w:%s, h:%s)", 
		getNumberString(rt->x, xstr), getNumberString(rt->y, ystr),
		getNumberString(rt->width, wstr), getNumberString(rt->height, hstr));

	return json_pack("{ssssss}",
					 "name", name,
					 "type", "lg.rect",
					 "value", temp);
}

static json_t *createCxformVariable(const char *name, LgCxformP cxform) {
	json_t *members = json_array();
	char temp[256];
	NumStr(rstr);
	NumStr(gstr);
	NumStr(bstr);
	NumStr(astr);
	
	sprintf(temp, "(r:%s, g:%s, b:%s, a:%s)",
			getNumberString(cxform->mul[0], rstr),
			getNumberString(cxform->mul[1], gstr),
			getNumberString(cxform->mul[2], bstr),
			getNumberString(cxform->mul[3], astr)
			);
	json_array_append_new(members, createStringVariable("mul", temp));
	
	sprintf(temp, "(r:%s, g:%s, b:%s, a:%s)",
			getNumberString(cxform->add[0], rstr),
			getNumberString(cxform->add[1], gstr),
			getNumberString(cxform->add[2], bstr),
			getNumberString(cxform->add[3], astr)
			);
	json_array_append_new(members, createStringVariable("add", temp));

	return json_pack("{ssssso}",
					 "name", name,
					 "type", "lg.cxform",
					 "members", members);
}

static json_t *createColorVariable(const char *name, const LgColor *color) {
	char temp[256];
	NumStr(rstr);
	NumStr(gstr);
	NumStr(bstr);
	NumStr(astr);

	sprintf(temp, "(r:%s, g:%s, b:%s, a:%s)",
			getNumberString(LgColorRedF(*color), rstr),
			getNumberString(LgColorRedF(*color), gstr),
			getNumberString(LgColorRedF(*color), bstr),
			getNumberString(LgColorRedF(*color), astr)
			);
	return json_pack("{ssssss}",
					 "name", name,
					 "type", "lg.color",
					 "value", temp);
}

static json_t *createActionVariable(const char *name, LgActionP action) {
	json_t *members = json_array();

	json_array_append_new(members, createBooleanVariable("paused", LgActionIsPaused(action)));
	json_array_append_new(members, createBooleanVariable("running", LgActionIsRunning(action)));

	return json_pack("{ssssso}",
					 "name", name,
					 "type", "lg.action",
					 "members", members);
}

static json_t *getVariable(lua_State *L, const char *varname, int level);

static json_t *createTableVariable(const char *name, lua_State *L, int idx, int level) {
	if (level < LgDebugDumpVaraibleMaxDepth) {
		json_t *var;
		json_t *members = json_array();

		var = json_pack("{ssssso}",
						"name", name,
						"type", "table",
						"members", members);

		lua_pushnil(L);
		while(lua_next(L, -2) != 0) {
			if (lua_isstring(L, -2) || lua_isnumber(L, -2)) {
				char *temp;
				json_t *member;

				lua_pushvalue(L, -2);
				temp = strdup(lua_tostring(L, -1));
				lua_pop(L, 1);
				member = getVariable(L, temp, level + 1);
				free(temp);
				if (member) json_array_append_new(members, member);
			}
			lua_pop(L, 1);
		}

		return var;
	} else {
		return NULL;
	}
}

static json_t *createNodeVariable(const char *name, LgNodeP node) {
	struct LgVector2 position, scale, transformAnchor, anchor;
	struct LgCxform cxform;
	float rotate;
	int zorder;
	json_t *members = json_array();

	LgNodePosition(node, &position);
	LgNodeScale(node, &scale);
	rotate = LgNodeRotate(node);
	zorder = LgNodeZorder(node);
	LgNodeAnchor(node, &anchor);
	LgNodeTransformAnchor(node, &transformAnchor);
	LgNodeCxform(node, &cxform);

	json_array_append_new(members, createVector2Variable("position", &position));
	json_array_append_new(members, createVector2Variable("scale", &scale));
	json_array_append_new(members, createNumberVariable("rotate", rotate));
	json_array_append_new(members, createNumberVariable("zorder", zorder));
	json_array_append_new(members, createVector2Variable("anchor", &anchor));
	json_array_append_new(members, createVector2Variable("transform_anchor", &transformAnchor));
	json_array_append_new(members, createCxformVariable("cxform", &cxform));

	return json_pack("{ssssso}",
					 "name", name,
					 "type", "lg.node",
					 "members", members);
}

static json_t *getVariable(lua_State *L, const char *varname, int level) {
	if (lua_isnumber(L, -1)) {
		return createNumberVariable(varname, luaL_checknumber(L, -1));
	} else if (lua_isstring(L, -1)) {
		return createStringVariable(varname, luaL_checkstring(L, -1));
	} else if (lua_isboolean(L, -1)) {
		return createBooleanVariable(varname, lua_toboolean(L, -1));
	} else if (lua_istable(L, -1)) {
		return createTableVariable(varname, L, -1, level + 1);
	} else if (LgLuaIsObject(L, -1, LGNODE_HANDLE)) {
		return createNodeVariable(varname, *((LgNodeP *)LgLuaCheckObject(L, -1, LGNODE_HANDLE)));
	} else if (LgLuaIsObject(L, -1, LGCXFORM_HANDLE)) {
		return createCxformVariable(varname, (LgCxformP)LgLuaCheckObject(L, -1, LGCXFORM_HANDLE));
	} else if (LgLuaIsObject(L, -1, LGPOINT_HANDLE)) {
		return createVector2Variable(varname, (LgVector2P)LgLuaCheckObject(L, -1, LGPOINT_HANDLE));
	} else if (LgLuaIsObject(L, -1, LGRECT_HANDLE)) {
		return createRectVariable(varname, (LgAabbP)LgLuaCheckObject(L, -1, LGRECT_HANDLE));
	} else if (LgLuaIsObject(L, -1, LGCOLOR_HANDLE)) {
		return createColorVariable(varname, (const LgColor *)LgLuaCheckObject(L, -1, LGCOLOR_HANDLE));
	} else if (LgLuaIsObject(L, -1, LGACTION_HANDLE)) {
		return createActionVariable(varname, *((LgActionP *)LgLuaCheckObject(L, -1, LGACTION_HANDLE)));
	} else {
		return createUnknownVariable(varname);
	}
}

static void commandBackTrace(struct bufferevent *bev, json_t *json) {
	json_t *reply;
	
	reply = json_object();
	
	if (g_suspend) {
		lua_State *L = LgScriptEnvState();
		int i = 0;
		json_t *frames = json_array();
		struct lua_Debug ar;
		
		while(lua_getstack(L, i, &ar)) {
			json_t *frame = json_object();
			json_t *variables = json_array();
			int j = 1;
			const char *varname;
			lua_getinfo(L, "Sln", &ar);
			
			// 取frame基本信息
			json_array_append_new(frames, frame);
			if (ar.source) json_object_set_new(frame, "source", json_string(ar.source));
			if (ar.name) json_object_set_new(frame, "name", json_string(ar.name));
			json_object_set_new(frame, "line", json_integer(ar.currentline));
			
			// 取局部变量
			json_object_set_new(frame, "variables", variables);
			while((varname = lua_getlocal(L, &ar, j))) {
				if (varname[0] != '(') {
					json_t *var = getVariable(L, varname, 0);
					if (var) json_array_append_new(variables, var);
				}
				lua_pop(L, 1); // value
				j++;
			}

			i++;
		}
		
		json_object_set_new(reply, "frames", frames);
		json_object_set_new(reply, "succeeded", json_boolean(LgTrue));
	} else {
		json_object_set_new(reply, "succeeded", json_boolean(LgFalse));
	}

	sendReply(bev, reply);
	json_delete(reply);
}

static int tracebackCount(lua_State *L) {
	lua_Debug ar;
	int index = 0;
	while(lua_getstack(L, index, &ar)) index++;
	return index;
}

static void commandStepOver(struct bufferevent *bev, json_t *json) {
	if (g_suspend) {
		g_stepStartLevel = tracebackCount(LgScriptEnvState());
		g_stepType = LgDebuggerStepOver;
		g_suspend = LgFalse;
		LgLogInfo("debug: step-over", NULL);
		sendReplyBool(bev, LgTrue);
		event_base_loopbreak(g_evtbase);
	} else {
		sendReplyBool(bev, LgFalse);
	}
}

static void commandStepInto(struct bufferevent *bev, json_t *json) {
	if (g_suspend) {
		g_stepStartLevel = tracebackCount(LgScriptEnvState());
		g_stepType = LgDebuggerStepInto;
		g_suspend = LgFalse;
		LgLogInfo("debug: step-into", NULL);
		sendReplyBool(bev, LgTrue);
		event_base_loopbreak(g_evtbase);
	} else {
		sendReplyBool(bev, LgFalse);
	}
}

static void commandStepReturn(struct bufferevent *bev, json_t *json) {
	if (g_suspend) {
		g_stepStartLevel = tracebackCount(LgScriptEnvState());
		g_stepType = LgDebuggerStepReturn;
		g_suspend = LgFalse;
		LgLogInfo("debug: step-return", NULL);
		sendReplyBool(bev, LgTrue);
		event_base_loopbreak(g_evtbase);
	} else {
		sendReplyBool(bev, LgFalse);
	}
}

static struct DebugCommand {
	const char *	type;
	LgDebugCommand	command;
}commands[] = {
	{"start", commandStart},
	{"resume", commandResume},
	{"suspend", commandSuspend},
	{"add-breakpoint", commandAddBreakpoint},
	{"remove-breakpoint", commandRemoveBreakpoint},
	{"terminate", commandTerminate},
	{"backtrace", commandBackTrace},
	{"step-over", commandStepOver},
	{"step-into", commandStepInto},
	{"step-return", commandStepReturn},
	{NULL}
};

static void processDebugCommand(struct bufferevent *bev, const char *jsonText) {
	json_t *json = json_loads(jsonText, 0, NULL);
	
	if (json) {
		json_t *type = json_object_get(json, "type");
		
		if (type && json_is_string(type)) {
			LgDebugCommand command = (LgDebugCommand)LgDictFind(g_commands, (void *)json_string_value(type));
			if (command) command(bev, json);
			else LgLogError("debug: unknown command: %s", type);
		}
		
		json_delete(json);
	}
}

static void hitBreakpoint(lua_State *L, lua_Debug *ar) {
	json_t *event;

	LgLogInfo("debug: break at %s:%d", ar->source, ar->currentline);
	g_suspend = LgTrue;
	
	event = json_pack("{sssssi}",
					  "event", "hit-breakpoint",
					  "filename", ar->source,
					  "line", ar->currentline);
	fireEvent(event);
	json_delete(event);
	event_base_loop(g_evtbase, 0);
}

static void fireSuspendEvent() {
	json_t *event = json_pack("{ss}", "event", "suspend");
	fireEvent(event);
	json_delete(event);
}

static void luaHook(lua_State *L, lua_Debug *ar) {
	lua_getinfo(L, "Sl", ar);

	if (g_requestSuspend) {
		g_suspend = LgTrue;
		g_requestSuspend = LgFalse;
		fireSuspendEvent();
		event_base_dispatch(g_evtbase);
	}

	if (g_stepType == LgDebuggerStepOver) {
		// 在stepover模式
		int currentStackCount = tracebackCount(L);
		if (currentStackCount <= g_stepStartLevel) {
			g_stepType = LgDebuggerStepNone;
			g_suspend = LgTrue;
			LgLogInfo("debug: stepover finished at %s:%d", ar->source, ar->currentline);
			fireSuspendEvent();
			event_base_dispatch(g_evtbase);
		}
	} else if (g_stepType == LgDebuggerStepInto) {
		g_stepType = LgDebuggerStepNone;
		g_suspend = LgTrue;
		LgLogInfo("debug: stepinto finished at %s:%d", ar->source, ar->currentline);
		fireSuspendEvent();
		event_base_dispatch(g_evtbase);
	} else if (g_stepType == LgDebuggerStepReturn) {
		int currentStackCount = tracebackCount(L);
		if (currentStackCount < g_stepStartLevel) {
			g_stepType = LgDebuggerStepNone;
			g_suspend = LgTrue;
			LgLogInfo("debug: stepreturn finished at %s:%d", ar->source, ar->currentline);
			fireSuspendEvent();
			event_base_dispatch(g_evtbase);
		}
	}

	if (LgCheckBreakpoint(ar->source, ar->currentline)) {
		// 有断点
		hitBreakpoint(L, ar);
	}
}

static void readCallback(struct bufferevent *bev, void *arg) {
	struct evbuffer *input = bufferevent_get_input(bev);
	char *json;
	
	while((json = evbuffer_readln(input, NULL, EVBUFFER_EOL_NUL))) {
		if (!g_suspend) {
			// 读到一个请求，丢到g_requests里面
			LgDebugRequestP request = (LgDebugRequestP)LgMallocZ(sizeof(struct LgDebugRequest));
			request->json = json;
			request->bev = bev;
			LgListAppend(g_requests, request);
		} else {
			processDebugCommand(bev, json);
			free(json);
		}
	}
}

static void doCommandAccept(evutil_socket_t listener, short event, void *arg) {
	// 有新的连接
	evutil_socket_t fd;
	struct sockaddr_in sin;
	socklen_t slen = sizeof(sin);
	
	fd = accept(listener, (struct sockaddr *)&sin, &slen);
	if (fd > 0) {
		struct bufferevent *bev;
		bev = bufferevent_socket_new(g_evtbase, fd, BEV_OPT_CLOSE_ON_FREE);
		bufferevent_setcb(bev, readCallback, NULL, NULL, NULL);
		bufferevent_enable(bev, EV_READ | EV_PERSIST);
		LgLogInfo("debug command client attached", NULL);
	}
}

static void removeEventClient(struct bufferevent *bev) {
	LgListNodeP node = LgListFirstNode(g_eventClients);
	while(node) {
		if (LgListNodeValue(node) == bev) {
			LgListRemove(g_eventClients, node);
			break;
		}
		node = LgListNextNode(node);
	}
}

static void eventCallback(struct bufferevent *bev, short event, void *arg) {
	if (event & BEV_EVENT_EOF) {
		// 客户端断开了连接
		removeEventClient(bev);
	}
}

static void doEventAccept(evutil_socket_t listener, short event, void *arg) {
	// 有新的连接
	evutil_socket_t fd;
	struct sockaddr_in sin;
	socklen_t slen = sizeof(sin);

	fd = accept(listener, (struct sockaddr *)&sin, &slen);
	if (fd > 0) {
		struct bufferevent *bev;
		bev = bufferevent_socket_new(g_evtbase, fd, BEV_OPT_CLOSE_ON_FREE);
		bufferevent_setcb(bev, NULL, NULL, eventCallback, NULL);
		bufferevent_enable(bev, EV_READ | EV_PERSIST);
		bufferevent_setwatermark(bev, EV_READ, 0, 2048);
		bufferevent_setwatermark(bev, EV_WRITE, 0, 2048);
		LgListAppend(g_eventClients, bev);
		LgLogInfo("debug event client attached", NULL);
	}
}

static evutil_socket_t makeListener(int port, event_callback_fn callback) {
	evutil_socket_t s = 0;
	struct sockaddr_in sin;
	struct event *listenEvent;

	s = socket(AF_INET, SOCK_STREAM, 0);
	if (!s) {
		LgLogError("unable create socket for debug!", NULL);
		goto _error;
	}

	evutil_make_listen_socket_reuseable(s);

	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = 0;
	sin.sin_port = htons(port);

	if (bind(s, (struct sockaddr *)&sin, sizeof(sin)) < 0) {
		LgLogError("port %d is already used!", port);
		goto _error;
	}

	LG_CHECK_ERROR(listen(s, 32) >= 0);

	listenEvent = event_new(g_evtbase, s, EV_READ | EV_PERSIST, callback, NULL);
	event_add(listenEvent, NULL);

	evutil_make_socket_nonblocking(s);
	return s;

_error:
	if (s) evutil_closesocket(s);
	return 0;
}

LgBool LgDebugOpen() {
	int i;
	
	// 创建调试服务器
	g_commandListener = makeListener(g_commandLine.debugPort, doCommandAccept);
	LG_CHECK_ERROR(g_commandListener);
	LgLogInfo("debug: command port: %d", g_commandLine.debugPort);
	g_eventListener = makeListener(g_commandLine.debugPort + 1, doEventAccept);
	LG_CHECK_ERROR(g_eventListener);
	LgLogInfo("debug: event port: %d", g_commandLine.debugPort);
	
	// 初始化调试命令
	g_commands = LgDictCreate((LgCompare)strcmp, NULL, NULL);
	i = 0;
	while(commands[i].type) {
		LgDictReplace(g_commands, (void *)commands[i].type, commands[i].command);
		i++;
	}

	// 初始化调试环境
	g_breakpoints = LgRbTreeCreate((LgCompare)strnocasecmp, LgFree);
	g_eventClients = LgListCreate(NULL);
	g_suspend = LgFalse;
	g_stepType = LgDebuggerStepNone;
	g_requestSuspend = LgFalse;
	g_requests = LgListCreate((LgDestroy)destroyDebugRequest);
	g_requestStart = g_started = LgFalse;

	// 设置lua的debug hook
	lua_sethook(LgScriptEnvState(), luaHook, LUA_MASKLINE, 0);
	return LgTrue;
	
_error:
	LgDebugClose();
	return LgFalse;
}

void LgDebugClose() {
	if (g_commandListener) {
		evutil_closesocket(g_commandListener);
		g_commandListener = 0;
	}
	if (g_eventListener) {
		evutil_closesocket(g_eventListener);
		g_eventListener = 0;
	}
	if (g_commands) {
		LgDictDestroy(g_commands);
		g_commands = NULL;
	}
	if (g_breakpoints) {
		LgAvlTreeDestroy(g_breakpoints);
		g_breakpoints = NULL;
	}
	if (g_eventClients) {
		LgListDestroy(g_eventClients);
		g_eventClients = NULL;
	}
	if (g_requests) {
		LgListDestroy(g_requests);
		g_requests = NULL;
	}
}

LgBool LgCheckBreakpoint(const char *filename, int line) {
	return LgAvlTreeFind(g_breakpoints, (void *)getBreakpointString(filename, line)) != NULL;
}

void LgDebugProcessCommands() {
	while(LgListCount(g_requests)) {
		LgDebugRequestP request = (LgDebugRequestP)LgListNodeValue(LgListFirstNode(g_requests));
		processDebugCommand(request->bev, request->json);
		LgListRemove(g_requests, LgListFirstNode(g_requests));
	}

	if (g_requestStart) {
		// 请求启动，之所以放在这里启动，而不是在commandStart里面启动，是因为不希望程序阻塞在commandStart
		// 因为LgScriptEnvRunFile会一直循环，并且调用luaHook
		if (!g_started) {
			json_t *event = json_pack("{ss}", "event", "started");
			fireEvent(event);
			json_delete(event);

			LgScriptEnvRunFile("scripts/main.lua");
			g_started = LgTrue;
		}
		g_requestStart = LgFalse;
	}
}