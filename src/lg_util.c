#include "lg_util.h"
#include <string.h>
#include "lg_platform.h"
#include "lg_meminputstream.h"
#include "lg_memoutputstream.h"
#include <wchar.h>

void LgRelPath(char *result, size_t size, const char *relpath, const char *filename) {
	if (filename[0] == '/') {
		strlcpy(result, filename, size);
	} else {
		strlcpy(result, relpath, size);
		if (strrchr(result, '/')) {
			*(strrchr(result, '/') + 1) = 0;
			strcat(result, filename);
		} else {
			strlcpy(result, filename, size);
		}
	}
}

wchar_t *LgUtf8ToWide(const char *text, int len) {
	LgOutputStreamP output;
	LgInputStreamP input;
	wchar_t wc, *result;
	
	if (len == -1) len = strlen(text);
	input = LgMemInputStreamCreate(text, len);
	output = LgMemOutputStreamCreate(NULL, 0);
	
	while(LgInputStreamReadRune(input, &wc)) {
		LgOutputStreamWriteBytes(output, &wc, sizeof(wc));
	}
	
	wc = 0;
	LgOutputStreamWriteBytes(output, &wc, sizeof(wc));
	result = (wchar_t *)wcsdup((const wchar_t *)LgMemOutputStreamData(output));
	
	LgInputStreamDestroy(input);
	LgOutputStreamDestroy(output);
	return result;
}

char *LgWideToUtf8(const wchar_t *text, int len) {
	LgOutputStreamP output;
	char *result;
	int i;
	
	if (len == -1) len = wcslen(text);
	output = LgMemOutputStreamCreate(NULL, 0);
	
	for (i = 0; i < len; i++) LgOutputStreamWriteRune(output, text[i]);
	LgOutputStreamWriteByte(output, 0);
	result = strdup((const char *)LgMemOutputStreamData(output));
	
	LgOutputStreamDestroy(output);
	return result;
}
