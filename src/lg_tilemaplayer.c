﻿#include "lg_tilemaplayer.h"
#include "lg_textureatlas.h"
#include "lg_array.h"
#include "lg_dict.h"
#include "lg_director.h"
#include "lg_platform.h"
#include "lg_util.h"
#include "lg_log.h"
#include "lg_resourcemanager.h"
#include "lg_shapes.h"

typedef struct LgTileInfo {
	json_t *		config;
	const char *	name;
	struct LgSize	tileSize;
}*LgTileInfoP;

enum LgTileLayerType {
	LgTileLayerTile,
	LgTileLayerObjectGroup,
	LgTileLayerImage,
};

typedef struct LgTileLayer {
	char *					name;
	LgBool					visible;
	enum LgTileLayerType	type;
	float					opacity;
	json_t *				properties;

	union {
		struct {
			LgArrayP			drawBuffers;
			int *				tiles;
			json_t **			cellProperties;
		}tileLayer;

		struct {
			LgArrayP			objects;
			LgDictP				nameToObject;
		}objectGroup;

		struct {
			LgTextureP			texture;
		}image;
	};
}*LgTileLayerP;

typedef struct LgTileMapDriver *LgTileMapDriverP;

typedef struct LgTileMapCollision {
	char *				collisionLayer;	// 碰撞信息所在的层
	char *				collisionProp;	// 标记碰撞信息的属性名称
	char *				collision;		// 碰撞信息
	LgBool				value;			// 如果属性等于这个值，则表示这里无法通过
}*LgTileMapCollisionP;

typedef struct LgChildAt {
	const char *layerName;
	int			row;
}*LgChildAtP;

typedef struct LgTileMapData {
	struct LgLayerData	base;
	LgTileMapDriverP	driver;					// 地图驱动
	json_t *			config;					// 地图配置文件
	struct LgSize		size, tileSize;			// 地图大小和图块大小
	json_t *			properties;				// 地图属性
	LgArrayP			tilesets;				// 图块集合
	struct LgVector2	scroll;					// 当前滚动坐标
	LgTextureAtlasP		atlas;					// 保存图块的atlas
	LgArrayP			layers;					// 图层
	LgDictP				objects;				// 对象字典
	LgDictP				tileAlias;				// 图块别名对应图块id
	LgTileMapCollisionP	collision;				// 碰撞信息
	LgPointP			resultPath;				// 保存findpath的结果
	LgDictP				tileIdToProperties;		// 图块id对应属性
	LgDictP				childAtLayer;			// 子节点对应的图层信息 LgNodeP -> LgChildAtP
}*LgTileMapDataP;

struct LgTileMapDriver {
	const char *name;
	void (* pixelSize)(LgTileMapDataP td, LgVector2P size);
	void (* pixelCroodToGrid)(LgTileMapDataP td, LgVector2P pt);
	void (* gridToPixelCrood)(LgTileMapDataP td, LgVector2P pt);
	void (* createDrawBuffers)(LgTileMapDataP td, LgTileLayerP layer);
};

// ortho --------------------------------------------------------------------------------------------------------------------
static void orthoPixelSize(LgTileMapDataP td, LgVector2P size) {
	size->x = (float)(td->size.width * td->tileSize.width);
	size->y = (float)(td->size.height * td->tileSize.height);
}

static void orthoPixelCroodToGrid(LgTileMapDataP td, LgVector2P pt) {
	pt->x = pt->x / (float)td->tileSize.width;
	pt->y = pt->y / (float)td->tileSize.height;
}

static void orthoGridToPixelCrood(LgTileMapDataP td, LgVector2P pt) {
	pt->x = pt->x * (float)td->tileSize.width;
	pt->y = pt->y * (float)td->tileSize.height;
}

static void orthoCreateDrawBuffers(LgTileMapDataP td, LgTileLayerP layer) {
	int col, row;

	for (row = 0; row < td->size.height; row++) {
		LgAtlasDrawBufferP db = LgTextureAtlasCreateDrawBufferR(td->atlas);

		for (col = 0; col < td->size.width; col++) {
			int p = row * td->size.width + col;
			int cellv = layer->tileLayer.tiles[p];

			if (cellv > 0) {
				struct LgAabb box;
				struct LgTextAtlasRecord record;

				LgTextureAtlasRecord(td->atlas, cellv, &record);

				box.x = (float)(col * td->tileSize.width);
				box.y = (float)(row * td->tileSize.height - (record.size.height - td->tileSize.height));
				box.width = (float)(td->tileSize.width);
				box.height = (float)(record.size.height);
				LgAtlasBufferDrawRect(db, cellv, &box);
			}
		}

		LgArrayAppend(layer->tileLayer.drawBuffers, db);
	}
}

static struct LgTileMapDriver orthoMapDriver = {
	"orthogonal",
	orthoPixelSize,
	orthoPixelCroodToGrid,
	orthoGridToPixelCrood,
	orthoCreateDrawBuffers
};

// iso --------------------------------------------------------------------------------------------------------------------
static void isoPixelSize(LgTileMapDataP td, LgVector2P size) {
	size->x = (float)(td->size.width * td->tileSize.width);
	size->y = (float)(td->size.height * td->tileSize.height);
}

static void isoPixelCroodToGrid(LgTileMapDataP td, LgVector2P pt) {
	float px, py, tileX, tileY;

	px = pt->x, py = pt->y;
	px -= td->size.height * td->tileSize.width / 2;
	tileX = px / td->tileSize.width;
	tileY = py / td->tileSize.height;
	pt->x = tileY + tileX;
	pt->y = tileY - tileX;
}

static void isoGridToPixelCrood(LgTileMapDataP td, LgVector2P pt) {
	float originX = (float)td->size.height * td->tileSize.width / 2;
	float gx = pt->x, gy = pt->y;
	pt->x = (gx - gy) * td->tileSize.width / 2 + originX;
	pt->y = (gx + gy) * td->tileSize.height / 2;
}

static void isoCreateDrawBuffers(LgTileMapDataP td, LgTileLayerP layer) {
	int col, row;
	float halfWidth = (float)td->tileSize.width / 2, halfHeight = (float)td->tileSize.height / 2;

	for (row = 0; row < td->size.height; row++) {
		float dx, dy;
		LgAtlasDrawBufferP db = LgTextureAtlasCreateDrawBufferR(td->atlas);

		dx = (td->size.width * halfWidth - halfWidth) - row * halfWidth;
		dy = row * halfHeight;

		for (col = 0; col < td->size.width; col++) {
			int p = row * td->size.width + col;
			int cellv = layer->tileLayer.tiles[p];

			if (cellv > 0) {
				struct LgAabb box;
				struct LgTextAtlasRecord record;

				LgTextureAtlasRecord(td->atlas, cellv, &record);

				box.x = dx;
				box.y = dy - (record.size.height - td->tileSize.height);
				box.width = (float)(td->tileSize.width);
				box.height = (float)(record.size.height);
				
				LgAtlasBufferDrawRect(db, cellv, &box);
			}

			dx += halfWidth, dy += halfHeight;
		}

		LgArrayAppend(layer->tileLayer.drawBuffers, db);
	}
}

static struct LgTileMapDriver isoMapDriver = {
	"isometric",
	isoPixelSize,
	isoPixelCroodToGrid,
	isoGridToPixelCrood,
	isoCreateDrawBuffers
};

// drivers --------------------------------------------------------------------------------------------------------------------
static LgTileMapDriverP mapDrivers[] = {
	&orthoMapDriver,
	&isoMapDriver,
	NULL,
};

// tilemap --------------------------------------------------------------------------------------------------------------------

static void findpath(LgTileMapDataP td, const LgPointP start, const LgPointP end, LgBool bias, int *path);

static void destroyTileLayer(LgTileLayerP layer) {
	if (layer->name) LgFree(layer->name);

	if (layer->type == LgTileLayerTile) {
		if (layer->tileLayer.drawBuffers) LgArrayDestroy(layer->tileLayer.drawBuffers);
		if (layer->tileLayer.tiles) LgFree(layer->tileLayer.tiles);
		if (layer->tileLayer.cellProperties) LgFree(layer->tileLayer.cellProperties);
	} else if (layer->type == LgTileLayerObjectGroup) {
		if (layer->objectGroup.nameToObject) LgDictDestroy(layer->objectGroup.nameToObject);
		if (layer->objectGroup.objects) LgArrayDestroy(layer->objectGroup.objects);
	} else if (layer->type == LgTileLayerImage) {
		if (layer->image.texture) LgTextureRelease(layer->image.texture);
	}

	LgFree(layer);
}

static void destroyTileset(LgTileInfoP tileinfo) {
	LgFree(tileinfo);
}

static LgTileLayerP findLayer(LgTileMapDataP td, const char *name) {
	int i;
	for (i = 0; i < LgArrayLen(td->layers); i++) {
		LgTileLayerP layer = (LgTileLayerP)LgArrayGet(td->layers, i);
		if (strnocasecmp(layer->name, name) == 0) {
			return layer;
		}
	}
	return NULL;
}

static int findLayerIndex(LgTileMapDataP td, const char *name) {
	int i;
	for (i = 0; i < LgArrayLen(td->layers); i++) {
		LgTileLayerP layer = (LgTileLayerP)LgArrayGet(td->layers, i);
		if (strnocasecmp(layer->name, name) == 0) {
			return i;
		}
	}
	return -1;
}

void destroyCollision(LgTileMapCollisionP collision) {
	LgFree(collision->collisionLayer);
	LgFree(collision->collisionProp);
	LgFree(collision);
}

static void visitChildrenByLayerName(LgNodeP node, LgChildAtP ca, float delta) {
	if (LgNodeChildrenCount(node) > 0) {
		LgTileMapDataP td = (LgTileMapDataP)LgNodeUserData(node);
		int i, count = LgNodeChildrenCount(node);
		
		for (i = 0; i < count; i++) {
			LgNodeP child = LgNodeChildByIndex(node, i);
			LgChildAtP ca2 = td->childAtLayer ? (LgChildAtP)LgDictFind(td->childAtLayer, child) : NULL;
			if (!ca) {
				if (!ca2) LgNodeVisit(child, delta);
			} else {
				if (ca2 && ca2->row == ca->row && ca2->layerName == ca->layerName) LgNodeVisit(child, delta);
			}
		}
	}
}

static void tileMapLayerEventProc(LgEventType type, LgNodeP node, void *param) {
	switch(type) {
	case LG_NODE_EVENT_Destroy:
		{
			LgTileMapDataP td = (LgTileMapDataP)LgNodeUserData(node);
			if (td->atlas) LgTextureAtlasRelease(td->atlas);
			if (td->layers) LgArrayDestroy(td->layers);
			if (td->tilesets) LgArrayDestroy(td->tilesets);
			if (td->tileAlias) LgDictDestroy(td->tileAlias);
			if (td->resultPath) LgFree(td->resultPath);
			if (td->collision) destroyCollision(td->collision);
			if (td->tileIdToProperties) LgDictDestroy(td->tileIdToProperties);
			if (td->config) json_delete(td->config);
			if (td->childAtLayer) LgDictDestroy(td->childAtLayer);
		}
		break;
	case LG_NODE_EVENT_VisitChildren:
		// 自定义子节点绘制，屏蔽默认绘制功能
		return;
	case LG_NODE_EVENT_Draw:
		{
			LgTileMapDataP td = (LgTileMapDataP)LgNodeUserData(node);
			float delta = *((float *)param);
			
			if (td->layers) {
				int i, k;
				struct LgMatrix44 oldMatrix, newMatrix;

				LgRenderSystemSetPixelShader(LgPixelShaderCxformTexture);
				LgRenderSystemGetTransform(LgTransformWorld, &oldMatrix);
				LgMatrix44InitTranslate(&newMatrix, -td->scroll.x, -td->scroll.y, 0);
				LgMatrix44Mul(&newMatrix, &oldMatrix);
				LgRenderSystemSetTransform(LgTransformWorld, &newMatrix);

				for (i = 0; i < LgArrayLen(td->layers); i++) {
					LgTileLayerP layer = (LgTileLayerP)LgArrayGet(td->layers, i);
					struct LgCxform oldCxform, cxform;

					if (!layer->visible) continue;

					if (layer->opacity != 1.0f) {
						LgRenderSystemGetCxform(&oldCxform);
						cxform = oldCxform;
						cxform.mul[LGCXFORM_ALPHA] *= layer->opacity;
						LgRenderSystemSetCxform(&cxform);
					}

					if (layer->type == LgTileLayerTile) {
						LgAtlasDrawBufferP db;
						struct LgChildAt ca;

						if (!layer->tileLayer.drawBuffers) {
							layer->tileLayer.drawBuffers = LgArrayCreate((LgDestroy)LgAtlasDrawBufferRelease);
							td->driver->createDrawBuffers(td, layer);
						}

						for (k = 0; k < td->size.height; k++) {
							db = (LgAtlasDrawBufferP)LgArrayGet(layer->tileLayer.drawBuffers, k);

							LgAtlasBufferDraw(db);
							LgDirectorUpdateBatch(LgAtlasBufferNumberOfBatch(db));

							ca.layerName = layer->name;
							ca.row = k;
							visitChildrenByLayerName(node, &ca, delta);
						}
					} else if (layer->type == LgTileLayerImage) {
						struct LgMatrix44 imageTransform, pTransform;
						struct LgSize imageSize;

						LgTextureSize(layer->image.texture, &imageSize);
						LgRenderSystemSetTexture(layer->image.texture);

						LgRenderSystemGetTransform(LgTransformWorld, &pTransform);
						LgMatrix44InitScale(&imageTransform, (float)imageSize.width, (float)imageSize.height, 1);
						LgMatrix44Mul(&imageTransform, &pTransform);
						LgRenderSystemSetTransform(LgTransformWorld, &imageTransform);
						LgRenderSystemDrawPrimitive(LgPrimitiveTriangleStrip, LgFvfXYZ | LgFvfTexture, LgShapesRectangleTexcoord(), 2);
						LgRenderSystemSetTransform(LgTransformWorld, &pTransform);

						LgDirectorUpdateBatch(1);
					}

					if (layer->opacity != 1.0f) {
						LgRenderSystemSetCxform(&oldCxform);
					}
				}
				
				visitChildrenByLayerName(node, NULL, delta);
				LgRenderSystemSetTransform(LgTransformWorld, &oldMatrix);
			}
			return;
		}
		break;
	case LG_NODE_EVENT_RemoveChildNotify:
		{
			LgTileMapDataP td = (LgTileMapDataP)LgNodeUserData(node);
			if (td->childAtLayer) LgDictRemove(td->childAtLayer, param);
		}
		break;
	}

	LgLayerEventProc(type, node, param);
}

static LgBool loadTile(LgTileMapDataP td, json_t *setting, const char *filename) {
	json_t *tileName, *image, *firstgid, *tilewidth, *tileheight,
		*marginJson, *spacingJson, *tileproperties;
	struct LgSize textureSize;
	char textureFilename[LG_PATH_MAX];
	int margin = 0, spacing = 0;
	int col, row, cols, rows;
	int textureId;
	LgTileInfoP tileInfo;
	json_t *config = setting;
	int firstCharId, charId;

	firstgid = json_object_get(config, "firstgid");
	LG_CHECK_ERROR(firstgid && json_is_integer(firstgid));
	firstCharId = charId = (int)json_integer_value(firstgid);

	tileName = json_object_get(config, "name");
	LG_CHECK_ERROR(tileName && json_is_string(tileName));
	image = json_object_get(config, "image");
	LG_CHECK_ERROR(image && json_is_string(image));

	tilewidth = json_object_get(config, "tilewidth");
	tileheight = json_object_get(config, "tileheight");
	LG_CHECK_ERROR(tilewidth && json_is_integer(tilewidth) && tileheight && json_is_integer(tileheight));

	marginJson = json_object_get(config, "margin");
	if (marginJson && json_is_integer(marginJson)) margin = (int)json_integer_value(marginJson);

	spacingJson = json_object_get(config, "spacing");
	if (spacingJson && json_is_integer(spacingJson)) spacing = (int)json_integer_value(spacingJson);

	LgRelPath(textureFilename, sizeof(textureFilename), filename, json_string_value(image));
	LG_CHECK_ERROR((textureId = LgTextureAtlasAddTextureFromFile(td->atlas, textureFilename)) >= 0);
	LgTextureSize(LgTextureAtlasTexture(td->atlas, textureId), &textureSize);

	rows = (textureSize.height - margin * 2 + spacing) / ((int)json_integer_value(tileheight) + spacing);
	cols = (textureSize.width - margin * 2 + spacing) / ((int)json_integer_value(tilewidth) + spacing);

	for (row = 0; row < rows; row++) {
		for (col = 0; col < cols; col++) {
			struct LgTextAtlasRecord record;
			record.textureId = textureId;
			record.pt.x = (margin + (spacing + (int)json_integer_value(tilewidth)) * col);
			record.pt.y = (margin + (spacing + (int)json_integer_value(tileheight)) * row);
			record.size.width = (int)json_integer_value(tilewidth);
			record.size.height = (int)json_integer_value(tileheight);
			LgTextureAtlasAddRecord(td->atlas, charId, &record);
			charId++;
		}
	}

	// 添加tileIdToProperties和alias
	tileproperties = json_object_get(config, "tileproperties");

	if (tileproperties && json_is_object(tileproperties)) {
		void *iter;

		iter = json_object_iter(tileproperties);

		while(iter) {
			const char *id;
			json_t *properties = json_object_iter_value(iter);

			id = json_object_iter_key(iter);

			if (properties && json_is_object(properties)) {
				json_t *alias;

				// 图块属性
				LgDictReplace(td->tileIdToProperties, (void *)(firstCharId + atoi(id)), properties);

				// 图块别名
				alias = json_object_get(properties, "__alias");
				if (alias && json_is_string(alias)) {
					char fullAlias[256];

					strlcpy(fullAlias, json_string_value(tileName), sizeof(fullAlias));
					strcat(fullAlias, ".");
					strcat(fullAlias, json_string_value(alias));
					LgDictReplace(td->tileAlias, strdup(fullAlias), (void *)(firstCharId + atoi(id)));
				}
			}

			iter = json_object_iter_next(tileproperties, iter);
		}
	}

	// 添加tileinfo
	tileInfo = (LgTileInfoP)LgMallocZ(sizeof(struct LgTileInfo));
	tileInfo->config = config;
	tileInfo->name = json_string_value(tileName);
	tileInfo->tileSize.width = (int)json_integer_value(tilewidth), 
		tileInfo->tileSize.height = (int)json_integer_value(tileheight);
	LgArrayAppend(td->tilesets, tileInfo);
	return LgTrue;

_error:
	return LgFalse;
}

static LgBool loadTilesets(LgTileMapDataP td, json_t *config, const char *filename) {
	json_t *setting, *tile;
	int i;

	setting = json_object_get(config, "tilesets");
	LG_CHECK_ERROR(setting && json_is_array(setting));

	for (i = 0; i < (int)json_array_size(setting); i++) {
		tile = json_array_get(setting, i);
		if (json_is_object(tile)) LG_CHECK_ERROR(loadTile(td, tile, filename));
	}

	return LgTrue;

_error:
	return LgFalse;
}

static void fillCellProperties(LgTileMapDataP td, int *layerData, json_t **cellProperties) {
	int col, row;

	for (row = 0; row < td->size.height; row++) {
		for (col = 0; col < td->size.width; col++) {
			int p = row * td->size.width + col;
			int cellv = layerData[p];
			json_t *properties = (json_t *)LgDictFind(td->tileIdToProperties, (void *)cellv);
			if (properties) cellProperties[p] = properties;
		}
	}
}

static LgTileMapDriverP findDriver(const char *name) {
	int i;
	for (i = 0; mapDrivers[i]; i++) {
		if (strnocasecmp(mapDrivers[i]->name, name) == 0) {
			return mapDrivers[i];
		}
	}
	return NULL;
}

static LgBool loadCollision(LgTileMapDataP td) {
	json_t *collisionNode = json_object_get(td->config, "collision");

	if (collisionNode) {
		json_t *layerName, *prop, *value;
		
		layerName = json_object_get(collisionNode, "layer");
		prop = json_object_get(collisionNode, "prop");
		value = json_object_get(collisionNode, "value");
		
		if (layerName && json_is_string(layerName) &&
			prop && json_is_string(prop) &&
			value && json_is_boolean(value)) {
			LgTileLayerP layer;
			
			td->collision = (LgTileMapCollisionP)LgMallocZ(sizeof(struct LgTileMapCollision));
			LG_CHECK_ERROR(td->collision);
			td->collision->collisionLayer = strdup(json_string_value(layerName));
			td->collision->collisionProp = strdup(json_string_value(prop));
			td->collision->value = json_is_true(value);
			td->collision->collision = (char *)malloc(td->size.width * td->size.height);
			memset(td->collision->collision, 0, td->size.width * td->size.height);
			
			layer = findLayer(td, json_string_value(layerName));
			
			if (layer) {
				int col, row;
				for (row = 0; row < td->size.height; row++) {
					for (col = 0; col < td->size.width; col++) {
						json_t *_prop = layer->tileLayer.cellProperties[row * td->size.width + col], *vJson;
						LgBool v = LgFalse;
						
						if (_prop && (vJson = json_object_get(_prop, json_string_value(prop))) && json_is_boolean(vJson)) {
							v = json_is_true(vJson);
						}
						td->collision->collision[row * td->size.width + col] = (!(v == json_is_true(value)));
					}
				}
			}
		}
	}

	return LgTrue;

_error:
	return LgFalse;
}

static void destroyMapObject(LgMapObjectP object) {
	if (object->name) LgFree(object->name);
	if (object->type) LgFree(object->type);
	if (object->objectType == LgMapObjectPolygon || object->objectType == LgMapObjectPolyline)
		if (object->polygonOrPolyline.points) LgFree(object->polygonOrPolyline.points);
	LgFree(object);
}

static LgMapObjectP loadMapObject(json_t *json) {
	LgMapObjectP object = NULL;
	json_t *name, *type, *visible, *x, *y;

	name = json_object_get(json, "name");
	LG_CHECK_ERROR(name && json_is_string(name));

	type = json_object_get(json, "type");
	LG_CHECK_ERROR(type && json_is_string(type));

	object = (LgMapObjectP)LgMallocZ(sizeof(struct LgMapObject));
	LG_CHECK_ERROR(object);
	object->visible = LgTrue;

	if (strlen(json_string_value(name)) > 0)
		object->name = strdup(json_string_value(name));

	if (strlen(json_string_value(type)) > 0)
		object->type = strdup(json_string_value(type));

	visible = json_object_get(json, "visible");
	if (visible && json_is_boolean(visible)) object->visible = json_is_true(visible);

	object->properties = json_object_get(json, "properties");

	x = json_object_get(json, "x");
	LG_CHECK_ERROR(x && json_is_number(x));
	y = json_object_get(json, "y");
	LG_CHECK_ERROR(y && json_is_number(y));
	
	object->x = (float)json_number_value(x);
	object->y = (float)json_number_value(x);

	if (json_object_get(json, "gid")) {
		// 图块
		json_t *gid;

		gid = json_object_get(json, "gid");

		object->objectType = LgMapObjectTile;
		object->tile.gid = (int)json_integer_value(gid);
	} else if (json_object_get(json, "polygon") || json_object_get(json, "polyline")) {
		// 多边形或者折线
		json_t *points;

		points = json_object_get(json, "polygon");
		object->objectType = LgMapObjectPolygon;

		if (!points) {
			points = json_object_get(json, "polyline");
			object->objectType = LgMapObjectPolyline;
		}

		object->polygonOrPolyline.count = (int)json_array_size(points);

		if (json_array_size(points) > 0) {
			int i;

			object->polygonOrPolyline.points = (LgVector2P)LgMallocZ(sizeof(struct LgVector2) * json_array_size(points));
			LG_CHECK_ERROR(object->polygonOrPolyline.points);

			for (i = 0; i < (int)json_array_size(points); i++) {
				json_t *x, *y;

				x = json_object_get(json_array_get(points, i), "x");
				y = json_object_get(json_array_get(points, i), "y");
				object->polygonOrPolyline.points[i].x = (float)json_number_value(x);
				object->polygonOrPolyline.points[i].y = (float)json_number_value(y);
			}
		}
	} else {
		// 椭圆或者矩形
		json_t *width, *height;

		object->objectType = json_object_get(json, "ellipse") ? LgMapObjectEllipse : LgMapObjectRectangle;

		width = json_object_get(json, "width");
		LG_CHECK_ERROR(x && json_is_number(width));
		object->rectangleOrEllipse.width = (float)json_number_value(width);

		height = json_object_get(json, "height");
		LG_CHECK_ERROR(x && json_is_number(height));
		object->rectangleOrEllipse.height = (float)json_number_value(height);
	}

	return object;

_error:
	if (object) destroyMapObject(object);
	return object;
}

static LgTileLayerP loadLayer(LgTileMapDataP td, json_t *layerJson, const char *filename) {
	json_t *name, *type, *visible, *dataItem, *opacity;
	LgTileLayerP tileLayer = NULL;
	int i, rcount;

	name = json_object_get(layerJson, "name");
	type = json_object_get(layerJson, "type");

	LG_CHECK_ERROR(name && json_is_string(name) && type && json_is_string(type));

	// 创建LgTileLayer
	tileLayer = (LgTileLayerP)LgMallocZ(sizeof(struct LgTileLayer));
	
	if (strcmp(json_string_value(type), "tilelayer") == 0) tileLayer->type = LgTileLayerTile;
	else if (strcmp(json_string_value(type), "objectgroup") == 0) tileLayer->type = LgTileLayerObjectGroup;
	else if (strcmp(json_string_value(type), "imagelayer") == 0) tileLayer->type = LgTileLayerImage;
	else goto _error;

	tileLayer->name = strdup(json_string_value(name));
	tileLayer->visible = LgTrue;
	tileLayer->properties = json_object_get(layerJson, "properties");

	visible = json_object_get(layerJson, "visible");
	if (visible && json_is_boolean(visible)) tileLayer->visible = json_is_true(visible);

	opacity = json_object_get(layerJson, "opacity");
	if (opacity && json_is_number(opacity)) tileLayer->opacity = (float)json_number_value(opacity);

	if (tileLayer->type == LgTileLayerTile) {
		// 地图图层
		// 读图层数据
		json_t *data;

		data = json_object_get(layerJson, "data");
		LG_CHECK_ERROR(data && json_is_array(data));

		rcount = td->size.width * td->size.height;
		tileLayer->tileLayer.tiles = (int *)LgMalloc(sizeof(int) * td->size.width * td->size.height);

		for (i = 0; i < (int)json_array_size(data) && i < rcount; i++) {
			dataItem = json_array_get(data, i);
			if (json_is_integer(dataItem))
				tileLayer->tileLayer.tiles[i] = (int)json_integer_value(dataItem);
		}

		// 从tileIdToProperties中得到tile的属性节点
		tileLayer->tileLayer.cellProperties = (json_t **)LgMallocZ(sizeof(json_t *) * td->size.width * td->size.height);
		fillCellProperties(td, tileLayer->tileLayer.tiles, tileLayer->tileLayer.cellProperties);
	} else if (tileLayer->type == LgTileLayerObjectGroup) {
		// 对象层
		json_t *objects;

		objects = json_object_get(layerJson, "objects");
		LG_CHECK_ERROR(objects && json_is_array(objects));

		tileLayer->objectGroup.objects = LgArrayCreate((LgDestroy)destroyMapObject);
		tileLayer->objectGroup.nameToObject = LgDictCreate(strnocasecmp, NULL, NULL);

		for (i = 0; i < (int)json_array_size(objects); i++) {
			json_t *objectJson = json_array_get(objects, i);
			LgMapObjectP obj;

			LG_CHECK_ERROR(objectJson && json_is_object(objectJson));
			obj = loadMapObject(objectJson);
			LG_CHECK_ERROR(obj);
			
			LgArrayAppend(tileLayer->objectGroup.objects, obj);
			if (obj->name) LgDictInsert(tileLayer->objectGroup.nameToObject, obj->name, obj);
		}
	} else if (tileLayer->type == LgTileLayerImage) {
		// 图像层
		char imageFilename[LG_PATH_MAX];
		json_t *image;
		
		image = json_object_get(layerJson, "image");
		LG_CHECK_ERROR(image && json_is_string(image));
		LgRelPath(imageFilename, sizeof(imageFilename), filename, json_string_value(image));
		tileLayer->image.texture = LgTextureRetain(LgResourceManagerLoadTexture(imageFilename));
		LG_CHECK_ERROR(tileLayer->image.texture);
	}

	return tileLayer;

_error:
	if (tileLayer) destroyTileLayer(tileLayer);
	return NULL;
}

static LgBool loadMap(LgTileMapDataP td, const char *filename) {
	json_t *orientation, *sizeWidth, *sizeHeight,
		*tileSizeWidth, *tileSizeHeight, *layers;
	int i;

	// 读基本信息
	orientation = json_object_get(td->config, "orientation");
	LG_CHECK_ERROR(orientation && json_is_string(orientation));
	td->driver = findDriver(json_string_value(orientation));
	LG_CHECK_ERROR(td->driver);

	td->properties = json_object_get(td->config, "properties");
	
	sizeWidth = json_object_get(td->config, "width");
	sizeHeight = json_object_get(td->config, "height");
	LG_CHECK_ERROR(sizeWidth && json_is_integer(sizeWidth) &&
				   sizeHeight && json_is_integer(sizeHeight));
	td->size.width = (int)json_integer_value(sizeWidth);
	td->size.height = (int)json_integer_value(sizeHeight);
	
	tileSizeWidth = json_object_get(td->config, "tilewidth");
	tileSizeHeight = json_object_get(td->config, "tileheight");
	LG_CHECK_ERROR(tileSizeWidth && json_is_integer(sizeWidth) &&
				   sizeHeight && json_is_integer(sizeHeight));
	td->tileSize.width = (int)json_integer_value(tileSizeWidth);
	td->tileSize.height = (int)json_integer_value(tileSizeHeight);
	
	// 读tilesets
	td->tileIdToProperties = LgDictCreate(NULL, NULL, NULL);
	LG_CHECK_ERROR(td->tileIdToProperties);
	LG_CHECK_ERROR(loadTilesets(td, td->config, filename));

	// 读layer
	layers = json_object_get(td->config, "layers");
	LG_CHECK_ERROR(layers);

	for (i = 0; i < (int)json_array_size(layers); i++) {
		LgTileLayerP layer = loadLayer(td, json_array_get(layers, i), filename);
		LG_CHECK_ERROR(layer);
		LgArrayAppend(td->layers, layer);
	}

	return LgTrue;

_error:
	LgLogError("failed to load map '%s'", filename);
	return LgFalse;
}

LgNodeP LgTileMapLayerCreateR(const char *filename) {
	LgNodeP node = NULL;
	LgTileMapDataP td = NULL;

	node = LgNodeCreateR((LgEvent)tileMapLayerEventProc);
	LG_CHECK_ERROR(node);

	td = (LgTileMapDataP)LgLayerDataCreate(sizeof(struct LgTileMapData));
	LG_CHECK_ERROR(td);
	LgNodeSetUserData(node, td);

	td->atlas = LgTextureAtlasCreateR();
	td->layers = LgArrayCreate((LgDestroy)destroyTileLayer);
	td->config = json_loadp(filename);
	LG_CHECK_ERROR(td->config);
	td->tilesets = LgArrayCreate((LgDestroy)destroyTileset);
	td->tileAlias = LgDictCreate((LgCompare)strnocasecmp, LgFree, NULL);
	LG_CHECK_ERROR(td->tileAlias);
	LG_CHECK_ERROR(loadMap(td, filename));
	LG_CHECK_ERROR(loadCollision(td));

	return node;

_error:
	if (node) LgNodeRelease(node);
	return NULL;
}

// find path --------------------------------------------------------------------------------
#define tile_num(x, y) (g_mapWidth * y + x)
#define tile_x(n) (n % g_mapWidth)
#define tile_y(n) (n / g_mapHeight)
#define STACKSIZE 65536
#define MAXINT 8192

typedef struct FTree {
	int				h, tile;
	struct FTree *	father;
}*FTreeP;

typedef struct FLink {
	FTreeP	node;
	int		f;
	struct FLink *	next;
}*FLinkP;

static FLinkP g_queue;
static FTreeP g_stack[STACKSIZE];
static int g_stackTop;
static int g_startX, g_startY, g_endX, g_endY, g_mapWidth, g_mapHeight;
static char *g_map;
static int *g_disMap;
static LgBool g_bias;

// 初始化队列
static void initQueue() {
	g_queue = (FLinkP)LgMallocZ(sizeof(struct FLink));
	g_queue->f = -1;
	g_queue->next = (FLinkP)LgMallocZ(sizeof(struct FLink));
	g_queue->next->f = MAXINT;
}

// 待处理节点入队列, 依靠对目的地估价距离插入排序
static void enterQueue(FTreeP node, int f) {
	FLinkP p = g_queue, father, q;

	while(f > p->f) {
		father = p;
		p = p->next;
	}

	q = (FLinkP)LgMallocZ(sizeof(struct FLink));
	q->f = f, q->node = node, q->next = p;
	father->next = q;
}

// 将离目的地估计最近的方案出队列
static FTreeP getFromQueue()
{
	FTreeP bestchoice = g_queue->next->node;
	FLinkP next = g_queue->next->next;

	LgFree(g_queue->next);
	g_queue->next = next;
	g_stack[g_stackTop++] = bestchoice;
	return bestchoice;
}

static void popStack() {
	LgFree(g_stack[--g_stackTop]);
}

// 释放申请过的所有节点
static void freeTree() {
	int i;
	FLinkP p;

	for (i = 0; i < g_stackTop; i++)
		LgFree(g_stack[i]);

	while(g_queue) {
		p = g_queue;
		LgFree(p->node);
		g_queue = g_queue->next;
		LgFree(p);
	}
}

// 估价函数,估价 x,y 到目的地的距离,估计值必须保证比实际值小
static int judge(int x, int y) {
	int distance;
	distance = abs(g_endX - x) + abs(g_endY - y);
	return distance;
}

// 尝试下一步移动到 x,y 可行否
static LgBool trytile(int x, int y, FTreeP father) {
	FTreeP p = father;
	int h;
	if (x < 0 || y < 0 || x >= g_mapWidth || y >= g_mapHeight) return 1; // 越界了
	if (g_map[tile_num(x, y)] == 0) return 1; // 如果 (x,y) 处是障碍,失败
	while(p) {
		if (x == tile_x(p->tile) && y == tile_y(p->tile)) return LgTrue; //如果 (x,y) 曾经经过,失败
		p = p->father;
	}
	h = father->h + 1;
	if (h>=g_disMap[tile_num(x, y)]) return LgTrue; // 如果曾经有更好的方案移动到 (x,y) 失败
	g_disMap[tile_num(x, y)] = h; // 记录这次到 (x,y) 的距离为历史最佳距离

	// 将这步方案记入待处理队列
	p= (FTreeP)LgMallocZ(sizeof(struct FTree));
	p->father = father;
	p->h = father->h + 1;
	p->tile = tile_num(x,y);
	enterQueue(p, p->h + judge(x, y));
	return 0;
}

// 路径寻找主函数
static void findpath2(int *path) {
	FTreeP root;
	int i, j;

	g_stackTop = 0;
	g_disMap = (int *)LgMallocZ(sizeof(int) * g_mapWidth * g_mapHeight);
	for (i = 0; i < g_mapHeight; i++)
		for (j = 0;j<g_mapWidth; j++)
			g_disMap[tile_num(j, i)] = MAXINT;

	initQueue();

	root = (FTreeP)LgMallocZ(sizeof(struct FTree));
	root->tile = tile_num(g_startX, g_startY);
	root->h = 0;
	root->father = NULL;
	enterQueue(root, judge(g_startX, g_startY));
	
	for (;;) {
		int x, y, child;
		root = getFromQueue();
		if (root == NULL) {
			*path = -1;
			return;
		}
		x = tile_x(root->tile);
		y = tile_y(root->tile);
		if (x == g_endX && y == g_endY) break; // 达到目的地成功返回

		child = trytile(x, y - 1, root); //尝试向上移动
		child &= trytile(x, y + 1, root); //尝试向下移动
		child &= trytile(x - 1, y, root); //尝试向左移动
		child &= trytile(x + 1, y, root); //尝试向右移动

		if (g_bias) {
			child &= trytile(x - 1, y - 1, root); //尝试向左上移动
			child &= trytile(x - 1, y + 1, root); //尝试向左下移动
			child &= trytile(x + 1, y - 1, root); //尝试向右上移动
			child &= trytile(x + 1, y + 1, root); //尝试向右下移动
		}

		if (child!=0)
			popStack(); // 如果所有方向均不能移动,释放这个死节点
	}

	// 回溯树，将求出的最佳路径保存在 path 中
	for (i=0; root; i++) {
		path[i] = root->tile;
		root = root->father;
	}
	path[i] = -1;
	freeTree();
	LgFree(g_disMap);
}

static void findpath(LgTileMapDataP td, const LgPointP start, const LgPointP end, LgBool bias, int *path) {
	if (!td->collision) {
		*path = -1;
		return;
	}

	g_startX = start->x, g_startY = start->y;
	g_endX = end->x, g_endY = end->y;
	g_mapWidth = td->size.width, g_mapHeight = td->size.height;
	g_map = td->collision->collision;
	g_bias = bias;
	findpath2(path);
}

void LgTileMapLayerSize(LgNodeP node, LgVector2P size) {
	LgTileMapDataP td = (LgTileMapDataP)LgNodeUserData(node);
	td->driver->pixelSize(td, size);
}

void LgTileMapLayerGridSize(LgNodeP node, LgSizeP size) {
	LgTileMapDataP td = (LgTileMapDataP)LgNodeUserData(node);
	size->width = td->size.width;
	size->height = td->size.height;
}

void LgTileMapLayerSetScroll(LgNodeP node, const LgVector2P scroll) {
	LgTileMapDataP td = (LgTileMapDataP)LgNodeUserData(node);
	float x, y;
	struct LgSize viewSize;
	struct LgVector2 mapPixelSize;

	LgDirectorViewSize(&viewSize);
	LgTileMapLayerSize(node, &mapPixelSize);

	x = LgMin(LgMax(scroll->x, 0), mapPixelSize.x - viewSize.width);
	y = LgMin(LgMax(scroll->y, 0), mapPixelSize.y - viewSize.height);
	td->scroll.x = x;
	td->scroll.y = y;
}

void LgTileMapLayerScroll(LgNodeP node, LgVector2P scroll) {
	LgTileMapDataP td = (LgTileMapDataP)LgNodeUserData(node);
	LgCopyMemory(scroll, &td->scroll, sizeof(struct LgVector2));
}

void LgTileMapLayerCenterScroll(LgNodeP node, LgVector2P center) {
	LgTileMapDataP td = (LgTileMapDataP)LgNodeUserData(node);
	struct LgSize viewSize;

	LgDirectorViewSize(&viewSize);
	LgTileMapLayerGridToLocal(node, center);
	center->x += td->scroll.x;
	center->y += td->scroll.y;
	center->x -= viewSize.width / 2;
	center->y -= viewSize.height / 2;
}

void LgTileMapLayerCenterTo(LgNodeP node, const LgVector2P center) {
	struct LgSize viewSize;
	struct LgVector2 newScroll;

	LgDirectorViewSize(&viewSize);
	newScroll.x = center->x;
	newScroll.y = center->y;
	LgTileMapLayerCenterScroll(node, &newScroll);
	LgTileMapLayerSetScroll(node, &newScroll);
}

void LgTileMapLayerLocalToGrid(LgNodeP node, LgVector2P pt) {
	LgTileMapDataP td = (LgTileMapDataP)LgNodeUserData(node);
	pt->x += td->scroll.x;
	pt->y += td->scroll.y;
	LgTileMapLayerGlobalToGrid(node, pt);
}

void LgTileMapLayerGlobalToGrid(LgNodeP node, LgVector2P pt) {
	LgTileMapDataP td = (LgTileMapDataP)LgNodeUserData(node);
	td->driver->pixelCroodToGrid(td, pt);
}

void LgTileMapLayerGridToLocal(LgNodeP node, LgVector2P pt) {
	LgTileMapDataP td = (LgTileMapDataP)LgNodeUserData(node);
	LgTileMapLayerGridToGlobal(node, pt);
	pt->x -= td->scroll.x;
	pt->y -= td->scroll.y;
}

void LgTileMapLayerGridToGlobal(LgNodeP node, LgVector2P pt) {
	LgTileMapDataP td = (LgTileMapDataP)LgNodeUserData(node);
	td->driver->gridToPixelCrood(td, pt);
}

void LgTileMapLayerShowLayer(LgNodeP node, const char *name) {
	LgTileMapDataP td = (LgTileMapDataP)LgNodeUserData(node);
	LgTileLayerP layer = findLayer(td, name);
	if (layer) layer->visible = LgTrue;
}

void LgTileMapLayerHideLayer(LgNodeP node, const char *name) {
	LgTileMapDataP td = (LgTileMapDataP)LgNodeUserData(node);
	LgTileLayerP layer = findLayer(td, name);
	if (layer) layer->visible = LgFalse;
}

json_t *LgTileMapLayerProperties(LgNodeP node) {
	LgTileMapDataP td = (LgTileMapDataP)LgNodeUserData(node);
	return td->properties;
}

json_t *LgTileMapLayerLayerProperties(LgNodeP node, const char *layerName) {
	LgTileMapDataP td = (LgTileMapDataP)LgNodeUserData(node);
	LgTileLayerP layer = findLayer(td, layerName);

	if (!layer) return NULL;
	return layer->properties;
}

json_t *LgTileMapLayerTileProperties(LgNodeP node, const char *layerName, const LgPointP gridPt) {
	LgTileMapDataP td = (LgTileMapDataP)LgNodeUserData(node);
	LgTileLayerP layer = findLayer(td, layerName);

	if (!layer || layer->type != LgTileLayerTile) {
		return NULL;
	}

	if (gridPt->x < 0 || gridPt->y < 0 ||
		gridPt->x >= td->size.width || gridPt->y >= td->size.height) {
		return NULL;
	}

	return layer->tileLayer.cellProperties[gridPt->y * td->size.width + gridPt->x];
}

int LgTileMapLayerFindPath(LgNodeP node, const LgPointP start, const LgPointP end, LgBool bias, LgPointP *path) {
	LgTileMapDataP td = (LgTileMapDataP)LgNodeUserData(node);
	int *_path, i, pathCount = 0;

	_path = (int *)LgMallocZ(sizeof(int) * td->size.width * td->size.height);
	findpath(td, start, end, bias, _path);

	if (_path[0] != -1 && !td->resultPath) {
		*path = (LgPointP)LgMalloc(td->size.width * td->size.height * sizeof(struct LgPoint));
	}

	for (i = 0; _path[i] != -1; i++) {
		(*path)[i].x = _path[i] % td->size.width;
		(*path)[i].y = _path[i] / td->size.width;
		pathCount++;
	}
	LgFree(path);
	return pathCount;
}

int LgTileMapLayerTileIdByAlias(LgNodeP node, const char *alias) {
	LgTileMapDataP td = (LgTileMapDataP)LgNodeUserData(node);
	return (int)LgDictFind(td->tileAlias, (void *)alias);
}

int LgTileMapLayerTileId(LgNodeP node, const char *layerName, int col, int row) {
	LgTileMapDataP td = (LgTileMapDataP)LgNodeUserData(node);

	if (col >= 0 && row >= 0 && col < td->size.width && row < td->size.height) {
		LgTileLayerP layer = findLayer(td, layerName);
		if (layer && layer->type == LgTileLayerTile) {
			return layer->tileLayer.tiles[row * td->size.width + col];
		}
	}

	return 0;
}

void LgTileMapLayerSetTileId(LgNodeP node, const char *layerName, int col, int row, int id) {
	LgTileMapDataP td = (LgTileMapDataP)LgNodeUserData(node);

	if (col >= 0 && row >= 0 && col < td->size.width && row < td->size.height) {
		LgTileLayerP layer = findLayer(td, layerName);

		if (layer && layer->type == LgTileLayerTile &&
			layer->tileLayer.tiles[row * td->size.width + col] != id) {

				layer->tileLayer.tiles[row * td->size.width + col] = id;

				if (layer->tileLayer.drawBuffers) {
					// 更新drawbuffer
					LgArrayDestroy(layer->tileLayer.drawBuffers);
					layer->tileLayer.drawBuffers = NULL;
				}

				// 更新图块属性
				layer->tileLayer.cellProperties[row * td->size.width + col] = 
					(json_t *)LgDictFind(td->tileIdToProperties, (void *)id);

				// 更新碰撞信息
				if (td->collision) {
					json_t *prop;
					LgBool v = LgFalse;

					prop = layer->tileLayer.cellProperties[row * td->size.width + col];
					if (prop) {
						json_t *collision;
						collision = json_object_get(prop, td->collision->collisionProp);
						if (collision && json_is_true(collision)) v = LgTrue;
					}
					td->collision->collision[row * td->size.width + col] = (!(v == td->collision->value));
				}
		}
	}
}

void LgTileMapLayerSetChildAt(LgNodeP node, const char *layerName, int row, LgNodeP child) {
	LgTileMapDataP td = (LgTileMapDataP)LgNodeUserData(node);

	if (LgNodeParent(child) == node) {
		int layerIndex = findLayerIndex(td, layerName);

		if (layerIndex != -1) {
			LgChildAtP ca;
			if (!td->childAtLayer) td->childAtLayer = LgDictCreate(NULL, NULL, LgFree);
			ca = (LgChildAtP)LgMallocZ(sizeof(struct LgChildAt));
			ca->layerName = ((LgTileLayerP)LgArrayGet(td->layers, layerIndex))->name;
			ca->row = row;
			LgDictReplace(td->childAtLayer, child, ca);
		}
	}
}

LgBool LgNodeIsTileMapLayer(LgNodeP node) {
	return LgNodeEventProc(node) == (LgEvent)tileMapLayerEventProc;
}

int LgTileMapLayerLayerCount(LgNodeP node) {
	LgTileMapDataP td = (LgTileMapDataP)LgNodeUserData(node);
	return LgArrayLen(td->layers);
}

json_t *LgTileMapLayerTilePropertiesByLayerIndex(LgNodeP node, int layerIndex, const LgPointP gridPt){
	LgTileMapDataP td = (LgTileMapDataP)LgNodeUserData(node);
	LgTileLayerP layer = (LgTileLayerP)LgArrayGet(td->layers, layerIndex);

	if (!layer || layer->type != LgTileLayerTile) {
		return NULL;
	}

	if (gridPt->x < 0 || gridPt->y < 0 ||
		gridPt->x >= td->size.width || gridPt->y >= td->size.height) {
		return NULL;
	}

	return layer->tileLayer.cellProperties[gridPt->y * td->size.width + gridPt->x];
}

LgMapObjectP LgTileMapLayerFindObject(LgNodeP node, const char *layerName, const char *name) {
	LgTileMapDataP td = (LgTileMapDataP)LgNodeUserData(node);
	LgTileLayerP layer = (LgTileLayerP)findLayer(td, layerName);
	LgMapObjectP object;

	if (!layer || layer->type != LgTileLayerObjectGroup) return NULL;
	object = (LgMapObjectP)LgDictFind(layer->objectGroup.nameToObject, (void *)name);
	return object;
}