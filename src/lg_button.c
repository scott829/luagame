#include "lg_button.h"
#include "lg_sprite.h"
#include "lg_label.h"
#include "lg_action.h"

#define LG_UILAYER_BUTTONFACE	(LG_UILAYER_CHILD + 1)

#define LG_UILAYER_BUTTONTITLE	(LG_UILAYER_CHILD + 2)

static void buttonPress(LgNodeP button, LgButtonDataP bd, LgNodeTouchEventP evt) {
	if (bd->pressAction) LgNodeDoActionAndRelease(button, LgActionClone(bd->pressAction));
	if (bd->titleNode && LgNodeIsLabel(bd->titleNode)) LgLabelSetColor(bd->titleNode, bd->titlePressColor);
	if (bd->pressFaceNode) {
		if (bd->faceNode) LgNodeSetVisible(bd->faceNode, LgFalse);
		LgNodeSetVisible(bd->pressFaceNode, LgTrue);
	}
	LgTouchWorldPosition(evt->touches[0], &bd->downPosition);
	bd->down = LgTrue;
}

static void buttonRelease(LgNodeP button, LgButtonDataP bd) {
	if (bd->releaseAction) LgNodeDoActionAndRelease(button, LgActionClone(bd->releaseAction));
	if (bd->titleNode && LgNodeIsLabel(bd->titleNode)) LgLabelSetColor(bd->titleNode, bd->titleColor);
	if (bd->pressFaceNode) LgNodeSetVisible(bd->pressFaceNode, LgFalse);
	if (bd->faceNode) LgNodeSetVisible(bd->faceNode, LgTrue);
	bd->down = LgFalse;
}

static void buttonOnTouch(LgNodeP button, LgNodeTouchEventP evt) {
	LgButtonDataP bd = (LgButtonDataP)LgNodeUserData(button);

	if (evt->type == LgNodeTouchBegan) {
		buttonPress(button, bd, evt);
	} else if (evt->type == LgNodeTouchEnded) {
		if (LgTouchCurrentNode(evt->touches[0]) == button && bd->down) LgSlotCall(&bd->slotClick, NULL);
		buttonRelease(button, bd);
	} else if (evt->type == LgNodeTouchCancelled) {
		buttonRelease(button, bd);
	} else if (evt->type == LgNodeTouchMoved) {
		struct LgVector2 currentPosition;
		LgTouchWorldPosition(evt->touches[0], &currentPosition);
		if (fabs(currentPosition.x - bd->downPosition.x) > 5 ||
			fabs(currentPosition.y - bd->downPosition.y) > 5) {
			buttonRelease(button, bd);
		}
	}
}

static void buttonEventProc(LgEventType type, LgNodeP node, void *param) {
	LgButtonDataP bd = (LgButtonDataP)LgNodeUserData(node);

	switch(type) {
	case LG_NODE_EVENT_Destroy:
		if (bd->pressAction) LgActionRelease(bd->pressAction);
		if (bd->releaseAction) LgActionRelease(bd->releaseAction);
		break;
	case LG_NODE_EVENT_ClearSlots:
		LgSlotClear(&bd->slotClick);
		break;
	case LG_NODE_EVENT_HandleTouch:
		buttonOnTouch(node, (LgNodeTouchEventP)param);
		return;
	case LG_CONTROLNODE_EVENT_SetSize:
		LgControlEventProc(type, node, param);
		if (bd->faceNode) LgSpriteSetSize(bd->faceNode, &bd->base.size);
		if (bd->pressFaceNode) LgSpriteSetSize(bd->pressFaceNode, &bd->base.size);
		break;
	}

	LgControlEventProc(type, node, param);
}

LgNodeP LgButtonCreateR(const LgVector2P size) {
	LgNodeP node = LgControlNodeCreateSubClassR(sizeof(struct LgButtonData), (LgEvent)buttonEventProc, size);
	struct LgVector2 scale;
	LgButtonDataP bd = (LgButtonDataP)LgNodeUserData(node);

	LgNodeEnableHitTest(node, LgTrue);
	
	scale.x = 1.2f, scale.y = 1.2f;
	bd->pressAction = LgActionScaleToCreate(0.1, &scale);

	scale.x = 1.0f, scale.y = 1.0f;
	bd->releaseAction = LgActionScaleToCreate(0.1, &scale);
	
	bd->titleColor = bd->titlePressColor = LgColorXRGB(255, 255, 255);
	return node;
}

LgBool LgNodeIsButton(LgNodeP node) {
	return LgNodeEventProc(node) == (LgEvent)buttonEventProc;
}

void LgButtonSetTitle(LgNodeP node, LgNodeP titleNode) {
	LgButtonDataP bd = (LgButtonDataP)LgNodeUserData(node);
	if (bd->titleNode) LgNodeRemove(bd->titleNode);
	LgNodeAddChild(node, titleNode);
	LgNodeSetZorder(titleNode, LG_UILAYER_BUTTONTITLE);
	LgNodeCenter(titleNode);
	bd->titleNode = titleNode;
}

void LgButtonSetTitleAsText(LgNodeP node, const char *text, const char *fontname, int fontsize) {
	LgNodeP labelNode = LgLabelCreateR(text);

	LgLabelSetFontName(labelNode, fontname);
	LgLabelSetFontSize(labelNode, fontsize);
	LgButtonSetTitle(node, labelNode);
	LgNodeCenterAnchor(labelNode);
	LgNodeRelease(labelNode);
}

void LgButtonSetPressAction(LgNodeP node, LgActionP action) {
	LgButtonDataP bd = (LgButtonDataP)LgNodeUserData(node);
	if (bd->pressAction) LgActionRelease(bd->pressAction);
	bd->pressAction = LgActionRetain(action);
}

void LgButtonSetReleaseAction(LgNodeP node, LgActionP action) {
	LgButtonDataP bd = (LgButtonDataP)LgNodeUserData(node);
	if (bd->releaseAction) LgActionRelease(bd->releaseAction);
	bd->releaseAction = LgActionRetain(action);
}

void LgButtonSetTitleTextColor(LgNodeP node, LgColor color) {
	LgButtonDataP bd = (LgButtonDataP)LgNodeUserData(node);
	bd->titleColor = color;
}

void LgButtonSetTitlePressTextColor(LgNodeP node, LgColor color) {
	LgButtonDataP bd = (LgButtonDataP)LgNodeUserData(node);
	bd->titlePressColor = color;
}

void LgButtonSetFace(LgNodeP node, LgNodeP faceNode) {
	if (faceNode && LgNodeIsSprite(faceNode)) {
		LgButtonDataP bd = (LgButtonDataP)LgNodeUserData(node);
		if (bd->faceNode) LgNodeRemove(bd->faceNode);
		bd->faceNode = faceNode;
		LgNodeAddChild(node, faceNode);
		LgNodeSetZorder(node, LG_UILAYER_BUTTONFACE);
		LgSpriteSetSize(faceNode, &bd->base.size);
		LgNodeCenter(faceNode);
	}
}

void LgButtonSetPressFace(LgNodeP node, LgNodeP faceNode) {
	if (faceNode && LgNodeIsSprite(faceNode)) {
		LgButtonDataP bd = (LgButtonDataP)LgNodeUserData(node);
		if (bd->pressFaceNode) LgNodeRemove(bd->pressFaceNode);
		bd->pressFaceNode = faceNode;
		LgNodeAddChild(node, faceNode);
		LgSpriteSetSize(faceNode, &bd->base.size);
		LgNodeCenter(faceNode);
		LgNodeSetZorder(node, LG_UILAYER_BUTTONFACE);
		LgNodeSetVisible(faceNode, LgFalse);
	}
}

LgSlot *LgButtonSlotClick(LgNodeP node) {
	LgButtonDataP bd = (LgButtonDataP)LgNodeUserData(node);
	return &bd->slotClick;
}