#ifndef _LG_BASE64_H
#define _LG_BASE64_H

#include "lg_base64.h"

char* LgBase64Encode(const char* data, int data_len);

char *LgBase64Decode(const char* data, int data_len);

#endif