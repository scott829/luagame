#ifndef _LG_CLIPNODE_H
#define _LG_CLIPNODE_H

#include "lg_node.h"

enum {
	LG_CLIPNODE_EVENT_SetStencil = LG_NODE_EVENT_LAST,
	LG_CLIPNODE_EVENT_Stencil,
	LG_CLIPNODE_EVENT_SetInvert,
	LG_CLIPNODE_EVENT_IsInvert,
	LG_CLIPNODE_EVENT_LAST,
};

LgNodeP LgClipNodeCreateR();

void LgClipNodeSetStencil(LgNodeP clipnode, LgNodeP stencil);

LgNodeP LgClipNodeStencil(LgNodeP clipnode);

void LgClipNodeSetInvert(LgNodeP clipnode, LgBool invert);

LgBool LgClipNodeIsInvert(LgNodeP clipnode);

LgBool LgNodeIsClip(LgNodeP node);

#endif