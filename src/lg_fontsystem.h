#ifndef _LG_FONTSYSTEM_H
#define _LG_FONTSYSTEM_H

#include "lg_rendersystem.h"
#include "lg_fontdriver.h"

typedef struct LgFont *LgFontP;

LgBool LgFontSystemOpen();

void LgFontSystemClose();

LgFontP LgFontSystemFindFont(const char *alias);

const char *LgFontGetAlias(LgFontP font);

LgFontDriverP LgFontGetDriver(LgFontP font);

LgFontDataP LgFontGetData(LgFontP font);

#endif