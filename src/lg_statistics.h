#ifndef _LG_STATISTICS_H
#define _LG_STATISTICS_H

#include "lg_node.h"

LgNodeP LgStatisticeCreateR();

void LgStatisticeLoopStart(LgNodeP node);

void LgStatisticeLoopEnd(LgNodeP node);

void LgStatisticeUpdateBatch(LgNodeP node, int batch);

#endif