#include "lg_dict.h"
#include "lg_avltree.h"
#include "lg_memory.h"

struct LgDict {
	LgAvlTreeP	tree;
	LgCompare	compareKey;
	LgDestroy	destroyKey, destroyData;
};

typedef struct LgDictItem {
	LgDictP		dict;
	void *		key, *data;
}*LgDictItemP;

int nodeCompare(LgDictItemP lhs, LgDictItemP rhs) {
	if (lhs->dict->compareKey) {
		return lhs->dict->compareKey(lhs->key, rhs->key);
	} else {
		return (int)((char *)lhs->key - (char *)rhs->key);
	}
}

void nodeDestroy(LgDictItemP item) {
	if (item->dict->destroyKey) item->dict->destroyKey(item->key);
	if (item->dict->destroyData) item->dict->destroyData(item->data);
	LgFree(item);
}

LgDictP LgDictCreate(LgCompare compareKey, LgDestroy destroyKey, LgDestroy destroyData) {
	LgDictP dict;

	dict = (LgDictP)LgMallocZ(sizeof(struct LgDict));
	LG_CHECK_ERROR(dict);
	dict->tree = LgRbTreeCreate((LgCompare)nodeCompare, (LgDestroy)nodeDestroy);
	LG_CHECK_ERROR(dict->tree);
	dict->compareKey = compareKey;
	dict->destroyKey = destroyKey;
	dict->destroyData = destroyData;
	return dict;

_error:
	if (dict) LgDictDestroy(dict);
	return NULL;
}

void LgDictDestroy(LgDictP dict) {
	LgAvlTreeDestroy(dict->tree);
	LgFree(dict);
}

LgBool LgDictInsert(LgDictP dict, void *key, void *value) {
	LgDictItemP item = (LgDictItemP)LgMallocZ(sizeof(struct LgDictItem));
	LG_CHECK_ERROR(item);
	item->dict = dict;
	item->key = key;
	item->data = value;
	LG_CHECK_ERROR(LgAvlTreeInsert(dict->tree, item));
	return LgTrue;

_error:
	if (item) nodeDestroy(item);
	return LgFalse;
}

void LgDictReplace(LgDictP dict, void *key, void *value) {
	LgDictItemP item = (LgDictItemP)LgMallocZ(sizeof(struct LgDictItem));
	item->dict = dict;
	item->key = key;
	item->data = value;
	LgAvlTreeReplace(dict->tree, item);
}

void *LgDictFind(LgDictP dict, void *key) {
	struct LgDictItem *item, fitem = {dict, key, NULL};
	item = (LgDictItemP)LgAvlTreeFind(dict->tree, &fitem);
	return item ? item->data : NULL;
}

LgBool LgDictContains(LgDictP dict, void *key) {
	struct LgDictItem fitem = {dict, key, NULL};
	return LgAvlTreeFind(dict->tree, &fitem) != NULL;
}

LgBool LgDictRemove(LgDictP dict, void *key) {
	struct LgDictItem fitem = {dict, key, NULL};
	return LgAvlTreeRemove(dict->tree, &fitem);
}

struct dzDictForeachContext {
	void *			userdata;
	LgForeachPair	callback;
};

static void treeForeach(LgDictItemP item, struct dzDictForeachContext *context) {
	context->callback(item->key, item->data, context->userdata);
}

void LgDictForeach(LgDictP dict, LgForeachPair foreach, void *userdata) {
	struct dzDictForeachContext context = {userdata, foreach};
	LgAvlTreeForeach(dict->tree, (LgForeach)treeForeach, &context);
}

void LgDictClear(LgDictP dict) {
	LgAvlTreeClear(dict->tree);
}

int LgDictCount(LgDictP dict) {
	return LgAvlTreeCount(dict->tree);
}