#ifndef _LG_SLIDER_H
#define _LG_SLIDER_H

#include "lg_controlnode.h"

typedef struct LgSliderData {
	struct LgControlData	base;
	LgNodeP					thumbNode;
	LgActionP				pressAction, releaseAction;
	LgSlot					slotChange;
	float					minValue, maxValue, value;
	float					downX;
	LgBool					isInteger;
}*LgSliderDataP;

LgNodeP LgSliderCreateR(const LgVector2P size);

LgBool LgNodeIsSlider(LgNodeP node);

void LgSliderSetThumb(LgNodeP node, LgNodeP thumb);

float LgSliderValue(LgNodeP node);

void LgSliderSetValue(LgNodeP node, float value);

void LgSliderSetRange(LgNodeP node, float minValue, float maxValue);

void LgSliderRange(LgNodeP node, float *minValue, float *maxValue);

void LgSliderSetPressAction(LgNodeP node, LgActionP action);

void LgSliderSetReleaseAction(LgNodeP node, LgActionP action);

LgSlot *LgSliderSlotChange(LgNodeP node);

void LgSliderSetIsInteger(LgNodeP node, LgBool value);

LgBool LgSliderIsInteger(LgNodeP node);

#endif
