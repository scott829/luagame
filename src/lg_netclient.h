﻿#ifndef _LG_NETCLIENT_H
#define _LG_NETCLIENT_H

#include "lg_basetypes.h"
#include "jansson/jansson.h"
#include "lg_url.h"

enum LgNetClientErrorType {
	LgNetClientErrorNone = 0,
	LgNetClientErrorBadResponse,
	LgNetClientErrorHandshake_Version,	// 客户端版本号不符合要求
	LgNetClientErrorHandshake_Unknown,	// 未知握手错误
	LgNetClientErrorKickByServer,		// 被服务端主动踢下线
	LgNetClientErrorTimeout,			// 接收或发送超时
	LgNetClientErrorReading,			// 接收错误
	LgNetClientErrorWriting,			// 发送错误
};

typedef struct LgNetClientPushMessage {
	const char *route;
	json_t *	json;
}*LgNetClientPushMessageP;

typedef struct LgNetClientDriver {
	const char *name;
	LgBool (* open)(LgUrlP urls, LgSlot *slot);
	void (* close)();
	void (* request)(const char *route, json_t *msg, LgSlot *slot);
	void (* notify)(const char *route, json_t *msg);
	LgBool (* isOpened)();
}*LgNetClientDriverP;

extern LgNetClientDriverP g_currentNetClientDriver;

LgBool LgNetClientOpen(const char *url, LgSlot *slot);

LgBool LgNetClientRequest(const char *route, json_t *msg, LgSlot *slot);

LgBool LgNetClientNotify(const char *route, json_t *msg);

LgBool LgNetClientIsOpened();

void LgNetClientClose();

LgSlot *LgNetClientSlotError();

LgSlot *LgNetClientSlotPush();


#endif