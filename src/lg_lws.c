#include "lg_basetypes.h"
#include "jansson/jansson.h"
#include "event2/event.h"
#include "event2/util.h"
#include "event2/bufferevent.h"
#include "event2/buffer.h"
#include "lg_init.h"
#include "lg_platform.h"
#include "lg_dict.h"
#include "lg_memory.h"
#include "lg_netclient.h"
#include "lg_websocket.h"

#define LG_LWS_VERSION "1.0.0"
#define LG_LWS_CLIENT_TYPE "lws-luagame-tcp"

typedef struct LgLwsRequest {
	char	route[256];
	LgSlot	callback;
}*LgLwsRequestP;

static LgWebSocketP g_ws = NULL;
static LgSlot g_slotConnected = {NULL};
static LgDictP g_requests = NULL;
static int g_reqId;

static void onConnected(void *userdata, void *param) {
	LgSlotCall(&g_slotConnected, NULL);
}

static void onMessage(void *param, void *userdata) {
	LgWebSocketMessageEventP evt = (LgWebSocketMessageEventP)param;
	json_t *msg = json_loadb(evt->data, evt->len, 0, NULL);

	if (msg) {
		json_t *type = json_object_get(msg, "type");

		if (json_is_string(type)) {
			if (strcmp(json_string_value(type), "response") == 0) {
				json_t *seq = json_object_get(msg, "seq");
				if (seq && json_is_integer(seq)) {
					int seqi = (int)json_integer_value(seq);
					LgLwsRequestP request = (LgLwsRequestP)LgDictFind(g_requests, (void *)seqi);
					if (request) {
						LgSlotCall(&request->callback, json_object_get(msg, "body"));
						LgDictRemove(g_requests, (void *)seqi);
					}
				}
			} else if (strcmp(json_string_value(type), "push") == 0) {
				json_t *route = json_object_get(msg, "route");
				if (route && json_is_string(route)) {
					struct LgNetClientPushMessage pmsg;
					pmsg.route = json_string_value(route);
					pmsg.json = json_object_get(msg, "body");
					LgSlotCall(LgNetClientSlotPush(), &pmsg);
				}
			}
		}

		json_delete(msg);
	}
}

static void onError(void *userdata, void *param) {
	enum LgWebSocketErrorType type = (enum LgWebSocketErrorType)param;
	enum LgNetClientErrorType ntype = LgNetClientErrorNone;

	switch(type) {
	case LgWebSocketErrorTimeout:
		ntype = LgNetClientErrorTimeout;
		break;
	case LgWebSocketErrorClose:
		ntype = LgNetClientErrorKickByServer;
		break;
	case LgWebSocketErrorReadData:
		ntype = LgNetClientErrorReading;
		break;
	case LgWebSocketErrorBadResponse:
	case LgWebSocketErrorBadRequest:
	case LgWebSocketErrorNotFound:
	case LgWebSocketErrorForbidden:
		ntype = LgNetClientErrorHandshake_Unknown;
		break;
	}

	if (ntype != LgNetClientErrorNone) {
		LgSlotCall(LgNetClientSlotError(), (void *)ntype);
	}
}

static void destroyRequest(LgLwsRequestP request) {
	LgSlotClear(&request->callback);
	LgFree(request);
}

static LgBool open(LgUrlP urls, LgSlot *slot) {
	char url[LG_PATH_MAX];

	g_ws = LgWebSocketCreateR();
	LG_CHECK_ERROR(g_ws);

	LgSlotAssign(LgWebSocketSlotOpen(g_ws), onConnected, NULL, NULL, NULL);
	LgSlotAssign(LgWebSocketSlotMessage(g_ws), onMessage, NULL, NULL, NULL);
	LgSlotAssign(LgWebSocketSlotError(g_ws), onError, NULL, NULL, NULL);
	
	if (slot) LgSlotAssign(&g_slotConnected, SLOT_PARAMS_VALUE_REF(*slot));
	else LgSlotClear(&g_slotConnected);

	g_requests = LgDictCreate(NULL, NULL, (LgDestroy)destroyRequest);

	sprintf(url, "ws://%s:%d/%s", urls->host, urls->port, urls->path);
	LG_CHECK_ERROR(LgWebSocketOpen(g_ws, url));
	g_reqId = 1;
	return LgTrue;

_error:
	if (g_ws) LgWebSocketClose(g_ws);
	return LgFalse;
}

static void request(const char *route, json_t *msg, LgSlot *slot) {
	json_t *requestObject = json_object();
	char *data;
	LgLwsRequestP request;

	request = (LgLwsRequestP)LgMallocZ(sizeof(struct LgLwsRequest));

	if (slot) LgSlotAssign(&request->callback, SLOT_PARAMS_VALUE_REF(*slot));
	LgDictInsert(g_requests, (void *)g_reqId, request);

	requestObject = json_object();
	json_object_set_new(requestObject, "type", json_string("request"));
	json_object_set_new(requestObject, "route", json_string(route));
	json_object_set_new(requestObject, "seq", json_integer(g_reqId));
	if (msg) json_object_set(requestObject, "body", msg);

	data = json_dumps(requestObject, JSON_COMPACT);
	LgWebSocketSendMessage(g_ws, data, strlen(data), LgWebSocketFrameText);
	free(data);
	json_delete(requestObject);

	g_reqId++;
}

static void notify(const char *route, json_t *msg) {
	json_t *requestObject = json_object();
	char *data;

	requestObject = json_object();
	json_object_set_new(requestObject, "type", json_string("notify"));
	json_object_set_new(requestObject, "route", json_string(route));
	if (msg) json_object_set(requestObject, "body", msg);

	data = json_dumps(requestObject, JSON_COMPACT);
	LgWebSocketSendMessage(g_ws, data, strlen(data), LgWebSocketFrameText);
	free(data);
	json_delete(requestObject);
}

static void close() {
	if (g_ws) {
		LgWebSocketClose(g_ws);
		g_ws = NULL;
	}
	if (g_requests) {
		LgDictDestroy(g_requests);
		g_requests = NULL;
	}
	LgSlotClear(&g_slotConnected);
}


static LgBool isOpened() {
	return g_ws != NULL;
}

struct LgNetClientDriver lwsDriver = {
	"lws",
	open,
	close,
	request,
	notify,
	isOpened,
};