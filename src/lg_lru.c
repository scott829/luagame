#include "lg_lru.h"
#include "lg_dict.h"
#include "lg_list.h"
#include "lg_memory.h"

struct LgLru {
	LgDictP		dict;
	LgListP		list;
	LgDestroy	destroyData;
	int			maxcount;
};

typedef struct LgLruItem {
	void *		key;
	void *		data;
	LgDestroy	destroyData;
	LgListNodeP	listnode;
}*LgLruItemP;

static void destroyItem(LgLruItemP item) {
	if (item->data && item->destroyData) item->destroyData(item->data);
	LgFree(item);
}

LgLruP LgLruCreate(int maxcount, LgCompare compareKey, LgDestroy destroyKey, LgDestroy destroyData) {
	LgLruP lru = (LgLruP)LgMallocZ(sizeof(struct LgLru));
	LG_CHECK_ERROR(lru);
	lru->dict = LgDictCreate(compareKey, destroyKey, (LgDestroy)destroyItem);
	LG_CHECK_ERROR(lru->dict);
	lru->list = LgListCreate(NULL);
	LG_CHECK_ERROR(lru->list);
	lru->destroyData = destroyData;
	lru->maxcount = maxcount;
	return lru;

_error:
	if (lru) LgLruDestroy(lru);
	return NULL;
}

void LgLruDestroy(LgLruP lru) {
	if (lru->list) LgListDestroy(lru->list);
	if (lru->dict) LgDictDestroy(lru->dict);
	LgFree(lru);
}

static LgLruItemP createItem(void *key, void *data, LgDestroy destroyData) {
	LgLruItemP item = (LgLruItemP)LgMallocZ(sizeof(struct LgLruItem));
	item->key = key;
	item->data = data;
	item->destroyData = destroyData;
	return item;
}

LgBool LgLruInsert(LgLruP lru, void *key, void *value) {
	if (!LgDictContains(lru->dict, key)) {
		LgLruItemP item;

		if (LgListCount(lru->list) >= lru->maxcount) {
			LgLruItemP item = (LgLruItemP)LgListNodeValue(LgListLastNode(lru->list));
			LgDictRemove(lru->dict, item->key);
			LgListRemove(lru->list, LgListLastNode(lru->list));
		}

		item = createItem(key, value, lru->destroyData);
		LgDictInsert(lru->dict, key, item);
		item->listnode = LgListInsertBefore(lru->list, LgListFirstNode(lru->list), item);
		return LgTrue;
	} else {
		return LgFalse;
	}
}

void *LgLruLookup(LgLruP lru, void *key) {
	LgLruItemP item = (LgLruItemP)LgDictFind(lru->dict, key);
	if (item) {
		LgListRemove(lru->list, item->listnode);
		item->listnode = LgListInsertBefore(lru->list, LgListFirstNode(lru->list), item);
		return item->data;
	} else {
		return NULL;
	}
}

void LgLruClear(LgLruP lru) {
	LgListClear(lru->list);
	LgDictClear(lru->dict);
}

int LgLruCount(LgLruP lru) {
	return LgListCount(lru->list);
}