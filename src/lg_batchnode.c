#include "lg_batchnode.h"
#include "lg_dict.h"
#include "lg_textureatlas.h"
#include "lg_director.h"

typedef struct LgBatchCharacter {
	LgTextureP		texture;
	struct LgRect	textureRect;
}*LgBatchCharacterP;

typedef struct LgBatchNodeData {
	LgAtlasDrawBufferP	drawBuffer;
	LgTextureAtlasP		textureAtlas;
	LgDictP				characterMap;	// LgBatchCharacterP -> characterId
	LgDictP				textureToIdMap;	// LgTextureP -> textureId
	int					characterId;
	LgBool				inUpdate;
}*LgBatchNodeDataP;

static void destroy(LgNodeP node) {
	LgBatchNodeDataP bd = (LgBatchNodeDataP)LgNodeUserData(node);
	LgAtlasDrawBufferRelease(bd->drawBuffer);
	LgTextureAtlasRelease(bd->textureAtlas);
	LgDictDestroy(bd->characterMap);
	LgDictDestroy(bd->textureToIdMap);
	LgFree(bd);
}

static void initBatchCharacter(const LgBatchInfoP batchInfo, LgBatchCharacterP batchCharacter) {
	batchCharacter->texture = batchInfo->texture;
	batchCharacter->textureRect = batchInfo->textureRt;
}

static int getCharacterId(LgBatchNodeDataP bd, const LgBatchInfoP batchInfo) {
	struct LgBatchCharacter character;
	int textureId, id;

	// 获取纹理id
	textureId = (int)LgDictFind(bd->textureToIdMap, batchInfo->texture);

	if (!textureId) {
		// 纹理是第一次加载，保存它的id
		textureId = LgTextureAtlasAddTexture(bd->textureAtlas, batchInfo->texture);
		LgDictInsert(bd->textureToIdMap, batchInfo->texture, (void *)textureId);
	}

	// 判断character是否存在
	initBatchCharacter(batchInfo, &character);
	id = (int)LgDictFind(bd->characterMap, &character);

	if (!id) {
		// 添加character
		struct LgTextAtlasRecord record;
		LgBatchCharacterP newCharacter;

		record.textureId = textureId;
		record.pt.x = batchInfo->textureRt.x;
		record.pt.y = batchInfo->textureRt.y;
		record.size.width = batchInfo->textureRt.width;
		record.size.height = batchInfo->textureRt.height;
		id = bd->characterId++;
		LgTextureAtlasAddRecord(bd->textureAtlas, id, &record);

		newCharacter = (LgBatchCharacterP)LgMallocZ(sizeof(struct LgBatchCharacter));
		initBatchCharacter(batchInfo, newCharacter);
		LgDictInsert(bd->characterMap, newCharacter, (void *)id);
	}

	return id;
}

static void drawChildren(LgNodeP node) {
	int i, count = LgNodeChildrenCount(node);
	LgBatchNodeDataP bd = (LgBatchNodeDataP)LgNodeUserData(node);
	
	if (!bd->drawBuffer && count > 0) {
		bd->drawBuffer = LgTextureAtlasCreateDrawBufferR(bd->textureAtlas);
		bd->inUpdate = LgTrue;

		for (i = 0; i < count; i++) {
			LgNodeP child = LgNodeChildByIndex(node, i);
			struct LgBatchInfo batchInfo;

			if (LgNodeBatchInfo(child, &batchInfo)){
				int characterId = getCharacterId(bd, &batchInfo);
				LgAtlasBufferDrawQuad(bd->drawBuffer, characterId, 
					&batchInfo.drawQuad.lt, &batchInfo.drawQuad.rt,
					&batchInfo.drawQuad.lb, &batchInfo.drawQuad.rb);
			}
		}

		bd->inUpdate = LgFalse;
	}

	if (bd->drawBuffer) {
		LgAtlasBufferDraw(bd->drawBuffer);
		LgDirectorUpdateBatch(LgAtlasBufferNumberOfBatch(bd->drawBuffer));
	}
}

static void batchNodeEventProc(LgEventType type, LgNodeP node, void *param) {
	switch(type) {
	case LG_NODE_EVENT_Destroy:
		destroy(node);
		break;
	case LG_NODE_EVENT_VisitChildren:
		drawChildren(node);
		return;
	}
	
	LgNodeDefEventProc(type, node, param);
}

static int compareBatchCharacter(const LgBatchCharacterP lhs, const LgBatchCharacterP rhs) {
	return memcmp(lhs, rhs, sizeof(struct LgBatchCharacter));
}

LgNodeP LgBatchNodeCreateR() {
	LgNodeP node;
	LgBatchNodeDataP bd;
	
	node = LgNodeCreateR((LgEvent)batchNodeEventProc);
	LG_CHECK_ERROR(node);
	
	bd = (LgBatchNodeDataP)LgMallocZ(sizeof(struct LgBatchNodeData));
	LG_CHECK_ERROR(bd);
	LgNodeSetUserData(node, bd);
	
	bd->textureAtlas = LgTextureAtlasCreateR();
	bd->characterMap = LgDictCreate((LgCompare)compareBatchCharacter, LgFree, NULL);
	bd->textureToIdMap = LgDictCreate(NULL, NULL, NULL);
	bd->characterId = 1;
	return node;
	
_error:
	if (node) LgNodeRelease(node);
	return NULL;
}

LgBool LgNodeIsBatchNode(LgNodeP node) {
	return LgNodeEventProc(node) == (LgEvent)batchNodeEventProc;
}

void LgBatchNodeMakeDirty(LgNodeP node) {
	LgBatchNodeDataP bd = (LgBatchNodeDataP)LgNodeUserData(node);
	if (bd->drawBuffer && !bd->inUpdate) {
		LgAtlasDrawBufferRelease(bd->drawBuffer);
		bd->drawBuffer = NULL;
	}
}
