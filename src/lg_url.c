#include "lg_url.h"
#include "lg_memory.h"
#include "lg_platform.h"
#include <uriparser/Uri.h>

LgUrlP LgUrlParse(const char *urls) {
	UriParserStateA stateA;
	UriUriA uriA;
	LgUrlP result = NULL;

	stateA.uri = &uriA;
	LG_CHECK_ERROR(uriParseUriA(&stateA, urls) == 0);

	result = (LgUrlP)LgMallocZ(sizeof(struct LgUrl));
	LG_CHECK_ERROR(result);

	if (uriA.scheme.first) result->scheme = strndup(uriA.scheme.first, uriA.scheme.afterLast - uriA.scheme.first);
	if (uriA.hostText.first) result->host = strndup(uriA.hostText.first, uriA.hostText.afterLast - uriA.hostText.first);
	result->port = 80;
	if (uriA.portText.first) result->port = atoi(uriA.portText.first);
	if (uriA.pathHead) result->path = strndup(uriA.pathHead->text.first, uriA.pathTail->text.afterLast - uriA.pathHead->text.first);
	else result->path = strdup("/");
	if (uriA.query.first) result->query = strndup(uriA.query.first, uriA.query.afterLast - uriA.query.first);
	uriFreeUriMembersA(&uriA);
	return result;

_error:
	if (result) LgUrlDestroy(result);
	return NULL;
}

void LgUrlDestroy(LgUrlP url) {
	if (url->scheme) LgFree(url->scheme);
	if (url->host) LgFree(url->host);
	if (url->path) LgFree(url->path);
	if (url->query) LgFree(url->query);
	LgFree(url);
}

LgDictP LgUrlParseQuery(const char *query) {
	if (query) {
		char *d = strdup(query), *token;
		LgDictP result = NULL;

		token = strtok(d, "&");
		while(token) {
			char *name, *value, *p;

			p = strchr(token, '=');
			if (p) {
				p = 0;
				name = token;
				value = p + 1;

				uriUnescapeInPlaceA(name);
				uriUnescapeInPlaceA(value);

				if (!result) LgDictCreate(strnocasecmp, LgFree, LgFree);
				LgDictInsert(result, strdup(name), strdup(value));
			}

			token = strtok(NULL, "&");
		}

		return result;
	} else {
		return NULL;
	}
}