#ifndef _LG_AFFINETRANS_H
#define _LG_AFFINETRANS_H

#include "lg_basetypes.h"
#include "lg_vector2.h"
#include "lg_aabb.h"
#include "lg_matrix44.h"

typedef struct LgAffineTransform {
	float a, b, c, d;
	float tx, ty;
}*LgAffineTransformP;

void LgAffineTransformIndentity(LgAffineTransformP af);

void LgAffineTransformMul(LgAffineTransformP lhs, const LgAffineTransformP rhs);

void LgAffineTransformInitTranslate(LgAffineTransformP af, float tx, float ty);

void LgAffineTransformInitScale(LgAffineTransformP af, float sx, float sy);

void LgAffineTransformInitRotate(LgAffineTransformP af, float radians);

void LgAffineTransformTranslate(LgAffineTransformP af, float tx, float ty);

void LgAffineTransformScale(LgAffineTransformP af, float sx, float sy);

void LgAffineTransformRotate(LgAffineTransformP af, float radians);

void LgAffineTransformMulVector2(const LgAffineTransformP af, LgVector2P vec);

void LgAffineTransformMulAabb(const LgAffineTransformP af, LgAabbP aabb);

void LgAffineTransformMatrix44(const LgAffineTransformP af, LgMatrix44P m);

void LgAffineTransformInverse(LgAffineTransformP af);

#endif