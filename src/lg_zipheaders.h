#ifndef _LG_ZIPHEADERS_H
#define _LG_ZIPHEADERS_H

#define LGZIP_LOCALFILE_SIGNATURE				0x04034b50
#define LGZIP_ARCHIVE_SIGNATURE					0x02014b50
#define LGZIP_ARCHIVE_EXTRA_SIGNATURE			0x08064b50
#define LGZIP_DIGITAL_SIGNATURE					0x05054b50
#define LGZIP_CENTRAL_DIR_RECORD_SIGNATURE		0x06064b50
#define LGZIP_END_CENTRAL_DIR_RECORD_SIGNATURE	0x06054b50
#define LGZIP_CENTRAL_DIR_LOCATOR_SIGNATURE		0x07064b50

#pragma pack(1)

struct LgZipFileHead
{
	unsigned short		version;	//version needed to extract
	unsigned short		purepose;	//general purpose bit flag
	unsigned short		method;		//compression method
	unsigned short		time;		//last mod file time
	unsigned short		date;		//last mod file date
	unsigned int		crc32;		//crc-32
	unsigned int		comprsize;	//compressed size
	unsigned int		uncomprsize;//uncompressed size
	unsigned short		namelen;	//file name length
	unsigned short		extralen;	//extra field length
};

struct LgZipFileArchiveHead
{
	unsigned short	version;		//version made by
	unsigned short	extract;		//version needed to extract
	unsigned short	purpose;		//general purpose bit flag
	unsigned short	method;			//compression method
	unsigned short	modtime;		//last mod file time
	unsigned short	moddate;		//last mode file date
	unsigned int	crc32;			//crc-32
	unsigned int	comprsize;		//compressed size
	unsigned int	uncomprsize;	//uncompressed size
	unsigned short	namelen;		//file name length
	unsigned short	extralen;		//extra field length
	unsigned short	commlen;		//file comment length
	unsigned short	start;			//disk number start
	unsigned short	iattrs;			//internal file attributes
	unsigned int	seattrs;		//external file attributes
	unsigned int	offset;			//relative offset of local header
};

struct LgZipDigitDesc
{
	unsigned int	crc32;
	unsigned int	comprsize;		//compressed size
	unsigned int	uncompsize;		//uncompressed size
};

struct LgZipCentralDirRecord
{
	long long record;				//directory record
	unsigned short madeby;			//version made by
	unsigned short extra;			//version needed to extract
	unsigned int numdisk;			//number of this disk
	unsigned int start;				//number of the disk with the start of the central directory
	long long disk;					//central directory on the disk
	long long total;				//total number of entrieds in the central directory
	long long size;					//size of the central directory
	long long startdisk;			//offset of start of central directory with respect to the starting disk number
};

struct LgZipCentralDirLocator
{
	unsigned int startnum;			//number of the disk whti the start of the zip64 end of central directory
	long long reloffset;			//relative offset the the zip64 end of central directory record
	unsigned int total;				//total number of disks
};

struct LgZipEndCentralDirRecord
{
	unsigned short num;				//number of this disk
	unsigned short start;			//number of the disk with the start of the central directory
	unsigned short centotal;		//total number of entries in the central directory on this disk
	unsigned short total;			//total number of entries in the central directory
	unsigned int size;				//size of the central directory
	unsigned int offset;			//offset of start of central directory with respect to the starting disk number
	unsigned short commlen;			//.ZIP file comment length
};

#pragma pack()

#endif