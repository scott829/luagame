#include "lg_asyncjob.h"
#include <pthread.h>
#include "lg_list.h"
#include "lg_memory.h"

static pthread_t g_thread;
static pthread_mutex_t g_mutex;
static pthread_cond_t g_cond;
static LgListP g_jobs;

static void *threadProc(void *p) {
	while(LgTrue) {
		LgSlot *slot;

		pthread_mutex_lock(&g_mutex);
		
		if (LgListCount(g_jobs) == 0) {
			// 一个都没有了，需要等待
			pthread_cond_wait(&g_cond, &g_mutex);
			if (!g_jobs) {
				// 停止了
				pthread_mutex_unlock(&g_mutex);
				break;
			}
		}
		
		slot = LgListNodeValue(LgListFirstNode(g_jobs));
		LgSlotCall(slot, NULL);
		LgListRemove(g_jobs, LgListFirstNode(g_jobs));
		pthread_mutex_unlock(&g_mutex);
	}
	
	return NULL;
}

static void destroyJob(LgSlot *job) {
	LgSlotClear(job);
	LgFree(job);
}

LgBool LgAsyncJobManagerOpen() {
	g_jobs = LgListCreate((LgDestroy)destroyJob);
	pthread_mutex_init(&g_mutex, NULL);
	pthread_cond_init(&g_cond, NULL);
	pthread_create(&g_thread, NULL, threadProc, NULL);
	return LgTrue;
}

void LgAsyncJobManagerClose() {
	if (g_jobs) {
		pthread_mutex_lock(&g_mutex);
		LgListDestroy(g_jobs);
		g_jobs = NULL;
		pthread_mutex_unlock(&g_mutex);
		pthread_cond_signal(&g_cond);
		pthread_join(g_thread, NULL);
		pthread_mutex_destroy(&g_mutex);
		pthread_cond_destroy(&g_cond);
	}
}

void LgAsyncJobRun(SLOT_PARAMS) {
	LgSlot *slot;
	
	slot = (LgSlot *)LgMallocZ(sizeof(LgSlot));
	LgSlotAssign(slot, SLOT_PARAMS_VALUE);
	
	pthread_mutex_lock(&g_mutex);
	LgListAppend(g_jobs, slot);
	pthread_mutex_unlock(&g_mutex);
	pthread_cond_signal(&g_cond);
}