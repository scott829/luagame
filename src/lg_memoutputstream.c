#include "lg_memoutputstream.h"
#include "lg_memory.h"

struct LgMemOutputStream {
	struct LgOutputStream	base;
	LgBool					attach;
	char *					data;
	int						pos, len, size;
};

static int writeBytes(LgOutputStreamP stream, const void *data, int size) {
	struct LgMemOutputStream *output = (struct LgMemOutputStream *)stream;

	if (output->attach) {
		int available = output->size - output->pos;
		int wsz = LgMin(available, size);
		LgCopyMemory(output->data + output->pos, data, wsz);
		output->pos += wsz;
		return wsz;
	} else {
		if (output->size - output->len < size) {
			int nsz = output->size + (size / 4096 + 1) * 4096;
			output->data = (char *)LgRealloc(output->data, nsz);
			output->size = nsz;
		}
		LgCopyMemory(output->data + output->pos, data, size);
		output->len += size;
		output->pos += size;
		return size;
	}
}

static void destroy(LgOutputStreamP stream) {
	struct LgMemOutputStream *output = (struct LgMemOutputStream *)stream;
	if (!output->attach) {
		if (output->data) LgFree(output->data);
	}
	LgFree(output);
}

struct LgOutputStreamImpl memOutputImpl = {
	(LgDestroy)destroy,
	writeBytes,
};

LgOutputStreamP LgMemOutputStreamCreate(void *data, int size) {
	struct LgMemOutputStream *output =
		(struct LgMemOutputStream *)LgMallocZ(sizeof(struct LgMemOutputStream));
	LG_CHECK_ERROR(output);

	output->base.impl = &memOutputImpl;
	output->data = (char *)data;
	if (output->data) {
		output->attach = LgTrue;
		output->size = output->len = size;
	} else {
		output->attach = LgFalse;
		output->len = output->size = 0;
	}
	output->pos = 0;

	return (LgOutputStreamP)output;

_error:
	if (output) LgOutputStreamDestroy(output);
	return NULL;
}

void *LgMemOutputStreamData(LgOutputStreamP stream) {
	struct LgMemOutputStream *output = (struct LgMemOutputStream *)stream;
	return output->data;
}

int LgMemOutputStreamLen(LgOutputStreamP stream) {
	struct LgMemOutputStream *output = (struct LgMemOutputStream *)stream;
	return output->len;
}

void LgMemOutputStreamClear(LgOutputStreamP stream) {
	struct LgMemOutputStream *output = (struct LgMemOutputStream *)stream;
	if (output->attach) {
		output->len = output->pos = 0;
	} else {
		if (output->data) {
			LgFree(output->data);
			output->data = NULL;
		}
		output->len = output->pos = output->size = 0;
	}
}

void *LgMemOutputStreamDeatchData(LgOutputStreamP stream, int *size) {
	struct LgMemOutputStream *output = (struct LgMemOutputStream *)stream;
	void *data = output->data;
	*size = output->len;
	output->attach = LgFalse;
	output->data = NULL;
	output->len = output->pos = output->size = 0;
	return data;
}