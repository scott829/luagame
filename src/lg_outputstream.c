#include "lg_outputstream.h"

LgBool LgOutputStreamWriteRune(LgOutputStreamP stream, wchar_t wc) {
	int count;
	unsigned char r[6];

	if (wc < 0x80) count = 1;
	else if (wc < 0x800) count = 2;
	else if (wc < 0x10000) count = 3;
	else if (wc < 0x200000) count = 4;
	else if (wc < 0x4000000) count = 5;
	else if (wc <= 0x7fffffff) count = 6;
	else return LgFalse;

	switch(count) {
	case 6: r[5] = 0x80 | (wc & 0x3f); wc = wc >> 6; wc |= 0x4000000;
	case 5: r[4] = 0x80 | (wc & 0x3f); wc = wc >> 6; wc |= 0x200000;
	case 4: r[3] = 0x80 | (wc & 0x3f); wc = wc >> 6; wc |= 0x10000;
	case 3: r[2] = 0x80 | (wc & 0x3f); wc = wc >> 6; wc |= 0x800;
	case 2: r[1] = 0x80 | (wc & 0x3f); wc = wc >> 6; wc |= 0xc0;
	case 1: r[0] = (unsigned char)wc;
	}

	return LgOutputStreamWriteBytes(stream, r, count) == count;
}

LgBool LgOutputStreamWriteByte(LgOutputStreamP stream, unsigned char c) {
	return LgOutputStreamWriteBytes(stream, &c, 1) > 0;
}