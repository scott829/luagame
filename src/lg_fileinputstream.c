#include "lg_meminputstream.h"
#include "lg_memory.h"
#include <stdio.h>

struct LgFileInputStream {
	struct LgInputStream	base;
	FILE *					fp;
	struct LgRange			range;
	int						mark;
};

#define CUR_POS ((int)ftell(s->fp))

#define AVAILABLE (s->range.size - (CUR_POS - s->range.begin))

static void destroy(void *stream) {
	struct LgFileInputStream *s = (struct LgFileInputStream *)stream;
	if (s->fp) fclose(s->fp);
	LgFree(stream);
}

static int available(LgInputStreamP stream) {
	struct LgFileInputStream *s = (struct LgFileInputStream *)stream;
	return AVAILABLE;
}

static int readBytes(LgInputStreamP stream, void *data, int size) {
	struct LgFileInputStream *s = (struct LgFileInputStream *)stream;
	int rsz = LgMin(size, AVAILABLE);
	int r = fread(data, 1, rsz, s->fp);
	return r;
}

static void mark(LgInputStreamP stream) {
	struct LgFileInputStream *s = (struct LgFileInputStream *)stream;
	s->mark = CUR_POS;
}

static LgBool markSupported(LgInputStreamP stream) {
	return LgTrue;
}

static void reset(LgInputStreamP stream) {
	struct LgFileInputStream *s = (struct LgFileInputStream *)stream;
	fseek(s->fp, s->mark, SEEK_SET);
}

struct LgInputStreamImpl fileInputStream = {
	destroy,
	available,
	readBytes,
	mark,
	markSupported,
	reset
};

LgInputStreamP LgFileInputStreamCreate(const char *filename, const LgRangeP range) {
	struct LgFileInputStream *input =
		(struct LgFileInputStream *)LgMallocZ(sizeof(struct LgFileInputStream));
	LG_CHECK_ERROR(input);

	input->base.impl = &fileInputStream;

	input->fp = fopen(filename, "rb");
	LG_CHECK_ERROR(input->fp);

	if (range) {
		LgCopyMemory(&input->range, range, sizeof(struct LgRange));
		fseek(input->fp, range->begin, SEEK_SET);
		input->mark = range->begin;
	} else {
		fseek(input->fp, 0, SEEK_END);
		input->range.size = (int)ftell(input->fp);
		fseek(input->fp, 0, SEEK_SET);
	}

	return (LgInputStreamP)input;

_error:
	if (input) LgInputStreamDestroy(input);
	return NULL;
}