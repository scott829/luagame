#include "lg_actionmanager.h"
#include "lg_scheduler.h"
#include "lg_dict.h"
#include "lg_list.h"

static LgSchedulerItemP g_actmgr_item = NULL;
static LgDictP g_actions;
static LgNodeP g_currentNode = NULL;
static LgBool g_allDeleted = LgFalse;
static LgBool g_currentNodeDeleted = LgFalse;
static LgBool g_inStep = LgFalse;
static LgBool g_actionDeleted = LgFalse;
static LgListP g_emptyNodes = NULL;
// 如果正在一个动作执行循环里面，则新添加的动作都暂存到这里，避免传入错误的delta值
static LgListP g_newActions = NULL;

static void foreachDict(LgNodeP node, LgListP actions, LgForeach foreach) {
	LgListForeach(actions, foreach, NULL);
}

static void doPause(LgActionP action, void *userdata) {
	LgActionPause(action);
}

static void doResume(LgActionP action, void *userdata) {
	LgActionResume(action);
}

static void doStep(LgNodeP node, LgListP actions, void *userdata) {
	if (!g_allDeleted) {
		LgListNodeP p = LgListFirstNode(actions);
		float delta = (float)*((LgTime *)userdata);
		g_currentNode = node;
		g_currentNodeDeleted = LgFalse;
		
		while(p) {
			LgActionP action = (LgActionP)LgListNodeValue(p);

			if (LgActionDeleted(action)) {
				p = LgListNextNode(p);
				continue; // 在该节点的某个动作中被删除了
			}

			if (!LgActionIsPaused(action)) {
				if (!LgActionUpdate(action, delta)) {
					action->deleted = LgTrue;
					g_actionDeleted = LgTrue;
				}
			}
			
			if (g_currentNodeDeleted) {
				LgListNodeP p = LgListFirstNode(actions);
				while(p) {
					action = (LgActionP)LgListNodeValue(p);
					action->deleted = LgTrue;
					g_actionDeleted = LgTrue;
					p = LgListNextNode(p);
				}
				break;
			}

			p = LgListNextNode(p);
		}
	}
}

static void doDelete(LgNodeP node, LgListP actions, void *userdata) {
	LgListNodeP p = LgListFirstNode(actions), next;
	
	while(p) {
		LgActionP action = (LgActionP)LgListNodeValue(p);
		next = LgListNextNode(p);
		if (LgActionDeleted(action)) {
			action->running = LgFalse;
			LgActionStop(action);
			LgListRemove(actions, p);
		}
		p = next;
	}
	
	if (LgListCount(actions) == 0) {
		LgListAppend(g_emptyNodes, node);
	}
}

static void addAction(LgActionP action) {
	LgNodeP node = LgActionNode(action);
	LgListP actions = (LgListP)LgDictFind(g_actions, node);
	
	if (!actions) {
		actions = LgListCreate((LgDestroy)LgActionRelease);
		LgNodeRetain(node);
		LgActionResume(action);
		LgDictInsert(g_actions, node, actions);
	}
	
	LgListAppend(actions, LgActionRetain(action));
}

static LgBool step(LgTime delta, void *userdata) {
	LgListNodeP node;
	
	g_inStep = LgTrue;
	g_allDeleted = LgFalse;
	g_currentNodeDeleted = LgFalse;
	g_actionDeleted = LgFalse;
	
	LgDictForeach(g_actions, (LgForeachPair)doStep, &delta);
	g_inStep = LgFalse;
	
	if (g_allDeleted) LgDictClear(g_actions);
	else if (g_actionDeleted) LgDictForeach(g_actions, (LgForeachPair)doDelete, NULL);
	
	// 删除所有空的节点
	node = LgListFirstNode(g_emptyNodes);
	while(node != NULL) {
		LgDictRemove(g_actions, LgListNodeValue(node));
		node = LgListNextNode(node);
	}
	
	LgListClear(g_emptyNodes);
	
	// 添加新的动作
	if (g_newActions) {
		LgListNodeP it = LgListFirstNode(g_newActions);
		
		while(it) {
			LgActionP action = (LgActionP)LgListNodeValue(it);
			action->newItemsNode = NULL;
			addAction(action);
			it = LgListNextNode(it);
		}
		
		LgListDestroy(g_newActions);
		g_newActions = NULL;
	}

	return LgTrue;
}

LgBool LgActionManagerOpen() {
	g_actmgr_item = LgSchedulerRun(step, NULL, NULL, 0, LG_LOOP_FOREVER, 0, LgFalse);
	g_actions = LgDictCreate(NULL, (LgDestroy)LgNodeRelease, (LgDestroy)LgListDestroy);
	g_emptyNodes = LgListCreate(NULL);
	return LgTrue;
}

void LgActionManagerClose() {
	if (g_actmgr_item) {
		LgSchedulerRemove(g_actmgr_item);
		g_actmgr_item = NULL;
	}
	if (g_actions) {
		LgDictDestroy(g_actions);
		g_actions = NULL;
	}
	if (g_emptyNodes) {
		LgListDestroy(g_emptyNodes);
		g_emptyNodes = NULL;
	}
}

void LgActionManagerAdd(LgNodeP node, LgActionP action) {
	if(node && action && !LgActionNode(action)) {
		action->running = LgTrue;
		LgActionAttachNode(action, node);
		LgActionReset(action);

		if (g_inStep) {
			if (!g_newActions) g_newActions = LgListCreate(NULL);
			action->newItemsNode = LgListAppend(g_newActions, action);
		} else {
			addAction(action);
		}
	}
}

void LgActionManagerPause(LgNodeP node) {
	if (node) {
		LgListP actions = (LgListP)LgDictFind(g_actions, node);
		if (actions) LgListForeach(actions, (LgForeach)doPause, NULL);
	} else {
		LgDictForeach(g_actions, (LgForeachPair)foreachDict, (LgForeach)doPause);
	}
}

void LgActionManagerResume(LgNodeP node) {
	if (node) {
		LgListP actions = (LgListP)LgDictFind(g_actions, node);
		if (actions) LgListForeach(actions, (LgForeach)doResume, NULL);
	} else {
		LgDictForeach(g_actions, (LgForeachPair)foreachDict, (LgForeach)doResume);
	}
}

void LgActionManagerStop(LgNodeP node, LgActionP action) {
	if (!node) {
		if (g_inStep) g_allDeleted = LgTrue;
		else LgDictClear(g_actions);
	} else {
		if (!action) {
			if (g_inStep && node == g_currentNode) g_currentNodeDeleted = LgTrue;
			else LgDictRemove(g_actions, node);
		} else {
			if (action->newItemsNode) {
				// 在新建列表内
				action->newItemsNode = NULL;
				LgActionRelease((LgActionP)LgListNodeValue(action->newItemsNode));
				LgListRemove(g_newActions, action->newItemsNode);
			} else {
				LgListP actions = (LgListP)LgDictFind(g_actions, node);
				if (actions) {
					LgListNodeP node;
					for (node = LgListFirstNode(actions); node; node = LgListNextNode(node)) {
						LgActionP _action = (LgActionP)LgListNodeValue(node);
						if (_action == action) {
							if (g_inStep) {
								action->deleted = LgTrue;
								g_actionDeleted = LgTrue;
							} else {
								action->running = LgFalse;
								LgActionStop(action);
								LgListRemove(actions, node);
							}
							break;
						}
					}
				}
			}
		}
	}	
}

int LgActionManagerActionsCount(LgNodeP node) {
	LgListP actions = (LgListP)LgDictFind(g_actions, node);
	return actions ? LgListCount(actions) : 0;
}