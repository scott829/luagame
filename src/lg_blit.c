#include "lg_blit.h"

void LgBlit_A8R8G8B8_To_L8(unsigned char *dest, const unsigned char *src) {
	dest[0] = (unsigned char)(((src[0] * 38 + src[1] * 75 + src[2] * 15) >> 7) * (dest[3] / 255.0f));
}

void LgBlit_A8R8G8B8_To_A8L8(unsigned char *dest, const unsigned char *src) {
	dest[0] = (unsigned char)(((src[0] * 38 + src[1] * 75 + src[2] * 15) >> 7));
	dest[1] = src[3];
}

void LgBlit_A8R8G8B8_To_A8R8G8B8(unsigned char *dest, const unsigned char *src) {
	*((unsigned int *)dest) = *((const unsigned int *)src);
}

void LgBlit_A8L8_To_L8(unsigned char *dest, const unsigned char *src) {
	dest[0] = (unsigned char )(src[0] * (src[1] / 255.0f));
}

void LgBlit_A8L8_To_A8L8(unsigned char *dest, const unsigned char *src) {
	*((unsigned short *)dest) = *((const unsigned short *)src);
}

void LgBlit_A8L8_To_A8R8G8B8(unsigned char *dest, const unsigned char *src) {
	dest[0] = src[0], dest[1] = src[0], dest[2] = src[0], dest[3] = src[1];
}

void LgBlit_L8_To_L8(unsigned char *dest, const unsigned char *src) {
	*dest = *src;
}

void LgBlit_L8_To_A8L8(unsigned char *dest, const unsigned char *src) {
	dest[0] = src[0], dest[1] = src[0];
}

void LgBlit_L8_To_A8R8G8B8(unsigned char *dest, const unsigned char *src) {
	dest[0] = 255, dest[1] = 255, dest[2] = 255, dest[3] = *src;
}
