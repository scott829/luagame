#ifndef _LG_ANIMATION_H
#define _LG_ANIMATION_H

#include "lg_node.h"
#include <spine/spine.h>

typedef struct LgAnimation {
	LG_REFCNT_HEAD
	spAtlas *			atlas;
	spSkeletonData *	skeletonData;
	spSkeletonJson *	skeletonJson;
}*LgAnimationP;

LG_REFCNT_FUNCTIONS_DECL(LgAnimation)

enum LgAnimationEventType {
	LgAnimationEventAnimationStart = 1,
	LgAnimationEventAnimationEnd,
	LgAnimationEventAnimationComplete,
	LgAnimationEventAnimationCustom,
};

typedef struct LgAnimationEventData {
	const char *	name;
	int				intVal;
	float			floatVal;
	const char *	stringVal;
}LgAnimationEventDataP;

typedef struct LgAnimationEvent {
	enum LgAnimationEventType	type;
	const char *				animationName;
	int							loopCount;
	struct LgAnimationEventData	data;
}*LgAnimationEventP;

LgAnimationP LgAnimationCreateR(const char *filename);

LgBool LgNodeIsAnimation(LgNodeP node);

LgNodeP LgAnimationNodeCreateR(const char *animationFilename, const char *animationName, LgBool loop);

LgBool LgAnimationSetAnimation(LgNodeP node, const char *name, LgBool loop);

LgBool LgAnimationAddAnimation(LgNodeP node, const char *name, LgBool loop, float delay);

void LgAnimationStopAnimation(LgNodeP node);

float LgAnimationAnimationDuration(LgNodeP node, const char *name);

LgBool LgAnimationSetSkin(LgNodeP node, const char *name);

LgSlot *LgAnimationSlotEvent(LgNodeP node);

void LgAnimationSetExactTouch(LgNodeP node, LgBool value);

LgBool LgAnimationIsExactTouch(LgNodeP node);

#endif