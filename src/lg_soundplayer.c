﻿#include "lg_soundplayer.h"
#if defined(APPLE)
#	include <OpenAL/al.h>
#	include <OpenAL/alc.h>
#else
#	include "openal/AL/al.h"
#	include "openal/AL/alc.h"
#endif
#include "lg_sampledriver.h"
#include "lg_dict.h"
#include "lg_memory.h"
#include "lg_resourcemanager.h"
#include "lg_memoutputstream.h"
#include "lg_array.h"
#include "lg_log.h"

#define BKG_QUEUE_LEN 3
#define QUEUE_BUFFER_SEC 1

LG_REFCNT_FUNCTIONS(LgSoundData)

static ALCdevice *g_device = NULL;
static ALCcontext *g_context = NULL;
static LgSoundId g_nextSoundId = 1;
static LgDictP g_playingEffects = NULL;
static ALuint g_bkgMusicSource = 0;
static LgSampleProviderP g_bkgMusicSp = NULL;
static LgBool g_musicPlaying = LgFalse;
static LgSlot g_musicEndedCallback;
static float g_soundGain = 1.0f;

struct LgSoundData {
	LG_REFCNT_HEAD
	ALuint	id;
};

typedef struct LgEffect {
	ALuint source;
	LgSoundDataP sd;
}*LgEffectP;

static void destroyEffect(LgEffectP effect) {
	alSourceStop(effect->source);
	alDeleteSources(1, &effect->source);
	LgSoundDataRelease(effect->sd);
	LgFree(effect);
}

static ALenum alFormat(const LgSoundFormatP format) {
	if (format->numberChannel == 1) {
		if (format->sampleSize == 1) {
			return AL_FORMAT_MONO8;
		} else if (format->sampleSize == 2) {
			return AL_FORMAT_STEREO8;
		}
	} else if (format->numberChannel == 2) {
		if (format->sampleSize == 1) {
			return AL_FORMAT_MONO16;
		} else if (format->sampleSize == 2) {
			return AL_FORMAT_STEREO16;
		}
	}

	return 0;
}

static void destroySoundData(LgSoundDataP sd) {
	if (sd->id) alDeleteBuffers(1, &sd->id);
	LgFree(sd);
}

LgSoundDataP LgSoundDataCreateR(const char *filename) {
	LgSampleProviderP sp = NULL;
	struct LgSoundFormat format;
	char *data = NULL;
	LgOutputStreamP output = NULL;
	LgSoundDataP sd = (LgSoundDataP)LgMalloc(sizeof(struct LgSoundData));

	LG_CHECK_ERROR(sd);
	LG_REFCNT_INIT(sd, destroySoundData);

	sp = LgSampleProviderCreateR(filename);
	LG_CHECK_ERROR(sp);

	LgSampleProviderFormat(sp, &format);
	data = (char *)LgMalloc(LgSoundFormatBytesPerSecond(&format));
	output = LgMemOutputStreamCreate(NULL, 0);

	for (;;) {
		int rsz = LgSampleProviderReadSoundData(sp, data, format.rate);
		if (!rsz) break;
		LgOutputStreamWriteBytes(output, data, rsz * LgSoundFormatFrameSize(&format));
	}

	alGenBuffers(1, &sd->id);
	alBufferData(sd->id, alFormat(&format), LgMemOutputStreamData(output), LgMemOutputStreamLen(output), format.rate);
	LgSampleProviderRelease(sp);
	LgFree(data);
	LgOutputStreamDestroy(output);
	return sd;

_error:
	if (sd) LgSoundDataRelease(sd);
	if (sp) LgSampleProviderRelease(sp);
	if (data) LgFree(data);
	if (output) LgOutputStreamDestroy(output);
	return NULL;
}

LgBool LgSoundPlayerOpen() {
	g_nextSoundId = 1;
	g_bkgMusicSource = 0;
	g_musicPlaying = LgFalse;
	g_device = alcOpenDevice(NULL);
	LG_CHECK_ERROR(g_device);
	g_context = alcCreateContext(g_device, NULL);
	LG_CHECK_ERROR(g_context);
	alcMakeContextCurrent(g_context);
	alGenSources(1, &g_bkgMusicSource);
	LG_CHECK_ERROR(g_bkgMusicSource);
	g_playingEffects = LgDictCreate(NULL, NULL, (LgDestroy)destroyEffect);
	LgZeroMemory(&g_musicEndedCallback, sizeof(g_musicEndedCallback));

	// 设置默认音量
	LgSoundPlayerSetMusicGain(1.0f);
	LgSoundPlayerSetEffectGain(1.0f);
	LgLogInfo("sound player initialized.", NULL);
	return LgTrue;

_error:
	LgSoundPlayerClose();
	LgLogError("failed to initialize sound player.", NULL);
	return LgFalse;
}

void LgSoundPlayerClose() {
	if (g_playingEffects) {
		LgSoundPlayerStopAllEffects();
		LgDictDestroy(g_playingEffects);
		g_playingEffects = NULL;
	}
	if (g_bkgMusicSource) {
		LgSoundPlayerStopBackgroundMusic();
		alDeleteSources(1, &g_bkgMusicSource);
		g_bkgMusicSource = 0;
	}
	if (g_context) {
		alcDestroyContext(g_context);
		g_context = NULL;
	}
	if (g_device) {
		alcCloseDevice(g_device);
		g_device = NULL;
	}
	LgSlotClear(&g_musicEndedCallback);
}

static int queueBuffer(LgSampleProviderP sp, ALuint source, int count) {
	char *data;
	struct LgSoundFormat format;
	int rcount = 0, i;

	LgSampleProviderFormat(sp, &format);
	data = (char *)LgMalloc(LgSoundFormatBytesPerSecond(&format) * QUEUE_BUFFER_SEC);

	if (data) {
		for (i = 0; i < count; i++) {
			ALuint buffer;
			int count = LgSampleProviderReadSoundData(sp, data, format.rate * QUEUE_BUFFER_SEC);
			if (count <= 0) break;
			alGenBuffers(1, &buffer);
			alBufferData(buffer, alFormat(&format), data, count * LgSoundFormatFrameSize(&format), format.rate);
			alSourceQueueBuffers(source, 1, &buffer);
			rcount++;
		}
		LgFree(data);
	}

	return rcount;
}

LgBool LgSoundPlayerPlayBackgroundMusic(const char *filename, SLOT_PARAMS) {
	LgSoundPlayerStopBackgroundMusic();

	g_bkgMusicSp = LgSampleProviderCreateR(filename);
	if (g_bkgMusicSp) {
		// 缓存buffer
		LgSlotAssign(&g_musicEndedCallback, SLOT_PARAMS_VALUE);
		if (queueBuffer(g_bkgMusicSp, g_bkgMusicSource, BKG_QUEUE_LEN) > 0) {
			alSourcePlay(g_bkgMusicSource);
			g_musicPlaying = LgTrue;
			return LgTrue;
		}
		
		LgSampleProviderRelease(g_bkgMusicSp);
		g_bkgMusicSp = NULL;
	}

	return LgFalse;
}

void stopBackgroundMusic(LgBool finished) {
	// 如果正在播放，则停止它
	if (g_musicPlaying) {
		ALint state;
		ALint count, i;
		LgSlot callback;

		alGetSourcei(g_bkgMusicSource, AL_SOURCE_STATE, &state);
		if (state == AL_PLAYING) alSourceStop(g_bkgMusicSource);

		// 删除所有未处理的buffer
		alGetSourcei(g_bkgMusicSource, AL_BUFFERS_PROCESSED, &count);

		for (i = 0; i < count; i++) {
			ALuint buffer;
			alSourceUnqueueBuffers(g_bkgMusicSource, 1, &buffer);
			alDeleteBuffers(1, &buffer);
		}

		LgSampleProviderRelease(g_bkgMusicSp);
		g_bkgMusicSp = NULL;
		g_musicPlaying = LgFalse;
		
		// 由于callback里面可能播放新的音乐，所以需要clone一下
		LgZeroMemory(&callback, sizeof(callback));
		LgSlotClone(&callback, &g_musicEndedCallback);
		LgSlotClear(&g_musicEndedCallback);
		if (finished) {
			LgSlotCall(&callback, NULL);
			LgSlotClear(&callback);
		}
	}
}

void LgSoundPlayerStopBackgroundMusic() {
	stopBackgroundMusic(LgFalse);
}

LgSoundId LgSoundPlayerPlayEffect(const char *filename) {
	LgSoundDataP sd = LgResourceManagerLoadSound(filename);

	if (sd) {
		LgEffectP effect = (LgEffectP)LgMallocZ(sizeof(struct LgEffect));
		effect->sd = LgSoundDataRetain(sd);
		alGenSources(1, &effect->source);
		alSourceQueueBuffers(effect->source, 1, &sd->id);
		LgDictReplace(g_playingEffects, (void *)g_nextSoundId, effect);
		alSourcePlay(effect->source);
		return g_nextSoundId++;
	} else {
		return 0;
	}
}

void LgSoundPlayerStopEffect(LgSoundId id) {
	LgDictRemove(g_playingEffects, (void *)id);
}

void LgSoundPlayerStopAllEffects() {
	LgDictClear(g_playingEffects);
}

static void deleteStoppedEffect(void *key, void *value, void *userdata) {
	LgArrayP *d = (LgArrayP *)userdata;
	LgEffectP effect = (LgEffectP)value;
	ALint processed;

	alGetSourcei(effect->source, AL_BUFFERS_PROCESSED, &processed);
	if (processed > 0) {
		if (!*d) *d = LgArrayCreate(NULL);
		LgArrayAppend(*d, key);
	}
}

void LgSoundPlayerUpdate() {
	if (g_device) {
		ALint state;
		LgArrayP deleteEffects = NULL;

		if (g_musicPlaying) {
			// 正在播放背景音乐
			ALint i, processed, queued, need;
			int rcount = 0;

			alGetSourcei(g_bkgMusicSource, AL_SOURCE_STATE, &state);
			alGetSourcei(g_bkgMusicSource, AL_BUFFERS_PROCESSED, &processed);
			
			for (i = 0; i < processed; i++) {
				ALuint buffer;
				alSourceUnqueueBuffers(g_bkgMusicSource, 1, &buffer);
				alDeleteBuffers(1, &buffer);
			}
			
			alGetSourcei(g_bkgMusicSource, AL_BUFFERS_QUEUED, &queued);
			need = BKG_QUEUE_LEN - queued;
			
			for (i = 0; i < need; i++) {
				rcount = queueBuffer(g_bkgMusicSp, g_bkgMusicSource, need);
			}
			
			if (rcount + queued == 0) {
				// 没有需要播放的buffer了
				stopBackgroundMusic(LgTrue);
			}
		}

		// 删除已经播放完成的effect
		LgDictForeach(g_playingEffects, deleteStoppedEffect, &deleteEffects);
		if (deleteEffects) {
			int i;
			for (i = 0; i < LgArrayLen(deleteEffects); i++) LgDictRemove(g_playingEffects, LgArrayGet(deleteEffects, i));
			LgArrayDestroy(deleteEffects);
		}
	}
}

float LgSoundPlayerMusicGain() {
	float v;
	alGetSourcef(g_bkgMusicSource, AL_GAIN, &v);
	return v;
}

void LgSoundPlayerSetMusicGain(float gain) {
	alSourcef(g_bkgMusicSource, AL_GAIN, gain);
}

float LgSoundPlayerEffectGain() {
	return g_soundGain;
}

static void setGain(void *key, void *value, void *userdata) {
	LgEffectP effect = (LgEffectP)value;
	alSourcef(effect->source, AL_GAIN, g_soundGain);
}

void LgSoundPlayerSetEffectGain(float gain) {
	if (gain > 1.0f) gain = 1.0f;
	else if (gain < 0) gain = 0;
	g_soundGain = gain;
	LgDictForeach(g_playingEffects, setGain, NULL);
}
