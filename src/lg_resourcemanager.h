#ifndef _LG_RESOURCE_H
#define _LG_RESOURCE_H

#include "lg_rendersystem.h"
#include "lg_sample_mp3.h"
#include "lg_soundplayer.h"
#include "lg_animation.h"
#include "lg_sprite.h"

#define LOAD_DECLARE(type, name) \
	type LgResourceManagerLoad##name(const char *filename); \
	void LgResourceManagerAsyncLoad##name(const char *filename, LgSlot *callback);

typedef struct LgResourceDriver *LgResourceDriverP;

typedef struct LgResource {
	LgResourceDriverP	driver;
	void *				data;
}*LgResourceP;

typedef struct LgAsyncLoadResult {
	const char *	filename;
	LgBool			succeeded;
	float			time;
}*LgAsyncLoadResultP;

LgBool LgResourceManagerOpen();

void LgResourceManagerClose();

void LgResourceManagerClear();

int LgResourceManagerCachedCount();

int LgResourceManagerMaxCount();

LOAD_DECLARE(LgTextureP, Texture);

LOAD_DECLARE(LgSoundDataP, Sound);

LOAD_DECLARE(LgAnimationP, Animation);

LOAD_DECLARE(LgTexturePackP, TexturePack)

#endif