﻿#include "lg_director.h"
#include "lg_list.h"
#include "lg_refcnt.h"
#include "lg_radians.h"
#include "lg_vector3.h"
#include "lg_window.h"
#include "lg_touch.h"
#include "lg_statistics.h"
#include "lg_keyevent.h"
#include "lg_layer.h"
#include "lg_text_field.h"
#include "lg_scheduler.h"
#include "lg_resourcemanager.h"
#include "lg_scriptenv.h"
#include "lg_dict.h"
#include "lg_array.h"

static LgListP g_sceneStack = NULL;
static struct LgSize g_viewSize;
static LgDictP g_touches = NULL;
static LgNodeP g_statistics = NULL;
static LgBool g_showStatistics = LgFalse;
static float g_speed = 1.0f;
static LgBool g_paused = LgFalse;
static LgKeyEventP g_keyEvent = NULL;
static LgNodeP g_currentInputNode = NULL;
static LgTime g_prevTime = 0;
static LgBool g_hasCamera = LgFalse;
static float g_cameraCenterX, g_cameraCenterY, g_cameraScale;

static void addTouch(LgTouchP touch) {
	int id = LgTouchId(touch);
	LgListP touches = (LgListP)LgDictFind(g_touches, (void *)id);
	if (!touches) {
		touches = LgListCreate((LgDestroy)LgTouchRelease);
		LgDictInsert(g_touches, (void *)id, touches);
	}
	LgListAppend(touches, touch);
}
	
LgBool LgDirectorOpen() {
	struct LgVector2 offset;

	g_sceneStack = LgListCreate(LgRelease);
	LG_CHECK_ERROR(g_sceneStack);
	g_viewSize.width = 960;
	g_viewSize.height = 640;
	g_hasCamera = LgFalse;
	g_touches = LgDictCreate(NULL, NULL, (LgDestroy)LgListDestroy);
	g_keyEvent = LgKeyEventCreate();

	g_statistics = LgStatisticeCreateR();

	offset.x = 10, offset.y = 10;
	LgNodeSetPosition(g_statistics, &offset);
	if (g_statistics) LgNodeDoEvent(LG_NODE_EVENT_Enter, g_statistics, NULL);

	return LgTrue;

_error:
	LgDirectorClose();
	return LgFalse;
}

void LgDirectorClose() {
	if (g_statistics) {
		LgNodeDoEvent(LG_NODE_EVENT_Exit, g_statistics, NULL);
		LgNodeRelease(g_statistics);
		g_statistics = NULL;
	}

	if (g_sceneStack) {
		while(LgListCount(g_sceneStack) > 0) LgDirectorPop();
		LgListDestroy(g_sceneStack);
		g_sceneStack = NULL;
	}
	
	if (g_touches) {
		LgDictDestroy(g_touches);
		g_touches = NULL;
	}
	
	LgKeyEventDestroy(g_keyEvent);
}

static void switchScene(LgNodeP newScene, LgNodeP oldScene) {
	if (newScene) LgNodeDoEvent(LG_NODE_EVENT_Enter, newScene, NULL);
	if (oldScene) LgNodeDoEvent(LG_NODE_EVENT_Exit, oldScene, NULL);
}

void LgDirectorPush(LgNodeP scene) {
	LgNodeP oldScene = LgDirectorCurrentScene();
	LgNodeRetain(scene);
	LgListAppend(g_sceneStack, scene);
	switchScene(scene, oldScene);
}

void LgDirectorPop() {
	if (LgListCount(g_sceneStack) > 0) {
		LgNodeP oldScene = LgDirectorCurrentScene();
		LgNodeRetain(oldScene);
		LgListRemove(g_sceneStack, LgListLastNode(g_sceneStack));
		switchScene(LgDirectorCurrentScene(), oldScene);
		LgSchedulerReleaseAtNextFrame(oldScene);
	}
}

void LgDirectorReplace(LgNodeP scene) {
	LgNodeP oldScene = LgDirectorCurrentScene();
	LgNodeRetain(oldScene);

	LgListRemove(g_sceneStack, LgListLastNode(g_sceneStack));
	LgNodeRetain(scene);
	LgListAppend(g_sceneStack, scene);
	switchScene(scene, oldScene);

	LgSchedulerReleaseAtNextFrame(oldScene);
}

LgNodeP LgDirectorCurrentScene() {
	return LgListCount(g_sceneStack) > 0 ? (LgNodeP)LgListNodeValue(LgListLastNode(g_sceneStack)) : NULL;
}

void LgDirectorSetProjection() {
	struct LgMatrix44 projectionMatrix, viewMatrix;
	struct LgVector3 eye, at, up;
	float _scale = 1.0f / g_cameraScale;
		
	// 设置投影矩阵
	LgMatrix44PerspectiveFovLH(&projectionMatrix, LgAngleToRadians(60),
								(float)g_viewSize.width / g_viewSize.height, 0.1f, 3000.0f);
	LgRenderSystemSetTransform(LgTransformProjection, &projectionMatrix);
		
	// 设置视图矩阵
	eye.x = g_cameraCenterX, eye.y = g_cameraCenterY, eye.z = (float)g_viewSize.height / 1.1566f * _scale;
	at.x = g_cameraCenterX, at.y = g_cameraCenterY, at.z = 0;
	up.x = 0, up.y = -1.0f, up.z = 0;
	LgMatrix44LookAtLH(&viewMatrix, &eye, &at, &up);
	LgRenderSystemSetTransform(LgTransformView, &viewMatrix);
}

void LgDirectorUpdate() {
	LgNodeP current = LgDirectorCurrentScene();
	LgTime currentTime = LgGetTimeOfDay();
	float delta;

	if (g_prevTime == 0) {
		delta = 0;
		g_prevTime = currentTime;
	} else {
		delta = (float)(currentTime - g_prevTime);
		g_prevTime = currentTime;
	}
	
	LgRenderSystemBegin(NULL);
	LgRenderSystemClear(LgClearFlagColor, LgColorXRGB(0, 0, 0), 0);
	LgDirectorSetProjection();
	if (g_statistics) LgStatisticeLoopStart(g_statistics);
	if (current) LgNodeVisit(current, delta);
	if (g_statistics) LgStatisticeLoopEnd(g_statistics);
	if (g_showStatistics && g_statistics)
		LgNodeVisit(g_statistics, delta);
	LgRenderSystemEnd();
}

void LgDirectorSetViewSize(const LgSizeP size) {
	g_viewSize = *size;
	g_cameraCenterX = (float)g_viewSize.width / 2;
	g_cameraCenterY = (float)g_viewSize.height / 2;
	g_cameraScale = 1.0f;
}

void LgDirectorViewSize(LgSizeP size) {
	*size = g_viewSize;
}

void LgDirectorScreenToView(const LgPointP pt, LgVector2P viewPt) {
	struct LgMatrix44 viewMatrix, projMatrix;
	float zclip;
	struct LgSize windowSize;
	struct LgVector3 position;

	LgRenderSystemGetTransform(LgTransformView, &viewMatrix);
	LgRenderSystemGetTransform(LgTransformProjection, &projMatrix);
	LgMatrix44Mul(&viewMatrix, &projMatrix);
	zclip = viewMatrix.m44[3][2] / viewMatrix.m44[3][3];

	LgWindowSize(&windowSize);
	position.x = (((2.0f * pt->x) / windowSize.width) - 1.0f);
	position.y = (-((2.0f * pt->y) / windowSize.height) + 1.0f);
	position.z = zclip;

	LgMatrix44Inverse(&viewMatrix);
	LgMatrix44MulVector2(&viewMatrix, &position);
	viewPt->x = position.x, viewPt->y = position.y;
}

void LgDirectorHandleKeyDown(enum LgKey key) {
	if (!g_currentInputNode) {
		int i;
		LgNodeP scene;

		if (key == LgKeyF3) g_showStatistics = !g_showStatistics;

		LgKeyEventDown(g_keyEvent, key);
		scene = LgDirectorCurrentScene();
		if (scene) {
			for (i = 0; i < LgNodeChildrenCount(scene); i++) {
				LgNodeP child = LgNodeChildByIndex(scene, i);
				if (LgNodeIsLayer(child)) {
					LgSlot *slot = LgLayerSlotKeyDown(child);
					if (slot) LgSlotCall(slot, g_keyEvent);
				}
			}
		}
	}
}

void LgDirectorHandleKeyUp(enum LgKey key) {
	if (!g_currentInputNode) {
		int i;
		LgNodeP scene;

		LgKeyEventUp(g_keyEvent, key);
		scene = LgDirectorCurrentScene();
		if (scene) {
			for (i = 0; i < LgNodeChildrenCount(scene); i++) {
				LgNodeP child = LgNodeChildByIndex(scene, i);
				if (LgNodeIsLayer(child)) {
					LgSlot *slot = LgLayerSlotKeyUp(child);
					if (slot) LgSlotCall(slot, g_keyEvent);
				}
			}
		}
	}
}

static LgDictP createTouchesGroup() {
	return LgDictCreate(NULL, NULL, (LgDestroy)LgArrayDestroy);
}

static void addTouchByNode(LgDictP d, LgNodeP node, LgTouchP touch) {
	LgArrayP touches = (LgArrayP)LgDictFind(d, node);
	if (touches == NULL) {
		touches = LgArrayCreate(NULL);
		LgDictInsert(d, node, touches);
	}
	LgArrayAppend(touches, touch);
}

static void fireTouchEventCallback(LgNodeP node, LgArrayP touches, void *userdata) {
	struct LgNodeTouchEvent evt;
	
	evt.type = (enum LgNodeTouchType)userdata;
	evt.num = LgArrayLen(touches);
	evt.touches = (LgTouchP *)LgArrayData(touches);

	LgNodeHandleTouch(node, &evt);
}

static void fireTouchEventAndDeleteTouchesGroup(LgDictP d, int type) {
	LgDictForeach(d, (LgForeachPair)fireTouchEventCallback, (void *)type);
	LgDictDestroy(d);
}

void LgDirectorHandleTouchBegan(int num, const int *touchid, const LgPointP pts) {
	LgNodeP currentScene;
	struct LgVector2 vpt;

	currentScene = LgDirectorCurrentScene();
	
	if (currentScene) {
		int i;
		LgDictP touches = createTouchesGroup();
		LgBool hasTextField = LgFalse;

		// 设置每个touch的节点
		for (i = 0; i < num; i++) {
			LgNodeP node;

			LgDirectorScreenToView(pts + i, &vpt);
			
			node = LgNodeHitTest(currentScene, &vpt, NULL);
			
			if (node) {
				LgNodeP pnode = node;
				while(pnode) {
					LgTouchP touch = LgTouchCreateR(touchid[i], pnode, &vpt);
					addTouch(touch);
					addTouchByNode(touches, pnode, touch);
					if (!LgNodeIsTouchBubble(pnode)) break;
					pnode = LgNodeParent(pnode);
				}
			}
			
			if (!hasTextField && node && LgNodeIsTextField(node)) {
				// 被点击的节点是一个文本输入节点
				LgWindowShowKeyboard(LgTrue);
				g_currentInputNode = node;
				LgTextFieldSetFocus(g_currentInputNode);
				hasTextField = LgTrue;
			}
		}
		
		// 分发事件
		fireTouchEventAndDeleteTouchesGroup(touches, LgNodeTouchBegan);
		
		if (!hasTextField) {
			// 没有点击文本输入节点
			if (g_currentInputNode) {
				LgTextFieldKillFocus(g_currentInputNode);
				g_currentInputNode = NULL;
				LgWindowShowKeyboard(LgFalse);
			}
		}
	}
}

void LgDirectorHandleTouchMoved(int num, const int *touchid, const LgPointP pts) {
	LgNodeP currentScene;
	struct LgVector2 vpt;
	
	currentScene = LgDirectorCurrentScene();
	
	if (currentScene) {
		int i;
		LgDictP touches = createTouchesGroup();
		
		for (i = 0; i < num; i++) {
			LgListP tlist = (LgListP)LgDictFind(g_touches, (void *)touchid[i]);
			
			if (tlist) {
				LgListNodeP iter;

				iter = LgListFirstNode(tlist);
				while(iter) {
					LgTouchP touch = (LgTouchP)LgListNodeValue(iter);

					if (touch && LgTouchDownNode(touch)) {
						LgNodeP currentNode;
						LgDirectorScreenToView(pts + i, &vpt);
						currentNode = LgNodeHitTest(currentScene, &vpt, NULL);
						LgDirectorScreenToView(pts + i, &vpt);
						LgTouchReplace(touch, currentNode, &vpt);
						addTouchByNode(touches, LgTouchDownNode(touch), touch);
					}

					iter = LgListNextNode(iter);
				}
			}
		}
		
		// 分发事件
		fireTouchEventAndDeleteTouchesGroup(touches, LgNodeTouchMoved);
	}
}

void LgDirectorHandleTouchEnded(int num, const int *touchid) {
	if (LgDirectorCurrentScene()) {
		LgDictP touches = createTouchesGroup();
		int i;
		
		for (i = 0; i < num; i++) {
			LgListP tlist = (LgListP)LgDictFind(g_touches, (void *)touchid[i]);
			if (tlist) {
				LgListNodeP iter;
				
				iter = LgListFirstNode(tlist);
				while(iter) {
					LgTouchP touch = (LgTouchP)LgListNodeValue(iter);
					if (touch) addTouchByNode(touches, LgTouchDownNode(touch), touch);
					iter = LgListNextNode(iter);
				}
			}
		}

		fireTouchEventAndDeleteTouchesGroup(touches, LgNodeTouchEnded);
		for (i = 0; i < num; i++) LgDictRemove(g_touches, (void *)touchid[i]);
	}
}

void LgDirectorHandleTouchCancelled(int num, const int *touchid) {
	if (LgDirectorCurrentScene()) {
		LgDictP touches = createTouchesGroup();
		int i;
	
		for (i = 0; i < num; i++) {
			LgListP tlist = (LgListP)LgDictFind(g_touches, (void *)touchid[i]);
			
			if (tlist) {
				LgListNodeP iter;
				
				iter = LgListFirstNode(tlist);
				while(iter) {
					LgTouchP touch = (LgTouchP)LgListNodeValue(iter);
					if (touch) addTouchByNode(touches, LgTouchDownNode(touch), touch);
					iter = LgListNextNode(iter);
				}
			}
		}
	
		fireTouchEventAndDeleteTouchesGroup(touches, LgNodeTouchCancelled);
		for (i = 0; i < num; i++) LgDictRemove(g_touches, (void *)touchid[i]);
	}
}

int LgDirectorSceneStackSize() {
	return LgListCount(g_sceneStack);
}

void LgDirectorUpdateBatch(int batch) {
	if (g_statistics) LgStatisticeUpdateBatch(g_statistics, batch);
}

void LgDirectorSetSpeed(float speed) {
	g_speed = speed;
}

float LgDirectorSpeed() {
	return g_speed;
}

void LgDirectorPause() {
	g_paused = LgTrue;
}

void LgDirectorResume() {
	g_paused = LgFalse;
}

LgBool LgDirectorIsPaused() {
	return g_paused;
}

void LgDirectorHandleKeyboardEvent(LgKeyboardEventP evt) {
	if (g_currentInputNode) {
		if (evt->type == LgKeyboardEventInputText) {
			LgTextFieldInsertText(g_currentInputNode, evt->text, evt->textLength);
		} else if (evt->type == LgKeyboardEventCompText) {
			LgTextFieldCompText(g_currentInputNode, evt->text, evt->textLength);
		} else if (evt->type == LgKeyboardEventDeleteChar) {
			LgTextFieldDeleteChar(g_currentInputNode);
		} else if (evt->type == LgKeyboardEventCloseKeyboard) {
			LgTextFieldKillFocus(g_currentInputNode);
			g_currentInputNode = NULL;
			LgWindowShowKeyboard(LgFalse);
		}
	}
}

void LgDirectorHandleLowMemory() {
	LgLogWarning("Low Memory!", NULL);
	LgResourceManagerClear();
	lua_gc(LgScriptEnvState(), LUA_GCCOLLECT, 0);
}

void LgDirectorSetCamera(float centerX, float centerY, float scale) {
	g_cameraCenterX = centerX;
	g_cameraCenterY = centerY;
	g_cameraScale = scale;
}

void LgDirectorCamera(float *centerX, float *centerY, float *scale) {
	*centerX = g_cameraCenterX;
	*centerY = g_cameraCenterY;
	*scale = g_cameraScale;
}