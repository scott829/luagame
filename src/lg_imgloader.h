#ifndef _LG_IMGLOADER_H
#define _LG_IMGLOADER_H

#include "lg_inputstream.h"

typedef struct LgImageLoader {
	const char *name;
	LgBool (* checkType)(const unsigned char *data, int size);
	LgImageP (* load)(LgInputStreamP inputStream);
}*LgImageLoaderP;

#endif