#ifndef _LG_KEYEVENT_H
#define _LG_KEYEVENT_H

#include "lg_keyevent.h"
#include "lg_time.h"

enum LgKey {
	LgKeyUnknown = 0,
	LgKeyUp,
	LgKeyDown,
	LgKeyLeft,
	LgKeyRight,
	LgKeyPageUp,
	LgKeyPageDown,
	LgKeyEnter,
	LgKeyBack,
	LgKeyOptions,

	LgKeyF1 = 10000,
	LgKeyF2,
	LgKeyF3,
	LgKeyF4,
	LgKeyF5,
	LgKeyF6,
	LgKeyF7,
	LgKeyF8,
	LgKeyF9,
	LgKeyF10,
	LgKeyF11,
	LgKeyF12,
};

typedef struct LgKeyEvent *LgKeyEventP;

LgKeyEventP LgKeyEventCreate();

void LgKeyEventDestroy(LgKeyEventP event);

void LgKeyEventDown(LgKeyEventP event, enum LgKey key);

void LgKeyEventUp(LgKeyEventP event, enum LgKey key);

enum LgKey LgKeyEventKey(LgKeyEventP event);

LgTime LgKeyEventDownTime(LgKeyEventP event);

#endif