﻿#ifndef _LG_SAMPLEDRIVER_H
#define _LG_SAMPLEDRIVER_H

#include "lg_refcnt.h"
#include "lg_inputstream.h"

enum LgSampleRate {
	LgSampleRate_8000 = 8000,
	LgSampleRate_11025 = 11025,
	LgSampleRate_22050 = 22050,
	LgSampleRate_44100 = 44100,
};

typedef struct LgSoundFormat
{
	int rate;			// 比特率
	int sampleSize;		// 声音样本大小（字节）
	int numberChannel;	// 声道数
}*LgSoundFormatP;

#define LgSoundFormatFrameSize(format) ((format)->sampleSize * (format)->numberChannel)

#define LgSoundFormatBytesPerSecond(format) (LgSoundFormatFrameSize(format) * (format)->rate)

typedef struct LgSampleDriver *LgSampleDriverP;

typedef struct LgSampleProvider {
	LG_REFCNT_HEAD
	LgSampleDriverP driver;
}*LgSampleProviderP;

LG_REFCNT_FUNCTIONS_DECL(LgSampleProvider)

struct LgSampleDriver {
	const char *name;
	LgSampleProviderP (* createSampleProviderR)(LgInputStreamP input);
	LgBool (* checkFormat)(LgInputStreamP input);
	void (* getFormat)(LgSampleProviderP sp, LgSoundFormatP format);
	int (* readSoundData)(LgSampleProviderP sp, void *data, int frameCount);
};

LgSampleProviderP LgSampleProviderCreateR(const char *filename);

#define LgSampleProviderDestroy(sp) sp->driver->destroySampleProvider(sp)

#define LgSampleProviderFormat(sp, format) sp->driver->getFormat(sp, format)

#define LgSampleProviderReadSoundData(sp, data, frameCount) sp->driver->readSoundData(sp, data, frameCount)

#endif