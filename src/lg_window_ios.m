#include "lg_window_ios.h"
#include "lg_director.h"
#include "lg_dict.h"

#define MAX_TOUCH 10

static struct LgPoint g_points[MAX_TOUCH];
static int g_touchId[MAX_TOUCH];
static UITouch *g_touches[MAX_TOUCH];

static int allocTouchId(UITouch *touch) {
	int i;
	
	for (i = 0; i < MAX_TOUCH; i++) {
		if (!g_touches[i]) {
			g_touches[i] = touch;
			return i + 1;
		}
	}
	
	return 0;
}

static void freeTouchId(UITouch *touch) {
	int i;
	for (i = 0; i < MAX_TOUCH; i++) {
		if (g_touches[i] == touch) {
			g_touches[i] = NULL;
			break;
		}
	}
}

static int getTouchId(UITouch *touch) {
	int i;
	
	for (i = 0; i < MAX_TOUCH; i++) {
		if (g_touches[i] == touch) {
			return i + 1;
		}
	}
	
	return 0;
}

#pragma mark - RootViewController

@interface RootViewController : UIViewController {
	
}

- (BOOL)prefersStatusBarHidden;

@end

@implementation RootViewController

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (NSUInteger) supportedInterfaceOrientations{
#ifdef __IPHONE_6_0
    return UIInterfaceOrientationMaskAllButUpsideDown;
#endif
}

- (BOOL) shouldAutorotate {
    return YES;
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}


- (void)dealloc {
    [super dealloc];
}

@end

#pragma mark LgWindowIos

@interface LgWindowIos : UIWindow<UITextInput> {
@private
	BOOL		showKeyboard;
	NSString *	markString;
}

- (void)showKeyboard;

- (void)hideKeyboard;

@end

@implementation LgWindowIos

- (id)initWithFrame:(CGRect)frame {
	showKeyboard = false;
	markString = NULL;
	return [super initWithFrame:frame];
}

- (void)dealloc {
	if (markString) {
		[markString release];
		markString = NULL;
	}
	[super dealloc];
}

- (void)showKeyboard {
	showKeyboard = YES;
	[self becomeFirstResponder];
}

- (void)hideKeyboard {
	[self resignFirstResponder];
}

- (BOOL)canBecomeFirstResponder {
	if (markString != nil) {
		[markString release];
		markString = nil;
	}
	return showKeyboard;
}

- (BOOL)resignFirstResponder {
	showKeyboard = NO;
	return [super resignFirstResponder];
}

#pragma mark UITextInput

- (UITextAutocapitalizationType) autocapitalizationType {
    return UITextAutocapitalizationTypeNone;
}

- (UIKeyboardType)keyboardType {
	// 在这里改变弹出的键盘类型
	return UIKeyboardTypeDefault;
}

@synthesize beginningOfDocument;
@synthesize endOfDocument;
@synthesize inputDelegate;
@synthesize markedTextRange;
@synthesize markedTextStyle;
@synthesize tokenizer;

- (void)setSelectedTextRange:(UITextRange *)aSelectedTextRange {
}

- (UITextRange *)selectedTextRange; {
    return [[[UITextRange alloc] init] autorelease];
}

- (UITextPosition *)closestPositionToPoint:(CGPoint)point {
	return nil;
}

- (UITextPosition *)closestPositionToPoint:(CGPoint)point withinRange:(UITextRange *)range {
	return nil;
}

- (void)unmarkText {
	struct LgKeyboardEvent evt;
	if (markString == nil) return;
	evt.type = LgKeyboardEventInputText;
	evt.text = (const wchar_t *)[markString cStringUsingEncoding:NSUTF32StringEncoding];
	evt.textLength = [markString length];
	LgDirectorHandleKeyboardEvent(&evt);
	[markString release];
	markString = nil;
}

- (UITextRange *)textRangeFromPosition:(UITextPosition *)fromPosition toPosition:(UITextPosition *)toPosition {
	return nil;
}

- (NSComparisonResult)comparePosition:(UITextPosition *)position toPosition:(UITextPosition *)other {
	return (NSComparisonResult)0;
}

- (UITextWritingDirection)baseWritingDirectionForPosition:(UITextPosition *)position inDirection:(UITextStorageDirection)direction {
	return UITextWritingDirectionNatural;
}

- (void)deleteBackward {
	// 删除最后一个字符
	struct LgKeyboardEvent evt;
	
	if (markString != nil) {
		[markString release];
		markString = nil;
	}

	evt.type = LgKeyboardEventDeleteChar;
	LgDirectorHandleKeyboardEvent(&evt);
}

- (CGRect)firstRectForRange:(UITextRange *)range {
	return CGRectNull;
}

- (UITextPosition *)positionFromPosition:(UITextPosition *)position inDirection:(UITextLayoutDirection)direction offset:(NSInteger)offset {
	return nil;
}

- (UITextPosition *)positionFromPosition:(UITextPosition *)position offset:(NSInteger)offset {
	return nil;
}

- (void)setBaseWritingDirection:(UITextWritingDirection)writingDirection forRange:(UITextRange *)range {
}

- (UITextRange *)characterRangeByExtendingPosition:(UITextPosition *)position inDirection:(UITextLayoutDirection)direction {
	return nil;
}

- (UITextRange *)characterRangeAtPoint:(CGPoint)point {
	return nil;
}

- (void)replaceRange:(UITextRange *)range withText:(NSString *)text {
}

- (BOOL)hasText {
	return NO;
}

- (void)insertText:(NSString *)text {
	// 插入文本
	if (markString != nil) {
		[markString release];
		markString = nil;
	}
	if ([text characterAtIndex:0] == '\n') {
		// 回车，关闭键盘
		struct LgKeyboardEvent evt;
		evt.type = LgKeyboardEventCloseKeyboard;
		LgDirectorHandleKeyboardEvent(&evt);
	} else {
		struct LgKeyboardEvent evt;
		evt.type = LgKeyboardEventInputText;
		evt.text = (const wchar_t *)[text cStringUsingEncoding:NSUTF32StringEncoding];
		evt.textLength = [text length];
		LgDirectorHandleKeyboardEvent(&evt);
	}
}

- (NSString *)textInRange:(UITextRange *)range {
	return @"";
}

- (NSInteger)offsetFromPosition:(UITextPosition *)from toPosition:(UITextPosition *)toPosition {
	return 0;
}

- (CGRect)caretRectForPosition:(UITextPosition *)position {
	return CGRectZero; // 这里不能返回CGRectNull，否则无法输入中文
}

- (NSArray *)selectionRectsForRange:(UITextRange *)range {
	return nil;
}

- (UITextPosition *)positionWithinRange:(UITextRange *)range atCharacterOffset:(NSInteger)offset {
	return nil;
}

- (UITextPosition *)positionWithinRange:(UITextRange *)range farthestInDirection:(UITextLayoutDirection)direction {
	return nil;
}

-(void)setMarkedText:(NSString *)markedText selectedRange:(NSRange)selectedRange {
	struct LgKeyboardEvent evt;
	int i, size;
	wchar_t *str;
	
    if (markedText == markString) return;
    if (markString != nil) [markString release];
	markString = markedText;
	[markString retain];
	
	if (markString == nil) return;
	evt.type = LgKeyboardEventCompText;
	size = [markString lengthOfBytesUsingEncoding:NSUTF32StringEncoding];
	str = (wchar_t *)LgMalloc(size);
	LgCopyMemory(str, [markString cStringUsingEncoding:NSUTF32StringEncoding], size);
	evt.text = str;
	evt.textLength = [markString length];
	
	for (i = 0; i < evt.textLength; i++) {
		if (str[i] == 8198) str[i] = 32;
	}
	
	LgDirectorHandleKeyboardEvent(&evt);
	LgFree(str);
}

@end

#pragma mark LgViewIos

@interface LgViewIos : UIView {
	EAGLContext *context;
	GLuint defaultFramebuffer, colorRenderbuffer, depthRenderbuffer;
	int backingWidth, backingHeight;
}

- (void)initOpengl;

- (void)beginDraw;

- (void)endDraw;

@end

@implementation LgViewIos

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	int i = 0;

	for (UITouch *touch in touches) {
		CGPoint pt = [touch locationInView:self];
		g_points[i].x = (int)pt.x;
		g_points[i].y = (int)pt.y;
		g_touchId[i] = allocTouchId(touch);
		i++;
	}
	LgDirectorHandleTouchBegan([touches count], g_touchId, g_points);
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	int i = 0;

	for (UITouch *touch in touches) {
		CGPoint pt = [touch locationInView:self];
		g_points[i].x = (int)pt.x;
		g_points[i].y = (int)pt.y;
		g_touchId[i] = getTouchId(touch);
		i++;
	}
	LgDirectorHandleTouchMoved([touches count], g_touchId, g_points);
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	int i = 0;

	for (UITouch *touch in touches) {
		g_touchId[i] = getTouchId(touch);
		freeTouchId(touch);
		i++;
	}
	LgDirectorHandleTouchEnded([touches count], g_touchId);
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
	int i = 0;

	for (UITouch *touch in touches) {
		g_touchId[i] = getTouchId(touch);
		freeTouchId(touch);
		i++;
	}
	LgDirectorHandleTouchCancelled([touches count], g_touchId);
}

+ (Class) layerClass {
    return [CAEAGLLayer class];
}

- (void)initOpengl {
	CAEAGLLayer *eaglLayer = (CAEAGLLayer *)self.layer;
	
	eaglLayer.opaque = TRUE;
	eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:
									[NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
	
	context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
	[EAGLContext setCurrentContext:context];
	
	glGenFramebuffers(1, &defaultFramebuffer);
	glGenRenderbuffers(1, &colorRenderbuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
	
	depthRenderbuffer = 0;
}

- (void) layoutSubviews {
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [context renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
	glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        NSLog(@"Failed to make complete framebuffer object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
	
	if (!depthRenderbuffer) {
		glGenRenderbuffers(1, &depthRenderbuffer);
		NSAssert(depthRenderbuffer, @"Can't create depth buffer");
	}
	
	glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8_OES, backingWidth, backingHeight);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
	
	// bind color buffer
	glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
}

- (void) beginDraw {
	[EAGLContext setCurrentContext:context];
	glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
}

- (void) endDraw {
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [context presentRenderbuffer:GL_RENDERBUFFER];
}

- (void) dealloc {
	if (defaultFramebuffer) {
		glDeleteFramebuffers(1, &defaultFramebuffer);
		defaultFramebuffer = 0;
	}
	
	if (colorRenderbuffer) {
		glDeleteRenderbuffers(1, &colorRenderbuffer);
		colorRenderbuffer = 0;
	}

	if (depthRenderbuffer) {
		glDeleteRenderbuffers(1, &depthRenderbuffer);
		depthRenderbuffer = 0;
	}

	[super dealloc];
}

@end

#pragma mark LgWindow

static LgViewIos *g_view = nil;
static UIWindow *g_window = nil;
static UIViewController *g_viewController;

LgBool LgWindowOpen(const LgSizeP size) {
	UIScreen *screen = [UIScreen mainScreen];
	CGRect rt = [screen bounds];
	
	LgZeroMemory(g_touches, sizeof(g_touches));
	
	// 创建window
	g_window = [[LgWindowIos alloc] initWithFrame:rt];
	
	// 创建view
	g_view = [LgViewIos alloc];
	[g_view initWithFrame: [g_window bounds]];
	[g_view initOpengl];
	[g_view setMultipleTouchEnabled:LgTrue];
	
	// 创建根视图控制器
	g_viewController = [[RootViewController alloc] initWithNibName:nil bundle:nil];
	//    viewController.wantsFullScreenLayout = YES;
    g_viewController.view = g_view;
	
    if ([[UIDevice currentDevice].systemVersion floatValue] < 6.0) {
		[g_window addSubview: g_viewController.view];
    }
    else {
        [g_window setRootViewController:g_viewController];
    }
	
	[[UIApplication sharedApplication] setStatusBarHidden: YES];
	
	[g_window makeKeyAndVisible];
	return LgTrue;
}

void LgWindowClose() {
	if (g_view) {
		[g_view release];
		g_view = nil;
	}
	if (g_window) {
		[g_window release];
		g_window = nil;
	}
	if (g_viewController) {
		[g_viewController release];
		g_viewController = nil;
	}
}

void LgWindowSize(LgSizeP size) {
	CGRect rt = [g_view bounds];
	size->width = (int)rt.size.width;
	size->height = (int)rt.size.height;
}

UIWindow *LgWindowUIWindow() {
	return g_window;
}

void LgWindowBeginDraw() {
	[g_view beginDraw];
}

void LgWindowEndDraw() {
	[g_view endDraw];
}

void LgWindowShowKeyboard(LgBool show) {
	if (show) [(LgWindowIos *)g_window showKeyboard];
	else [(LgWindowIos *)g_window hideKeyboard];
}

LgBool LgWindowIsHD() {
	UIScreen *screen = [UIScreen mainScreen];
	float scale = [screen scale];
	return scale > 1.0f;
}