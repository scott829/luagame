#include "lg_rendersystem.h"
#include "lg_director.h"
#include "lg_platform.h"
#include "lg_log.h"
#include "lg_init.h"
#include "jansson/jansson.h"
#include "lg_imagecopy.h"

LG_REFCNT_FUNCTIONS(LgTexture)

#ifdef HAVE_RS_D3D9
extern struct LgRenderSystemDriver d3d9Driver;
#endif

#ifdef HAVE_RS_OPENGL
extern struct LgRenderSystemDriver openglDriver;
#endif

#ifdef HAVE_RS_OPENGLES
extern struct LgRenderSystemDriver openglesDriver;
#endif

LgRenderSystemDriverP g_renderDrivers[] = {
#ifdef HAVE_RS_D3D9
	&d3d9Driver,
#endif
#ifdef HAVE_RS_OPENGL
	&openglDriver,
#endif
#ifdef HAVE_RS_OPENGLES
	&openglesDriver,
#endif
	NULL,
};

LgRenderSystemDriverP g_currentRenderSystemDriver = NULL;

void LgRenderSystemSetDriver(const char *name) {
	if (name) {
		int i = 0;
		for (i = 0; g_renderDrivers[i]; i++) {
			if (strnocasecmp(g_renderDrivers[i]->name, name) == 0) {
				g_currentRenderSystemDriver = g_renderDrivers[i];
				break;
			}
		}
	}

	if (!g_currentRenderSystemDriver) g_currentRenderSystemDriver = g_renderDrivers[0];
	LgLogInfo("current render driver : '%s'", g_currentRenderSystemDriver->name);
}

LgBool LsRenderSystemIsVSync() {
	json_t *graphics, *vsync;
	if ((graphics = json_object_get(g_globalConfig, "graphics"))) {
		if ((vsync = json_object_get(graphics, "vsync")) && json_is_true(vsync)) {
			return LgTrue;
		}
	}
	return LgFalse;
}

LgTextureP LgTextureCreateFromFileR(const char *filename) {
	LgImageP img = NULL;
	LgTextureP texture = NULL;

	img = LgImageCreateFromFileR(filename);
	LG_CHECK_ERROR(img);
	LgRenderSystemBegin(NULL);
	texture = LgImageCreateTextureR(img);
	LgRenderSystemEnd();
	LG_CHECK_ERROR(texture);
	LgImageRelease(img);

	return texture;

_error:
	if (img) LgImageRelease(img);
	return NULL;
}