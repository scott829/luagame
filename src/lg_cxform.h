#ifndef _LG_CXFORM_H
#define _LG_CXFORM_H

#include "lg_basetypes.h"
#include "lg_memory.h"

#define LGCXFORM_RED 0
#define LGCXFORM_GREEN 1
#define LGCXFORM_BLUE 2
#define LGCXFORM_ALPHA 3

typedef struct LgCxform {
	// ˳��: r, g, b, a
	float mul[4], add[4];
}*LgCxformP;

void LgCxformIndentity(LgCxformP cxform);

void LgCxformMul(LgCxformP lhs, const LgCxformP rhs);

void LgCxformInitColor(LgCxformP cxform, LgColor color);

#endif