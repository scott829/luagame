﻿#include "lg_imagecopy.h"

void LgImageCopyData(void *destData, int destPitch, enum LgImageFormat_ destFormat, const LgRectP destRect, 
					 const void *srcData, int srcPitch, enum LgImageFormat_ srcFormat, const LgRectP srcRect, LgBlit blit) {
	int destX, destY, destW, destH,
	srcX, srcY, srcW, srcH;
	int srcPixelBytes = LgImagePixelSize(srcFormat), 
		destPixelBytes = LgImagePixelSize(destFormat);
	const unsigned char *psrcline;
	unsigned char *pdstline;
	int row;

	destX = destRect->x, destY = destRect->y, destW = destRect->width, destH = destRect->height;
	srcX = srcRect->x, srcY = srcRect->y, srcW = srcRect->width, srcH = srcRect->height;

	// 调整位置
	if (destX < 0) {
		srcX = -destX;
		destX = 0;
	}

	if (destY < 0) {
		srcY = -destY;
		destY = 0;
	}

	if (destX + srcW > destW) srcW -= ((destX + srcW) - destW);
	if (destY + srcH > destH) srcH -= ((destY + srcH) - destH);

	psrcline = (const unsigned char *)srcData + srcY * srcPitch + srcX * srcPixelBytes;
	pdstline = (unsigned char *)destData + destY * destPitch + destX * destPixelBytes;

	for (row = srcY; row < srcH; row++) {
		unsigned char *pdst = pdstline;
		const unsigned char *psrc = psrcline;
		int col;

		for (col = srcX; col < srcW; col++) {
			blit(pdst, psrc);
			pdst += destPixelBytes, psrc += srcPixelBytes;
		}

		pdstline += destPitch, psrcline += srcPitch;
	}
}

void LgImageCopy(LgImageP dest, const LgRectP destRect, LgImageP src, const LgRectP srcRect, LgBlit blit) {
	LgImageCopyData(LgImageData(dest), LgImageLineSize(dest), LgImageFormat(dest), destRect,
		LgImageData(src), LgImageLineSize(src), LgImageFormat(src), srcRect, blit);
}

LgTextureP LgImageCreateTextureR(LgImageP image) {
	enum LgImageFormat_ format;
	struct LgSize size;
	LgTextureP texture;
	
	format = LgImageFormat(image);
	LgImageSize(image, &size);
	texture = LgRenderSystemCreateTextureR(format, &size);
	if (!texture) return NULL;
	LgTextureUpdate(texture, 0, 0, size.width, size.height, image);
	return texture;
}
