﻿#include "lg_scrollview.h"
#include "lg_clipnode.h"
#include "lg_drawnode.h"
#include "lg_action.h"

static void updateClip(LgScrollViewDataP sd) {
	LgDrawNodeClear(sd->clipDrawNode);
	LgDrawNodeRectangle(sd->clipDrawNode, 0, 0, sd->base.size.x, sd->base.size.y);
}

static void fireScrollEventCall(void *param, void *userdata) {
	LgScrollViewDataP sd = (LgScrollViewDataP)userdata;
	LgSlotCall(&sd->slotScroll, NULL);
}

static void normalizeOffset(LgScrollViewDataP sd, LgBool animation, LgBool fireScrollEvent) {
	struct LgVector2 newOffset, currentOffset;
	struct LgAabb contentViewBox;

	LgNodePosition(sd->contentView, &currentOffset);
	newOffset = currentOffset;
	LgNodeContentBox(sd->contentView, &contentViewBox);

	if (contentViewBox.width + newOffset.x < sd->base.size.x)
		newOffset.x = -(contentViewBox.width - sd->base.size.x);

	if (contentViewBox.height + newOffset.y < sd->base.size.y)
		newOffset.y = -(contentViewBox.height - sd->base.size.y);
 
	if (newOffset.x > 0) newOffset.x = 0;
	if (newOffset.y > 0) newOffset.y = 0;

	if (newOffset.x != currentOffset.x || newOffset.y != currentOffset.y) {

		if (animation) {
			LgActionP action, ease;

			action = LgActionMoveToCreate(0.1, &newOffset);
			ease = LgActionEaseInCreate(action, 0.3f);

			if (fireScrollEvent) {
				LgActionP sequence;
				LgActionP call;
				LgActionP children[2];
				
				call = LgActionCallCreate(fireScrollEventCall, NULL, NULL, sd);
				children[0] = ease;
				children[1] = call;
				sequence = LgActionSequenceCreate(children, 2);
				LgNodeDoAction(sd->contentView, sequence);
				LgActionRelease(sequence);
				LgActionRelease(call);
			} else {
				LgNodeDoAction(sd->contentView, ease);
			}

			LgActionRelease(action);
			LgActionRelease(ease);
		} else {
			LgNodeSetPosition(sd->contentView, &newOffset);
			if (fireScrollEvent) LgSlotCall(&sd->slotScroll, NULL);
		}
	} else {
		if (fireScrollEvent) LgSlotCall(&sd->slotScroll, NULL);
	}
}

static void scrollviewOnTouch(LgNodeP scrollview, LgNodeTouchEventP evt) {
	LgScrollViewDataP sd = (LgScrollViewDataP)LgNodeUserData(scrollview);

	if (evt->type == LgNodeTouchBegan) {
		LgNodePosition(sd->contentView, &sd->downOffset);
		LgTouchWorldPosition(evt->touches[0], &sd->downWorldPosition);
	} else if (evt->type == LgNodeTouchMoved) {
		struct LgVector2 currentWorldPosition, newOffset;
		float ox = 0, oy = 0;

		LgTouchWorldPosition(evt->touches[0], &currentWorldPosition);
		if (!sd->lockHorizontal) ox = currentWorldPosition.x - sd->downWorldPosition.x;
		if (!sd->lockVertical) oy = currentWorldPosition.y - sd->downWorldPosition.y;

		newOffset.x = sd->downOffset.x + ox;
		newOffset.y = sd->downOffset.y + oy;

		LgNodeSetPosition(sd->contentView, &newOffset);
	} else if (evt->type == LgNodeTouchEnded || evt->type == LgNodeTouchCancelled) {
		// 恢复正常位置
		normalizeOffset(sd, LgTrue, LgTrue);
	}
}

static void scrollviewEventProc(LgEventType type, LgNodeP node, void *param) {
	LgScrollViewDataP sd = (LgScrollViewDataP)LgNodeUserData(node);

	switch(type) {
	case LG_CONTROLNODE_EVENT_SetSize:
		LgControlEventProc(type, node, param);
		updateClip(sd);
		normalizeOffset(sd, LgFalse, LgFalse);
		return;
	case LG_NODE_EVENT_HandleTouch:
		scrollviewOnTouch(node, (LgNodeTouchEventP)param);
		break;
	}

	LgControlEventProc(type, node, param);
}

LgNodeP LgScrollViewCreateR(const LgVector2P size) {
	LgNodeP node = LgControlNodeCreateSubClassR(sizeof(struct LgScrollViewData), (LgEvent)scrollviewEventProc, size);
	LgScrollViewDataP sd = (LgScrollViewDataP)LgNodeUserData(node);

	sd->clipNode = LgClipNodeCreateR();
	LgNodeSetTouchBubble(sd->clipNode, LgTrue);
	LgNodeAddChild(node, sd->clipNode);
	LgNodeSetZorder(sd->clipNode, LG_UILAYER_CHILD);
	LgNodeRelease(sd->clipNode);

	sd->clipDrawNode = LgDrawNodeCreateR();
	LgClipNodeSetStencil(sd->clipNode, sd->clipDrawNode);
	LgNodeRelease(sd->clipDrawNode);
	updateClip(sd);

	sd->contentView = LgControlNodeCreateR(&LgVector2Zero);
	LgNodeSetTouchBubble(sd->contentView, LgTrue);
	LgNodeAddChild(sd->clipNode, sd->contentView);
	LgNodeRelease(sd->contentView);

	LgNodeEnableHitTest(node, LgTrue);
	return node;
}

LgBool LgNodeIsScrollView(LgNodeP node) {
	return LgNodeEventProc(node) == (LgEvent)scrollviewEventProc;
}

LgNodeP LgScrollViewContentView(LgNodeP node) {
	LgScrollViewDataP sd = (LgScrollViewDataP)LgNodeUserData(node);
	return sd->contentView;
}

void LgScrollViewSetContentSize(LgNodeP node, const LgVector2P size) {
	LgScrollViewDataP sd = (LgScrollViewDataP)LgNodeUserData(node);
	LgControlNodeSetSize(sd->contentView, size);
}

void LgScrollViewSetLock(LgNodeP node, LgBool lockHorizontal, LgBool lockVertical) {
	LgScrollViewDataP sd = (LgScrollViewDataP)LgNodeUserData(node);
	sd->lockHorizontal = lockHorizontal;
	sd->lockVertical = lockVertical;
}

void LgScrollViewGetViewRect(LgNodeP node, LgAabbP view) {
	LgScrollViewDataP sd = (LgScrollViewDataP)LgNodeUserData(node);
	struct LgVector2 position;
	struct LgAabb contentBox;

	LgNodePosition(sd->contentView, &position);
	view->x = -position.x;
	view->y = -position.y;
	view->width = sd->base.size.x;
	view->height = sd->base.size.y;
	
	LgNodeContentBox(sd->contentView, &contentBox);

	if (view->x + view->width > contentBox.width) {
		view->x = sd->base.size.x - (view->x + view->width - contentBox.width);
	}

	if (view->y + view->height > contentBox.height) {
		view->y = sd->base.size.y - (view->y + view->height - contentBox.height);
	}
}

void LgScrollViewSetScroll(LgNodeP node, const LgVector2P position) {
	LgScrollViewDataP sd = (LgScrollViewDataP)LgNodeUserData(node);
	struct LgVector2 _pos = *position;
	_pos.x = -_pos.x;
	_pos.y = -_pos.y;
	LgNodeSetPosition(sd->contentView, &_pos);
	normalizeOffset(sd, LgFalse, LgFalse);
}

LgSlot *LgScrollViewSlotScroll(LgNodeP node) {
	LgScrollViewDataP sd = (LgScrollViewDataP)LgNodeUserData(node);
	return &sd->slotScroll;
}
