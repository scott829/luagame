#include "lg_sample_mp3.h"
#include "libmad/mad.h"
#include "libid3tag/id3tag.h"
#include "lg_memory.h"

#define INPUT_BUFFER_SIZE	(1024 * 10)

struct LgSampleProviderMP3 {
	struct 	LgSampleProvider base;
	LgInputStreamP input;
	struct LgSoundFormat format;
	struct mad_stream stream;
	struct mad_frame frame;
	struct mad_synth synth;
	unsigned char inputBuffer[INPUT_BUFFER_SIZE + MAD_BUFFER_GUARD];
	int currentPcm;
};

static LgBool decodeFrame(struct LgSampleProviderMP3 *spMp3);

static int scale(mad_fixed_t sample) {
	sample += (1L << (MAD_F_FRACBITS - 16));

	if (sample >= MAD_F_ONE)
		sample = MAD_F_ONE - 1;
	else if (sample < -MAD_F_ONE)
		sample = -MAD_F_ONE;

	return sample >> (MAD_F_FRACBITS + 1 - 16);
}

static void destroySampleProvider(LgSampleProviderP sp) {
	struct LgSampleProviderMP3 *spMp3 = (struct LgSampleProviderMP3 *)sp;
	if (spMp3->input) LgInputStreamDestroy(spMp3->input);
	mad_synth_finish(&spMp3->synth);
	mad_frame_finish(&spMp3->frame);
	mad_stream_finish(&spMp3->stream);
	LgFree(sp);
}

static LgSampleProviderP createSampleProviderR(LgInputStreamP input) {
	struct LgSampleProviderMP3 * sp = (struct LgSampleProviderMP3 *)LgMallocZ(sizeof(struct LgSampleProviderMP3));
	LG_CHECK_ERROR(sp);
	LG_REFCNT_INIT(sp, destroySampleProvider);
	sp->base.driver = &mp3SampleDriver;
	sp->input = input;
	mad_stream_init(&sp->stream);
	mad_frame_init(&sp->frame);
	mad_synth_init(&sp->synth);
	LG_CHECK_ERROR(decodeFrame(sp));
	sp->format.numberChannel = sp->synth.pcm.channels;
	sp->format.rate = sp->synth.pcm.samplerate;
	sp->format.sampleSize = 2;
	return (LgSampleProviderP)sp;

_error:
	if (sp) LgSampleProviderRelease((LgSampleProviderP)sp);
	return NULL;
}

static LgBool checkFormat(LgInputStreamP input) {
	struct mad_stream stream;
	struct mad_frame frame;
	unsigned char inputBuffer[INPUT_BUFFER_SIZE + MAD_BUFFER_GUARD];
	LgBool isMp3 = LgFalse;
	int rsz;

	mad_stream_init(&stream);
	mad_frame_init(&frame);

	rsz = LgInputStreamReadBytes(input, inputBuffer, sizeof(inputBuffer));
	mad_stream_buffer(&stream, inputBuffer, rsz);

	for(;;) {
		if (stream.buffer == NULL || stream.error == MAD_ERROR_BUFLEN) {
			// 补数据
			int readSize, remaining;
			unsigned char *readStart;

			if (stream.next_frame != NULL) {

				remaining = stream.bufend - stream.next_frame;
				memmove(inputBuffer, stream.next_frame, remaining);
				readStart = inputBuffer + remaining;
				readSize = INPUT_BUFFER_SIZE - remaining;
			} else {
				readSize = INPUT_BUFFER_SIZE;
				readStart = inputBuffer;
				remaining = 0;
			}

			readSize = LgInputStreamReadBytes(input, readStart, readSize);

			if (readSize <= 0) {
				// 没有数据了
				return LgFalse;
			}

			mad_stream_buffer(&stream, inputBuffer, readSize + remaining);
			stream.error = MAD_ERROR_NONE;
		}

		if (mad_frame_decode(&frame, &stream)) {
			if (stream.error == MAD_ERROR_BUFLEN) {
				// 需要补数据
				continue;
			} else if (stream.error == MAD_ERROR_LOSTSYNC) {
				long tagsize = id3_tag_query(stream.this_frame, stream.bufend - stream.this_frame);

				if (tagsize > INPUT_BUFFER_SIZE) {
					int skipsize = (int)tagsize - INPUT_BUFFER_SIZE;
					LgInputStreamSkip(input, skipsize);
					mad_stream_buffer(&stream, NULL, 0);
				}
				else {
					mad_stream_skip(&stream, tagsize);
				}
			} else {
				// 出错
				break;
			}
		}
		else {
			isMp3 = LgTrue;
			break;
		}
	}

	mad_frame_finish(&frame);
	mad_stream_finish(&stream);
	return isMp3;
}

static void getFormat(LgSampleProviderP sp, LgSoundFormatP format) {
	struct LgSampleProviderMP3 *spMp3 = (struct LgSampleProviderMP3 *)sp;
	LgCopyMemory(format, &spMp3->format, sizeof(spMp3->format));
}

static LgBool decodeFrame(struct LgSampleProviderMP3 *spMp3) {
	for(;;) {
		if (spMp3->stream.buffer == NULL || spMp3->stream.error == MAD_ERROR_BUFLEN) {
			// 补数据
			int readSize, remaining;
			unsigned char *readStart;

			if (spMp3->stream.next_frame != NULL) {

				remaining = spMp3->stream.bufend - spMp3->stream.next_frame;
				memmove(spMp3->inputBuffer, spMp3->stream.next_frame, remaining);
				readStart = spMp3->inputBuffer + remaining;
				readSize = INPUT_BUFFER_SIZE - remaining;
			} else {
				readSize = INPUT_BUFFER_SIZE;
				readStart = spMp3->inputBuffer;
				remaining = 0;
			}

			readSize = LgInputStreamReadBytes(spMp3->input, readStart, readSize);

			if (readSize <= 0) {
				// 没有数据了
				return LgFalse;
			}

			mad_stream_buffer(&spMp3->stream, spMp3->inputBuffer, readSize + remaining);
			spMp3->stream.error = MAD_ERROR_NONE;
		}

		if (mad_frame_decode(&spMp3->frame, &spMp3->stream)) {
			if (spMp3->stream.error == MAD_ERROR_BUFLEN) {
				// 需要补数据
				continue;
			} else if (spMp3->stream.error == MAD_ERROR_LOSTSYNC) {
				long tagsize = id3_tag_query(spMp3->stream.this_frame, spMp3->stream.bufend - spMp3->stream.this_frame);

				if (tagsize > INPUT_BUFFER_SIZE) {
					int skipsize = (int)tagsize - INPUT_BUFFER_SIZE;
					LgInputStreamSkip(spMp3->input, skipsize);
					mad_stream_buffer(&spMp3->stream, NULL, 0);
				} else {
					mad_stream_skip(&spMp3->stream, tagsize);
				}
			} else {
				// 出错
				return LgFalse;
			}
		} else {
			mad_synth_frame(&spMp3->synth, &spMp3->frame);
			break;
		}
	}

	spMp3->currentPcm = 0;
	return LgTrue;
}

static int readSoundData(LgSampleProviderP sp, void *data, int frameCount) {
	struct LgSampleProviderMP3 *spMp3 = (struct LgSampleProviderMP3 *)sp;
	int frame = 0;
	short *p = (short *)data;

	while(frame < frameCount) {
		if (spMp3->currentPcm < spMp3->synth.pcm.length) {
			// 还有未读取的已解压数据
			while(frame < frameCount && spMp3->currentPcm < spMp3->synth.pcm.length) {
				int value;

				value = scale(spMp3->synth.pcm.samples[0][spMp3->currentPcm]);
				*p++ = value / 2;

				if (spMp3->synth.pcm.channels > 1) {
					value = scale(spMp3->synth.pcm.samples[1][spMp3->currentPcm]);
					*p++ = value / 2;
				}

				frame++, spMp3->currentPcm++;
			}
		} else {
			// 需要解压
			if (!decodeFrame(spMp3)) {
				// 解压失败
				return frame;
			}
		}
	}

	return frame;
}

struct LgSampleDriver mp3SampleDriver = {
	"mp3",
	createSampleProviderR,
	checkFormat,
	getFormat,
	readSoundData,
};