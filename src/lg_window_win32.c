#include "lg_window_win32.h"
#include "lg_basetypes.h"
#include "lg_memory.h"
#include "lg_director.h"
#include "lg_keyevent.h"
#include "lg_platform.h"
#include "lg_log.h"

static const char *LgWindowClassName = "LgWindow";

static const char *LgWindowOldWndProcPropName = "LgWindowOldWndProc";

static int g_touchId = 1;
static HWND g_hwnd = NULL;
static LgBool g_destroy = TRUE, g_mouseDown = FALSE;

static enum LgKey keyMapping(UINT key) {
	switch(key) {
	case VK_UP: return LgKeyUp;
	case VK_DOWN: return LgKeyDown;
	case VK_LEFT: return LgKeyLeft;
	case VK_RIGHT: return LgKeyRight;
	case VK_NEXT: return LgKeyPageDown;
	case VK_PRIOR: return LgKeyPageUp;
	case VK_RETURN: return LgKeyEnter;
	case VK_BACK: return LgKeyBack;
	case VK_TAB: return LgKeyOptions;
	case VK_F1: return LgKeyF1;
	case VK_F2: return LgKeyF2;
	case VK_F3: return LgKeyF3;
	case VK_F4: return LgKeyF4;
	case VK_F5: return LgKeyF5;
	case VK_F6: return LgKeyF6;
	case VK_F7: return LgKeyF7;
	case VK_F8: return LgKeyF8;
	case VK_F9: return LgKeyF9;
	case VK_F10: return LgKeyF10;
	case VK_F11: return LgKeyF11;
	case VK_F12: return LgKeyF12;
	default: return LgKeyUnknown;
	}
}

static const wchar_t *getCompositionString(HWND hwnd, DWORD type) {
	static wchar_t temp[512];

	HIMC imc = ImmGetContext(hwnd);
	if (imc) {
		char str[1024];
		DWORD size = ImmGetCompositionString(imc, type, str, sizeof(str));
		int r = MultiByteToWideChar(CP_ACP, 0, str, size, temp, sizeof(temp));
		temp[r] = 0;
		return temp;
	}

	return NULL;
}

static void fireKeyEvent(LgBool isDown, enum LgKey key) {
	if (key != LgKeyUnknown) {
		if (isDown) LgDirectorHandleKeyDown(key);
		else LgDirectorHandleKeyUp(key);
	};
}

static LRESULT LgWindowProc(HWND hwnd, UINT msg, WPARAM wp, LPARAM lp) {
	WNDPROC oldProc = (WNDPROC)GetProp(hwnd, LgWindowOldWndProcPropName);

	if (msg == WM_CLOSE) {
		PostQuitMessage(0);
	} else if (msg == WM_KEYDOWN) {
		if (wp == VK_ESCAPE) PostQuitMessage(0);
		else {
			if ((lp & 0x40000000) == 0) fireKeyEvent(LgTrue, keyMapping(wp));
		}
	} else if (msg == WM_KEYUP) {
		LgDirectorHandleKeyUp(keyMapping(wp));
	} else if (msg == WM_CHAR) {
		if ((lp & 0x40000000) == 0) {
			if (wp >= 32 && wp < 128) {
				// 是Ascii字符
				struct LgKeyboardEvent evt;
				wchar_t c = (wchar_t)wp;
				evt.type = LgKeyboardEventInputText;
				evt.text = &c;
				evt.textLength = 1;
				LgDirectorHandleKeyboardEvent(&evt);
			} else if (wp == VK_BACK) {
				struct LgKeyboardEvent evt;
				evt.type = LgKeyboardEventDeleteChar;
				LgDirectorHandleKeyboardEvent(&evt);
			} else if (wp == VK_RETURN) {
				struct LgKeyboardEvent evt;
				evt.type = LgKeyboardEventCloseKeyboard;
				LgDirectorHandleKeyboardEvent(&evt);
			}
		}
	} else if (msg == WM_LBUTTONDOWN) {
		struct LgPoint pt = {(short)LOWORD(lp), (short)HIWORD(lp)};
		SetCapture(hwnd);
		g_mouseDown = TRUE;
		LgDirectorHandleTouchBegan(1, &g_touchId, &pt);
	} else if (msg == WM_LBUTTONUP) {
		g_mouseDown = FALSE;
		LgDirectorHandleTouchEnded(1, &g_touchId);
		ReleaseCapture();
	} else if (msg == WM_MOUSEMOVE) {
		if (g_mouseDown) {
			struct LgPoint pt = {(short)LOWORD(lp), (short)HIWORD(lp)};
			LgDirectorHandleTouchMoved(1, &g_touchId, &pt);
		}
	} else if (msg == WM_CAPTURECHANGED) {
		if (g_mouseDown) {
			LgDirectorHandleTouchCancelled(1, &g_touchId);
			g_mouseDown = FALSE;
		}
	} else if (msg == WM_IME_COMPOSITION) {
		if (lp & GCS_RESULTSTR) {
			struct LgKeyboardEvent evt;
			evt.type = LgKeyboardEventInputText;
			evt.text = getCompositionString(hwnd, GCS_RESULTSTR);
			evt.textLength = wcslen(getCompositionString(hwnd, GCS_RESULTSTR));
			LgDirectorHandleKeyboardEvent(&evt);
		} else if (lp & GCS_COMPSTR) {
			struct LgKeyboardEvent evt;
			evt.type = LgKeyboardEventCompText;
			evt.text = getCompositionString(hwnd, GCS_RESULTSTR);
			evt.textLength = wcslen(getCompositionString(hwnd, GCS_COMPSTR));
			LgDirectorHandleKeyboardEvent(&evt);
		}
	}

	return CallWindowProc(oldProc, hwnd, msg, wp, lp);
}

LgBool LgWindowAttach(HWND hwnd, LgBool destroy) {
	g_hwnd = hwnd;
	g_destroy = destroy;

	SetProp(hwnd, LgWindowOldWndProcPropName, (HANDLE)GetWindowLong(hwnd, GWL_WNDPROC));
	SetWindowLong(hwnd, GWL_WNDPROC, (LONG)LgWindowProc);
	return LgTrue;
}

static LRESULT CALLBACK dummyWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	return DefWindowProc(hWnd, message, wParam, lParam);
}

static void registerWndClass() {
	WNDCLASS wc;

	ZeroMemory(&wc, sizeof(wc));
	wc.style			= CS_CLASSDC;
	wc.lpfnWndProc		= (WNDPROC)dummyWndProc; // 不知道为什么，这里如果写DefWndProc，输入法会出问题
	wc.cbClsExtra		= 0;
	wc.cbWndExtra		= 0;
	wc.hInstance		= GetModuleHandle(NULL);
	wc.hIcon			= NULL;
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground	= NULL;
	wc.lpszMenuName		= NULL;
	wc.lpszClassName	= LgWindowClassName;

	RegisterClass(&wc);
}

static void setHwndClientSize(HWND hwnd, int cx, int cy) {
	SIZE sz = {cx, cy};
	RECT rt_w_w;
	RECT rt_c;
	RECT rt_w_h;
	int bh, wh, bw, ww;

	GetWindowRect(hwnd, &rt_w_w);
	GetClientRect(hwnd, &rt_c);

	if (rt_c.right - rt_c.left == sz.cx &&
		rt_c.bottom - rt_c.top == sz.cy) {
		return;
	}

	bw = (rt_w_w.right - rt_w_w.left) - (rt_c.right - rt_c.left);
	ww = sz.cx + bw;

	MoveWindow(hwnd, rt_w_w.left, rt_w_w.top, ww, rt_w_w.bottom - rt_w_w.top, TRUE);

	GetWindowRect(hwnd, &rt_w_h);
	GetClientRect(hwnd, &rt_c);

	bh = (rt_w_h.bottom - rt_w_h.top) - (rt_c.bottom - rt_c.top);
	wh = sz.cy + bh;

	MoveWindow(hwnd, rt_w_h.left, rt_w_h.top, ww, wh, TRUE);
}

LgBool LgWindowOpen(const LgSizeP size) {
	HWND hwnd;

	registerWndClass();

	hwnd = CreateWindowEx(
		WS_EX_APPWINDOW | WS_EX_WINDOWEDGE,
		LgWindowClassName,
		LG_PROJECT_NAME,
		WS_DLGFRAME | WS_SYSMENU,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		0,
		0,
		NULL,
		NULL,
		GetModuleHandle(NULL),
		NULL);

	LG_CHECK_ERROR(hwnd);
	setHwndClientSize(hwnd, size->width, size->height);
	ShowWindow(hwnd, SW_SHOW);
	UpdateWindow(hwnd);
	
	LG_CHECK_ERROR(LgWindowAttach(hwnd, LgTrue));
	LgLogInfo("window is opened");
	return LgTrue;

_error:
	LgLogError("unable open window.");
	return LgFalse;
}

void LgWindowClose() {
	if (g_hwnd) {
		if (g_destroy) 
			DestroyWindow(g_hwnd);
		else {
			WNDPROC oldProc = (WNDPROC)GetProp(g_hwnd, LgWindowOldWndProcPropName);
			SetWindowLong(g_hwnd, GWL_WNDPROC, (LONG)oldProc);
			RemoveProp(g_hwnd, LgWindowOldWndProcPropName);
		}
		g_hwnd = NULL;
	}
}

void LgWindowSize(LgSizeP size) {
	RECT rt;
	GetClientRect(g_hwnd, &rt);
	size->width = rt.right - rt.left;
	size->height = rt.bottom - rt.top;
}

HWND LgWindowHWND() {
	return g_hwnd;
}

void LgWindowShowKeyboard(LgBool show) {
}

LgBool LgWindowIsHD() {
	return LgTrue;
}