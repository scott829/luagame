#ifndef _LG_VECTOR3_H
#define _LG_VECTOR3_H

#include "lg_basetypes.h"
#include <math.h>

typedef struct LgVector3 {
	float x, y, z;
}*LgVector3P;

float LgVector3Length(const LgVector3P vec);

float LgVector3SquaredLength(const LgVector3P vec);

float LgVector3Distance(const LgVector3P lhs, const LgVector3P rhs);

float LgVector3SquaredDistance(const LgVector3P lhs, const LgVector3P rhs);

float LgVector3Dot(const LgVector3P lhs, const LgVector3P rhs);

void LgVector3Cross(LgVector3P lhs, const LgVector3P rhs);

void LgVector3MidPoint(LgVector3P lhs, const LgVector3P rhs);

void LgVector3Normalize(LgVector3P vec);

LgBool LgVector3IsZero(const LgVector3P vec);

void LgVector3Add(LgVector3P lhs, const LgVector3P rhs);

void LgVector3Sub(LgVector3P lhs, const LgVector3P rhs);

void LgVector3Mul(LgVector3P lhs, const LgVector3P rhs);

void LgVector3Div(LgVector3P lhs, const LgVector3P rhs);

LgBool LgVector3Equal(LgVector3P lhs, const LgVector3P rhs);

void LgVector3AddFloat(LgVector3P lhs, float rhs);

void LgVector3SubFloat(LgVector3P lhs, float rhs);

void LgVector3MulFloat(LgVector3P lhs, float rhs);

void LgVector3DivFloat(LgVector3P lhs, float rhs);

#endif
