﻿#ifndef _LG_PARTICLE_H_
#define _LG_PARTICLE_H_

#include "lg_node.h"
#include "lg_platform.h"
#include "jansson/jansson.h"

enum {
	LG_PS_EVENT_SetActive = LG_NODE_EVENT_LAST,
	LG_PS_EVENT_SetInWorld,
	LG_PS_EVENT_GetConfig,
	LG_PS_EVENT_LAST,
};

// 发射器类型
enum LGPSEmitterType {
	LGPSGravity,	// 重力模式
	LGPSRadius,		// 半径模式
};

typedef struct LgParticleSystemConfig {
	// 粒子属性
	float			emissionRate;	// 每秒发射多少粒子
	int				maxParticles;	// 最大粒子数量
	float			lifeSpan;		// 决定例子会在几秒钟内从屏幕上消失
	float			lifeSpanVariance;
	float			startSize;		// 起始大小
	float			startSizeVariance;
	float			finishSize;		// 终止大小
	float			finishSizeVariance;
	float			emitAngle;		// 发射角度
	float			emitAngleVariance;
	float			rotationStart;	// 起始旋转角度
	float			rotationStartVariance;
	float			rotationEnd;	// 终止旋转角度
	float			rotationEndVariance;
	char			texture[LG_PATH_MAX];

	// 粒子颜色
	float			startRed;		// 起始红色
	float			startGreen;		// 起始绿色
	float			startBlue;		// 起始蓝色
	float			startAlpha;		// 起始透明度
	float			startRedVariance;
	float			startGreenVariance;
	float			startBlueVariance;
	float			startAlphaVariance;

	float			finishRed;			// 终止红色
	float			finishGreen;		// 终止绿色
	float			finishBlue;			// 终止蓝色
	float			finishAlpha;		// 终止透明度
	float			finishRedVariance;
	float			finishGreenVariance;
	float			finishBlueVariance;
	float			finishAlphaVariance;

	// 发射器通用属性
	float			duration;			// 发射器持续时间
	enum LGPSEmitterType	emitterType; // 发射器类型
	float			sourcePositionX;	// 发射器X坐标
	float			sourcePositionXVariance;
	float			sourcePositionY;	// 发射器Y坐标
	float			sourcePositionYVariance;

	// 重力模式属性
	struct {
		float			speed;	// 粒子的速度
		float			speedVariance;
		float			gravityX, gravityY;	// 粒子的重力
		float			radialAccel;		// 径向加速度
		float			radialAccelVariance;
		float			tangencialAccel;	// 正切加速度
		float			tangencialAccelVariance;
		LgBool			rotationIsDir;
	}gravity;

	// 半径模式
	struct {
		float			startRadius;	// 开始时的半径
		float			startRadiusVariance;
		float			endRadius;		// 结束时的半径
		float			endRadiusVariance;
		float			rotatePerSecond;	// 粒子围绕原点每秒旋转的度数
		float			rotatePerSecondVariance;
	}radius;
}*LgParticleSystemConfigP;

LgBool LgNodeIsParticleSystem(LgNodeP node);

LgNodeP LgParticleSystemCreateR(const char *filename);

LgNodeP LgParticleSystemCreateFromConfigR(const LgParticleSystemConfigP config);

void LgParticleSystemSetActive(LgNodeP node, LgBool active);

void LgParticleSystemSetInWorld(LgNodeP node, LgBool inWorld);

void LgParticleSystemGetConfig(LgNodeP node, LgParticleSystemConfigP config);

LgBool LgParticleSystemConfigFromJson(json_t *json,LgParticleSystemConfigP config);

#endif