﻿#include "lg_init.h"
#include "lg_basetypes.h"
#include "lg_window.h"
#include "lg_rendersystem.h"
#include "lg_archive.h"
#include "lg_scriptenv.h"
#include "lg_init.h"
#include "lg_fontsystem.h"
#include "lg_scheduler.h"
#include "lg_actionmanager.h"
#include "lg_director.h"
#include "lg_resourcemanager.h"
#include "lg_shapes.h"
#include "lg_platform.h"
#include "lg_soundplayer.h"
#include "lg_log.h"
#include "lg_cmdline.h"
#include "lg_debug.h"
#include "lg_asyncjob.h"
#include "lg_payment.h"
#include "lg_netclient.h"

json_t *g_globalConfig = NULL;
static char g_resourceDirectory[LG_PATH_MAX] = {0};
struct event_base *g_evtbase = NULL;
static LgBool g_exit = LgFalse;

static void initNetwork() {
#ifdef WIN32
	WSADATA wsaData;
	WSAStartup(MAKEWORD(1, 1), &wsaData);
	evthread_use_windows_threads();
#else
	evthread_use_pthreads();
#endif

	g_evtbase = event_base_new();
}

void shutdownNetwork() {
	if (g_evtbase) event_base_free(g_evtbase);

#ifdef WIN32
	WSACleanup();
#endif
}

static LgBool openWindow() {
	struct LgSize windowSize;
	json_t *graphics, *window_size;

	windowSize.width = 960;
	windowSize.height = 640;

	if ((graphics = json_object_get(g_globalConfig, "graphics"))) {
		if ((window_size = json_object_get(graphics, "window_size"))) {
			json_t *width, *height;
			width = json_object_get(window_size, "width");
			height = json_object_get(window_size, "height");
			if (json_is_integer(width)) windowSize.width = (int)json_integer_value(width);
			if (json_is_integer(height)) windowSize.height = (int)json_integer_value(height);
		}
	}

	LgLogInfo("window size: (%d, %d)", windowSize.width, windowSize.height);
	return LgWindowOpen(&windowSize);
}

static LgBool setRenderDriver() {
	json_t *graphics, *driver;

	if ((graphics = json_object_get(g_globalConfig, "graphics"))) {
		if ((driver = json_object_get(graphics, "driver")) && json_is_string(driver)) {
			LgRenderSystemSetDriver(json_string_value(driver));
			return LgTrue;
		}
	}

	 return LgFalse;
}

LgBool LgInit(int argc, char **argv) {
	LgParseCommandLine(argc, argv);

	// 开启日志
	LgLogSystemOpen();

	// 设置资源目录
	if (strlen(g_commandLine.resourceDirectory) > 0) {
		LgSetResourceDirectory(g_commandLine.resourceDirectory);
	}

	LgLogInfo("resource directory: %s", LgResourceDirectory());
	LgLogInfo("application starting...", NULL);

	// 创建lua环境
	LG_CHECK_ERROR(LgScriptEnvOpen());

	// 打开资源包
	LG_CHECK_ERROR(LgArchiveOpen());

	// 打开配置文件
	g_globalConfig = json_loadp("global.conf");

	if (!g_globalConfig) {
		LgLogError("unable open 'global.conf'.", NULL);
		goto _error;
	}

	// 打开窗口
	LG_CHECK_ERROR(openWindow());

	// 创建渲染环境
	LG_CHECK_ERROR(setRenderDriver());
	if (!LgRenderSystemOpen()) {
		LgLogError("failed to initialize render system!", NULL);
		goto _error;
	}
	
	LG_CHECK_ERROR(LgSoundPlayerOpen());
	LG_CHECK_ERROR(LgShapesOpen());
	LG_CHECK_ERROR(LgFontSystemOpen());
	LG_CHECK_ERROR(LgSchedulerOpen());
	LG_CHECK_ERROR(LgActionManagerOpen());
	LG_CHECK_ERROR(LgResourceManagerOpen());
	LG_CHECK_ERROR(LgAsyncJobManagerOpen());
	LG_CHECK_ERROR(LgDirectorOpen());
	LG_CHECK_ERROR(LgPaymentOpen());

	// 初始化网络
	initNetwork();

	// 运行main.lua
	if (g_commandLine.debug) {
		LgDebugOpen();
	} else {
		LG_CHECK_ERROR(LgScriptEnvRunFile("scripts/main.lua"));
	}

	return LgTrue;

_error:
	LgCleanup();
	return LgFalse;
}

void LgCleanup() {
	if (g_commandLine.debug) LgDebugClose();
	LgPaymentClose();
	LgNetClientClose();
	LgAsyncJobManagerClose();
	LgDirectorClose();
	LgActionManagerClose();
	LgResourceManagerClose();
	LgSchedulerClose();
	LgSoundPlayerClose();

	LgScriptEnvClose();
	
	LgFontSystemClose();
	LgShapesClose();
	LgRenderSystemClose();
	LgWindowClose();
	if (g_globalConfig) json_delete(g_globalConfig);
	LgArchiveClose();

	LgLogSystemClose();
	
	shutdownNetwork();
}

LgBool LgMainLoop() {
	event_base_loop(g_evtbase, EVLOOP_NONBLOCK);
	if (g_commandLine.debug) LgDebugProcessCommands();
	LgSoundPlayerUpdate();
	if (!LgDirectorIsPaused()) LgSchedulerUpdate();
	LgDirectorUpdate();
	lua_gc(LgScriptEnvState(), LUA_GCCOLLECT, 0);
	return !g_exit;
}

const char *LgResourceDirectory() {
	if (g_resourceDirectory[0] == 0) {
		return LgPlatformResourceDirectory();
	} else {
		return g_resourceDirectory;
	}
}

const char *LgResourceFilename(const char *filename) {
	static char path[512];
	sprintf(path, "%s%s", LgResourceDirectory(), filename);
	return path;
}

const char *LgResourceDirname(const char *filename) {
	static char path[512];
	sprintf(path, "%s%s%s", LgResourceDirectory(), filename, LgPlatformPathSeparator());
	return path;
}

void LgSetResourceDirectory(const char *path) {
	strlcpy(g_resourceDirectory, path, sizeof(g_resourceDirectory));
	strcat(g_resourceDirectory, LgPlatformPathSeparator());
}

void LgExit() {
	g_exit = LgTrue;
}

void LgDummyFunction() {
}
