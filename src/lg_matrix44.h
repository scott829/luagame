#ifndef _LG_MATRIX44_H
#define _LG_MATRIX44_H

#include <math.h>
#include "lg_memory.h"
#include "lg_vector3.h"

typedef struct LgMatrix44 {
	float m44[4][4];
}*LgMatrix44P;

void LgMatrix44Indentity(LgMatrix44P m);

void LgMatrix44InitScale(LgMatrix44P m, float sx, float sy, float sz);

void LgMatrix44InitTranslate(LgMatrix44P m, float tx, float ty, float tz);

void LgMatrix44InitRotateX(LgMatrix44P m, float radians);

void LgMatrix44InitRotateY(LgMatrix44P m, float radians);

void LgMatrix44InitRotateZ(LgMatrix44P m, float radians);

void LgMatrix44Transpose(LgMatrix44P m);

void LgMatrix44Mul(LgMatrix44P lhs, const LgMatrix44P rhs);

void LgMatrix44Scale(LgMatrix44P m, float sx, float sy, float sz);

void LgMatrix44Translate(LgMatrix44P m, float tx, float ty, float tz);

void LgMatrix44RotateX(LgMatrix44P m, float radians);

void LgMatrix44RotateY(LgMatrix44P m, float radians);

void LgMatrix44RotateZ(LgMatrix44P m, float radians);

float LgMatrix44Determinant(const LgMatrix44P m);

void LgMatrix44Inverse(LgMatrix44P m);

void LgMatrix44LhToRh(LgMatrix44P m);

#define LgMatrix44RhToLh(m) LgMatrix44LhToRh(m);

void LgMatrix44OrthoOffsetCenterLH(LgMatrix44P m, float left, float right, float bottom, float top, float nearPlane, float farPlane);

void LgMatrix44OrthoOffsetCenterRH(LgMatrix44P m, float left, float right, float bottom, float top, float nearPlane, float farPlane);

void LgMatrix44OrthoLH(LgMatrix44P m, float width, float height, float nearPlane, float farPlane);

void LgMatrix44OrthoRH(LgMatrix44P m, float width, float height, float nearPlane, float farPlane);

void LgMatrix44PerspectiveOffsetCenterLH(LgMatrix44P m, float left, float right, float bottom, float top, float nearPlane, float farPlane);

void LgMatrix44PerspectiveOffsetCenterRH(LgMatrix44P m, float left, float right, float bottom, float top, float nearPlane, float farPlane);

void LgMatrix44PerspectiveLH(LgMatrix44P m, float width, float height, float nearPlane, float farPlane);

void LgMatrix44PerspectiveRH(LgMatrix44P m, float width, float height, float nearPlane, float farPlane);

void LgMatrix44PerspectiveFovLH(LgMatrix44P m, float fov, float aspect, float nearPlane, float farPlane);

void LgMatrix44PerspectiveFovRH(LgMatrix44P m, float fov, float aspect, float nearPlane, float farPlane);

void LgMatrix44LookAtLH(LgMatrix44P m, const LgVector3P eye, const LgVector3P at, const LgVector3P up);

void LgMatrix44LookAtRH(LgMatrix44P m, const LgVector3P eye, const LgVector3P at, const LgVector3P up);

void LgMatrix44MulVector2(const LgMatrix44P m, LgVector3P vec);

#endif
