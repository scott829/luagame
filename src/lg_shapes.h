#ifndef _LG_SHAPES_H
#define _LG_SHAPES_H

#include "lg_rendersystem.h"

LgBool LgShapesOpen();

void LgShapesClose();

LgVertexBufferP LgShapesRectangleTexcoord();

LgVertexBufferP LgShapesRectangleDiffuse();

LgVertexBufferP LgShapesDiffuseTexCroodTriangleList();

int LgShapesDiffuseTexCroodTriangleListCount();

#endif