#ifndef _LG_INPUTSTREAM_H
#define _LG_INPUTSTREAM_H

#include "lg_basetypes.h"

typedef struct LgInputStream *LgInputStreamP;

struct LgInputStreamImpl {
	LgDestroy destroy;
	int (* available)(LgInputStreamP stream);
	int (* readBytes)(LgInputStreamP stream, void *data, int size);
	void (* mark)(LgInputStreamP stream);
	LgBool (* markSupported)(LgInputStreamP stream);
	void (* reset)(LgInputStreamP stream);
};

struct LgInputStream {
	struct LgInputStreamImpl *impl;
};

#define LgInputStreamAvailable(stream) ((LgInputStreamP)stream)->impl->available(((LgInputStreamP)stream))

#define LgInputStreamReadBytes(stream, data, size) ((LgInputStreamP)stream)->impl->readBytes(((LgInputStreamP)stream), data, size)

#define LgInputStreamMark(stream) if (((LgInputStreamP)stream)->impl->mark) ((LgInputStreamP)stream)->impl->mark(((LgInputStreamP)stream))

#define LgInputStreamMarkSupported(stream) ((LgInputStreamP)stream)->impl->markSupported(((LgInputStreamP)stream))

#define LgInputStreamReset(stream) if (((LgInputStreamP)stream)->impl->reset) ((LgInputStreamP)stream)->impl->reset(((LgInputStreamP)stream))

#define LgInputStreamDestroy(stream) ((LgInputStreamP)stream)->impl->destroy(((LgInputStreamP)stream))

void *LgInputStreamReadAll(LgInputStreamP stream);

void *LgInputStreamReadAllBytes(LgInputStreamP stream, int *length);

LgBool LgInputStreamReadRune(LgInputStreamP stream, wchar_t *result);

void LgInputStreamReadLine(LgInputStreamP stream, char *line, int len);

int LgInputStreamSkip(LgInputStreamP stream, int size);

#endif