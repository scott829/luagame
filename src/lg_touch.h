#ifndef _LG_TOUCH_H
#define _LG_TOUCH_H

#include "lg_vector2.h"
#include "lg_time.h"
#include "lg_refcnt.h"

typedef struct LgTouch *LgTouchP;
typedef struct LgNode *LgNodeP;

LG_REFCNT_FUNCTIONS_DECL(LgTouch)

LgTouchP LgTouchCreateR(int id, LgNodeP node, const LgVector2P pt);

void LgTouchReplace(LgTouchP touch, LgNodeP node, const LgVector2P pt);

void LgTouchLocalPosition(LgTouchP touch, LgVector2P pt);

void LgTouchLocalPreviousPosition(LgTouchP touch, LgVector2P pt);

void LgTouchWorldPosition(LgTouchP touch, LgVector2P pt);

void LgTouchWorldPreviousPosition(LgTouchP touch, LgVector2P pt);

LgNodeP LgTouchDownNode(LgTouchP touch);

LgNodeP LgTouchCurrentNode(LgTouchP touch);

void LgTouchDelta(LgTouchP touch, LgVector2P delta);

int LgTouchId(LgTouchP touch);

LgTime LgTouchDownTime(LgTouchP touch);

LgTouchP LgTouchParentTouch(LgTouchP touch);

#endif