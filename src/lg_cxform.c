#include "lg_cxform.h"

void LgCxformIndentity(LgCxformP cxform) {
	cxform->mul[0] = cxform->mul[1] = 
		cxform->mul[2] = cxform->mul[3] = 1.0f;
	cxform->add[0] = cxform->add[1] = 
		cxform->add[2] = cxform->add[3] = 0.0f;
}

void LgCxformMul(LgCxformP lhs, const LgCxformP rhs) {
	struct LgCxform result;

	result.mul[0] = lhs->mul[0] * rhs->mul[0];
	result.mul[1] = lhs->mul[1] * rhs->mul[1];
	result.mul[2] = lhs->mul[2] * rhs->mul[2];
	result.mul[3] = lhs->mul[3] * rhs->mul[3];
	result.add[0] = lhs->add[0] + lhs->mul[0] * rhs->add[0];
	result.add[1] = lhs->add[1] + lhs->mul[1] * rhs->add[1];
	result.add[2] = lhs->add[2] + lhs->mul[2] * rhs->add[2];
	result.add[3] = lhs->add[3] + lhs->mul[3] * rhs->add[3];
	LgCopyMemory(lhs, &result, sizeof(struct LgCxform));
}

void LgCxformInitColor(LgCxformP cxform, LgColor color) {
	cxform->mul[0] = cxform->mul[1] = cxform->mul[2] = cxform->mul[3] = 0;
	cxform->add[0] = LgColorRed(color) / 255.0f,
		cxform->add[1] = LgColorGreen(color) / 255.0f,
		cxform->add[2] = LgColorBlue(color) / 255.0f,
		cxform->add[3] = LgColorAlpha(color) / 255.0f;
}
