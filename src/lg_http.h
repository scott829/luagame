﻿#ifndef _LG_HTTP_H
#define _LG_HTTP_H

#include "lg_basetypes.h"
#include "lg_refcnt.h"

enum LgHttpMethod {
	LgHttpMethodGet,
	LgHttpMethodPost,
};

enum LgHttpState {
	LgHttpStateDisconnected,
	LgHttpStateConnecting,
	LgHttpStateSendingRequestHeaders,
	LgHttpStateSendingRequestBody,
	LgHttpStateReadingFirstLine,
	LgHttpStateReadingHeaders,
	LgHttpStateReadingBody,
};

enum LgHttpErrorType {
	LgHttpErrorNone,
	LgHttpErrorInternal,
	LgHttpErrorTimeout,
	LgHttpErrorClose,
	LgHttpErrorReadData,
	LgHttpErrorBadResponse,
	LgHttpErrorNotFound,
	LgHttpErrorForbidden,
};

typedef struct LgHttp *	LgHttpP;

typedef struct LgHttpResponseDataEvent {
	const char *	data;	// 接收到的数据
	int				size;	// 接收到的数据长度
	LgBool			finished;	// 如果为true，则表示已经没有数据了
	float			ratio;	// 接受数据的比例 0 - 1
}*LgHttpResponseDataEventP;

typedef struct LgHttpStateChangedEvent {
	enum LgHttpState oldState, newState;
}LgHttpStateChangedEventP;

LG_REFCNT_FUNCTIONS_DECL(LgHttp)

LgHttpP LgHttpCreateR();

LgBool LgHttpGet(LgHttpP http, const char *url);

LgBool LgHttpPost(LgHttpP http, const char *url, const void *postData, int postDataLength);

void LgHttpSetRequestField(LgHttpP http, const char *name, const char *value);

const char *LgHttpResponseField(LgHttpP http, const char *name);

int LgHttpResponseCode(LgHttpP http);

LgSlot *LgHttpSlotResponseData(LgHttpP http);

LgSlot *LgHttpSlotStateChanged(LgHttpP http);

LgSlot *LgHttpSlotError(LgHttpP http);

#endif