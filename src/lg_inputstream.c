#include "lg_inputstream.h"
#include "lg_memory.h"

void *LgInputStreamReadAll(LgInputStreamP stream) {
	int len = LgInputStreamAvailable(stream);
	char *data = (char *)LgMalloc(len + 1), *p;
	char temp[256];
	int rsz;

	p = data;
	while((rsz = LgInputStreamReadBytes(stream, temp, sizeof(temp))) > 0) {
		LgCopyMemory(p, temp, rsz);
		p += rsz;
	}
	data[len] = 0;
	return data;
}

void *LgInputStreamReadAllBytes(LgInputStreamP stream, int *length) {
	int len = LgInputStreamAvailable(stream);
	char *data = (char *)LgMalloc(len), *p;
	char temp[256];
	int rsz;

	p = data;
	while((rsz = LgInputStreamReadBytes(stream, temp, sizeof(temp))) > 0) {
		LgCopyMemory(p, temp, rsz);
		p += rsz;
	}
	*length = len;
	return data;
}

LgBool LgInputStreamReadRune(LgInputStreamP stream, wchar_t *result) {
	unsigned char s[6], c;

	if (LgInputStreamReadBytes(stream, s, 1) < 1) return LgFalse;
	c = s[0];

	if (c < 0x80) {
		*result = c;
		return LgTrue;
	}
	else if (c < 0xc2) {
		return LgFalse;
	}
	else if (c < 0xe0) {
		if (LgInputStreamReadBytes(stream, s + 1, 1) < 1) return LgFalse;
		if (!((s[1] ^ 0x80) < 0x40)) return LgFalse;
		*result = ((wchar_t)(c & 0x1f) << 6) | (wchar_t)(s[1] ^ 0x80);
		return LgTrue;
	}
	else if (c < 0xf0) {
		if (LgInputStreamReadBytes(stream, s + 1, 2) < 2) return LgFalse;
		if (!((s[1] ^ 0x80) < 0x40 && (s[2] ^ 0x80) < 0x40
			&& (c >= 0xe1 || s[1] >= 0xa0)))
			return LgFalse;
		*result = ((wchar_t) (c & 0x0f) << 12) |
			((wchar_t) (s[1] ^ 0x80) << 6) | 
			(wchar_t) (s[2] ^ 0x80);

		return LgTrue;
	} 
	else if (c < 0xf8 && sizeof(wchar_t) * 8 >= 32) {
		if (LgInputStreamReadBytes(stream, s + 1, 3) < 3) return LgFalse;
		if (!((s[1] ^ 0x80) < 0x40 && (s[2] ^ 0x80) < 0x40
			&& (s[3] ^ 0x80) < 0x40
			&& (c >= 0xf1 || s[1] >= 0x90)))
			return LgFalse;
		*result = ((wchar_t) (c & 0x07) << 18) | 
			((wchar_t) (s[1] ^ 0x80) << 12) |
			((wchar_t) (s[2] ^ 0x80) << 6) |
			(wchar_t) (s[3] ^ 0x80);

		return LgTrue;
	} 
	else if (c < 0xfc && sizeof(wchar_t) * 8 >= 32) {
		if (LgInputStreamReadBytes(stream, s + 1, 4) < 4) return LgFalse;
		if (!((s[1] ^ 0x80) < 0x40 && (s[2] ^ 0x80) < 0x40
			&& (s[3] ^ 0x80) < 0x40 && (s[4] ^ 0x80) < 0x40
			&& (c >= 0xf9 || s[1] >= 0x88)))
			return LgFalse;
		*result = ((wchar_t) (c & 0x03) << 24) | 
			((wchar_t) (s[1] ^ 0x80) << 18) |
			((wchar_t) (s[2] ^ 0x80) << 12) |
			((wchar_t) (s[3] ^ 0x80) << 6)| 
			(wchar_t) (s[4] ^ 0x80);

		return LgTrue;
	} 
	else if (c < 0xfe && sizeof(wchar_t) * 8 >= 32) {
		if (LgInputStreamReadBytes(stream, s + 1, 5) < 5) return LgFalse;
		if (!((s[1] ^ 0x80) < 0x40 && (s[2] ^ 0x80) < 0x40
			&& (s[3] ^ 0x80) < 0x40 && (s[4] ^ 0x80) < 0x40
			&& (s[5] ^ 0x80) < 0x40
			&& (c >= 0xfd || s[1] >= 0x84)))
			return LgFalse;
		*result = ((wchar_t) (c & 0x01) << 30) |
			((wchar_t) (s[1] ^ 0x80) << 24) | 
			((wchar_t) (s[2] ^ 0x80) << 18) |
			((wchar_t) (s[3] ^ 0x80) << 12)	|
			((wchar_t) (s[4] ^ 0x80) << 6) |
			(wchar_t) (s[5] ^ 0x80);

		return LgTrue;
	} 
	else
		return LgFalse;
}

void LgInputStreamReadLine(LgInputStreamP stream, char *line, int len) {
	char c;
	int i = 0;

	line[i] = 0;

	while(LgTrue) {
		if (LgInputStreamReadBytes(stream, &c, sizeof(c))) {
			if (c == '\n') break;
			else {
				if (c != '\r' && i < len - 1) {
					line[i++] = c;
					line[i] = 0;
				}
			}
		} else {
			break;
		}
	}
}

int LgInputStreamSkip(LgInputStreamP stream, int size) {
	char temp[512];
	int count = 0;
	while(size > 0) {
		int rsz = LgMin(sizeof(temp), size);
		if (LgInputStreamReadBytes(stream, temp, rsz) < rsz) {
			break;
		}
		size -= rsz;
		count += rsz;
	}
	return count;
}
