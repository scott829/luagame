#ifndef _LG_IMAGECOPY_H
#define _LG_IMAGECOPY_H

#include "lg_image.h"
#include "lg_blit.h"
#include "lg_rendersystem.h"

LgTextureP LgImageCreateTextureR(LgImageP image);

void LgImageCopy(LgImageP dest, const LgRectP destRect, LgImageP src, const LgRectP srcRect, LgBlit blit);

void LgImageCopyData(void *destData, int destPitch, enum LgImageFormat_ destFormat, const LgRectP destRect, 
					 const void *srcData, int srcPitch, enum LgImageFormat_ srcFormat, const LgRectP srcRect, LgBlit blit);

#endif