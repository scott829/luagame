#include "lg_colorlayer.h"
#include "lg_shapes.h"
#include "lg_director.h"

typedef struct LgColorData {
	struct LgLayerData	base;
	LgColor				color;
}*LgColorDataP;

static void colorLayerEventProc(LgEventType type, LgNodeP node, void *param) {
	switch(type) {
	case LG_NODE_EVENT_Draw:
		{
			LgColorDataP cd = (LgColorDataP)LgNodeUserData(node);
			struct LgMatrix44 oldMatrix, newMatrix;
			struct LgCxform oldCxform, newCxform, tmpCxform;
			struct LgAabb contentBox;

			LgRenderSystemGetTransform(LgTransformWorld, &oldMatrix);
			LgRenderSystemGetCxform(&oldCxform);

			LgNodeContentBox(node, &contentBox);
			LgMatrix44InitScale(&newMatrix, contentBox.width, contentBox.height, 1);
			LgMatrix44Mul(&newMatrix, &oldMatrix);
			LgRenderSystemSetTransform(LgTransformWorld, &newMatrix);

			LgCopyMemory(&newCxform, &oldCxform, sizeof(struct LgCxform));
			LgCxformInitColor(&tmpCxform, cd->color);
			LgCxformMul(&newCxform, &tmpCxform);
			LgRenderSystemSetCxform(&newCxform);

			LgRenderSystemSetPixelShader(LgPixelShaderCxformDiffuse);
			LgRenderSystemDrawPrimitive(LgPrimitiveTriangleStrip, LgFvfXYZ | LgFvfDiffuse, LgShapesRectangleDiffuse(), 2);

			LgRenderSystemSetTransform(LgTransformWorld, &oldMatrix);
			LgRenderSystemSetCxform(&oldCxform);
			LgDirectorUpdateBatch(1);
			return;
		}
		break;
	case LG_COLORLAYER_EVENT_SetColor:
		{
			LgColorDataP cd = (LgColorDataP)LgNodeUserData(node);
			cd->color = *((LgColor *)param);
		}
		break;
	case LG_COLORLAYER_EVENT_Color:
		{
			LgColorDataP cd = (LgColorDataP)LgNodeUserData(node);
			*((LgColor *)param) = cd->color;
		}
		break;
	}

	LgLayerEventProc(type, node, param);
}

LgNodeP LgColorLayerCreateR(LgColor color) {
	LgNodeP node = NULL;
	LgColorDataP cd = NULL;

	node = LgNodeCreateR((LgEvent)colorLayerEventProc);
	LG_CHECK_ERROR(node);

	cd = (LgColorDataP)LgLayerDataCreate(sizeof(struct LgColorData));
	LG_CHECK_ERROR(cd);
	cd->color = color;
	LgNodeSetUserData(node, cd);

	return node;

_error:
	if (node) LgNodeRelease(node);
	return NULL;
}

void LgColorLayerSetColor(LgNodeP node, LgColor color) {
	LgNodeDoEvent(LG_COLORLAYER_EVENT_SetColor, node, &color);
}

LgColor LgColorLayerColor(LgNodeP node) {
	LgColor color;
	LgNodeDoEvent(LG_COLORLAYER_EVENT_Color, node, &color);
	return color;
}

LgBool LgNodeIsColorLayer(LgNodeP node) {
	return LgNodeEventProc(node) == (LgEvent)colorLayerEventProc;
}
