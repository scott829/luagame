#include "lg_basetypes.h"

void LgSlotClear(LgSlot *slot) {
	if (slot->action) {
		if (slot->destroyUserData) slot->destroyUserData(slot->userdata);
		slot->action = NULL;
		slot->destroyUserData = NULL;
		slot->cloneUserData = NULL;
		slot->userdata = NULL;
	}
}

void LgSlotAssign(LgSlot *slot, SLOT_PARAMS) {
	LgSlotClear(slot);
	if (action) {
		slot->action = action;
		slot->destroyUserData = destroyUserdata;
		slot->cloneUserData = cloneUserdata;
		slot->userdata = userdata;
	}
}

void LgSlotCall(LgSlot *slot, void *param) {
	if (slot->action) slot->action(param, slot->userdata);
}

void LgSlotClone(LgSlot *dest, LgSlot *src) {
	LgSlotClear(dest);
	if (src->action) {
		dest->action = src->action;
		if (src->cloneUserData) {
			dest->userdata = src->cloneUserData(src->userdata);
		} else {
			dest->userdata = src->userdata;
		}
		dest->cloneUserData = src->cloneUserData;
		dest->destroyUserData = src->destroyUserData;
	}
}

LgBool LgSlotIsEmpty(LgSlot *slot) {
	return slot->action == NULL;
}