﻿#ifndef _LG_PLATFORM_H
#define _LG_PLATFORM_H

#include "lg_basetypes.h"
#include <string.h>

// 忽略大小写的字符串比较函数
#if !defined(HAVE_STRNOCASECMP)
#	if defined(HAVE_STRICMP)
#		define strnocasecmp stricmp
#	elif defined(HAVE_STRCASECMP)
#		define strnocasecmp strcasecmp
#	else
#		define PLATFORM_STRNOCASECMP
		int strnocasecmp(const char *lhs, const char *rhs);
#	endif
#endif

// 安全的字符串拷贝函数
#if !defined(HAVE_STRLCPY)
#	if defined(HAVE_STRCPY_S)
#		define strlcpy(dst, src, sz) strcpy_s(dst, sz, src)
#	else
#		define PLATFORM_STRLCPY
		int strlcpy(char *dst, const char *src, size_t sz);
#	endif
#endif

// 指定长度的字符串克隆函数
#if !defined(HAVE_STRNDUP)
#	define PLATFORM_STRNDUP
	char *strndup(const char *src, size_t sz);
#endif

#define LG_PATH_MAX 512

// 获取当前平台类型
const char *LgCurrentPlatform();

// 获得资源目录，不要直接使用，应该使用LgResourceDirectory函数
const char *LgPlatformResourceDirectory();

const char *LgPlatformPathSeparator();

// 获取文档目录（可写）
const char *LgPlatformDocumentDirectory();

#endif
