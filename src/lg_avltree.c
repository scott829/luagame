#include "lg_avltree.h"
#include "lg_memory.h"
#include "libavl/tavl.h"
#include "libavl/trb.h"

void LgAvlTreeDestroy(LgAvlTreeP tree) {
	((LgAvlTreeP)tree)->impl->destroy(tree);
}

// avl tree -----------------------------------------------------------------------
struct LgAvlTree_Avl {
	struct LgAvlTree	base;
	struct tavl_table *	avl;
	LgDestroy			destroyData;
	LgCompare			compareData;
};

static void tavlDeleteItem(void *data, void *param) {
	struct LgAvlTree_Avl *avl = (struct LgAvlTree_Avl *)param;
	if (avl->destroyData) avl->destroyData(data);
}

static int tavlCompareItem(void *data1, void *data2, void *param) {
	struct LgAvlTree_Avl *avl = (struct LgAvlTree_Avl *)param;
	return avl->compareData ? avl->compareData(data1, data2) : 
		(int)((char *)data1 - (char *)data2);
}

static void avlDestroy(void *tree) {
	struct LgAvlTree_Avl *avl = (struct LgAvlTree_Avl *)tree;
	tavl_destroy(avl->avl, tavlDeleteItem);
	LgFree(tree);
}

static LgBool avlInsert(LgAvlTreeP tree, void *data) {
	struct LgAvlTree_Avl *avl = (struct LgAvlTree_Avl *)tree;
	return tavl_insert(avl->avl, data) == NULL;
}

static void avlReplace(LgAvlTreeP tree, void *data) {
	struct LgAvlTree_Avl *avl = (struct LgAvlTree_Avl *)tree;
	void *duplicate = tavl_replace(avl->avl, data);
	if (duplicate) {
		if (avl->destroyData) avl->destroyData(duplicate);
	}
}

static void *avlFind(LgAvlTreeP tree, void *data) {
	struct LgAvlTree_Avl *avl = (struct LgAvlTree_Avl *)tree;
	return tavl_find(avl->avl, data);
}

static LgBool avlRemove(LgAvlTreeP tree, void *data) {
	struct LgAvlTree_Avl *avl = (struct LgAvlTree_Avl *)tree;
	void *itemData = tavl_delete(avl->avl, data);
	if (itemData) {
		if (avl->destroyData) avl->destroyData(itemData);
		return LgTrue;
	} else {
		return LgFalse;
	}
}

static void avlClear(LgAvlTreeP tree) {
	struct LgAvlTree_Avl *avl = (struct LgAvlTree_Avl *)tree;
	tavl_destroy(avl->avl, tavlDeleteItem);
	avl->avl = tavl_create((tavl_comparison_func *)tavlCompareItem, tree, NULL);
}

static int avlCount(LgAvlTreeP tree) {
	struct LgAvlTree_Avl *avl = (struct LgAvlTree_Avl *)tree;
	return avl->avl->tavl_count;
}

static void avlForeach(LgAvlTreeP tree, LgForeach foreach, void *userdata) {
	struct LgAvlTree_Avl *avl = (struct LgAvlTree_Avl *)tree;
	struct tavl_traverser t;
	void *data;

	tavl_t_init(&t, avl->avl);
	while((data = tavl_t_next(&t)) != NULL) {
		foreach(data, userdata);
	}
}

static struct LgAvlTreeImpl avlImpl = {
	avlDestroy,
	avlInsert,
	avlReplace,
	avlFind,
	avlRemove,
	avlClear,
	avlCount,
	avlForeach,
};

LgAvlTreeP LgAvlTreeCreate(LgCompare compareData, LgDestroy destroyData) {
	struct LgAvlTree_Avl *tree = (struct LgAvlTree_Avl *)LgMalloc(sizeof(struct LgAvlTree_Avl));
	tree->base.impl = &avlImpl;
	tree->avl = tavl_create((tavl_comparison_func *)tavlCompareItem, tree, NULL);
	tree->destroyData = destroyData;
	tree->compareData = compareData;
	return (LgAvlTreeP)tree;
}

// rb tree -----------------------------------------------------------------------
struct LgAvlTree_Rb {
	struct LgAvlTree	base;
	struct trb_table *	avl;
	LgDestroy			destroyData;
	LgCompare			compareData;
};

static void trbDeleteItem(void *data, void *param) {
	struct LgAvlTree_Rb *avl = (struct LgAvlTree_Rb *)param;
	if (avl->destroyData) avl->destroyData(data);
}

static int trbCompareItem(void *data1, void *data2, void *param) {
	struct LgAvlTree_Rb *avl = (struct LgAvlTree_Rb *)param;
	return avl->compareData ? avl->compareData(data1, data2) : 
		(int)((char *)data1 - (char *)data2);
}

static void trbDestroy(void *tree) {
	struct LgAvlTree_Rb *avl = (struct LgAvlTree_Rb *)tree;
	trb_destroy(avl->avl, trbDeleteItem);
	LgFree(tree);
}

static LgBool trbInsert(LgAvlTreeP tree, void *data) {
	struct LgAvlTree_Rb *avl = (struct LgAvlTree_Rb *)tree;
	return trb_insert(avl->avl, data) == NULL;
}

static void trbReplace(LgAvlTreeP tree, void *data) {
	struct LgAvlTree_Rb *avl = (struct LgAvlTree_Rb *)tree;
	void *duplicate = trb_replace(avl->avl, data);
	if (duplicate) {
		if (avl->destroyData) avl->destroyData(duplicate);
	}
}

static void *trbFind(LgAvlTreeP tree, void *data) {
	struct LgAvlTree_Rb *avl = (struct LgAvlTree_Rb *)tree;
	return trb_find(avl->avl, data);
}

static LgBool trbRemove(LgAvlTreeP tree, void *data) {
	struct LgAvlTree_Rb *avl = (struct LgAvlTree_Rb *)tree;
	void *itemData = trb_delete(avl->avl, data);
	if (itemData) {
		if (avl->destroyData) avl->destroyData(itemData);
		return LgTrue;
	} else {
		return LgFalse;
	}
}

static void trbClear(LgAvlTreeP tree) {
	struct LgAvlTree_Rb *avl = (struct LgAvlTree_Rb *)tree;
	trb_destroy(avl->avl, trbDeleteItem);
	avl->avl = trb_create((tavl_comparison_func *)trbCompareItem, tree, NULL);
}

static int trbCount(LgAvlTreeP tree) {
	struct LgAvlTree_Rb *avl = (struct LgAvlTree_Rb *)tree;
	return avl->avl->trb_count;
}

static void trbForeach(LgAvlTreeP tree, LgForeach foreach, void *userdata) {
	struct LgAvlTree_Rb *avl = (struct LgAvlTree_Rb *)tree;
	struct trb_traverser t;
	void *data;

	trb_t_init(&t, avl->avl);
	while((data = trb_t_next(&t)) != NULL) {
		foreach(data, userdata);
	}
}

static struct LgAvlTreeImpl rbImpl = {
	trbDestroy,
	trbInsert,
	trbReplace,
	trbFind,
	trbRemove,
	trbClear,
	trbCount,
	trbForeach,
};

LgAvlTreeP LgRbTreeCreate(LgCompare compareData, LgDestroy destroyData) {
	struct LgAvlTree_Rb *tree = (struct LgAvlTree_Rb *)LgMalloc(sizeof(struct LgAvlTree_Rb));
	tree->base.impl = &rbImpl;
	tree->avl = trb_create((tavl_comparison_func *)trbCompareItem, tree, NULL);
	tree->destroyData = destroyData;
	tree->compareData = compareData;
	return (LgAvlTreeP)tree;
}