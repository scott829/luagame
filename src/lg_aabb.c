#include "lg_aabb.h"
#include "lg_basetypes.h"

void LgAabbLeftTop(const LgAabbP aabb, LgVector2P vec) {
	vec->x = aabb->x, vec->y = aabb->y;
}

void LgAabbRightTop(const LgAabbP aabb, LgVector2P vec) {
	vec->x = aabb->x + aabb->width, vec->y = aabb->y;
}

void LgAabbLeftBottom(const LgAabbP aabb, LgVector2P vec) {
	vec->x = aabb->x, vec->y = aabb->y + aabb->height;
}

void LgAabbRightBottom(const LgAabbP aabb, LgVector2P vec) {
	vec->x = aabb->x + aabb->width, vec->y = aabb->y + aabb->height;
}

LgBool LgAabbIntersect(const LgAabbP lhs, const LgAabbP rhs) {
	return !(lhs->x > rhs->x + rhs->width ||
		rhs->x > lhs->x + lhs->width ||
		rhs->y > lhs->y + lhs->height ||
		lhs->y > rhs->y + rhs->height);
}

LgBool LgAabbContains(const LgAabbP aabb, const LgVector2P pt) {
	return pt->x >= aabb->x && pt->y >= aabb->y &&
		pt->x < aabb->x + aabb->width && pt->y < aabb->y + aabb->height;
}

LgBool LgAabbContainsAabb(const LgAabbP aabb, const LgAabbP aabb2) {
	struct LgVector2 vec;

	LgAabbLeftTop(aabb2, &vec);
	if (!LgAabbContains(aabb, &vec)) return LgFalse;
	LgAabbRightTop(aabb2, &vec);
	if (!LgAabbContains(aabb, &vec)) return LgFalse;
	LgAabbLeftBottom(aabb2, &vec);
	if (!LgAabbContains(aabb, &vec)) return LgFalse;
	LgAabbRightBottom(aabb2, &vec);
	if (!LgAabbContains(aabb, &vec)) return LgFalse;
	return LgTrue;
}

void LgAabbCenterPoint(const LgAabbP aabb, LgVector2P pt) {
	pt->x = aabb->x + aabb->width / 2;
	pt->y = aabb->y + aabb->height / 2;
}

LgBool LgAabbIntersectEllipse(const LgAabbP aabb, float rx, float ry, float radius) {
	float dx = LgMax(LgMin(rx, aabb->x + aabb->width), aabb->x);
	float dy = LgMax(LgMin(ry, aabb->y + aabb->height), aabb->y);
	return (rx - dx) * (rx - dx) + (ry - dy) * (ry - dy) <= radius * radius;
}