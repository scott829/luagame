#include "lg_affinetrans.h"

void LgAffineTransformIndentity(LgAffineTransformP af) {
	af->a = 1, af->b = 0, af->c = 0, af->d = 1;
	af->tx = af->ty = 0;
}

void LgAffineTransformMul(LgAffineTransformP lhs, const LgAffineTransformP rhs) {
	float _a, _b, _c, _d, _tx, _ty;
	_a = lhs->a * rhs->a + lhs->b * rhs->c;
	_b = lhs->a * rhs->b + lhs->b * rhs->d;
	_c = lhs->c * rhs->a + lhs->d * rhs->c;
	_d = lhs->c * rhs->b + lhs->d * rhs->d;
	_tx = lhs->tx * rhs->a + lhs->ty * rhs->c + rhs->tx;
	_ty = lhs->tx * rhs->b + lhs->ty * rhs->d + rhs->ty;
	lhs->a = _a, lhs->b = _b, lhs->c = _c, lhs->d = _d;
	lhs->tx = _tx, lhs->ty = _ty;
}

void LgAffineTransformInitTranslate(LgAffineTransformP af, float tx, float ty) {
	af->a = 1, af->b = 0, af->c = 0, af->d = 1, af->tx = tx, af->ty = ty;
}

void LgAffineTransformInitScale(LgAffineTransformP af, float sx, float sy) {
	af->a = sx, af->b = 0, af->c = 0, af->d = sy, af->tx = 0, af->ty = 0;
}

void LgAffineTransformInitRotate(LgAffineTransformP af, float radians) {
	float _sin = sinf(radians);
	float _cos = cosf(radians);
	af->a = _cos, af->b = _sin, af->c = -_sin, af->d = _cos, af->tx = 0, af->ty = 0;
}

void LgAffineTransformTranslate(LgAffineTransformP af, float tx, float ty) {
	struct LgAffineTransform t;
	LgAffineTransformInitTranslate(&t, tx, ty);
	LgAffineTransformMul(af, &t);
}

void LgAffineTransformScale(LgAffineTransformP af, float sx, float sy) {
	struct LgAffineTransform t;
	LgAffineTransformInitScale(&t, sx, sy);
	LgAffineTransformMul(af, &t);
}

void LgAffineTransformRotate(LgAffineTransformP af, float radians) {
	struct LgAffineTransform t;
	LgAffineTransformInitRotate(&t, radians);
	LgAffineTransformMul(af, &t);
}

void LgAffineTransformMulVector2(const LgAffineTransformP af, LgVector2P vec) {
	float _x = af->a * vec->x + af->c * vec->y + af->tx,
		_y = af->b * vec->x + af->d * vec->y + af->ty;
	vec->x = _x, vec->y = _y;
}

void LgAffineTransformMulAabb(const LgAffineTransformP af, LgAabbP aabb) {
	struct LgVector2 lt, lb, rt, rb;
	float minx, miny, maxx, maxy;

	LgAabbLeftTop(aabb, &lt);
	LgAabbLeftBottom(aabb, &lb);
	LgAabbRightTop(aabb, &rt);
	LgAabbRightBottom(aabb, &rb);
	
	LgAffineTransformMulVector2(af, &lt);
	LgAffineTransformMulVector2(af, &lb);
	LgAffineTransformMulVector2(af, &rt);
	LgAffineTransformMulVector2(af, &rb);

	maxx = LgMax(rb.x, LgMax(lb.x, LgMax(lt.x, rt.x)));
	maxy = LgMax(rb.y, LgMax(lb.y, LgMax(lt.y, rt.y)));
	minx = LgMin(rb.x, LgMin(lb.x, LgMin(lt.x, rt.x)));
	miny = LgMin(rb.y, LgMin(lb.y, LgMin(lt.y, rt.y)));
	aabb->x = minx, aabb->y = miny;
	aabb->width = maxx - minx, aabb->height = maxy - miny;
}

void LgAffineTransformMatrix44(const LgAffineTransformP af, LgMatrix44P m) {
	m->m44[0][0] = af->a,	m->m44[0][1] = af->b,	m->m44[0][2] = 0, m->m44[0][3] = 0;
	m->m44[1][0] = af->c,	m->m44[1][1] = af->d,	m->m44[1][2] = 0, m->m44[1][3] = 0;
	m->m44[2][0] = 0,		m->m44[2][1] = 0,		m->m44[2][2] = 1, m->m44[2][3] = 0;
	m->m44[3][0] = af->tx,	m->m44[3][1] = af->ty,	m->m44[3][2] = 0, m->m44[3][3] = 1;
}

void LgAffineTransformInverse(LgAffineTransformP af) {
	float determinant = 1.0f / (af->a * af->d - af->b * af->c);
	float _a, _b, _c, _d, _tx, _ty;

	_a = determinant * af->d;
	_b = -determinant * af->b;
	_c = -determinant * af->c;
	_d = determinant * af->a;
	_tx = determinant * (af->c * af->ty - af->d * af->tx);
	_ty = determinant * (af->b * af->tx - af->a * af->ty);
	af->a = _a, af->b = _b, af->c = _c, af->d = _d;
	af->tx = _tx, af->ty = _ty;
}
