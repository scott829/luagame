#include "lg_init.h"
#include <android/input.h>
#include <android/log.h>
#include "android/android_native_app_glue.h"
#include "lg_keyevent.h"
#include "lg_director.h"

struct android_app* g_androidApp;
static LgBool g_inited = LgFalse;

static enum LgKey keyMapping(int key) {
	switch(key) {
	case AKEYCODE_DPAD_UP: return LgKeyUp;
	case AKEYCODE_DPAD_DOWN: return LgKeyDown;
	case AKEYCODE_DPAD_LEFT: return LgKeyLeft;
	case AKEYCODE_DPAD_RIGHT: return LgKeyRight;
	case AKEYCODE_PAGE_DOWN: return LgKeyPageDown;
	case AKEYCODE_PAGE_UP: return LgKeyPageUp;
	case AKEYCODE_DPAD_CENTER: return LgKeyEnter;
	case AKEYCODE_BACK: return LgKeyBack;
	case AKEYCODE_TAB: return LgKeyOptions;
	case AKEYCODE_F1: return LgKeyF1;
	case AKEYCODE_F2: return LgKeyF2;
	case AKEYCODE_F3: return LgKeyF3;
	case AKEYCODE_F4: return LgKeyF4;
	case AKEYCODE_F5: return LgKeyF5;
	case AKEYCODE_F6: return LgKeyF6;
	case AKEYCODE_F7: return LgKeyF7;
	case AKEYCODE_F8: return LgKeyF8;
	case AKEYCODE_F9: return LgKeyF9;
	case AKEYCODE_F10: return LgKeyF10;
	case AKEYCODE_F11: return LgKeyF11;
	case AKEYCODE_F12: return LgKeyF12;
	default: return LgKeyUnknown;
	}
}

static int32_t handleInput(struct android_app* app, AInputEvent* event) {
	if (AInputEvent_getType(event) == AINPUT_EVENT_TYPE_MOTION) {
		int act = (AMotionEvent_getAction(event) & AMOTION_EVENT_ACTION_MASK);
		int count = AMotionEvent_getPointerCount(event), i;
		static struct LgPoint points[10];
		static int touchesId[10];

		for (i = 0; i < count; i++) {
			points[i].x = AMotionEvent_getX(event, i);
			points[i].y = AMotionEvent_getY(event, i);
			touchesId[i] = AMotionEvent_getPointerId(event, i);
		}

		switch(act) {
		case AMOTION_EVENT_ACTION_DOWN:
			LgDirectorHandleTouchBegan(count, touchesId, points);
			break;
		case AMOTION_EVENT_ACTION_UP:
			LgDirectorHandleTouchEnded(count, touchesId);
			break;
		case AMOTION_EVENT_ACTION_MOVE:
			LgDirectorHandleTouchMoved(count, touchesId, points);
			break;
		case AMOTION_EVENT_ACTION_CANCEL:
			LgDirectorHandleTouchCancelled(count, touchesId);
			break;
		}

		return 1;
	} else if (AInputEvent_getType(event) == AINPUT_EVENT_TYPE_KEY) {
		int act = AKeyEvent_getAction(event);
		enum LgKey key = keyMapping(AKeyEvent_getKeyCode(event));

		if (key != LgKeyUnknown) {
			switch(act) {
			case AKEY_EVENT_ACTION_DOWN:
				LgDirectorHandleKeyDown(key);
				break;
			case AKEY_EVENT_ACTION_UP:
				LgDirectorHandleKeyUp(key);
				break;
			}
		}
	}

	return 0;
}

static void handleCommand(struct android_app* app, int32_t cmd) {
	switch(cmd) {
	case APP_CMD_SAVE_STATE:
		break;
	case APP_CMD_INIT_WINDOW:
		if (!LgInit(0, NULL)) {
			ANativeActivity_finish(app->activity);
		}
		g_inited = LgTrue;
		break;
	case APP_CMD_TERM_WINDOW:
		if (g_inited) {
			LgCleanup();
			g_inited = LgFalse;
		}
		break;
	case APP_CMD_GAINED_FOCUS:
		LgDirectorResume();
		break;
	case APP_CMD_LOST_FOCUS:
		LgDirectorPause();
		break;
	}
}

void android_main(struct android_app* state) {
	app_dummy();

	g_androidApp = state;
	state->userData = NULL;
	state->onAppCmd = handleCommand;
	state->onInputEvent = handleInput;

	while(1) {
		// Read all pending events.
		int ident;
		int events;
		struct android_poll_source* source;

		while((ident = ALooper_pollAll(0, NULL, &events, (void**)&source)) >= 0) {
			// Process this event.
			if (source != NULL) {
				source->process(state, source);
			}

			// Check if we are exiting.
			if (state->destroyRequested != 0) {
				return;
			}
		}

		if (g_inited) LgMainLoop();
	}
}