#ifndef _LG_GRID_H
#define _LG_GRID_H

#include "lg_basetypes.h"
#include "lg_vector3.h"
#include "lg_vector2.h"

enum LgGridAnchor {
	LgGridAnchorLT,
	LgGridAnchorRT,
	LgGridAnchorLB,
	LgGridAnchorRB,
};

typedef struct LgGrid *LgGridP;

struct LgGridImpl {
	LgDestroy destroy;
	void (* beforeDraw)(LgGridP grid);
	void (* afterDraw)(LgGridP grid);
	void (* enter)(LgGridP grid);
	void (* exit)(LgGridP grid);
	int (* vertexIdx)(LgGridP grid, int col, int row, enum LgGridAnchor anchor);
};

struct LgGrid {
	int cols, rows;
	struct LgGridImpl *impl;
};

#define LgGridDestroy(grid) ((LgGridP)grid)->impl->destroy(((LgGridP)grid))

#define LgGridBeforeDraw(grid) ((LgGridP)grid)->impl->beforeDraw(((LgGridP)grid))

#define LgGridAfterDraw(grid) ((LgGridP)grid)->impl->afterDraw(((LgGridP)grid))

#define LgGridEnter(grid) if (((LgGridP)grid)->impl->enter) ((LgGridP)grid)->impl->enter(((LgGridP)grid))

#define LgGridExit(grid) if (((LgGridP)grid)->impl->exit) ((LgGridP)grid)->impl->exit(((LgGridP)grid))

#define LgGridVertexIdx(grid, col, row, anchor) ((LgGridP)grid)->impl->vertexIdx(((LgGridP)grid), col, row, anchor)

void LgGridSize(LgGridP grid, LgSizeP size);

void LgGridCellSize(LgGridP grid, LgVector2P cellSize);

// grid 3d ----------------------------------------------------------------------
LgGridP LgGrid3DCreate(int cols, int rows);

void LgGrid3DSetPositionWithAnchor(LgGridP grid, int col, int row, enum LgGridAnchor anchor, const LgVector3P pt);

void LgGrid3DPositionWithAnchor(LgGridP grid, int col, int row, enum LgGridAnchor anchor, LgVector3P pt);

void LgGrid3DOrgPositionWithAnchor(LgGridP grid, int col, int row, enum LgGridAnchor anchor, LgVector3P pt);

void LgGrid3DSetPosition(LgGridP grid, int x, int y, const LgVector3P pt);

void LgGrid3DPosition(LgGridP grid, int x, int y, LgVector3P pt);

void LgGrid3DOrgPosition(LgGridP grid, int x, int y, LgVector3P pt);

// tilegrid 3d ----------------------------------------------------------------------
LgGridP LgTileGrid3DCreate(int cols, int rows);

void LgTileGrid3DSetPositionWithAnchor(LgGridP grid, int col, int row, enum LgGridAnchor anchor, const LgVector3P pt);

void LgTileGrid3DPositionWithAnchor(LgGridP grid, int col, int row, enum LgGridAnchor anchor, LgVector3P pt);

void LgTileGrid3DOrgPositionWithAnchor(LgGridP grid, int col, int row, enum LgGridAnchor anchor, LgVector3P pt);

#endif