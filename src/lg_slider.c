#include "lg_slider.h"
#include "lg_action.h"

static void sliderEventProc(LgEventType type, LgNodeP node, void *param) {
	LgSliderDataP sd = (LgSliderDataP)LgNodeUserData(node);

	switch(type) {
	case LG_NODE_EVENT_Destroy:
		if (sd->pressAction) LgActionRelease(sd->pressAction);
		if (sd->releaseAction) LgActionRelease(sd->releaseAction);
		break;
	case LG_NODE_EVENT_ClearSlots:
		LgSlotClear(&sd->slotChange);
		break;
	}

	LgControlEventProc(type, node, param);
}

LgNodeP LgSliderCreateR(const LgVector2P size) {
	LgNodeP node = LgControlNodeCreateSubClassR(sizeof(struct LgSliderData), (LgEvent)sliderEventProc, size);
	LgSliderDataP sd = (LgSliderDataP)LgNodeUserData(node);
	struct LgVector2 scale;

	sd->minValue = 0, sd->maxValue = 100, sd->value = 0;

	scale.x = 1.2f, scale.y = 1.2f;
	sd->pressAction = LgActionScaleToCreate(0.1, &scale);

	scale.x = 1.0f, scale.y = 1.0f;
	sd->releaseAction = LgActionScaleToCreate(0.1, &scale);
	return node;
}

LgBool LgNodeIsSlider(LgNodeP node) {
	return LgNodeEventProc(node) == (LgEvent)sliderEventProc;
}

static void updateThumnPosition(LgSliderDataP sd) {
	if (sd->thumbNode) {
		float r = (sd->value - sd->minValue) / (sd->maxValue - sd->minValue);
		struct LgVector2 pos;
		pos.x = sd->base.size.x * r;
		pos.y = sd->base.size.y / 2;
		LgNodeSetPosition(sd->thumbNode, &pos);
	}
}

static void touchSlider(LgNodeP slider, LgSliderDataP sd, LgTouchP touch) {
	struct LgVector2 pos, localPosition;

	LgTouchWorldPosition(touch, &pos);
	LgTouchLocalPosition(touch, &localPosition);
	LgNodePointToLocal(slider, &pos);
	pos.x -= sd->downX;
	if (pos.x < 0) pos.x = 0;
	if (pos.x > sd->base.size.x) pos.x = sd->base.size.x;
	LgSliderSetValue(slider, (pos.x / sd->base.size.x) * (sd->maxValue - sd->minValue) + sd->minValue);
	LgSlotCall(&sd->slotChange, NULL);
}

static void slotThumbTouch(void *param, void *userdata) {
	LgNodeTouchEventP evt = (LgNodeTouchEventP)param;
	LgNodeP slider = (LgNodeP)userdata;
	LgSliderDataP sd = (LgSliderDataP)LgNodeUserData(slider);

	if (evt->type == LgNodeTouchBegan) {
		struct LgVector2 localPosition;
		if (sd->pressAction) LgNodeDoActionAndRelease(sd->thumbNode, LgActionClone(sd->pressAction));
		LgTouchLocalPosition(evt->touches[0], &localPosition);
		sd->downX = localPosition.x;
	} else if (evt->type == LgNodeTouchEnded || evt->type == LgNodeTouchCancelled) {
		if (sd->releaseAction) LgNodeDoActionAndRelease(sd->thumbNode, LgActionClone(sd->releaseAction));
	} else if (evt->type == LgNodeTouchMoved) {
		touchSlider(slider, sd, evt->touches[0]);
	}
}

void LgSliderSetThumb(LgNodeP node, LgNodeP thumb) {
	if (thumb) {
		LgSliderDataP sd = (LgSliderDataP)LgNodeUserData(node);
		if (sd->thumbNode) LgNodeRemove(sd->thumbNode);
		sd->thumbNode = thumb;
		LgNodeAddChild(node, thumb);
		LgNodeSetZorder(thumb, LG_UILAYER_CHILD);
		updateThumnPosition(sd);
		LgNodeEnableHitTest(thumb, LgTrue);
		LgSlotAssign(LgNodeSlotTouch(thumb), slotThumbTouch, NULL, NULL, node);
	}
}

float LgSliderValue(LgNodeP node) {
	LgSliderDataP sd = (LgSliderDataP)LgNodeUserData(node);
	return sd->value;
}

void LgSliderSetValue(LgNodeP node, float value) {
	LgSliderDataP sd = (LgSliderDataP)LgNodeUserData(node);
	value = LgMax(value, sd->minValue);
	value = LgMin(value, sd->maxValue);
	sd->value = sd->isInteger ? (int)value : value;
	updateThumnPosition(sd);
}

void LgSliderSetRange(LgNodeP node, float minValue, float maxValue) {
	LgSliderDataP sd = (LgSliderDataP)LgNodeUserData(node);
	sd->minValue = minValue;
	sd->maxValue = maxValue;
	LgSliderSetValue(node, sd->value);
}

void LgSliderRange(LgNodeP node, float *minValue, float *maxValue) {
	LgSliderDataP sd = (LgSliderDataP)LgNodeUserData(node);
	*minValue = sd->minValue;
	*maxValue = sd->maxValue;
}

void LgSliderSetPressAction(LgNodeP node, LgActionP action) {
	LgSliderDataP sd = (LgSliderDataP)LgNodeUserData(node);
	if (sd->pressAction) LgActionRelease(sd->pressAction);
	sd->pressAction = LgActionRetain(action);
}

void LgSliderSetReleaseAction(LgNodeP node, LgActionP action) {
	LgSliderDataP sd = (LgSliderDataP)LgNodeUserData(node);
	if (sd->releaseAction) LgActionRelease(sd->releaseAction);
	sd->releaseAction = LgActionRetain(action);
}

LgSlot *LgSliderSlotChange(LgNodeP node) {
	LgSliderDataP sd = (LgSliderDataP)LgNodeUserData(node);
	return &sd->slotChange;
}

void LgSliderSetIsInteger(LgNodeP node, LgBool value) {
	LgSliderDataP sd = (LgSliderDataP)LgNodeUserData(node);
	sd->isInteger = value;
}

LgBool LgSliderIsInteger(LgNodeP node) {
	LgSliderDataP sd = (LgSliderDataP)LgNodeUserData(node);
	return sd->isInteger;
}
