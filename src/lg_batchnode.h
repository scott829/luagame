#ifndef _LG_BATCH_NODE_H
#define _LG_BATCH_NODE_H

#include "lg_node.h"

LgNodeP LgBatchNodeCreateR();

LgBool LgNodeIsBatchNode(LgNodeP node);

void LgBatchNodeMakeDirty(LgNodeP node);

#endif