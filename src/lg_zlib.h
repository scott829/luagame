#ifndef _LG_ZLIB_H
#define _LG_ZLIB_H

#include "lg_basetypes.h"
#include "lg_inputstream.h"

typedef struct LgZlib *LgZlibP;

LgZlibP LgZlibCreate(LgBool compress, LgInputStreamP input);

void LgZlibDestroy(LgZlibP zlib);

int LgZlibReadBytes(LgZlibP zlib, void *data, int len);

int LgZlibTotalRead(LgZlibP zlib);

#endif