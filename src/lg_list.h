#ifndef _LG_LIST_H
#define _LG_LIST_H

#include "lg_basetypes.h"

typedef struct LgList * LgListP;

typedef struct LgListNode * LgListNodeP;

LgListP LgListCreate(LgDestroy destroyData);

void LgListDestroy(LgListP list);

int LgListCount(LgListP list);

LgListNodeP LgListInsertBefore(LgListP list, LgListNodeP node, void *data);

LgListNodeP LgListInsertAfter(LgListP list, LgListNodeP node, void *data);

LgListNodeP LgListAppend(LgListP list, void *data);

void LgListRemove(LgListP list, LgListNodeP node);

void LgListClear(LgListP list);

void LgListForeach(LgListP list, LgForeach foreach, void *userdata);

LgListNodeP LgListNextNode(LgListNodeP node);

LgListNodeP LgListPreviousNode(LgListNodeP node);

LgListNodeP LgListFirstNode(LgListP list);

LgListNodeP LgListLastNode(LgListP list);

void *LgListNodeValue(LgListNodeP node);

LgListP LgListClone(LgListP list, LgClone clone);

#endif