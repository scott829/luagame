#ifndef _LG_URL_H_
#define _LG_URL_H_

#include "lg_basetypes.h"
#include "lg_dict.h"

typedef struct LgUrl {
	char *	scheme;
	char *	host;
	int		port;
	char *	path;
	char *	query;
}*LgUrlP;

LgUrlP LgUrlParse(const char *urls);

void LgUrlDestroy(LgUrlP url);

LgDictP LgUrlParseQuery(const char *query);

#endif