#include "lg_gradientlayer.h"
#include "lg_director.h"

typedef struct LgGradientData {
	struct LgLayerData	base;
	LgColor				beginColor, endColor;
	struct LgVector2	v;
	LgVertexBufferP		vb;
}*LgGradientDataP;

static void buildVertexBuffer(LgAabbP box, LgGradientDataP gd) {
	LgVertexDiffuseP p;
	float c = sqrtf(2.0f);
	struct LgVector2 u;
	float br, bg, bb, ba, er, eg, eb, ea, r, g, b, a, h2;

	u.x = gd->v.x / box->width, u.y = gd->v.y / box->height;
	h2 = 1.0f / (fabsf(u.x) + fabsf(u.y));
	LgVector2MulFloat(&u, h2 * c);

	br = LgColorRed(gd->beginColor) / 255.0f;
	bg = LgColorGreen(gd->beginColor) / 255.0f;
	bb = LgColorBlue(gd->beginColor) / 255.0f;
	ba = LgColorAlpha(gd->beginColor) / 255.0f;

	er = LgColorRed(gd->endColor) / 255.0f;
	eg = LgColorGreen(gd->endColor) / 255.0f;
	eb = LgColorBlue(gd->endColor) / 255.0f;
	ea = LgColorAlpha(gd->endColor) / 255.0f;

	gd->vb = LgRenderSystemCreateVertexBuffer(sizeof(struct LgVertexDiffuse) * 4);
	p = (LgVertexDiffuseP)LgVertexBufferLock(gd->vb);

	p->position.x = box->x, p->position.y = box->y + box->height, p->position.z = 0;
	a = ea + (ba - ea) * ((c + u.x - u.y) / (2.0f * c));
	r = er + (br - er) * ((c + u.x - u.y) / (2.0f * c));
	g = eg + (bg - eg) * ((c + u.x - u.y) / (2.0f * c));
	b = eb + (bb - eb) * ((c + u.x - u.y) / (2.0f * c));
	p->color = LgColorARGB_F(a, r, g, b);
	p++;

	p->position.x = box->x, p->position.y = box->y, p->position.z = 0;
	a = ea + (ba - ea) * ((c + u.x + u.y) / (2.0f * c));
	r = er + (br - er) * ((c + u.x + u.y) / (2.0f * c));
	g = eg + (bg - eg) * ((c + u.x + u.y) / (2.0f * c));
	b = eb + (bb - eb) * ((c + u.x + u.y) / (2.0f * c));
	p->color = LgColorARGB_F(a, r, g, b);
	p++;

	p->position.x = box->x + box->width, p->position.y = box->y + box->height, p->position.z = 0;
	a = ea + (ba - ea) * ((c - u.x - u.y) / (2.0f * c));
	r = er + (br - er) * ((c - u.x - u.y) / (2.0f * c));
	g = eg + (bg - eg) * ((c - u.x - u.y) / (2.0f * c));
	b = eb + (bb - eb) * ((c - u.x - u.y) / (2.0f * c));
	p->color = LgColorARGB_F(a, r, g, b);
	p++;

	p->position.x = box->x + box->width, p->position.y = box->y, p->position.z = 0;
	a = ea + (ba - ea) * ((c - u.x + u.y) / (2.0f * c));
	r = er + (br - er) * ((c - u.x + u.y) / (2.0f * c));
	g = eg + (bg - eg) * ((c - u.x + u.y) / (2.0f * c));
	b = eb + (bb - eb) * ((c - u.x + u.y) / (2.0f * c));
	p->color = LgColorARGB_F(a, r, g, b);
	p++;

	LgVertexBufferUnlock(gd->vb);
}

static void gradientLayerEventProc(LgEventType type, LgNodeP node, void *param) {
	switch(type) {
	case LG_NODE_EVENT_Draw:
		{
			LgGradientDataP gd = (LgGradientDataP)LgNodeUserData(node);
			LgRenderSystemSetPixelShader(LgPixelShaderCxformDiffuse);
			LgRenderSystemDrawPrimitive(LgPrimitiveTriangleStrip, LgFvfXYZ | LgFvfDiffuse, gd->vb, 2);
			LgDirectorUpdateBatch(1);
			return;
		}
		break;
	case LG_NODE_EVENT_Enter:
		{
			LgGradientDataP gd = (LgGradientDataP)LgNodeUserData(node);
			struct LgAabb contentBox;

			LgNodeContentBox(node, &contentBox);
			buildVertexBuffer(&contentBox, gd);
		}
		break;
	case LG_NODE_EVENT_Exit:
		{
			LgGradientDataP gd = (LgGradientDataP)LgNodeUserData(node);
			if (gd->vb) {
				LgVertexBufferDestroy(gd->vb);
				gd->vb = NULL;
			}
		}
		break;
	}

	LgLayerEventProc(type, node, param);
}

LgNodeP LgGradientLayerCreateR(LgColor beginColor, LgColor endColor, const LgVector2P v) {
	LgNodeP node = NULL;
	LgGradientDataP gd = NULL;

	node = LgNodeCreateR((LgEvent)gradientLayerEventProc);
	LG_CHECK_ERROR(node);
	
	gd = (LgGradientDataP)LgLayerDataCreate(sizeof(struct LgGradientData));
	LG_CHECK_ERROR(gd);
	gd->beginColor = beginColor;
	gd->endColor = endColor;
	LgCopyMemory(&gd->v, v, sizeof(struct LgVector2));
	LgNodeSetUserData(node, gd);

	return node;

_error:
	if (node) LgNodeRelease(node);
	return NULL;
}

LgBool LgNodeIsGradientLayer(LgNodeP node) {
	return LgNodeEventProc(node) == (LgEvent)gradientLayerEventProc;
}
