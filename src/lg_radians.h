#ifndef _LG_RADIANS_H
#define _LG_RADIANS_H

#include <math.h>

float LgRadiansToAngle(float radians);

float LgAngleToRadians(float angle);

#endif
