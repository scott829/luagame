#ifndef _LG_ARCHIVE_H
#define _LG_ARCHIVE_H

#include "lg_inputstream.h"
#include "lg_list.h"

LgBool LgArchiveOpen();

void LgArchiveClose();

LgBool LgArchiveAddSource(const char *filename, LgBool head);

LgBool LgArchiveRemoveSource(const char *filename);

LgInputStreamP LgArchiveOpenFile(const char *filename);

LgBool LgArchiveNextSource(LgListNodeP *node, const char **filename);

#endif