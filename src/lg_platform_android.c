#include "lg_platform.h"

const char *LgPlatformResourceDirectory() {
	return "/storage/sdcard/";
}

const char *LgPlatformPathSeparator() {
	return "/";
}

const char *LgPlatformDocumentDirectory() {
	return "/storage/sdcard/";
}

const char *LgCurrentPlatform() {
	return "android";
}
