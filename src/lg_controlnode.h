﻿#ifndef _LG_CONTROLNODE_H
#define _LG_CONTROLNODE_H

#include "lg_node.h"

#define LG_UILAYER_BACKGROUND	0

#define LG_UILAYER_CHILD		1

enum {
	LG_CONTROLNODE_EVENT_SetEnable = LG_NODE_EVENT_LAST,
	LG_CONTROLNODE_EVENT_IsEnabled,
	LG_CONTROLNODE_EVENT_SetHighlight,
	LG_CONTROLNODE_EVENT_IsHighlighted,
	LG_CONTROLNODE_EVENT_SetSelect,
	LG_CONTROLNODE_EVENT_IsSelected,
	LG_CONTROLNODE_EVENT_SetSize,
	LG_CONTROLNODE_EVENT_LAST,
};

typedef struct LgControlData {
	struct LgVector2	size;
	LgBool				enabled;
	LgBool				selected;
	LgBool				highlight;
	LgNodeP				backgroundNode;	// 背景必须是sprite或者sprite9
}*LgControlDataP;

void LgControlEventProc(LgEventType type, LgNodeP node, void *param);

LgNodeP LgControlNodeCreateR(const LgVector2P size);

LgNodeP LgControlNodeCreateSubClassR(size_t dataSize, LgEvent eventProc, const LgVector2P size);

LgBool LgNodeIsControlNode(LgNodeP node);

void LgControlNodeSetEnable(LgNodeP node, LgBool enable);

LgBool LgControlNodeIsEnabled(LgNodeP node);

void LgControlNodeSetHighlight(LgNodeP node, LgBool enable);

LgBool LgControlNodeIsHighlighted(LgNodeP node);

void LgControlNodeSetSelect(LgNodeP node, LgBool enable);

LgBool LgControlNodeIsSelected(LgNodeP node);

void LgControlNodeSetSize(LgNodeP node, const LgVector2P size);

void LgControlNodeSetBackground(LgNodeP node, LgNodeP backgroundNode);

#endif