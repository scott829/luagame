#include "lg_scene.h"
#include "lg_director.h"

void LgSceneEventProc(LgEventType type, LgNodeP node, void *param) {
	switch(type) {
	case LG_NODE_EVENT_Create:
		{
			struct LgSize viewSize;
			struct LgAabb aabb;
			struct LgVector2 anchor;

			LgDirectorViewSize(&viewSize);
			aabb.x = aabb.y = 0;
			aabb.width = (float)viewSize.width, aabb.height = (float)viewSize.height;
 
			LgNodeSetTransformAnchor(node, &anchor);
		}
		break;
	case LG_NODE_EVENT_ContentBox:
		{
			struct LgSize viewSize;
			LgAabbP aabb = (LgAabbP)param;
			
			LgDirectorViewSize(&viewSize);
			aabb->x = aabb->y = 0;
			aabb->width = (float)viewSize.width, aabb->height = (float)viewSize.height;
		}
		return;
	}

	LgNodeDefEventProc(type, node, param);
}

LgNodeP LgSceneCreateR() {
	return LgNodeCreateR((LgEvent)LgSceneEventProc);
}

LgBool LgNodeIsScene(LgNodeP node) {
	return LgNodeEventProc(node) == (LgEvent)LgSceneEventProc;
}
