#ifndef _LG_WINDOW_H
#define _LG_WINDOW_H

#include "lg_basetypes.h"

LgBool LgWindowOpen(const LgSizeP size);

void LgWindowClose();

void LgWindowSize(LgSizeP size);

void LgWindowShowKeyboard(LgBool show);

LgBool LgWindowIsHD();

#endif