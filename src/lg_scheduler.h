#ifndef _LG_SCHEDULER_H
#define _LG_SCHEDULER_H

#include "lg_basetypes.h"
#include "lg_time.h"
#include "lg_refcnt.h"

#define LG_LOOP_FOREVER -1

typedef struct LgSchedulerItem *LgSchedulerItemP;

LG_REFCNT_FUNCTIONS_DECL(LgSchedulerItem);

LgBool LgSchedulerOpen();

void LgSchedulerClose();

LgSchedulerItemP LgSchedulerRun(LgStep step, LgDestroy destroyUserdata, void *userdata, LgTime interval, int repeat, LgTime delay, LgBool paused);

void LgSchedulerUpdate();

void LgSchedulerPause(LgSchedulerItemP item);

void LgSchedulerResume(LgSchedulerItemP item);

void LgSchedulerRemove(LgSchedulerItemP item);

void LgSchedulerClear();

void LgSchedulerReleaseAtNextFrame(void *obj);

int LgSchedulerCount();

#endif