#include "lg_shapes.h"

#define TRIANGLE_LEN 1024

LgVertexBufferP g_rectangleTexcoord = NULL;
LgVertexBufferP g_rectangleDiffuse = NULL;
LgVertexBufferP g_diffuseTexCroodTriangleList = NULL;

static LgBool initRectangleTexcoord() {
	struct LgVertexTexCrood vertexs[4];

	vertexs[0].position.x = 0, vertexs[0].position.y = 1, vertexs[0].position.z = 0, 
		vertexs[0].texcrood.x = 0, vertexs[0].texcrood.y = 1;
	vertexs[1].position.x = 0, vertexs[1].position.y = 0, vertexs[1].position.z = 0, 
		vertexs[1].texcrood.x = 0, vertexs[1].texcrood.y = 0;
	vertexs[2].position.x = 1, vertexs[2].position.y = 1, vertexs[2].position.z = 0, 
		vertexs[2].texcrood.x = 1, vertexs[2].texcrood.y = 1;
	vertexs[3].position.x = 1, vertexs[3].position.y = 0, vertexs[3].position.z = 0, 
		vertexs[3].texcrood.x = 1, vertexs[3].texcrood.y = 0;

	g_rectangleTexcoord = LgRenderSystemCreateVertexBuffer(sizeof(vertexs));
	if (!g_rectangleTexcoord) return LgFalse;

	LgCopyMemory(LgVertexBufferLock(g_rectangleTexcoord), vertexs, sizeof(vertexs));
	LgVertexBufferUnlock(g_rectangleTexcoord);
	return LgTrue;
}

static LgBool initRectangleDiffuse() {
	struct LgVertexDiffuse vertexs[4];

	vertexs[0].position.x = 0, vertexs[0].position.y = 1, vertexs[0].position.z = 0, 
		vertexs[0].color = 0;
	vertexs[1].position.x = 0, vertexs[1].position.y = 0, vertexs[1].position.z = 0, 
		vertexs[1].color = 0;
	vertexs[2].position.x = 1, vertexs[2].position.y = 1, vertexs[2].position.z = 0, 
		vertexs[2].color = 0;
	vertexs[3].position.x = 1, vertexs[3].position.y = 0, vertexs[3].position.z = 0, 
		vertexs[3].color = 0;

	g_rectangleDiffuse = LgRenderSystemCreateVertexBuffer(sizeof(vertexs));
	if (!g_rectangleDiffuse) return LgFalse;

	LgCopyMemory(LgVertexBufferLock(g_rectangleDiffuse), vertexs, sizeof(vertexs));
	LgVertexBufferUnlock(g_rectangleDiffuse);
	return LgTrue;
}

static LgBool initDiffuseTexCroodTriangleList() {
	g_diffuseTexCroodTriangleList = LgRenderSystemCreateVertexBuffer(TRIANGLE_LEN * sizeof(struct LgVertexDiffuseTexCrood) * 3);
	return g_diffuseTexCroodTriangleList != NULL;
}

LgBool LgShapesOpen() {
	LG_CHECK_ERROR(
		initRectangleTexcoord() && 
		initRectangleDiffuse() &&
		initDiffuseTexCroodTriangleList());
	return LgTrue;

_error:
	LgShapesClose();
	return LgFalse;
}

void LgShapesClose() {
	if (g_rectangleTexcoord) {
		LgVertexBufferDestroy(g_rectangleTexcoord);
		g_rectangleTexcoord = NULL;
	}
	if (g_rectangleDiffuse) {
		LgVertexBufferDestroy(g_rectangleDiffuse);
		g_rectangleDiffuse = NULL;
	}
	if (g_diffuseTexCroodTriangleList) {
		LgVertexBufferDestroy(g_diffuseTexCroodTriangleList);
		g_diffuseTexCroodTriangleList = NULL;
	}
}

LgVertexBufferP LgShapesRectangleTexcoord() {
	return g_rectangleTexcoord;
}

LgVertexBufferP LgShapesRectangleDiffuse() {
	return g_rectangleDiffuse;
}

LgVertexBufferP LgShapesDiffuseTexCroodTriangleList() {
	return g_diffuseTexCroodTriangleList;
}

int LgShapesDiffuseTexCroodTriangleListCount() {
	return TRIANGLE_LEN;
}
