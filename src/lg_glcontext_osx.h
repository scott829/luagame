#ifndef _GLCONTEXT_OSX_H_
#define _GLCONTEXT_OSX_H_

#include "lg_basetypes.h"
#include <OpenGL/OpenGL.h>
#include <OpenGL/gl.h>
#include <OpenGL/glext.h>

// gl context ---------------------------------------------------------------------------------------------------

LgBool LgGlContextOpen();

void LgGlContextClose();

void LgGlContextBegin();

void LgGlContextEnd();

#endif