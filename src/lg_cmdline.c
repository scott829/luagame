﻿#include "lg_cmdline.h"
#include "lg_memory.h"
#include <string.h>

LgCommandLine g_commandLine;

static void setDefaultCommandLine() {
	g_commandLine.resourceDirectory[0] = 0;
	g_commandLine.debug = LgFalse;
	g_commandLine.debugPort = 39598;
}

void LgParseCommandLine(int argc, char **argv) {
	int i;

	setDefaultCommandLine();

#define IS(_name) (strcmp(name, _name) == 0)

	for (i = 0; i < argc; ) {
		const char *name = argv[i];
		const char *arg = argv[i + 1];
		LgBool hasArg = LgFalse;

		if (IS("--resource") && arg) {
			hasArg = LgTrue;
			strcpy(g_commandLine.resourceDirectory, arg);
		} else if (IS("--debug")) {
			g_commandLine.debug = LgTrue;
		} else if (IS("debug-port") && arg) {
			hasArg = LgTrue;
			g_commandLine.debugPort = atoi(arg);
		}

		i += (hasArg ? 2 : 1);
	}
}