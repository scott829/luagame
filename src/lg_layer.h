#ifndef _LG_LAYER_H
#define _LG_LAYER_H

#include "lg_node.h"

typedef struct LgLayerData {
	LgSlot slotKeyDown, slotKeyUp;
}*LgLayerDataP;

enum {
	LG_LAYER_EVENT_KeyDownEvent = LG_NODE_EVENT_LAST,
	LG_LAYER_EVENT_KeyUpEvent,
	LG_LAYER_EVENT_LAST,
};

LgBool LgNodeIsBaseLayer(LgNodeP node);

LgSlot *LgLayerSlotKeyDown(LgNodeP node);

LgSlot *LgLayerSlotKeyUp(LgNodeP node);

void LgLayerDataDestroy(LgLayerDataP data);

LgLayerDataP LgLayerDataCreate(size_t size);

LgNodeP LgLayerCreateR();

void LgLayerEventProc(LgEventType type, LgNodeP node, void *param);

#endif