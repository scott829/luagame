#ifndef _LG_SCRIPTENV_H
#define _LG_SCRIPTENV_H

#include "lua/lua.h"
#include "lua/lauxlib.h"
#include "lg_basetypes.h"
#include "lg_log.h"

LgBool LgScriptEnvOpen();

void LgScriptEnvClose();

lua_State *LgScriptEnvState();

LgBool LgScriptEnvRunFile(const char *filename);

#define LgScriptShowError() LgLogError("lua: %s", luaL_checkstring(LgScriptEnvState(), -1))

#endif