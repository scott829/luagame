#ifndef _LG_JPGLOADER_H
#define _LG_JPGLOADER_H

#include "lg_inputstream.h"
#include "lg_image.h"

LgBool jpgCheckType(const unsigned char *data, int size);

LgImageP jpgLoad(LgInputStreamP inputStream);

#endif