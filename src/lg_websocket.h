#ifndef _LG_WEBSOCKET_H
#define _LG_WEBSOCKET_H

#include "lg_basetypes.h"
#include "lg_refcnt.h"

enum LgWebSocketState {
	LgWebSocketStateDisconnected,
	LgWebSocketStateConnecting,
	LgWebSocketStateHandshake,
	LgWebSocketStateReadingHandshakeHeaders,
	LgWebSocketStateConnected,
};

enum LgWebSocketErrorType {
	LgWebSocketErrorNone,
	LgWebSocketErrorTimeout,
	LgWebSocketErrorClose,
	LgWebSocketErrorReadData,
	LgWebSocketErrorBadResponse,
	LgWebSocketErrorBadRequest,
	LgWebSocketErrorNotFound,
	LgWebSocketErrorForbidden,
};

enum LgWebSocketFrameOpCode {
	LgWebSocketFrameContinue = 0x0,
	LgWebSocketFrameText = 0x1,
	LgWebSocketFrameBinary = 0x2,
	LgWebSocketFrameClose = 0x8,
	LgWebSocketFramePing = 0x9,
	LgWebSocketFramePong = 0xa,
};

typedef struct LgWebSocketChangedEvent {
	enum LgWebSocketState oldState, newState;
}*LgWebSocketChangedEventP;

typedef struct LgWebSocketMessageEvent {
	const char *data;
	int len;
}*LgWebSocketMessageEventP;

typedef struct LgWebSocket *	LgWebSocketP;

LgWebSocketP LgWebSocketCreateR();

LgBool LgWebSocketOpen(LgWebSocketP ws, const char *url);

void LgWebSocketClose(LgWebSocketP ws);

void LgWebSocketSendMessage(LgWebSocketP ws, const char *data, size_t len, enum LgWebSocketFrameOpCode type);

LgSlot *LgWebSocketSlotOpen(LgWebSocketP ws);

LgSlot *LgWebSocketSlotMessage(LgWebSocketP ws);

LgSlot *LgWebSocketSlotError(LgWebSocketP ws);

#endif