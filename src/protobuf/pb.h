#ifndef _PB_H_
#define _PB_H_

/* pb.h: protobuf api for pomelo c client library.
 */

#define PB_VERSION 1.1.1

#include <stdint.h>
#include <stddef.h>
#include "../jansson/jansson.h"

typedef struct _pb_istream_t pb_istream_t;

/* Lightweight input stream.
 * You can provide a callback function for reading or use
 * pb_istream_from_buffer.
 *
 * Rules for callback:
 * 1) Return 0 on IO errors. This will cause decoding to abort.
 *
 * 2) You can use state to store your own data (e.g. buffer pointer),
 * and rely on pb_read to verify that no-body reads past bytes_left.
 *
 * 3) Your callback may be used with substreams, in which case bytes_left
 * is different than from the main stream. Don't use bytes_left to compute
 * any pointers.
 */
struct _pb_istream_t {
    int (*callback)(pb_istream_t *stream, uint8_t *buf, size_t count);
    void *state; /* Free field for use by callback implementation */
    size_t bytes_left;

#ifndef PB_NO_ERRMSG
    const char *errmsg;
#endif
};

typedef struct _pb_ostream_t pb_ostream_t;

/* Lightweight output stream.
 * You can provide callback for writing or use pb_ostream_from_buffer.
 *
 * Alternatively, callback can be NULL in which case the stream will just
 * count the number of bytes that would have been written. In this case
 * max_size is not checked.
 *
 * Rules for callback:
 * 1) Return 0 on IO errors. This will cause encoding to abort.
 *
 * 2) You can use state to store your own data (e.g. buffer pointer).
 *
 * 3) pb_write will update bytes_written after your callback runs.
 *
 * 4) Substreams will modify max_size and bytes_written. Don't use them to
 * calculate any pointers.
 */
struct _pb_ostream_t {
    int (*callback)(pb_ostream_t *stream, const uint8_t *buf, size_t count);
    void *state; /* Free field for use by callback implementation */
    size_t max_size; /* Limit number of output bytes written (or use SIZE_MAX). */
    size_t bytes_written;
};

/*
 * protobuf encode
 */
int pc_pb_encode(uint8_t *buf, size_t len, size_t *written,
                 const json_t *gprotos, const json_t *protos, json_t *msg);

int pb_encode(pb_ostream_t *stream, const json_t *gprotos, const json_t *protos, json_t *msg);

/*
 * protobuf decode
 */
int pc_pb_decode(uint8_t *buf, size_t len, const json_t *gprotos,
                 const json_t *protos, json_t *result);

#endif