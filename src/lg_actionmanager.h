#ifndef _LG_ACTIONMANAGER_H
#define _LG_ACTIONMANAGER_H

#include "lg_action.h"

LgBool LgActionManagerOpen();

void LgActionManagerClose();

void LgActionManagerAdd(LgNodeP node, LgActionP action);

void LgActionManagerPause(LgNodeP node);

void LgActionManagerResume(LgNodeP node);

void LgActionManagerStop(LgNodeP node, LgActionP action);

int LgActionManagerActionsCount(LgNodeP node);

#endif