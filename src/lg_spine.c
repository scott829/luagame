#include "lg_rendersystem.h"
#include "lg_archive.h"
#include "spine/spine.h"

void _spAtlasPage_createTexture (spAtlasPage* self, const char* path) {
	LgTextureP texture = LgTextureCreateFromFileR(path);
	struct LgSize size = {0, 0};

	if (texture) LgTextureSize(texture, &size);
	self->rendererObject = texture;
	self->width = size.width;
	self->height = size.height;
}

void _spAtlasPage_disposeTexture (spAtlasPage* self) {
	LgTextureRelease((LgTextureP)self->rendererObject);
}

char* _spUtil_readFile (const char* path, int* length) {
	LgInputStreamP is = LgArchiveOpenFile(path);
	char *ret;

	if (!is) {
		*length = 0;
		return NULL;
	}

	ret = (char *)LgInputStreamReadAllBytes(is, length);
	LgInputStreamDestroy(is);
	return ret;
}