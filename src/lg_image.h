#ifndef _LG_IMAGE_H
#define _LG_IMAGE_H

#include "lg_basetypes.h"
#include "lg_refcnt.h"
#include "lg_inputstream.h"

enum LgImageFormat_ {
	LgImageFormatL8,
	LgImageFormatA8L8,
	LgImageFormatA8R8G8B8,
};

typedef struct LgImage *LgImageP;

LG_REFCNT_FUNCTIONS_DECL(LgImage)

LgImageP LgImageCreateR(const LgSizeP size, enum LgImageFormat_ format);

int LgImagePixelSize(enum LgImageFormat_ format);

void *LgImageData(LgImageP image);

void LgImageSize(LgImageP image, LgSizeP size);

enum LgImageFormat_ LgImageFormat(LgImageP image);

int LgImageLineSize(LgImageP image);

void *LgImageLineData(LgImageP image, int line);

LgImageP LgImageCreateFromStreamR(LgInputStreamP input);

LgImageP LgImageCreateFromFileR(const char *filename);

#endif