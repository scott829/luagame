﻿#include "lg_luaapi.h"
#include "lg_scriptenv.h"
#include "lg_node.h"
#include "lg_action.h"
#include "lg_director.h"
#include "lg_sprite.h"
#include "lg_scene.h"
#include "lg_colorlayer.h"
#include "lg_gradientlayer.h"
#include "lg_label.h"
#include "lg_text_field.h"
#include "lg_drawnode.h"
#include "lg_clipnode.h"
#include "lg_action.h"
#include "lg_tilemaplayer.h"
#include "lg_scheduler.h"
#include "lg_soundplayer.h"
#include "lg_url.h"
#include "lg_http.h"
#include "lg_fontdriver.h"
#include "lg_archive.h"
#include "lg_log.h"
#include <uriparser/Uri.h>
#include "lg_platform.h"
#include "lg_init.h"
#include "lg_transition.h"
#include "lg_particle_system.h"
#include "lg_resourcemanager.h"
#include "lg_netclient.h"
#include "lg_gesture_recognizer.h"
#include "lg_payment.h"
#include "lg_batchnode.h"
#include "lg_animation.h"
#include "lg_controlnode.h"
#include "lg_button.h"
#include "lg_scrollview.h"
#include "lg_slider.h"
#include "lg_switcher.h"

#define checkObject LgLuaCheckObject

static void *pushUserdata(lua_State *L, size_t size, const char *tname) {
	void *ret = lua_newuserdata(L, size);
	luaL_setmetatable(L, tname);
	return ret;
}

LgBool LgLuaIsObject(lua_State *L, int idx, const char *base) {
	if (lua_isuserdata(L, idx)) {
		if (lua_getmetatable(L, idx)) {
			const char *next = NULL;

			luaL_getmetatable(L, base);
			if (lua_rawequal(L, -1, -2)) {
				lua_pop(L, 2);
				return LgTrue;
			}

			lua_pop(L, 1); // metatable of base

			while(LgTrue) {
				lua_getfield(L, -1, "__basemetatable");
				next = (const char *)lua_touserdata(L, -1);
				if (!next) {
					lua_pop(L, 2); // metatable, __basemetatable
					return LgFalse;
				} else {
					if (strcmp(next, base) == 0) {
						lua_pop(L, 2);
						return LgTrue;
					} else if (next) {
						luaL_getmetatable(L, next);
						lua_remove(L, -2);
						lua_remove(L, -2);
					}
				}
			}
		}
	}
	
	return LgFalse;
}

static void *getObject(lua_State *L, int idx) {
	if (lua_isuserdata(L, idx)) {
		return (void *)lua_touserdata(L, idx);
	} else {
		return NULL;
	}
}

// 警告，idx不允许是一个相对位置
void *LgLuaCheckObject(lua_State *L, int idx, const char *tname) {
	if (LgLuaIsObject(L, idx, tname)) {
		return getObject(L, idx);
	} else {
		const char *msg = lua_pushfstring(L, "%s expected, got %s",
			tname, luaL_typename(L, -1));
		luaL_argerror(L, -1, msg);
		return NULL;
	}
}

static void pushNode(lua_State *L, LgNodeP node, const char *metatable) {
	if (node) {
		LgNodeP *ret = (LgNodeP *)lua_newuserdata(L, sizeof(LgNodeP));
		*ret = node;
		if (metatable) {
			luaL_setmetatable(L, metatable);
		} else {
			// 自动判断类型
			if (LgNodeIsScene(node)) luaL_setmetatable(L, LGSCENE_HANDLE);
			else if (LgNodeIsBaseLayer(node)) luaL_setmetatable(L, LGLAYER_HANDLE);
			else if (LgNodeIsColorLayer(node)) luaL_setmetatable(L, LGCOLORLAYER_HANDLE);
			else if (LgNodeIsGradientLayer(node)) luaL_setmetatable(L, LGGRADIENTLAYER_HANDLE);
			else if (LgNodeIsTileMapLayer(node)) luaL_setmetatable(L, LGTILEMAPLAYER_HANDLE);
			else if (LgNodeIsSprite(node)) luaL_setmetatable(L, LGSPRITE_HANDLE);
			else if (LgNodeIsLabel(node)) luaL_setmetatable(L, LGLABEL_HANDLE);
			else if (LgNodeIsTextField(node)) luaL_setmetatable(L, LGTEXTFIELD_HANDLE);
			else if (LgNodeIsDraw(node)) luaL_setmetatable(L, LGDRAWNODE_HANDLE);
			else if (LgNodeIsClip(node)) luaL_setmetatable(L, LGCLIPNODE_HANDLE);
			else if (LgNodeIsParticleSystem(node)) luaL_setmetatable(L, LGPARTICLESYSTEM_HANDLE);
			else if (LgNodeIsBatchNode(node)) luaL_setmetatable(L, LGBATCHNODE_HANDLE);
			else if (LgNodeIsAnimation(node)) luaL_setmetatable(L, LGANIMATION_HANDLE);

			else if (LgNodeIsControlNode(node)) luaL_setmetatable(L, LGCONTROLNODE_HANDLE);
			else if (LgNodeIsButton(node)) luaL_setmetatable(L, LGBUTTON_HANDLE);
			else if (LgNodeIsScrollView(node)) luaL_setmetatable(L, LGSCROLLVIEW_HANDLE);
			else if (LgNodeIsSlider(node)) luaL_setmetatable(L, LGSLIDER_HANDLE);
			else if (LgNodeIsSwitcher(node)) luaL_setmetatable(L, LGSLIDER_HANDLE);

			else luaL_setmetatable(L, LGNODE_HANDLE);
		}
	} else {
		lua_pushnil(L);
	}
}

static void pushAction(lua_State *L, LgActionP action, const char *metatable) {
	if (action) {
		LgActionP *ret = (LgActionP *)lua_newuserdata(L, sizeof(LgActionP));
		*ret = action;
		luaL_setmetatable(L, metatable);
	} else {
		lua_pushnil(L);
	}
}

static void pushCJSON(lua_State *L, json_t *setting) {
	if (!setting) {
		lua_pushnil(L);
		return;
	}

	switch(json_typeof(setting)) {
	case JSON_INTEGER:
		lua_pushinteger(L, (lua_Integer)json_integer_value(setting));
		break;
	case JSON_REAL:
		lua_pushnumber(L, (lua_Number)json_number_value(setting));
		break;
	case JSON_STRING:
		lua_pushstring(L, json_string_value(setting));
		break;
	case JSON_NULL:
		lua_pushnil(L);
		break;
	case JSON_TRUE:
		lua_pushboolean(L, 1);
		break;
	case JSON_FALSE:
		lua_pushboolean(L, 0);
		break;
	default:
		break;
	}
}

static void createMetaTable(lua_State *L, const char *name, const char *basemetatable, const luaL_Reg *funcs) {
	luaL_newmetatable(L, name);
	lua_pushvalue(L, -1);
	lua_setfield(L, -2, "__index");

	if (funcs) {
		luaL_setfuncs(L, funcs, 0);
		lua_pushlightuserdata(L, (void *)funcs);
		lua_setfield(L, -2, "__cfuncs");
	}

	if (basemetatable) {
		const char *pbase = basemetatable;

		lua_pushlightuserdata(L, (void *)name);
		lua_setfield(L, -2, "__class");

		lua_pushlightuserdata(L, (void *)basemetatable);
		lua_setfield(L, -2, "__basemetatable");

		while(pbase) {
			const char *next = NULL;
			const luaL_Reg *baseFuncs;

			luaL_getmetatable(L, pbase);
			lua_getfield(L, -1, "__basemetatable");
			next = (const char *)lua_touserdata(L, -1);
			lua_pop(L, 1);

			lua_getfield(L, -1, "__cfuncs");
			baseFuncs = (const luaL_Reg *)lua_touserdata(L, -1);
			lua_pop(L, 2);
			luaL_setfuncs(L, baseFuncs, 0);

			pbase = next;
		}
	}
	
	lua_pop(L, 1);
}

static void defineEnum(lua_State *L, const char *name, ...) {
	va_list va;
	
	lua_newtable(L);
	lua_pushvalue(L, -1);
	lua_setfield(L, -3, name);
	
	va_start(va, name);

	while(LgTrue) {
		int evalue;
		const char *ename = va_arg(va, const char *);
		if (!ename) break;
		evalue = va_arg(va, int);

		lua_pushinteger(L, evalue);
		lua_setfield(L, -2, ename);
	}
	
	va_end(va);
	lua_pop(L, 1);
}

static void destroyLuaRef(void *p) {
	luaL_unref(LgScriptEnvState(), LUA_REGISTRYINDEX, (int)p);
}

static void *slotCloneLuaRef(void *p) {
	lua_rawgeti(LgScriptEnvState(), LUA_REGISTRYINDEX, (int)p);
	return (void *)luaL_ref(LgScriptEnvState(), LUA_REGISTRYINDEX);
}

static void slotCallFunction(void *param, void *userdata) {
	if ((int)userdata) {
		lua_rawgeti(LgScriptEnvState(), LUA_REGISTRYINDEX, (int)userdata);
		if (lua_pcall(LgScriptEnvState(), 0, 0, 0)) {
			LgScriptShowError();
		}
	}
}

static void slotFireKeyEvent(void *param, void *userdata) {
	LgKeyEventP *keyevent;

	lua_rawgeti(LgScriptEnvState(), LUA_REGISTRYINDEX, (int)userdata);
	keyevent = (LgKeyEventP *)pushUserdata(LgScriptEnvState(), sizeof(LgKeyEventP), LGKEYEVENT_HANDLE);
	*keyevent = (LgKeyEventP)param;
	if (lua_pcall(LgScriptEnvState(), 1, 0, 0)) {
		LgScriptShowError();
	}
}

// color metatable ------------------------------------------------------------------------------------------------
static int color__eq(lua_State *L) {
	LgColor color1 = *((LgColor *)checkObject(L, 1, LGCOLOR_HANDLE));
	LgColor color2 = *((LgColor *)checkObject(L, 2, LGCOLOR_HANDLE));
	lua_pushboolean(L, color1 == color2);
	return 1;
}

static int color_components(lua_State *L) {
	LgColor color = *((LgColor *)checkObject(L, 1, LGCOLOR_HANDLE));
	lua_pushnumber(L, LgColorRedF(color));
	lua_pushnumber(L, LgColorGreenF(color));
	lua_pushnumber(L, LgColorBlueF(color));
	lua_pushnumber(L, LgColorAlphaF(color));
	return 4;
}

static int color_clone(lua_State *L) {
	LgColor color = *((LgColor *)checkObject(L, 1, LGCOLOR_HANDLE));
	LgColor *newcolor = (LgColor *)pushUserdata(L, sizeof(LgColor), LGCOLOR_HANDLE);
	*newcolor = color;
	return 1;
}

static int color__index(lua_State *L) {
	LgColor color = *((LgColor *)checkObject(L, 1, LGCOLOR_HANDLE));
	const char *name = luaL_checkstring(L, 2);

	if (strlen(name) == 1) {
		switch(name[0]) {
		case 'r': lua_pushnumber(L, LgColorRedF(color)); break;
		case 'g': lua_pushnumber(L, LgColorGreenF(color)); break;
		case 'b': lua_pushnumber(L, LgColorBlueF(color)); break;
		case 'a': lua_pushnumber(L, LgColorAlphaF(color)); break;
		default: lua_pushnil(L);
		}
	} else {
		lua_getmetatable(L, -2);
		lua_pushvalue(L, -2);
		lua_gettable(L, -2);
	}

	return 1;
}

static int color__newindex(lua_State *L) {
	LgColor *color = (LgColor *)checkObject(L, 1, LGCOLOR_HANDLE);
	const char *name = luaL_checkstring(L, 2);
	lua_Number value = luaL_checknumber(L, 3);

	if (strlen(name) == 1) {
		switch(name[0]) {
		case 'r': *color = LgColorARGB_F(LgColorAlphaF(*color), (float)value, LgColorGreenF(*color), LgColorBlueF(*color)); break;
		case 'g': *color = LgColorARGB_F(LgColorAlphaF(*color), LgColorRedF(*color), (float)value, LgColorBlueF(*color)); break;
		case 'b': *color = LgColorARGB_F(LgColorAlphaF(*color), LgColorRedF(*color), LgColorGreenF(*color), (float)value); break;
		case 'a': *color = LgColorARGB_F((float)value, LgColorRedF(*color), LgColorGreenF(*color), LgColorBlueF(*color)); break;
		}
	}

	return 0;
}

static const luaL_Reg color_methods[] = {
	{"__eq", color__eq},
	{"__index", color__index},
	{"__newindex", color__newindex},
	{"components", color_components},
	{"clone", color_clone},
	{NULL, NULL}
};

// color ------------------------------------------------------------------------------------------------
static int _rgb(lua_State *L) {
	LgColor *color = (LgColor *)pushUserdata(L, sizeof(LgColor), LGCOLOR_HANDLE);

	float r, g, b;
	r = (float)luaL_checknumber(L, 1);
	g = (float)luaL_checknumber(L, 2);
	b = (float)luaL_checknumber(L, 3);
	*color = LgColorXRGB_F(r, g, b);
	return 1;
}

static int _argb(lua_State *L) {
	LgColor *color = (LgColor *)pushUserdata(L, sizeof(LgColor), LGCOLOR_HANDLE);
	
	float r, g, b, a;
	a = (float)luaL_checknumber(L, 1);
	r = (float)luaL_checknumber(L, 2);
	g = (float)luaL_checknumber(L, 3);
	b = (float)luaL_checknumber(L, 4);
	*color = LgColorARGB_F(a, r, g, b);
	return 1;
}

// cxform metatable ------------------------------------------------------------------------------------------------
static int cxform__eq(lua_State *L) {
	LgCxformP lhs = (LgCxformP)checkObject(L, 1, LGCXFORM_HANDLE),
		rhs = (LgCxformP)checkObject(L, 2, LGCXFORM_HANDLE);
	lua_pushboolean(L, 
		(LgFloatEqual(lhs->mul[0],rhs->mul[0]) &&
		LgFloatEqual(lhs->mul[1],rhs->mul[1]) &&
		LgFloatEqual(lhs->mul[2],rhs->mul[2]) &&
		LgFloatEqual(lhs->mul[3],rhs->mul[3]) &&
		LgFloatEqual(lhs->add[0],rhs->add[0]) &&
		LgFloatEqual(lhs->add[1],rhs->add[1]) &&
		LgFloatEqual(lhs->add[2],rhs->add[2]) &&
		LgFloatEqual(lhs->add[3],rhs->add[3]))
		);
	return 1;
}

static int cxform__mul(lua_State *L) {
	LgCxformP lhs = (LgCxformP)checkObject(L, 1, LGCXFORM_HANDLE),
		rhs = (LgCxformP)checkObject(L, 2, LGCXFORM_HANDLE);
	LgCxformP result = (LgCxformP)pushUserdata(L, sizeof(struct LgCxform), LGCXFORM_HANDLE);
	LgCopyMemory(result, lhs, sizeof(struct LgCxform));
	LgCxformMul(result, rhs);
	return 1;
}

static int cxform__index(lua_State *L) {
	LgCxformP cxform = (LgCxformP)checkObject(L, 1, LGCXFORM_HANDLE);
	const char *name = luaL_checkstring(L, 2);

	if (strlen(name) == 2) {
		switch(name[0]) {
		case 'm':
			switch(name[1]) {
			case 'a': lua_pushnumber(L, cxform->mul[LGCXFORM_ALPHA]); break;
			case 'r': lua_pushnumber(L, cxform->mul[LGCXFORM_RED]); break;
			case 'g': lua_pushnumber(L, cxform->mul[LGCXFORM_GREEN]); break;
			case 'b': lua_pushnumber(L, cxform->mul[LGCXFORM_BLUE]); break;
			default: lua_pushnil(L);
			}
			break;
		case 'a':
			switch(name[1]) {
			case 'a': lua_pushnumber(L, cxform->add[LGCXFORM_ALPHA]); break;
			case 'r': lua_pushnumber(L, cxform->add[LGCXFORM_RED]); break;
			case 'g': lua_pushnumber(L, cxform->add[LGCXFORM_GREEN]); break;
			case 'b': lua_pushnumber(L, cxform->add[LGCXFORM_BLUE]); break;
			default: lua_pushnil(L);
			}
			break;
		default: lua_pushnil(L);
		}
	} else {
		lua_getmetatable(L, -2);
		lua_pushvalue(L, -2);
		lua_gettable(L, -2);
	}

	return 1;
}

static int cxform__newindex(lua_State *L) {
	LgCxformP cxform = (LgCxformP)checkObject(L, 1, LGCXFORM_HANDLE);
	const char *name = luaL_checkstring(L, 2);
	lua_Number value = luaL_checknumber(L, 3);

	if (strlen(name) == 2) {
		switch(name[0]) {
		case 'm':
			switch(name[1]) {
			case 'a': cxform->mul[LGCXFORM_ALPHA] = (float)value; break;
			case 'r': cxform->mul[LGCXFORM_RED] = (float)value; break;
			case 'g': cxform->mul[LGCXFORM_GREEN] = (float)value; break;
			case 'b': cxform->mul[LGCXFORM_BLUE] = (float)value; break;
			}
			break;
		case 'a':
			switch(name[1]) {
			case 'a': cxform->add[LGCXFORM_ALPHA] = (float)value; break;
			case 'r': cxform->add[LGCXFORM_RED] = (float)value; break;
			case 'g': cxform->add[LGCXFORM_GREEN] = (float)value; break;
			case 'b': cxform->add[LGCXFORM_BLUE] = (float)value; break;
			}
			break;
		}
	}

	return 0;
}

static int cxform_multiply(lua_State *L) {
	LgCxformP cxform = (LgCxformP)checkObject(L, 1, LGCXFORM_HANDLE);
	LgColor *mul = (LgColor *)pushUserdata(L, sizeof(LgColor), LGCOLOR_HANDLE);
	*mul = LgColorARGB_F(cxform->mul[3], cxform->mul[0], cxform->mul[1], cxform->mul[2]);
	return 1;
}

static int cxform_addition(lua_State *L) {
	LgCxformP cxform = (LgCxformP)checkObject(L, 1, LGCXFORM_HANDLE);
	LgColor *add = (LgColor *)pushUserdata(L, sizeof(LgColor), LGCOLOR_HANDLE);
	*add = LgColorARGB_F(cxform->add[3], cxform->add[0], cxform->add[1], cxform->add[2]);
	return 1;
}

static int cxform_set_multiply(lua_State *L) {
	LgCxformP cxform = (LgCxformP)checkObject(L, 1, LGCXFORM_HANDLE);
	LgColor color = *((LgColor *)checkObject(L, 2, LGCOLOR_HANDLE));
	cxform->mul[3] = LgColorAlpha(color) / 255.0f;
	cxform->mul[0] = LgColorRed(color) / 255.0f;
	cxform->mul[1] = LgColorGreen(color) / 255.0f;
	cxform->mul[2] = LgColorBlue(color) / 255.0f;
	return 1;
}

static int cxform_set_addition(lua_State *L) {
	LgCxformP cxform = (LgCxformP)checkObject(L, 1, LGCXFORM_HANDLE);
	LgColor color = *((LgColor *)checkObject(L, 2, LGCOLOR_HANDLE));
	cxform->add[3] = LgColorAlpha(color) / 255.0f;
	cxform->add[0] = LgColorRed(color) / 255.0f;
	cxform->add[1] = LgColorGreen(color) / 255.0f;
	cxform->add[2] = LgColorBlue(color) / 255.0f;
	return 1;
}

static int cxform_clone(lua_State *L) {
	LgCxformP cxform = (LgCxformP)checkObject(L, 1, LGCXFORM_HANDLE);
	LgCxformP newcxform = (LgCxformP)pushUserdata(L, sizeof(struct LgCxform), LGCXFORM_HANDLE);
	LgCopyMemory(newcxform, cxform, sizeof(struct LgCxform));
	return 1;
}

static const luaL_Reg cxform_methods[] = {
	{"__eq", cxform__eq},
	{"__mul", cxform__mul},
	{"__index", cxform__index},
	{"__newindex", cxform__newindex},
	{"multiply", cxform_multiply},
	{"addition", cxform_addition},
	{"set_multiply", cxform_set_multiply},
	{"set_addition", cxform_set_addition},
	{"clone", cxform_clone},
	{NULL, NULL}
};

// cxform ------------------------------------------------------------------------------------------------
static int cxform(lua_State *L) {
	if (lua_gettop(L) == 8) {
		LgCxformP cxform = (LgCxformP)pushUserdata(L, sizeof(struct LgCxform), LGCXFORM_HANDLE);
		cxform->mul[0] = (float)luaL_checknumber(L, 2);
		cxform->mul[1] = (float)luaL_checknumber(L, 3);
		cxform->mul[2] = (float)luaL_checknumber(L, 4);
		cxform->mul[3] = (float)luaL_checknumber(L, 1);
		cxform->add[0] = (float)luaL_checknumber(L, 6);
		cxform->add[1] = (float)luaL_checknumber(L, 7);
		cxform->add[2] = (float)luaL_checknumber(L, 8);
		cxform->add[3] = (float)luaL_checknumber(L, 5);
	} else {
		LgCxformP cxform = (LgCxformP)pushUserdata(L, sizeof(struct LgCxform), LGCXFORM_HANDLE);
		LgCxformIndentity(cxform);
	}

	return 1;
}

// point metatable ------------------------------------------------------------------------------------------------

static int point__eq(lua_State *L) {
	LgVector2P vector2Lhs = (LgVector2P)checkObject(L, 1, LGPOINT_HANDLE);
	LgVector2P vector2Rhs = (LgVector2P)checkObject(L, 2, LGPOINT_HANDLE);
	lua_pushboolean(L, 
		LgFloatEqual(vector2Lhs->x, vector2Rhs->x) &&
		LgFloatEqual(vector2Lhs->y, vector2Rhs->y));
	return 1;
}

static int point__index(lua_State *L) {
	LgVector2P vector2 = (LgVector2P)checkObject(L, 1, LGPOINT_HANDLE);
	const char *name = luaL_checkstring(L, 2);

	if (strlen(name) == 1) {
		switch(name[0]) {
		case 'x': lua_pushnumber(L, vector2->x); break;
		case 'y': lua_pushnumber(L, vector2->y); break;
		default: lua_pushnil(L);
		}
	} else {
		lua_getmetatable(L, -2);
		lua_pushvalue(L, -2);
		lua_gettable(L, -2);
	}

	return 1;
}

static int point__newindex(lua_State *L) {
	LgVector2P vector2 = (LgVector2P)checkObject(L, 1, LGPOINT_HANDLE);
	const char *name = luaL_checkstring(L, 2);
	lua_Number value = luaL_checknumber(L, 3);

	if (strlen(name) == 1) {
		switch(name[0]) {
		case 'x': vector2->x = (float)value; break;
		case 'y': vector2->y = (float)value; break;
		}
	}

	return 0;
}

static int point_clone(lua_State *L) {
	LgVector2P vector2 = (LgVector2P)checkObject(L, 1, LGPOINT_HANDLE);
	LgVector2P newvector2 = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
	LgCopyMemory(newvector2, vector2, sizeof(struct LgVector2));
	return 1;
}

static int point__add(lua_State *L) {
	LgVector2P vector2Lhs = (LgVector2P)checkObject(L, 1, LGPOINT_HANDLE);
	
	if (lua_isnumber(L, 2)) {
		LgVector2P result = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
		lua_Number rhs = luaL_checknumber(L, 2);
		result->x = vector2Lhs->x + (float)rhs;
		result->y = vector2Lhs->y + (float)rhs;
	} else if (LgLuaIsObject(L, 2, LGPOINT_HANDLE)) {
		LgVector2P result = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
		LgVector2P rhs = (LgVector2P)checkObject(L, 2, LGPOINT_HANDLE);
		result->x = vector2Lhs->x + rhs->x;
		result->y = vector2Lhs->y + rhs->y;
	} else {
		return luaL_error(L, "invalid operands to binary expression");
	}
	
	return 1;
}

static int point__sub(lua_State *L) {
	LgVector2P vector2Lhs = (LgVector2P)checkObject(L, 1, LGPOINT_HANDLE);
	
	if (lua_isnumber(L, 2)) {
		LgVector2P result = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
		lua_Number rhs = luaL_checknumber(L, 2);
		result->x = vector2Lhs->x - (float)rhs;
		result->y = vector2Lhs->y - (float)rhs;
	} else if (LgLuaIsObject(L, 2, LGPOINT_HANDLE)) {
		LgVector2P result = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
		LgVector2P rhs = (LgVector2P)checkObject(L, 2, LGPOINT_HANDLE);
		result->x = vector2Lhs->x - rhs->x;
		result->y = vector2Lhs->y - rhs->y;
	} else {
		return luaL_error(L, "invalid operands to binary expression");
	}
	
	return 1;
}

static int point__mul(lua_State *L) {
	LgVector2P vector2Lhs = (LgVector2P)checkObject(L, 1, LGPOINT_HANDLE);
	
	if (lua_isnumber(L, 2)) {
		LgVector2P result = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
		lua_Number rhs = luaL_checknumber(L, 2);
		result->x = vector2Lhs->x * (float)rhs;
		result->y = vector2Lhs->y * (float)rhs;
	} else if (LgLuaIsObject(L, 2, LGPOINT_HANDLE)) {
		LgVector2P result = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
		LgVector2P rhs = (LgVector2P)checkObject(L, 2, LGPOINT_HANDLE);
		result->x = vector2Lhs->x * rhs->x;
		result->y = vector2Lhs->y * rhs->y;
	} else {
		return luaL_error(L, "invalid operands to binary expression");
	}
	
	return 1;
}

static int point__div(lua_State *L) {
	LgVector2P vector2Lhs = (LgVector2P)checkObject(L, 1, LGPOINT_HANDLE);
	
	if (lua_isnumber(L, 2)) {
		LgVector2P result = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
		lua_Number rhs = luaL_checknumber(L, 2);
		result->x = vector2Lhs->x / (float)rhs;
		result->y = vector2Lhs->y / (float)rhs;
	} else if (LgLuaIsObject(L, 2, LGPOINT_HANDLE)) {
		LgVector2P result = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
		LgVector2P rhs = (LgVector2P)checkObject(L, 2, LGPOINT_HANDLE);
		result->x = vector2Lhs->x / rhs->x;
		result->y = vector2Lhs->y / rhs->y;
	} else {
		return luaL_error(L, "invalid operands to binary expression");
	}
	
	return 1;
}

static int point_is_zero(lua_State *L) {
	LgVector2P vector2 = (LgVector2P)checkObject(L, 1, LGPOINT_HANDLE);
	lua_pushboolean(L, LgVector2IsZero(vector2));
	return 1;
}

static int point_length(lua_State *L) {
	LgVector2P vector2 = (LgVector2P)checkObject(L, 1, LGPOINT_HANDLE);
	lua_pushnumber(L, LgVector2Length(vector2));
	return 1;
}

static int point_distance(lua_State *L) {
	LgVector2P vector2Lhs = (LgVector2P)checkObject(L, 1, LGPOINT_HANDLE);
	LgVector2P vector2Rhs = (LgVector2P)checkObject(L, 2, LGPOINT_HANDLE);
	lua_pushnumber(L, LgVector2Distance(vector2Lhs, vector2Rhs));
	return 1;
}

static int point_mid_point(lua_State *L) {
	LgVector2P vector2Lhs = (LgVector2P)checkObject(L, 1, LGPOINT_HANDLE);
	LgVector2P vector2Rhs = (LgVector2P)checkObject(L, 2, LGPOINT_HANDLE);
	LgVector2P result = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
	result->x = vector2Lhs->x, result->y = vector2Lhs->y;
	LgVector2MidPoint(result, vector2Rhs);
	return 1;
}

static int point_normalize(lua_State *L) {
	LgVector2P vector2 = (LgVector2P)checkObject(L, 1, LGPOINT_HANDLE);
	LgVector2P result = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
	result->x = vector2->x, result->y = vector2->y;
	LgVector2Normalize(result);
	return 0;
}

static int point_to_angle(lua_State *L) {
	LgVector2P vector2 = (LgVector2P)checkObject(L, 1, LGPOINT_HANDLE);

	if (lua_gettop(L) == 0) {
		lua_pushnumber(L, LgVector2ToAngle(vector2));
	} else {
		LgVector2P rhs = (LgVector2P)checkObject(L, 2, LGPOINT_HANDLE);
		lua_pushnumber(L, LgVector2ToVector2Angle(vector2, rhs));
	}
	
	return 1;
}

static const luaL_Reg point_methods[] = {
	{"__eq", point__eq},
	{"__index", point__index},
	{"__newindex", point__newindex},
	{"__add", point__add},
	{"__sub", point__sub},
	{"__mul", point__mul},
	{"__div", point__div},
	{"clone", point_clone},
	{"is_zero", point_is_zero},
	{"length", point_length},
	{"distance", point_distance},
	{"mid_point", point_mid_point},
	{"normalize", point_normalize},
	{"to_angle", point_to_angle},
	{NULL, NULL}
};

// point ------------------------------------------------------------------------------------------------
static int point(lua_State *L) {
	if (lua_gettop(L) == 2) {
		lua_Number x = luaL_checknumber(L, 1);
		lua_Number y = luaL_checknumber(L, 2);
		LgVector2P vector2 = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
		vector2->x = (float)x;
		vector2->y = (float)y;
	} else {
		LgVector2P vector2 = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
		vector2->x = vector2->y = 0;
	}

	return 1;
}

// rect metatable ------------------------------------------------------------------------------------------------
static int rect__eq(lua_State *L) {
	LgAabbP rectLhs = (LgAabbP)checkObject(L, 1, LGRECT_HANDLE);
	LgAabbP rectRhs = (LgAabbP)checkObject(L, 2, LGRECT_HANDLE);
	lua_pushboolean(L, 
		LgFloatEqual(rectLhs->x, rectRhs->x) && 
		LgFloatEqual(rectLhs->y, rectRhs->y) && 
		LgFloatEqual(rectLhs->width, rectRhs->width) && 
		LgFloatEqual(rectLhs->height, rectRhs->height));
	return 1;
}

static int rect__index(lua_State *L) {
	LgAabbP rect = (LgAabbP)checkObject(L, 1, LGRECT_HANDLE);
	const char *name = luaL_checkstring(L, 2);

	if (strcmp(name, "x") == 0) {
		lua_pushnumber(L, rect->x);
	} else if (strcmp(name, "y") == 0) {
		lua_pushnumber(L, rect->y);
	} else if (strcmp(name, "width") == 0) {
		lua_pushnumber(L, rect->width);
	} else if (strcmp(name, "height") == 0) {
		lua_pushnumber(L, rect->height);
	} else {
		lua_getmetatable(L, -2);
		lua_pushvalue(L, -2);
		lua_gettable(L, -2);
	}

	return 1;
}

static int rect__newindex(lua_State *L) {
	LgAabbP rect = (LgAabbP)checkObject(L, 1, LGRECT_HANDLE);
	const char *name = luaL_checkstring(L, 2);
	lua_Number value = luaL_checknumber(L, 3);

	if (strcmp(name, "x") == 0) {
		rect->x = (float)value;
	} else if (strcmp(name, "y") == 0) {
		rect->y = (float)value;
	} else if (strcmp(name, "width") == 0) {
		rect->width = (float)value;
	} else if (strcmp(name, "height") == 0) {
		rect->height = (float)value;
	}

	return 0;
}

static int rect_clone(lua_State *L) {
	LgAabbP rect = (LgAabbP)checkObject(L, 1, LGRECT_HANDLE);
	LgAabbP newrect = (LgAabbP)pushUserdata(L, sizeof(struct LgAabb), LGRECT_HANDLE);
	LgCopyMemory(newrect, rect, sizeof(struct LgAabb));
	return 1;
}

static int rect_intersect(lua_State *L) {
	LgAabbP rect1 = (LgAabbP)checkObject(L, 1, LGRECT_HANDLE);
	LgAabbP rect2 = (LgAabbP)checkObject(L, 2, LGRECT_HANDLE);
	lua_pushboolean(L, LgAabbIntersect(rect1, rect2));
	return 1;
}

static int rect_contains(lua_State *L) {
	LgAabbP rect = (LgAabbP)checkObject(L, 1, LGRECT_HANDLE);
	LgVector2P vector2 = (LgVector2P)checkObject(L, 2, LGPOINT_HANDLE);
	lua_pushboolean(L, LgAabbContains(rect, vector2));
	return 1;
}

static int rect_center(lua_State *L) {
	LgAabbP rect = (LgAabbP)checkObject(L, 1, LGRECT_HANDLE);
	LgVector2P vector2 = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
	LgAabbCenterPoint(rect, vector2);
	return 1;
}

static const luaL_Reg rect_methods[] = {
	{"__eq", rect__eq},
	{"__index", rect__index},
	{"__newindex", rect__newindex},
	{"clone", rect_clone},
	{"intersect", rect_intersect},
	{"contains", rect_contains},
	{"center", rect_center},
	{NULL, NULL}
};

// rect ------------------------------------------------------------------------------------------------
static int rect(lua_State *L) {
	if (lua_gettop(L) == 4) {
		lua_Number x = luaL_checknumber(L, 1);
		lua_Number y = luaL_checknumber(L, 2);
		lua_Number width = luaL_checknumber(L, 3);
		lua_Number height = luaL_checknumber(L, 4);
		LgAabbP rect = (LgAabbP)pushUserdata(L, sizeof(struct LgAabb), LGRECT_HANDLE);
		rect->x = (float)x, rect->y = (float)y;
		rect->width = (float)width, rect->height = (float)height;
	} else {
		LgAabbP rect = (LgAabbP)pushUserdata(L, sizeof(struct LgAabb), LGRECT_HANDLE);
		LgZeroMemory(rect, sizeof(struct LgAabb));
	}

	return 1;
}

// action metatable ------------------------------------------------------------------------------------------------
static int action__eq(lua_State *L) {
	LgActionP action1 = *((LgActionP *)checkObject(L, 1, LGACTION_HANDLE));
	LgActionP action2 = *((LgActionP *)checkObject(L, 2, LGACTION_HANDLE));
	lua_pushboolean(L, action1 == action2);
	return 1;
}

static int action__gc(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTION_HANDLE));
	LgActionRelease(action);
	return 0;
}

static int action_is_paused(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTION_HANDLE));
	lua_pushboolean(L, LgActionIsPaused(action));
	return 1;
}

static int action_pause(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTION_HANDLE));
	LgActionPause(action);
	return 0;
}

static int action_resume(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTION_HANDLE));
	LgActionResume(action);
	return 1;
}

static int action_is_running(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTION_HANDLE));
	lua_pushboolean(L, LgActionIsRunning(action));
	return 1;
}

static int action_stop(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTION_HANDLE));
	LgActionDoStop(action);
	return 0;
}

static const luaL_Reg action_methods[] = {
	{"__eq", action__eq},
	{"__gc", action__gc},
	{"is_paused", action_is_paused},
	{"pause", action_pause},
	{"resume", action_resume},
	{"is_running", action_is_running},
	{"stop", action_stop},
	{NULL, NULL}
};

// director ------------------------------------------------------------------------------------------------
static int director_push(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgDirectorPush(node);
	return 0;
}

static int director_pop(lua_State *L) {
	LgDirectorPop();
	return 0;
}

static int director_replace(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgDirectorReplace(node);
	return 0;
}

static int director_current_scene(lua_State *L) {
	pushNode(L, LgNodeRetain(LgDirectorCurrentScene()), NULL);
	return 1;
}

static int director_set_viewsize(lua_State *L) {
	struct LgSize size;

	size.width = luaL_checkint(L, 1);
	size.height = luaL_checkint(L, 2);
	LgDirectorSetViewSize(&size);
	return 0;
}

static int director_viewsize(lua_State *L) {
	struct LgSize size;
	LgDirectorViewSize(&size);
	lua_pushinteger(L, size.width);
	lua_pushinteger(L, size.height);
	return 2;
}

static int director_set_speed(lua_State *L) {
	LgDirectorSetSpeed((float)luaL_checknumber(L, 1));
	return 0;
}

static int director_speed(lua_State *L) {
	lua_pushnumber(L, LgDirectorSpeed());
	return 1;
}

static int director_pause(lua_State *L) {
	LgDirectorPause();
	return 0;
}

static int director_resume(lua_State *L) {
	LgDirectorResume();
	return 0;
}

static int director_is_paused(lua_State *L) {
	lua_pushboolean(L, LgDirectorIsPaused());
	return 1;
}

static int director_set_camera(lua_State *L) {
	float centerX, centerY, scale;

	centerX = (float)luaL_checknumber(L, 1);
	centerY = (float)luaL_checknumber(L, 2);
	scale = (float)luaL_checknumber(L, 3);
	LgDirectorSetCamera(centerX, centerY, scale);
	return 0;
}

// scheduler item metatable ------------------------------------------------------------------------------------------------
static int scheduler_item__eq(lua_State *L) {
	LgSchedulerItemP item1 = *((LgSchedulerItemP *)checkObject(L, 1, LGSCHEDULEITEM_HANDLE));
	LgSchedulerItemP item2 = *((LgSchedulerItemP *)checkObject(L, 2, LGSCHEDULEITEM_HANDLE));
	lua_pushboolean(L, item1== item2);
	return 1;
}

static int scheduler_item__gc(lua_State *L) {
	LgSchedulerItemP item = *((LgSchedulerItemP *)checkObject(L, 1, LGSCHEDULEITEM_HANDLE));
	LgSchedulerItemRelease(item);
	return 0;
}

static int scheduler_item_pause(lua_State *L) {
	LgSchedulerItemP item = *((LgSchedulerItemP *)checkObject(L, 1, LGSCHEDULEITEM_HANDLE));
	LgSchedulerPause(item);
	return 0;
}

static int scheduler_item_resume(lua_State *L) {
	LgSchedulerItemP item = *((LgSchedulerItemP *)checkObject(L, 1, LGSCHEDULEITEM_HANDLE));
	LgSchedulerResume(item);
	return 0;
}

static int scheduler_item_remove(lua_State *L) {
	LgSchedulerItemP item = *((LgSchedulerItemP *)checkObject(L, 1, LGSCHEDULEITEM_HANDLE));
	LgSchedulerRemove(item);
	return 0;
}

static const luaL_Reg schedule_item_methods[] = {
	{"__eq", scheduler_item__eq},
	{"__gc", scheduler_item__gc},
	{"pause", scheduler_item_pause},
	{"resume", scheduler_item_resume},
	{"remove", scheduler_item_remove},
	{NULL, NULL}
};

// scheduler
static LgBool callStep(LgTime delta, void *userdata) {
	int ref = (int)userdata;

	lua_rawgeti(LgScriptEnvState(), LUA_REGISTRYINDEX, ref);
	lua_pushnumber(LgScriptEnvState(), delta);
	if (lua_pcall(LgScriptEnvState(), 1, 0, 0)) {
		LgScriptShowError();
	}
	return LgTrue;
}

static int scheduler_schedule(lua_State *L) {
	float interval = 0.0f, delay = 0.0f;
	int repeat = LG_LOOP_FOREVER;
	LgBool paused = LgFalse;
	int ref;
	LgSchedulerItemP *item;

	if (!lua_isfunction(L, 1)) return 0;
	if (lua_gettop(L) >= 2) interval = (float)luaL_checknumber(L, 2);
	if (lua_gettop(L) >= 3) repeat = luaL_checkint(L, 3);
	if (lua_gettop(L) >= 4) delay = (float)luaL_checknumber(L, 4);
	if (lua_gettop(L) >= 5) paused = lua_toboolean(L, 5);

	lua_pushvalue(L, 1);
	ref = luaL_ref(L, LUA_REGISTRYINDEX);
	item = (LgSchedulerItemP *)pushUserdata(LgScriptEnvState(), sizeof(LgSchedulerItemP), LGSCHEDULEITEM_HANDLE);
	*item = LgSchedulerRun(callStep, destroyLuaRef, (void *)ref, interval, repeat, delay, paused);
	LgSchedulerItemRetain(*item);
	return 1;
}

// touch metatable ------------------------------------------------------------------------------------------------
static int touch__gc(lua_State *L) {
	LgTouchP touch = *((LgTouchP *)checkObject(L, 1, LGTOUCH_HANDLE));
	LgTouchRelease(touch);
	return 0;
}

static int touch__eq(lua_State *L) {
	LgTouchP touch1 = *((LgTouchP *)checkObject(L, 1, LGTOUCH_HANDLE));
	LgTouchP touch2 = *((LgTouchP *)checkObject(L, 2, LGTOUCH_HANDLE));
	lua_pushboolean(L, LgTouchId(touch1) == LgTouchId(touch2));
	return 1;
}

static int touch_id(lua_State *L) {
	LgTouchP touch = *((LgTouchP *)checkObject(L, 1, LGTOUCH_HANDLE));
	lua_pushinteger(L, LgTouchId(touch));
	return 1;
}

static int touch_local_position(lua_State *L) {
	LgTouchP touch = *((LgTouchP *)checkObject(L, 1, LGTOUCH_HANDLE));
	LgVector2P position = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
	LgTouchLocalPosition(touch, position);
	return 1;
}

static int touch_local_previous_position(lua_State *L) {
	LgTouchP touch = *((LgTouchP *)checkObject(L, 1, LGTOUCH_HANDLE));
	LgVector2P position = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
	LgTouchLocalPreviousPosition(touch, position);
	return 1;
}

static int touch_world_position(lua_State *L) {
	LgTouchP touch = *((LgTouchP *)checkObject(L, 1, LGTOUCH_HANDLE));
	LgVector2P position = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
	LgTouchWorldPosition(touch, position);
	return 1;
}

static int touch_world_previous_position(lua_State *L) {
	LgTouchP touch = *((LgTouchP *)checkObject(L, 1, LGTOUCH_HANDLE));
	LgVector2P position = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
	LgTouchWorldPreviousPosition(touch, position);
	return 1;
}

static int touch_delta(lua_State *L) {
	LgTouchP touch = *((LgTouchP *)checkObject(L, 1, LGTOUCH_HANDLE));
	LgVector2P delta = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
	LgTouchDelta(touch, delta);
	return 1;
}

static int touch_down_time(lua_State *L) {
	LgTouchP touch = *((LgTouchP *)checkObject(L, 1, LGTOUCH_HANDLE));
	lua_pushnumber(L, LgTouchDownTime(touch));
	return 1;
}

static int touch_down_node(lua_State *L) {
	LgTouchP touch = *((LgTouchP *)checkObject(L, 1, LGTOUCH_HANDLE));
	pushNode(L, LgNodeRetain(LgTouchDownNode(touch)), NULL);
	return 1;
}

static int touch_current_node(lua_State *L) {
	LgTouchP touch = *((LgTouchP *)checkObject(L, 1, LGTOUCH_HANDLE));
	pushNode(L, LgNodeRetain(LgTouchCurrentNode(touch)), NULL);
	return 1;
}

static const luaL_Reg touch_methods[] = {
	{"__gc", touch__gc},
	{"__eq", touch__eq},
	{"id", touch_id},
	{"local_position", touch_local_position},
	{"local_previous_position", touch_local_previous_position},
	{"world_position", touch_world_position},
	{"world_previous_position", touch_world_previous_position},
	{"delta", touch_delta},
	{"down_time", touch_down_time},
	{"down_node", touch_down_node},
	{"current_node", touch_current_node},
	{NULL, NULL}
};

// keyevent metatable ------------------------------------------------------------------------------------------------
static int keyevent_key(lua_State *L) {
	LgKeyEventP event = *((LgKeyEventP *)checkObject(L, 1, LGKEYEVENT_HANDLE));
	lua_pushinteger(L, (int)LgKeyEventKey(event));
	return 1;
}

static int keyevent_down_time(lua_State *L) {
	LgKeyEventP event = *((LgKeyEventP *)checkObject(L, 1, LGKEYEVENT_HANDLE));
	lua_pushnumber(L, LgKeyEventDownTime(event));
	return 1;
}

static const luaL_Reg keyevent_methods[] = {
	{"key", keyevent_key},
	{"down_time", keyevent_down_time},
	{NULL, NULL}
};

// node metatable ------------------------------------------------------------------------------------------------
static int node__gc(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgNodeRelease(node);
	return 0;
}

static int node__eq(lua_State *L) {
	LgNodeP node1 = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgNodeP node2 = *((LgNodeP *)checkObject(L, 2, LGNODE_HANDLE));
	lua_pushboolean(L, node1 == node2);
	return 1;
}

static int node_set_cxform(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgCxformP cxform = (LgCxformP)checkObject(L, 2, LGCXFORM_HANDLE);
	LgNodeSetCxform(node, cxform);
	return 0;
}

static int node_cxform(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgCxformP cxform = (LgCxformP)pushUserdata(L, sizeof(struct LgCxform), LGCXFORM_HANDLE);
	LgNodeCxform(node, cxform);
	return 1;
}

static int node_set_position(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));

	if (lua_gettop(L) == 3) {
		struct LgVector2 position;
		position.x = (float)luaL_checknumber(L, 2);
		position.y = (float)luaL_checknumber(L, 3);
		LgNodeSetPosition(node, &position);
	} else {
		LgVector2P position = (LgVector2P)checkObject(L, 2, LGPOINT_HANDLE);
		LgNodeSetPosition(node, position);
	}

	return 0;
}

static int node_position(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgVector2P position = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
	LgNodePosition(node, position);
	return 1;
}

static int node_set_anchor(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));

	if (lua_gettop(L) == 3) {
		struct LgVector2 anchor;
		anchor.x = (float)luaL_checknumber(L, 2);
		anchor.y = (float)luaL_checknumber(L, 3);
		LgNodeSetAnchor(node, &anchor);
	} else {
		LgVector2P anchor = (LgVector2P)checkObject(L, 2, LGPOINT_HANDLE);
		LgNodeSetAnchor(node, anchor);
	}

	return 0;
}

static int node_anchor(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgVector2P position = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
	LgNodeAnchor(node, position);
	return 1;
}

static int node_set_scale(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));

	if (lua_gettop(L) == 3) {
		struct LgVector2 scale;
		scale.x = (float)luaL_checknumber(L, 2);
		scale.y = (float)luaL_checknumber(L, lua_gettop(L) >= 3 ? 3 : 2);
		LgNodeSetScale(node, &scale);
	} else {
		struct LgVector2 scale;
		scale.x = scale.y = (float)luaL_checknumber(L, 2);
		LgNodeSetScale(node, &scale);
	}

	return 0;
}

static int node_scale(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgVector2P scale = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
	LgNodeScale(node, scale);
	return 1;
}

static int node_set_rotate(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	float angle = (float)luaL_checknumber(L, 2);
	LgNodeSetRotate(node, LgAngleToRadians(angle));
	return 0;
}

static int node_rotate(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	lua_pushnumber(L, LgRadiansToAngle(LgNodeRotate(node)));
	return 1;
}

static int node_set_transformAnchor(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));

	if (lua_gettop(L) == 3) {
		struct LgVector2 transformAnchor;
		transformAnchor.x = (float)luaL_checknumber(L, 2);
		transformAnchor.y = (float)luaL_checknumber(L, 3);
		LgNodeSetTransformAnchor(node, &transformAnchor);
	} else {
		LgVector2P transformAnchor = (LgVector2P)checkObject(L, 2, LGPOINT_HANDLE);
		LgNodeSetTransformAnchor(node, transformAnchor);
	}

	return 0;
}

static int node_transformAnchor(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgVector2P transformAnchor = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
	LgNodeTransformAnchor(node, transformAnchor);
	return 1;
}

static int node_set_zorder(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	int zorder = (int)luaL_checknumber(L, 2);
	LgNodeSetZorder(node, zorder);
	return 0;
}

static int node_zorder(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	lua_pushnumber(L, LgNodeZorder(node));
	return 1;
}

static int node_add_child(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgNodeP child = *((LgNodeP *)checkObject(L, 2, LGNODE_HANDLE));
	
	lua_pushvalue(L, 2);
	LgNodeAddChild(node, child);
	return 0;
}

static int node_remove(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgNodeRemove(node);
	return 0;
}

static int node_remove_all_children(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgNodeRemoveAllChildren(node);
	return 0;
}

static int node_is_visible(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	lua_pushboolean(L, LgNodeIsVisible(node));
	return 1;
}

static int node_set_visible(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgBool visible = (LgBool)lua_toboolean(L, 2);
	LgNodeSetVisible(node, visible);
	return 0;
}

static int node_do_action(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgActionP action = *((LgActionP *)checkObject(L, 2, LGACTION_HANDLE));
	LgNodeDoAction(node, action);
	return 0;
}

static int node_stop_actions(lua_State *L) {
	LgNodeP node = node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgNodeStopActions(node);
	return 0;
}

static int node_pause_actions(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgNodePauseActions(node);
	return 0;
}

static int node_resume_actions(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgNodeResumeActions(node);
	return 0;
}

static int node_parent(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	pushNode(L, LgNodeRetain(LgNodeParent(node)), NULL);
	return 1;
}

static int node_root(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	pushNode(L, LgNodeRetain(LgNodeRoot(node)), NULL);
	return 1;
}

static int node_set_opacity(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	float opacity = (float)luaL_checknumber(L, 2);
	LgNodeSetOpacity(node, opacity);
	return 0;
}

static int node_touch_is_enabled(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	lua_pushboolean(L, LgNodeHitTestIsEnabled(node));
	return 1;
}

static int node_enable_touch(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgNodeEnableHitTest(node, lua_toboolean(L, 2));
	return 0;
}

static int node_child(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	int i = luaL_checkint(L, 2);
	LgNodeP child;

	if (i > 0) {
		child = LgNodeChildByIndex(node, i - 1);
		pushNode(L, LgNodeRetain(child), NULL);
	} else {
		lua_pushnil(L);
	}
	
	return 1;
}

static int node_children_count(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	lua_pushinteger(L, LgNodeChildrenCount(node));
	return 1;
}

static int node_size(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	struct LgAabb aabb;
	LgVector2P size = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);

	LgNodeContentBox(node, &aabb);
	size->x = aabb.width;
	size->y = aabb.height;
	return 1;
}

static int node_content_box(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgAabbP aabb = (LgAabbP)pushUserdata(L, sizeof(struct LgAabb), LGRECT_HANDLE);
	LgNodeContentBox(node, aabb);
	return 1;
}

static int node_actions_count(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	lua_pushinteger(L, LgNodeActionsCount(node));
	return 1;
}

static int node_local_to_world(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));

	if (lua_gettop(L) == 3) {
		struct LgVector2 pt;
		pt.x = (float)luaL_checknumber(L, 2);
		pt.y = (float)luaL_checknumber(L, 3);
		LgNodePointToWorld(node, &pt);
		lua_pushnumber(L, pt.x);
		lua_pushnumber(L, pt.y);
		return 2;
	} else {
		LgVector2P pt = (LgVector2P)checkObject(L, 2, LGPOINT_HANDLE);
		LgVector2P newpt = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
		newpt->x = pt->x, newpt->y = pt->y;
		LgNodePointToWorld(node, newpt);
		return 1;
	}
}

static int node_world_to_local(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));

	if (lua_gettop(L) == 3) {
		struct LgVector2 pt;
		pt.x = (float)luaL_checknumber(L, 2);
		pt.y = (float)luaL_checknumber(L, 3);
		LgNodePointToLocal(node, &pt);
		lua_pushnumber(L, pt.x);
		lua_pushnumber(L, pt.y);
		return 2;
	} else {
		LgVector2P pt = (LgVector2P)checkObject(L, 2, LGPOINT_HANDLE);
		LgVector2P newpt = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
		newpt->x = pt->x, newpt->y = pt->y;
		LgNodePointToLocal(node, newpt);
		return 1;
	}
}

static int node_center_anchor(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgNodeCenterAnchor(node);
	return 0;
}

static int node_center_transform_anchor(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgNodeCenterAnchor(node);
	return 0;
}

static int node_set_touch_bubble(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgNodeSetTouchBubble(node, lua_toboolean(L, 2));
	return 0;
}

static int node_is_touch_bubble(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	lua_pushboolean(L, LgNodeIsTouchBubble(node));
	return 1;
}

static int node_add_gesture_recognizer(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgGestureRecognizerP gr = *((LgGestureRecognizerP *)checkObject(L, 2, LGGESTURERECOGNIZER_HANDLE));
	LgNodeAddGestureRecognizer(node, gr);
	return 0;
}

static void slotFireTouchEvent(void *param, void *userdata) {
	lua_State *L = LgScriptEnvState();
	int i;
	LgNodeTouchEventP evt = (LgNodeTouchEventP)param;

	lua_rawgeti(L, LUA_REGISTRYINDEX, (int)userdata);

	lua_pushinteger(L, evt->type);
	
	lua_newtable(L);
	for (i = 0; i < evt->num; i++) {
		LgTouchP *touch = (LgTouchP *)pushUserdata(L, sizeof(LgTouchP), LGTOUCH_HANDLE);
		*touch = LgTouchRetain(evt->touches[i]);
		lua_rawseti(L, -2, i + 1);
	}
	
	if (lua_pcall(LgScriptEnvState(), 2, 0, 0)) {
		LgScriptShowError();
	}
}

static int node_set_touch(lua_State *L) {
	int ref;
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	if (!lua_isfunction(L, 2)) return 0;
	lua_pushvalue(L, 2);
	ref = luaL_ref(L, LUA_REGISTRYINDEX);
	LgSlotAssign(LgNodeSlotTouch(node), slotFireTouchEvent, destroyLuaRef, NULL, (void *)ref);
	return 0;
}

static void slotFireGestureEvent(void *param, void *userdata) {
	lua_State *L = LgScriptEnvState();
	LgGestureEventP evt = (LgGestureEventP)param;

	lua_rawgeti(L, LUA_REGISTRYINDEX, (int)userdata);
	lua_pushinteger(L, evt->type);

	if (evt->type == LgGestureTap || evt->type == LgGestureLongPress) {
		const LgVector2P vec = (const LgVector2P)evt->userdata;
		LgVector2P _vec = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
		*_vec = *vec;
	} else if (evt->type == LgGesturePinch) {
		LgGesturePinchEventP evtData = (LgGesturePinchEventP)evt->userdata;
		lua_newtable(L);
		lua_pushliteral(L, "start_distance");
		lua_pushnumber(L, evtData->startDistance);
		lua_rawset(L, -3);
		lua_pushliteral(L, "current_distance");
		lua_pushnumber(L, evtData->currentDistance);
		lua_rawset(L, -3);
	} else if (evt->type == LgGestureSwipe) {
		enum LgGestureSwipeDirection d = (enum LgGestureSwipeDirection)evt->userdata;
		lua_pushinteger(L, (int)d);
	}

	if (lua_pcall(LgScriptEnvState(), 2, 0, 0)) {
		LgScriptShowError();
	}
}

static int node_set_gesture(lua_State *L) {
	int ref;
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	if (!lua_isfunction(L, 2)) return 0;
	lua_pushvalue(L, 2);
	ref = luaL_ref(L, LUA_REGISTRYINDEX);
	LgSlotAssign(LgNodeSlotGesture(node), slotFireGestureEvent, destroyLuaRef, NULL, (void *)ref);
	return 0;
}

static int node_set_enter(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	int ref;
	if (!lua_isfunction(L, 2)) return 0;
	lua_pushvalue(L, 2);
	ref = luaL_ref(L, LUA_REGISTRYINDEX);
	LgSlotAssign(LgNodeSlotEnter(node), slotCallFunction, destroyLuaRef, NULL, (void *)ref);
	return 0;
}

static int node_set_exit(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	int ref;
	if (!lua_isfunction(L, 2)) return 0;
	lua_pushvalue(L, 2);
	ref = luaL_ref(L, LUA_REGISTRYINDEX);
	LgSlotAssign(LgNodeSlotExit(node), slotCallFunction, destroyLuaRef, NULL, (void *)ref);
	return 0;
}

static int node_set_vertex_z(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	float z = (float)luaL_checknumber(L, 2);
	LgNodeSetVertexZ(node, z);
	return 0;
}

static int node_add_class(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	int count = lua_gettop(L), i;
	for (i = 2; i <= count; i++) {
		LgNodeAddClass(node, luaL_checkstring(L, i));
	}
	return 0;
}

static int node_remove_class(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	int count = lua_gettop(L), i;
	for (i = 2; i <= count; i++) {
		LgNodeRemoveClass(node, luaL_checkstring(L, i));
	}
	return 0;
}

static int node_toggle_class(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	int count = lua_gettop(L), i;
	for (i = 2; i <= count; i++) {
		LgNodeToggleClass(node, luaL_checkstring(L, i));
	}
	return 0;
}

static int node_has_class(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	int count = lua_gettop(L), i;
	for (i = 2; i <= count; i++) {
		if (LgNodeHasClass(node, luaL_checkstring(L, i))) {
			lua_pushboolean(L, 1);
			return 1;
		}
	}
	
	lua_pushboolean(L, 0);
	return 1;
}

static int node_clear_classes(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgNodeClearClasses(node);
	return 0;
}

static void pushClassName(const char *name, int *i) {
	lua_pushstring(LgScriptEnvState(), name);
	lua_rawseti(LgScriptEnvState(), -2, *i);
	(*i)++;
}

static int node_classes(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgAvlTreeP classes = LgNodeClasses(node);
	if (classes) {
		int i = 1;
		lua_newtable(L);
		LgAvlTreeForeach(classes, (LgForeach)pushClassName, &i);
		return 1;
	} else {
		lua_pushnil(L);
		return 1;
	}
}

static int node_select(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	const char *className = luaL_checkstring(L, 2);
	LgBool recursive = LgFalse;
	LgListP result = LgListCreate(NULL);
	LgListNodeP it;
	int i = 1;
	
	if (lua_gettop(L) >= 3) recursive = lua_toboolean(L, 3);
	LgNodeSelect(node, className, recursive, result);
	
	lua_newtable(L);
	it = LgListFirstNode(result);
	while(it) {
		LgNodeP child = (LgNodeP)LgListNodeValue(it);
		pushNode(L, child, NULL);
		lua_rawseti(L, -2, i++);
		it = LgListNextNode(it);
	}
	
	LgListDestroy(result);
	return 1;
}

static int node_bounds(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgAabbP rect = (LgAabbP)pushUserdata(L, sizeof(struct LgAabb), LGRECT_HANDLE);
	LgNodeBounds(node, rect);
	return 1;
}

static int node_enable_rtree(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgBool enable = lua_toboolean(L, 2);
	LgNodeEnableRTree(node, enable);
	return 0;
}

LgBool rtreeSearchCallback(LgRTreeId id, void* userdata) {
	int *i = (int *)userdata;
	lua_State *L = LgScriptEnvState();
	LgNodeP node = (LgNodeP)id;
	pushNode(L, LgNodeRetain(node), NULL);
	lua_rawseti(L, -2, *i);
	(*i)++;
	return LgTrue;
}

static int node_rtree_search_by_rect(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgAabbP rect = (LgAabbP)checkObject(L, 2, LGRECT_HANDLE);
	LgBool inRect = LgFalse;
	int i = 1;

	if (lua_gettop(L) >= 3) inRect = lua_toboolean(L, 3);
	lua_newtable(L);
	LgNodeRTreeSearchByRect(node, rect, inRect, rtreeSearchCallback, &i);
	return 1;
}

static int node_rtree_search_by_radius(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgVector2P center = (LgVector2P)checkObject(L, 2, LGPOINT_HANDLE);
	float radius = (float)luaL_checknumber(L, 3);
	int i = 1;

	lua_newtable(L);
	LgNodeRTreeSearchByRadius(node, center, radius, rtreeSearchCallback, &i);
	return 1;
}

static int node_set_id(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	const char *id = luaL_checkstring(L, 2);
	LgNodeSetId(node, id);
	return 0;
}

static int node_id(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	const char *id;
	id = LgNodeId(node);
	if (id) lua_pushstring(L, id);
	else lua_pushnil(L);
	return 1;
}

static void pushTouch(LgTouchP touch, int *i) {
	LgTouchP *_touch = (LgTouchP *)pushUserdata(LgScriptEnvState(), sizeof(LgTouchP), LGTOUCH_HANDLE);
	*_touch = LgTouchRetain(touch);
	lua_rawseti(LgScriptEnvState(), -2, *i);
	(*i)++;
}

static int node_touches(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgAvlTreeP touches = LgNodeTouches(node);
	if (touches) {
		int i = 1;
		lua_newtable(L);
		LgAvlTreeForeach(touches, (LgForeach)pushTouch, &i);
		return 1;
	} else {
		lua_pushnil(L);
		return 1;
	}
}

static int node_enable_multiple_touch(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	LgBool enable = lua_toboolean(L, 2);
	LgNodeEnableMultipleTouch(node, enable);
	return 0;
}

static const luaL_Reg node_methods[] = {
	{"__gc", node__gc},
	{"__eq", node__eq},
	{"set_cxform", node_set_cxform},
	{"cxform", node_cxform},
	{"set_position", node_set_position},
	{"position", node_position},
	{"set_anchor", node_set_anchor},
	{"anchor", node_anchor},
	{"set_scale", node_set_scale},
	{"scale", node_scale},
	{"set_rotate", node_set_rotate},
	{"rotate", node_rotate},
	{"set_transform_anchor", node_set_transformAnchor},
	{"transform_anchor", node_transformAnchor},
	{"set_zorder", node_set_zorder},
	{"zorder", node_zorder},
	{"add_child", node_add_child},
	{"remove", node_remove},
	{"remove_all_children", node_remove_all_children},
	{"is_visible", node_is_visible},
	{"set_visible", node_set_visible},
	{"do_action", node_do_action},
	{"stop_actions", node_stop_actions},
	{"pause_actions", node_pause_actions},
	{"resume_actions", node_resume_actions},
	{"parent", node_parent},
	{"root", node_root},
	{"set_opacity", node_set_opacity},
	{"enable_touch", node_enable_touch},
	{"touch_is_enabled", node_touch_is_enabled},
	{"child", node_child},
	{"children_count", node_children_count},
	{"size", node_size},
	{"content_box", node_content_box},
	{"actions_count", node_actions_count},
	{"local_to_world", node_local_to_world},
	{"world_to_local", node_world_to_local},
	{"center_anchor", node_center_anchor},
	{"center_transform_anchor", node_center_transform_anchor},
	{"set_touch_bubble", node_set_touch_bubble},
	{"is_touch_bubble", node_is_touch_bubble},
	{"add_gesture_recognizer", node_add_gesture_recognizer},
	{"set_touch", node_set_touch},
	{"set_gesture", node_set_gesture},
	{"set_enter", node_set_enter},
	{"set_exit", node_set_exit},
	{"set_vertex_z", node_set_vertex_z},
	{"add_class", node_add_class},
	{"remove_class", node_remove_class},
	{"toggle_class", node_toggle_class},
	{"has_class", node_has_class},
	{"clear_classes", node_clear_classes},
	{"classes", node_classes},
	{"select", node_select},
	{"bounds", node_bounds},
	{"enable_rtree", node_enable_rtree},
	{"rtree_search_by_rect", node_rtree_search_by_rect},
	{"rtree_search_by_radius", node_rtree_search_by_radius},
	{"set_id", node_set_id},
	{"id", node_id},
	{"touches", node_touches},
	{"enable_multiple_touch", node_enable_multiple_touch},
	{NULL, NULL}
};

// node ------------------------------------------------------------------------------------------------
static int new_node(lua_State *L) {
	pushNode(L, LgNodeCreateR(NULL), LGNODE_HANDLE);
	return 1;
}

// gesture recognizer ------------------------------------------------------------------------------------------------
static int gesture_recognizer__gc(lua_State *L) {
	LgGestureRecognizerP gr = *((LgGestureRecognizerP *)checkObject(L, 1, LGGESTURERECOGNIZER_HANDLE));
	LgGestureRecognizerRelease(gr);
	return 0;
}

static const luaL_Reg gesture_recognizer_methods[] = {
	{"__gc", gesture_recognizer__gc},
	{NULL, NULL}
};

static void pushGestureRecognizer(lua_State *L, LgGestureRecognizerP gr) {
	if (gr) {
		LgGestureRecognizerP *ret = (LgGestureRecognizerP *)lua_newuserdata(L, sizeof(LgGestureRecognizerP));
		*ret = gr;
		luaL_setmetatable(L, LGGESTURERECOGNIZER_HANDLE);
	} else {
		lua_pushnil(L);
	}
}

static int gesture_recognizer_tap(lua_State *L) {
	int numberOfTapsRequired = 1;
	if (lua_gettop(L) >= 1) numberOfTapsRequired = luaL_checkint(L, 1);
	pushGestureRecognizer(L, LgGestureRecognizerTapCreate(numberOfTapsRequired));
	return 1;
}

static int gesture_recognizer_longpress(lua_State *L) {
	pushGestureRecognizer(L, LgGestureRecognizerLongPressCreate());
	return 1;
}

static int gesture_recognizer_pinch(lua_State *L) {
	pushGestureRecognizer(L, LgGestureRecognizerPinchCreate());
	return 1;
}

static int gesture_recognizer_swipe(lua_State *L) {
	pushGestureRecognizer(L, LgGestureRecognizerSwipeCreate());
	return 1;
}

static struct luaL_Reg gesture_recognizer_functions[] = {
	// ---------------------------------------------
	{"tap", gesture_recognizer_tap},
	{"longpress", gesture_recognizer_longpress},
	{"pinch", gesture_recognizer_pinch},
	{"swipe", gesture_recognizer_swipe},
	{NULL, NULL},
};

// scene ------------------------------------------------------------------------------------------------
static int new_scene(lua_State *L) {
	pushNode(L, LgSceneCreateR(), LGSCENE_HANDLE);
	return 1;
}

// layer metatable ------------------------------------------------------------------------------------------------
static int layer_set_key_down(lua_State *L) {
	int ref;
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	if (!lua_isfunction(L, 2)) return 0;
	lua_pushvalue(L, 2);
	ref = luaL_ref(L, LUA_REGISTRYINDEX);
	LgSlotAssign(LgLayerSlotKeyDown(node), slotFireKeyEvent, destroyLuaRef, NULL, (void *)ref);
	return 0;
}

static int layer_set_key_up(lua_State *L) {
	int ref;
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	if (!lua_isfunction(L, 2)) return 0;
	lua_pushvalue(L, 2);
	ref = luaL_ref(L, LUA_REGISTRYINDEX);
	LgSlotAssign(LgLayerSlotKeyUp(node), slotFireKeyEvent, destroyLuaRef, NULL, (void *)ref);
	return 0;
}

static const luaL_Reg layer_methods[] = {
	{"set_key_down", layer_set_key_down},
	{"set_key_up", layer_set_key_up},
	{NULL, NULL}
};

// layer ------------------------------------------------------------------------------------------------
static int new_layer(lua_State *L) {
	pushNode(L, LgLayerCreateR(), LGCOLORLAYER_HANDLE);
	return 1;
}

// colorlayer metatable ------------------------------------------------------------------------------------------------

static int colorlayer_set_color(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGCOLORLAYER_HANDLE));
	LgColor color = *((LgColor *)checkObject(L, 2, LGCOLOR_HANDLE));
	LgColorLayerSetColor(node, color);
	return 0;
}

static int colorlayer_color(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGCOLORLAYER_HANDLE));
	LgColor *color = (LgColor *)pushUserdata(L, sizeof(LgColor), LGCOLOR_HANDLE);
	*color = LgColorLayerColor(node);
	return 1;
}

static const luaL_Reg colorlayer_methods[] = {
	{"set_color", colorlayer_set_color},
	{"color", colorlayer_color},
	{NULL, NULL}
};

// colorlayer ------------------------------------------------------------------------------------------------
static int new_colorlayer(lua_State *L) {
	LgColor color = *((LgColor *)checkObject(L, 1, LGCOLOR_HANDLE));
	pushNode(L, LgColorLayerCreateR(color), LGCOLORLAYER_HANDLE);
	return 1;
}

// gradientlayer ------------------------------------------------------------------------------------------------
static int new_gradientlayer(lua_State *L) {
	LgColor beginColor = *((LgColor *)checkObject(L, 1, LGCOLOR_HANDLE));
	LgColor endColor = *((LgColor *)checkObject(L, 2, LGCOLOR_HANDLE));
	struct LgVector2 up;

	up.x = (float)luaL_checknumber(L, 3);
	up.y = (float)luaL_checknumber(L, 4);

	pushNode(L, LgGradientLayerCreateR(beginColor, endColor, &up), LGGRADIENTLAYER_HANDLE);
	return 1;
}

// sprite metatable ------------------------------------------------------------------------------------------------

static int sprite_set_size(lua_State *L) {
	LgNodeP sprite = *((LgNodeP *)checkObject(L, 1, LGSPRITE_HANDLE));
	struct LgVector2 size;
	
	size.x = (float)luaL_checknumber(L, 2);
	size.y = (float)luaL_checknumber(L, 3);
	LgSpriteSetSize(sprite, &size);
	return 0;
}

static const luaL_Reg sprite_methods[] = {
	{"set_size", sprite_set_size},
	{NULL, NULL}
};

// sprite ------------------------------------------------------------------------------------------------
static LgBool parseSpriteLocation(lua_State *L, int idx, LgSpriteLocationP location) {
	if (lua_isstring(L, idx)) {
		location->type = LgSpriteInFile;
		location->filename = luaL_checkstring(L, idx);
		location->name = NULL;
		return LgTrue;
	} else if (lua_istable(L, idx)) {
		location->type = LgSpriteInTexturePack;

		lua_getfield(L, idx, "atlas");

		if (!lua_isstring(L, -1)) {
			lua_pop(L, 1);
			return LgFalse;
		}

		location->filename = luaL_checkstring(L, -1);
		lua_pop(L, 1);

		lua_getfield(L, idx, "name");

		if (!lua_isstring(L, -1)) {
			lua_pop(L, 1);
			return LgFalse;
		}

		location->name = luaL_checkstring(L, -1);
		lua_pop(L, 1);
		return LgTrue;
	} else {
		return LgFalse;
	}
}

static int new_sprite(lua_State *L) {
	struct LgSpriteLocation location;

	if (!parseSpriteLocation(L, 1, &location)) {
		lua_pushliteral(L, "bad sprite location");
		lua_error(L);
		return 0;
	}
	
	if (lua_gettop(L) == 3) {
		struct LgVector2 size;
		size.x = (float)luaL_checknumber(L, 2);
		size.y = (float)luaL_checknumber(L, 3);
		pushNode(L, LgSpriteCreateR(&location, &size, NULL, NULL), LGSPRITE_HANDLE);
	} else if (lua_gettop(L) == 7) {
		struct LgVector2 size;
		struct LgPoint insetLT, insetRB;

		size.x = (float)luaL_checknumber(L, 2);
		size.y = (float)luaL_checknumber(L, 3);

		insetLT.x = (int)luaL_checkint(L, 4);
		insetLT.y = (int)luaL_checkint(L, 5);
		insetRB.x = (int)luaL_checkint(L, 6);
		insetRB.y = (int)luaL_checkint(L, 7);

		pushNode(L, LgSpriteCreateR(&location, &size, &insetLT, &insetRB), LGSPRITE_HANDLE);
	} else {
		pushNode(L, LgSpriteCreateR(&location, NULL, NULL, NULL), LGSPRITE_HANDLE);
	}

	return 1;
}

// label metatable ------------------------------------------------------------------------------------------------
static int label_set_fontname(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGLABEL_HANDLE));
	const char *fontname = luaL_checkstring(L, 2);
	LgLabelSetFontName(node, fontname);
	return 0;
}

static int label_fontname(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGLABEL_HANDLE));
	lua_pushstring(L, LgLabelFontName(node));
	return 1;
}

static int label_set_fontsize(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGLABEL_HANDLE));
	int fontsize = luaL_checkint(L, 2);
	LgLabelSetFontSize(node, fontsize);
	return 0;
}

static int label_fontsize(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGLABEL_HANDLE));
	lua_pushinteger(L, LgLabelFontSize(node));
	return 1;
}

static int label_set_color(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGLABEL_HANDLE));
	LgColor color = *((LgColor *)checkObject(L, 2, LGCOLOR_HANDLE));
	LgLabelSetColor(node, color);
	return 0;
}

static int label_color(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGLABEL_HANDLE));
	LgColor *color = (LgColor *)pushUserdata(L, sizeof(LgColor), LGCOLOR_HANDLE);
	*color = LgLabelColor(node);
	return 1;
}

static int label_set_draw_flags(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGLABEL_HANDLE));
	int flags = luaL_checkint(L, 2);
	LgLabelSetDrawFlag(node, flags);
	return 0;
}

static int label_draw_flags(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGLABEL_HANDLE));
	lua_pushinteger(L, LgLabelDrawFlag(node));
	return 1;
}

static int label_set_text(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGLABEL_HANDLE));
	const char *text = luaL_checkstring(L, 2);
	LgLabelSetText(node, text);
	return 0;
}

static int label_text(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGLABEL_HANDLE));
	int textlen = LgLabelTextLength(node);
	char *text;

	text = (char *)LgMalloc(textlen + 1);
	LG_CHECK_ERROR(text);
	LgLabelText(node, text, textlen + 1);
	lua_pushstring(L, text);
	LgFree(text);
	return 1;

_error:
	lua_pushnil(L);
	return 1;
}

static const luaL_Reg label_methods[] = {
	{"set_fontname", label_set_fontname},
	{"fontname", label_fontname},
	{"set_fontsize", label_set_fontsize},
	{"fontsize", label_fontsize},
	{"set_color", label_set_color},
	{"color", label_color},
	{"set_text", label_set_text},
	{"text", label_text},
	{"set_draw_flags", label_set_draw_flags},
	{"draw_flags", label_draw_flags},
	{NULL, NULL}
};

// label ------------------------------------------------------------------------------------------------
static int new_label(lua_State *L) {
	const char *text = NULL;
	if (lua_gettop(L) >= 1) text = luaL_checkstring(L, 1);
	pushNode(L, LgLabelCreateR(text), LGLABEL_HANDLE);
	return 1;
}

// textfield metatable ------------------------------------------------------------------------------------------------
static int textfield_set_fontname(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTEXTFIELD_HANDLE));
	const char *fontname = luaL_checkstring(L, 2);
	LgTextFieldSetFontName(node, fontname);
	return 0;
}

static int textfield_fontname(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTEXTFIELD_HANDLE));
	lua_pushstring(L, LgTextFieldFontName(node));
	return 1;
}

static int textfield_set_fontsize(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTEXTFIELD_HANDLE));
	int fontsize = luaL_checkint(L, 2);
	LgTextFieldSetFontSize(node, fontsize);
	return 0;
}

static int textfield_fontsize(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTEXTFIELD_HANDLE));
	lua_pushinteger(L, LgTextFieldFontSize(node));
	return 1;
}

static int textfield_set_color(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTEXTFIELD_HANDLE));
	LgColor color = *((LgColor *)checkObject(L, 2, LGCOLOR_HANDLE));
	LgTextFieldSetColor(node, color);
	return 0;
}

static int textfield_color(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTEXTFIELD_HANDLE));
	LgColor *color = (LgColor *)pushUserdata(L, sizeof(LgColor), LGCOLOR_HANDLE);
	*color = LgTextFieldColor(node);
	return 1;
}

static int textfield_set_text(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTEXTFIELD_HANDLE));
	const char *text = luaL_checkstring(L, 2);
	LgTextFieldSetText(node, text);
	return 0;
}

static int textfield_text(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTEXTFIELD_HANDLE));
	int textlen = LgTextFieldTextLength(node);
	char *text;
	
	text = (char *)LgMalloc(textlen + 1);
	LG_CHECK_ERROR(text);
	LgLabelText(node, text, textlen + 1);
	lua_pushstring(L, text);
	LgFree(text);
	return 1;
	
_error:
	lua_pushnil(L);
	return 1;
}

static int textfield_set_comp_color(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTEXTFIELD_HANDLE));
	LgColor color = *((LgColor *)checkObject(L, 2, LGCOLOR_HANDLE));
	LgTextFieldSetCompColor(node, color);
	return 0;
}

static int textfield_comp_color(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTEXTFIELD_HANDLE));
	LgColor *color = (LgColor *)pushUserdata(L, sizeof(LgColor), LGCOLOR_HANDLE);
	*color = LgTextFieldCompColor(node);
	return 1;
}

static int textfield_set_placeholder_color(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTEXTFIELD_HANDLE));
	LgColor color = *((LgColor *)checkObject(L, 2, LGCOLOR_HANDLE));
	LgTextFieldSetPlaceHolderColor(node, color);
	return 0;
}

static int textfield_placeholder_color(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTEXTFIELD_HANDLE));
	LgColor *color = (LgColor *)pushUserdata(L, sizeof(LgColor), LGCOLOR_HANDLE);
	*color = LgTextFieldPlaceHolderColor(node);
	return 1;
}

static int textfield_set_cursor_color(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTEXTFIELD_HANDLE));
	LgColor color = *((LgColor *)checkObject(L, 2, LGCOLOR_HANDLE));
	LgTextFieldSetCursorColor(node, color);
	return 0;
}

static int textfield_cursor_color(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTEXTFIELD_HANDLE));
	LgColor *color = (LgColor *)pushUserdata(L, sizeof(LgColor), LGCOLOR_HANDLE);
	*color = LgTextFieldCursorColor(node);
	return 1;
}

static int textfield_set_change(lua_State *L) {
	int ref;
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGNODE_HANDLE));
	if (!lua_isfunction(L, 2)) return 0;
	lua_pushvalue(L, 2);
	ref = luaL_ref(L, LUA_REGISTRYINDEX);
	LgSlotAssign(LgTextFieldSlotChange(node), slotCallFunction, destroyLuaRef, NULL, (void *)ref);
	return 0;
}

static const luaL_Reg textfield_methods[] = {
	{"set_fontname", textfield_set_fontname},
	{"fontname", textfield_fontname},
	{"set_fontsize", textfield_set_fontsize},
	{"fontsize", textfield_fontsize},
	{"set_color", textfield_set_color},
	{"color", textfield_color},
	{"set_text", textfield_set_text},
	{"text", textfield_text},
	{"set_comp_color", textfield_set_comp_color},
	{"comp_color", textfield_comp_color},
	{"set_placeholder_color", textfield_set_placeholder_color},
	{"placeholder_color", textfield_placeholder_color},
	{"set_cursor_color", textfield_set_cursor_color},
	{"cursor_color", textfield_cursor_color},
	{"set_change", textfield_set_change},
	{NULL, NULL}
};

// textfield ------------------------------------------------------------------------------------------------
static int new_textfield(lua_State *L) {
	struct LgVector2 size;
	const char *text = NULL;
	const char *placeHolder = NULL;

	size.x = (float)luaL_checknumber(L, 1);
	size.y = (float)luaL_checknumber(L, 2);

	if (lua_gettop(L) >= 3) text = luaL_checkstring(L, 3);
	if (lua_gettop(L) >= 4) placeHolder = luaL_checkstring(L, 4);

	pushNode(L, LgTextFieldCreateR(&size, text, placeHolder), LGTEXTFIELD_HANDLE);
	return 1;
}

// drawnode metatable ------------------------------------------------------------------------------------------------
static int drawnode_set_color(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGDRAWNODE_HANDLE));
	LgColor color = *((LgColor *)checkObject(L, 2, LGCOLOR_HANDLE));
	LgDrawNodeSetColor(node, color);
	return 0;
}

static int drawnode_color(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGDRAWNODE_HANDLE));
	LgColor *color = (LgColor *)pushUserdata(L, sizeof(LgColor), LGCOLOR_HANDLE);
	*color = LgDrawNodeColor(node);
	return 1;
}

static int drawnode_clear(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGDRAWNODE_HANDLE));
	LgDrawNodeClear(node);
	return 1;
}

static int drawnode_rectangle(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGDRAWNODE_HANDLE));
	float x, y, w, h;
	x = (float)luaL_checknumber(L, 2);
	y = (float)luaL_checknumber(L, 3);
	w = (float)luaL_checknumber(L, 4);
	h = (float)luaL_checknumber(L, 5);
	LgDrawNodeRectangle(node, x, y, w, h);
	return 1;
}

static int drawnode_ellipse(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGDRAWNODE_HANDLE));
	float x, y, w, h;
	x = (float)luaL_checknumber(L, 2);
	y = (float)luaL_checknumber(L, 3);
	w = (float)luaL_checknumber(L, 4);
	h = (float)luaL_checknumber(L, 5);
	LgDrawNodeEllipse(node, x, y, w, h);
	return 1;
}

static int drawnode_fan(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGDRAWNODE_HANDLE));
	float x, y, w, h, start, end;
	x = (float)luaL_checknumber(L, 2);
	y = (float)luaL_checknumber(L, 3);
	w = (float)luaL_checknumber(L, 4);
	h = (float)luaL_checknumber(L, 5);
	start = (float)luaL_checknumber(L, 6);
	end = (float)luaL_checknumber(L, 7);
	LgDrawNodeFan(node, x, y, w, h, start, end);
	return 1;
}

static int drawnode_triangle(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGDRAWNODE_HANDLE));
	float x1, y1, x2, y2, x3, y3;
	x1 = (float)luaL_checknumber(L, 2);
	y1 = (float)luaL_checknumber(L, 3);
	x2 = (float)luaL_checknumber(L, 4);
	y2 = (float)luaL_checknumber(L, 5);
	x3 = (float)luaL_checknumber(L, 6);
	y3 = (float)luaL_checknumber(L, 7);
	LgDrawNodeTriangle(node, x1, y1, x2, y2, x3, y3);
	return 1;
}

static const luaL_Reg drawnode_methods[] = {
	{"set_color", drawnode_set_color},
	{"color", drawnode_color},
	{"clear", drawnode_clear},
	{"rectangle", drawnode_rectangle},
	{"ellipse", drawnode_ellipse},
	{"fan", drawnode_fan},
	{"triangle", drawnode_triangle},
	{NULL, NULL}
};

// drawnode ------------------------------------------------------------------------------------------------
static int new_drawnode(lua_State *L) {
	pushNode(L, LgDrawNodeCreateR(), LGDRAWNODE_HANDLE);
	return 1;
}

// clipnode metatable ------------------------------------------------------------------------------------------------

static int clipnode_set_stencil(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGCLIPNODE_HANDLE));
	LgNodeP stencil = *((LgNodeP *)checkObject(L, 2, LGNODE_HANDLE));
	LgClipNodeSetStencil(node, stencil);
	return 0;
}

static int clipnode_stencil(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGCLIPNODE_HANDLE));
	pushNode(L, LgNodeRetain(LgClipNodeStencil(node)), NULL);
	return 1;
}

static int clipnode_set_invert(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGCLIPNODE_HANDLE));
	LgBool invert = (LgBool)lua_toboolean(L, 2);
	LgClipNodeSetInvert(node, invert);
	return 0;
}

static int clipnode_is_invert(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGCLIPNODE_HANDLE));
	lua_pushboolean(L, LgClipNodeIsInvert(node));
	return 1;
}

static const luaL_Reg clipnode_methods[] = {
	{"set_stencil", clipnode_set_stencil},
	{"stencil", clipnode_stencil},
	{"set_invert", clipnode_set_invert},
	{"is_invert", clipnode_is_invert},
	{NULL, NULL}
};

// clipnode ------------------------------------------------------------------------------------------------
static int new_clipnode(lua_State *L) {
	pushNode(L, LgClipNodeCreateR(), LGCLIPNODE_HANDLE);
	return 1;
}

// transition ------------------------------------------------------------------------------------------------
static int transition(lua_State *L) {
	LgNodeP scene = *((LgNodeP *)checkObject(L, 1, LGSCENE_HANDLE));
	LgActionP inact = NULL, outact = NULL;

	if (lua_gettop(L) >= 2 && LgLuaIsObject(L, 2, LGACTION_HANDLE)) {
		inact = *((LgActionP *)checkObject(L, 2, LGACTION_HANDLE));
	}

	if (lua_gettop(L) >= 3 && LgLuaIsObject(L, 3, LGACTION_HANDLE)) {
		outact = *((LgActionP *)checkObject(L, 3, LGACTION_HANDLE));
	}
	
	LgTransition(scene, inact, outact);
	return 0;
}

// particle system metatable ---------------------------------------------------------------

static int ps_set_active(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGPARTICLESYSTEM_HANDLE));
	LgParticleSystemSetActive(node, lua_toboolean(L, 2));
	return 0;
}

static int ps_set_in_world(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGPARTICLESYSTEM_HANDLE));
	LgParticleSystemSetInWorld(node, lua_toboolean(L, 2));
	return 0;
}

static const luaL_Reg ps_methods[] = {
	{"set_active", ps_set_active},
	{"set_in_world", ps_set_in_world},
	{NULL, NULL}
};

// particle system ------------------------------------------------------------------------------------------------
static int new_particle_system(lua_State *L) {
	const char *filename = luaL_checkstring(L, 1);
	pushNode(L, LgParticleSystemCreateR(filename), LGPARTICLESYSTEM_HANDLE);
	return 1;
}

// batchnode ------------------------------------------------------------------------------------------------
static int new_batchnode(lua_State *L) {
	pushNode(L, LgBatchNodeCreateR(), LGBATCHNODE_HANDLE);
	return 1;
}

// animation matatable ------------------------------------------------------------------------------------------------
static int animation_set_animation(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGANIMATION_HANDLE));
	LgBool loop = lua_gettop(L) >= 3 ? lua_toboolean(L, 3) : LgTrue;
	LgAnimationSetAnimation(node, luaL_checkstring(L, 2), loop);
	return 0;
}

static int animation_add_animation(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGANIMATION_HANDLE));
	LgBool loop = lua_gettop(L) >= 3 ? lua_toboolean(L, 3) : LgTrue;
	float delay = (float)luaL_optnumber(L, 4, 0);
	LgAnimationAddAnimation(node, luaL_checkstring(L, 2), loop, delay);
	return 0;
}

static int animation_stop_animation(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGANIMATION_HANDLE));
	LgAnimationStopAnimation(node);
	return 0;
}

static int animation_duration(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGANIMATION_HANDLE));
	lua_pushnumber(L, LgAnimationAnimationDuration(node, luaL_checkstring(L, 2)));
	return 0;
}

static int animation_set_skin(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGANIMATION_HANDLE));
	LgAnimationSetSkin(node, luaL_checkstring(L, 2));
	return 0;
}

static void slotFireAnimationEvent(void *param, void *userdata) {
	lua_State *L = LgScriptEnvState();
	LgAnimationEventP evt = (LgAnimationEventP)param;

	lua_rawgeti(L, LUA_REGISTRYINDEX, (int)userdata);

	lua_pushinteger(L, evt->type);

	lua_newtable(L);

	if (evt->type == LgAnimationEventAnimationStart ||
		evt->type == LgAnimationEventAnimationEnd) {
		lua_pushstring(L, evt->animationName);
		lua_setfield(L, -2, "animation_name");
	} else if (evt->type == LgAnimationEventAnimationComplete) {
		lua_pushstring(L, evt->animationName);
		lua_setfield(L, -2, "animation_name");
		lua_pushinteger(L, evt->loopCount);
		lua_setfield(L, -2, "loop_count");
	} else if (evt->type == LgAnimationEventAnimationCustom) {
		lua_pushinteger(L, evt->data.intVal);
		lua_setfield(L, -2, "int_value");
		lua_pushnumber(L, evt->data.floatVal);
		lua_setfield(L, -2, "float_value");
		lua_pushstring(L, evt->data.stringVal);
		lua_setfield(L, -2, "string_value");
	} else {
		lua_pushnil(L);
	}

	if (lua_pcall(LgScriptEnvState(), 2, 0, 0)) {
		LgScriptShowError();
	}
}

static int animation_set_event(lua_State *L) {
	int ref;
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGANIMATION_HANDLE));
	if (!lua_isfunction(L, 2)) return 0;
	lua_pushvalue(L, 2);
	ref = luaL_ref(L, LUA_REGISTRYINDEX);
	LgSlotAssign(LgAnimationSlotEvent(node), slotFireAnimationEvent, destroyLuaRef, NULL, (void *)ref);
	return 0;
}

static int animation_set_exact_touch(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGANIMATION_HANDLE));
	LgAnimationSetExactTouch(node, lua_toboolean(L, 2));
	return 0;
}

static int animation_is_exact_touch(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGANIMATION_HANDLE));
	lua_pushboolean(L, LgAnimationIsExactTouch(node));
	return 0;
}

static const luaL_Reg animation_methods[] = {
	{"set_animation", animation_set_animation},
	{"add_animation", animation_add_animation},
	{"stop_animation", animation_stop_animation},
	{"duration", animation_duration},
	{"set_skin", animation_set_skin},
	{"set_exact_touch", animation_set_exact_touch},
	{"is_exact_touch", animation_is_exact_touch},
	{"set_event", animation_set_event},
	{NULL, NULL}
};

// animation ------------------------------------------------------------------------------------------------
static int new_animation(lua_State *L) {
	const char *filename = luaL_checkstring(L, 1);
	const char *animationName = luaL_optstring(L, 2, NULL);
	LgBool loop = lua_gettop(L) >= 3 ? lua_toboolean(L, 3) : LgTrue;

	pushNode(L, LgAnimationNodeCreateR(filename, animationName, loop), LGANIMATION_HANDLE);
	return 1;
}

// tilemap matatable ------------------------------------------------------------------------------------------------
static int tilemaplayer_size(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTILEMAPLAYER_HANDLE));
	LgVector2P size = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
	LgTileMapLayerSize(node, size);
	return 1;
}

static int tilemaplayer_grid_size(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTILEMAPLAYER_HANDLE));
	struct LgSize size;
	LgVector2P sizev;

	LgTileMapLayerGridSize(node, &size);
	sizev = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
	sizev->x = (float)size.width;
	sizev->y = (float)size.height;
	return 1;
}

static int tilemaplayer_set_scroll(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTILEMAPLAYER_HANDLE));

	if (lua_gettop(L) == 3) {
		struct LgVector2 scroll;
		scroll.x = (float)luaL_checknumber(L, 2);
		scroll.y = (float)luaL_checknumber(L, 3);
		LgTileMapLayerSetScroll(node, &scroll);
	} else {
		LgVector2P scroll = (LgVector2P)checkObject(L, 1, LGPOINT_HANDLE);
		LgTileMapLayerSetScroll(node, scroll);
	}

	return 0;
}

static int tilemaplayer_scroll(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTILEMAPLAYER_HANDLE));
	LgVector2P scroll = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
	LgTileMapLayerScroll(node, scroll);
	return 1;
}

static int tilemaplayer_center_scroll(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTILEMAPLAYER_HANDLE));

	if (lua_gettop(L) == 3) {
		struct LgVector2 center;
		center.x = (float)luaL_checknumber(L, 2) - 1;
		center.y = (float)luaL_checknumber(L, 3) - 1;
		LgTileMapLayerCenterScroll(node, &center);
		lua_pushnumber(L, center.x);
		lua_pushnumber(L, center.y);
		return 2;
	} else {
		LgVector2P grid = (LgVector2P)checkObject(L, 2, LGPOINT_HANDLE);
		LgVector2P center = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
		center->x = grid->x - 1, center->y = grid->y - 1;
		LgTileMapLayerCenterScroll(node, center);
		return 1;
	}
}

static int tilemaplayer_center_to(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTILEMAPLAYER_HANDLE));

	if (lua_gettop(L) == 3) {
		struct LgVector2 center;
		center.x = (float)luaL_checknumber(L, 2) - 1;
		center.y = (float)luaL_checknumber(L, 3) - 1;
		LgTileMapLayerCenterTo(node, &center);
	} else {
		LgVector2P grid = (LgVector2P)checkObject(L, 2, LGPOINT_HANDLE);
		struct LgVector2 center = {grid->x - 1, grid->y - 1};
		LgTileMapLayerCenterTo(node, &center);
	}

	return 0;
}

static int tilemaplayer_local_to_grid(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTILEMAPLAYER_HANDLE));

	if (lua_gettop(L) == 3) {
		struct LgVector2 pt;
		pt.x = (float)luaL_checknumber(L, 2);
		pt.y = (float)luaL_checknumber(L, 3);
		LgTileMapLayerLocalToGrid(node, &pt);
		lua_pushnumber(L, pt.x + 1);
		lua_pushnumber(L, pt.y + 1);
		return 2;
	} else {
		LgVector2P pt = (LgVector2P)checkObject(L, 2, LGPOINT_HANDLE);
		LgVector2P newpt = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
		newpt->x = pt->x, newpt->y = pt->y;
		LgTileMapLayerLocalToGrid(node, newpt);
		newpt->x += 1, newpt->y += 1;
		return 1;
	}
}

static int tilemaplayer_global_to_grid(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTILEMAPLAYER_HANDLE));

	if (lua_gettop(L) == 3) {
		struct LgVector2 pt;
		pt.x = (float)luaL_checknumber(L, 2);
		pt.y = (float)luaL_checknumber(L, 3);
		LgTileMapLayerGlobalToGrid(node, &pt);
		lua_pushnumber(L, pt.x + 1);
		lua_pushnumber(L, pt.y + 1);
		return 2;
	} else {
		LgVector2P pt = (LgVector2P)checkObject(L, 2, LGPOINT_HANDLE);
		LgVector2P newpt = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
		newpt->x = pt->x, newpt->y = pt->y;
		LgTileMapLayerGlobalToGrid(node, newpt);
		newpt->x += 1, newpt->y += 1;
		return 1;
	}
}

static int tilemaplayer_grid_to_local(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTILEMAPLAYER_HANDLE));

	if (lua_gettop(L) == 3) {
		struct LgVector2 pt;
		pt.x = (float)luaL_checknumber(L, 2) - 1;
		pt.y = (float)luaL_checknumber(L, 3) - 1;
		LgTileMapLayerGridToLocal(node, &pt);
		lua_pushnumber(L, pt.x);
		lua_pushnumber(L, pt.y);
		return 2;
	} else {
		LgVector2P pt = (LgVector2P)checkObject(L, 2, LGPOINT_HANDLE);
		LgVector2P newpt = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
		newpt->x = pt->x - 1, newpt->y = pt->y - 1;
		LgTileMapLayerGridToLocal(node, newpt);
		return 1;
	}
}

static int tilemaplayer_grid_to_global(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTILEMAPLAYER_HANDLE));

	if (lua_gettop(L) == 3) {
		struct LgVector2 pt;
		pt.x = (float)luaL_checknumber(L, 2) - 1;
		pt.y = (float)luaL_checknumber(L, 3) - 1;
		LgTileMapLayerGridToGlobal(node, &pt);
		lua_pushnumber(L, pt.x);
		lua_pushnumber(L, pt.y);
		return 2;
	} else {
		LgVector2P pt = (LgVector2P)checkObject(L, 2, LGPOINT_HANDLE);
		LgVector2P newpt = (LgVector2P)pushUserdata(L, sizeof(struct LgVector2), LGPOINT_HANDLE);
		newpt->x = pt->x - 1, newpt->y = pt->y - 1;
		LgTileMapLayerGridToGlobal(node, newpt);
		return 1;
	}
}

static int tilemaplayer_show_layer(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTILEMAPLAYER_HANDLE));
	const char *name = luaL_checkstring(L, 2);
	LgTileMapLayerShowLayer(node, name);
	return 0;
}

static int tilemaplayer_hide_layer(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTILEMAPLAYER_HANDLE));
	const char *name = luaL_checkstring(L, 2);
	LgTileMapLayerHideLayer(node, name);
	return 0;
}

static int tilemaplayer_prop(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTILEMAPLAYER_HANDLE));
	const char *propName = luaL_checkstring(L, 2);
	json_t *properties = LgTileMapLayerProperties(node);

	if (properties) {
		pushCJSON(L, json_object_get(properties, propName));
	} else {
		lua_pushnil(L);
	}

	return 1;
}

static int tilemaplayer_layer_prop(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTILEMAPLAYER_HANDLE));
	const char *layerName = luaL_checkstring(L, 2);
	const char *propName = luaL_checkstring(L, 3);
	json_t *properties = LgTileMapLayerLayerProperties(node, layerName);

	if (properties) {
		pushCJSON(L, json_object_get(properties, propName));
	} else {
		lua_pushnil(L);
	}

	return 1;
}

static int tilemaplayer_tile_prop(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTILEMAPLAYER_HANDLE));

	if (lua_gettop(L) == 5) {
		const char *layerName = luaL_checkstring(L, 2);
		int col = luaL_checkint(L, 3) - 1;
		int row = luaL_checkint(L, 4) - 1;
		const char *propName = luaL_checkstring(L, 5);
		struct LgPoint pt = {col, row};
		json_t *properties = LgTileMapLayerTileProperties(node, layerName, &pt);

		if (properties) {
			pushCJSON(L, json_object_get(properties, propName));
			return 1;
		}
	} else if (lua_gettop(L) == 4) {
		int layerCount = LgTileMapLayerLayerCount(node), i;
		int col = luaL_checkint(L, 2) - 1;
		int row = luaL_checkint(L, 3) - 1;
		const char *propName = luaL_checkstring(L, 4);
		struct LgPoint pt = {col, row};

		for (i = layerCount - 1; i >= 0; i--) {
			json_t *properties = LgTileMapLayerTilePropertiesByLayerIndex(node, i, &pt);

			if (properties) {
				json_t *item = json_object_get(properties, propName);
				if (item) {
					pushCJSON(L, item);
					return 1;
				}
			}
		}
	}

	lua_pushnil(L);
	return 1;
}

static int tilemaplayer_find_path(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTILEMAPLAYER_HANDLE));
	struct LgPoint start, end;
	LgBool bias;
	LgPointP path;
	struct LgSize size;
	int count, i;

	start.x = luaL_checkint(L, 2) - 1;
	start.y = luaL_checkint(L, 3) - 1;
	end.x = luaL_checkint(L, 4) - 1;
	end.y = luaL_checkint(L, 5) - 1;
	bias = lua_toboolean(L, 6);

	LgTileMapLayerGridSize(node, &size);
	count = LgTileMapLayerFindPath(node, &start, &end, bias, &path);

	lua_newtable(L);

	for (i = 0; i < count; i++) {
		lua_pushinteger(L, path[i].x + 1);
		lua_rawseti(L, -2, i * 2 + 1);
		lua_pushinteger(L, path[i].y + 1);
		lua_rawseti(L, -2, i * 2 + 2);
	}

	LgFree(path);
	return 1;
}

static int tilemaplayer_tileIdByAlias(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTILEMAPLAYER_HANDLE));
	const char *alias = luaL_checkstring(L, 2);
	lua_pushinteger(L, LgTileMapLayerTileIdByAlias(node, alias));
	return 1;
}

static int tilemaplayer_tileId(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTILEMAPLAYER_HANDLE));
	const char *layer = luaL_checkstring(L, 2);
	int col = luaL_checkint(L, 3) - 1;
	int row = luaL_checkint(L, 4) - 1;
	lua_pushinteger(L, LgTileMapLayerTileId(node, layer, col, row));
	return 1;
}

static int tilemaplayer_setTileId(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTILEMAPLAYER_HANDLE));
	const char *layer = luaL_checkstring(L, 2);
	int col = luaL_checkint(L, 3) - 1;
	int row = luaL_checkint(L, 4) - 1;
	int id = luaL_checkint(L, 5);
	LgTileMapLayerSetTileId(node, layer, col, row, id);
	return 0;
}

static int tilemaplayer_setChildAt(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTILEMAPLAYER_HANDLE));
	LgNodeP child = *((LgNodeP *)checkObject(L, 2, LGNODE_HANDLE));
	const char *layer = luaL_checkstring(L, 3);
	int row = luaL_checkint(L, 4) - 1;
	LgTileMapLayerSetChildAt(node, layer, row, child);
	return 0;
}

static void pushMapObjectPoints(lua_State *L, const LgVector2P points, int count) {
	int i;
	
	lua_newtable(L);
	
	for (i = 0; i < count; i++) {
		lua_newtable(L);
		lua_pushnumber(L, points[i].x);
		lua_setfield(L, -2, "x");
		lua_pushnumber(L, points[i].y);
		lua_setfield(L, -2, "y");
		lua_rawseti(L, -2, i + 1);
	}
}

static void pushMapObject(lua_State *L, LgMapObjectP object) {
	lua_newtable(L);

	lua_pushinteger(L, object->objectType);
	lua_setfield(L, -2, "object_type");

	if (object->name) {
		lua_pushstring(L, object->name);
		lua_setfield(L, -2, "name");
	}

	if (object->type) {
		lua_pushstring(L, object->type);
		lua_setfield(L, -2, "type");
	}

	lua_pushnumber(L, object->x);
	lua_setfield(L, -2, "x");
	lua_pushnumber(L, object->y);
	lua_setfield(L, -2, "y");

	switch(object->objectType) {
	case LgMapObjectRectangle:
	case LgMapObjectEllipse:
		lua_pushnumber(L, object->rectangleOrEllipse.width);
		lua_setfield(L, -2, "width");
		lua_pushnumber(L, object->rectangleOrEllipse.height);
		lua_setfield(L, -2, "height");
		break;
	case LgMapObjectPolygon:
	case LgMapObjectPolyline:
		pushMapObjectPoints(L, object->polygonOrPolyline.points, object->polygonOrPolyline.count);
		lua_setfield(L, -2, "points");
		break;
	case LgMapObjectTile:
		lua_pushnumber(L, object->tile.gid);
		lua_setfield(L, -2, "gid");
		break;
	}
}

static int tilemaplayer_object(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGTILEMAPLAYER_HANDLE));
	const char *layerName = luaL_checkstring(L, 2);
	const char *objectName = luaL_checkstring(L, 3);
	LgMapObjectP object = LgTileMapLayerFindObject(node, layerName, objectName);

	if (object) {
		pushMapObject(L, object);
	} else {
		lua_pushnil(L);
	}

	return 1;
}

static const luaL_Reg tilemaplayer_methods[] = {
	{"size", tilemaplayer_size},
	{"grid_size", tilemaplayer_grid_size},
	{"set_scroll", tilemaplayer_set_scroll},
	{"scroll", tilemaplayer_scroll},
	{"center_to", tilemaplayer_center_to},
	{"center_scroll", tilemaplayer_center_scroll},
	{"local_to_grid", tilemaplayer_local_to_grid},
	{"global_to_grid", tilemaplayer_global_to_grid},
	{"grid_to_local", tilemaplayer_grid_to_local},
	{"grid_to_global", tilemaplayer_grid_to_global},
	{"show_layer", tilemaplayer_show_layer},
	{"hide_layer", tilemaplayer_hide_layer},
	{"prop", tilemaplayer_prop},
	{"layer_prop", tilemaplayer_layer_prop},
	{"tile_prop", tilemaplayer_tile_prop},
	{"find_path", tilemaplayer_find_path},
	{"tileid_by_alias", tilemaplayer_tileIdByAlias},
	{"tileid", tilemaplayer_tileId},
	{"set_tileid", tilemaplayer_setTileId},
	{"set_child_at", tilemaplayer_setChildAt},
	{"object", tilemaplayer_object},
	{NULL, NULL}
};

// tilemap ------------------------------------------------------------------------------------------------
static int new_tilemaplayer(lua_State *L) {
	const char *filename = luaL_checkstring(L, 1);
	pushNode(L, LgTileMapLayerCreateR(filename), LGTILEMAPLAYER_HANDLE);
	return 1;
}

// ui ------------------------------------------------------------------------------------------------

// control node

static int controlnode_set_size(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGCONTROLNODE_HANDLE));
	struct LgVector2 size;
	
	size.x = (float)luaL_checknumber(L, 2);
	size.y = (float)luaL_checknumber(L, 3);
	LgControlNodeSetSize(node, &size);
	return 0;
}

static int controlnode_set_enable(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGCONTROLNODE_HANDLE));
	LgControlNodeSetEnable(node, lua_toboolean(L, 2));
	return 0;
}

static int controlnode_is_enabled(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGCONTROLNODE_HANDLE));
	lua_pushboolean(L, LgControlNodeIsEnabled(node));
	return 1;
}

static int controlnode_set_select(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGCONTROLNODE_HANDLE));
	LgControlNodeSetSelect(node, lua_toboolean(L, 2));
	return 0;
}

static int controlnode_is_selected(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGCONTROLNODE_HANDLE));
	lua_pushboolean(L, LgControlNodeIsSelected(node));
	return 1;
}

static int controlnode_set_highlight(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGCONTROLNODE_HANDLE));
	LgControlNodeSetHighlight(node, lua_toboolean(L, 2));
	return 0;
}

static int controlnode_is_highlighted(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGCONTROLNODE_HANDLE));
	lua_pushboolean(L, LgControlNodeIsHighlighted(node));
	return 1;
}

static int controlnode_set_background(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGCONTROLNODE_HANDLE));
	LgNodeP background = *((LgNodeP *)checkObject(L, 2, LGSPRITE_HANDLE));
	LgControlNodeSetBackground(node, background);
	return 0;
}

static const luaL_Reg controlnode_methods[] = {
	{"set_size", controlnode_set_size},
	{"set_enable", controlnode_set_enable},
	{"is_enabled", controlnode_is_enabled},
	{"set_select", controlnode_set_select},
	{"is_selected", controlnode_is_selected},
	{"set_highlight", controlnode_set_highlight},
	{"is_highlighted", controlnode_is_highlighted},
	{"set_background", controlnode_set_background},
	{NULL, NULL}
};

#define NEW_CONTROL(name, createFunc, handle) \
	static int new_##name(lua_State *L) { \
		if (lua_gettop(L) == 2) { \
			struct LgVector2 size; \
			size.x = (float)luaL_optnumber(L, 1, 0); \
			size.y = (float)luaL_optnumber(L, 2, 0); \
			pushNode(L, createFunc(&size), handle); \
		} else { \
			pushNode(L, createFunc(&LgVector2Zero), handle); \
		} \
		return 1; \
	}

NEW_CONTROL(control, LgControlNodeCreateR, LGCONTROLNODE_HANDLE)

// button

static int button_set_title_text(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGBUTTON_HANDLE));
	const char *text = luaL_checkstring(L, 2);
	const char *fontname = luaL_optstring(L, 3, "_default");
	int fontsize = luaL_optinteger(L, 4, 16);
	LgButtonSetTitleAsText(node, text, fontname, fontsize);
	return 0;
}

static int button_set_title(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGBUTTON_HANDLE));
	LgNodeP title = *((LgNodeP *)checkObject(L, 2, LGNODE_HANDLE));
	LgButtonSetTitle(node, title);
	return 0;
}

static int button_set_press_action(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGBUTTON_HANDLE));
	LgActionP action = *((LgActionP *)checkObject(L, 2, LGACTION_HANDLE));
	LgButtonSetPressAction(node, action);
	return 0;
}

static int button_set_release_action(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGBUTTON_HANDLE));
	LgActionP action = *((LgActionP *)checkObject(L, 2, LGACTION_HANDLE));
	LgButtonSetReleaseAction(node, action);
	return 0;
}

static int button_set_textcolor(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGBUTTON_HANDLE));
	LgColor color = *((LgColor *)checkObject(L, 2, LGCOLOR_HANDLE));
	LgButtonSetTitleTextColor(node, color);
	return 0;
}

static int button_set_press_textcolor(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGBUTTON_HANDLE));
	LgColor color = *((LgColor *)checkObject(L, 2, LGCOLOR_HANDLE));
	LgButtonSetTitlePressTextColor(node, color);
	return 0;
}

static int button_set_face(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGBUTTON_HANDLE));
	LgNodeP facenode = *((LgNodeP *)checkObject(L, 2, LGSPRITE_HANDLE));
	LgButtonSetFace(node, facenode);
	return 0;
}

static int button_set_press_face(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGBUTTON_HANDLE));
	LgNodeP facenode = *((LgNodeP *)checkObject(L, 2, LGSPRITE_HANDLE));
	LgButtonSetPressFace(node, facenode);
	return 0;
}

static int button_set_click(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGBUTTON_HANDLE));
	int ref;
	if (!lua_isfunction(L, 2)) return 0;
	lua_pushvalue(L, 2);
	ref = luaL_ref(L, LUA_REGISTRYINDEX);
	LgSlotAssign(LgButtonSlotClick(node), slotCallFunction, destroyLuaRef, NULL, (void *)ref);
	return 0;
}

static const luaL_Reg button_methods[] = {
	{"set_title_text", button_set_title_text},
	{"set_title", button_set_title},
	{"set_press_action", button_set_press_action},
	{"set_release_action", button_set_release_action},
	{"set_text_color", button_set_textcolor},
	{"set_press_text_color", button_set_press_textcolor},
	{"set_face", button_set_face},
	{"set_press_face", button_set_press_face},
	{"set_click", button_set_click},
	{NULL, NULL}
};

NEW_CONTROL(button, LgButtonCreateR, LGBUTTON_HANDLE)

// scrollview

static int scrollview_content_view(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGSCROLLVIEW_HANDLE));
	pushNode(L, LgNodeRetain(LgScrollViewContentView(node)), NULL);
	return 1;
}

static int scrollview_set_content_size(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGSCROLLVIEW_HANDLE));
	struct LgVector2 size;
	size.x = (float)luaL_checknumber(L, 2);
	size.y = (float)luaL_checknumber(L, 3);
	LgScrollViewSetContentSize(node, &size);
	return 0;
}

static int scrollview_set_lock(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGSCROLLVIEW_HANDLE));
	LgBool lockHorizontal = lua_toboolean(L, 2);
	LgBool lockVertical = lua_toboolean(L, 3);
	LgScrollViewSetLock(node, lockHorizontal, lockVertical);
	return 0;
}

static int scrollview_view_rect(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGSCROLLVIEW_HANDLE));
	LgAabbP rect = (LgAabbP)pushUserdata(L, sizeof(struct LgAabb), LGRECT_HANDLE);
	LgScrollViewGetViewRect(node, rect);
	return 1;
}

static int scrollview_set_scroll(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGSCROLLVIEW_HANDLE));
	struct LgVector2 offset;
	offset.x = (float)luaL_checknumber(L, 2);
	offset.y = (float)luaL_checknumber(L, 3);
	LgScrollViewSetScroll(node, &offset);
	return 0;
}

static int scrollview_set_onscroll(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGSCROLLVIEW_HANDLE));
	int ref;
	if (!lua_isfunction(L, 2)) return 0;
	lua_pushvalue(L, 2);
	ref = luaL_ref(L, LUA_REGISTRYINDEX);
	LgSlotAssign(LgScrollViewSlotScroll(node), slotCallFunction, destroyLuaRef, NULL, (void *)ref);
	return 0;
}

static const luaL_Reg scrollview_methods[] = {
	{"content_view", scrollview_content_view},
	{"set_content_size", scrollview_set_content_size},
	{"set_lock", scrollview_set_lock},
	{"view_rect", scrollview_view_rect},
	{"set_scroll", scrollview_set_scroll},
	{"set_onscroll", scrollview_set_onscroll},
	{NULL, NULL}
};

NEW_CONTROL(scrollview, LgScrollViewCreateR, LGSCROLLVIEW_HANDLE)

// slider

static int slider_set_thumb(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGSLIDER_HANDLE));
	LgNodeP thumb = *((LgNodeP *)checkObject(L, 2, LGNODE_HANDLE));
	LgSliderSetThumb(node, thumb);
	return 0;
}

static int slider_set_value(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGSLIDER_HANDLE));
	float value = (float)luaL_checknumber(L, 2);
	LgSliderSetValue(node, value);
	return 0;
}

static int slider_value(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGSLIDER_HANDLE));
	lua_pushnumber(L, LgSliderValue(node));
	return 1;
}

static int slider_set_range(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGSLIDER_HANDLE));
	float minValue = (float)luaL_checknumber(L, 2);
	float maxValue = (float)luaL_checknumber(L, 3);
	LgSliderSetRange(node, minValue, maxValue);
	return 1;
}

static int slider_range(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGSLIDER_HANDLE));
	float minValue, maxValue;
	LgSliderRange(node, &minValue, &maxValue);
	lua_pushnumber(L, minValue);
	lua_pushnumber(L, maxValue);
	return 2;
}

static int slider_set_is_integer(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGSLIDER_HANDLE));
	LgSliderSetIsInteger(node, lua_toboolean(L, 2));
	return 2;
}

static int slider_is_integer(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGSLIDER_HANDLE));
	lua_pushboolean(L, LgSliderIsInteger(node));
	return 1;
}

static int slider_set_press_action(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGSLIDER_HANDLE));
	LgActionP action = *((LgActionP *)checkObject(L, 2, LGACTION_HANDLE));
	LgSliderSetPressAction(node, action);
	return 0;
}

static int slider_set_release_action(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGSLIDER_HANDLE));
	LgActionP action = *((LgActionP *)checkObject(L, 2, LGACTION_HANDLE));
	LgSliderSetReleaseAction(node, action);
	return 0;
}

static int slider_set_change(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGSLIDER_HANDLE));
	int ref;
	if (!lua_isfunction(L, 2)) return 0;
	lua_pushvalue(L, 2);
	ref = luaL_ref(L, LUA_REGISTRYINDEX);
	LgSlotAssign(LgSliderSlotChange(node), slotCallFunction, destroyLuaRef, NULL, (void *)ref);
	return 0;
}

static const luaL_Reg slider_methods[] = {
	{"set_thumb", slider_set_thumb},
	{"set_value", slider_set_value},
	{"value", slider_value},
	{"set_range", slider_set_range},
	{"range", slider_range},
	{"set_is_integer", slider_set_is_integer},
	{"is_integer", slider_is_integer},
	{"set_press_action", slider_set_press_action},
	{"set_release_action", slider_set_release_action},
	{"set_change", slider_set_change},
	{NULL, NULL}
};

NEW_CONTROL(slider, LgSliderCreateR, LGSLIDER_HANDLE)

// switcher

static int switcher_set_value(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGSWITCHER_HANDLE));
	LgSwitcherSetValue(node, lua_toboolean(L, 2));
	return 0;
}

static int switcher_value(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGSWITCHER_HANDLE));
	lua_pushboolean(L, LgSwitcherValue(node));
	return 1;
}

static int switcher_set_button(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGSWITCHER_HANDLE));
	LgNodeP button = *((LgNodeP *)checkObject(L, 2, LGSPRITE_HANDLE));
	LgSwitcherSetButton(node,button);
	return 0;
}

static int switcher_set_change(lua_State *L) {
	LgNodeP node = *((LgNodeP *)checkObject(L, 1, LGSWITCHER_HANDLE));
	int ref;
	if (!lua_isfunction(L, 2)) return 0;
	lua_pushvalue(L, 2);
	ref = luaL_ref(L, LUA_REGISTRYINDEX);
	LgSlotAssign(LgSwitcherSlotChange(node), slotCallFunction, destroyLuaRef, NULL, (void *)ref);
	return 0;
}

static const luaL_Reg switcher_methods[] = {
	{"set_value", switcher_set_value},
	{"value", switcher_value},
	{"set_button", switcher_set_button},
	{"set_change", switcher_set_change},
	{NULL, NULL}
};

NEW_CONTROL(switcher, LgSwitcherCreateR, LGSWITCHER_HANDLE)

// ui functions table
static struct luaL_Reg ui_functions[] = {
	{"new_control", new_control},
	{"new_button", new_button},
	{"new_scrollview", new_scrollview},
	{"new_slider", new_slider},
	{"new_switcher", new_switcher},
	{NULL, NULL}
};

// actions ------------------------------------------------------------------------------------------------
static int action_clone(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTION_HANDLE));
	pushAction(L, LgActionClone(action), LGACTION_HANDLE);
	return 1;
}

static int action_loop(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTION_HANDLE));
	int times = (int)luaL_checkint(L, 2);
	pushAction(L, LgActionLoopCreate(action, times), LGACTION_HANDLE);
	return 1;
}

static int action_loop_forever(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTION_HANDLE));
	pushAction(L, LgActionLoopForeverCreate(action), LGACTION_HANDLE);
	return 1;
}

static int action_sequence(lua_State *L) {
	if (lua_gettop(L) > 0) {
		LgActionP *actions;
		int i;
		for (i = 0; i < lua_gettop(L); i++) checkObject(L, i + 1, LGACTION_HANDLE);
		actions = (LgActionP *)LgMallocZ(sizeof(LgActionP) * lua_gettop(L));
		for (i = 0; i < lua_gettop(L); i++)
			actions[i] = *((LgActionP *)checkObject(L, i + 1, LGACTION_HANDLE));
		pushAction(L, LgActionSequenceCreate(actions, lua_gettop(L)), LGACTION_HANDLE);
		LgFree(actions);
	} else {
		lua_pushnil(L);
	}

	return 1;
}

static int action_call(lua_State *L) {
	int ref;
	if (!lua_isfunction(L, 1)) return 0;
	lua_pushvalue(L, 1);
	ref = luaL_ref(L, LUA_REGISTRYINDEX);
	pushAction(L, LgActionCallCreate(slotCallFunction, destroyLuaRef, slotCloneLuaRef, (void *)ref), LGACTION_HANDLE);
	return 1;
}

static int action_interval(lua_State *L) {
	LgTime duration = (LgTime)luaL_checknumber(L, 1);
	int refFunc, refContext = 0;
	
	if (!lua_istable(L, 2)) return 0;
	lua_pushvalue(L, 2);
	refFunc = luaL_ref(L, LUA_REGISTRYINDEX);
	if (lua_gettop(L) >= 3) {
		lua_pushvalue(L, 3);
		refContext = luaL_ref(L, LUA_REGISTRYINDEX);
	}
	assert(refFunc != refContext);
	pushAction(L, LgActionLuaIntervalCreate(duration, refFunc, refContext), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_camera(lua_State *L) {
	LgTime duration = (LgTime)luaL_checknumber(L, 1);
	float centerX, centerY, scale;
	centerX = (float)luaL_checknumber(L, 2);
	centerY = (float)luaL_checknumber(L, 3);
	scale = (float)luaL_checknumber(L, 4);
	pushAction(L, LgActionCameraCreate(duration, centerX, centerY, scale), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_moveto(lua_State *L) {
	LgTime duration = (LgTime)luaL_checknumber(L, 1);
	struct LgVector2 position;
	position.x = (float)luaL_checknumber(L, 2);
	position.y = (float)luaL_checknumber(L, 3);
	pushAction(L, LgActionMoveToCreate(duration, &position), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_moveby(lua_State *L) {
	LgTime duration = (LgTime)luaL_checknumber(L, 1);
	struct LgVector2 delta;
	delta.x = (float)luaL_checknumber(L, 2);
	delta.y = (float)luaL_checknumber(L, 3);
	pushAction(L, LgActionMoveByCreate(duration, &delta), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_bezierto(lua_State *L) {
	LgTime duration = (LgTime)luaL_checknumber(L, 1);
	struct LgVector2 cp1, cp2, end;
	cp1.x = (float)luaL_checknumber(L, 2);
	cp1.y = (float)luaL_checknumber(L, 3);
	cp2.x = (float)luaL_checknumber(L, 4);
	cp2.y = (float)luaL_checknumber(L, 5);
	end.x = (float)luaL_checknumber(L, 6);
	end.y = (float)luaL_checknumber(L, 7);
	pushAction(L, LgActionBezierToCreate(duration, &cp1, &cp2, &end), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_bezierby(lua_State *L) {
	LgTime duration = (LgTime)luaL_checknumber(L, 1);
	struct LgVector2 cp1, cp2, end;
	cp1.x = (float)luaL_checknumber(L, 2);
	cp1.y = (float)luaL_checknumber(L, 3);
	cp2.x = (float)luaL_checknumber(L, 4);
	cp2.y = (float)luaL_checknumber(L, 5);
	end.x = (float)luaL_checknumber(L, 6);
	end.y = (float)luaL_checknumber(L, 7);
	pushAction(L, LgActionBezierByCreate(duration, &cp1, &cp2, &end), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_scaleto(lua_State *L) {
	LgTime duration = (LgTime)luaL_checknumber(L, 1);
	struct LgVector2 scale;

	scale.x = (float)luaL_checknumber(L, 2);
	scale.y = lua_gettop(L) >= 3 ? (float)luaL_checknumber(L, 3) : (float)luaL_checknumber(L, 2);
	pushAction(L, LgActionScaleToCreate(duration, &scale), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_scaleby(lua_State *L) {
	LgTime duration = (LgTime)luaL_checknumber(L, 1);
	struct LgVector2 delta;
	delta.x = (float)luaL_checknumber(L, 2);
	delta.y = lua_gettop(L) >= 3 ? (float)luaL_checknumber(L, 3) : (float)luaL_checknumber(L, 2);
	pushAction(L, LgActionScaleByCreate(duration, &delta), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_rotateto(lua_State *L) {
	LgTime duration = (LgTime)luaL_checknumber(L, 1);
	float d = (float)luaL_checknumber(L, 2);
	pushAction(L, LgActionRotateToCreate(duration, LgAngleToRadians(d)), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_rotateby(lua_State *L) {
	LgTime duration = (LgTime)luaL_checknumber(L, 1);
	float delta = (float)luaL_checknumber(L, 2);
	pushAction(L, LgActionRotateByCreate(duration, LgAngleToRadians(delta)), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_blink(lua_State *L) {
	LgTime duration = (LgTime)luaL_checknumber(L, 1);
	int blinks = luaL_checkint(L, 2);
	pushAction(L, LgActionBlinkCreate(duration, blinks), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_fadein(lua_State *L) {
	LgTime duration = (LgTime)luaL_checknumber(L, 1);
	pushAction(L, LgActionFadeInCreate(duration), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_fadeout(lua_State *L) {
	LgTime duration = (LgTime)luaL_checknumber(L, 1);
	pushAction(L, LgActionFadeOutCreate(duration), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_delay(lua_State *L) {
	LgTime duration = (LgTime)luaL_checknumber(L, 1);
	pushAction(L, LgActionDelayCreate(duration), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_jump(lua_State *L) {
	LgTime duration = (LgTime)luaL_checknumber(L, 1);
	struct LgVector2 offset;
	int jumps = 3;
	float height = 100.0f;

	offset.x = (float)luaL_checknumber(L, 2);
	offset.y = (float)luaL_checknumber(L, 3);

	if (lua_gettop(L) >= 4) jumps = luaL_checkint(L, 4);
	if (lua_gettop(L) >= 5) height = (float)luaL_checknumber(L, 5);
	pushAction(L, LgActionJumpCreate(duration, &offset, height, jumps), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_speed(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTIONINTERVAL_HANDLE));
	float speed = (float)luaL_checknumber(L, 2);
	pushAction(L, LgActionSpeedCreate(action, speed), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_range(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTIONINTERVAL_HANDLE));
	float start = (float)luaL_checknumber(L, 2);
	float end = (float)luaL_checknumber(L, 3);
	pushAction(L, LgActionRangeCreate(action, start, end), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_spawn(lua_State *L) {
	if (lua_gettop(L) > 0) {
		LgActionP *actions;
		int i;
		// 先测试一遍是否都是从interval派生，避免出现异常以后actions内存泄露
		for (i = 0; i < lua_gettop(L); i++) checkObject(L, i + 1, LGACTIONINTERVAL_HANDLE);
		actions = (LgActionP *)LgMallocZ(sizeof(LgActionP) * lua_gettop(L));
		for (i = 0; i < lua_gettop(L); i++)
			actions[i] = *((LgActionP *)checkObject(L, i + 1, LGACTIONINTERVAL_HANDLE));
		pushAction(L, LgActionSpawnCreate(actions, lua_gettop(L)), LGACTIONINTERVAL_HANDLE);
		LgFree(actions);
	} else {
		lua_pushnil(L);
	}

	return 1;
}

static int action_easein(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTIONINTERVAL_HANDLE));
	float rate = 0.3f;
	if (lua_gettop(L) >= 2) rate = (float)luaL_checknumber(L, 2);
	pushAction(L, LgActionEaseInCreate(action, rate), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_easeout(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTIONINTERVAL_HANDLE));
	float rate = 0.3f;
	if (lua_gettop(L) >= 2) rate = (float)luaL_checknumber(L, 2);
	pushAction(L, LgActionEaseOutCreate(action, rate), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_easeinout(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTIONINTERVAL_HANDLE));
	float rate = 0.3f;
	if (lua_gettop(L) >= 2) rate = (float)luaL_checknumber(L, 2);
	pushAction(L, LgActionEaseInOutCreate(action, rate), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_easeexpin(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTIONINTERVAL_HANDLE));
	float rate = 0.3f;
	if (lua_gettop(L) >= 2) rate = (float)luaL_checknumber(L, 2);
	pushAction(L, LgActionEaseExponentialInCreate(action, rate), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_easeexpout(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTIONINTERVAL_HANDLE));
	float rate = 0.3f;
	if (lua_gettop(L) >= 2) rate = (float)luaL_checknumber(L, 2);
	pushAction(L, LgActionEaseExponentialOutCreate(action, rate), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_easeexpinout(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTIONINTERVAL_HANDLE));
	float rate = 0.3f;
	if (lua_gettop(L) >= 2) rate = (float)luaL_checknumber(L, 2);
	pushAction(L, LgActionEaseExponentialInOutCreate(action, rate), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_easesinein(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTIONINTERVAL_HANDLE));
	float rate = 0.3f;
	if (lua_gettop(L) >= 2) rate = (float)luaL_checknumber(L, 2);
	pushAction(L, LgActionEaseSineInCreate(action, rate), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_easesineout(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTIONINTERVAL_HANDLE));
	float rate = 0.3f;
	if (lua_gettop(L) >= 2) rate = (float)luaL_checknumber(L, 2);
	pushAction(L, LgActionEaseSineOutCreate(action, rate), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_easesineinout(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTIONINTERVAL_HANDLE));
	float rate = 0.3f;
	if (lua_gettop(L) >= 2) rate = (float)luaL_checknumber(L, 2);
	pushAction(L, LgActionEaseSineInOutCreate(action, rate), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_easeelasticin(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTIONINTERVAL_HANDLE));
	float period = 0.3f;
	if (lua_gettop(L) >= 2) period = (float)luaL_checknumber(L, 2);
	pushAction(L, LgActionEaseElasticInCreate(action, period), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_easeelasticout(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTIONINTERVAL_HANDLE));
	float period = 0.3f;
	if (lua_gettop(L) >= 2) period = (float)luaL_checknumber(L, 2);
	pushAction(L, LgActionEaseElasticOutCreate(action, period), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_easeelasticinout(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTIONINTERVAL_HANDLE));
	float period = 0.3f;
	if (lua_gettop(L) >= 2) period = (float)luaL_checknumber(L, 2);
	pushAction(L, LgActionEaseElasticInOutCreate(action, period), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_easebouncein(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTIONINTERVAL_HANDLE));
	pushAction(L, LgActionEaseBounceInCreate(action), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_easebounceout(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTIONINTERVAL_HANDLE));
	pushAction(L, LgActionEaseBounceOutCreate(action), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_easebounceinout(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTIONINTERVAL_HANDLE));
	pushAction(L, LgActionEaseBounceInOutCreate(action), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_easebackin(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTIONINTERVAL_HANDLE));
	pushAction(L, LgActionEaseBackInCreate(action), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_easebackout(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTIONINTERVAL_HANDLE));
	pushAction(L, LgActionEaseBackOutCreate(action), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_easebackinout(lua_State *L) {
	LgActionP action = *((LgActionP *)checkObject(L, 1, LGACTIONINTERVAL_HANDLE));
	pushAction(L, LgActionEaseBackInOutCreate(action), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_wave3d(lua_State *L) {
	LgTime duration = (LgTime)luaL_checknumber(L, 1);
	int cols = 16, rows = 12, waves = 4;
	float amplitude = 20;

	if (lua_gettop(L) >= 2) cols = luaL_checkint(L, 2);
	if (lua_gettop(L) >= 3) rows = luaL_checkint(L, 3);
	if (lua_gettop(L) >= 4) waves = luaL_checkint(L, 4);
	if (lua_gettop(L) >= 5) amplitude = (float)luaL_checknumber(L, 4);

	pushAction(L, LgActionWave3DCreate(duration, cols, rows, waves, amplitude), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_lens3d(lua_State *L) {
	LgTime duration = (LgTime)luaL_checknumber(L, 1);
	int cols = 16, rows = 12;
	struct LgSize viewsize;
	struct LgVector2 center;
	struct LgVector2 defaultCenter;
	float radius, lensEffect;

	LgDirectorViewSize(&viewsize);
	defaultCenter.x = (float)viewsize.width / 2;
	defaultCenter.y = (float)viewsize.height / 2;
	LgCopyMemory(&center, &defaultCenter, sizeof(defaultCenter));
	radius = (float)LgMin(viewsize.width / 3, viewsize.height / 3);
	lensEffect = 0.7f;

	if (lua_gettop(L) >= 2) cols = luaL_checkint(L, 2);
	if (lua_gettop(L) >= 3) rows = luaL_checkint(L, 3);
	if (lua_gettop(L) >= 5) {
		center.x = (float)luaL_checknumber(L, 4);
		center.x = (float)luaL_checknumber(L, 5);
	}
	if (lua_gettop(L) >= 6) radius = (float)luaL_checknumber(L, 6);
	if (lua_gettop(L) >= 7) lensEffect = (float)luaL_checknumber(L, 7);

	pushAction(L, LgActionLens3DCreate(duration, cols, rows, &center, radius, lensEffect), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_ripple3d(lua_State *L) {
	LgTime duration = (LgTime)luaL_checknumber(L, 1);
	int cols = 16, rows = 12;
	struct LgSize viewsize;
	struct LgVector2 center;
	struct LgVector2 defaultCenter;
	float radius, amplitude;
	int waves = 4;

	LgDirectorViewSize(&viewsize);
	defaultCenter.x = (float)viewsize.width / 2;
	defaultCenter.y = (float)viewsize.height / 2;
	LgCopyMemory(&center, &defaultCenter, sizeof(defaultCenter));
	radius = (float)LgMin(viewsize.width / 3, viewsize.height / 3);
	amplitude = 60.0f;

	if (lua_gettop(L) >= 2) cols = luaL_checkint(L, 2);
	if (lua_gettop(L) >= 3) rows = luaL_checkint(L, 3);
	if (lua_gettop(L) >= 5) {
		center.x = (float)luaL_checknumber(L, 4);
		center.x = (float)luaL_checknumber(L, 5);
	}

	if (lua_gettop(L) >= 6) radius = (float)luaL_checknumber(L, 6);
	if (lua_gettop(L) >= 7) waves = luaL_checkint(L, 7);
	if (lua_gettop(L) >= 8) amplitude = (float)luaL_checknumber(L, 8);

	pushAction(L, LgActionRipple3DCreate(duration, cols, rows, &center, radius, waves, amplitude), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_liquid3d(lua_State *L) {
	LgTime duration = (LgTime)luaL_checknumber(L, 1);
	int cols = 16, rows = 12, waves = 4;
	float amplitude = 20;

	if (lua_gettop(L) >= 2) cols = luaL_checkint(L, 2);
	if (lua_gettop(L) >= 3) rows = luaL_checkint(L, 3);
	if (lua_gettop(L) >= 4) waves = luaL_checkint(L, 4);
	if (lua_gettop(L) >= 5) amplitude = (float)luaL_checknumber(L, 4);

	pushAction(L, LgActionLiquid3DCreate(duration, cols, rows, waves, amplitude), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_shaky(lua_State *L) {
	LgTime duration = (LgTime)luaL_checknumber(L, 1);
	int cols = 16, rows = 12, range = 1;
	LgBool shakeZ = LgFalse;

	if (lua_gettop(L) >= 2) cols = luaL_checkint(L, 2);
	if (lua_gettop(L) >= 3) rows = luaL_checkint(L, 3);
	if (lua_gettop(L) >= 4) range = luaL_checkint(L, 4);
	if (lua_gettop(L) >= 5) shakeZ = (LgBool)lua_toboolean(L, 5);

	pushAction(L, LgActionShakyYCreate(duration, cols, rows, range, shakeZ), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_fadeoutlt(lua_State *L) {
	LgTime duration = (LgTime)luaL_checknumber(L, 1);
	int cols = 16, rows = 12;

	if (lua_gettop(L) >= 2) cols = luaL_checkint(L, 2);
	if (lua_gettop(L) >= 3) rows = luaL_checkint(L, 3);

	pushAction(L, LgActionFadeOutLTCreate(duration, cols, rows), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_fadeoutrb(lua_State *L) {
	LgTime duration = (LgTime)luaL_checknumber(L, 1);
	int cols = 16, rows = 12;

	if (lua_gettop(L) >= 2) cols = luaL_checkint(L, 2);
	if (lua_gettop(L) >= 3) rows = luaL_checkint(L, 3);

	pushAction(L, LgActionFadeOutRBCreate(duration, cols, rows), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_fadeoutup(lua_State *L) {
	LgTime duration = (LgTime)luaL_checknumber(L, 1);
	int cols = 16, rows = 12;

	if (lua_gettop(L) >= 2) cols = luaL_checkint(L, 2);
	if (lua_gettop(L) >= 3) rows = luaL_checkint(L, 3);

	pushAction(L, LgActionFadeOutUPCreate(duration, cols, rows), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_fadeoutdown(lua_State *L) {
	LgTime duration = (LgTime)luaL_checknumber(L, 1);
	int cols = 16, rows = 12;

	if (lua_gettop(L) >= 2) cols = luaL_checkint(L, 2);
	if (lua_gettop(L) >= 3) rows = luaL_checkint(L, 3);

	pushAction(L, LgActionFadeOutDOWNCreate(duration, cols, rows), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_splitcols(lua_State *L) {
	LgTime duration = (LgTime)luaL_checknumber(L, 1);
	int cols = 3;

	if (lua_gettop(L) >= 2) cols = luaL_checkint(L, 2);
	pushAction(L, LgActionSplitColsCreate(duration, cols), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static int action_splitrows(lua_State *L) {
	LgTime duration = (LgTime)luaL_checknumber(L, 1);
	int rows = 3;

	if (lua_gettop(L) >= 2) rows = luaL_checkint(L, 2);
	pushAction(L, LgActionSplitRowsCreate(duration, rows), LGACTIONINTERVAL_HANDLE);
	return 1;
}

static struct luaL_Reg action_functions[] = {
	// ---------------------------------------------
	{"clone", action_clone},
	{"loop", action_loop},
	{"loop_forever", action_loop_forever},
	{"sequence", action_sequence},
	{"call", action_call},
	{"interval", action_interval},
	{"camera", action_camera},
	{"moveto", action_moveto},
	{"moveby", action_moveby},
	{"bezierto", action_bezierto},
	{"bezierby", action_bezierby},
	{"scaleto", action_scaleto},
	{"scaleby", action_scaleby},
	{"rotateto", action_rotateto},
	{"rotateby", action_rotateby},
	{"blink", action_blink},
	{"fadein", action_fadein},
	{"fadeout", action_fadeout},
	{"delay", action_delay},
	{"jump", action_jump},
	{"speed", action_speed},
	{"range", action_range},
	{"spawn", action_spawn},

	// ---------------------------------------------
	{"ease_in", action_easein},
	{"ease_out", action_easeout},
	{"ease_inout", action_easeinout},
	{"easeexp_in", action_easeexpin},
	{"easeexp_out", action_easeexpout},
	{"easeexp_inout", action_easeexpinout},
	{"easesine_in", action_easesinein},
	{"easesine_out", action_easesineout},
	{"easesine_inout", action_easesineinout},
	{"easeelastic_in", action_easeelasticin},
	{"easeelastic_out", action_easeelasticout},
	{"easeelastic_inout", action_easeelasticinout},
	{"easebounce_in", action_easebouncein},
	{"easebounce_out", action_easebounceout},
	{"easebounce_inout", action_easebounceinout},
	{"easeback_in", action_easebackin},
	{"easeback_out", action_easebackout},
	{"easeback_inout", action_easebackinout},

	// ---------------------------------------------
	{"wave3d", action_wave3d},
	{"lens3d", action_lens3d},
	{"ripple3d", action_ripple3d},
	{"liquid3d", action_liquid3d},

	// ---------------------------------------------
	{"shaky", action_shaky},
	{"fadeout_lt", action_fadeoutlt},
	{"fadeout_rb", action_fadeoutrb},
	{"fadeout_up", action_fadeoutup},
	{"fadeout_down", action_fadeoutdown},
	{"split_cols", action_splitcols},
	{"split_rows", action_splitrows},
	{NULL, NULL},
};

// soundplayer ------------------------------------------------------------------------------------------------
static int soundplayer_play_music(lua_State *L) {
	const char *filename = luaL_checkstring(L, 1);
	
	if (lua_isfunction(L, 2)) {
		int ref;
		
		lua_pushvalue(L, 2);
		ref = luaL_ref(L, LUA_REGISTRYINDEX);
		LgSoundPlayerPlayBackgroundMusic(filename, slotCallFunction, destroyLuaRef, slotCloneLuaRef, (void *)ref);
	} else {
		LgSoundPlayerPlayBackgroundMusic(filename, SLOT_PARAMS_VALUE_NULL);
	}
	
	return 0;
}

static int soundplayer_stop_music(lua_State *L) {
	LgSoundPlayerStopBackgroundMusic();
	return 0;
}

static int soundplayer_play_sound(lua_State *L) {
	const char *filename = luaL_checkstring(L, 1);
	lua_pushunsigned(L, LgSoundPlayerPlayEffect(filename));
	return 1;
}

static int soundplayer_stop_sound(lua_State *L) {
	LgSoundId id = luaL_checkunsigned(L, 1);
	LgSoundPlayerStopEffect(id);
	return 0;
}

static int soundplayer_stop_all_sound(lua_State *L) {
	LgSoundPlayerStopAllEffects();
	return 0;
}

static int soundplayer_music_gain(lua_State *L) {
	lua_pushnumber(L, LgSoundPlayerMusicGain());
	return 1;
}

static int soundplayer_set_music_gain(lua_State *L) {
	lua_Number gain = luaL_checknumber(L, 1);
	LgSoundPlayerSetMusicGain((float)gain);
	return 0;
}

static int soundplayer_sound_gain(lua_State *L) {
	lua_pushnumber(L, LgSoundPlayerEffectGain());
	return 1;
}

static int soundplayer_set_sound_gain(lua_State *L) {
	lua_Number gain = luaL_checknumber(L, 1);
	LgSoundPlayerSetEffectGain((float)gain);
	return 0;
}

// networking ------------------------------------------------------------------------------------------------

static int parse_url(lua_State *L) {
	UriParserStateA stateA;
	UriUriA uriA;

	stateA.uri = &uriA;
	LG_CHECK_ERROR(uriParseUriA(&stateA, luaL_checkstring(L, 1)) == 0);

	lua_newtable(L);

	lua_pushlstring(L, uriA.scheme.first, uriA.scheme.afterLast - uriA.scheme.first);
	lua_setfield(L, -2, "scheme");

	lua_pushlstring(L, uriA.hostText.first, uriA.hostText.afterLast - uriA.hostText.first);
	lua_setfield(L, -2, "host");

	lua_pushinteger(L, uriA.portText.first ? atoi(uriA.portText.first) : 80);
	lua_setfield(L, -2, "port");

	if (uriA.pathHead) {
		lua_pushlstring(L, uriA.pathHead->text.first, uriA.pathTail->text.afterLast - uriA.pathHead->text.first);
		lua_setfield(L, -2, "path");
	}

	if (uriA.query.first) {
		lua_pushlstring(L, uriA.query.first, uriA.query.afterLast - uriA.query.first);
		lua_setfield(L, -2, "query");
	}

	uriFreeUriMembersA(&uriA);
	return 1;

_error:
	lua_pushnil(L);
	return 1;
}

static int url_encode(lua_State *L) {
	const char *text = luaL_checkstring(L, 1);
	char *temp = (char *)LgMalloc(strlen(text) * 3 + 1);
	char *p = uriEscapeA(text, temp, 0, 0);
	lua_pushlstring(L, temp, p - temp);
	LgFree(temp);
	return 1;
}

static int url_decode(lua_State *L) {
	char *temp = strdup(luaL_checkstring(L, 1));
	const char *p = uriUnescapeInPlaceA(temp);
	lua_pushlstring(L, temp, p - temp);
	LgFree(temp);
	return 1;
}

typedef struct LgHttpContext {
	LgHttpP	http;
	int		callback;
	char *	responseData;
	int		responseDataLen;
	char *	targetFile;
	FILE *	fp;
}*LgHttpContextP;

static void destroyHttpContext(LgHttpContextP context) {
	if (context->http) LgSchedulerReleaseAtNextFrame(context->http); // 在回调函数里面删除http对象不安全
	if (context->callback) luaL_unref(LgScriptEnvState(), LUA_REGISTRYINDEX, context->callback);
	if (context->responseData) LgFree(context->responseData);
	if (context->targetFile) LgFree(context->targetFile);
	if (context->fp) fclose(context->fp);
	LgFree(context);
}

static void httpResponseData(void *param, void *userdata) {
	LgHttpContextP context = (LgHttpContextP)userdata;
	LgHttpResponseDataEventP evt = (LgHttpResponseDataEventP)param;
	lua_State *L = LgScriptEnvState();

	if (context->targetFile) {
		if (!context->fp) context->fp = fopen(context->targetFile, "wb+");
		if (context->fp) fwrite(evt->data, evt->size, 1, context->fp);
		if (evt->finished) {
			fclose(context->fp);
			context->fp = NULL;
		}
	} else {
		if (evt->size > 0) {
			context->responseData = (char *)LgRealloc(context->responseData, context->responseDataLen + evt->size);
			LgCopyMemory(context->responseData + context->responseDataLen, evt->data, evt->size);
			context->responseDataLen += evt->size;
		}

		if (context->callback) {
			if (evt->finished) {
				// 发送完成的loading消息
				lua_rawgeti(L, LUA_REGISTRYINDEX, context->callback);
				lua_pushstring(L, "loading");
				lua_pushnumber(L, 1);
				if (lua_pcall(L, 2, 0, 0)) LgScriptShowError();
				
				// 发送data消息
				lua_rawgeti(L, LUA_REGISTRYINDEX, context->callback);
				lua_pushstring(L, "data");
				if (context->responseData) lua_pushlstring(L, context->responseData, context->responseDataLen);
				else lua_pushnil(L);
				if (lua_pcall(L, 2, 0, 0)) LgScriptShowError();
				destroyHttpContext(context);
			} else {
				lua_rawgeti(L, LUA_REGISTRYINDEX, context->callback);
				lua_pushstring(L, "loading");
				lua_pushnumber(L, evt->ratio);
				if (lua_pcall(L, 2, 0, 0)) LgScriptShowError();
			}
		}
	}
}

static void httpResponseError(void *param, void *userdata) {
	LgHttpContextP context = (LgHttpContextP)userdata;
	enum LgHttpErrorType error = (enum LgHttpErrorType)param;
	lua_State *L = LgScriptEnvState();

	lua_rawgeti(L, LUA_REGISTRYINDEX, context->callback);
	lua_pushstring(L, "error");
	lua_pushnumber(L, error);
	if (lua_pcall(L, 2, 0, 0)) LgScriptShowError();

	destroyHttpContext(context);
}

static int http_get(lua_State *L) {
	const char *url = luaL_checkstring(L, 1);
	LgHttpContextP context = NULL;

	context = (LgHttpContextP)LgMallocZ(sizeof(struct LgHttpContext));
	context->http = LgHttpCreateR();

	if (lua_gettop(L) >= 2 && lua_isstring(L, 2)) {
		context->targetFile = strdup(luaL_checkstring(L, 2));
	}
	if (lua_gettop(L) >= 3) {
		if (!lua_isfunction(L, 3)) return 0;
		lua_pushvalue(L, 3);
		context->callback = luaL_ref(L, LUA_REGISTRYINDEX);
	}
	if (lua_gettop(L) >= 4) {
		// 请求字段表
		LG_CHECK_ERROR(lua_istable(L, 4));
		lua_pushnil(L);
		while(lua_next(L, 4) != 0) {
			LG_CHECK_ERROR(lua_isstring(L, -1));
			LG_CHECK_ERROR(lua_isstring(L, -2));
			LgHttpSetRequestField(context->http, luaL_checkstring(L, -2), luaL_checkstring(L, -1));
			lua_pop(L, 1);
		}
	}

	LgSlotAssign(LgHttpSlotResponseData(context->http), httpResponseData, NULL, NULL, context);
	LgSlotAssign(LgHttpSlotError(context->http), httpResponseError, NULL, NULL, context);
	LG_CHECK_ERROR(LgHttpGet(context->http, url));
	lua_pushboolean(L, LgTrue);
	return 1;

_error:
	if (context) destroyHttpContext(context);
	lua_pushboolean(L, LgFalse);
	return 1;
}

static int http_post(lua_State *L) {
	const char *url = luaL_checkstring(L, 1);
	const char *postData = luaL_checkstring(L, 2);
	int postDataLength = luaL_len(L, 2);
	LgHttpContextP context = NULL;

	context = (LgHttpContextP)LgMallocZ(sizeof(struct LgHttpContext));
	context->http = LgHttpCreateR();

	if (lua_gettop(L) >= 3 && lua_isstring(L, 3)) {
		context->targetFile = strdup(luaL_checkstring(L, 3));
	}
	if (lua_gettop(L) >= 4) {
		if (!lua_isfunction(L, 4)) return 0;
		lua_pushvalue(L, 4);
		context->callback = luaL_ref(L, LUA_REGISTRYINDEX);
	}
	if (lua_gettop(L) >= 5) {
		// 请求字段表
		LG_CHECK_ERROR(lua_istable(L, 5));
		lua_pushnil(L);
		while(lua_next(L, 5) != 0) {
			LG_CHECK_ERROR(lua_isstring(L, -1));
			LG_CHECK_ERROR(lua_isstring(L, -2));
			LgHttpSetRequestField(context->http, luaL_checkstring(L, -2), luaL_checkstring(L, -1));
			lua_pop(L, 1);
		}
	}

	LgSlotAssign(LgHttpSlotResponseData(context->http), httpResponseData, NULL, NULL, context);
	LgSlotAssign(LgHttpSlotError(context->http), httpResponseError, NULL, NULL, context);
	LG_CHECK_ERROR(LgHttpPost(context->http, url, postData, postDataLength));
	lua_pushboolean(L, LgTrue);
	return 1;

_error:
	if (context) destroyHttpContext(context);
	lua_pushboolean(L, LgFalse);
	return 1;
}

// resource ------------------------------------------------------------------------------------------------

static int add_resource(lua_State *L) {
	LgBool head = LgFalse;
	const char *filename = luaL_checkstring(L, 1);
	if (lua_gettop(L) >= 2) head = luaL_checkint(L, 2);
	lua_pushboolean(L, LgArchiveAddSource(filename, head));
	return 1;
}

static int remove_resource(lua_State *L) {
	LgBool head = LgFalse;
	const char *filename = luaL_checkstring(L, 1);
	if (lua_gettop(L) >= 2) head = luaL_checkint(L, 2);
	lua_pushboolean(L, LgArchiveRemoveSource(filename));
	return 1;
}

static int resource_list(lua_State *L) {
	LgListNodeP node = NULL;
	const char *filename;
	int i = 1;

	lua_newtable(L);
	while(LgArchiveNextSource(&node, &filename)) {
		lua_pushstring(L, filename);
		lua_rawseti(L, -2, i++);
	}

	return 1;
}

static int resource_path(lua_State *L) {
	lua_pushstring(L, LgResourceDirectory());
	return 1;
}

static int document_path(lua_State *L) {
	lua_pushstring(L, LgPlatformDocumentDirectory());
	return 1;
}

static void slotCacheFinished(void *param, void *userdata) {
	if ((int)userdata) {
		LgAsyncLoadResultP result = (LgAsyncLoadResultP)param;
		lua_rawgeti(LgScriptEnvState(), LUA_REGISTRYINDEX, (int)userdata);
		lua_pushstring(LgScriptEnvState(), result->filename);
		lua_pushboolean(LgScriptEnvState(), result->succeeded);
		lua_pushnumber(LgScriptEnvState(), result->time);
		if (lua_pcall(LgScriptEnvState(), 3, 0, 0)) {
			LgScriptShowError();
		}
	}
}

static int cache_resource(lua_State *L) {
	const char *filename = luaL_checkstring(L, 1);
	int type = luaL_checkint(L, 2);
	LgSlot slotFinished;
	
	if (lua_gettop(L) > 2 && lua_isfunction(L, 3)) {
		lua_pushvalue(L, 3);
		slotFinished.action = slotCacheFinished;
		slotFinished.cloneUserData = slotCloneLuaRef;
		slotFinished.destroyUserData = destroyLuaRef;
		slotFinished.userdata = (void *)luaL_ref(L, LUA_REGISTRYINDEX);
	} else {
		LgZeroMemory(&slotFinished, sizeof(slotFinished));
	}
	
	switch(type) {
	case 1:
		LgResourceManagerAsyncLoadTexture(filename, &slotFinished);
		break;
	case 2:
		LgResourceManagerAsyncLoadSound(filename, &slotFinished);
		break;
	}
	
	return 0;
}

// log ------------------------------------------------------------------------------------------------
static int _log(enum LgLogType type, lua_State *L) {
	lua_Debug ar;
	int n = lua_gettop(L), i;
	const char *text;
	
	if (n == 1) {
		text = lua_tostring(L, 1);
	} else {
		lua_getglobal(L, "string");
		lua_getfield(L, -1, "format");
		for (i = 0; i < n; i++) lua_pushvalue(L, i + 1);
		lua_pcall(L, n, 1, 0);
		text = luaL_checkstring(L, -1);
	}
	
	lua_getstack(L, 1, &ar);
	lua_getinfo(L, "Sl", &ar);
	LgLogSystemLog(type, ar.source, ar.currentline, "%s", text);
	return 0;
}

static int log_info(lua_State *L) {
	return _log(LgLogINFO, L);
}

static int log_warning(lua_State *L) {
	return _log(LgLogWARN, L);
}

static int log_error(lua_State *L) {
	return _log(LgLogERR, L);
}

// log ------------------------------------------------------------------------------------------------
static int net_open(lua_State *L) {
	const char *url = luaL_checkstring(L, 1);
	LgSlot slot;

	LgZeroMemory(&slot, sizeof(slot));

	if (lua_gettop(L) >= 2) {
		if (lua_isfunction(L, 2)) {
			lua_pushvalue(L, 2);
			slot.userdata = (void*)luaL_ref(L, LUA_REGISTRYINDEX);
			slot.action = slotCallFunction;
			slot.cloneUserData = slotCloneLuaRef;
			slot.destroyUserData = destroyLuaRef;
		}
	}

	lua_pushboolean(L, LgNetClientOpen(url, &slot));
	return 1;
}

static int net_close(lua_State *L) {
	LgNetClientClose();
	return 0;
}

static json_t *toJson(lua_State *L, int idx) {
	if (!lua_isnil(L, idx)) {
		json_t *json;

		lua_getglobal(L, "cjson");
		lua_getfield(L, -1, "encode");
		lua_pushvalue(L, idx);
		lua_pcall(L, 1, 1, 0);
		json = json_loads(luaL_checkstring(L, -1), 0, NULL);
		lua_pop(L, 2);
		return json;
	}

	return NULL;
}

static void jsonToLua(lua_State *L, json_t *json) {
	char *jsonstr;

	if (json == NULL) {
		lua_pushnil(L);
		return;
	} 

	lua_getglobal(L, "cjson");
	lua_getfield(L, -1, "decode");
	jsonstr = json_dumps(json, JSON_COMPACT);
	lua_pushstring(L, jsonstr);
	lua_pcall(L, 1, 1, 0);
	lua_remove(L, -2);
	free(jsonstr);
}

static void slotPomeloResponse(void *param, void *userdata) {
	if ((int)userdata) {
		json_t *json = (json_t *)param;
		lua_rawgeti(LgScriptEnvState(), LUA_REGISTRYINDEX, (int)userdata);
		jsonToLua(LgScriptEnvState(), json);
		if (lua_pcall(LgScriptEnvState(), 1, 0, 0)) {
			LgScriptShowError();
		}
	}
}

static int net_request(lua_State *L) {
	const char *route = luaL_checkstring(L, 1);
	LgBool hasMsg = lua_gettop(L) >= 2 && !lua_isnil(L, 2), hasCallback = lua_gettop(L) >= 3;
	json_t *msg = NULL;
	LgSlot slot;

	LgZeroMemory(&slot, sizeof(slot));

	if (hasMsg) msg = toJson(L, 2);
	if (hasCallback) {
		if (lua_isfunction(L, 3)) {
			lua_pushvalue(L, 3);
			slot.userdata = (void*)luaL_ref(L, LUA_REGISTRYINDEX);
			slot.action = slotPomeloResponse;
			slot.cloneUserData = slotCloneLuaRef;
			slot.destroyUserData = destroyLuaRef;
		}
	}

	lua_pushboolean(L, LgNetClientRequest(route, msg, &slot));
	if (msg != NULL) json_delete(msg);
	return 1;
}

static int net_notify(lua_State *L) {
	const char *route = luaL_checkstring(L, 1);
	LgBool hasMsg = lua_gettop(L) >= 2 && !lua_isnil(L, 2);
	json_t *msg = NULL;

	if (hasMsg) msg = toJson(L, 2);
	lua_pushboolean(L, LgNetClientNotify(route, msg));
	if (msg != NULL) json_delete(msg);
	return 1;
}

static void slotPomeloError(void *param, void *userdata) {
	if ((int)userdata) {
		int err = (int)param;
		lua_rawgeti(LgScriptEnvState(), LUA_REGISTRYINDEX, (int)userdata);
		lua_pushinteger(LgScriptEnvState(), err);
		if (lua_pcall(LgScriptEnvState(), 1, 0, 0)) {
			LgScriptShowError();
		}
	}
}

static int net_set_error(lua_State *L) {
	if (lua_isnil(L, 1)) {
		LgSlotClear(LgNetClientSlotError());
		return 0;
	}
	if (!lua_isfunction(L, 1)) return 0;
	lua_pushvalue(L, 1);
	LgSlotAssign(LgNetClientSlotError(), 
		slotPomeloError, destroyLuaRef, slotCloneLuaRef, 
		(void*)luaL_ref(L, LUA_REGISTRYINDEX));
	return 0;
}

static void slotPomeloPush(void *param, void *userdata) {
	if ((int)userdata) {
		LgNetClientPushMessageP push = (LgNetClientPushMessageP)param;
		lua_rawgeti(LgScriptEnvState(), LUA_REGISTRYINDEX, (int)userdata);
		lua_pushstring(LgScriptEnvState(), push->route);
		jsonToLua(LgScriptEnvState(), push->json);
		if (lua_pcall(LgScriptEnvState(), 2, 0, 0)) {
			LgScriptShowError();
		}
	}
}

static int net_set_push(lua_State *L) {
	if (lua_isnil(L, 1)) {
		LgSlotClear(LgNetClientSlotPush());
		return 0;
	}
	if (!lua_isfunction(L, 1)) return 0;
	lua_pushvalue(L, 1);
	LgSlotAssign(LgNetClientSlotPush(), 
		slotPomeloPush, destroyLuaRef, slotCloneLuaRef, 
		(void*)luaL_ref(L, LUA_REGISTRYINDEX));
	return 0;
}

static int net_is_opened(lua_State *L) {
	lua_pushboolean(L, LgNetClientIsOpened());
	return 1;
}

// payment --------------------------------------------------------------------------------------

static int payment_can_make_payments(lua_State *L) {
	lua_pushboolean(L, LgPaymentCanMakePayments());
	return 1;
}

static int payment_purchase(lua_State *L) {
	lua_pushboolean(L, LgPaymentPurchase(luaL_checkstring(L, 1)));
	return 1;
}

static void slotPaymentEvent(void *param, void *userdata) {
	if ((int)userdata) {
		LgPaymentEventP evt = (LgPaymentEventP)param;
		lua_rawgeti(LgScriptEnvState(), LUA_REGISTRYINDEX, (int)userdata);
		lua_pushinteger(LgScriptEnvState(), (int)evt->type);
		
		if (evt->receipt) {
			jsonToLua(LgScriptEnvState(), evt->receipt);
		} else {
			lua_pushnil(LgScriptEnvState());
		}
		
		if (lua_pcall(LgScriptEnvState(), 2, 0, 0)) {
			LgScriptShowError();
		}
	}
}

static int payment_set_event(lua_State *L) {
	if (lua_isnil(L, 1)) {
		LgSlotClear(LgPaymentSlot());
		return 0;
	}
	if (!lua_isfunction(L, 1)) return 0;
	lua_pushvalue(L, 1);
	LgSlotAssign(LgPaymentSlot(),
				 slotPaymentEvent, destroyLuaRef, slotCloneLuaRef,
				 (void*)luaL_ref(L, LUA_REGISTRYINDEX));
	return 0;
}

// string --------------------------------------------------------------------------------------
static int string_strtrim(lua_State *L) {
	int front = 0, back;
	const char *str = luaL_checklstring(L, 1, (size_t *)(&back));
	const char *del = luaL_optstring(L, 2, "\t\n\r ");
	--back;
	
	while (front <= back && strchr(del, str[front]))
		++front;
	while (back > front && strchr(del, str[back]))
		--back;
	
	lua_pushlstring(L, &str[front], back - front + 1);
	return 1;
}

static int string_strsplit(lua_State *L) {
	const char *sep = luaL_checkstring(L, 2);
	const char *str = luaL_checkstring(L, 1);
	int limit = luaL_optint(L, 3, 0);
	int count = 0;
	/* Set the stack to a predictable size */
	lua_settop(L, 0);
	/* Initialize the result count */
	/* Tokenize the string */
	if(!limit || limit > 1) {
		const char *end = str;
		while(*end) {
			int issep = 0;
			const char *s = sep;
			for(; *s; ++s) {
				if(*s == *end) {
					issep = 1;
					break;
				}
			}
			if(issep) {
				luaL_checkstack(L, count+1, "too many results");
				lua_pushlstring(L, str, (end-str));
				++count;
				str = end+1;
				if(count == (limit-1)) {
					break;
				}
			}
			++end;
		}
	}
	/* Add the remainder */
	luaL_checkstack(L, count+1, "too many results");
	lua_pushstring(L, str);
	++count;
	/* Return with the number of values found */
	return count;
}

static int string_strjoin(lua_State *L) {
	size_t seplen;
	int entries;
	const char *sep = luaL_checklstring(L, 1, &seplen);
	
	/* Guarantee we have 1 stack slot free */
	lua_remove(L, 1);
	
	entries = lua_gettop(L);
	
	if (seplen == 0) /* If there's no seperator, then this is the same as a concat */
		lua_concat(L, entries);
	else if (entries == 0) /* If there are no entries then we can't concatenate anything */
		lua_pushstring(L, "");
	else if (entries == 1) /* If there's only one entry, just return it */
		;
	else {
		luaL_Buffer b;
		int i;
		
		/* Set up buffer to store resulting string */
		luaL_buffinit(L, &b);
		for(i = 1; i <= entries; ++i) {
			/* Push the current entry and add it to the buffer */
			lua_pushvalue(L, i);
			luaL_addvalue(&b);
			/* Add the separator to the buffer */
			if (i < entries) {
				luaL_addlstring(&b, sep, seplen);
			}
		}
		luaL_pushresult(&b);
	}
	
	return 1;
}

static int string_strconcat(lua_State *L) {
	lua_concat(L, lua_gettop(L));
	return 1;
}

static int string_strreplace(lua_State *L) {
	const char *subject = luaL_checkstring(L, 1);
	const char *search = luaL_checkstring(L, 2);
	const char *replace = luaL_checkstring(L, 3);
	
	char *replaced = (char*)calloc(1, 1), *temp = NULL;
	const char *p = subject, *p3 = subject, *p2;
	int  found = 0;
	int count = 0;
	
	while ( (p = strstr(p, search)) != NULL) {
		found = 1;
		count++;
		temp = realloc(replaced, strlen(replaced) + (p - p3) + strlen(replace) + 1);
		if (temp == NULL) {
			free(replaced);
			luaL_error(L, "Unable to allocate memory");
			return 0;
		}
		replaced = temp;
		strncat(replaced, p - (p - p3), p - p3);
		strcat(replaced, replace);
		p3 = p + strlen(search);
		p += strlen(search);
		p2 = p;
	}
	
	if (found == 1) {
		if (strlen(p2) > 0) {
			temp = realloc(replaced, strlen(replaced) + strlen(p2) + 1);
			if (temp == NULL) {
				free(replaced);
				luaL_error(L, "Unable to allocate memory");
				return 0;
			}
			replaced = temp;
			strcat(replaced, p2);
		}
	} else {
		temp = realloc(replaced, strlen(subject) + 1);
		if (temp != NULL) {
			replaced = temp;
			strcpy(replaced, subject);
		}
	}
	
	lua_pushstring(L, replaced);
	lua_pushinteger(L, count);
	return 2;
}


// ------------------------------------------------------------------------------------------------
static struct luaL_Reg functions[] = {
	// color
	{"rgb", _rgb},
	{"argb", _argb},

	// cxform
	{"cxform", cxform},

	// point
	{"point", point},

	// rect
	{"rect", rect},

	// director
	{"push_scene", director_push},
	{"pop_scene", director_pop},
	{"replace_scene", director_replace},
	{"current_scene", director_current_scene},
	{"set_viewsize", director_set_viewsize},
	{"viewsize", director_viewsize},
	{"set_speed", director_set_speed},
	{"speed", director_speed},
	{"pause", director_pause},
	{"resume", director_resume},
	{"is_paused", director_is_paused},
	{"set_camera", director_set_camera},

	// scheduler
	{"schedule", scheduler_schedule},
	
	// node
	{"new_node", new_node},

	// scene
	{"new_scene", new_scene},

	// layer
	{"new_layer", new_layer},

	// color layer
	{"new_colorlayer", new_colorlayer},

	// gradient layer
	{"new_gradientlayer", new_gradientlayer},

	// tilemaplayer
	{"new_tilemaplayer", new_tilemaplayer},

	// sprite
	{"new_sprite", new_sprite},

	// label
	{"new_label", new_label},

	// textfield
	{"new_textfield", new_textfield},

	// drawnode
	{"new_drawnode", new_drawnode},

	// clipnode
	{"new_clipnode", new_clipnode},

	// transition
	{"transition", transition},

	// particle system
	{"new_particle_system", new_particle_system},

	// batchnode
	{"new_batchnode", new_batchnode},

	// animation
	{"new_animation", new_animation},

	// soundplayer
	{"play_music", soundplayer_play_music},
	{"stop_music", soundplayer_stop_music},
	{"play_sound", soundplayer_play_sound},
	{"stop_sound", soundplayer_stop_sound},
	{"stop_all_sound", soundplayer_stop_all_sound},
	{"music_gain", soundplayer_music_gain},
	{"set_music_gain", soundplayer_set_music_gain},
	{"sound_gain", soundplayer_sound_gain},
	{"set_sound_gain", soundplayer_set_sound_gain},

	// networking
	{"parse_url", parse_url},
	{"url_encode", url_encode},
	{"url_decode", url_decode},
	{"http_get", http_get},
	{"http_post", http_post},

	// resource
	{"add_resource", add_resource},
	{"remove_resource", remove_resource},
	{"resource_list", resource_list},
	{"resource_path", resource_path},
	{"document_path", document_path},
	{"cache_resource", cache_resource},

	// log
	{"log_info", log_info},
	{"log_warning", log_warning},
	{"log_error", log_error},

	// pomelo
	{"net_open", net_open},
	{"net_close", net_close},
	{"net_request", net_request},
	{"net_notify", net_notify},
	{"net_set_error", net_set_error},
	{"net_set_push", net_set_push},
	{"net_is_opened", net_is_opened},
	
	// payment
	{"can_make_payments", payment_can_make_payments},
	{"purchase", payment_purchase},
	{"payment_set_event", payment_set_event},

	// string
	{"strtrim", string_strtrim},
	{"strsplit", string_strsplit},
	{"strjoin", string_strjoin},
	{"strconcat", string_strconcat},
	{"strreplace", string_strreplace},

	{NULL, NULL},
};

LgBool LgLuaApiInstall() {
	lua_State *L = LgScriptEnvState();

	// 创建元表
	createMetaTable(L, LGCOLOR_HANDLE,				NULL,				color_methods);
	createMetaTable(L, LGCXFORM_HANDLE,				NULL,				cxform_methods);
	createMetaTable(L, LGPOINT_HANDLE,				NULL,				point_methods);
	createMetaTable(L, LGRECT_HANDLE,				NULL,				rect_methods);
	createMetaTable(L, LGACTION_HANDLE,				NULL,				action_methods);
	createMetaTable(L, LGACTIONINTERVAL_HANDLE,		LGACTION_HANDLE,	NULL);
	createMetaTable(L, LGTOUCH_HANDLE,				NULL,				touch_methods);
	createMetaTable(L, LGKEYEVENT_HANDLE,			NULL,				keyevent_methods);
	createMetaTable(L, LGSCHEDULEITEM_HANDLE,		NULL,				schedule_item_methods);
	createMetaTable(L, LGNODE_HANDLE,				NULL,				node_methods);
	createMetaTable(L, LGSCENE_HANDLE,				LGNODE_HANDLE,		NULL);
	createMetaTable(L, LGLAYER_HANDLE,				LGNODE_HANDLE,		layer_methods);
	createMetaTable(L, LGCOLORLAYER_HANDLE,			LGLAYER_HANDLE,		colorlayer_methods);
	createMetaTable(L, LGGRADIENTLAYER_HANDLE,		LGLAYER_HANDLE,		NULL);
	createMetaTable(L, LGTILEMAPLAYER_HANDLE,		LGLAYER_HANDLE,		tilemaplayer_methods);
	createMetaTable(L, LGSPRITE_HANDLE,				LGNODE_HANDLE,		sprite_methods);
	createMetaTable(L, LGLABEL_HANDLE,				LGNODE_HANDLE,		label_methods);
	createMetaTable(L, LGTEXTFIELD_HANDLE,			LGNODE_HANDLE,		textfield_methods);
	createMetaTable(L, LGDRAWNODE_HANDLE,			LGNODE_HANDLE,		drawnode_methods);
	createMetaTable(L, LGCLIPNODE_HANDLE,			LGNODE_HANDLE,		clipnode_methods);
	createMetaTable(L, LGPARTICLESYSTEM_HANDLE,		LGNODE_HANDLE,		ps_methods);
	createMetaTable(L, LGBATCHNODE_HANDLE,			LGNODE_HANDLE,		NULL);
	createMetaTable(L, LGANIMATION_HANDLE,			LGNODE_HANDLE,		animation_methods);
	createMetaTable(L, LGGESTURERECOGNIZER_HANDLE,	NULL,				gesture_recognizer_methods);

	createMetaTable(L, LGCONTROLNODE_HANDLE,		LGNODE_HANDLE,			controlnode_methods);
	createMetaTable(L, LGBUTTON_HANDLE,				LGCONTROLNODE_HANDLE,	button_methods);
	createMetaTable(L, LGSCROLLVIEW_HANDLE,			LGCONTROLNODE_HANDLE,	scrollview_methods);
	createMetaTable(L, LGSLIDER_HANDLE,				LGCONTROLNODE_HANDLE,	slider_methods);
	createMetaTable(L, LGSWITCHER_HANDLE,			LGCONTROLNODE_HANDLE,	switcher_methods);
	
	// 创建lg命名空间
	lua_pushglobaltable(L);
	lua_newtable(L);
	lua_pushvalue(L, -1);
	lua_setfield(L, -3, "lg");
	luaL_setfuncs(L, functions, 0);

	lua_pushstring(L, LgCurrentPlatform());
	lua_setfield(L, -2, "platform");

	defineEnum(L, "touch_mode", 
		"none", LgNodeHitTestNone,
		"simple", LgNodeHitTestSimple,
		"exact", LgNodeHitTestExact,
		NULL);

	defineEnum(L, "touch_type", 
		"began", LgNodeTouchBegan,
		"moved", LgNodeTouchMoved,
		"ended", LgNodeTouchEnded,
		"cancelled", LgNodeTouchCancelled,
		NULL);

	defineEnum(L, "gesture_type", 
		"tap", LgGestureTap,
		"longpress", LgGestureLongPress,
		"pinch", LgGesturePinch,
		"swipe", LgGestureSwipe,
		NULL);
	
	defineEnum(L, "swipe_direction",
		"left", LgGestureSwipeLeft,
		"right", LgGestureSwipeRight,
		"up", LgGestureSwipeUp,
		"down", LgGestureSwipeDown,
		NULL);

	defineEnum(L, "draw_string_flags",
		"baseline", LgDrawStringBaseline,
		NULL
		);
	
	defineEnum(L, "res_type",
		"image", 1,
		"sound", 2,
		NULL);
	
	defineEnum(L, "mapobject_type",
		"rectangle", LgMapObjectRectangle,
		"ellipse", LgMapObjectEllipse,
		"polygon", LgMapObjectPolygon,
		"polyline", LgMapObjectPolyline,
		"tile", LgMapObjectTile,
		NULL);

	defineEnum(L, "animation_event_type", 
		"start", LgAnimationEventAnimationStart,
		"end", LgAnimationEventAnimationEnd,
		"complete", LgAnimationEventAnimationComplete,
		"custom", LgAnimationEventAnimationCustom,
		NULL);

	defineEnum(L, "net_error",
		"bad_response", LgNetClientErrorBadResponse,
		"handshake_version", LgNetClientErrorHandshake_Version,
		"handshake_unknown", LgNetClientErrorHandshake_Unknown,
		"kick_by_server", LgNetClientErrorKickByServer,
		"timeout", LgNetClientErrorTimeout,
		"reading", LgNetClientErrorReading,
		"writing", LgNetClientErrorWriting,
		NULL);
	
	defineEnum(L, "payment",
		"purchased", LgPaymentEventPurchased,
		"failed", LgPaymentEventFailed,
		"not_found", LgPaymentEventNotFound,
		NULL);

	defineEnum(L, "key", 
		"unknown", LgKeyUnknown,
		"up", LgKeyUp,
		"down", LgKeyDown,
		"left", LgKeyLeft,
		"right", LgKeyRight,
		"page_up", LgKeyPageUp,
		"page_down", LgKeyPageDown,
		"enter", LgKeyEnter,
		"back", LgKeyBack,
		"options", LgKeyOptions,

		"f1", LgKeyF1,
		"f2", LgKeyF2,
		"f3", LgKeyF3,
		"f4", LgKeyF4,
		"f5", LgKeyF5,
		"f6", LgKeyF6,
		"f7", LgKeyF7,
		"f8", LgKeyF8,
		"f9", LgKeyF9,
		"f10", LgKeyF10,
		"f11", LgKeyF11,
		"f12", LgKeyF12,
		NULL);
	
	// ui命名空间
	lua_newtable(L);
	lua_pushvalue(L, -1);
	lua_setfield(L, -3, "ui");
	luaL_setfuncs(L, ui_functions, 0);
	lua_pop(L, 1);

	// actions命名空间
	lua_newtable(L);
	lua_pushvalue(L, -1);
	lua_setfield(L, -3, "actions");
	luaL_setfuncs(L, action_functions, 0);
	lua_pop(L, 1);

	// gr命名空间
	lua_newtable(L);
	lua_pushvalue(L, -1);
	lua_setfield(L, -3, "gr");
	luaL_setfuncs(L, gesture_recognizer_functions, 0);
	lua_pop(L, 1);
		
	lua_pop(L, 2);
	return LgTrue;
}
