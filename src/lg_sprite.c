﻿#include "lg_sprite.h"
#include "lg_shapes.h"
#include "lg_director.h"
#include "lg_resourcemanager.h"
#include "lg_batchnode.h"
#include "lg_textureatlas.h"

// texture pack ----------------------------------------------------

LG_REFCNT_FUNCTIONS(LgTexturePack)

static void destroyTexturePack(LgTexturePackP tp) {
	if (tp->atlas) spAtlas_dispose(tp->atlas);
	LgFree(tp);
}

LgTexturePackP LgTexturePackCreateR(const char *filename) {
	LgTexturePackP texturePack = NULL;

	texturePack = (LgTexturePackP)LgMallocZ(sizeof(struct LgTexturePack));
	LG_CHECK_ERROR(texturePack);
	LG_REFCNT_INIT(texturePack, destroyTexturePack);

	texturePack->atlas = spAtlas_readAtlasFile(filename);
	LG_CHECK_ERROR(texturePack->atlas);

	return texturePack;

_error:
	if (texturePack) LgTexturePackRelease(texturePack);
	return NULL;
}


// sprite -----------------------------------------------------------

enum LgSprite9Character {
	LgSprite9_LT = 1,
	LgSprite9_LM,
	LgSprite9_LB,
	LgSprite9_MT,
	LgSprite9_MM,
	LgSprite9_MB,
	LgSprite9_RT,
	LgSprite9_RM,
	LgSprite9_RB,
};

typedef struct LgSpriteData {
	struct LgVector2	size;
	LgTextureP			texture;
	LgBool				isSprite9;

	// for sprite9
	struct LgPoint		insetLT, insetRB;
	LgTextureAtlasP		atlas;
	LgAtlasDrawBufferP	drawBuffer;

	// texture pack
	LgTexturePackP		texurePack;
	spAtlasRegion *		region;
	LgVertexBufferP		vb;
}*LgSpriteDataP;

static void makeBatchDirty(LgNodeP node) {
	LgNodeP parent = LgNodeParent(node);
	if (parent && LgNodeIsBatchNode(parent)) LgBatchNodeMakeDirty(parent);
}

static void makeSprite9DrawBuffer(LgSpriteDataP sd) {
	if (!sd->drawBuffer) {
		float lw, th, rw, bh, cw, ch;
		struct LgAabb box;

		sd->drawBuffer = LgTextureAtlasCreateDrawBufferR(sd->atlas);

		lw = (float)sd->insetLT.x, rw = (float)sd->insetRB.x;
		th = (float)sd->insetLT.y, bh = (float)sd->insetRB.y;
		cw = sd->size.x - lw - rw;
		ch = sd->size.y - th - bh;

		// lt
		box.x = 0, box.y = 0, box.width = lw, box.height = th;
		LgAtlasBufferDrawRect(sd->drawBuffer, LgSprite9_LT, &box);

		// mt
		box.x = lw, box.y = 0, box.width = cw, box.height = th;
		LgAtlasBufferDrawRect(sd->drawBuffer, LgSprite9_MT, &box);

		// rt
		box.x = lw + cw, box.y = 0, box.width = rw, box.height = th;
		LgAtlasBufferDrawRect(sd->drawBuffer, LgSprite9_RT, &box);

		// lm
		box.x = 0, box.y = th, box.width = lw, box.height = ch;
		LgAtlasBufferDrawRect(sd->drawBuffer, LgSprite9_LM, &box);

		// mm
		box.x = lw, box.y = th, box.width = cw, box.height = ch;
		LgAtlasBufferDrawRect(sd->drawBuffer, LgSprite9_MM, &box);

		// rm
		box.x = lw + cw, box.y = th, box.width = rw, box.height = ch;
		LgAtlasBufferDrawRect(sd->drawBuffer, LgSprite9_RM, &box);

		// lb
		box.x = 0, box.y = th + ch, box.width = lw, box.height = bh;
		LgAtlasBufferDrawRect(sd->drawBuffer, LgSprite9_LB, &box);

		// mb
		box.x = lw, box.y = th + ch, box.width = cw, box.height = bh;
		LgAtlasBufferDrawRect(sd->drawBuffer, LgSprite9_MB, &box);

		// rb
		box.x = lw + cw, box.y = th + ch, box.width = rw, box.height = bh;
		LgAtlasBufferDrawRect(sd->drawBuffer, LgSprite9_RB, &box);
	}
}

static void spriteEventProc(LgEventType type, LgNodeP node, void *param) {
	switch(type) {
	case LG_NODE_EVENT_Destroy:
		{
			LgSpriteDataP sd = (LgSpriteDataP)LgNodeUserData(node);
			if (sd->texture) LgTextureRelease(sd->texture);
			if (sd->isSprite9) {
				LgTextureAtlasRelease(sd->atlas);
				if (sd->drawBuffer) LgAtlasDrawBufferRelease(sd->drawBuffer);
			}
			if (sd->texurePack) LgTexturePackRelease(sd->texurePack);
			if (sd->vb) LgVertexBufferDestroy(sd->vb);
			LgFree(sd);
		}
		break;
	case LG_NODE_EVENT_Draw:
		{
			LgSpriteDataP sd = (LgSpriteDataP)LgNodeUserData(node);
			struct LgMatrix44 oldMatrix, newMatrix;

			LgRenderSystemSetPixelShader(LgPixelShaderCxformTexture);

			if (!sd->isSprite9) {
				LgRenderSystemGetTransform(LgTransformWorld, &oldMatrix);
				LgMatrix44InitTranslate(&newMatrix, -0.5f, -0.5f, 0.0f);
				LgMatrix44Scale(&newMatrix, sd->size.x, sd->size.y, 1);
				LgMatrix44Mul(&newMatrix, &oldMatrix);
				LgRenderSystemSetTransform(LgTransformWorld, &newMatrix);

				if (!sd->texurePack) {
					LgRenderSystemSetTexture(sd->texture);
					LgRenderSystemDrawPrimitive(LgPrimitiveTriangleStrip, LgFvfXYZ | LgFvfTexture, LgShapesRectangleTexcoord(), 2);
				} else {
					LgRenderSystemSetTexture((LgTextureP)sd->region->page->rendererObject);
					LgRenderSystemDrawPrimitive(LgPrimitiveTriangleStrip, LgFvfXYZ | LgFvfTexture, sd->vb, 2);
				}

				LgRenderSystemSetTransform(LgTransformWorld, &oldMatrix);
				LgRenderSystemSetTexture(NULL);

				LgDirectorUpdateBatch(1);
			} else {
				makeSprite9DrawBuffer(sd);

				if (sd->drawBuffer) {
					LgRenderSystemGetTransform(LgTransformWorld, &oldMatrix);
					LgMatrix44InitTranslate(&newMatrix, -sd->size.x / 2, -sd->size.y / 2, 0.0f);
					LgMatrix44Mul(&newMatrix, &oldMatrix);
					LgRenderSystemSetTransform(LgTransformWorld, &newMatrix);

					LgAtlasBufferDraw(sd->drawBuffer);
					LgDirectorUpdateBatch(LgAtlasBufferNumberOfBatch(sd->drawBuffer));
					LgRenderSystemSetTransform(LgTransformWorld, &oldMatrix);
				}
			}
		}
		break;
	case LG_NODE_EVENT_BatchInfo:
		{
			LgSpriteDataP sd = (LgSpriteDataP)LgNodeUserData(node);

			if ((sd->texture || sd->texurePack) && !sd->isSprite9) {
				LgNodeEventBatchInfoP evtBatchInfo = (LgNodeEventBatchInfoP)param;
				LgBatchInfoP batchInfo = &evtBatchInfo->batchInfo;
				struct LgAabb contentBox;
				struct LgAffineTransform transform;
				struct LgSize textureSize;

				LgNodeLocalTransform(node, &transform);
				LgNodeContentBox(node, &contentBox);

				LgAabbLeftTop(&contentBox, &batchInfo->drawQuad.lt);
				LgAffineTransformMulVector2(&transform, &batchInfo->drawQuad.lt);
				LgAabbRightTop(&contentBox, &batchInfo->drawQuad.rt);
				LgAffineTransformMulVector2(&transform, &batchInfo->drawQuad.rt);
				LgAabbLeftBottom(&contentBox, &batchInfo->drawQuad.lb);
				LgAffineTransformMulVector2(&transform, &batchInfo->drawQuad.lb);
				LgAabbRightBottom(&contentBox, &batchInfo->drawQuad.rb);
				LgAffineTransformMulVector2(&transform, &batchInfo->drawQuad.rb);
			
				if (!sd->texurePack) {
					LgTextureSize(sd->texture, &textureSize);
					batchInfo->texture = sd->texture;
					batchInfo->textureRt.x = batchInfo->textureRt.y = 0;
					batchInfo->textureRt.width = textureSize.width;
					batchInfo->textureRt.height = textureSize.height;
				} else {
					batchInfo->texture = (LgTextureP)sd->region->page->rendererObject;
					batchInfo->textureRt.x = sd->region->x, batchInfo->textureRt.y = sd->region->y;
					batchInfo->textureRt.width = sd->region->originalWidth;
					batchInfo->textureRt.height = sd->region->originalHeight;
				}

				evtBatchInfo->supported = LgTrue;
				return;
			}
		}
		break;
	case LG_NODE_EVENT_ContentBox:
		{
			LgSpriteDataP sd = (LgSpriteDataP)LgNodeUserData(node);
			LgAabbP contentBox = (LgAabbP)param;
			contentBox->x = -sd->size.x / 2, contentBox->y = -sd->size.y / 2;
			contentBox->width = sd->size.x;
			contentBox->height = sd->size.y;
		}
		return;
	case LG_NODE_EVENT_SetPosition:
	case LG_NODE_EVENT_SetScale:
	case LG_NODE_EVENT_SetRotate:
	case LG_NODE_EVENT_SetAnchor:
	case LG_NODE_EVENT_SetTransformAnchor:
		makeBatchDirty(node);
		break;
	}

	LgNodeDefEventProc(type, node, param);
}

static void setSprite9Atlas(LgSpriteDataP sd) {
	int lw, rw, th, bh, cw, ch;
	struct LgTextAtlasRecord record;

	if (!sd->texurePack) {
		struct LgSize textureSize;
		LgTextureSize(LgTextureAtlasTexture(sd->atlas, LG_TEXTUREATLAS_FIRSTTEXTURE), &textureSize);

		lw = sd->insetLT.x, rw = sd->insetRB.x;
		th = sd->insetLT.y, bh = sd->insetRB.y;
		cw = textureSize.width - lw - rw;
		ch = textureSize.height - th - bh;

		// lt
		record.textureId = LG_TEXTUREATLAS_FIRSTTEXTURE;
		record.pt.x = 0,			record.pt.y = 0;
		record.size.width = lw,		record.size.height = th;
		LgTextureAtlasAddRecord(sd->atlas, LgSprite9_LT, &record);

		// mt
		record.pt.x = lw,			record.pt.y = 0;
		record.size.width = cw,		record.size.height = th;
		LgTextureAtlasAddRecord(sd->atlas, LgSprite9_MT, &record);

		// rt
		record.pt.x = lw + cw,		record.pt.y = 0;
		record.size.width = rw,		record.size.height = th;
		LgTextureAtlasAddRecord(sd->atlas, LgSprite9_RT, &record);

		// lm
		record.pt.x = 0,			record.pt.y = th;
		record.size.width = lw,		record.size.height = ch;
		LgTextureAtlasAddRecord(sd->atlas, LgSprite9_LM, &record);

		// mm
		record.pt.x = lw,			record.pt.y = th;
		record.size.width = cw,		record.size.height = ch;
		LgTextureAtlasAddRecord(sd->atlas, LgSprite9_MM, &record);

		// rm
		record.pt.x = lw + cw,		record.pt.y = th;
		record.size.width = rw,		record.size.height = ch;
		LgTextureAtlasAddRecord(sd->atlas, LgSprite9_RM, &record);

		// lb
		record.pt.x = 0,			record.pt.y = th + ch;
		record.size.width = lw,		record.size.height = bh;
		LgTextureAtlasAddRecord(sd->atlas, LgSprite9_LB, &record);

		// mb
		record.pt.x = lw,			record.pt.y = th + ch;
		record.size.width = cw,		record.size.height = bh;
		LgTextureAtlasAddRecord(sd->atlas, LgSprite9_MB, &record);

		// rb
		record.pt.x = lw + cw,		record.pt.y = th + ch;
		record.size.width = rw,		record.size.height = bh;
		LgTextureAtlasAddRecord(sd->atlas, LgSprite9_RB, &record);
	} else {
		lw = sd->insetLT.x, rw = sd->insetRB.x;
		th = sd->insetLT.y, bh = sd->insetRB.y;
		cw = sd->region->originalWidth - lw - rw;
		ch = sd->region->originalHeight - th - bh;

		// lt
		record.textureId = LG_TEXTUREATLAS_FIRSTTEXTURE;
		record.pt.x = sd->region->x,				record.pt.y = sd->region->y;
		record.size.width = lw,						record.size.height = th;
		LgTextureAtlasAddRecord(sd->atlas, LgSprite9_LT, &record);

		// mt
		record.pt.x = sd->region->x + lw,			record.pt.y = sd->region->y;
		record.size.width = cw,						record.size.height = th;
		LgTextureAtlasAddRecord(sd->atlas, LgSprite9_MT, &record);

		// rt
		record.pt.x = sd->region->x + lw + cw,		record.pt.y = sd->region->y;
		record.size.width = rw,						record.size.height = th;
		LgTextureAtlasAddRecord(sd->atlas, LgSprite9_RT, &record);

		// lm
		record.pt.x = sd->region->x,				record.pt.y = sd->region->y + th;
		record.size.width = lw,						record.size.height = ch;
		LgTextureAtlasAddRecord(sd->atlas, LgSprite9_LM, &record);

		// mm
		record.pt.x = sd->region->x + lw,			record.pt.y = sd->region->y + th;
		record.size.width = cw,						record.size.height = ch;
		LgTextureAtlasAddRecord(sd->atlas, LgSprite9_MM, &record);

		// rm
		record.pt.x = sd->region->x + lw + cw,		record.pt.y = sd->region->y + th;
		record.size.width = rw,						record.size.height = ch;
		LgTextureAtlasAddRecord(sd->atlas, LgSprite9_RM, &record);

		// lb
		record.pt.x = sd->region->x,				record.pt.y = sd->region->y + th + ch;
		record.size.width = lw,						record.size.height = bh;
		LgTextureAtlasAddRecord(sd->atlas, LgSprite9_LB, &record);

		// mb
		record.pt.x = sd->region->x + lw,			record.pt.y = sd->region->y + th + ch;
		record.size.width = cw,						record.size.height = bh;
		LgTextureAtlasAddRecord(sd->atlas, LgSprite9_MB, &record);

		// rb
		record.pt.x = sd->region->x + lw + cw,		record.pt.y = sd->region->y + th + ch;
		record.size.width = rw,						record.size.height = bh;
		LgTextureAtlasAddRecord(sd->atlas, LgSprite9_RB, &record);
	}
}

LgNodeP LgSpriteCreateR(const LgSpriteLocationP location, const LgVector2P size, const LgPointP insetLT, const LgPointP insetRB) {
	LgNodeP node = NULL;
	LgSpriteDataP sd;

	// 创建节点
	node = LgNodeCreateR((LgEvent)spriteEventProc);
	LG_CHECK_ERROR(node);

	sd = (LgSpriteDataP)LgMallocZ(sizeof(struct LgSpriteData));
	LG_CHECK_ERROR(sd);
	LgNodeSetUserData(node, sd);

	// 载入资源
	if (location->type == LgSpriteInFile) {
		sd->texture = LgTextureRetain(LgResourceManagerLoadTexture(location->filename));
		LG_CHECK_ERROR(sd->texture);
	} else if (location->type == LgSpriteInTexturePack) {
		sd->texurePack = LgTexturePackRetain(LgResourceManagerLoadTexturePack(location->filename));
		LG_CHECK_ERROR(sd->texurePack);
		sd->region = spAtlas_findRegion(sd->texurePack->atlas, location->name);
		LG_CHECK_ERROR(sd->region);
	}

	// 设置精灵大小
	if (!size || size->x == 0 || size->y == 0) {
		// 使用默认大小
		if (!sd->texurePack) {
			struct LgSize _size;
			LgTextureSize(sd->texture, &_size);
			sd->size.x = (float)_size.width;
			sd->size.y = (float)_size.height;
		} else {
			sd->size.x = (float)sd->region->width;
			sd->size.y = (float)sd->region->height;
		}
	} else {
		LgCopyMemory(&sd->size, size, sizeof(struct LgSize));
	}

	// 不是sprite9并且使用texturePack，则创建vertexBuffer
	if (sd->texurePack && !sd->isSprite9) {
		struct LgVertexTexCrood vertexs[4];

		vertexs[0].position.x = 0, vertexs[0].position.y = 1, vertexs[0].position.z = 0, 
			vertexs[0].texcrood.x = sd->region->u, vertexs[0].texcrood.y = sd->region->v2;
		vertexs[1].position.x = 0, vertexs[1].position.y = 0, vertexs[1].position.z = 0, 
			vertexs[1].texcrood.x = sd->region->u, vertexs[1].texcrood.y = sd->region->v;
		vertexs[2].position.x = 1, vertexs[2].position.y = 1, vertexs[2].position.z = 0, 
			vertexs[2].texcrood.x = sd->region->u2, vertexs[2].texcrood.y = sd->region->v2;
		vertexs[3].position.x = 1, vertexs[3].position.y = 0, vertexs[3].position.z = 0, 
			vertexs[3].texcrood.x = sd->region->u2, vertexs[3].texcrood.y = sd->region->v;

		sd->vb = LgRenderSystemCreateVertexBuffer(sizeof(vertexs));
		LG_CHECK_ERROR(sd->vb);

		LgCopyMemory(LgVertexBufferLock(sd->vb), vertexs, sizeof(vertexs));
		LgVertexBufferUnlock(sd->vb);
	}
	
	// 设置insert
	if (insetLT) {
		sd->isSprite9 = LgTrue;
		sd->insetLT = *insetLT;
		sd->insetRB = *insetRB;
		sd->atlas = LgTextureAtlasCreateR();
		if (!sd->texurePack) LgTextureAtlasAddTexture(sd->atlas, sd->texture);
		else LgTextureAtlasAddTexture(sd->atlas, (LgTextureP)sd->region->page->rendererObject);
		setSprite9Atlas(sd);
	} else {
		sd->isSprite9 = LgFalse;
	}

	return node;

_error:
	if (node) LgNodeRelease(node);
	return NULL;
}

LgBool LgNodeIsSprite(LgNodeP node) {
	return LgNodeEventProc(node) == (LgEvent)spriteEventProc;
}

void LgSpriteSetSize(LgNodeP node, const LgVector2P size) {
	LgSpriteDataP sd = (LgSpriteDataP)LgNodeUserData(node);
	sd->size = *size;
	makeBatchDirty(node);
	if (sd->isSprite9) {
		if (sd->drawBuffer) {
			LgAtlasDrawBufferRelease(sd->drawBuffer);
			sd->drawBuffer = NULL;
		}
	}
}
