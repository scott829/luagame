#ifndef _LG_ARRAY_H_
#define _LG_ARRAY_H_

#include "lg_basetypes.h"

typedef struct LgArray *LgArrayP;

LgArrayP LgArrayCreate(LgDestroy destroyData);

void LgArrayDestroy(LgArrayP array);

int LgArrayLen(LgArrayP array);

LgBool LgArraySet(LgArrayP array, int i, void *data);

void *LgArrayGet(LgArrayP array, int i);

void LgArrayInsert(LgArrayP array, int i, void *data);

void LgArrayAppend(LgArrayP array, void *data);

LgBool LgArrayRemove(LgArrayP array, int i);

void LgArraySort(LgArrayP array, LgCompare compare);

int LgArrayFind(LgArrayP array, void *data);

void *LgArrayData(LgArrayP array);

void LgArrayClear(LgArrayP array);

#endif