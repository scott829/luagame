#ifndef _LG_GLESCONTEXT_IOS_H
#define _LG_GLESCONTEXT_IOS_H

#include "lg_basetypes.h"
#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>

// gl context ---------------------------------------------------------------------------------------------------

LgBool LgGlContextOpen();

void LgGlContextClose();

void LgGlContextBegin();

void LgGlContextEnd();

#endif
