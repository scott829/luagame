﻿#include "lg_node.h"
#include "lg_array.h"
#include "lg_director.h"
#include "lg_actionmanager.h"
#include "lg_luaapi.h"
#include "lg_scheduler.h"
#include "lg_gesture_recognizer.h"
#include "lg_platform.h"
#include "lg_dict.h"

struct LgNode {
	LG_REFCNT_HEAD
	LgEvent						eventProc;
	LgArrayP					children;
	LgArrayP					sortedChildren;
	struct LgVector2			position, anchor, scale, transformAnchor;
	float						rotateRadians;
	LgBool						transformDirty;
	struct LgAffineTransform	transform;
	float						vertexZ;
	struct LgCxform				cxform;
	float						opacity;
	LgNodeP						parent;
	int							zorder;
	LgBool						visible;
	int							running;
	LgGridP						grid;
	LgBool						enableHitTest;
	void *						userdata;
	char *						id;
	
	// 当前节点内所有touch对象，所有touch对象都是弱引用，
	// 因为每个touch对象都引用了当前节点
	LgBool						enableMultipleTouch;
	LgAvlTreeP					touches;
	LgBool						touchBubble;	// 触摸事件冒泡，默认关闭
	
	// 手势
	LgListP						gestureRecognizerList;
	
	// class支持
	LgAvlTreeP					classes;
	LgDictP						childrenClasses;
	
	// rtree支持
	LgRTreeP					childrenRTree;
	LgBool						inRTree;
	
	// 事件
	LgSlot						onTouch;
	LgSlot						onEnter, onExit;
	LgSlot						onGesture;
};

LG_REFCNT_FUNCTIONS(LgNode)

typedef struct LgNodeChild {
	LgNodeP	child;
}*LgNodeChildP;

static int g_nodeCount = 0;

int LgNodeCount() {
	return g_nodeCount;
}

void destroyNodeChild(LgNodeChildP child) {
	if (child->child) LgNodeRelease(child->child);
	LgFree(child);
}

void LgNodeDoEvent(LgEventType type, LgNodeP node, void *param) {
	node->eventProc(type, node, param);
}

static void hasClass(LgNodeP node, LgNodeEventHasClassP hasClass) {
	hasClass->result = node->classes && LgAvlTreeFind(node->classes, (void *)hasClass->className) != NULL;
}

static void addClassIndexToParent(const char *className, LgNodeP node) {
	if (node->parent) {
		LgDictP childrenClasses = node->parent->childrenClasses;
		LgAvlTreeP nodeset;
		
		if (!childrenClasses)
			childrenClasses = node->parent->childrenClasses = LgDictCreate((LgCompare)strnocasecmp, LgFree, (LgDestroy)LgAvlTreeDestroy);
		
		nodeset = (LgAvlTreeP)LgDictFind(childrenClasses, (void *)className);
		if (!nodeset) {
			nodeset = LgRbTreeCreate(NULL, NULL);
			LgDictInsert(childrenClasses, strdup(className), nodeset);
		}
		
		LgAvlTreeInsert(nodeset, node);
	}
}

static void addClassesIndexToParent(LgNodeP node) {
	if (node->classes) LgAvlTreeForeach(node->classes, (LgForeach)addClassIndexToParent, node);
}

static void addClass(LgNodeP node, const char *className) {
	if (className && strlen(className) > 0) {
		if (LgNodeHasClass(node, className)) return; // 已经存在了
		if (!node->classes) node->classes = LgRbTreeCreate((LgCompare)strnocasecmp, LgFree);
		LgAvlTreeReplace(node->classes, strdup(className));
		addClassIndexToParent(className, node);
	}
}

static void removeClassIndexFromParent(const char *className, LgNodeP node) {
	if (node->parent) {
		LgDictP childrenClasses = node->parent->childrenClasses;
		LgAvlTreeP nodeset = (LgAvlTreeP)LgDictFind(childrenClasses, (void *)className);
		assert(nodeset);
		LgAvlTreeRemove(nodeset, node);
		if (LgAvlTreeCount(nodeset) == 0) LgDictRemove(childrenClasses, (void *)className);
		if (LgDictCount(childrenClasses) == 0) {
			LgDictDestroy(childrenClasses);
			node->parent->childrenClasses = NULL;
		}
	}
}

static void removeClassesIndexFromParent(LgNodeP node) {
	if (node->classes) LgAvlTreeForeach(node->classes, (LgForeach)removeClassIndexFromParent, node);
}

static void removeClass(LgNodeP node, const char *className) {
	if (LgNodeHasClass(node, className)) {
		LgAvlTreeRemove(node->classes, (void *)className);
		removeClassIndexFromParent(className, node);
	}
}

static void toggleClass(LgNodeP node, const char *className) {
	if (LgNodeHasClass(node, className)) removeClass(node, className);
	else addClass(node, className);
}

static void clearClasses(LgNodeP node) {
	removeClassesIndexFromParent(node);
	if (node->classes) LgAvlTreeDestroy(node->classes);
	node->classes = NULL;
}

static void setCxform(LgNodeP node, const LgCxformP cxform) {
	LgCopyMemory(&node->cxform, cxform, sizeof(struct LgCxform));
}

static void getCxform(LgNodeP node, LgCxformP cxform) {
	LgCopyMemory(cxform, &node->cxform, sizeof(struct LgCxform));
}

static void setPosition(LgNodeP node, const LgVector2P position) {
	LgCopyMemory(&node->position, position, sizeof(struct LgVector2));
	node->transformDirty = LgTrue;
}

static void getPosition(LgNodeP node, LgVector2P position) {
	LgCopyMemory(position, &node->position, sizeof(struct LgVector2));
}

static void setAnchor(LgNodeP node, const LgVector2P anchor) {
	LgCopyMemory(&node->anchor, anchor, sizeof(struct LgVector2));
	node->transformDirty = LgTrue;
}

static void getAnchor(LgNodeP node, LgVector2P anchor) {
	LgCopyMemory(anchor, &node->anchor, sizeof(struct LgVector2));
}

static void setScale(LgNodeP node, const LgVector2P scale) {
	LgCopyMemory(&node->scale, scale, sizeof(struct LgVector2));
	node->transformDirty = LgTrue;
}

static void getScale(LgNodeP node, LgVector2P scale) {
	LgCopyMemory(scale, &node->scale, sizeof(struct LgVector2));
}

static void setRotate(LgNodeP node, float radians) {
	node->rotateRadians = radians;
	node->transformDirty = LgTrue;
}

static float getRotate(LgNodeP node) {
	return node->rotateRadians;
}

static void setTransformAnchor(LgNodeP node, const LgVector2P transformAnchor) {
	LgCopyMemory(&node->transformAnchor, transformAnchor, sizeof(struct LgVector2));
	node->transformDirty = LgTrue;
}

static void getTransformAnchor(LgNodeP node, LgVector2P transformAnchor) {
	LgCopyMemory(transformAnchor, &node->transformAnchor, sizeof(struct LgVector2));
}

static void makeSortChildren(LgNodeP node) {
	if (node->sortedChildren) {
		LgArrayDestroy(node->sortedChildren);
		node->sortedChildren = NULL;
	}
}

static void setZorder(LgNodeP node, int z) {
	node->zorder = z;
	if (node->parent) makeSortChildren(node->parent);
}

static int getZorder(LgNodeP node) {
	return node->zorder;
}

static void nodeEnter(LgNodeP node) {
	node->running++;

	if (node->running == 1) {
		int i;

		if (node->children) {
			for (i = 0; i < LgArrayLen(node->children); i++) {
				LgNodeChildP child = (LgNodeChildP)LgArrayGet(node->children, i);
				child->child->eventProc(LG_NODE_EVENT_Enter, child->child, NULL);
			}
		}

		if (node->grid) LgGridEnter(node->grid);
		LgSlotCall(&node->onEnter, NULL);
	}
}

static void childAabb(LgNodeP node, LgNodeP child, LgAabbP aabb) {
	assert(LgNodeParent(child) == node);
	LgNodeContentBox(child, aabb);
	LgAffineTransformMulAabb(&child->transform, aabb);
}

static void addToRTree(LgNodeP node, LgNodeP child) {
	struct LgAabb aabb;
	assert(LgNodeParent(child) == node);
	childAabb(node, child, &aabb);
	node->childrenRTree = LgRTreeInsertRect(node->childrenRTree, &aabb, child);
	child->inRTree = LgTrue;
}

static void removeFromRTree(LgNodeP node, LgNodeP child) {
	struct LgAabb aabb;
	assert(LgNodeParent(child) == node);
	childAabb(node, child, &aabb);
	node->childrenRTree = LgRTreeDelectRect(node->childrenRTree, &aabb, child);
	child->inRTree = LgFalse;
}

static void addChild(LgNodeP node, LgNodeP child) {
	if (node != child && !child->parent) {
		LgNodeChildP nodeChild = (LgNodeChildP)LgMallocZ(sizeof(struct LgNodeChild));

		LgNodeRetain(child);
		nodeChild->child = child;

		if (!node->children) node->children = LgArrayCreate((LgDestroy)destroyNodeChild);
		LgArrayAppend(node->children, nodeChild);
		if (node->running > 0) nodeChild->child->eventProc(LG_NODE_EVENT_Enter, nodeChild->child, NULL);
		makeSortChildren(node);
		child->parent = node;
		addClassesIndexToParent(child); // 添加类索引
		node->eventProc(LG_NODE_EVENT_AddChildNotify, node, child);
	}
}

static void nodeExit(LgNodeP node) {
	node->running--;

	if (node->running == 0) {
		int i;

		if (node->grid) LgGridExit(node->grid);

		if (node->children) {
			for (i = 0; i < LgArrayLen(node->children); i++) {
				LgNodeChildP child = (LgNodeChildP)LgArrayGet(node->children, i);
				child->child->eventProc(LG_NODE_EVENT_Exit, child->child, NULL);
			}
		}

		node->eventProc(LG_NODE_EVENT_ClearSlots, node, NULL);
		LgActionManagerStop(node, NULL);
		LgSlotCall(&node->onExit, NULL);
	}
}

static void removeNode(LgNodeP node) {
	if (node->parent) {
		LgNodeRetain(node);
		if (node->running > 0) node->eventProc(LG_NODE_EVENT_Exit, node, NULL);
		if (node->parent->children) {
			int i, idx = -1;

			for (i = 0; i < LgArrayLen(node->parent->children); i++) {
				if (((LgNodeChildP)(LgArrayGet(node->parent->children, i)))->child == node) {
					idx = i;
					break;
				}
			}
			
			if (idx != -1) {
				LgArrayRemove(node->parent->children, idx);
				makeSortChildren(node->parent);
			}
		}
		removeClassesIndexFromParent(node); // 移除类索引
		if (node->inRTree) removeFromRTree(node->parent, node); // 移除RTree索引
		node->parent->eventProc(LG_NODE_EVENT_RemoveChildNotify, node->parent, node);
		node->parent = NULL;
		LgSchedulerReleaseAtNextFrame(node); // 下一帧再释放
	}
}

static void removeAllChildren(LgNodeP node) {
	if (node->children) {
		while(LgArrayLen(node->children) > 0) {
			LgNodeChildP child = (LgNodeChildP)LgArrayGet(node->children, 0);
			removeNode(child->child);
		}
		LgArrayDestroy(node->children);
		node->children = NULL;
	}
}

static int _compareZorder(LgNodeChildP lhs, LgNodeChildP rhs) {
	return lhs->child->zorder - rhs->child->zorder;
}

static void visitChildren(LgNodeP node, float delta) {
	if (node->sortedChildren) {
		int i;
		for (i = 0; i < LgArrayLen(node->sortedChildren); i++)
			LgNodeVisit(((LgNodeChildP)LgArrayGet(node->sortedChildren, i))->child, delta);
	}
}

static void visit(LgNodeP node, float delta) {
	if (node->visible) {
		struct LgMatrix44 currentMatrix, newMatrix;
		struct LgCxform currentCxform, newCxform;

		if (node->gestureRecognizerList) {
			LgListNodeP it = LgListFirstNode(node->gestureRecognizerList);
			while(it) {
				LgGestureRecognizerUpdate((LgGestureRecognizerP)LgListNodeValue(it), delta);
				it = LgListNextNode(it);
			}
		}

		if (!node->sortedChildren && node->children) {
			int i;
			node->sortedChildren = LgArrayCreate(NULL);
			for (i = 0; i < LgArrayLen(node->children); i++)
				LgArrayAppend(node->sortedChildren, LgArrayGet(node->children, i));
			LgArraySort(node->sortedChildren, (LgCompare)_compareZorder);
		}

		node->eventProc(LG_NODE_EVENT_Transform, node, NULL);
		LgRenderSystemGetTransform(LgTransformWorld, &currentMatrix);
		LgRenderSystemGetCxform(&currentCxform);

		LgAffineTransformMatrix44(&node->transform, &newMatrix);
		newMatrix.m44[3][2] = node->vertexZ;
		LgMatrix44Mul(&newMatrix, &currentMatrix);
		LgRenderSystemSetTransform(LgTransformWorld, &newMatrix);

		LgCopyMemory(&newCxform, &currentCxform, sizeof(struct LgCxform));
		LgCxformMul(&newCxform, &node->cxform);
		newCxform.mul[LGCXFORM_ALPHA] *= node->opacity;
		LgRenderSystemSetCxform(&newCxform);

		if (node->grid) LgGridBeforeDraw(node->grid);
		node->eventProc(LG_NODE_EVENT_Draw, node, &delta);

		node->eventProc(LG_NODE_EVENT_VisitChildren, node, &delta);

		node->eventProc(LG_NODE_EVENT_AfterDraw, node, NULL);
		if (node->grid) LgGridAfterDraw(node->grid);

		LgRenderSystemSetTransform(LgTransformWorld, &currentMatrix);
		LgRenderSystemSetCxform(&currentCxform);
	}
}

static void worldTransform(LgNodeP node, LgAffineTransformP af) {
	struct LgAffineTransform localAf;
	LgNodeP p;

	LgAffineTransformIndentity(af);
	p = node;
	while(p) {
		LgNodeLocalTransform(p, &localAf);
		LgAffineTransformMul(af, &localAf);
		p = p->parent;
	}
}

static void pointToWorld(LgNodeP node, LgVector2P pt) {
	struct LgAffineTransform worldAf;
	worldTransform(node, &worldAf);
	LgAffineTransformMulVector2(&worldAf, pt);
}

static void pointToLocal(LgNodeP node, LgVector2P pt) {
	struct LgAffineTransform worldAf;
	worldTransform(node, &worldAf);
	LgAffineTransformInverse(&worldAf);
	LgAffineTransformMulVector2(&worldAf, pt);
}

static void transform(LgNodeP node) {
	if (node->transformDirty) {
		LgNodeP parent = LgNodeParent(node);

		if (node->inRTree) removeFromRTree(parent, node);
		LgAffineTransformInitTranslate(&node->transform, -node->transformAnchor.x, -node->transformAnchor.y);
		LgAffineTransformScale(&node->transform, node->scale.x, node->scale.y);
		if (node->rotateRadians != 0) LgAffineTransformRotate(&node->transform, node->rotateRadians);
		LgAffineTransformTranslate(&node->transform, node->transformAnchor.x, node->transformAnchor.y);
		LgAffineTransformTranslate(&node->transform, node->position.x - node->anchor.x, node->position.y - node->anchor.y);
		node->transformDirty = LgFalse;
		if (parent && parent->childrenRTree) addToRTree(parent, node);
	}
}

static void doHitTest(LgNodeP node, LgNodeEventDoHitTestP hittest) {
	// 简单点击测试
	struct LgAabb contentBox;

	LgNodeContentBox(node, &contentBox);
	if (LgAabbContains(&contentBox, &hittest->localPosition)) {
		hittest->hitted = LgTrue;
	}
}

static LgNodeP hitTest2(LgNodeP node, const LgVector2P pt, const LgAffineTransformP world, LgVector2P localPosition) {
	struct LgAffineTransform currentWorld, inverseCurrentWorld;
	struct LgVector2 point2;
	int i;
	struct LgNodeEventDoHitTest _doHitTest;
	LgNodeP ret = NULL;
	LgBool isClipTouch = LgFalse;

	// 如果该节点被隐藏了，则直接返回NULL
	if (!node->visible) return NULL;
	
	LgNodeLocalTransform(node, &currentWorld);
	LgAffineTransformMul(&currentWorld, world);
	LgCopyMemory(&inverseCurrentWorld, &currentWorld, sizeof(struct LgAffineTransform));
	LgAffineTransformInverse(&inverseCurrentWorld);

	LgCopyMemory(&point2, pt, sizeof(struct LgVector2));
	LgAffineTransformMulVector2(&inverseCurrentWorld, &point2);

	// 测试当前节点
	_doHitTest.node = node;
	LgCopyMemory(&_doHitTest.localPosition, &point2, sizeof(struct LgVector2));
	_doHitTest.hitted = LgFalse;
	node->eventProc(LG_NODE_EVENT_DoHitTest, node, &_doHitTest);
	if (_doHitTest.hitted) ret = node;

	// 如果该节点是裁剪touch，并且没有命中自己，则直接返回NULL
	node->eventProc(LG_NODE_EVENT_IsClipTouch, node, &isClipTouch);
	if (isClipTouch && !ret) return NULL;

	// 测试子节点
	if (node->sortedChildren) {
		for (i = LgArrayLen(node->sortedChildren) - 1; i >= 0; i--) {
			LgNodeChildP child = (LgNodeChildP)LgArrayGet(node->sortedChildren, i);
			LgNodeP hittestChild;
			hittestChild = hitTest2(child->child, pt, &currentWorld, localPosition);
			if (hittestChild) {
				ret = hittestChild;
				break;
			}
		}
	}

	if (ret == node) {
		return node->enableHitTest ? node : NULL;
	} else {
		return ret;
	}
}

static void hitTest(LgNodeP node, LgNodeEventHitTestP hittest) {
	struct LgAffineTransform world;
	LgAffineTransformIndentity(&world);
	hittest->node = hitTest2(node, &hittest->pt, &world, &hittest->localPosition);
}

static void appendNodeToList(LgNodeP node, LgListP result) {
	LgListAppend(result, node);
}

static void selectNode(LgNodeP node, const char *className, LgBool recursive, LgListP result) {
	LgDictP childrenClasses = node->childrenClasses;

	if (childrenClasses) {
		LgAvlTreeP nodeset = LgDictFind(childrenClasses, (void *)className);
		if (nodeset) LgAvlTreeForeach(nodeset, (LgForeach)appendNodeToList, result);
	}
	
	if (recursive) {
		int childCount = LgNodeChildrenCount(node), i;
		for (i = 0; i < childCount; i++) {
			LgNodeP child = LgNodeChildByIndex(node, i);
			selectNode(child, className, recursive, result);
		}
	}
}

static void bounds(LgNodeP node, LgAabbP aabb) {
	struct LgAffineTransform worldTransform;

	LgNodeWorldTransform(node, &worldTransform);
	LgNodeContentBox(node, aabb);
	LgAffineTransformMulAabb(&worldTransform, aabb);
}

static void enableRTree(LgNodeP node, LgBool enable) {
	if (enable) {
		if (!node->childrenRTree) {
			// 开启RTree支持
			int i, childrenCount = LgNodeChildrenCount(node);
			node->childrenRTree = LgRTreeCreate();
			for (i = 0; i < childrenCount; i++) addToRTree(node, LgNodeChildByIndex(node, i));
		}
	} else {
		if (node->childrenRTree) {
			// 关闭RTree支持
			LgRTreeDestroy(node->childrenRTree);
			node->childrenRTree = NULL;
		}
	}
}

static LgBool rtreeInRectCallback(LgRTreeId id, void* userdata) {
	LgNodeEventRTreeSearchByRectP context = (LgNodeEventRTreeSearchByRectP)userdata;
	LgNodeP child = (LgNodeP)id, parent = LgNodeParent((LgNodeP)id);
	struct LgAabb aabb;

	childAabb(parent, child, &aabb);
	if (LgAabbContainsAabb(context->aabb, &aabb)) {
		context->callback(id, context->userdata);
		context->result++;
	}

	return LgTrue;
}

static void searchRTreeByRect(LgNodeP node, LgNodeEventRTreeSearchByRectP s) {
	if (node->childrenRTree) {
		if (s->inRect) {
			// 必须包含在整个矩形内
			s->result = 0;
			LgRTreeSearch(node->childrenRTree, s->aabb, rtreeInRectCallback, s);
		} else {
			s->result = LgRTreeSearch(node->childrenRTree, s->aabb, s->callback, s->userdata);
		}
	} else {
		s->result = 0;
	}
}

static LgBool rtreeByRadiusCallback(LgRTreeId id, void* userdata) {
	LgNodeEventRTreeSearchByRadiusP context = (LgNodeEventRTreeSearchByRadiusP)userdata;
	LgNodeP child = (LgNodeP)id, parent = LgNodeParent((LgNodeP)id);
	struct LgAabb aabb;

	childAabb(parent, child, &aabb);
	if (!LgAabbIntersectEllipse(&aabb, context->center->x, context->center->y, context->radius)) return LgTrue;

	context->callback(id, context->userdata);
	context->result++;
	return LgTrue;
}
static void searchRTreeByRadius(LgNodeP node, LgNodeEventRTreeSearchByRadiusP s) {
	if (node->childrenRTree) {
		struct LgAabb aabb;
		aabb.x = s->center->x - s->radius;
		aabb.y = s->center->y - s->radius;
		aabb.width = s->radius * 2;
		aabb.height = s->radius * 2;
		LgRTreeSearch(node->childrenRTree, &aabb, rtreeByRadiusCallback, s);
	} else {
		s->result = 0;
	}
}

static void handleTouch(LgNodeP node, const LgNodeTouchEventP evt) {
	int i;
	struct LgNodeTouchEvent _evt = *evt;
	LgTouchP singleTouch = NULL;

	// 为开启多点触摸支持的处理
	if (!node->enableMultipleTouch) {
		if (evt->type == LgNodeTouchBegan) {
			if (node->touches && LgAvlTreeCount(node->touches) > 0) return;
			singleTouch = evt->touches[0];
			_evt.num = 1;
			_evt.touches = &singleTouch;
		} else {
			for (i = 0; i < evt->num; i++) {
				if (node->touches && LgAvlTreeFind(node->touches, evt->touches[i])) {
					singleTouch = evt->touches[i];
					_evt.num = 1;
					_evt.touches = &singleTouch;
					break;
				}
			}

			if (!singleTouch) return;
		}
	}

	// 维护节点的touches集合
	if (_evt.type == LgNodeTouchBegan) {
		if (!node->touches) node->touches = LgRbTreeCreate(NULL, NULL);
		for (i = 0; i < _evt.num; i++) LgAvlTreeInsert(node->touches, _evt.touches[i]);
	} else if (_evt.type == LgNodeTouchEnded || _evt.type == LgNodeTouchCancelled) {
		for (i = 0; i < _evt.num; i++) LgAvlTreeRemove(node->touches, _evt.touches[i]);
		if (LgAvlTreeCount(node->touches) == 0) {
			LgAvlTreeDestroy(node->touches);
			node->touches = NULL;
		}
	}

	// 触发节点的触摸事件
	LgNodeDoEvent(LG_NODE_EVENT_FireTouchEvent, node, &_evt);

	// 手势识别
	if (node->gestureRecognizerList) {
		LgListNodeP it = LgListFirstNode(node->gestureRecognizerList);
		while(it) {
			LgGestureRecognizerHandleTouchEvent((LgGestureRecognizerP)LgListNodeValue(it), &_evt);
			it = LgListNextNode(it);
		}
	}
}

void LgNodeDefEventProc(LgEventType type, LgNodeP node, void *param) {
	switch(type) {
	case LG_NODE_EVENT_SetCxform:
		setCxform(node, (const LgCxformP)param);
		break;
	case LG_NODE_EVENT_Cxform:
		getCxform(node, (LgCxformP)param);
		break;
	case LG_NODE_EVENT_SetPosition:
		setPosition(node, (const LgVector2P)param);
		break;
	case LG_NODE_EVENT_Position:
		getPosition(node, (LgVector2P)param);
		break;
	case LG_NODE_EVENT_SetAnchor:
		setAnchor(node, (const LgVector2P)param);
		break;
	case LG_NODE_EVENT_Anchor:
		getAnchor(node, (LgVector2P)param);
		break;
	case LG_NODE_EVENT_SetScale:
		setScale(node, (const LgVector2P)param);
		break;
	case LG_NODE_EVENT_Scale:
		getScale(node, (LgVector2P)param);
		break;
	case LG_NODE_EVENT_SetRotate:
		setRotate(node, *((float *)param));
		break;
	case LG_NODE_EVENT_Rotate:
		*((float *)param) = getRotate(node);
		break;
	case LG_NODE_EVENT_SetTransformAnchor:
		setTransformAnchor(node, (const LgVector2P)param);
		break;
	case LG_NODE_EVENT_TransformAnchor:
		getTransformAnchor(node, (LgVector2P)param);
		break;
	case LG_NODE_EVENT_SetZorder:
		setZorder(node, *((int *)param));
		break;
	case LG_NODE_EVENT_Zorder:
		*((int *)param) = getZorder(node);
		break;
	case LG_NODE_EVENT_AddChild:
		addChild(node, (LgNodeP)param);
		break;
	case LG_NODE_EVENT_Remove:
		removeNode(node);
		break;
	case LG_NODE_EVENT_RemoveAllChildren:
		removeAllChildren(node);
		break;
	case LG_NODE_EVENT_Visit:
		visit(node, *((float*)param));
		break;
	case LG_NODE_EVENT_IsRunning:
		*((LgBool *)param) = node->running > 0;
		break;
	case LG_NODE_EVENT_IsVisible:
		*((LgBool *)param) = node->visible;
		break;
	case LG_NODE_EVENT_SetVisible:
		node->visible = *((LgBool *)param);
		break;
	case LG_NODE_EVENT_DoAction:
		LgActionManagerAdd(node, (LgActionP)param);
		break;
	case LG_NODE_EVENT_StopActions:
		LgActionManagerStop(node, NULL);
		break;
	case LG_NODE_EVENT_PauseActions:
		LgActionManagerPause(node);
		break;
	case LG_NODE_EVENT_ResumeActions:
		LgActionManagerResume(node);
		break;
	case LG_NODE_EVENT_LocalTransform:
		node->eventProc(LG_NODE_EVENT_Transform, node, NULL);
		LgCopyMemory(param, &node->transform, sizeof(struct LgAffineTransform));
		break;
	case LG_NODE_EVENT_WorldTransform:
		worldTransform(node, (LgAffineTransformP)param);
		break;
	case LG_NODE_EVENT_PointToWorld:
		pointToWorld(node, (LgVector2P)param);
		break;
	case LG_NODE_EVENT_PointToLocal:
		pointToLocal(node, (LgVector2P)param);
		break;
	case LG_NODE_EVENT_Parent:
		*((LgNodeP *)param) = node->parent;
		break;
	case LG_NODE_EVENT_Root:
		{
			LgNodeP p = node;
			while(p->parent) p = p->parent;
			*((LgNodeP *)param) = p;
		}
		break;
	case LG_NODE_EVENT_ChildByIndex:
		if (node->children) {
			((LgNodeEventChildByIndexP)param)->node = 
				((LgNodeChildP)LgArrayGet(node->children, ((LgNodeEventChildByIndexP)param)->i))->child;
		} else {
			((LgNodeEventChildByIndexP)param)->node = NULL;
		}
		break;
	case LG_NODE_EVENT_ChildrenCount:
		*((int *)param) = node->children ? LgArrayLen(node->children) : 0;
		break;
	case LG_NODE_EVENT_SetOpacity:
		node->opacity = *((float *)param);
		break;
	case LG_NODE_EVENT_SetGrid:
		if (node->grid) LgGridDestroy(node->grid);
		node->grid = (LgGridP)param;
		break;
	case LG_NODE_EVENT_Grid:
		*((LgGridP *)param) = node->grid;
		break;
	case LG_NODE_EVENT_Enter:
		nodeEnter(node);
		break;
	case LG_NODE_EVENT_Exit:
		nodeExit(node);
		break;
	case LG_NODE_EVENT_Transform:
		transform(node);
		break;
	case LG_NODE_EVENT_DoHitTest:
		doHitTest(node, (LgNodeEventDoHitTestP)param);
		break;
	case LG_NODE_EVENT_HitTest:
		hitTest(node, (LgNodeEventHitTestP)param);
		break;
	case LG_NODE_EVENT_ActionsCount:
		*((int *)param) = LgActionManagerActionsCount(node);
		break;
	case LG_NODE_EVENT_IsLayer:
		*((LgBool *)param) = LgFalse;
		break;
	case LG_NODE_EVENT_ClearSlots:
		LgSlotClear(&node->onTouch);
		LgSlotClear(&node->onEnter);
		LgSlotClear(&node->onExit);
		LgSlotClear(&node->onGesture);
		break;
	case LG_NODE_EVENT_FireTouchEvent:
		LgSlotCall(&node->onTouch, param);
		break;
	case LG_NODE_EVENT_HandleTouch:
		handleTouch(node, (const LgNodeTouchEventP)param);
		break;
	case LG_NODE_EVENT_SetVertexZ:
		node->vertexZ = *((float *)param);
		break;
	case LG_NODE_EVENT_VisitChildren:
		visitChildren(node, *((float *)param));
		break;
	case LG_NODE_EVENT_AddClass:
		addClass(node, (const char *)param);
		break;
	case LG_NODE_EVENT_RemoveClass:
		removeClass(node, (const char *)param);
		break;
	case LG_NODE_EVENT_ToggleClass:
		toggleClass(node, (const char *)param);
		break;
	case LG_NODE_EVENT_HasClass:
		hasClass(node, (LgNodeEventHasClassP)param);
		break;
	case LG_NODE_EVENT_ClearClasses:
		clearClasses(node);
		break;
	case LG_NODE_EVENT_Classes:
		*((LgAvlTreeP *)param) = node->classes;
		break;
	case LG_NODE_EVENT_Select:
		{
			LgNodeEventSelectP evt = (LgNodeEventSelectP)param;
			selectNode(node, evt->className, evt->recursive, evt->result);
		}
		break;
	case LG_NODE_EVENT_ContentBox:
		((LgAabbP)param)->x = ((LgAabbP)param)->y = 0,
			((LgAabbP)param)->width = ((LgAabbP)param)->height = 0;
		break;
	case LG_NODE_EVENT_Bounds:
		bounds(node, (LgAabbP)param);
		break;
	case LG_NODE_EVENT_EnableRTree:
		enableRTree(node, (LgBool)param);
		break;
	case LG_NODE_EVENT_RTreeSearchByRect:
		searchRTreeByRect(node, (LgNodeEventRTreeSearchByRectP)param);
		break;
	case LG_NODE_EVENT_RTreeSearchByRadius:
		searchRTreeByRadius(node, (LgNodeEventRTreeSearchByRadiusP)param);
		break;
	case LG_NODE_EVENT_SetId:
		if (node->id) LgFree(node->id);
		node->id = strdup((const char *)param);
		break;
	case LG_NODE_EVENT_Id:
		*((const char **)param) = node->id;
		break;
	case LG_NODE_EVENT_BatchInfo:
		((LgNodeEventBatchInfoP)param)->supported = LgFalse;
		break;
	case LG_NODE_EVENT_IsClipTouch:
		*((LgBool *)param) = LgFalse;
		break;
	}
}

static void destroy(LgNodeP node) {
	if (node->gestureRecognizerList) LgListDestroy(node->gestureRecognizerList);
	node->eventProc(LG_NODE_EVENT_ClearSlots, node, NULL);
	node->eventProc(LG_NODE_EVENT_Destroy, node, NULL);
	if (node->children) {
		// 把他下面节点的parent设置为NULL
		int i, len = LgArrayLen(node->children);
		for (i = 0; i < len; i++)
			((LgNodeChildP)LgArrayGet(node->children, i))->child->parent = NULL;
		LgArrayDestroy(node->children);
	}
	if (node->sortedChildren) LgArrayDestroy(node->sortedChildren);
	if (node->grid) LgGridDestroy(node->grid);
	if (node->classes) LgAvlTreeDestroy(node->classes);
	if (node->childrenClasses) LgDictDestroy(node->childrenClasses);
	if (node->childrenRTree) LgRTreeDestroy(node->childrenRTree);
	if (node->id) LgFree(node->id);
	LgFree(node);
	g_nodeCount--;
}

LgNodeP LgNodeCreateR(LgEvent eventProc) {
	LgNodeP node = (LgNodeP)LgMallocZ(sizeof(struct LgNode));
	LG_CHECK_ERROR(node);
	LG_REFCNT_INIT(node, destroy);
	node->scale.x = node->scale.y = 1.0f;
	LgAffineTransformIndentity(&node->transform);
	node->opacity = 1.0f;
	LgCxformIndentity(&node->cxform);
	node->visible = LgTrue;
	node->eventProc = eventProc ? eventProc : (LgEvent)LgNodeDefEventProc;
	node->eventProc(LG_NODE_EVENT_Create, node, NULL);
	g_nodeCount++;
	return node;

_error:
	return NULL;
}

LgEvent LgNodeEventProc(LgNodeP node) {
	return node->eventProc;
}

void LgNodeSetEventProc(LgNodeP node, LgEvent eventProc) {
	node->eventProc = eventProc;
}

void *LgNodeUserData(LgNodeP node) {
	return node->userdata;
}

void LgNodeSetUserData(LgNodeP node, void *userdata) {
	node->userdata = userdata;
}

void LgNodeSetCxform(LgNodeP node, const LgCxformP cxform) {
	LgNodeDoEvent(LG_NODE_EVENT_SetCxform, node, cxform);
}

void LgNodeCxform(LgNodeP node, LgCxformP cxform) {
	LgNodeDoEvent(LG_NODE_EVENT_Cxform, node, cxform);
}

void LgNodeSetPosition(LgNodeP node, const LgVector2P position) {
	LgNodeDoEvent(LG_NODE_EVENT_SetPosition, node, position);
}

void LgNodePosition(LgNodeP node, LgVector2P position) {
	LgNodeDoEvent(LG_NODE_EVENT_Position, node, position);
}

void LgNodeSetAnchor(LgNodeP node, const LgVector2P anchor) {
	LgNodeDoEvent(LG_NODE_EVENT_SetAnchor, node, anchor);
}

void LgNodeAnchor(LgNodeP node, LgVector2P anchor) {
	LgNodeDoEvent(LG_NODE_EVENT_Anchor, node, anchor);
}

void LgNodeSetScale(LgNodeP node, const LgVector2P scale) {
	LgNodeDoEvent(LG_NODE_EVENT_SetScale, node, scale);
}

void LgNodeScale(LgNodeP node, LgVector2P scale) {
	LgNodeDoEvent(LG_NODE_EVENT_Scale, node, scale);
}

void LgNodeSetRotate(LgNodeP node, float rotateRadians) {
	LgNodeDoEvent(LG_NODE_EVENT_SetRotate, node, &rotateRadians);
}

float LgNodeRotate(LgNodeP node) {
	float radians;
	LgNodeDoEvent(LG_NODE_EVENT_Rotate, node, &radians);
	return radians;
}

void LgNodeSetTransformAnchor(LgNodeP node, const LgVector2P transformAnchor) {
	LgNodeDoEvent(LG_NODE_EVENT_SetTransformAnchor, node, transformAnchor);
}

void LgNodeTransformAnchor(LgNodeP node, LgVector2P transformAnchor) {
	LgNodeDoEvent(LG_NODE_EVENT_TransformAnchor, node, transformAnchor);
}

void LgNodeSetZorder(LgNodeP node, int zorder) {
	LgNodeDoEvent(LG_NODE_EVENT_SetZorder, node, &zorder);
}

int LgNodeZorder(LgNodeP node) {
	int zorder;
	LgNodeDoEvent(LG_NODE_EVENT_Zorder, node, &zorder);
	return zorder;
}

void LgNodeAddChild(LgNodeP node, LgNodeP child) {
	LgNodeDoEvent(LG_NODE_EVENT_AddChild, node, child);
}

void LgNodeRemove(LgNodeP node) {
	LgNodeDoEvent(LG_NODE_EVENT_Remove, node, NULL);
}

void LgNodeRemoveAllChildren(LgNodeP node) {
	LgNodeDoEvent(LG_NODE_EVENT_RemoveAllChildren, node, NULL);
}

void LgNodeVisit(LgNodeP node, float delta) {
	LgNodeDoEvent(LG_NODE_EVENT_Visit, node, &delta);
}

LgBool LgNodeIsRunning(LgNodeP node) {
	LgBool r;
	LgNodeDoEvent(LG_NODE_EVENT_IsRunning, node, &r);
	return r;
}

LgBool LgNodeIsVisible(LgNodeP node) {
	LgBool r;
	LgNodeDoEvent(LG_NODE_EVENT_IsVisible, node, &r);
	return r;
}

void LgNodeSetVisible(LgNodeP node, LgBool visible) {
	LgNodeDoEvent(LG_NODE_EVENT_SetVisible, node, &visible);
}

void LgNodeDoAction(LgNodeP node, LgActionP action) {
	LgNodeDoEvent(LG_NODE_EVENT_DoAction, node, action);
}

void LgNodeDoActionAndRelease(LgNodeP node, LgActionP action) {
	LgNodeDoEvent(LG_NODE_EVENT_DoAction, node, action);
	LgActionRelease(action);
}

void LgNodeStopActions(LgNodeP node) {
	LgNodeDoEvent(LG_NODE_EVENT_StopActions, node, NULL);
}

void LgNodePauseActions(LgNodeP node) {
	LgNodeDoEvent(LG_NODE_EVENT_PauseActions, node, NULL);
}

void LgNodeResumeActions(LgNodeP node) {
	LgNodeDoEvent(LG_NODE_EVENT_ResumeActions, node, NULL);
}

void LgNodeLocalTransform(LgNodeP node, LgAffineTransformP af) {
	LgNodeDoEvent(LG_NODE_EVENT_LocalTransform, node, af);
}

void LgNodeWorldTransform(LgNodeP node, LgAffineTransformP af) {
	LgNodeDoEvent(LG_NODE_EVENT_WorldTransform, node, af);
}

void LgNodePointToWorld(LgNodeP node, LgVector2P pt) {
	LgNodeDoEvent(LG_NODE_EVENT_PointToWorld, node, pt);
}

void LgNodePointToLocal(LgNodeP node, LgVector2P pt) {
	LgNodeDoEvent(LG_NODE_EVENT_PointToLocal, node, pt);
}

LgNodeP LgNodeParent(LgNodeP node) {
	LgNodeP parent;
	LgNodeDoEvent(LG_NODE_EVENT_Parent, node, &parent);
	return parent;
}

LgNodeP LgNodeRoot(LgNodeP node) {
	LgNodeP root;
	LgNodeDoEvent(LG_NODE_EVENT_Root, node, &root);
	return root;
}

LgNodeP LgNodeChildByIndex(LgNodeP node, int index) {
	struct LgNodeEventChildByIndex context;
	context.i = index;
	LgNodeDoEvent(LG_NODE_EVENT_ChildByIndex, node, &context);
	return context.node;
}

int LgNodeChildrenCount(LgNodeP node) {
	int count;
	LgNodeDoEvent(LG_NODE_EVENT_ChildrenCount, node, &count);
	return count;
}

void LgNodeSetOpacity(LgNodeP node, float opacity) {
	LgNodeDoEvent(LG_NODE_EVENT_SetOpacity, node, &opacity);
}

void LgNodeSetGrid(LgNodeP node, LgGridP grid) {
	LgNodeDoEvent(LG_NODE_EVENT_SetGrid, node, grid);
}

LgGridP LgNodeGrid(LgNodeP node) {
	LgGridP grid;
	LgNodeDoEvent(LG_NODE_EVENT_Grid, node, &grid);
	return grid;
}

LgBool LgNodeHitTestIsEnabled(LgNodeP node) {
	return node->enableHitTest;
}

void LgNodeEnableHitTest(LgNodeP node, LgBool value) {
	node->enableHitTest = value;
}

void LgNodeContentBox(LgNodeP node, LgAabbP aabb) {
	LgNodeDoEvent(LG_NODE_EVENT_ContentBox, node, aabb);
}

LgNodeP LgNodeHitTest(LgNodeP node, const LgVector2P pt, LgVector2P localPosition) {
	struct LgNodeEventHitTest ht;
	LgZeroMemory(&ht, sizeof(ht));
	LgCopyMemory(&ht.pt, pt, sizeof(struct LgVector2));
	LgNodeDoEvent(LG_NODE_EVENT_HitTest, node, &ht);
	if (localPosition) LgCopyMemory(localPosition, &ht.localPosition, sizeof(struct LgVector2));
	return ht.node;
}

int LgNodeActionsCount(LgNodeP node) {
	int count;
	LgNodeDoEvent(LG_NODE_EVENT_ActionsCount, node, &count);
	return count;
}

LgBool LgNodeIsLayer(LgNodeP node) {
	LgBool ret;
	LgNodeDoEvent(LG_NODE_EVENT_IsLayer, node, &ret);
	return ret;
}

LgSlot *LgNodeSlotTouch(LgNodeP node) {
	return &node->onTouch;
}

LgSlot *LgNodeSlotEnter(LgNodeP node) {
	return &node->onEnter;
}

LgSlot *LgNodeSlotExit(LgNodeP node) {
	return &node->onExit;
}

void LgNodeHandleTouch(LgNodeP node, const LgNodeTouchEventP evt) {
	LgNodeDoEvent(LG_NODE_EVENT_HandleTouch, node, (void *)evt);
}

void LgNodeAddGestureRecognizer(LgNodeP node, LgGestureRecognizerP gr) {
	if (!LgGestureRecognizerAttachNode(gr, node)) return;
	if (!node->gestureRecognizerList)
		node->gestureRecognizerList = LgListCreate((LgDestroy)LgGestureRecognizerRelease);
	LgGestureRecognizerRetain(gr);
	LgListAppend(node->gestureRecognizerList, gr);
}

LgSlot *LgNodeSlotGesture(LgNodeP node) {
	return &node->onGesture;
}

void LgNodeSetVertexZ(LgNodeP node, float z) {
	LgNodeDoEvent(LG_NODE_EVENT_SetVertexZ, node, &z);
}

void LgNodeAddClass(LgNodeP node, const char *className) {
	LgNodeDoEvent(LG_NODE_EVENT_AddClass, node, (void *)className);
}

void LgNodeRemoveClass(LgNodeP node, const char *className) {
	LgNodeDoEvent(LG_NODE_EVENT_RemoveClass, node, (void *)className);
}

void LgNodeToggleClass(LgNodeP node, const char *className) {
	LgNodeDoEvent(LG_NODE_EVENT_ToggleClass, node, (void *)className);
}

LgBool LgNodeHasClass(LgNodeP node, const char *className) {
	struct LgNodeEventHasClass evt;
	evt.className = className;
	evt.result = LgFalse;
	LgNodeDoEvent(LG_NODE_EVENT_HasClass, node, &evt);
	return evt.result;
}

void LgNodeClearClasses(LgNodeP node) {
	LgNodeDoEvent(LG_NODE_EVENT_ClearClasses, node, NULL);
}

LgAvlTreeP LgNodeClasses(LgNodeP node) {
	LgAvlTreeP classes;
	LgNodeDoEvent(LG_NODE_EVENT_Classes, node, &classes);
	return classes;
}

void LgNodeSelect(LgNodeP node, const char *className, LgBool recursive, LgListP result) {
	struct LgNodeEventSelect evt;
	evt.className = className;
	evt.recursive = recursive;
	evt.result = result;
	LgNodeDoEvent(LG_NODE_EVENT_Select, node, &evt);
}

void LgNodeBounds(LgNodeP node, LgAabbP aabb) {
	LgNodeDoEvent(LG_NODE_EVENT_Bounds, node, aabb);
}

void LgNodeEnableRTree(LgNodeP node, LgBool enable) {
	LgNodeDoEvent(LG_NODE_EVENT_EnableRTree, node, (void *)enable);
}

int LgNodeRTreeSearchByRect(LgNodeP node, const LgAabbP aabb, LgBool inRect, LgRTreeSearchCallback callback, void *userdata) {
	struct LgNodeEventRTreeSearchByRect evt;
	evt.aabb = aabb;
	evt.inRect = inRect;
	evt.callback = callback;
	evt.userdata = userdata;
	evt.result = 0;
	LgNodeDoEvent(LG_NODE_EVENT_RTreeSearchByRect, node, &evt);
	return evt.result;
}

int LgNodeRTreeSearchByRadius(LgNodeP node, const LgVector2P center, float radius, LgRTreeSearchCallback callback, void *userdata) {
	struct LgNodeEventRTreeSearchByRadius evt;
	evt.center = center;
	evt.radius = radius;
	evt.callback = callback;
	evt.userdata = userdata;
	evt.result = 0;
	LgNodeDoEvent(LG_NODE_EVENT_RTreeSearchByRadius, node, &evt);
	return evt.result;
}

void LgNodeSetId(LgNodeP node, const char *id) {
	LgNodeDoEvent(LG_NODE_EVENT_SetId, node, (void *)id);
}

const char *LgNodeId(LgNodeP node) {
	const char *id;
	LgNodeDoEvent(LG_NODE_EVENT_Id, node, (void *)&id);
	return id;
}

LgAvlTreeP LgNodeTouches(LgNodeP node) {
	return node->touches;
}

int LgNodeTouchesCount(LgNodeP node) {
	return node->touches ? LgAvlTreeCount(node->touches) : 0;
}

void LgNodeEnableMultipleTouch(LgNodeP node, LgBool enable) {
	node->enableMultipleTouch = enable;
}

LgBool LgNodeBatchInfo(LgNodeP node, LgBatchInfoP batchInfo) {
	struct LgNodeEventBatchInfo evt;
	LgNodeDoEvent(LG_NODE_EVENT_BatchInfo, node, &evt);
	if (evt.supported) LgCopyMemory(batchInfo, &evt.batchInfo, sizeof(struct LgBatchInfo));
	return evt.supported;
}

void LgNodeCenterAnchor(LgNodeP node) {
	struct LgAabb contentBox;
	struct LgVector2 center;
	
	LgNodeContentBox(node, &contentBox);
	LgAabbCenterPoint(&contentBox, &center);
	LgNodeSetAnchor(node, &center);
}

void LgNodeCenterTransformAnchor(LgNodeP node) {
	struct LgAabb contentBox;
	struct LgVector2 center;
	
	LgNodeContentBox(node, &contentBox);
	LgAabbCenterPoint(&contentBox, &center);
	LgNodeSetTransformAnchor(node, &center);
}

void LgNodeCenter(LgNodeP node) {
	LgNodeP parentNode = LgNodeParent(node);

	if (parentNode) {
		struct LgAabb contentBox;
		struct LgVector2 center;

		LgNodeContentBox(parentNode, &contentBox);
		LgAabbCenterPoint(&contentBox, &center);
		LgNodeSetPosition(node, &center);
	}
}

void LgNodeSetTouchBubble(LgNodeP node, LgBool value) {
	node->touchBubble = value;
}

LgBool LgNodeIsTouchBubble(LgNodeP node) {
	return node->touchBubble;
}