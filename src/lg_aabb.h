#ifndef _LG_AABB_H
#define _LG_AABB_H

#include "lg_vector2.h"

typedef struct LgAabb {
	float x, y, width, height;
}*LgAabbP;

void LgAabbLeftTop(const LgAabbP aabb, LgVector2P vec);

void LgAabbRightTop(const LgAabbP aabb, LgVector2P vec);

void LgAabbLeftBottom(const LgAabbP aabb, LgVector2P vec);

void LgAabbRightBottom(const LgAabbP aabb, LgVector2P vec);

LgBool LgAabbIntersect(const LgAabbP lhs, const LgAabbP rhs);

LgBool LgAabbIntersectEllipse(const LgAabbP aabb, float rx, float ry, float radius);

LgBool LgAabbContains(const LgAabbP aabb, const LgVector2P pt);

void LgAabbCenterPoint(const LgAabbP aabb, LgVector2P pt);

LgBool LgAabbContainsAabb(const LgAabbP aabb, const LgAabbP aabb2);

#endif