#ifndef _LG_MEMINPUTSTREAM_H
#define _LG_MEMINPUTSTREAM_H

#include "lg_inputstream.h"

LgInputStreamP LgMemInputStreamCreate(const void *data, int size);

void LgMemInputStreamAttach(LgInputStreamP stream, const void *data, int size);

#endif