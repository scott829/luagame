﻿#include "lg_scriptenv.h"
#include "lua/lualib.h"
#include "lg_archive.h"
#include "lg_memory.h"
#include "lg_platform.h"
#include "string.h"
#include "lg_luaapi.h"

int luaopen_cjson(lua_State *l);

static lua_State *g_luaState = NULL;

static int searcherScriptsInZip(lua_State *L) {
	const char *name = luaL_checkstring(L, 1);
	char path[LG_PATH_MAX];
	LgInputStreamP inputStream = NULL;
	char *script = NULL;
	int scriptLen;
	int ret, i;
	
	strcpy(path, "scripts/");
	strcat(path, name);
	for (i = 0; path[i] != 0; i++) {
		if (path[i] == '.') path[i] = '/';
	}
	strcat(path, ".lua");

	inputStream = LgArchiveOpenFile(path);
	LG_CHECK_ERROR(inputStream);
	scriptLen = LgInputStreamAvailable(inputStream);
	script = (char *)LgInputStreamReadAll(inputStream);

	ret = luaL_loadbuffer(L, script, scriptLen, path);
	LG_CHECK_ERROR(ret == 0);
	lua_pushstring(L, path);
	LgFree(script);
	LgInputStreamDestroy(inputStream);
	return 2;

_error:
	if (inputStream) LgInputStreamDestroy(inputStream);
	if (script) LgFree(script);
	return luaL_error(L, "error loading module " LUA_QS
		" from resource.zip " LUA_QS ":\n\t%s",
		lua_tostring(L, 1), path, lua_tostring(L, -1));
}

LUALIB_API int luaopen_lsqlite3(lua_State *L);

LgBool LgScriptEnvOpen() {
	size_t count;

	g_luaState = luaL_newstate();
	LG_CHECK_ERROR(g_luaState);
	luaL_openlibs(g_luaState);

	// 自定义模块加载器
	lua_getglobal(g_luaState, "package");
	lua_getfield(g_luaState, -1, "searchers");
	count = lua_rawlen(g_luaState, -1);
	lua_pushcclosure(g_luaState, searcherScriptsInZip, 0);
	lua_rawseti(g_luaState, -2, count + 1);
	lua_pop(g_luaState, 2);

	// 安装lsqlite包
	// lsqlite3参考: http://lua.sqlite.org/index.cgi/doc/tip/doc/lsqlite3.wiki
	luaopen_lsqlite3(g_luaState);
	// cjson包
	luaopen_cjson(g_luaState);

	// 安装lg包
	LG_CHECK_ERROR(LgLuaApiInstall());
	LgLogInfo("script environment initialized.", NULL);
	return LgTrue;

_error:
	LgLogError("failed to initialize script environment.", NULL);
	return LgFalse;
}

void LgScriptEnvClose() {
	if (g_luaState) {
		lua_close(g_luaState);
		g_luaState = NULL;
	}
	LgLogInfo("script environment closed.", NULL);
}

lua_State *LgScriptEnvState() {
	return g_luaState;
}

LgBool LgScriptEnvRunFile(const char *filename) {
	char *script = NULL;
	int scriptSize;
	LgInputStreamP input = NULL;

	LgLogInfo("run script '%s'", filename);
	input = LgArchiveOpenFile(filename);
	LG_CHECK_ERROR(input);
	scriptSize = LgInputStreamAvailable(input);
	script = (char *)LgInputStreamReadAll(input);
	if (luaL_loadbuffer(g_luaState, script, scriptSize, filename)) {
		LgScriptShowError();
		goto _error;
	}
	if (lua_pcall(g_luaState, 0, 0, 0)) {
		LgScriptShowError();
		goto _error;
	}
	LgInputStreamDestroy(input);
	LgFree(script);
	return LgTrue;

_error:
	LgLogError("failed to run '%s'!", filename);
	if (input) LgInputStreamDestroy(input);
	if (script) LgFree(script);
	return LgFalse;
}