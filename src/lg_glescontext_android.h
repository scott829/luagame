#ifndef _LG_GLESCONTEXT_ANDROID_H
#define _LG_GLESCONTEXT_ANDROID_H

#include "lg_basetypes.h"
#include <EGL/egl.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

// gl context ---------------------------------------------------------------------------------------------------

LgBool LgGlContextOpen();

void LgGlContextClose();

void LgGlContextBegin();

void LgGlContextEnd();


#endif