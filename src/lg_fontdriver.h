#ifndef _LG_FONTDRIVER_H
#define _LG_FONTDRIVER_H

#include "lg_basetypes.h"

enum LgDrawStringFlag {
	LgDrawStringBaseline = 0x1,
};

typedef void *LgFontDataP;

typedef void *LgDrawBufferP;

typedef struct LgFontMetrics {
	float ascent, descent;
}*LgFontMetricsP;

typedef struct LgFontDriver {
	const char *name;
	LgBool (* init)();
	void (* destroy)();
	LgFontDataP (* createFont)(const char *filename);
	void (* destroyFont)(LgFontDataP font);
	LgDrawBufferP (* createStringBuffer)(LgFontDataP font, int fontsize, const wchar_t *text, int textLen);
	void (* destroyStringBuffer)(LgDrawBufferP buffer);
	void (* metrics)(LgFontDataP font, int fontsize, LgFontMetricsP metrics);
	float (* stringWidth)(LgFontDataP font, int fontsize, const wchar_t *text, int textLen);
	void (* charsWidth)(LgFontDataP font, int fontsize, const wchar_t *text, int textLen, int *width);
	float (* kerning)(LgFontDataP font, int fontsize, int leftChar, int rightChar);
	float (* height)(LgFontDataP font, int fontsize);
	void (* draw)(LgDrawBufferP buffer, float x, float y, LgColor color, enum LgDrawStringFlag flags);
	int (* numberOfbatch)(LgDrawBufferP buffer);
}*LgFontDriverP;

#endif