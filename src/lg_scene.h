#ifndef _LG_SCENE_H
#define _LG_SCENE_H

#include "lg_node.h"

LgBool LgNodeIsScene(LgNodeP node);

LgNodeP LgSceneCreateR();

void LgSceneEventProc(LgEventType type, LgNodeP node, void *param);

#endif
