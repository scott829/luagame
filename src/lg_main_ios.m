#include "lg_init.h"
#import <UIKit/UIKit.h>
#include "lg_director.h"
#include "lg_log.h"
#include "lg_window_ios.h"

#pragma mark - App Delegate

@interface LgAppDelegate : UIResponder <UIApplicationDelegate> {
	id gameTimer;
};

@end

@implementation LgAppDelegate

- (void)update:(CADisplayLink *)sender; {
	LgMainLoop();
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	if (!LgInit(0, NULL)) {
        printf("Failed to initialize.\n");
        return NO;
    }

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
	LgDirectorPause();
	[gameTimer removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	LgDirectorResume();
	
	gameTimer = [CADisplayLink displayLinkWithTarget:self
                                            selector:@selector(update:)];
	[gameTimer addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    LgCleanup();
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
	LgDirectorHandleLowMemory();
}

@end

#pragma mark Main

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([LgAppDelegate class]));
	}
}
