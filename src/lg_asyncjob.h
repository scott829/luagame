#ifndef _ASYNC_JOB_H
#define _ASYNC_JOB_H

#include "lg_basetypes.h"

LgBool LgAsyncJobManagerOpen();

void LgAsyncJobManagerClose();

void LgAsyncJobRun(SLOT_PARAMS);

#endif