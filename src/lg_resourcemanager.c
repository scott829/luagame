#include "lg_resourcemanager.h"
#include "lg_lru.h"
#include "lg_platform.h"
#include "lg_image.h"
#include "lg_imagecopy.h"
#include <pthread.h>
#include "lg_asyncjob.h"
#include "lg_scheduler.h"

#define LOAD_DEFINE(type, driver, name) \
	type LgResourceManagerLoad##name(const char *filename) { \
		LgResourceP res = load(&driver, filename); \
		if (res) return (type)res->data; \
		else return NULL; \
	} \
	void LgResourceManagerAsyncLoad##name(const char *filename, LgSlot *callback) { \
		asyncLoad(&driver, filename, callback); \
	}

typedef void *(* loadResource)(const char *filename);

struct LgResourceDriver {
	loadResource load;
	LgDestroy destroy;
};

static LgLruP g_resources = NULL;
static pthread_mutex_t g_mutex;

static void destroyResource(LgResourceP resource) {
	resource->driver->destroy(resource->data);
	LgFree(resource);
}

LgBool LgResourceManagerOpen() {
	g_resources = LgLruCreate(LG_RESOURCE_MAX_CACHE, (LgCompare)strnocasecmp, (LgDestroy)LgFree, (LgDestroy)destroyResource);
	LG_CHECK_ERROR(g_resources);
	pthread_mutex_init(&g_mutex, NULL);
	return LgTrue;

_error:
	LgResourceManagerClose();
	return LgFalse;
}

void LgResourceManagerClose() {
	if (g_resources) {
		LgLruDestroy(g_resources);
		g_resources = NULL;
		pthread_mutex_destroy(&g_mutex);
	}
}

void LgResourceManagerClear() {
	LgLruClear(g_resources);
}

// 载入资源函数，由于该函数可能在多个线程调用，所以使用mutex来互斥
static LgResourceP load(LgResourceDriverP driver, const char *filename) {
	LgResourceP res;
	
	pthread_mutex_lock(&g_mutex);
	res = (LgResourceP)LgLruLookup(g_resources, (void *)filename);
	if (res) {
		if (res->driver != driver) {
			pthread_mutex_unlock(&g_mutex);
			return NULL;
		}
		
		pthread_mutex_unlock(&g_mutex);
		return res;
	} else {
		void *data = driver->load(filename);

		if (data) {
			res = (LgResourceP)LgMallocZ(sizeof(struct LgResource));
			res->driver = driver;
			res->data = data;
			LgLruInsert(g_resources, strdup(filename), res);
			pthread_mutex_unlock(&g_mutex);
			return res;
		}

		pthread_mutex_unlock(&g_mutex);
		return NULL;
	}
}

// 异步加载资源
typedef struct LgAsyncLoadContext {
	LgResourceDriverP	driver;
	char *				filename;
	LgSlot				callback;
	LgBool				result;
	float				time;
}*LgAsyncLoadContextP;

static LgBool callAsyncFinish(LgTime delta, void *userdata) {
	LgAsyncLoadContextP context = (LgAsyncLoadContextP)userdata;
	struct LgAsyncLoadResult result;
	
	result.filename = context->filename;
	result.succeeded = context->result;
	result.time = context->time;
	LgSlotCall(&context->callback, &result);
	return LgFalse;
}

static void destroyAsyncContext(LgAsyncLoadContextP context) {
	free(context->filename);
	LgSlotClear(&context->callback);
	LgFree(context);
}

static void loadAsyncCallback(void *param, void *userdata) {
	LgAsyncLoadContextP context = (LgAsyncLoadContextP)userdata;
	LgTime t = LgGetTimeOfDay();
	LgResourceP res = load(context->driver, context->filename);
	context->result = res != NULL;
	context->time = (float)(LgGetTimeOfDay() - t);
	LgSchedulerRun(callAsyncFinish, (LgDestroy)destroyAsyncContext, context, 0, 1, 0, LgFalse);
}

static void asyncLoad(LgResourceDriverP driver, const char *filename, LgSlot *callback) {
	LgAsyncLoadContextP context = (LgAsyncLoadContextP)LgMallocZ(sizeof(struct LgAsyncLoadContext));
	
	context->filename = strdup(filename);
	context->driver = driver;
	LgSlotAssign(&context->callback, SLOT_PARAMS_VALUE_REF(*callback));
	LgAsyncJobRun(loadAsyncCallback, NULL, NULL, context);
}

int LgResourceManagerCachedCount() {
	return LgLruCount(g_resources);
}

int LgResourceManagerMaxCount() {
	return LG_RESOURCE_MAX_CACHE;
}

// texture ----------------------------------------------------------------------------------------------
static struct LgResourceDriver resourceTexture = {
	(loadResource)LgTextureCreateFromFileR,
	(LgDestroy)LgTextureRelease,
};

LOAD_DEFINE(LgTextureP, resourceTexture, Texture)

// sound ----------------------------------------------------------------------------------------------
static struct LgResourceDriver resourceSound = {
	(loadResource)LgSoundDataCreateR,
	(LgDestroy)LgSoundDataRelease,
};

LOAD_DEFINE(LgSoundDataP, resourceSound, Sound)

// animation ----------------------------------------------------------------------------------------------

static struct LgResourceDriver resourceAnimation = {
	(loadResource)LgAnimationCreateR,
	(LgDestroy)LgAnimationRelease,
};

LOAD_DEFINE(LgAnimationP, resourceAnimation, Animation)

// texture pack ----------------------------------------------------------------------------------------------

static struct LgResourceDriver resourceTexturePack = {
	(loadResource)LgTexturePackCreateR,
	(LgDestroy)LgTexturePackRelease,
};

LOAD_DEFINE(LgTexturePackP, resourceTexturePack, TexturePack)
