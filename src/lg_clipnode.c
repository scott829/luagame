#include "lg_clipnode.h"

typedef struct LgClipNodeData {
	LgNodeP	stencilNode;
	LgBool	invert;
}*LgClipNodeDataP;

static void clipNodeEventProc(LgEventType type, LgNodeP node, void *param) {
	switch(type) {
	case LG_NODE_EVENT_Destroy:
		{
			LgClipNodeDataP cd = (LgClipNodeDataP)LgNodeUserData(node);
			if (cd->stencilNode) LgNodeRelease(cd->stencilNode);
			LgFree(cd);
		}
		break;
	case LG_NODE_EVENT_Draw:
		{
			LgClipNodeDataP cd = (LgClipNodeDataP)LgNodeUserData(node);

			if (cd->stencilNode) {
				struct LgCxform oldCxform, newCxform;

				LgRenderSystemGetCxform(&oldCxform);
				newCxform.mul[0] = newCxform.mul[1] = newCxform.mul[2] = 1;
				newCxform.mul[3] = 0;
				LgRenderSystemSetCxform(&newCxform);

				LgRenderSystemEnableStencil(LgTrue);
				LgRenderSystemClear(LgClearFlagStencil, 0, 0);
				LgRenderSystemApplyStencil(1, LgStencilCompareAlways, LgStencilOpReplace, LgStencilOpZero);
				LgNodeVisit(cd->stencilNode, 0);

				LgRenderSystemSetCxform(&oldCxform);
				LgRenderSystemApplyStencil(cd->invert ? 0 : 1, LgStencilCompareEqual, LgStencilOpKeep, LgStencilOpKeep);
			}

			return;
		}
		break;
	case LG_NODE_EVENT_AfterDraw:
		LgRenderSystemEnableStencil(LgFalse);
		return;
	case LG_CLIPNODE_EVENT_SetStencil:
		{
			LgClipNodeDataP cd = (LgClipNodeDataP)LgNodeUserData(node);
			if (cd->stencilNode) LgNodeRelease(cd->stencilNode);
			cd->stencilNode = (LgNodeP)param;
			LgNodeRetain(cd->stencilNode);
		}
		break;
	case LG_CLIPNODE_EVENT_Stencil:
		{
			LgClipNodeDataP cd = (LgClipNodeDataP)LgNodeUserData(node);
			*((LgNodeP *)param) = cd->stencilNode;
		}
		break;
	case LG_CLIPNODE_EVENT_SetInvert:
		{
			LgClipNodeDataP cd = (LgClipNodeDataP)LgNodeUserData(node);
			cd->invert = *((LgBool *)param);
		}
		break;
	case LG_CLIPNODE_EVENT_IsInvert:
		{
			LgClipNodeDataP cd = (LgClipNodeDataP)LgNodeUserData(node);
			*((LgBool *)param) = cd->invert;
		}
		break;
	}

	LgNodeDefEventProc(type, node, param);
}

LgNodeP LgClipNodeCreateR() {
	LgNodeP node;
	LgClipNodeDataP cd;

	node = LgNodeCreateR((LgEvent)clipNodeEventProc);
	LG_CHECK_ERROR(node);

	cd = (LgClipNodeDataP)LgMallocZ(sizeof(struct LgClipNodeData));
	LG_CHECK_ERROR(cd);
	LgNodeSetUserData(node, cd);
	return node;

_error:
	if (node) LgNodeRelease(node);
	return NULL;
}

void LgClipNodeSetStencil(LgNodeP clipnode, LgNodeP stencil) {
	LgNodeDoEvent(LG_CLIPNODE_EVENT_SetStencil, clipnode, stencil);
}

LgNodeP LgClipNodeStencil(LgNodeP clipnode) {
	LgNodeP stencil;
	LgNodeDoEvent(LG_CLIPNODE_EVENT_Stencil, clipnode, &stencil);
	return stencil;
}

void LgClipNodeSetInvert(LgNodeP clipnode, LgBool invert) {
	LgNodeDoEvent(LG_CLIPNODE_EVENT_SetInvert, clipnode, &invert);
}

LgBool LgClipNodeIsInvert(LgNodeP clipnode) {
	LgBool invert;
	LgNodeDoEvent(LG_CLIPNODE_EVENT_IsInvert, clipnode, &invert);
	return invert;
}

LgBool LgNodeIsClip(LgNodeP node) {
	return LgNodeEventProc(node) == (LgEvent)clipNodeEventProc;
}
