﻿#include "lg_netclient.h"
#include "lg_platform.h"
#include "lg_log.h"

LgNetClientDriverP g_currentNetClientDriver = NULL;
static LgSlot g_slotError = {NULL}, g_slotPush = {NULL};

extern struct LgNetClientDriver pomeloDriver;
extern struct LgNetClientDriver lwsDriver;

LgNetClientDriverP g_netDrivers[] = {
	&pomeloDriver,
	&lwsDriver,
	NULL,
};

static LgBool setDriver(const char *name) {
	if (name) {
		int i = 0;
		for (i = 0; g_netDrivers[i]; i++) {
			if (strnocasecmp(g_netDrivers[i]->name, name) == 0) {
				g_currentNetClientDriver = g_netDrivers[i];
				return LgTrue;
			}
		}
	}

	return LgFalse;
}

LgBool LgNetClientOpen(const char *url, LgSlot *slot) {
	LgUrlP urls = NULL;

	urls = LgUrlParse(url);
	LG_CHECK_ERROR(urls && urls->host && urls->port);

	// 设置driver
	LG_CHECK_ERROR(setDriver(urls->scheme));
	g_currentNetClientDriver->open(urls, slot);
	LgUrlDestroy(urls);
	return LgTrue;

_error:
	if (urls) LgUrlDestroy(urls);
	return LgFalse;
}

LgBool LgNetClientRequest(const char *route, json_t *msg, LgSlot *slot) {
	if (g_currentNetClientDriver) {
		g_currentNetClientDriver->request(route, msg, slot);
		return LgTrue;
	} else {
		return LgFalse;
	}
}

LgBool LgNetClientNotify(const char *route, json_t *msg) {
	if (g_currentNetClientDriver) {
		g_currentNetClientDriver->notify(route, msg);
		return LgTrue;
	} else {
		return LgFalse;
	}
}

LgBool LgNetClientIsOpened() {
	return g_currentNetClientDriver ? 
		g_currentNetClientDriver->isOpened() : LgFalse;
}

void LgNetClientClose() {
	if (g_currentNetClientDriver) {
		g_currentNetClientDriver->close();
	}
	LgSlotClear(&g_slotPush);
	LgSlotClear(&g_slotError);
	g_currentNetClientDriver = NULL;
}

LgSlot *LgNetClientSlotError() {
	return &g_slotError;
}

LgSlot *LgNetClientSlotPush() {
	return &g_slotPush;
}