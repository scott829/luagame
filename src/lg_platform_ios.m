#include "lg_platform.h"
#include <Foundation/Foundation.h>

const char *LgPlatformResourceDirectory() {
	NSString *path = [[NSBundle mainBundle] resourcePath];
	NSString *ret = [path stringByAppendingString:@"/"];
	return [ret UTF8String];
}

const char *LgPlatformPathSeparator() {
	return "/";
}

const char *LgPlatformDocumentDirectory() {
	return [[NSHomeDirectory() stringByAppendingFormat:@"/Documents/"] UTF8String];
}

const char *LgCurrentPlatform() {
	return "ios";
}
