#include "lg_list.h"
#include "lg_memory.h"

struct LgListNode {
	LgListNodeP	next, prev;
	void *		data;
};

struct LgList {
	int			count;
	LgDestroy	destroyData;
	LgListNodeP	first, last;
};

LgListP LgListCreate(LgDestroy destroyData) {
	LgListP list = (LgListP)LgMalloc(sizeof(struct LgList));
	LG_CHECK_ERROR(list);

	list->count = 0;
	list->destroyData = destroyData;
	list->first = list->last = NULL;
	return list;

_error:
	return NULL;
}

void LgListDestroy(LgListP list) {
	LgListClear(list);
	LgFree(list);
}

int LgListCount(LgListP list) {
	return list->count;
}

LgListNodeP LgListInsertBefore(LgListP list, LgListNodeP node, void *data) {
	if (node) {
		LgListNodeP newNode = (LgListNodeP)LgMalloc(sizeof(struct LgListNode));
		newNode->data = data;
		newNode->next = node;
		newNode->prev = node->prev;
		node->prev = newNode;
		if (newNode->prev) newNode->prev->next = newNode;
		if (node == list->first) list->first = newNode;
		++list->count;
		return newNode;
	} else {
		return LgListAppend(list, data);
	}
}

LgListNodeP LgListInsertAfter(LgListP list, LgListNodeP node, void *data) {
	if (node) {
		LgListNodeP newNode = (LgListNodeP)LgMalloc(sizeof(struct LgListNode));
		newNode->data = data;
		newNode->prev = node;
		newNode->next = node->next;
		if (node->next) newNode->next->prev = newNode;
		else list->last = newNode;
		node->next = newNode;
		++list->count;
		return newNode;
	} else {
		return LgListAppend(list, data);
	}
}

LgListNodeP LgListAppend(LgListP list, void *data) {
	if (list->first) {
		return LgListInsertAfter(list, list->last, data);
	} else {
		LgListNodeP newNode = (LgListNodeP)LgMalloc(sizeof(struct LgListNode));
		newNode->data = data;
		newNode->prev = newNode->next = NULL;
		list->first = list->last = newNode;
		++list->count;
		return newNode;
	}
}

void LgListRemove(LgListP list, LgListNodeP node) {
	if (node->prev) {
		node->prev->next = node->next;
	} else {
		list->first = node->next;
	}

	if (node->next) {
		node->next->prev = node->prev;
	} else {
		list->last = node->prev;
	}

	if (list->destroyData) list->destroyData(node->data);
	LgFree(node);
	--list->count;
}

void LgListClear(LgListP list) {
	while(list->count > 0) LgListRemove(list, list->first);
}

void LgListForeach(LgListP list, LgForeach foreach, void *userdata) {
	LgListNodeP node = list->first;
	while(node) {
		foreach(node->data, userdata);
		node = node->next;
	}
}

LgListNodeP LgListNextNode(LgListNodeP node) {
	return node->next;
}

LgListNodeP LgListPreviousNode(LgListNodeP node) {
	return node->prev;
}

LgListNodeP LgListFirstNode(LgListP list) {
	return list->first;
}

LgListNodeP LgListLastNode(LgListP list) {
	return list->last;
}

void *LgListNodeValue(LgListNodeP node) {
	return node->data;
}

LgListP LgListClone(LgListP list, LgClone clone) {
	LgListP newlist = LgListCreate(list->destroyData);
	LgListNodeP node = list->first;
	while(node) {
		LgListAppend(newlist, clone ? clone(node->data) : node->data);
		node = node->next;
	}
	return newlist;
}