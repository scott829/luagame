#ifndef _LG_DRAWNODE_H
#define _LG_DRAWNODE_H

#include "lg_node.h"

enum {
	LG_DRAWNODE_EVENT_SetColor = LG_NODE_EVENT_LAST,
	LG_DRAWNODE_EVENT_Color,
	LG_DRAWNODE_EVENT_Clear,
	LG_DRAWNODE_EVENT_Rectangle,
	LG_DRAWNODE_EVENT_Fan,
	LG_DRAWNODE_EVENT_Ellipse,
	LG_DRAWNODE_EVENT_Triangle,
	LG_DRAWNODE_EVENT_LAST,
};

typedef struct LgDrawNodeEventDrawFan {
	struct LgAabb	box;
	float			start, end;
}*LgDrawNodeEventDrawFanP;

typedef struct LgDrawNodeEventDrawTriangle {
	struct LgVector2 pt1, pt2, pt3;
}*LgDrawNodeEventDrawTriangleP;

LgBool LgNodeIsDraw(LgNodeP node);

LgNodeP LgDrawNodeCreateR();

void LgDrawNodeSetColor(LgNodeP node, LgColor color);

LgColor LgDrawNodeColor(LgNodeP node);

void LgDrawNodeClear(LgNodeP node);

void LgDrawNodeRectangle(LgNodeP node, float x, float y, float width, float height);

void LgDrawNodeEllipse(LgNodeP node, float x, float y, float width, float height);

void LgDrawNodeFan(LgNodeP node, float x, float y, float width, float height, float start, float end);

void LgDrawNodeTriangle(LgNodeP node, float x1, float y1, float x2, float y2, float x3, float y3);

#endif
