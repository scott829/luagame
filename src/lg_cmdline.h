﻿#ifndef _LG_GLOBALCONFIG_H_
#define _LG_GLOBALCONFIG_H_

#include "lg_basetypes.h"

typedef struct _LgCommandLine {
	char	resourceDirectory[512];	// 资源目录
	LgBool	debug;					// 调试模式
	int		debugPort;				// 调试端口
}LgCommandLine;

extern LgCommandLine g_commandLine;

void LgParseCommandLine(int argc, char **argv);

#endif