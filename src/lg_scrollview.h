﻿#ifndef _LG_SCROLLVIEW_H
#define _LG_SCROLLVIEW_H

#include "lg_controlnode.h"

typedef struct LgScrollViewData {
	struct LgControlData	base;
	LgNodeP					clipNode, clipDrawNode;
	LgNodeP					contentView;
	struct LgVector2		downOffset, downWorldPosition;
	LgBool					lockHorizontal, lockVertical;
	LgSlot					slotScroll;
}*LgScrollViewDataP;

LgNodeP LgScrollViewCreateR(const LgVector2P size);

LgBool LgNodeIsScrollView(LgNodeP node);

LgNodeP LgScrollViewContentView(LgNodeP node);

void LgScrollViewSetContentSize(LgNodeP node, const LgVector2P size);

void LgScrollViewSetLock(LgNodeP node, LgBool lockHorizontal, LgBool lockVertical);

void LgScrollViewGetViewRect(LgNodeP node, LgAabbP view);

void LgScrollViewSetScroll(LgNodeP node, const LgVector2P position);

LgSlot *LgScrollViewSlotScroll(LgNodeP node);

#endif