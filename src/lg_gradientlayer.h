#ifndef _LG_GRADIENTLAYER_H
#define _LG_GRADIENTLAYER_H

#include "lg_layer.h"

LgBool LgNodeIsGradientLayer(LgNodeP node);

LgNodeP LgGradientLayerCreateR(LgColor beginColor, LgColor endColor, const LgVector2P v);

#endif