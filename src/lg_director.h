#ifndef _LG_DIRECTOR_H
#define _LG_DIRECTOR_H

#include "lg_node.h"
#include "lg_window.h"
#include "lg_keyevent.h"

enum LgKeyboardEventType {
	LgKeyboardEventInputText,
	LgKeyboardEventCompText,
	LgKeyboardEventDeleteChar,
	LgKeyboardEventCloseKeyboard,
};

typedef struct LgKeyboardEvent {
	enum LgKeyboardEventType type;
	const wchar_t *	text;
	size_t			textLength;
}*LgKeyboardEventP;

LgBool LgDirectorOpen();

void LgDirectorClose();

void LgDirectorPush(LgNodeP scene);

void LgDirectorPop();

void LgDirectorReplace(LgNodeP scene);

LgNodeP LgDirectorCurrentScene();

void LgDirectorUpdate();

void LgDirectorSetViewSize(const LgSizeP size);

void LgDirectorViewSize(LgSizeP size);

void LgDirectorScreenToView(const LgPointP pt, LgVector2P viewPt);

void LgDirectorHandleTouchBegan(int num, const int *touchid, const LgPointP pts);

void LgDirectorHandleTouchMoved(int num, const int *touchid, const LgPointP pts);

void LgDirectorHandleTouchEnded(int num, const int *touchid);

void LgDirectorHandleTouchCancelled(int num, const int *touchid);

void LgDirectorHandleKeyDown(enum LgKey key);

void LgDirectorHandleKeyUp(enum LgKey key);

void LgDirectorHandleKeyboardEvent(LgKeyboardEventP evt);

int LgDirectorSceneStackSize();

void LgDirectorUpdateBatch(int batch);

void LgDirectorSetSpeed(float speed);

float LgDirectorSpeed();

void LgDirectorPause();

void LgDirectorResume();

LgBool LgDirectorIsPaused();

void LgDirectorHandleLowMemory();

void LgDirectorSetCamera(float centerX, float centerY, float scale);

void LgDirectorCamera(float *centerX, float *centerY, float *scale);

#endif