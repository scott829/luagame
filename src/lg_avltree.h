#ifndef _LG_AVLTREE_H
#define _LG_AVLTREE_H

#include "lg_basetypes.h"

typedef struct LgAvlTree *LgAvlTreeP;

typedef void * LgAvlTreeTraverserP;

struct LgAvlTreeImpl {
	LgDestroy destroy;
	LgBool (* insert)(LgAvlTreeP tree, void *data);
	void (* replace)(LgAvlTreeP tree, void *data);
	void *(* find)(LgAvlTreeP tree, void *data);
	LgBool (* remove)(LgAvlTreeP tree, void *data);
	void (* clear)(LgAvlTreeP tree);
	int (* count)(LgAvlTreeP tree);
	void (* foreach)(LgAvlTreeP tree, LgForeach foreach, void *userdata);
};

struct LgAvlTree {
	struct LgAvlTreeImpl *	impl;
};

void LgAvlTreeDestroy(LgAvlTreeP tree);

#define LgAvlTreeInsert(tree, data) ((LgAvlTreeP)tree)->impl->insert(tree, data)

#define LgAvlTreeReplace(tree, data) ((LgAvlTreeP)tree)->impl->replace(tree, data)

#define LgAvlTreeFind(tree, data) ((LgAvlTreeP)tree)->impl->find(tree, data)

#define LgAvlTreeRemove(tree, data) ((LgAvlTreeP)tree)->impl->remove(tree, data)

#define LgAvlTreeClear(tree) ((LgAvlTreeP)tree)->impl->clear(tree)

#define LgAvlTreeCount(tree) ((LgAvlTreeP)tree)->impl->count(tree)

#define LgAvlTreeForeach(tree, _foreach, userdata) ((LgAvlTreeP)tree)->impl->foreach(tree, _foreach, userdata)

// ------------------------------------------------------------------------------

LgAvlTreeP LgAvlTreeCreate(LgCompare compareData, LgDestroy destroyData);

LgAvlTreeP LgRbTreeCreate(LgCompare compareData, LgDestroy destroyData);

#endif
