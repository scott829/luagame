#include "lg_time.h"

#ifdef HAVE_GETTIMEOFDAY
#include <sys/time.h>

LgTime LgGetTimeOfDay() {
	struct timeval tm;
	gettimeofday(&tm, NULL);
	return tm.tv_sec + tm.tv_usec / 1000000.0;
}

#else

#ifdef _WIN32
#include <windows.h>

LgTime LgGetTimeOfDay() {
	LARGE_INTEGER liTime, liFreq;
	long sec, usec;

	QueryPerformanceFrequency(&liFreq);
	QueryPerformanceCounter(&liTime);
	sec = (long)(liTime.QuadPart / liFreq.QuadPart);
	usec = (long)(liTime.QuadPart * 1000000.0 / liFreq.QuadPart - sec * 1000000.0);
	return (LgTime)(sec + usec / 1000000.0);
}
#endif

#endif