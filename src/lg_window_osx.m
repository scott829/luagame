#include "lg_window_osx.h"
#include "lg_keyevent.h"
#include "lg_director.h"

static int g_touchId = 1;

static enum LgKey keyMapping(UInt16 key) {
	switch(key) {
        case 126: return LgKeyUp;
        case 125: return LgKeyDown;
        case 123: return LgKeyLeft;
        case 124: return LgKeyRight;
        case 36: return LgKeyEnter;
        case 53: return LgKeyBack;
        case 48: return LgKeyOptions;
        case 122: return LgKeyF1;
        case 120: return LgKeyF2;
        case 99: return LgKeyF3;
        case 118: return LgKeyF4;
        case 96: return LgKeyF5;
        case 97: return LgKeyF6;
        case 98: return LgKeyF7;
        case 100: return LgKeyF8;
        default: return LgKeyUnknown;
	}
}

#pragma mark - LgWindowOsx

@interface LgWindowOsx : NSWindow {
}

@end

@implementation LgWindowOsx

-(BOOL)windowShouldClose:(id)sender {
	[NSApp terminate:nil];
	return YES;
}

@end

#pragma mark - LgViewOsx

@interface LgViewOsx : NSOpenGLView<NSTextInputClient> {
	BOOL keyboardShown;
	NSRange markedRange, selectedRange;
}

- (void)showKeyboard;

- (void)hideKeyboard;

@end

@implementation LgViewOsx

- (id)initWithFrame:(NSRect)frameRect pixelFormat:(NSOpenGLPixelFormat*)format; {
	markedRange = NSMakeRange(NSNotFound, 0);
	selectedRange = NSMakeRange(0, 0);
	return [super initWithFrame:frameRect pixelFormat:format];
}

- (void)showKeyboard {
	[self becomeFirstResponder];
	keyboardShown = YES;
}

- (void)hideKeyboard {
	[self resignFirstResponder];
	keyboardShown = NO;
}

-(void)keyDown:(NSEvent *)theEvent {
	if (keyboardShown) {
		[[self inputContext] handleEvent:theEvent];
	} else {
		if (![theEvent isARepeat]) {
			UInt16 code = [theEvent keyCode];
			enum LgKey key = keyMapping(code);
			if (key != LgKeyUnknown) LgDirectorHandleKeyDown(key);
		}
	}
}

-(void)keyUp:(NSEvent *)theEvent {
	if (!keyboardShown) {
		UInt16 code = [theEvent keyCode];
		enum LgKey key = keyMapping(code);
		if (key != LgKeyUnknown) LgDirectorHandleKeyUp(key);
	}
}

-(BOOL)acceptsFirstResponder {
	return YES;
}

- (BOOL)becomeFirstResponder {
    return YES;
}

- (BOOL)resignFirstResponder {
    return YES;
}

-(void)mouseDown:(NSEvent *)theEvent {
	NSPoint point = [theEvent locationInWindow];
	NSRect rt = [self frame];
	struct LgPoint pt;
	
	pt.x = (int)point.x;
	pt.y = (int)(rt.size.height - point.y);
	LgDirectorHandleTouchBegan(1, &g_touchId, &pt);
}

-(void)mouseDragged:(NSEvent *)theEvent {
	NSPoint point = [theEvent locationInWindow];
	NSRect rt = [self frame];
	struct LgPoint pt;
	
	pt.x = (int)point.x;
	pt.y = (int)(rt.size.height - point.y);
	LgDirectorHandleTouchMoved(1, &g_touchId, &pt);
}

-(void)mouseUp:(NSEvent *)theEvent {
	LgDirectorHandleTouchEnded(1, &g_touchId);
}

-(void)deleteBackward:(id)sender {
	struct LgKeyboardEvent evt;
	evt.type = LgKeyboardEventDeleteChar;
	LgDirectorHandleKeyboardEvent(&evt);
}

-(void)insertNewline:(id)sender {
	struct LgKeyboardEvent evt;
	evt.type = LgKeyboardEventCloseKeyboard;
	LgDirectorHandleKeyboardEvent(&evt);
}

#pragma mark - NSInputTextClient

- (void)insertText:(id)aString replacementRange:(NSRange)replacementRange {
	const wchar_t *wstr;
	
	if ([aString isKindOfClass:[NSAttributedString class]]) {
		NSAttributedString *attrString = aString;
		wstr = (const wchar_t *)[[attrString string] cStringUsingEncoding:NSUTF32StringEncoding];
	} else {
		wstr = (const wchar_t *)[aString cStringUsingEncoding:NSUTF32StringEncoding];
	}

	struct LgKeyboardEvent evt;
	evt.type = LgKeyboardEventInputText;
	evt.text = wstr;
	evt.textLength = [aString length];
	LgDirectorHandleKeyboardEvent(&evt);
}

- (BOOL)hasMarkedText {
	return markedRange.location != NSNotFound;
}

- (NSRange)markedRange {
	return markedRange;
}

- (NSRange)selectedRange {
    return selectedRange;
}

- (void)setMarkedText:(id)aString selectedRange:(NSRange)newSelection replacementRange:(NSRange)replacementRange {
	const wchar_t *wstr;
	
	if ([aString isKindOfClass:[NSAttributedString class]]) {
		NSAttributedString *attrString = aString;
		wstr = (const wchar_t *)[[attrString string] cStringUsingEncoding:NSUTF32StringEncoding];
	} else {
		wstr = (const wchar_t *)[aString cStringUsingEncoding:NSUTF32StringEncoding];
	}
	
	markedRange = NSMakeRange(replacementRange.location, [aString length]);

	struct LgKeyboardEvent evt;
	evt.type = LgKeyboardEventCompText;
	evt.text = wstr;
	evt.textLength = [aString length];
	LgDirectorHandleKeyboardEvent(&evt);
}

- (void)unmarkText {
}

- (NSArray *)validAttributesForMarkedText {
	return [NSArray arrayWithObjects:NSMarkedClauseSegmentAttributeName, NSGlyphInfoAttributeName, nil];
}

- (NSAttributedString *)attributedSubstringFromRange:(NSRange)theRange {
	return nil;
}

- (NSUInteger)characterIndexForPoint:(NSPoint)thePoint {
	return NSNotFound;
}

- (void)doCommandBySelector:(SEL)aSelector {
	[super doCommandBySelector:aSelector];
}

- (NSRect)firstRectForCharacterRange:(NSRange)aRange actualRange:(NSRangePointer)actualRange {
	return CGRectZero;
}

- (NSAttributedString *)attributedSubstringForProposedRange:(NSRange)aRange actualRange:(NSRangePointer)actualRange {
	return NULL;
}

@end

#pragma mark - LgWindow

LgWindowOsx *g_window = nil;
LgViewOsx *g_view = nil;

LgBool LgWindowOpen(const LgSizeP size) {
	NSRect rt = NSMakeRect(0, 0, size->width, size->height);
	
	NSOpenGLPixelFormatAttribute attributes [] = {
		NSOpenGLPFAWindow,
		NSOpenGLPFADoubleBuffer,	// double buffered
		NSOpenGLPFADepthSize, (NSOpenGLPixelFormatAttribute)16, // 16 bit depth buffer
		NSOpenGLPFAStencilSize, (NSOpenGLPixelFormatAttribute)8,
		(NSOpenGLPixelFormatAttribute)nil
	};
	
	NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc] initWithAttributes:attributes] autorelease];
	
	int styleMask = NSTitledWindowMask | NSClosableWindowMask;
		g_window = [[LgWindowOsx alloc] initWithContentRect:rt styleMask:styleMask backing:NSBackingStoreBuffered defer:NO];
	g_view = [[LgViewOsx alloc] initWithFrame:rt pixelFormat:pixelFormat];
	
	[[g_window contentView] setAutoresizesSubviews:YES];
	[[g_window contentView] addSubview:g_view];
	[[g_view openGLContext] makeCurrentContext];
	[g_view setAutoresizingMask:NSViewWidthSizable | NSViewHeightSizable];
	return LgTrue;
}

void LgWindowClose() {
	if (g_view) {
		[g_view release];
		g_view = nil;
	}
	if (g_window) {
		[g_window release];
		g_window = nil;
	}
}

void LgWindowSize(LgSizeP size) {
	CGRect rt = [g_view frame];
	size->width = (int)rt.size.width;
	size->height = (int)rt.size.height;
}

NSWindow *LgWindowNSWindow() {
	return g_window;
}

NSOpenGLView *LgWindowNSOpenGLView() {
	return g_view;
}

void LgWindowShowKeyboard(LgBool show) {
	if (show) [g_view showKeyboard];
	else [g_view hideKeyboard];
}

LgBool LgWindowIsHD() {
	return LgTrue;
}