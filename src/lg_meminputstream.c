#include "lg_meminputstream.h"
#include "lg_memory.h"

struct LgMemInputStream {
	struct LgInputStream	base;
	const char *			data;
	int						size, pos, mark;
};

static int available(LgInputStreamP stream) {
	struct LgMemInputStream *s = (struct LgMemInputStream *)stream;
	return s->size - s->pos;
}

static int readBytes(LgInputStreamP stream, void *data, int size) {
	struct LgMemInputStream *s = (struct LgMemInputStream *)stream;
	int rsz = LgMin(size, s->size - s->pos);
	LgCopyMemory(data, s->data + s->pos, rsz);
	s->pos += rsz;
	return rsz;
}

static void mark(LgInputStreamP stream) {
	struct LgMemInputStream *s = (struct LgMemInputStream *)stream;
	s->mark = s->pos;
}

static LgBool markSupported(LgInputStreamP stream) {
	return LgTrue;
}

static void reset(LgInputStreamP stream) {
	struct LgMemInputStream *s = (struct LgMemInputStream *)stream;
	s->pos = s->mark;
}

struct LgInputStreamImpl memInputImpl = {
	LgFree,
	available,
	readBytes,
	mark,
	markSupported,
	reset
};

LgInputStreamP LgMemInputStreamCreate(const void *data, int size) {
	struct LgMemInputStream *input =
		(struct LgMemInputStream *)LgMallocZ(sizeof(struct LgMemInputStream));
	LG_CHECK_ERROR(input);

	input->base.impl = &memInputImpl;
	input->data = (const char *)data;
	input->size = size;

	return (LgInputStreamP)input;

_error:
	if (input) LgInputStreamDestroy(input);
	return NULL;
}

void LgMemInputStreamAttach(LgInputStreamP stream, const void *data, int size) {
	struct LgMemInputStream *s = (struct LgMemInputStream *)stream;
	s->data = data;
	s->size = size;
	s->mark = s->pos = 0;
}
