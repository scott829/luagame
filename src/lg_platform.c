#include "lg_platform.h"
#include "lg_memory.h"

#ifdef PLATFORM_STRNOCASECMP
int strnocasecmp(const char *lhs, const char *rhs) {
	size_t len = strlen(lhs), otherLen = strlen(rhs);

	if (len > otherLen) return 1;
	else if (len < otherLen) return -1;
	else {
		size_t i, j;
		for (i = j = 0; i < len && j < otherLen; i++, j++) 	{
			char c1, c2;

			c1 = tolower(lhs[i]), c2 = tolower(rhs[j]);
			if (c1 > c2) return 1;
			else if (c1 < c2) return -1;
		}

		return 0;
	}
}
#endif

#ifdef PLATFORM_STRLCPY
int strlcpy(char *dst, const char *src, size_t sz) {
	char *p = dst;
	while(*src != 0 && (p - dst) < sz - 1) {
		*p = *src++;
	}
	*p = 0;
}
#endif

#ifdef PLATFORM_STRNDUP
char *strndup(const char *src, size_t sz) {
	size_t len = LgMin(strlen(src), sz), i;
	char *result = (char *)LgMalloc(len + 1), *p;
	p = result;
	for (i = 0; i < len; i++) *p++ = *src++;
	*p = 0;
	return result;
}
#endif
