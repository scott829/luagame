﻿#ifndef _LG_BASETYPES_H
#define _LG_BASETYPES_H

#include "lg_config.h"
#include <stdlib.h>

// 错误跳转
#define LG_CHECK_ERROR(c) if (!(c)) goto _error;

// 布尔值类型
typedef int LgBool;

#define LgTrue 1

#define LgFalse 0

// 常用回调函数类型
typedef void (* LgDestroy)(void *p);

typedef int (* LgCompare)(void *lhs, void *rhs);

typedef int (* LgCompareWithParam)(void *lhs, void *rhs, void *param);

typedef void (* LgUnaryOp)(void *p);

typedef void *(* LgClone)(void *p);

typedef void (* LgBinaryOp)(void *lhs, void *rhs);

typedef void (* LgForeach)(void *data, void *userdata);

typedef void (* LgForeachPair)(void *key, void *data, void *userdata);

typedef int LgEventType;

typedef void (* LgEvent)(LgEventType type, void *sender, void *param);

// 最大最小值
#define LgMax(a, b) ((a) > (b) ? (a) : (b))

#define LgMin(a, b) ((a) < (b) ? (a) : (b))

// 判断浮点数相等
#define LgFloatEqual(a, b) (fabsf(a - b) < 0.0001)

// 颜色类型
typedef unsigned int LgColor;

#define LgColorARGB(a, r, g, b)	((LgColor)((((a) & 0xff) << 24) | (((b) & 0xff) << 16) | (((g) & 0xff) << 8) | ((r) & 0xff)))

#define LgColorARGB_F(a, r, g, b) LgColorARGB((int)(a * 255), (int)(r * 255), (int)(g * 255), (int)(b * 255))

#define LgColorRGBA(r, g, b, a) LgColorARGB(a, r, g, b)

#define LgColorRGBA_F(r, g, b, a) LgColorARGB_F(a, r, g, b)

#define LgColorXRGB(r, g, b) LgColorARGB(0xff, r, g, b)

#define LgColorXRGB_F(r, g, b) LgColorARGB_F(1.0f, r, g, b)

#define LgColorAlpha(color) ((color >> 24) & 0xff)

#define LgColorBlue(color) ((color >> 16) & 0xff)

#define LgColorGreen(color) ((color >> 8) & 0xff)

#define LgColorRed(color) (color & 0xff)

#define LgColorRedF(color) ((float)LgColorRed(color) / 255.0f)

#define LgColorGreenF(color) ((float)LgColorGreen(color) / 255.0f)

#define LgColorBlueF(color) ((float)LgColorBlue(color) / 255.0f)

#define LgColorAlphaF(color) ((float)LgColorAlpha(color) / 255.0f)

#define LgColorSwapRedBlue(color) LgColorARGB(LgColorAlpha(color), LgColorBlue(color), LgColorGreen(color), LgColorRed(color))

// size
typedef struct LgSize {
	int width, height;
}*LgSizeP;

// point
typedef struct LgPoint {
	int x, y;
}*LgPointP;

// rect
typedef struct LgRect {
	int x, y, width, height;
}*LgRectP;

// range
typedef struct LgRange {
	int begin, size;
}*LgRangeP;

// slot
typedef struct _LgSlot {
	LgBinaryOp	action;
	LgDestroy	destroyUserData;
	LgClone		cloneUserData;
	void *		userdata;
}LgSlot;

#define LG_SWAP_SHORT(l)            \
	( ( ((l) >> 8) & 0x000000FF) |  \
	( ((l) <<  8) & 0x0000FF00) )

#define LG_SWAP_LONG(l)                \
	( ( ((l) >> 24) & 0x000000FFL ) |  \
	( ((l) >>  8) & 0x0000FF00L ) |    \
	( ((l) <<  8) & 0x00FF0000L ) |    \
	( ((l) << 24) & 0xFF000000L ) )

#define LG_SWAP_LONGLONG(l)                        \
	( ( ((l) >> 56) & 0x00000000000000FFLL ) |     \
	( ((l) >> 40) & 0x000000000000FF00LL ) |       \
	( ((l) >> 24) & 0x0000000000FF0000LL ) |       \
	( ((l) >>  8) & 0x00000000FF000000LL ) |       \
	( ((l) <<  8) & 0x000000FF00000000LL ) |       \
	( ((l) << 24) & 0x0000FF0000000000LL ) |       \
	( ((l) << 40) & 0x00FF000000000000LL ) |       \
	( ((l) << 56) & 0xFF00000000000000LL ) )

#define SLOT_PARAMS LgBinaryOp action, LgDestroy destroyUserdata, LgClone cloneUserdata, void *userdata

#define SLOT_PARAMS_VALUE action, destroyUserdata, cloneUserdata, userdata

#define SLOT_PARAMS_VALUE_NULL NULL, NULL, NULL, NULL

#define SLOT_PARAMS_VALUE_REF(slot) (slot).action, (slot).destroyUserData, (slot).cloneUserData, (slot).userdata

void LgSlotClear(LgSlot *slot);

void LgSlotAssign(LgSlot *slot, SLOT_PARAMS);

void LgSlotCall(LgSlot *slot, void *param);

void LgSlotClone(LgSlot *dest, LgSlot *src);

LgBool LgSlotIsEmpty(LgSlot *slot);

#endif