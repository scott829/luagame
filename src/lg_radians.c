#include "lg_radians.h"

float LgRadiansToAngle(float radians) {
	return radians * 57.295779f;
}

float LgAngleToRadians(float angle) {
	return angle * 0.017453f;
}
