#include "lg_vector2.h"
#include "lg_radians.h"

struct LgVector2 LgVector2Zero = {0, 0};

float LgVector2Length(const LgVector2P vec) {
	return sqrtf(vec->x * vec->x + vec->y * vec->y);
}

float LgVector2SquaredLength(const LgVector2P vec) {
	return vec->x * vec->x + vec->y * vec->y;
}

float LgVector2Distance(const LgVector2P lhs, const LgVector2P rhs) {
	struct LgVector2 t = {lhs->x - rhs->x, lhs->y - rhs->y};
	return LgVector2Length(&t);
}

float LgVector2SquaredDistance(const LgVector2P lhs, const LgVector2P rhs) {
	struct LgVector2 t = {lhs->x - rhs->x, lhs->y - rhs->y};
	return LgVector2SquaredLength(&t);
}

void LgVector2MidPoint(LgVector2P lhs, const LgVector2P rhs) {
	lhs->x = (lhs->x + rhs->x) / 2;
	lhs->y = (lhs->y + rhs->y) / 2;
}

void LgVector2Normalize(LgVector2P vec) {
	float d = sqrtf(vec->x * vec->x + vec->y * vec->y);
	vec->x /= d, vec->y /= d;
}

LgBool LgVector2IsZero(const LgVector2P vec) {
	return LgFloatEqual(vec->x, 0.0f) && LgFloatEqual(vec->y, 0.0f);
}

void LgVector2Add(LgVector2P lhs, const LgVector2P rhs) {
	lhs->x = lhs->x + rhs->x;
	lhs->y = lhs->y + rhs->y;
}

void LgVector2Sub(LgVector2P lhs, const LgVector2P rhs) {
	lhs->x = lhs->x - rhs->x;
	lhs->y = lhs->y - rhs->y;
}

void LgVector2Mul(LgVector2P lhs, const LgVector2P rhs) {
	lhs->x = lhs->x * rhs->x;
	lhs->y = lhs->y * rhs->y;
}

void LgVector2Div(LgVector2P lhs, const LgVector2P rhs) {
	lhs->x = lhs->x / rhs->x;
	lhs->y = lhs->y / rhs->y;
}

LgBool LgVector2Equal(LgVector2P lhs, const LgVector2P rhs) {
	return LgFloatEqual(lhs->x, rhs->x) && LgFloatEqual(lhs->y, rhs->y);
}

void LgVector2AddFloat(LgVector2P lhs, float rhs) {
	lhs->x = lhs->x + rhs;
	lhs->y = lhs->y + rhs;
}

void LgVector2SubFloat(LgVector2P lhs, float rhs) {
	lhs->x = lhs->x - rhs;
	lhs->y = lhs->y - rhs;
}

void LgVector2MulFloat(LgVector2P lhs, float rhs) {
	lhs->x = lhs->x * rhs;
	lhs->y = lhs->y * rhs;
}

void LgVector2DivFloat(LgVector2P lhs, float rhs) {
	lhs->x = lhs->x / rhs;
	lhs->y = lhs->y / rhs;
}

float LgVector2ToAngle(LgVector2P vec) {
	return LgRadiansToAngle(atan2f(vec->y, vec->x));
}

float LgVector2ToVector2Angle(LgVector2P vec1, LgVector2P vec2) {
	struct LgVector2 _vec1, _vec2;

	_vec1 = *vec1, _vec2 = *vec2;
	LgVector2Sub(&_vec2, &_vec1);
	LgVector2Normalize(&_vec2);
	return LgVector2ToAngle(&_vec2);
}

float LgVector2Cross(LgVector2P lhs, const LgVector2P rhs) {
	return lhs->x * rhs->y - lhs->y * rhs->x;
}

static LgBool sameSide(float x0, float y0, float x1, float y1, float x2, float y2, float x3, float y3) {
	float a, b, c;
	a = y0 - y1;
	b = x1 - x0;
	c = x0 * y1 - x1 * y0;
	return ((a * x2 + b * y2 + c) * (a * x3 + b * y3 + c) > 0);
}

LgBool LgVector2InTriangle(const LgVector2P vec, const LgVector2P a, const LgVector2P b, const LgVector2P c) {
	return !(sameSide(vec->x,vec->y,a->x,a->y,b->x,b->y,c->x,c->y) || 
		sameSide(vec->x,vec->y,b->x,b->y,c->x,c->y,a->x,a->y) ||
		sameSide(vec->x,vec->y,c->x,c->y,a->x,a->y,b->x,b->y));
}
