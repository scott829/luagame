#include "lg_refcnt.h"

void *LgRetain(void *p) {
	((struct LgBaseRefCnt *)p)->_refcnt++;
	return p;
}

void LgRelease(void *p) {
	struct LgBaseRefCnt *obj = (struct LgBaseRefCnt *)p;
	assert(obj->_refcnt > 0);
	obj->_refcnt--;
	if (obj->_refcnt == 0)
		obj->_destroy(p);
}

int LgRefCount(void *p) {
	return ((struct LgBaseRefCnt *)p)->_refcnt;
}
