#ifndef _LG_WINDOW_OSX_H_
#define _LG_WINDOW_OSX_H_

#include "lg_window.h"
#import <Cocoa/Cocoa.h>

NSWindow *LgWindowNSWindow();

NSOpenGLView *LgWindowNSOpenGLView();

#endif