﻿#include "lg_grid.h"
#include "lg_memory.h"
#include "lg_rendersystem.h"
#include "lg_director.h"

void LgGridSize(LgGridP grid, LgSizeP size) {
	size->width = grid->cols;
	size->height =grid->rows;
}

void LgGridCellSize(LgGridP grid, LgVector2P cellSize) {
	struct LgSize viewSize;
	LgDirectorViewSize(&viewSize);
	cellSize->x = (float)viewSize.width / grid->cols;
	cellSize->y = (float)viewSize.height / grid->rows;
}

// grid 3d ----------------------------------------------------------------------
struct LgGrid3D {
	struct LgGrid			base;
	LgVertexTexCroodP		buffer;
	LgBool					dirty;
	LgVertexBufferP			vb;
	LgIndexBufferP			ib;
	LgRenderTargetP			target;
	int						vertexCount;
};

static void destroyGrid3D(struct LgGrid3D *grid) {
	if (grid->buffer) LgFree(grid->buffer);
	if (grid->target) LgRenderTargetDestroy(grid->target);
	if (grid->vb) LgVertexBufferDestroy(grid->vb);
	if (grid->ib) LgIndexBufferDestroy(grid->ib);
	LgFree(grid);
}

static int grid3dVertexIdx(LgGridP grid, int col, int row, enum LgGridAnchor anchor) {
	int pos = 0;

	switch(anchor) {
	case LgGridAnchorLT: pos = col + (grid->cols + 1) * row; break;
	case LgGridAnchorRT: pos = col + (grid->cols + 1) * row + 1; break;
	case LgGridAnchorLB: pos = col + (grid->cols + 1) * (row + 1); break;
	case LgGridAnchorRB: pos = col + (grid->cols + 1) * (row + 1) + 1; break;
	}

	return pos;
}

static LgVertexTexCroodP grid3dVertex(struct LgGrid3D *grid, int col, int row, enum LgGridAnchor anchor) {
	return &grid->buffer[LgGridVertexIdx(grid, col, row, anchor)];
}

static void grid3dExit(LgGridP grid) {
	struct LgGrid3D *grid3d = (struct LgGrid3D *)grid;
	if (grid3d->target) LgRenderTargetDestroy(grid3d->target);
	grid3d->target = NULL;
	if (grid3d->vb) LgVertexBufferDestroy(grid3d->vb);
	grid3d->vb = NULL;
	if (grid3d->ib) LgIndexBufferDestroy(grid3d->ib);
	grid3d->ib = NULL;
}

static void grid3dBuildVertexBuffer(struct LgGrid3D *grid) {
	struct LgGrid3D *grid3d = (struct LgGrid3D *)grid;
	LgBool copy = LgFalse;

	if (!grid->vb) {
		grid->vb = LgRenderSystemCreateVertexBuffer(sizeof(struct LgVertexTexCrood) * grid3d->vertexCount);
		copy = LgTrue;
	}

	if ((copy || grid->dirty) && grid->vb) {
		void *p = LgVertexBufferLock(grid->vb);
		LgCopyMemory(p, grid3d->buffer, sizeof(struct LgVertexTexCrood) * grid3d->vertexCount);
		LgVertexBufferUnlock(grid->vb);
		grid->dirty = LgFalse;
	}

	if (!grid->ib) {
		int idxcount = grid3d->base.cols * grid3d->base.rows * 6;

		grid3d->ib = LgRenderSystemCreateIndexBuffer(idxcount);

		if (grid3d->ib) {
			unsigned short *pidx = (unsigned short *)LgIndexBufferLock(grid3d->ib);
			int col, row;

			for (row = 0; row < grid3d->base.rows; row++) {
				for (col = 0; col < grid3d->base.cols; col++) {
					*pidx++ = LgGridVertexIdx(grid3d, col, row, LgGridAnchorLB);
					*pidx++ = LgGridVertexIdx(grid3d, col, row, LgGridAnchorLT);
					*pidx++ = LgGridVertexIdx(grid3d, col, row, LgGridAnchorRB);

					*pidx++ = LgGridVertexIdx(grid3d, col, row, LgGridAnchorLT);
					*pidx++ = LgGridVertexIdx(grid3d, col, row, LgGridAnchorRT);
					*pidx++ = LgGridVertexIdx(grid3d, col, row, LgGridAnchorRB);
				}
			}
			LgIndexBufferUnlock(grid3d->ib);
		}
	}
}

static void grid3dBeforeDraw(LgGridP grid) {
	struct LgGrid3D *grid3d = (struct LgGrid3D *)grid;
	struct LgSize windowSize;

	LgWindowSize(&windowSize);

	if (!grid3d->target) 
		grid3d->target = LgRenderSystemCreateRenderTarget(&windowSize);

	if (grid3d->target) {
		LgRenderSystemBegin(grid3d->target);
		LgRenderSystemClear(LgClearFlagColor | LgClearFlagStencil, 0, 0);
	}
}

static void grid3dAfterDraw(LgGridP grid) {
	struct LgGrid3D *grid3d = (struct LgGrid3D *)grid;

	if (grid3d->target) {
		struct LgMatrix44 oldMatrix, newMatrix;

		LgRenderSystemEnd();
		grid3dBuildVertexBuffer(grid3d);

		LgRenderSystemGetTransform(LgTransformWorld, &oldMatrix);
		LgMatrix44Indentity(&newMatrix);
		LgRenderSystemSetTransform(LgTransformWorld, &newMatrix);

		LgRenderSystemSetTexture(LgRenderTargetTexture(grid3d->target));
		LgRenderSystemSetPixelShader(LgPixelShaderCopyRenderTarget);
		LgRenderSystemDrawIndexPrimitive(LgPrimitiveTriangleList, LgFvfXYZ | LgFvfTexture, 
			grid3d->vb, grid3d->ib, grid3d->base.cols * grid3d->base.rows * 2, grid3d->vertexCount);

		LgRenderSystemSetTransform(LgTransformWorld, &oldMatrix);
		LgRenderSystemSetTexture(NULL);
	}
}

struct LgGridImpl grid3dImpl = {
	(LgDestroy)destroyGrid3D,
	grid3dBeforeDraw,
	grid3dAfterDraw,
	NULL,
	grid3dExit,
	grid3dVertexIdx
};

LgGridP LgGrid3DCreate(int cols, int rows) {
	struct LgGrid3D *grid = (struct LgGrid3D *)LgMallocZ(sizeof(struct LgGrid3D));
	struct LgSize viewSize;
	float cellWidth, cellHeight;
	int row, col;

	LG_CHECK_ERROR(grid);
	grid->base.impl = &grid3dImpl;
	grid->base.cols = cols, grid->base.rows = rows;
	grid->vertexCount = (cols + 1) * (rows + 1);
	grid->buffer = (LgVertexTexCroodP)LgMallocZ(sizeof(struct LgVertexTexCrood) * grid->vertexCount);
	LG_CHECK_ERROR(grid);

	LgDirectorViewSize(&viewSize);
	cellWidth = (float)viewSize.width / cols;
	cellHeight = (float)viewSize.height / rows;

	for (row = 0; row <= rows; row++) {
		for (col = 0; col <= cols; col++) {
			LgVertexTexCroodP vertex = &grid->buffer[col + (cols + 1) * row];
			vertex->position.x = col * cellWidth;
			vertex->position.y = row * cellHeight;
			vertex->texcrood.x = (float)col / cols;
			vertex->texcrood.y = (float)row / rows;
		}
	}

	return (LgGridP)grid;

_error:
	LgGridDestroy(grid);
	return NULL;
}

void LgGrid3DSetPositionWithAnchor(LgGridP grid, int col, int row, enum LgGridAnchor anchor, const LgVector3P pt) {
	struct LgGrid3D *grid3d = (struct LgGrid3D *)grid;
	LgCopyMemory(&grid3dVertex(grid3d, col, row, anchor)->position, pt, sizeof(struct LgVector3));
	grid3d->dirty = LgTrue;
}

void LgGrid3DPositionWithAnchor(LgGridP grid, int col, int row, enum LgGridAnchor anchor, LgVector3P pt) {
	struct LgGrid3D *grid3d = (struct LgGrid3D *)grid;
	LgCopyMemory(pt, &grid3dVertex(grid3d, col, row, anchor)->position, sizeof(struct LgVector3));
}

void LgGrid3DOrgPositionWithAnchor(LgGridP grid, int col, int row, enum LgGridAnchor anchor, LgVector3P pt) {
	struct LgGrid3D *grid3d = (struct LgGrid3D *)grid;
	int idx = LgGridVertexIdx(grid3d, col, row, anchor);
	int x, y;
	struct LgSize viewSize;
	float cellWidth, cellHeight;

	LgDirectorViewSize(&viewSize);
	cellWidth = (float)viewSize.width / grid3d->base.cols;
	cellHeight = (float)viewSize.height / grid3d->base.rows;

	x = idx % (grid3d->base.cols + 1);
	y = idx / (grid3d->base.cols + 1);

	pt->x = x * cellWidth;
	pt->x = y * cellHeight;
	pt->z = 0;
}

void LgGrid3DSetPosition(LgGridP grid, int x, int y, const LgVector3P pt) {
	struct LgGrid3D *grid3d = (struct LgGrid3D *)grid;
	LgCopyMemory(&grid3d->buffer[y * (grid3d->base.cols + 1) + x].position, pt, sizeof(struct LgVector3));
	grid3d->dirty = LgTrue;
}

void LgGrid3DPosition(LgGridP grid, int x, int y, LgVector3P pt) {
	struct LgGrid3D *grid3d = (struct LgGrid3D *)grid;
	LgCopyMemory(pt, &grid3d->buffer[y * (grid3d->base.cols + 1) + x].position, sizeof(struct LgVector3));
}

void LgGrid3DOrgPosition(LgGridP grid, int x, int y, LgVector3P pt) {
	struct LgSize viewSize;
	float cellWidth, cellHeight;

	LgDirectorViewSize(&viewSize);
	cellWidth = (float)viewSize.width / grid->cols;
	cellHeight = (float)viewSize.height / grid->rows;

	pt->x = x * cellWidth;
	pt->y = y * cellHeight;
	pt->z = 0;
}

// tilegrid 3d ----------------------------------------------------------------------
static int tilegrid3dVertexIdx(LgGridP tilegrid, int col, int row, enum LgGridAnchor anchor) {
	int pos = col * 4 + row * tilegrid->cols * 4;

	switch(anchor) {
	case LgGridAnchorLT: return pos + 1;
	case LgGridAnchorRT: return pos + 3;
	case LgGridAnchorLB: return pos;
	case LgGridAnchorRB: return pos + 2;
	}

	return pos;
}

struct LgGridImpl tilegrid3dImpl = {
	(LgDestroy)destroyGrid3D,
	grid3dBeforeDraw,
	grid3dAfterDraw,
	NULL,
	grid3dExit,
	tilegrid3dVertexIdx
};

LgGridP LgTileGrid3DCreate(int cols, int rows) {
	struct LgGrid3D *grid = (struct LgGrid3D *)LgMallocZ(sizeof(struct LgGrid3D));
	struct LgSize viewSize;
	float cellWidth, cellHeight;
	int row, col;
	float uvw = 1.0f / cols, uvh = 1.0f / rows;

	LG_CHECK_ERROR(grid);
	grid->base.impl = &tilegrid3dImpl;
	grid->base.cols = cols, grid->base.rows = rows;
	grid->vertexCount = cols * rows * 4;
	grid->buffer = (LgVertexTexCroodP)LgMallocZ(sizeof(struct LgVertexTexCrood) * grid->vertexCount);
	LG_CHECK_ERROR(grid);

	LgDirectorViewSize(&viewSize);
	cellWidth = (float)viewSize.width / cols;
	cellHeight = (float)viewSize.height / rows;

	for (row = 0; row < rows; row++) {
		for (col = 0; col < cols; col++) {
			LgVertexTexCroodP p = &grid->buffer[cols * 4 * row + col * 4];
			float cellLeft = col * cellWidth, cellTop = row * cellHeight;
			float uvl = (float)col / cols, uvt = (float)row / rows;

			// 左下
			p->position.x = cellLeft, p->position.y = cellTop + cellHeight, p->position.z = 0;
			p->texcrood.x = uvl, p->texcrood.y = uvt + uvh;
			p++;

			// 左上
			p->position.x = cellLeft, p->position.y = cellTop, p->position.z = 0;
			p->texcrood.x = uvl, p->texcrood.y = uvt;
			p++;

			// 右下
			p->position.x = cellLeft + cellWidth, p->position.y = cellTop + cellHeight, p->position.z = 0;
			p->texcrood.x = uvl + uvw, p->texcrood.y = uvt + uvh;
			p++;

			// 右上
			p->position.x = cellLeft + cellWidth, p->position.y = cellTop, p->position.z = 0;
			p->texcrood.x = uvl + uvw, p->texcrood.y = uvt;
			p++;
		}
	}

	return (LgGridP)grid;

_error:
	LgGridDestroy(grid);
	return NULL;
}

void LgTileGrid3DSetPositionWithAnchor(LgGridP grid, int col, int row, enum LgGridAnchor anchor, const LgVector3P pt) {
	struct LgGrid3D *tilegrid3d = (struct LgGrid3D *)grid;
	LgVector3P vec = &grid3dVertex(tilegrid3d, col, row, anchor)->position;
	LgCopyMemory(vec, pt, sizeof(struct LgVector3));
	tilegrid3d->dirty = LgTrue;
}

void LgTileGrid3DPositionWithAnchor(LgGridP grid, int col, int row, enum LgGridAnchor anchor, LgVector3P pt) {
	struct LgGrid3D *tilegrid3d = (struct LgGrid3D *)grid;
	LgVector3P vec = &grid3dVertex(tilegrid3d, col, row, anchor)->position;
	LgCopyMemory(pt, vec, sizeof(struct LgVector3));
}

void LgTileGrid3DOrgPositionWithAnchor(LgGridP grid, int col, int row, enum LgGridAnchor anchor, LgVector3P pt) {
	struct LgSize viewSize;
	float cellWidth, cellHeight;

	LgDirectorViewSize(&viewSize);
	cellWidth = (float)viewSize.width / grid->cols;
	cellHeight = (float)viewSize.height / grid->rows;

	switch(anchor) {
	case LgGridAnchorLT:
		pt->x = col * cellWidth, pt->y = row * cellHeight, pt->z = 0;
		break;
	case LgGridAnchorRT:
		pt->x = col * cellWidth + cellWidth, pt->y = row * cellHeight, pt->z = 0;
		break;
	case LgGridAnchorLB:
		pt->x = col * cellWidth, pt->y = row * cellHeight + cellHeight, pt->z = 0;
		break;
	case LgGridAnchorRB:
		pt->x = col * cellWidth + cellWidth, pt->y = row * cellHeight + cellHeight, pt->z = 0;
		break;
	}
}
