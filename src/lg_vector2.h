#ifndef _LG_VECTOR2_H
#define _LG_VECTOR2_H

#include "lg_basetypes.h"
#include <math.h>

typedef struct LgVector2 {
	float x, y;
}*LgVector2P;

extern struct LgVector2 LgVector2Zero;

float LgVector2Length(const LgVector2P vec);

float LgVector2SquaredLength(const LgVector2P vec);

float LgVector2Distance(const LgVector2P lhs, const LgVector2P rhs);

float LgVector2SquaredDistance(const LgVector2P lhs, const LgVector2P rhs);

void LgVector2MidPoint(LgVector2P lhs, const LgVector2P rhs);

void LgVector2Normalize(LgVector2P vec);

LgBool LgVector2IsZero(const LgVector2P vec);

void LgVector2Add(LgVector2P lhs, const LgVector2P rhs);

void LgVector2Sub(LgVector2P lhs, const LgVector2P rhs);

void LgVector2Mul(LgVector2P lhs, const LgVector2P rhs);

void LgVector2Div(LgVector2P lhs, const LgVector2P rhs);

LgBool LgVector2Equal(LgVector2P lhs, const LgVector2P rhs);

void LgVector2AddFloat(LgVector2P lhs, float rhs);

void LgVector2SubFloat(LgVector2P lhs, float rhs);

void LgVector2MulFloat(LgVector2P lhs, float rhs);

void LgVector2DivFloat(LgVector2P lhs, float rhs);

float LgVector2ToAngle(LgVector2P vec);

float LgVector2ToVector2Angle(LgVector2P vec1, LgVector2P vec2);

float LgVector2Cross(LgVector2P lhs, const LgVector2P rhs);

LgBool LgVector2InTriangle(const LgVector2P vec, const LgVector2P a, const LgVector2P b, const LgVector2P c);

#endif