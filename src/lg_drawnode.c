#include "lg_drawnode.h"
#include "lg_memoutputstream.h"
#include <float.h>
#include "lg_director.h"

typedef struct LgDrawNodeData {
	LgOutputStreamP	buffer;
	LgVertexBufferP	vb;
	LgColor			color;
	struct LgAabb	contentBox;
}*LgDrawNodeDataP;

static void makeDirty(LgDrawNodeDataP dd) {
	if (dd->vb) LgVertexBufferDestroy(dd->vb);
	dd->vb = NULL;
}

static void prepareBuffer(LgDrawNodeDataP dd) {
	if (!dd->buffer) dd->buffer = LgMemOutputStreamCreate(NULL, 0);
}

static void buildBuffer(LgNodeP node, LgDrawNodeDataP dd) {
	if (!dd->vb && dd->buffer) {
		float xMax, yMax, xMin, yMin;
		const struct LgVertexDiffuse *p;
		int count, i;

		dd->vb = LgRenderSystemCreateVertexBuffer(LgMemOutputStreamLen(dd->buffer));
		if (dd->vb) {
			LgCopyMemory(LgVertexBufferLock(dd->vb), LgMemOutputStreamData(dd->buffer), 
				LgMemOutputStreamLen(dd->buffer));
			LgVertexBufferUnlock(dd->vb);
		}

		xMax = yMax = FLT_MIN;
		xMin = yMin = FLT_MAX;
		p = (const struct LgVertexDiffuse *)LgMemOutputStreamData(dd->buffer);
		count = LgMemOutputStreamLen(dd->buffer) / sizeof(struct LgVertexDiffuse);

		for (i = 0; i < count; i++, p++) {
			xMax = LgMax(p->position.x, xMax);
			xMin = LgMin(p->position.x, xMin);
			yMax = LgMax(p->position.y, yMax);
			yMin = LgMin(p->position.y, yMin);
		}

		dd->contentBox.x = xMin, dd->contentBox.y = yMin;
		dd->contentBox.width = xMax - xMin;
		dd->contentBox.height = yMax - yMin;
	}
}

static void drawRectangle(LgDrawNodeDataP dd, const LgAabbP box) {
	struct LgVertexDiffuse v[3];

	LgZeroMemory(v, sizeof(v));

	v[0].position.x = box->x,				v[0].position.y = box->y + box->height,	v[0].color = dd->color;
	v[1].position.x = box->x,				v[1].position.y = box->y,				v[1].color = dd->color;
	v[2].position.x = box->x + box->width,	v[2].position.y = box->y + box->height,	v[2].color = dd->color;
	LgOutputStreamWriteBytes(dd->buffer, v, sizeof(v));

	v[0].position.x = box->x,				v[0].position.y = box->y,				v[0].color = dd->color;
	v[1].position.x = box->x + box->width,	v[1].position.y = box->y,				v[1].color = dd->color;
	v[2].position.x = box->x + box->width,	v[2].position.y = box->y + box->height,	v[2].color = dd->color;
	LgOutputStreamWriteBytes(dd->buffer, v, sizeof(v));
}

static void drawFan(LgDrawNodeDataP dd, const LgAabbP box, float start, float end) {
	const int triangleCount = 32;
	int i;
	struct LgVertexDiffuse v[3];
	float rx = box->width / 2, ry = box->height / 2;
	float cx = box->x + rx, cy = box->y + ry;
	float seg = (float)(end - start) / triangleCount, s = start;

	LgZeroMemory(v, sizeof(v));

	for (i = 0; i < triangleCount; i++) {
		float sx, sy, ex, ey;

		sx = cx + rx * cosf(s);
		sy = cy + ry * sinf(s);

		ex = cx + rx * cosf(s + seg);
		ey = cy + ry * sinf(s + seg);

		v[0].position.x = cx,	v[0].position.y = cy,	v[0].color = dd->color;
		v[1].position.x = sx,	v[1].position.y = sy,	v[1].color = dd->color;
		v[2].position.x = ex,	v[2].position.y = ey,	v[2].color = dd->color;
		LgOutputStreamWriteBytes(dd->buffer, v, sizeof(v));
		s += seg;
	}
}

static void drawTriangle(LgDrawNodeDataP dd, const LgDrawNodeEventDrawTriangleP t) {
	struct LgVertexDiffuse v[3];

	LgZeroMemory(v, sizeof(v));

	v[0].position.x = t->pt1.x,	v[0].position.y = t->pt1.y,	v[0].color = dd->color;
	v[1].position.x = t->pt2.x,	v[1].position.y = t->pt2.y,	v[1].color = dd->color;
	v[2].position.x = t->pt3.x,	v[2].position.y = t->pt3.y,	v[2].color = dd->color;
	LgOutputStreamWriteBytes(dd->buffer, v, sizeof(v));
}

static void drawNodeEventProc(LgEventType type, LgNodeP node, void *param) {
	switch(type) {
	case LG_NODE_EVENT_Destroy:
		{
			LgDrawNodeDataP dd = (LgDrawNodeDataP)LgNodeUserData(node);
			if (dd->vb) LgVertexBufferDestroy(dd->vb);
			if (dd->buffer) LgOutputStreamDestroy(dd->buffer);
			LgFree(dd);
		}
		break;
	case LG_NODE_EVENT_Draw:
		{
			LgDrawNodeDataP dd = (LgDrawNodeDataP)LgNodeUserData(node);
			buildBuffer(node, dd);
			if (dd->vb) {
				LgRenderSystemSetPixelShader(LgPixelShaderCxformDiffuse);
				LgRenderSystemDrawPrimitive(LgPrimitiveTriangleList, LgFvfXYZ | LgFvfDiffuse, dd->vb, 
					LgMemOutputStreamLen(dd->buffer) / sizeof(struct LgVertexDiffuse) / 3);
				LgDirectorUpdateBatch(1);
			}
			return;
		}
		break;
	case LG_NODE_EVENT_Exit:
		makeDirty((LgDrawNodeDataP)LgNodeUserData(node));
		break;
	case LG_DRAWNODE_EVENT_SetColor:
		((LgDrawNodeDataP)LgNodeUserData(node))->color = *((LgColor *)param);
		break;
	case LG_DRAWNODE_EVENT_Color:
		*((LgColor *)param) = ((LgDrawNodeDataP)LgNodeUserData(node))->color;
		break;
	case LG_DRAWNODE_EVENT_Clear:
		{
			LgDrawNodeDataP dd = (LgDrawNodeDataP)LgNodeUserData(node);
			if (dd->buffer) {
				LgOutputStreamDestroy(dd->buffer);
				dd->buffer = NULL;
			}
			makeDirty(dd);
			LgZeroMemory(&dd->contentBox, sizeof(dd->contentBox));
		}
		break;
	case LG_DRAWNODE_EVENT_Rectangle:
		{
			LgDrawNodeDataP dd = (LgDrawNodeDataP)LgNodeUserData(node);
			prepareBuffer(dd);
			drawRectangle(dd, (const LgAabbP)param);
			makeDirty(dd);
		}
		break;
	case LG_DRAWNODE_EVENT_Fan:
		{
			LgDrawNodeDataP dd = (LgDrawNodeDataP)LgNodeUserData(node);
			const LgDrawNodeEventDrawFanP p = (const LgDrawNodeEventDrawFanP)param;
			prepareBuffer(dd);
			drawFan(dd, &p->box, LgAngleToRadians(p->start), LgAngleToRadians(p->end));
			makeDirty(dd);
		}
		break;
	case LG_DRAWNODE_EVENT_Ellipse:
		{
			LgDrawNodeDataP dd = (LgDrawNodeDataP)LgNodeUserData(node);
			prepareBuffer(dd);
			drawFan(dd, (const LgAabbP)param, LgAngleToRadians(0), LgAngleToRadians(360));
			makeDirty(dd);
		}
		break;
	case LG_DRAWNODE_EVENT_Triangle:
		{
			LgDrawNodeDataP dd = (LgDrawNodeDataP)LgNodeUserData(node);
			const LgDrawNodeEventDrawTriangleP p = (const LgDrawNodeEventDrawTriangleP)param;
			prepareBuffer(dd);
			drawTriangle(dd, p);
			makeDirty(dd);
		}
		break;
	case LG_NODE_EVENT_ContentBox:
		*((LgAabbP)param) = ((LgDrawNodeDataP)LgNodeUserData(node))->contentBox;
		return;
	}

	LgNodeDefEventProc(type, node, param);
}

LgNodeP LgDrawNodeCreateR() {
	LgNodeP node;
	LgDrawNodeDataP dd;

	node = LgNodeCreateR((LgEvent)drawNodeEventProc);
	LG_CHECK_ERROR(node);

	dd = (LgDrawNodeDataP)LgMallocZ(sizeof(struct LgDrawNodeData));
	LG_CHECK_ERROR(dd);
	dd->color = LgColorXRGB(255, 255, 255);
	LgNodeSetUserData(node, dd);

	return node;

_error:
	if (node) LgNodeRelease(node);
	return NULL;
}

void LgDrawNodeSetColor(LgNodeP node, LgColor color) {
	LgNodeDoEvent(LG_DRAWNODE_EVENT_SetColor, node, &color);
}

LgColor LgDrawNodeColor(LgNodeP node) {
	LgColor color;
	LgNodeDoEvent(LG_DRAWNODE_EVENT_Color, node, &color);
	return color;
}

void LgDrawNodeClear(LgNodeP node) {
	LgNodeDoEvent(LG_DRAWNODE_EVENT_Clear, node, NULL);
}

void LgDrawNodeRectangle(LgNodeP node, float x, float y, float width, float height) {
	struct LgAabb box;
	box.x = x, box.y = y;
	box.width = width, box.height = height;
	LgNodeDoEvent(LG_DRAWNODE_EVENT_Rectangle, node, &box);
}

void LgDrawNodeEllipse(LgNodeP node, float x, float y, float width, float height) {
	struct LgAabb box;
	box.x = x, box.y = y;
	box.width = width, box.height = height;
	LgNodeDoEvent(LG_DRAWNODE_EVENT_Ellipse, node, &box);
}

void LgDrawNodeFan(LgNodeP node, float x, float y, float width, float height, float start, float end) {
	struct LgDrawNodeEventDrawFan df;
	df.box.x = x, df.box.y = y;
	df.box.width = width, df.box.height = height;
	df.start = start, df.end = end;
	LgNodeDoEvent(LG_DRAWNODE_EVENT_Fan, node, &df);
}

void LgDrawNodeTriangle(LgNodeP node, float x1, float y1, float x2, float y2, float x3, float y3) {
	struct LgDrawNodeEventDrawTriangle dt;
	dt.pt1.x = x1, dt.pt1.y = y1;
	dt.pt2.x = x2, dt.pt2.y = y2;
	dt.pt3.x = x3, dt.pt3.y = y3;
	LgNodeDoEvent(LG_DRAWNODE_EVENT_Triangle, node, &dt);
}

LgBool LgNodeIsDraw(LgNodeP node) {
	return LgNodeEventProc(node) == (LgEvent)drawNodeEventProc;
}
