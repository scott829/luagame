#include "lg_jpgloader.h"
#include "stdio.h"
#include "libjpg/jpeglib.h"

LgBool jpgCheckType(const unsigned char *data, int size) {
	return size > 2 && data[0] == 0xff && data[1] == 0xd8;
}

size_t JFREAD(FILE *fp, void *buf, size_t sz)
{
	LgInputStreamP in = (LgInputStreamP)fp;
	return LgInputStreamReadBytes(in, buf, sz);
}

LgImageP jpgLoad(LgInputStreamP inputStream) {
	struct jpeg_error_mgr jerr;
	struct jpeg_decompress_struct cinfo;
	int width, height, rowSize;
	JSAMPARRAY buffer;
	LgImageP img = NULL;
	struct LgSize size;

	cinfo.err = jpeg_std_error(&jerr);
	jpeg_create_decompress(&cinfo);
	jpeg_stdio_src(&cinfo, (FILE *)inputStream);
	jpeg_read_header(&cinfo, TRUE);
	jpeg_start_decompress(&cinfo);

	rowSize = cinfo.output_width * cinfo.output_components;
	width = cinfo.output_width;
	height = cinfo.output_height;

	buffer = (*cinfo.mem->alloc_sarray)
		((j_common_ptr) &cinfo, JPOOL_IMAGE, rowSize, 1);

	size.width = width, size.height = height;
	img = LgImageCreateR(&size, LgImageFormatA8R8G8B8);
	LG_CHECK_ERROR(img);
	
	if (cinfo.output_components == 1) {
		while(cinfo.output_scanline < cinfo.output_height) {
			unsigned char *p;
			int i;

			p = (unsigned char *)LgImageLineData(img, cinfo.output_scanline);
			jpeg_read_scanlines(&cinfo, buffer, 1);

			for (i = 0; i < width; i++) {
				*p++ = buffer[0][i];
				*p++ = buffer[0][i];
				*p++ = buffer[0][i];
				*p++ = 0xff;
			}
		}
	} else if (cinfo.output_components == 3) {
		while(cinfo.output_scanline < cinfo.output_height) {
			unsigned char *p;
			const unsigned char *psrc;
			int i;

			p = (unsigned char *)LgImageLineData(img, cinfo.output_scanline);
			jpeg_read_scanlines(&cinfo, buffer, 1);
			psrc = (const unsigned char *)buffer[0];

			for (i = 0; i < width; i++) {
				p[0] = psrc[0];
				p[1] = psrc[1];
				p[2] = psrc[2];
				p[3] = 0xff;
				p += 4;
				psrc += 3;
			}
		}
	}

	jpeg_finish_decompress(&cinfo);
	jpeg_destroy_decompress(&cinfo);
	return img;

_error:
	jpeg_destroy_decompress(&cinfo);
	if (img) LgImageRelease(img);
	return NULL;
}
