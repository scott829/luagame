#include "lg_textureatlas.h"
#include "lg_dict.h"
#include "lg_array.h"
#include "lg_memory.h"
#include "lg_resourcemanager.h"
#include "lg_memoutputstream.h"

struct LgTextureAtlas {
	LG_REFCNT_HEAD
	LgArrayP	textures;
	LgDictP		records;
};

struct LgAtlasDrawBuffer {
	LG_REFCNT_HEAD
	LgTextureAtlasP	atlas;
	LgDictP			buffers;
	LgDictP			vertexBuffers;
};

LG_REFCNT_FUNCTIONS(LgTextureAtlas)

LG_REFCNT_FUNCTIONS(LgAtlasDrawBuffer)

static void textureAtlasDestroy(LgTextureAtlasP ta) {
	if (ta->textures) LgArrayDestroy(ta->textures);
	if (ta->records) LgDictDestroy(ta->records);
	LgFree(ta);
}

LgTextureAtlasP LgTextureAtlasCreateR() {
	LgTextureAtlasP ta = (LgTextureAtlasP)LgMallocZ(sizeof(struct LgTextureAtlas));
	LG_CHECK_ERROR(ta);
	LG_REFCNT_INIT(ta, textureAtlasDestroy);
	ta->textures = LgArrayCreate((LgDestroy)LgTextureRelease);
	LG_CHECK_ERROR(ta->textures);
	ta->records = LgDictCreate(NULL, NULL, LgFree);
	LG_CHECK_ERROR(ta->records);
	return ta;

_error:
	if (ta) LgTextureAtlasRelease(ta);
	return NULL;
}

int LgTextureAtlasAddEmptyTexture(LgTextureAtlasP ta, enum LgImageFormat_ format, const LgSizeP size) {
	LgTextureP texture;
	texture = LgRenderSystemCreateTextureR(format, size);
	LG_CHECK_ERROR(texture);
	LgArrayAppend(ta->textures, texture);
	return LgArrayLen(ta->textures) - 1;

_error:
	return -1;
}

int LgTextureAtlasAddTextureFromFile(LgTextureAtlasP ta, const char *filename) {
	LgTextureP texture;
	texture = LgResourceManagerLoadTexture(filename);
	LG_CHECK_ERROR(texture);
	LgArrayAppend(ta->textures, LgTextureRetain(texture));
	return LgArrayLen(ta->textures);

_error:
	return -1;
}

int LgTextureAtlasAddTexture(LgTextureAtlasP ta, LgTextureP texture) {
	if (!texture) return LgFalse;
	LgTextureRetain(texture);
	LgArrayAppend(ta->textures, texture);
	return LgArrayLen(ta->textures);
}

void LgTextureAtlasRemoveAllTextures(LgTextureAtlasP ta) {
	LgArrayClear(ta->textures);
}

void LgTextureAtlasRemoveAllRecords(LgTextureAtlasP ta) {
	LgDictClear(ta->records);
}

void LgTextureAtlasRemoveAll(LgTextureAtlasP ta) {
	LgArrayClear(ta->textures);
	LgDictClear(ta->records);
}

int LgTextureAtlasCountTextures(LgTextureAtlasP ta) {
	return LgArrayLen(ta->textures);
}

int LgTextureAtlasCountRecord(LgTextureAtlasP ta) {
	return LgDictCount(ta->records);
}

LgTextureP LgTextureAtlasTexture(LgTextureAtlasP ta, int i) {
	return (LgTextureP)LgArrayGet(ta->textures, i - 1);
}

void LgTextureAtlasAddRecord(LgTextureAtlasP ta, int characterId, const LgTextAtlasRecordP record) {
	LgTextAtlasRecordP newRecord = (LgTextAtlasRecordP)LgMallocZ(sizeof(struct LgTextAtlasRecord));
	if (newRecord) {
		LgCopyMemory(newRecord, record, sizeof(struct LgTextAtlasRecord));
		LgDictReplace(ta->records, (void *)characterId, newRecord);
	}
}

LgBool LgTextureAtlasRecord(LgTextureAtlasP ta, int characterId, LgTextAtlasRecordP record) {
	LgTextAtlasRecordP rRecord = (LgTextAtlasRecordP)LgDictFind(ta->records, (void *)characterId);
	if (rRecord) {
		LgCopyMemory(record, rRecord, sizeof(struct LgTextAtlasRecord));
		return LgTrue;
	} else {
		return LgFalse;
	}
}

static void atlasDrawBufferDestroy(LgAtlasDrawBufferP buf) {
	if (buf->atlas) LgTextureAtlasRelease(buf->atlas);
	if (buf->buffers) LgDictDestroy(buf->buffers);
	if (buf->vertexBuffers) LgDictDestroy(buf->vertexBuffers);
	LgFree(buf);
}

static void destroyBuffer(LgOutputStreamP stream) {
	LgOutputStreamDestroy(stream);
}

LgAtlasDrawBufferP LgTextureAtlasCreateDrawBufferR(LgTextureAtlasP ta) {
	LgAtlasDrawBufferP buf = (LgAtlasDrawBufferP)LgMallocZ(sizeof(struct LgAtlasDrawBuffer));
	LG_CHECK_ERROR(buf);
	LG_REFCNT_INIT(buf, atlasDrawBufferDestroy);
	buf->atlas = ta;
	LgTextureAtlasRetain(ta);
	buf->buffers = LgDictCreate(NULL, NULL, (LgDestroy)destroyBuffer);
	buf->vertexBuffers = LgDictCreate(NULL, NULL, (LgDestroy)g_currentRenderSystemDriver->vertexBufferDestroy);
	return buf;

_error:
	if (buf) LgAtlasDrawBufferRelease(buf);
	return NULL;
}

static void makeDirty(LgAtlasDrawBufferP buf, int textureId) {
	LgDictRemove(buf->vertexBuffers, (void *)textureId);
}

static void getUv(const LgTextAtlasRecordP record, const LgSizeP textureSize, float *uv_l, float *uv_t, float *uv_r, float *uv_b) {
#define MODIFIER 0.99f
	*uv_l = (float)(record->pt.x + MODIFIER) / (float)textureSize->width;
	*uv_t = (float)(record->pt.y + MODIFIER) / (float)textureSize->height;
	*uv_r = (float)(record->pt.x + record->size.width - MODIFIER) / (float)textureSize->width;
	*uv_b = (float)(record->pt.y + record->size.height - MODIFIER) / (float)textureSize->height;
}

LgBool LgAtlasBufferDrawRect(LgAtlasDrawBufferP buf, int characterId, const LgAabbP aabb) {
	struct LgTextAtlasRecord record;
	LgTextureP texture;
	LgOutputStreamP buffer;
	struct LgVertexTexCrood vertex;
	struct LgSize textureSize;
	float uv_l, uv_t, uv_r, uv_b;
	
	LG_CHECK_ERROR(LgTextureAtlasRecord(buf->atlas, characterId, &record));
	texture = LgTextureAtlasTexture(buf->atlas, record.textureId);
	LG_CHECK_ERROR(texture);
	LgTextureSize(texture, &textureSize);
	buffer = (LgOutputStreamP)LgDictFind(buf->buffers, (void *)record.textureId);
	if (!buffer) {
		buffer = LgMemOutputStreamCreate(NULL, 0);
		LgDictReplace(buf->buffers, (void *)record.textureId, buffer);
	}
	
	getUv(&record, &textureSize, &uv_l, &uv_t, &uv_r, &uv_b);

	vertex.position.x = aabb->x, vertex.position.y = aabb->y + aabb->height, vertex.position.z = 0;
	vertex.texcrood.x = uv_l, vertex.texcrood.y = uv_b;
	LgOutputStreamWriteBytes(buffer, &vertex, sizeof(vertex));

	vertex.position.x = aabb->x, vertex.position.y = aabb->y, vertex.position.z = 0;
	vertex.texcrood.x = uv_l, vertex.texcrood.y = uv_t;
	LgOutputStreamWriteBytes(buffer, &vertex, sizeof(vertex));

	vertex.position.x = aabb->x + aabb->width, vertex.position.y = aabb->y + aabb->height, vertex.position.z = 0;
	vertex.texcrood.x = uv_r, vertex.texcrood.y = uv_b;
	LgOutputStreamWriteBytes(buffer, &vertex, sizeof(vertex));

	vertex.position.x = aabb->x + aabb->width, vertex.position.y = aabb->y + aabb->height, vertex.position.z = 0;
	vertex.texcrood.x = uv_r, vertex.texcrood.y = uv_b;
	LgOutputStreamWriteBytes(buffer, &vertex, sizeof(vertex));

	vertex.position.x = aabb->x + aabb->width, vertex.position.y = aabb->y, vertex.position.z = 0;
	vertex.texcrood.x = uv_r, vertex.texcrood.y = uv_t;
	LgOutputStreamWriteBytes(buffer, &vertex, sizeof(vertex));

	vertex.position.x = aabb->x, vertex.position.y = aabb->y, vertex.position.z = 0;
	vertex.texcrood.x = uv_l, vertex.texcrood.y = uv_t;
	LgOutputStreamWriteBytes(buffer, &vertex, sizeof(vertex));

	makeDirty(buf, record.textureId);
	return LgTrue;

_error:
	return LgFalse;
}

LgBool LgAtlasBufferDrawQuad(LgAtlasDrawBufferP buf, int characterId,
							 const LgVector2P lt, const LgVector2P rt, const LgVector2P lb, const LgVector2P rb) {
	struct LgTextAtlasRecord record;
	LgTextureP texture;
	LgOutputStreamP buffer;
	struct LgVertexTexCrood vertex;
	struct LgSize textureSize;
	float uv_l, uv_t, uv_r, uv_b;
	
	LG_CHECK_ERROR(LgTextureAtlasRecord(buf->atlas, characterId, &record));
	texture = LgTextureAtlasTexture(buf->atlas, record.textureId);
	LG_CHECK_ERROR(texture);
	LgTextureSize(texture, &textureSize);
	buffer = (LgOutputStreamP)LgDictFind(buf->buffers, (void *)record.textureId);
	if (!buffer) {
		buffer = LgMemOutputStreamCreate(NULL, 0);
		LgDictReplace(buf->buffers, (void *)record.textureId, buffer);
	}
	
	getUv(&record, &textureSize, &uv_l, &uv_t, &uv_r, &uv_b);
	
	vertex.position.x = lb->x, vertex.position.y = lb->y, vertex.position.z = 0;
	vertex.texcrood.x = uv_l, vertex.texcrood.y = uv_b;
	LgOutputStreamWriteBytes(buffer, &vertex, sizeof(vertex));
	
	vertex.position.x = lt->x, vertex.position.y = lt->y, vertex.position.z = 0;
	vertex.texcrood.x = uv_l, vertex.texcrood.y = uv_t;
	LgOutputStreamWriteBytes(buffer, &vertex, sizeof(vertex));
	
	vertex.position.x = rb->x, vertex.position.y = rb->y, vertex.position.z = 0;
	vertex.texcrood.x = uv_r, vertex.texcrood.y = uv_b;
	LgOutputStreamWriteBytes(buffer, &vertex, sizeof(vertex));
	
	vertex.position.x = rb->x, vertex.position.y = rb->y, vertex.position.z = 0;
	vertex.texcrood.x = uv_r, vertex.texcrood.y = uv_b;
	LgOutputStreamWriteBytes(buffer, &vertex, sizeof(vertex));
	
	vertex.position.x = rt->x, vertex.position.y = rt->y, vertex.position.z = 0;
	vertex.texcrood.x = uv_r, vertex.texcrood.y = uv_t;
	LgOutputStreamWriteBytes(buffer, &vertex, sizeof(vertex));
	
	vertex.position.x = lt->x, vertex.position.y = lt->y, vertex.position.z = 0;
	vertex.texcrood.x = uv_l, vertex.texcrood.y = uv_t;
	LgOutputStreamWriteBytes(buffer, &vertex, sizeof(vertex));
	
	makeDirty(buf, record.textureId);
	return LgTrue;
	
_error:
	return LgFalse;
}

void LgAtlasBufferClear(LgAtlasDrawBufferP buf) {
	LgDictClear(buf->buffers);
}

static void createVertexBuffer(void *textureId, LgOutputStreamP buffer, LgAtlasDrawBufferP db) {
	if (!LgDictFind(db->vertexBuffers, textureId)) {
		LgVertexBufferP vb = LgRenderSystemCreateVertexBuffer(LgMemOutputStreamLen(buffer));
		if (vb) {
			void *p = LgVertexBufferLock(vb);
			LgCopyMemory(p, LgMemOutputStreamData(buffer), LgMemOutputStreamLen(buffer));
			LgVertexBufferUnlock(vb);
			LgDictReplace(db->vertexBuffers, textureId, vb);
		}
	}
}

static void drawVertexBuffer(void *textureId, LgVertexBufferP vb, LgAtlasDrawBufferP db) {
	LgTextureP texture = LgTextureAtlasTexture(db->atlas, (int)textureId);
	if (texture) {
		LgRenderSystemSetTexture(texture);
		LgRenderSystemDrawPrimitive(LgPrimitiveTriangleList, LgFvfXYZ | LgFvfTexture, vb, 
			LgVertexBufferSize(vb) / sizeof(struct LgVertexTexCrood) / 3);
	}
}

void LgAtlasBufferDraw(LgAtlasDrawBufferP buf) {
	LgDictForeach(buf->buffers, (LgForeachPair)createVertexBuffer, buf);
	LgRenderSystemSetPixelShader(LgPixelShaderCxformTexture);
	LgDictForeach(buf->vertexBuffers, (LgForeachPair)drawVertexBuffer, buf);
	LgRenderSystemSetTexture(NULL);
}

int LgAtlasBufferNumberOfBatch(LgAtlasDrawBufferP buf) {
	return LgDictCount(buf->vertexBuffers);
}
