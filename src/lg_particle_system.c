﻿#include "lg_particle_system.h"
#include "lg_list.h"
#include "lg_director.h"
#include <math.h>
#include "lg_resourcemanager.h"
#include "lg_platform.h"
#include "lg_util.h"

#define PS_VBSIZE 1024

#define RANDOM_MINUS1_1() ((2.0f*((float)rand()/RAND_MAX))-1.0f)

#define RANDOM_0_1() ((float)rand()/RAND_MAX)

// particle
typedef struct LgParticle {
	float				r, g, b, a;
	float				deltaR, deltaG, deltaB, deltaA;
	struct LgVector2	pos, startPos;
	float				size, deltaSize;
	float				rotation, deltaRotation;
	float				timeToLive;

	struct {
		struct LgVector2	dir;
		float				radialAccel;
		float				tangentialAccel;
	}gravity;

	struct {
		float        angle;
		float        degreesPerSecond;
		float        radius;
		float        deltaRadius;
	}radius;
}*LgParticleP;

// particle system --------------------------------------------------------------------------------------------------------------
typedef struct LgParticleSystemData {
	struct LgParticleSystemConfig	config; // 绮掑瓙绯荤粺閰嶇疆
	LgListP			particles;		// 鎵鏈夌矑瀛愬璞
	LgBool			isActive;
	LgBool			inWorld;
	float			emitCounter;
	float			elapsed;

	// 缁樺浘灞炴
	LgVertexBufferP vertexBuffer;
	LgIndexBufferP	indexBuffer;
	LgTextureP		texture;
}*LgParticleSystemDataP;

#define PD ((LgParticleSystemDataP)LgNodeUserData(node))

static void psDestroy(LgNodeP node) {
	LgParticleSystemDataP pd = PD;
	
	if (pd) {
		if (pd->vertexBuffer) LgVertexBufferDestroy(pd->vertexBuffer);
		if (pd->indexBuffer) LgIndexBufferDestroy(pd->indexBuffer);
		if (pd->texture) LgTextureRelease(pd->texture);
		if (pd->particles) LgListDestroy(pd->particles);
		LgFree(pd);
	}
}

static void psExit(LgNodeP node) {
	LgParticleSystemDataP pd = PD;
	LgListClear(pd->particles);
	if (pd->vertexBuffer) {
		LgVertexBufferDestroy(pd->vertexBuffer);
		pd->vertexBuffer = NULL;
	}
	if (pd->indexBuffer) {
		LgIndexBufferDestroy(pd->indexBuffer);
		pd->indexBuffer = NULL;
	}
}

static LgBool psIsFull(LgNodeP node) {
	return LgListCount(PD->particles) >= PD->config.maxParticles;
}

static float clampf(float value, float min_inclusive, float max_inclusive) {
	if (min_inclusive > max_inclusive) {
		float tmp = min_inclusive;
		min_inclusive = max_inclusive;
		max_inclusive = tmp;
	}
	return value < min_inclusive ? min_inclusive : value < max_inclusive? value : max_inclusive;
}


static void psInitParticle(LgNodeP node, LgParticleSystemDataP pd, LgParticleP p) {
	float startRed, startGreen, startBlue, startAlpha;
	float endRed, endGreen, endBlue, endAlpha;
	float startS, startA, endA, a;

	// timeToLive
	p->timeToLive = pd->config.lifeSpan + pd->config.lifeSpanVariance * RANDOM_MINUS1_1();
	if (p->timeToLive < 0) p->timeToLive = 0;

	// position
	if (pd->inWorld) {
		struct LgVector2 vec;
		vec.x = vec.y = 0;
		LgNodePointToWorld(node, &vec);
		p->startPos = vec;
		p->pos.x = vec.x + pd->config.sourcePositionX + pd->config.sourcePositionXVariance * RANDOM_MINUS1_1();
		p->pos.y = vec.y + pd->config.sourcePositionY + pd->config.sourcePositionYVariance * RANDOM_MINUS1_1();
	} else {
		p->startPos.x = p->startPos.y = 0;
		p->pos.x = pd->config.sourcePositionX + pd->config.sourcePositionXVariance * RANDOM_MINUS1_1();
		p->pos.y = pd->config.sourcePositionY + pd->config.sourcePositionYVariance * RANDOM_MINUS1_1();
	}

	// color
	startRed = clampf(pd->config.startRed + pd->config.startRedVariance * RANDOM_MINUS1_1(), 0, 1);
	startGreen = clampf(pd->config.startGreen + pd->config.startGreenVariance * RANDOM_MINUS1_1(), 0, 1);
	startBlue = clampf(pd->config.startBlue + pd->config.startBlueVariance * RANDOM_MINUS1_1(), 0, 1);
	startAlpha = clampf(pd->config.startAlpha + pd->config.startAlphaVariance * RANDOM_MINUS1_1(), 0, 1);

	endRed = clampf(pd->config.finishRed + pd->config.finishRedVariance * RANDOM_MINUS1_1(), 0, 1);
	endGreen = clampf(pd->config.finishGreen + pd->config.finishGreenVariance * RANDOM_MINUS1_1(), 0, 1);
	endBlue = clampf(pd->config.finishBlue + pd->config.finishBlueVariance * RANDOM_MINUS1_1(), 0, 1);
	endAlpha = clampf(pd->config.finishAlpha + pd->config.finishAlphaVariance * RANDOM_MINUS1_1(), 0, 1);

	p->r = startRed, p->g = startGreen, p->b = startBlue, p->a = startAlpha;
	p->deltaR = (endRed - startRed) / p->timeToLive;
	p->deltaG = (endGreen - startGreen) / p->timeToLive;
	p->deltaB = (endBlue - startBlue) / p->timeToLive;
	p->deltaA = (endAlpha - startAlpha) / p->timeToLive;

	// size
	startS = pd->config.startSize + pd->config.startSizeVariance * RANDOM_MINUS1_1();
	if (startS < 0) startS = 0;
	p->size = startS;
	if (pd->config.finishSize == -1) p->deltaSize = 0;
	else {
		float endS = pd->config.finishSize + pd->config.finishSizeVariance* RANDOM_MINUS1_1();
		if (endS < 0) endS = 0;
		p->deltaSize = (endS - startS) / p->timeToLive;
	}

	// rotate
	startA = pd->config.rotationStart + pd->config.rotationStartVariance * RANDOM_MINUS1_1();
	endA = pd->config.rotationEnd + pd->config.rotationEndVariance * RANDOM_MINUS1_1();
	p->rotation = startA;
	p->deltaRotation = (endA - startA) / p->timeToLive;

	// direction
	a = LgAngleToRadians(pd->config.emitAngle + pd->config.emitAngleVariance * RANDOM_MINUS1_1());

	// gravity
	if (pd->config.emitterType == LGPSGravity) {
		struct LgVector2 v;
		float s;

		v.x = cosf(a), v.y = sinf(a);
		s = pd->config.gravity.speed + pd->config.gravity.speedVariance * RANDOM_MINUS1_1();

		// direction
		p->gravity.dir = v;
		LgVector2MulFloat(&p->gravity.dir, s);

		// radial accel
		p->gravity.radialAccel = pd->config.gravity.radialAccel + pd->config.gravity.radialAccelVariance * RANDOM_MINUS1_1();

		// tangential accel
		p->gravity.tangentialAccel = pd->config.gravity.tangencialAccel + pd->config.gravity.tangencialAccelVariance * RANDOM_MINUS1_1();

		// rotation is dir
		if (pd->config.gravity.rotationIsDir)
			p->rotation = -LgVector2ToAngle(&p->gravity.dir);
	} else if (pd->config.emitterType == LGPSRadius) {
		float startRadius = pd->config.radius.startRadius + pd->config.radius.startRadiusVariance + RANDOM_MINUS1_1();
		float endRadius = pd->config.radius.endRadius + pd->config.radius.endRadiusVariance + RANDOM_MINUS1_1();

		p->radius.radius = startRadius;

		if (pd->config.radius.endRadius == -1) p->radius.deltaRadius = 0;
		else p->radius.deltaRadius = (endRadius - startRadius) / p->timeToLive;

		p->radius.angle = a;
		p->radius.degreesPerSecond = LgAngleToRadians(pd->config.radius.rotatePerSecond + pd->config.radius.rotatePerSecondVariance * RANDOM_MINUS1_1());
	}
}

static LgBool psAddParticle(LgNodeP node) {
	LgParticleP p;
	if (psIsFull(node)) return LgFalse;
	p = (LgParticleP)LgMallocZ(sizeof(struct LgParticle));
	psInitParticle(node, PD, p);
	LgListAppend(PD->particles, p);
	return LgTrue;
}

static void psStop(LgNodeP node) {
	PD->isActive = LgFalse;
}

static void psUpdate(LgNodeP node, float delta) {
	LgListNodeP lnode;
	LgParticleSystemDataP pd = PD;

	// 鐢熸垚鏂扮矑瀛
	if (pd->isActive && pd->config.emissionRate) {
		float rate = 1.0f / pd->config.emissionRate;

		if (LgListCount(pd->particles) < pd->config.maxParticles) pd->emitCounter += delta;
		while(LgListCount(pd->particles) < pd->config.maxParticles && pd->emitCounter > rate) {
			psAddParticle(node);
			pd->emitCounter -= rate;
		}

		pd->elapsed += delta;

		if (pd->config.duration != -1 && pd->config.duration < pd->elapsed) {
			psStop(node);
		}
	}

	lnode = LgListFirstNode(pd->particles);
	while(lnode) {
		LgParticleP p = (LgParticleP)LgListNodeValue(lnode);
		LgListNodeP next = LgListNextNode(lnode);
		
		p->timeToLive -= delta;

		if (p->timeToLive > 0) {
			if (pd->config.emitterType == LGPSGravity) {
				struct LgVector2 tmp, radial, tangential;
				float newy;

				radial.x = radial.y = 0;

				if (p->pos.x || p->pos.y) {
					radial = p->pos;
					LgVector2Normalize(&radial);
				}

				tangential = radial;
				LgVector2MulFloat(&radial, p->gravity.radialAccel);

				newy = tangential.x;
				tangential.x = -tangential.y;
				tangential.y = newy;
				LgVector2MulFloat(&tangential, p->gravity.tangentialAccel);

				// (gravity + radial + tangential) * delta
				tmp.x = radial.x + tangential.x + pd->config.gravity.gravityX;
				tmp.y = radial.y + tangential.y + pd->config.gravity.gravityY;
				LgVector2MulFloat(&tmp, delta);
				LgVector2Add(&p->gravity.dir, &tmp);
				tmp.x = p->gravity.dir.x * delta;
				tmp.y = p->gravity.dir.y * delta;
				LgVector2Add(&p->pos, &tmp);
			} else if (pd->config.emitterType == LGPSRadius) {
				p->radius.angle += p->radius.degreesPerSecond * delta;
				p->radius.radius += p->radius.deltaRadius * delta;
				p->pos.x = p->startPos.x + -cosf(p->radius.angle) * p->radius.radius;
				p->pos.y = p->startPos.y + -sinf(p->radius.angle) * p->radius.radius;
			}

			// color
			p->r += (p->deltaR * delta);
			p->g += (p->deltaG * delta);
			p->b += (p->deltaB * delta);
			p->a += (p->deltaA * delta);

			// size
			p->size += (p->deltaSize* delta);
			if (p->size < 0) p->size = 0;
			
			// angle
			p->rotation += (p->deltaRotation * delta);
		} else {
			// life < 0
			LgListRemove(pd->particles, lnode);
		}
		
		lnode = next;
	}
}

static void psDraw(LgNodeP node) {
	LgParticleSystemDataP pd = PD;
	LgListNodeP lnode;
	struct LgMatrix44 oldTransform; 
	
	if (!pd->vertexBuffer) {
		// 闇瑕佸垱寤簐b
		unsigned short *pidx;
		int i;
		
		pd->vertexBuffer = LgRenderSystemCreateVertexBuffer(PS_VBSIZE * sizeof(struct LgVertexDiffuseTexCrood) * 4);
		pd->indexBuffer = LgRenderSystemCreateIndexBuffer(PS_VBSIZE * 6);
		pidx = (unsigned short *)LgIndexBufferLock(pd->indexBuffer);
		for (i = 0; i < PS_VBSIZE; i++) {
			int baseVertex = i * 4;
			pidx[0] = baseVertex + 0;
			pidx[1] = baseVertex + 1;
			pidx[2] = baseVertex + 2;
			pidx[3] = baseVertex + 1;
			pidx[4] = baseVertex + 3;
			pidx[5] = baseVertex + 2;
			pidx += 6;
		}
		LgIndexBufferUnlock(pd->indexBuffer);
	}
	
	// 寮濮嬬粯鍒
	if (pd->inWorld) {
		struct LgMatrix44 ind; 
		LgRenderSystemGetTransform(LgTransformWorld, &oldTransform);
		LgMatrix44Indentity(&ind);
		LgRenderSystemSetTransform(LgTransformWorld, &ind);
	}

	lnode = LgListFirstNode(pd->particles);
	while(lnode) {
		LgVertexDiffuseTexCroodP pstart = (LgVertexDiffuseTexCroodP)LgVertexBufferLock(pd->vertexBuffer);
		LgVertexDiffuseTexCroodP pend = pstart + PS_VBSIZE * 4, p = pstart;
		int count;
		
		while(p < pend && lnode) {
			LgParticleP particle = (LgParticleP)LgListNodeValue(lnode);
			struct LgVector2 vec2;
			float halfSize = particle->size;
			LgColor color = LgColorARGB_F(particle->a, particle->r, particle->g, particle->b);
			vec2.x = particle->pos.x, vec2.y = particle->pos.y;
			
			if (particle->rotation) {
				float x1 = -halfSize;
				float y1 = -halfSize;

				float x2 = halfSize;
				float y2 = halfSize;
				float x = particle->pos.x;
				float y = particle->pos.y;

				float r = -LgAngleToRadians(particle->rotation);
				float cr = cosf(r);
				float sr = sinf(r);
				float ax = x1 * cr - y1 * sr + x;
				float ay = x1 * sr + y1 * cr + y;
				float bx = x2 * cr - y1 * sr + x;
				float by = x2 * sr + y1 * cr + y;
				float cx = x2 * cr - y2 * sr + x;
				float cy = x2 * sr + y2 * cr + y;
				float dx = x1 * cr - y2 * sr + x;
				float dy = x1 * sr + y2 * cr + y;

				p[0].position.x = ax, p[0].position.y = ay, p[0].position.z = 0;
				p[0].color = color;
				p[0].texcrood.x = 0, p[0].texcrood.y = 1;

				p[1].position.x = dx, p[1].position.y = dy, p[1].position.z = 0;
				p[1].color = color;
				p[1].texcrood.x = 0, p[1].texcrood.y = 0;

				p[2].position.x = bx, p[2].position.y = by, p[2].position.z = 0;
				p[2].color = color;
				p[2].texcrood.x = 1, p[2].texcrood.y = 1;

				p[3].position.x = cx, p[3].position.y = cy, p[3].position.z = 0;
				p[3].color = color;
				p[3].texcrood.x = 1, p[32].texcrood.y = 0;
			} else {
				p[0].position.x = vec2.x - halfSize, p[0].position.y = vec2.y + halfSize, p[0].position.z = 0;
				p[0].color = color;
				p[0].texcrood.x = 0, p[0].texcrood.y = 1;

				p[1].position.x = vec2.x - halfSize, p[1].position.y = vec2.y - halfSize, p[1].position.z = 0;
				p[1].color = color;
				p[1].texcrood.x = 0, p[1].texcrood.y = 0;

				p[2].position.x = vec2.x + halfSize, p[2].position.y = vec2.y + halfSize, p[2].position.z = 0;
				p[2].color = color;
				p[2].texcrood.x = 1, p[2].texcrood.y = 1;

				p[3].position.x = vec2.x + halfSize, p[3].position.y = vec2.y - halfSize, p[3].position.z = 0;
				p[3].color = color;
				p[3].texcrood.x = 1, p[3].texcrood.y = 0;
			}
			
			p += 4;
			lnode = LgListNextNode(lnode);
		}
		
		LgVertexBufferUnlock(pd->vertexBuffer);
		count = (int)(p - pstart) / 4;

		LgRenderSystemSetPixelShader(LgPixelShaderCxformDiffuseAndTexture);
		LgRenderSystemSetTexture(pd->texture);
		LgRenderSystemDrawIndexPrimitive(LgPrimitiveTriangleList, LgFvfXYZ | LgFvfDiffuse | LgFvfTexture,
											pd->vertexBuffer, pd->indexBuffer, count * 2, count * 4);

		LgDirectorUpdateBatch(1);
	}

	if (pd->inWorld) {
		LgRenderSystemSetTransform(LgTransformWorld, &oldTransform);
	}
}

static void psEventProc(LgEventType type, LgNodeP node, void *param) {
	switch(type) {
	case LG_NODE_EVENT_Destroy:
		psDestroy(node);
		break;
	case LG_NODE_EVENT_Exit:
		psExit(node);
		break;
	case LG_NODE_EVENT_Visit:
		psUpdate(node, *((float *)param));
		break;
	case LG_NODE_EVENT_Draw:
		psDraw(node);
		break;
	case LG_PS_EVENT_SetActive:
		PD->isActive = (LgBool)param;
		break;
	case LG_PS_EVENT_SetInWorld:
		PD->inWorld = (LgBool)param;
		break;
	case LG_PS_EVENT_GetConfig:
		LgCopyMemory(param, &PD->config, sizeof(struct LgParticleSystemConfig));
		break;
	}
	
	LgNodeDefEventProc(type, node, param);
}

LgNodeP LgParticleSystemCreateR(const char *filename) {
	LgNodeP node;
	LgParticleSystemDataP pd;
	json_t *json = NULL;
	char textureFilename[LG_PATH_MAX];
	
	node = LgNodeCreateR((LgEvent)psEventProc);
	LG_CHECK_ERROR(node);

	pd = (LgParticleSystemDataP)LgMallocZ(sizeof(struct LgParticleSystemData));
	pd->config.emitterType = LGPSGravity;
	pd->isActive = LgTrue;
	pd->inWorld = LgTrue;

	LgNodeSetUserData(node, pd);
	pd->particles = LgListCreate(LgFree);
	LG_CHECK_ERROR(pd->particles);

	json = json_loadp(filename);
	LG_CHECK_ERROR(json);
	LgParticleSystemConfigFromJson(json, &pd->config);
	json_delete(json);

	LgRelPath(textureFilename, sizeof(textureFilename), filename, pd->config.texture);
	pd->texture = LgResourceManagerLoadTexture(textureFilename);
	LG_CHECK_ERROR(pd->texture);
	LgTextureRetain(pd->texture);

	return node;
	
_error:
	LgNodeRelease(node);
	if (json) json_delete(json);
	return NULL;
}

void LgParticleSystemSetActive(LgNodeP node, LgBool active) {
	LgNodeDoEvent(LG_PS_EVENT_SetActive, node, (void*)active);
}

void LgParticleSystemSetInWorld(LgNodeP node, LgBool inWorld) {
	LgNodeDoEvent(LG_PS_EVENT_SetInWorld, node, (void*)inWorld);
}

LgNodeP LgParticleSystemCreateFromConfigR(const LgParticleSystemConfigP config) {
	LgNodeP node;
	LgParticleSystemDataP pd;

	node = LgNodeCreateR((LgEvent)psEventProc);
	LG_CHECK_ERROR(node);

	pd = (LgParticleSystemDataP)LgMallocZ(sizeof(struct LgParticleSystemData));
	pd->config.emitterType = LGPSGravity;
	pd->isActive = LgTrue;
	pd->inWorld = LgTrue;
	LgCopyMemory(&pd->config, config, sizeof(struct LgParticleSystemConfig));

	LgNodeSetUserData(node, pd);
	pd->particles = LgListCreate(LgFree);
	LG_CHECK_ERROR(pd->particles);

	pd->texture = LgResourceManagerLoadTexture(config->texture);
	LG_CHECK_ERROR(pd->texture);
	LgTextureRetain(pd->texture);

	return node;

_error:
	LgNodeRelease(node);
	return NULL;
}

void LgParticleSystemGetConfig(LgNodeP node, LgParticleSystemConfigP config) {
	LgNodeDoEvent(LG_PS_EVENT_GetConfig, node, (void *)config);
}

#define GET_FLOAT(name) { \
	json_t *value = json_object_get(json, #name); \
	if (value != NULL) { \
	LG_CHECK_ERROR(json_is_number(value)); \
	config->name = (float)json_number_value(value); \
	} \
}

#define GET_FLOAT_2(field, name) { \
	json_t *value = json_object_get(json, #name); \
	if (value != NULL) { \
	LG_CHECK_ERROR(json_is_number(value)); \
	config->field = (float)json_number_value(value); \
	} \
}

#define GET_BOOL_2(field, name) { \
	json_t *value = json_object_get(json, #name); \
	if (value != NULL) { \
	LG_CHECK_ERROR(json_is_boolean(value)); \
	config->field = json_is_true(value); \
	} \
}

#define GET_INT(name) { \
	json_t *value = json_object_get(json, #name); \
	if (value != NULL) { \
	LG_CHECK_ERROR(json_is_integer(value)); \
	config->name = (int)json_integer_value(value); \
	} \
}

LgBool LgParticleSystemConfigFromJson(json_t *json, LgParticleSystemConfigP config) {
	json_t *type, *texture;

	LgZeroMemory(config, sizeof(struct LgParticleSystemConfig));

	// 绮掑瓙灞炴
	GET_FLOAT(emissionRate);
	GET_INT(maxParticles);
	GET_FLOAT(lifeSpan);
	GET_FLOAT(lifeSpanVariance);
	GET_FLOAT(startSize);
	GET_FLOAT(startSizeVariance);
	GET_FLOAT(finishSize);
	GET_FLOAT(finishSizeVariance);
	GET_FLOAT(emitAngle);
	GET_FLOAT(emitAngleVariance);
	GET_FLOAT(rotationStart);
	GET_FLOAT(rotationStartVariance);
	GET_FLOAT(rotationEnd);
	GET_FLOAT(rotationEndVariance);

	// 绮掑瓙棰滆壊
	GET_FLOAT(startRed);
	GET_FLOAT(startGreen);
	GET_FLOAT(startBlue);
	GET_FLOAT(startAlpha);
	GET_FLOAT(startRedVariance);
	GET_FLOAT(startGreenVariance);
	GET_FLOAT(startBlueVariance);
	GET_FLOAT(startAlphaVariance);

	GET_FLOAT(finishRed);
	GET_FLOAT(finishGreen);
	GET_FLOAT(finishBlue);
	GET_FLOAT(finishAlpha);
	GET_FLOAT(finishRedVariance);
	GET_FLOAT(finishGreenVariance);
	GET_FLOAT(finishBlueVariance);
	GET_FLOAT(finishAlphaVariance);

	// 鍙戝皠鍣ㄩ氱敤灞炴
	GET_FLOAT(duration);
	type = json_object_get(json, "type");
	LG_CHECK_ERROR(type && json_is_string(type));
	if (strnocasecmp(json_string_value(type), "gravity") == 0) config->emitterType = LGPSGravity;
	else if (strnocasecmp(json_string_value(type), "radius") == 0) config->emitterType = LGPSRadius;
	GET_FLOAT(sourcePositionX);
	GET_FLOAT(sourcePositionXVariance);
	GET_FLOAT(sourcePositionY);
	GET_FLOAT(sourcePositionYVariance);

	if (config->emitterType == LGPSGravity) {
		GET_FLOAT_2(gravity.speed, speed);
		GET_FLOAT_2(gravity.speedVariance, speedVariance);
		GET_FLOAT_2(gravity.gravityX, gravityX);
		GET_FLOAT_2(gravity.gravityY, gravityY);
		GET_FLOAT_2(gravity.radialAccel, radialAccel);
		GET_FLOAT_2(gravity.radialAccelVariance, radialAccelVariance);
		GET_FLOAT_2(gravity.tangencialAccel, tangencialAccel);
		GET_FLOAT_2(gravity.tangencialAccelVariance, tangencialAccelVariance);
		GET_BOOL_2(gravity.rotationIsDir, rotationIsDir);
	} else if (config->emitterType == LGPSRadius) {
		GET_FLOAT_2(radius.startRadius, startRadius);
		GET_FLOAT_2(radius.startRadiusVariance, startRadiusVariance);
		GET_FLOAT_2(radius.endRadius, endRadius);
		GET_FLOAT_2(radius.endRadiusVariance, endRadiusVariance);
		GET_FLOAT_2(radius.rotatePerSecond, rotatePerSecond);
		GET_FLOAT_2(radius.rotatePerSecondVariance, rotatePerSecondVariance);
	}

	texture = json_object_get(json, "texture");
	LG_CHECK_ERROR(texture && json_is_string(texture));
	strcpy(config->texture, json_string_value(texture));
	return LgTrue;
	
_error:
	return LgFalse;
}

LgBool LgNodeIsParticleSystem(LgNodeP node) {
	return LgNodeEventProc(node) == (LgEvent)psEventProc;
}
