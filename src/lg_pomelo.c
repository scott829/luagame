﻿#include "lg_basetypes.h"
#include "jansson/jansson.h"
#include "event2/event.h"
#include "event2/util.h"
#include "event2/bufferevent.h"
#include "event2/buffer.h"
#include "lg_init.h"
#include "lg_platform.h"
#include "lg_memoutputstream.h"
#include "lg_scheduler.h"
#include "lg_dict.h"
#include "lg_memory.h"
#include "protobuf/pb.h"
#include "lg_log.h"
#include "lg_netclient.h"

#define LG_POMELO_VERSION "1.0.0"
#define LG_POMELO_CLIENT_TYPE "pomelo-luagame-tcp"

enum LgPomeloMessageType {
	LgPomeloMessageRequest,
	LgPomeloMessageNotify,
	LgPomeloMessageResponse,
	LgPomeloMessagePush,
};

enum LgPomeloState {
	LgPomeloStateIdle,
	LgPomeloStateConnecting,
	LgPomeloStateHandshake,
	LgPomeloStateConnected,
};

enum LgPomeloPackageType {
	LgPomeloPackageHandShake = 1,
	LgPomeloPackageAck,
	LgPomeloPackageHeartBeat,
	LgPomeloPackageData,
	LgPomeloPackageKickByServer,
};

#define LG_POMELO_PACKAGE_HEADER_SIZE 4
#define LG_POMELO_PACKAGE_TYPE_SIZE 1
#define LG_POMELO_PACKAGE_LENGTH_SIZE 3

struct LgPomeloClientInfo {
	char	version[32];
	char	type[64];
};

#define LG_POMELO_MSG_COMPRESS_ROUTE_MASK	0x1
#define LG_POMELO_MSG_MSG_TYPE_MASK	0x7

typedef struct LgPomeloRequest {
	char	route[256];
	LgSlot	callback;
}*LgPomeloRequestP;

static evutil_socket_t g_socket = 0;
static struct bufferevent *g_bev = NULL;
static enum LgPomeloState g_state = LgPomeloStateIdle;
static struct LgPomeloClientInfo g_clientInfo;
static LgOutputStreamP g_packageData = NULL;
static LgSlot g_slotConnected = {NULL};
static LgSchedulerItemP g_heartBeatTimer = NULL;
static int g_heartBeatInterval = 3;
static int g_reqId;
static LgDictP g_requests = NULL;
static LgDictP g_routeDictNameToId = NULL;
static LgDictP g_routeDictIdToName = NULL;
static json_t *g_serverProtos = NULL;
static json_t *g_clientProtos = NULL;
static json_t *g_globalProtos = NULL;

static void close();

static void writeByte(unsigned char value) {
	bufferevent_write(g_bev, &value, 1);
}

static void sendPackage(enum LgPomeloPackageType type, const void *data, int size) {
	writeByte((unsigned char)type);
	writeByte((unsigned char)((size >> 16) & 0xff));
	writeByte((unsigned char)((size >> 8) & 0xff));
	writeByte((unsigned char)((size >> 0) & 0xff));
	if (data) bufferevent_write(g_bev, data, size);
}

static int getPackageLength(const void *data) {
	const unsigned char *p = ((const unsigned char *)data) + 1;
	return (p[0] << 16) | (p[1] << 8) | (p[2] << 0);
}

static enum LgPomeloPackageType getPackageType(const void *data) {
	const unsigned char *p = ((const unsigned char *)data);
	return (enum LgPomeloPackageType)p[0];
}

static LgBool heartBeat(LgTime delta, void *userdata) {
	sendPackage(LgPomeloPackageHeartBeat, NULL, 0);
	g_heartBeatTimer = NULL;
	return LgTrue;
}

static void setServerProtos(json_t *protos) {
	if (g_serverProtos) json_delete(g_serverProtos);
	g_serverProtos = json_incref(protos);
}

static void setClientProtos(json_t *protos) {
	if (g_clientProtos) json_delete(g_clientProtos);
	g_clientProtos = json_incref(protos);
}

static void handlePackage(enum LgPomeloPackageType type, int length, const void *data) {
	if (g_state == LgPomeloStateHandshake && type == LgPomeloPackageHandShake) {
		// 服务器握手响应
		json_t *json;

		json = json_loadb((const char *)data, length, 0, NULL);

		if (json != NULL) {
			json_t *code;

			code = json_object_get(json, "code");

			if (json_integer_value(code) == 200) {
				json_t *sys, *heartbeat = NULL, *dict = NULL, 
					*protos, *clientProtos = NULL, *serverProtos = NULL;

				sys = json_object_get(json, "sys");

				if (sys) {
					heartbeat = json_object_get(sys, "heartbeat");
					dict = json_object_get(sys, "dict");
					protos = json_object_get(sys, "protos");
				}

				// 路由字典
				if (dict && json_is_object(dict)) {
					void *iter;

					iter = json_object_iter(dict);
					while(iter) {
						const char *key = json_object_iter_key(iter);
						json_t *value = json_object_iter_value(iter);
						LgDictReplace(g_routeDictNameToId, strdup(key), (void *)json_integer_value(value));
						LgDictReplace(g_routeDictIdToName, (void *)json_integer_value(value), strdup(key));
						iter = json_object_iter_next(dict, iter);
					}
				}
				
				// protos
				if (protos) {
					serverProtos = json_object_get(protos, "server");
					if (serverProtos) setServerProtos(serverProtos);

					clientProtos = json_object_get(protos, "client");
					if (clientProtos) setClientProtos(clientProtos);
				}

				// 握手成功，发送ack
				sendPackage(LgPomeloPackageAck, NULL, 0);
				// 发送心跳
				g_heartBeatInterval = (int)json_integer_value(heartbeat);
				heartBeat(0, NULL);
				// 回调
				LgSlotCall(&g_slotConnected, NULL);
			} else {
				if (json_integer_value(code) == 500) {
					LgSlotCall(LgNetClientSlotError(), (void *)LgNetClientErrorHandshake_Unknown);
				} else if (json_integer_value(code) == 501) {
					LgSlotCall(LgNetClientSlotError(), (void *)LgNetClientErrorHandshake_Version);
				} else {
					LgSlotCall(LgNetClientSlotError(), (void *)LgNetClientErrorHandshake_Unknown);
				}

				close();
			}

			json_delete(json);
		} else {
			LgSlotCall(LgNetClientSlotError(), (void *)LgNetClientErrorBadResponse);
			close();
		}
	} else if (type == LgPomeloPackageHeartBeat) {
		g_heartBeatTimer = LgSchedulerRun(heartBeat, NULL, NULL, (float)g_heartBeatInterval, 1, 0, LgFalse);
	} else if (type == LgPomeloPackageKickByServer) {
		LgSlotCall(LgNetClientSlotError(), (void *)LgNetClientErrorKickByServer);
		close();
	} else if (type == LgPomeloPackageData) {
		// 接受到消息
		const unsigned char *bytes = (const unsigned char *)data;
		unsigned char flag = *bytes++;
		enum LgPomeloMessageType type = (enum LgPomeloMessageType)((flag >> 1) & LG_POMELO_MSG_MSG_TYPE_MASK);
		LgBool compressRoute = flag & LG_POMELO_MSG_COMPRESS_ROUTE_MASK;
		int id = 0;
		char route[256] = {0};
		json_t *json;
		LgPomeloRequestP request = NULL;
		json_t *protos = NULL;
		const char *body;
		int bodySize;

		if (type == LgPomeloMessageRequest || type == LgPomeloMessageResponse) {
			int i = 0, m;
			do {
				m = *bytes++;
				id = id + ((m & 0x7f) * (1 << (7 * i)));
				i++;
			}while(m >= 128);
		}

		if (type == LgPomeloMessageRequest || type == LgPomeloMessageNotify || type == LgPomeloMessagePush) {
			if (compressRoute) {
				int routeId;
				const char *routeName;
				routeId = (bytes[0] & 0xff) << 8;
				routeId |= bytes[1] & 0xff;
				bytes += 2;
				routeName = (const char *)LgDictFind(g_routeDictIdToName, (void *)routeId);
				if (!routeName) return;
				strcpy(route, routeName);
			} else {
				int len = *((unsigned char *)bytes);
				bytes++;
				strncpy(route, (const char*)bytes, len);
				bytes += len;
			}
		} else if (type == LgPomeloMessageResponse) {
			// 从请求列表里面得到route
			request = LgDictFind(g_requests, (void *)id);
			if (request) strcpy(route, request->route);
		}
		
		body = (const char *)bytes;
		bodySize = length - (int)((const char *)bytes - (const char *)data);

		if (g_serverProtos) protos = json_object_get(g_serverProtos, route);
		
		if (protos) {
			// 使用protobuf编码
			json = json_object();
			if (!pc_pb_decode((uint8_t *)body, bodySize, g_globalProtos, protos, json)) {
				LgLogError("failed to decode protobuf for route: %s", route);
				json_delete(json);
				return;
			}
		} else {
			json = json_loadb(body, bodySize, 0, NULL);
		}

		if (type == LgPomeloMessageResponse && request) {
			LgSlotCall(&request->callback, json);
			LgDictRemove(g_requests, (void *)id);
		} else if (type == LgPomeloMessagePush) {
			struct LgNetClientPushMessage msg;
			msg.route = route;
			msg.json = json;
			LgSlotCall(LgNetClientSlotPush(), &msg);
		}

		if (json) json_delete(json);
	}
}

static void socketRead(struct bufferevent *bev, void *arg) {
	char temp[512];
	int n;

	while((n = (int)bufferevent_read(bev, temp, sizeof(temp))) > 0) {
		int p = 0;

		while(p < n) {
			if (LgMemOutputStreamLen(g_packageData) < LG_POMELO_PACKAGE_HEADER_SIZE) {
				// 读包头
				int need = LG_POMELO_PACKAGE_HEADER_SIZE - LgMemOutputStreamLen(g_packageData), rsz;
				rsz = LgMin(need, n - p);
				LgOutputStreamWriteBytes(g_packageData, temp + p, rsz);
				p += rsz;
			} else {
				// 读数据
				int need = (getPackageLength(LgMemOutputStreamData(g_packageData)) + 
					LG_POMELO_PACKAGE_HEADER_SIZE) - LgMemOutputStreamLen(g_packageData), rsz;
				rsz = LgMin(need, n - p);
				LgOutputStreamWriteBytes(g_packageData, temp + p, rsz);
				p += rsz;
			}

			if (LgMemOutputStreamLen(g_packageData) == 
				LG_POMELO_PACKAGE_HEADER_SIZE + getPackageLength(LgMemOutputStreamData(g_packageData))) {
				// 读到完整的包了
				LgOutputStreamWriteByte(g_packageData, 0);
				handlePackage(
					getPackageType(LgMemOutputStreamData(g_packageData)), 
					getPackageLength(LgMemOutputStreamData(g_packageData)), 
					(const char *)LgMemOutputStreamData(g_packageData) + LG_POMELO_PACKAGE_HEADER_SIZE);
				LgMemOutputStreamClear(g_packageData);
			}
		}
	}
}

static void socketWrite(struct bufferevent *bev, void *arg) {
}

static void socketEvent(struct bufferevent *bev, short what, void *arg) {
	if (what == BEV_EVENT_CONNECTED) {
		// 连接成功，发送握手请求
		json_t *json = json_object(), *sys;
		char *data;
		sys = json_object();

		json_object_set_new(json, "sys", sys);
		json_object_set_new(sys, "version", json_string(g_clientInfo.version));
		json_object_set_new(sys, "type", json_string(g_clientInfo.type));

		g_state = LgPomeloStateHandshake;
		data = json_dumps(json, JSON_COMPACT);
		sendPackage(LgPomeloPackageHandShake, data, strlen(data));
		free(data);
		json_delete(json);
	} else if (what & BEV_EVENT_TIMEOUT) {
		// 超时了
		LgSlotCall(LgNetClientSlotError(), (void *)LgNetClientErrorTimeout);
		close();
	} else if (what == BEV_EVENT_EOF) {
		// 服务器主动断开
		LgSlotCall(LgNetClientSlotError(), (void *)LgNetClientErrorKickByServer);
		close();
	} else if (what & (BEV_EVENT_ERROR | BEV_EVENT_READING)) {
		LgSlotCall(LgNetClientSlotError(), (void *)LgNetClientErrorReading);
		close();
	} else if (what & (BEV_EVENT_ERROR | BEV_EVENT_WRITING)) {
		LgSlotCall(LgNetClientSlotError(), (void *)LgNetClientErrorWriting);
		close();
	}
}

static void destroyRequest(LgPomeloRequestP request) {
	LgSlotClear(&request->callback);
	LgFree(request);
}

static LgBool open(LgUrlP urls, LgSlot *slot) {
	const char *version = NULL;
	const char *clientType = NULL;
	LgDictP query = NULL;

	if (g_socket) close();

	query = LgUrlParseQuery(urls->query);
	version = (const char *)LgDictFind(query, "version");
	clientType = (const char *)LgDictFind(query, "clientType");

	// 创建socket连接
	g_socket = socket(AF_INET, SOCK_STREAM, 0);
	LG_CHECK_ERROR(g_socket);
	evutil_make_socket_nonblocking(g_socket);
	g_bev = bufferevent_socket_new(g_evtbase, g_socket, BEV_OPT_CLOSE_ON_FREE);
	bufferevent_setcb(g_bev, socketRead, socketWrite, socketEvent, NULL);
	bufferevent_enable(g_bev, EV_READ | EV_PERSIST);

	strlcpy(g_clientInfo.version, version ? version : LG_POMELO_VERSION, sizeof(g_clientInfo.version));
	strlcpy(g_clientInfo.type, clientType ? clientType : LG_POMELO_CLIENT_TYPE, sizeof(g_clientInfo.type));
	g_packageData = LgMemOutputStreamCreate(NULL, 0);
	g_reqId = 1;

	g_state = LgPomeloStateConnecting;
	LG_CHECK_ERROR(bufferevent_socket_connect_hostname(g_bev, NULL, AF_UNSPEC, urls->host, urls->port) == 0);

	g_requests = LgDictCreate(NULL, NULL, (LgDestroy)destroyRequest);
	g_routeDictNameToId = LgDictCreate((LgCompare)strnocasecmp, LgFree, NULL);
	g_routeDictIdToName = LgDictCreate(NULL, NULL, free);

	if (slot) LgSlotAssign(&g_slotConnected, SLOT_PARAMS_VALUE_REF(*slot));
	else LgSlotClear(&g_slotConnected);

	if (query) LgDictDestroy(query);
	return LgTrue;

_error:
	if (query) LgDictDestroy(query);
	close();
	return LgFalse;
}

static void close() {
	g_state = LgPomeloStateIdle;
	if (g_heartBeatTimer) {
		LgSchedulerRemove(g_heartBeatTimer);
		g_heartBeatTimer = NULL;
	}
	if (g_bev) {
		bufferevent_free(g_bev);
		g_bev = NULL;
	}
	if (g_socket) {
		evutil_closesocket(g_socket);
		g_socket = 0;
	}
	if (g_packageData) {
		LgOutputStreamDestroy(g_packageData);
		g_packageData = NULL;
	}
	if (g_requests) {
		LgDictDestroy(g_requests);
		g_requests = NULL;
	}
	if (g_routeDictNameToId) {
		LgDictDestroy(g_routeDictNameToId);
		g_routeDictNameToId = NULL;
	}
	if (g_routeDictIdToName) {
		LgDictDestroy(g_routeDictIdToName);
		g_routeDictIdToName = NULL;
	}
	if (g_clientProtos) {
		json_delete(g_clientProtos);
		g_clientProtos = NULL;
	}
	if (g_serverProtos) {
		json_delete(g_serverProtos);
		g_serverProtos = NULL;
	}
	if (g_globalProtos) {
		json_delete(g_globalProtos);
		g_globalProtos = NULL;
	}
	LgSlotClear(&g_slotConnected);
}

static int pbStreamCallback(pb_ostream_t *stream, const uint8_t *buf, size_t count) {
	LgOutputStreamP os = (LgOutputStreamP)stream->state;
	LgOutputStreamWriteBytes(os, buf, count);
	return 1;
}

static void makePBOutputStream(pb_ostream_t *pb_stream, LgOutputStreamP stream) {
	pb_stream->callback = pbStreamCallback;
	pb_stream->state = stream;
	pb_stream->max_size = SIZE_MAX;
	pb_stream->bytes_written = 0;
}

static void sendMessage(int id, const char *route, json_t *msg) {
	enum LgPomeloMessageType type = id ? LgPomeloMessageRequest : LgPomeloMessageNotify;
	unsigned char messageHeader = (((unsigned char)type) & LG_POMELO_MSG_MSG_TYPE_MASK) << 1;
	LgOutputStreamP output = LgMemOutputStreamCreate(NULL, 0);
	int routeId = (int)LgDictFind(g_routeDictNameToId, (void*)route);

	if (routeId) messageHeader |= LG_POMELO_MSG_COMPRESS_ROUTE_MASK;

	LgOutputStreamWriteBytes(output, &messageHeader, 1);

	if (id) {
		do {
			unsigned char tmp = id % 128;
			unsigned char next = id / 128;

			if (next != 0) tmp += 128;
			LgOutputStreamWriteBytes(output, &tmp, 1);
			id = next;
		}while(id != 0);
	}

	if (route) {
		if (routeId) {
			LgOutputStreamWriteByte(output, (routeId >> 8) & 0xff);
			LgOutputStreamWriteByte(output, routeId & 0xff);
		} else {
			unsigned char len = (unsigned char)strlen(route);
			LgOutputStreamWriteBytes(output, &len, sizeof(len));
			LgOutputStreamWriteBytes(output, route, len);
		}
	}

	if (msg) {
		json_t *protos = NULL;

		if (g_clientProtos) protos = json_object_get(g_clientProtos, route);

		if (protos) {
			// 使用protobuf编码
			pb_ostream_t outstream;

			makePBOutputStream(&outstream, output);
			if (!pb_encode(&outstream, NULL, protos, msg)) {
				LgLogError("failed to encode protobuf for route: %s", route);
				LgOutputStreamDestroy(output);
				return;
			}
		} else {
			char *jsonstr = json_dumps(msg, JSON_COMPACT);
			LgOutputStreamWriteBytes(output, jsonstr, strlen(jsonstr));
			free(jsonstr);
		}
	}

	sendPackage(LgPomeloPackageData, LgMemOutputStreamData(output), LgMemOutputStreamLen(output));
	LgOutputStreamDestroy(output);
}

static void request(const char *route, json_t *msg, LgSlot *slot) {
	if (g_bev) {
		LgPomeloRequestP request = (LgPomeloRequestP)LgMallocZ(sizeof(struct LgPomeloRequest));
		strcpy(request->route, route);
		if (slot) LgSlotAssign(&request->callback, SLOT_PARAMS_VALUE_REF(*slot));
		LgDictInsert(g_requests, (void *)g_reqId, request);
		sendMessage(g_reqId, route, msg);
		g_reqId++;
	}
}

static void notify(const char *route, json_t *msg) {
	sendMessage(0, route, msg);
}

static LgBool isOpened() {
	return g_socket != 0;
}

struct LgNetClientDriver pomeloDriver = {
	"pomelo",
	open,
	close,
	request,
	notify,
	isOpened,
};