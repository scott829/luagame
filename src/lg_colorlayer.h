#ifndef _LG_COLORLAYER_H
#define _LG_COLORLAYER_H

#include "lg_layer.h"

enum {
	LG_COLORLAYER_EVENT_SetColor = LG_LAYER_EVENT_LAST,
	LG_COLORLAYER_EVENT_Color,
	LG_COLORLAYER_EVENT_LAST,
};

LgBool LgNodeIsColorLayer(LgNodeP node);

LgNodeP LgColorLayerCreateR(LgColor color);

void LgColorLayerSetColor(LgNodeP node, LgColor color);

LgColor LgColorLayerColor(LgNodeP node);

#endif