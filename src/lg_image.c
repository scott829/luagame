#include "lg_image.h"
#include "lg_memory.h"
#include "lg_archive.h"
#include "lg_imgloader.h"
#include "lg_jpgloader.h"
#include "lg_pngloader.h"

struct LgImage {
	LG_REFCNT_HEAD
	struct LgSize		size;
	int					pitch;
	enum LgImageFormat_	format;
	void *				data;
};

LG_REFCNT_FUNCTIONS(LgImage)

static struct LgImageLoader g_imgloaders[] = {
	{"jpg", jpgCheckType, jpgLoad},
	{"png", pngCheckType, pngLoad},
	{NULL}
};

static void destroy(LgImageP image) {
	if (image->data) LgFree(image->data);
	LgFree(image);
}

LgImageP LgImageCreateR(const LgSizeP size, enum LgImageFormat_ format) {
	LgImageP img;
	int linesize = LgImagePixelSize(format) * size->width;

	img = (LgImageP)LgMallocZ(sizeof(struct LgImage));
	LG_CHECK_ERROR(img);
	LG_REFCNT_INIT(img, destroy);

	img->pitch = linesize % 4 == 0 ? linesize : (linesize / 4 + 1) * 4;
	LgCopyMemory(&img->size, size, sizeof(struct LgSize));
	img->format = format;
	img->data = LgMallocZ(img->pitch * size->height);
	LG_CHECK_ERROR(img->data);
	return img;

_error:
	if (img) LgImageRelease(img);
	return NULL;
}

int LgImagePixelSize(enum LgImageFormat_ format) {
	switch(format) {
	case LgImageFormatL8: return 1;
	case LgImageFormatA8L8: return 2;
	case LgImageFormatA8R8G8B8: return 4;
	default: return 0;
	}
}

void *LgImageData(LgImageP image) {
	return image->data;
}

void LgImageSize(LgImageP image, LgSizeP size) {
	LgCopyMemory(size, &image->size, sizeof(struct LgSize));
}

enum LgImageFormat_ LgImageFormat(LgImageP image) {
	return image->format;
}

int LgImageLineSize(LgImageP image) {
	return image->pitch;
}

void *LgImageLineData(LgImageP image, int line) {
	return (char *)image->data + line * image->pitch;
}

LgImageP LgImageCreateFromFileR(const char *filename) {
	LgInputStreamP input = NULL;
	LgImageP img = NULL;
	
	input = LgArchiveOpenFile(filename);
	LG_CHECK_ERROR(input);
	img = LgImageCreateFromStreamR(input);
	LG_CHECK_ERROR(img);
	LgInputStreamDestroy(input);
	return img;

_error:
	if (input) LgInputStreamDestroy(input);
	return NULL;
}

LgImageP LgImageCreateFromStreamR(LgInputStreamP input) {
	unsigned char head[20];
	int i, headsize;

	LgInputStreamMark(input);
	headsize = LgInputStreamReadBytes(input, head, sizeof(head));
	LgInputStreamReset(input);
	
	for (i = 0; g_imgloaders[i].name; i++) {
		if (g_imgloaders[i].checkType(head, headsize)) {
			return g_imgloaders[i].load(input);
		}
	}

	return NULL;
}
