#ifndef _LG_PAYMENT_H_
#define _LG_PAYMENT_H_

#include "lg_basetypes.h"
#include "jansson/jansson.h"

enum LgPaymentEventType {
	LgPaymentEventPurchased,	// 交易完成
	LgPaymentEventFailed,		// 交易失败
	LgPaymentEventNotFound,		// 没有找到指定商品
};

typedef struct LgPaymentEvent {
	enum LgPaymentEventType	type;
	const char *			productId;
	json_t *				receipt;
}*LgPaymentEventP;

LgBool LgPaymentOpen();

void LgPaymentClose();

LgBool LgPaymentCanMakePayments();

LgBool LgPaymentPurchase(const char *productId);

LgSlot *LgPaymentSlot();

#endif