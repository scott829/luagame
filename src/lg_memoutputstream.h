#ifndef _LG_MEMOUTPUTSTREAM_H
#define _LG_MEMOUTPUTSTREAM_H

#include "lg_outputstream.h"

LgOutputStreamP LgMemOutputStreamCreate(void *data, int size);

void *LgMemOutputStreamData(LgOutputStreamP stream);

void *LgMemOutputStreamDeatchData(LgOutputStreamP stream, int *size);

int LgMemOutputStreamLen(LgOutputStreamP stream);

void LgMemOutputStreamClear(LgOutputStreamP stream);

#endif