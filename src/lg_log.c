﻿#include "lg_log.h"
#include "lg_platform.h"
#include "lg_memory.h"
#include <stdio.h>
#include <stdarg.h>
#include "lg_init.h"
#include "lg_list.h"
#include <time.h>
#include "jansson/jansson.h"
#ifdef ANDROID
#	include <android/log.h>
#endif

typedef struct LgLogAppender {
	LgDestroy destroy;
	LgBool (* append)(enum LgLogType type, struct LgLogAppender *appender, const char *fmt, ...);
}*LgLogAppenderP;

static void destroyAppender(LgLogAppenderP appender) {
	if (appender->destroy) appender->destroy(appender);
	LgFree(appender);
}

// file appender ------------------------------------------------------------------------------------------------
struct LgLogFileAppender {
	struct LgLogAppender base;
	FILE *fp;
};

static LgBool fileAppenderAppend(enum LgLogType type,LgLogAppenderP appender, const char *fmt, ...) {
	struct LgLogFileAppender *fileAppender = (struct LgLogFileAppender *)appender;
	va_list argp;

	va_start(argp, fmt);
	vfprintf(fileAppender->fp, fmt, argp);
	fflush(fileAppender->fp);
	va_end(argp);
	return LgTrue;
}

static void fileAppenderDestroy(LgLogAppenderP appender) {
	struct LgLogFileAppender *fileAppender = (struct LgLogFileAppender *)appender;
	if (fileAppender->fp) fclose(fileAppender->fp);
}

static LgLogAppenderP createFileAppender(const char *filename) {
	struct LgLogFileAppender *appender;

	appender = (struct LgLogFileAppender *)LgMallocZ(sizeof(struct LgLogFileAppender));
	appender->base.append = fileAppenderAppend;
	appender->base.destroy = (LgDestroy)fileAppenderDestroy;
	appender->fp = fopen(filename, "w+");
	LG_CHECK_ERROR(appender->fp);
	return (LgLogAppenderP)appender;

_error:
	destroyAppender((LgLogAppenderP)appender);
	return NULL;
}

// stdout appender ------------------------------------------------------------------------------------------------
struct LgLogStdoutAppender {
	struct LgLogAppender base;
};

#ifdef WIN32
#include <windows.h>
#endif

static LgBool stdoutAppenderAppend(enum LgLogType type, LgLogAppenderP appender, const char *fmt, ...) {
	FILE *fp = type == LgLogERR ? stderr : stdout;
	va_list argp;
	va_start(argp, fmt);

#ifdef WIN32
	{
		static char info[1024];
		vsprintf(info, fmt, argp);
		OutputDebugString(info);
	}
#endif
	
	vfprintf(fp, fmt, argp);
	fflush(fp);
	va_end(argp);

	return LgTrue;
}

static LgLogAppenderP createStdoutAppender() {
	struct LgLogStdoutAppender *appender;

	appender = (struct LgLogStdoutAppender *)LgMallocZ(sizeof(struct LgLogStdoutAppender));
	appender->base.append = stdoutAppenderAppend;
	return (LgLogAppenderP)appender;
}

// system appender ------------------------------------------------------------------------------------------------
struct LgLogSystemAppender {
	struct LgLogAppender base;
};

#ifdef ANDROID
static LgBool systemAppenderAppend(enum LgLogType type, LgLogAppenderP appender, const char *fmt, ...) {
	va_list argp;
	int prio;

	va_start(argp, fmt);
	switch(type) {
	case LgLogINFO: prio = ANDROID_LOG_INFO;
	case LgLogWARN: prio = ANDROID_LOG_WARN;
	case LgLogERR: prio = ANDROID_LOG_ERROR;
	}
	__android_log_vprint(prio, "native c", fmt, argp);
	va_end(argp);
	return LgTrue;
}
#else
static LgBool systemAppenderAppend(enum LgLogType type, LgLogAppenderP appender, const char *fmt, ...) {
	return LgTrue;
}
#endif

static LgLogAppenderP createSystemAppender() {
	struct LgLogSystemAppender *appender;

	appender = (struct LgLogSystemAppender *)LgMallocZ(sizeof(struct LgLogSystemAppender));
	appender->base.append = systemAppenderAppend;
	return (LgLogAppenderP)appender;
}

// log system ------------------------------------------------------------------------------------------
static LgListP g_appenders = NULL;

static LgLogAppenderP createAppender(json_t *setting) {
	json_t *type = json_object_get(setting, "type");

	if (type && json_is_string(type)) {
		if (strcmp(json_string_value(type), "console") == 0) {
			return createStdoutAppender();
		} else if (strcmp(json_string_value(type), "file") == 0) {
			json_t *filename = json_object_get(setting, "filename");
			if (filename && json_is_string(filename)) {
				return createFileAppender(json_string_value(filename));
			}
		}
	}

	return NULL;
}

LgBool LgLogSystemOpen() {
	json_t *config = json_load_file("log.conf", 0, NULL);

	g_appenders = LgListCreate((LgDestroy)destroyAppender);

	if (config) {
		// 有配置文件
		json_t *appenders = json_object_get(config, "appenders");
		if (appenders && json_is_array(appenders)) {
			int i;
			
			for (i = 0; i < (int)json_array_size(appenders); i++) {
				json_t *appenderSetting = json_array_get(appenders, i);
				LgLogAppenderP appender = createAppender(appenderSetting);
				if (appender) LgListAppend(g_appenders, appender);
			}
		}

		json_delete(config);
	} else {
		LgListAppend(g_appenders, createStdoutAppender());
		LgListAppend(g_appenders, createSystemAppender());
	}
	
	return LgTrue;
}

void LgLogSystemClose() {
	if (g_appenders) {
		LgListDestroy(g_appenders);
		g_appenders = NULL;
	}
}

static const char *logTypeString(enum LgLogType type) {
	switch(type) {
	case LgLogINFO: return "INFO";
	case LgLogWARN: return "WARN";
	case LgLogERR: return "ERR";
	default: return "UNKNOWN";
	};
}

void LgLogSystemLog(enum LgLogType type, const char *filename, int line, const char *fmt, ...) {
	if (g_appenders && LgListCount(g_appenders) > 0) {
		LgListNodeP node = LgListFirstNode(g_appenders);
		static char info[32768];
		va_list argp;

		va_start(argp, fmt);
		vsprintf(info, fmt, argp);
		va_end(argp);

		while(node) {
			LgLogAppenderP appender = (LgLogAppenderP)LgListNodeValue(node);
			appender->append(type, appender, "[%s] %s (%s:%d)\n",
				logTypeString(type), info, filename, line);
			node = LgListNextNode(node);
		}
	}
}
