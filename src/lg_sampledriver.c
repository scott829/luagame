#include "lg_sampledriver.h"
#include "lg_archive.h"
#include "lg_sample_mp3.h"

LG_REFCNT_FUNCTIONS(LgSampleProvider)

static LgSampleDriverP g_sampleProviders[] = {
	&mp3SampleDriver,
	NULL,
};

LgSampleProviderP LgSampleProviderCreateR(const char *filename) {
	int i;
	LgInputStreamP input;

	input = LgArchiveOpenFile(filename);
	if (!input) return NULL;
	LgInputStreamMark(input);
	for (i = 0; g_sampleProviders[i]; i++) {
		if (!g_sampleProviders[i]->checkFormat(input)) {
			LgInputStreamReset(input);
		} else {
			LgInputStreamReset(input);
			return g_sampleProviders[i]->createSampleProviderR(input);
		}
	}

	LgInputStreamDestroy(input);
	return NULL;
}