#ifndef _LG_TILEMAPLAYER_H
#define _LG_TILEMAPLAYER_H

#include "lg_layer.h"
#include "jansson/jansson.h"

enum {
	LG_TILEMAPLAYER_EVENT_LAST = LG_LAYER_EVENT_LAST,
};

enum LgMapObjectType {
	LgMapObjectRectangle,
	LgMapObjectEllipse,
	LgMapObjectPolygon,
	LgMapObjectPolyline,
	LgMapObjectTile,
};

typedef struct LgMapObject {
	enum LgMapObjectType	objectType;
	char *					name;
	char *					type;
	LgBool					visible;
	json_t *				properties;
	float					x, y;

	union {
		struct {
			float width, height;
		}rectangleOrEllipse;

		struct {
			LgVector2P	points;
			int			count;
		}polygonOrPolyline;

		struct {
			int		gid;
		}tile;
	};
}*LgMapObjectP;

LgBool LgNodeIsTileMapLayer(LgNodeP node);

LgNodeP LgTileMapLayerCreateR(const char *filename);

void LgTileMapLayerSize(LgNodeP node, LgVector2P size);

void LgTileMapLayerGridSize(LgNodeP node, LgSizeP size);

void LgTileMapLayerSetScroll(LgNodeP node, const LgVector2P scroll);

void LgTileMapLayerScroll(LgNodeP node, LgVector2P scroll);

void LgTileMapLayerCenterScroll(LgNodeP node, LgVector2P center);

void LgTileMapLayerCenterTo(LgNodeP node, const LgVector2P center);

void LgTileMapLayerLocalToGrid(LgNodeP node, LgVector2P pt);

void LgTileMapLayerGlobalToGrid(LgNodeP node, LgVector2P pt);

void LgTileMapLayerGridToLocal(LgNodeP node, LgVector2P pt);

void LgTileMapLayerGridToGlobal(LgNodeP node, LgVector2P pt);

void LgTileMapLayerShowLayer(LgNodeP node, const char *name);

void LgTileMapLayerHideLayer(LgNodeP node, const char *name);

json_t *LgTileMapLayerProperties(LgNodeP node);

json_t *LgTileMapLayerLayerProperties(LgNodeP node, const char *layerName);

json_t *LgTileMapLayerTileProperties(LgNodeP node, const char *layerName, const LgPointP gridPt);

int LgTileMapLayerFindPath(LgNodeP node, const LgPointP start, const LgPointP end, LgBool bias, LgPointP *path);

int LgTileMapLayerTileIdByAlias(LgNodeP node, const char *alias);

int LgTileMapLayerTileId(LgNodeP node, const char *layerName, int col, int row);

void LgTileMapLayerSetTileId(LgNodeP node, const char *layerName, int col, int row, int id);

void LgTileMapLayerSetChildAt(LgNodeP node, const char *layerName, int row, LgNodeP child);

int LgTileMapLayerLayerCount(LgNodeP node);

json_t *LgTileMapLayerTilePropertiesByLayerIndex(LgNodeP node, int layerIndex, const LgPointP gridPt);

LgMapObjectP LgTileMapLayerFindObject(LgNodeP node, const char *layerName, const char *name);

#endif