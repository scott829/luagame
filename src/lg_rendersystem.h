﻿#ifndef _LG_RENDERSYSTEM_H
#define _LG_RENDERSYSTEM_H

#include "lg_basetypes.h"
#include "lg_window.h"
#include "lg_matrix44.h"
#include "lg_cxform.h"
#include "lg_vector2.h"
#include "lg_vector3.h"
#include "lg_refcnt.h"
#include "lg_image.h"

enum LgTransformType {
	LgTransformWorld,
	LgTransformProjection,
	LgTransformView,
};

enum LgPrimitiveType {
	LgPrimitiveTriangleList,
	LgPrimitiveTriangleStrip,
	LgPrimitiveTriangleFan,
};

enum LgFVF {
	LgFvfXYZ		= 0x1,
	LgFvfDiffuse	= 0x2,
	LgFvfTexture	= 0x4,
};

enum LgClearFlag {
	LgClearFlagColor	= 0x1,
	LgClearFlagDepth	= 0x2,
	LgClearFlagStencil	= 0x4,
};

enum LgStencilCompare {
	LgStencilCompareNever,
	LgStencilCompareLess,
	LgStencilCompareEqual,
	LgStencilCompareLessEqual,
	LgStencilCompareGreater,
	LgStencilCompareNotEqual,
	LgStencilCompareGreaterEqual,
	LgStencilCompareAlways,
};

enum LgStencilOp {
	LgStencilOpKeep,
	LgStencilOpZero,
	LgStencilOpReplace,
	LgStencilOpIncrsat,
	LgStencilOpDecrsat,
	LgStencilOpInvert,
	LgStencilOpIncr,
	LgStencilOpDecr,
};

enum LgPixelShaderType {
	LgPixelShaderNone,
	LgPixelShaderCxformDiffuse,
	LgPixelShaderCxformTexture,
	LgPixelShaderCxformDiffuseAndTexture,
	LgPixelShaderCopyRenderTarget,
};

// 常用节点缓存结构
typedef struct LgVertexDiffuse {
	struct LgVector3 position;
	LgColor color;
}*LgVertexDiffuseP;

typedef struct LgVertexTexCrood {
	struct LgVector3 position;
	struct LgVector2 texcrood;
}*LgVertexTexCroodP;

typedef struct LgVertexDiffuseTexCrood {
	struct LgVector3 position;
	LgColor color;
	struct LgVector2 texcrood;
}*LgVertexDiffuseTexCroodP;

typedef void *LgVertexBufferP;

typedef void *LgIndexBufferP;

typedef void *LgTextureP;

LG_REFCNT_FUNCTIONS_DECL(LgTexture)

typedef void *LgRenderTargetP;

typedef struct LgRenderSystemDriver {
	const char *name;
	// base ---------------------------------------------------------------------
	LgBool (* open)();
	void (* close)();
	void (* setTransform)(enum LgTransformType type, const LgMatrix44P m);
	void (* getTransform)(enum LgTransformType type, LgMatrix44P m);
	void (* setCxform)(const LgCxformP cxform);
	void (* getCxform)(LgCxformP cxform);
	LgVertexBufferP (* createVertexBuffer)(int size);
	LgIndexBufferP (* createIndexBuffer)(int count);
	LgTextureP (* createTextureR)(enum LgImageFormat_ format, const LgSizeP size);
	void (* drawPrimitive)(enum LgPrimitiveType type, int fvf, LgVertexBufferP vb, int primitiveCount);
	void (* drawIndexPrimitive)(enum LgPrimitiveType type, int fvf, LgVertexBufferP vb, LgIndexBufferP ib, int primitiveCount, int vertexCount);
	void (* begin)(LgRenderTargetP target);
	void (* end)();
	void (* clear)(enum LgClearFlag flags, LgColor color, int stencil);
	void (* setTexture)(LgTextureP texture);
	void (* enableStencil)(LgBool value);
	void (* applyStencil)(int reference, enum LgStencilCompare cmp, enum LgStencilOp passOp, enum LgStencilOp failOp);
	LgRenderTargetP (* createRenderTarget)(const LgSizeP size);
	void (* setPixelShader)(enum LgPixelShaderType shaderType);

	// vertex buffer ---------------------------------------------------------------------
	void (* vertexBufferDestroy)(LgVertexBufferP vb);
	void *(* vertexBufferLock)(LgVertexBufferP vb);
	void (* vertexBufferUnlock)(LgVertexBufferP vb);
	int (* vertexBufferSize)(LgVertexBufferP vb);

	// index buffer ---------------------------------------------------------------------
	void (* indexBufferDestroy)(LgIndexBufferP ib);
	void *(* indexBufferLock)(LgIndexBufferP ib);
	void (* indexBufferUnlock)(LgIndexBufferP ib);
	int (* indexBufferCount)(LgIndexBufferP ib);

	// texture buffer ---------------------------------------------------------------------
	void (* textureSize)(LgTextureP texture, LgSizeP size);
	enum LgImageFormat_ (* textureFormat)(LgTextureP texture);
	LgBool (* textureUpdate)(LgTextureP texture, int destX, int destY, int srcW, int srcH, LgImageP image);

	// render target ---------------------------------------------------------------------
	void (* renderTargetDestroy)(LgRenderTargetP target);
	void (* renderTargetSize)(LgRenderTargetP target, LgSizeP size);
	LgTextureP (* renderTargetTexture)(LgRenderTargetP target);
}*LgRenderSystemDriverP;

extern LgRenderSystemDriverP g_currentRenderSystemDriver;

void LgRenderSystemSetDriver(const char *name);

#define LgRenderSystemName() g_currentRenderSystemDriver->name

#define LgRenderSystemOpen() g_currentRenderSystemDriver->open()

#define LgRenderSystemClose() if (g_currentRenderSystemDriver) g_currentRenderSystemDriver->close()

#define LgRenderSystemSetTransform(type, m) g_currentRenderSystemDriver->setTransform(type, m)

#define LgRenderSystemGetTransform(type, m) g_currentRenderSystemDriver->getTransform(type, m)

#define LgRenderSystemSetCxform(cxform) g_currentRenderSystemDriver->setCxform(cxform)

#define LgRenderSystemGetCxform(cxform) g_currentRenderSystemDriver->getCxform(cxform)

#define LgRenderSystemCreateVertexBuffer(size) g_currentRenderSystemDriver->createVertexBuffer(size)

#define LgRenderSystemCreateIndexBuffer(count) g_currentRenderSystemDriver->createIndexBuffer(count)

#define LgRenderSystemCreateTextureR(format, size) \
	g_currentRenderSystemDriver->createTextureR(format, size)

#define LgRenderSystemDrawPrimitive(type, fvf, vb, primitiveCount) \
	g_currentRenderSystemDriver->drawPrimitive(type, fvf, vb, primitiveCount)

#define LgRenderSystemDrawIndexPrimitive(type, fvf, vb, ib, primitiveCount, vertexCount) \
	g_currentRenderSystemDriver->drawIndexPrimitive(type, fvf, vb, ib, primitiveCount, vertexCount)

#define LgRenderSystemBegin(target) g_currentRenderSystemDriver->begin(target)

#define LgRenderSystemEnd() g_currentRenderSystemDriver->end()

#define LgRenderSystemClear(flags, color, stencil) g_currentRenderSystemDriver->clear(flags, color, stencil)

#define LgRenderSystemSetTexture(texture) g_currentRenderSystemDriver->setTexture(texture)

#define LgRenderSystemEnableStencil(value) g_currentRenderSystemDriver->enableStencil(value)

#define LgRenderSystemApplyStencil(reference, cmp, passOp, failOp) \
	g_currentRenderSystemDriver->applyStencil(reference, cmp, passOp, failOp)

#define LgRenderSystemCreateRenderTarget(size) \
	g_currentRenderSystemDriver->createRenderTarget(size)

#define LgRenderSystemSetPixelShader(shaderType) \
	g_currentRenderSystemDriver->setPixelShader(shaderType)

// 节点缓存
#define LgVertexBufferDestroy(vb) g_currentRenderSystemDriver->vertexBufferDestroy(vb)

#define LgVertexBufferLock(vb) g_currentRenderSystemDriver->vertexBufferLock(vb)

#define LgVertexBufferUnlock(vb) g_currentRenderSystemDriver->vertexBufferUnlock(vb)

#define LgVertexBufferSize(vb) g_currentRenderSystemDriver->vertexBufferSize(vb)

// 索引缓存
#define LgIndexBufferDestroy(ib) g_currentRenderSystemDriver->indexBufferDestroy(ib)

#define LgIndexBufferLock(ib) g_currentRenderSystemDriver->indexBufferLock(ib)

#define LgIndexBufferUnlock(ib) g_currentRenderSystemDriver->indexBufferUnlock(ib)

#define LgIndexBufferCount(ib) g_currentRenderSystemDriver->indexBufferCount(ib)

// 纹理
#define LgTextureSize(texture, size) g_currentRenderSystemDriver->textureSize(texture, size)

#define LgTextureFormat(texture) g_currentRenderSystemDriver->textureFormat(texture)

#define LgTextureUpdate(texture, destX, destY, srcW, srcH, image) \
	g_currentRenderSystemDriver->textureUpdate(texture, destX, destY, srcW, srcH, image)

// 渲染目标
#define LgRenderTargetDestroy(target) g_currentRenderSystemDriver->renderTargetDestroy(target)

#define LgRenderTargetSize(target, size) g_currentRenderSystemDriver->renderTargetSize(target, size)

#define LgRenderTargetTexture(target) g_currentRenderSystemDriver->renderTargetTexture(target)

LgBool LsRenderSystemIsVSync();

LgTextureP LgTextureCreateFromFileR(const char *filename);

#endif
