#include "lg_animation.h"
#include "lg_resourcemanager.h"
#include "lg_fileinputstream.h"
#include "lg_archive.h"
#include "lg_shapes.h"
#include "lg_director.h"
#include "jansson/jansson.h"
#include "lg_util.h"
#include "lg_platform.h"
#include <float.h>

// animation

LG_REFCNT_FUNCTIONS(LgAnimation)

static void destroyAnimation(LgAnimationP animation) {
	if (animation->atlas) spAtlas_dispose(animation->atlas);
	if (animation->skeletonData) spSkeletonData_dispose(animation->skeletonData);
	if (animation->skeletonJson) spSkeletonJson_dispose(animation->skeletonJson);
	LgFree(animation);
}

LgAnimationP LgAnimationCreateR(const char *filename) {
	json_t *json, *atlas, *skeleton;
	LgAnimationP animation = NULL;
	char path[LG_PATH_MAX];

	json = json_loadp(filename);
	LG_CHECK_ERROR(json);
	atlas = json_object_get(json, "atlas");
	skeleton = json_object_get(json, "skeleton");
	LG_CHECK_ERROR(
		atlas && json_is_string(atlas) &&
		skeleton && json_is_string(skeleton));

	animation = (LgAnimationP)LgMallocZ(sizeof(struct LgAnimation));
	LG_CHECK_ERROR(animation);
	LG_REFCNT_INIT(animation, destroyAnimation);

	LgRelPath(path, sizeof(path), filename, json_string_value(atlas));
	animation->atlas = spAtlas_readAtlasFile(path);
	LG_CHECK_ERROR(animation->atlas);

	animation->skeletonJson = spSkeletonJson_create(animation->atlas);
	LG_CHECK_ERROR(animation->skeletonJson);

	LgRelPath(path, sizeof(path), filename, json_string_value(skeleton));
	animation->skeletonData = spSkeletonJson_readSkeletonDataFile(animation->skeletonJson, path);
	LG_CHECK_ERROR(animation->skeletonData);

	json_delete(json);
	return animation;

_error:
	if (animation) LgAnimationRelease(animation);
	if (json) json_delete(json);
	return NULL;
}

// animation node

typedef struct LgAnimationData {
	LgAnimationP 		animation;
	spSkeleton *		skeleton;
	spAnimationState *	animationState;
	LgBool				exactTouch;
	LgSlot				slotEvent;
}*LgAnimationDataP;

static void destroy(LgNodeP node) {
	LgAnimationDataP ad = (LgAnimationDataP)LgNodeUserData(node);
	if (ad->animation) LgAnimationRelease(ad->animation);
	if (ad->skeleton) spSkeleton_dispose(ad->skeleton);
	if (ad->animationState) {
		spAnimationStateData_dispose(ad->animationState->data);
		spAnimationState_dispose(ad->animationState);
	}
	LgFree(ad);
}

static LgTextureP getTexture(spRegionAttachment* regionAttachment) {
	return (LgTextureP)((spAtlasRegion*)regionAttachment->rendererObject)->page->rendererObject;
}

static void appendQuad(spRegionAttachment *attachment, spSlot *slot, LgVertexDiffuseTexCroodP vertexBuffer) {
	struct LgVertexDiffuseTexCrood lt, rt, lb, rb;
	float vertices[8];
	float r = slot->skeleton->r * slot->r;
	float g = slot->skeleton->g * slot->g;
	float b = slot->skeleton->b * slot->b;
	float a = slot->skeleton->a * slot->a;

	r *= a;
	g *= a;
	b *= a;
	
	spRegionAttachment_computeWorldVertices(attachment, slot->skeleton->x, slot->skeleton->y, slot->bone, vertices);

	lt.color = LgColorRGBA_F(r, g, b, a);
	rt.color = LgColorRGBA_F(r, g, b, a);
	lb.color = LgColorRGBA_F(r, g, b, a);
	rb.color = LgColorRGBA_F(r, g, b, a);

	lt.position.x = vertices[VERTEX_X2], lt.position.y = -vertices[VERTEX_Y2], lt.position.z = 0;
	lt.texcrood.x = attachment->uvs[VERTEX_X2], lt.texcrood.y = attachment->uvs[VERTEX_Y2];

	rt.position.x = vertices[VERTEX_X3], rt.position.y = -vertices[VERTEX_Y3], rt.position.z = 0;
	rt.texcrood.x = attachment->uvs[VERTEX_X3], rt.texcrood.y = attachment->uvs[VERTEX_Y3];

	lb.position.x = vertices[VERTEX_X1], lb.position.y = -vertices[VERTEX_Y1], lb.position.z = 0;
	lb.texcrood.x = attachment->uvs[VERTEX_X1], lb.texcrood.y = attachment->uvs[VERTEX_Y1];

	rb.position.x = vertices[VERTEX_X4], rb.position.y = -vertices[VERTEX_Y4], rb.position.z = 0;
	rb.texcrood.x = attachment->uvs[VERTEX_X4], rb.texcrood.y = attachment->uvs[VERTEX_Y4];

	*vertexBuffer++ = lb;
	*vertexBuffer++ = lt;
	*vertexBuffer++ = rb;

	*vertexBuffer++ = rb;
	*vertexBuffer++ = rt;
	*vertexBuffer++ = lt;
}

static void draw(LgNodeP node) {
	LgAnimationDataP ad = (LgAnimationDataP)LgNodeUserData(node);
	int i, n;
	LgTextureP texture = NULL;
	LgVertexBufferP vb = LgShapesDiffuseTexCroodTriangleList();
	int maxQuadCount = LgShapesDiffuseTexCroodTriangleListCount() / 2, quadCount = 0;
	LgVertexDiffuseTexCroodP vp = (LgVertexDiffuseTexCroodP)LgVertexBufferLock(vb);

	LgRenderSystemSetPixelShader(LgPixelShaderCxformDiffuseAndTexture);

	for (i = 0, n = ad->skeleton->slotCount; i < n; i++) {
		spSlot* slot = ad->skeleton->drawOrder[i];
		spRegionAttachment* attachment;
		LgTextureP regionTexture;
		
		if (!slot->attachment || slot->attachment->type != ATTACHMENT_REGION) continue;
		attachment = (spRegionAttachment*)slot->attachment;
		regionTexture = getTexture(attachment);

		if ((regionTexture != texture && quadCount > 0) ||
			quadCount == maxQuadCount - 1) {
			LgVertexBufferUnlock(vb);
			LgRenderSystemSetTexture(texture);
			LgRenderSystemDrawPrimitive(LgPrimitiveTriangleList, LgFvfDiffuse | LgFvfTexture | LgFvfXYZ, vb, quadCount * 2);
			LgDirectorUpdateBatch(1);
			vp = (LgVertexDiffuseTexCroodP)LgVertexBufferLock(vb);
			quadCount = 0;
		}

		texture = regionTexture;
		appendQuad(attachment, slot, vp + quadCount * 3 * 2);
		quadCount++;
	}

	if (quadCount > 0) {
		LgVertexBufferUnlock(vb);
		LgRenderSystemSetTexture(texture);
		LgRenderSystemDrawPrimitive(LgPrimitiveTriangleList, LgFvfDiffuse | LgFvfTexture | LgFvfXYZ, vb, quadCount * 2);
		LgDirectorUpdateBatch(1);
	} else {
		LgVertexBufferUnlock(vb);
	}
}

static void update(LgAnimationDataP ad, float delta) {
	spAnimationState_update(ad->animationState, delta);
	spAnimationState_apply(ad->animationState, ad->skeleton);
	spSkeleton_updateWorldTransform(ad->skeleton);
}

static void visit(LgNodeP node, float delta) {
	LgAnimationDataP ad = (LgAnimationDataP)LgNodeUserData(node);
	update(ad, delta);
}

static void contentBox(LgNodeP node, LgAabbP contentBox) {
	LgAnimationDataP ad = (LgAnimationDataP)LgNodeUserData(node);
	float minX = FLT_MAX, minY = FLT_MAX, maxX = FLT_MIN, maxY = FLT_MIN;
	int i;
	float vertices[8];

	for (i = 0; i < ad->skeleton->slotCount; ++i) {
		spSlot* slot = ad->skeleton->slots[i];
		spRegionAttachment* attachment;

		if (!slot->attachment || slot->attachment->type != ATTACHMENT_REGION) continue;
		attachment = (spRegionAttachment *)slot->attachment;
		spRegionAttachment_computeWorldVertices(attachment, slot->skeleton->x, slot->skeleton->y, slot->bone, vertices);
		minX = LgMin(minX, vertices[VERTEX_X1]);
		minY = LgMin(minY, -vertices[VERTEX_Y1]);
		maxX = LgMax(maxX, vertices[VERTEX_X1]);
		maxY = LgMax(maxY, -vertices[VERTEX_Y1]);
		minX = LgMin(minX, vertices[VERTEX_X4]);
		minY = LgMin(minY, -vertices[VERTEX_Y4]);
		maxX = LgMax(maxX, vertices[VERTEX_X4]);
		maxY = LgMax(maxY, -vertices[VERTEX_Y4]);
		minX = LgMin(minX, vertices[VERTEX_X2]);
		minY = LgMin(minY, -vertices[VERTEX_Y2]);
		maxX = LgMax(maxX, vertices[VERTEX_X2]);
		maxY = LgMax(maxY, -vertices[VERTEX_Y2]);
		minX = LgMin(minX, vertices[VERTEX_X3]);
		minY = LgMin(minY, -vertices[VERTEX_Y3]);
		maxX = LgMax(maxX, vertices[VERTEX_X3]);
		maxY = LgMax(maxY, -vertices[VERTEX_Y3]);
	}

	contentBox->x = minX;
	contentBox->y = minY;
	contentBox->width = maxX - minX;
	contentBox->height = maxY - minY;
}

static void doHitTest(LgNodeP node, LgNodeEventDoHitTestP hittest) {
	LgAnimationDataP ad = (LgAnimationDataP)LgNodeUserData(node);
	int i;
	float vertices[8];

	for (i = 0; i < ad->skeleton->slotCount; ++i) {
		spSlot* slot = ad->skeleton->slots[i];
		spRegionAttachment* attachment;
		struct LgVector2 pt1, pt2, pt3, pt4;

		if (!slot->attachment || slot->attachment->type != ATTACHMENT_REGION) continue;
		attachment = (spRegionAttachment *)slot->attachment;
		spRegionAttachment_computeWorldVertices(attachment, slot->skeleton->x, slot->skeleton->y, slot->bone, vertices);

		pt1.x = vertices[VERTEX_X1], pt1.y = -vertices[VERTEX_Y1];
		pt2.x = vertices[VERTEX_X2], pt2.y = -vertices[VERTEX_Y2];
		pt3.x = vertices[VERTEX_X3], pt3.y = -vertices[VERTEX_Y3];
		pt4.x = vertices[VERTEX_X4], pt4.y = -vertices[VERTEX_Y4];

		if (LgVector2InTriangle(&hittest->localPosition, &pt1, &pt2, &pt4) ||
			LgVector2InTriangle(&hittest->localPosition, &pt4, &pt3, &pt2)) {
			hittest->hitted = LgTrue;
			return;
		}
	}
}

static void animationEventProc(LgEventType type, LgNodeP node, void *param) {
	switch(type) {
	case LG_NODE_EVENT_Destroy:
		destroy(node);
		break;
	case LG_NODE_EVENT_Draw:
		draw(node);
		break;
	case LG_NODE_EVENT_Visit:
		visit(node, *((float *)param));
		break;
	case LG_NODE_EVENT_ClearSlots:
		{
			LgAnimationDataP ad = (LgAnimationDataP)LgNodeUserData(node);
			LgSlotClear(&ad->slotEvent);
		}
		break;
	case LG_NODE_EVENT_ContentBox:
		contentBox(node, (LgAabbP)param);
		return;
	case LG_NODE_EVENT_DoHitTest:
		{
			LgAnimationDataP ad = (LgAnimationDataP)LgNodeUserData(node);
			if (ad->exactTouch) {
				doHitTest(node, (LgNodeEventDoHitTestP)param);
				return;
			}
		}
		break;
	}

	LgNodeDefEventProc(type, node, param);
}

static void animationCallback(spAnimationState* state, int trackIndex, spEventType type, spEvent* event, int loopCount) {
	LgNodeP node = (LgNodeP)state->context;
	LgAnimationDataP ad = (LgAnimationDataP)LgNodeUserData(node);
	spTrackEntry* entry = spAnimationState_getCurrent(state, 0);
	const char* animationName = (entry && entry->animation) ? entry->animation->name : 0;
	struct LgAnimationEvent evt;

	LgZeroMemory(&evt, sizeof(evt));

	if (type == ANIMATION_START) {
		evt.type = LgAnimationEventAnimationStart;
		evt.animationName = animationName;
		LgSlotCall(&ad->slotEvent, &evt);
	} else if (type == ANIMATION_END) {
		evt.type = LgAnimationEventAnimationEnd;
		evt.animationName = animationName;
		LgSlotCall(&ad->slotEvent, &evt);
	} else if (type == ANIMATION_COMPLETE) {
		evt.type = LgAnimationEventAnimationComplete;
		evt.animationName = animationName;
		evt.loopCount = 0;
		LgSlotCall(&ad->slotEvent, &evt);
	} else if (type == ANIMATION_EVENT) {
		evt.type = LgAnimationEventAnimationCustom;
		evt.data.name = event->data->name;
		evt.data.intVal = event->intValue;
		evt.data.floatVal = event->floatValue;
		evt.data.stringVal = event->stringValue;
		LgSlotCall(&ad->slotEvent, &evt);
	}
}

LgBool LgNodeIsAnimation(LgNodeP node) {
	return LgNodeEventProc(node) == (LgEvent)animationEventProc;
}

LgNodeP LgAnimationNodeCreateR(const char *animationFilename, const char *animationName, LgBool loop) {
	LgNodeP node;
	LgAnimationDataP ad;

	node = LgNodeCreateR((LgEvent)animationEventProc);
	LG_CHECK_ERROR(node);

	ad = (LgAnimationDataP)LgMallocZ(sizeof(struct LgAnimationData));
	LG_CHECK_ERROR(ad);
	LgNodeSetUserData(node, ad);

	ad->animation = LgResourceManagerLoadAnimation(animationFilename);
	LG_CHECK_ERROR(ad->animation);
	LgAnimationRetain(ad->animation);

	ad->skeleton = spSkeleton_create(ad->animation->skeletonData);
	LG_CHECK_ERROR(ad->skeleton);

	ad->animationState = spAnimationState_create(spAnimationStateData_create(ad->animation->skeletonData));
	LG_CHECK_ERROR(ad->animationState);

	if (animationName) LgAnimationSetAnimation(node, animationName, loop);
	spAnimationState_update(ad->animationState, 0);

	ad->animationState->context = node;
	ad->animationState->listener = animationCallback;

	update(ad, 0);
	return node;

_error:
	if (node) LgNodeRelease(node);
	return NULL;
}

LgBool LgAnimationSetAnimation(LgNodeP node, const char *name, LgBool loop) {
	LgAnimationDataP ad = (LgAnimationDataP)LgNodeUserData(node);
	spAnimation *animation = spSkeletonData_findAnimation(ad->animation->skeletonData, name);
	if (animation) {
		spAnimationState_setAnimation(ad->animationState, 0, animation, loop);
		return LgTrue;
	} else {
		return LgFalse;
	}
}

LgBool LgAnimationAddAnimation(LgNodeP node, const char *name, LgBool loop, float delay) {
	LgAnimationDataP ad = (LgAnimationDataP)LgNodeUserData(node);
	spAnimation *animation = spSkeletonData_findAnimation(ad->animation->skeletonData, name);

	if (animation) {
		spAnimationState_addAnimation(ad->animationState, 0, animation, loop, delay);
		return LgTrue;
	} else {
		return LgFalse;
	}
}

void LgAnimationStopAnimation(LgNodeP node) {
	LgAnimationDataP ad = (LgAnimationDataP)LgNodeUserData(node);
	spAnimationState_clearTrack(ad->animationState, 0);
}

float LgAnimationAnimationDuration(LgNodeP node, const char *name) {
	LgAnimationDataP ad = (LgAnimationDataP)LgNodeUserData(node);
	spAnimation *animation = spSkeletonData_findAnimation(ad->animation->skeletonData, name);
	return animation ? animation->duration : 0;
}

LgBool LgAnimationSetSkin(LgNodeP node, const char *name) {
	LgAnimationDataP ad = (LgAnimationDataP)LgNodeUserData(node);
	spSkin *skin = spSkeletonData_findSkin(ad->animation->skeletonData, name);
	if (skin) {
		spSkeleton_setSkin(ad->skeleton, skin);
		spSkeleton_setToSetupPose(ad->skeleton);
		return LgTrue;
	} else {
		return LgFalse;
	}
}

LgSlot *LgAnimationSlotEvent(LgNodeP node) {
	LgAnimationDataP ad = (LgAnimationDataP)LgNodeUserData(node);
	return &ad->slotEvent;
}

void LgAnimationSetExactTouch(LgNodeP node, LgBool value) {
	LgAnimationDataP ad = (LgAnimationDataP)LgNodeUserData(node);
	ad->exactTouch = value;
}

LgBool LgAnimationIsExactTouch(LgNodeP node) {
	LgAnimationDataP ad = (LgAnimationDataP)LgNodeUserData(node);
	return ad->exactTouch;
}
