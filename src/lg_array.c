#include "lg_array.h"
#include "lg_memory.h"
#include <stdlib.h>

struct LgArray {
	int			len, size;
	LgDestroy	destroyData;
	void **		data;
};

LgArrayP LgArrayCreate(LgDestroy destroyData) {
	LgArrayP array = (LgArrayP)LgMallocZ(sizeof(struct LgArray));
	LG_CHECK_ERROR(array);
	array->destroyData = destroyData;
	return array;

_error:
	return NULL;
}

void LgArrayDestroy(LgArrayP array) {
	int i;

	for (i = 0; i < array->len; i++) if (array->destroyData) array->destroyData(array->data[i]);
	if (array->data) LgFree(array->data);
	LgFree(array);
}

int LgArrayLen(LgArrayP array) {
	return array->len;
}

LgBool LgArraySet(LgArrayP array, int i, void *data) {
	if (i >= 0 && i < array->len) {
		array->destroyData(array->data[i]);
		array->data[i] = data;
		return LgTrue;
	} else {
		return LgFalse;
	}
}

void *LgArrayGet(LgArrayP array, int i) {
	return i >= 0 && i < array->len ? array->data[i] : NULL;
}

static void addCell(LgArrayP array) {
	if (array->len == array->size) {
		array->data = (void **)LgRealloc(array->data, (array->size + 16) * sizeof(void *));
		array->size = array->size + 16;
	}
}

void LgArrayInsert(LgArrayP array, int i, void *data) {
	addCell(array);
	LgMoveMemory(array->data + i + 1, array->data + i, 
		(array->len - i) * sizeof(void *));
	array->data[i] = data;
}

void LgArrayAppend(LgArrayP array, void *data) {
	addCell(array);
	array->data[array->len] = data;
	array->len++;
}

LgBool LgArrayRemove(LgArrayP array, int i) {
	if (i >= 0 && i < array->len) {
		array->destroyData(array->data[i]);
		LgMoveMemory(array->data + i, array->data + i + 1,
			(array->len - i - 1) * sizeof(void *));
		array->len--;
		return LgTrue;
	} else {
		return LgFalse;
	}
}

static void sort(void **values, int x, int y, LgCompare compare) {
	int xx = x, yy = y;
	void *k;

	if (x >= y) return;
	k = values[x];

	while(xx != yy) {
		while(xx < yy && compare(values[yy], k) >= 0) yy--;
		values[xx] = values[yy];
		while(xx < yy && compare(values[yy], k) <= 0) xx++;
		values[yy] = values[xx];
	}

	values[xx] = k;
	sort(values, x, xx - 1, compare);
	sort(values, xx + 1, y, compare);
}

void LgArraySort(LgArrayP array, LgCompare compare) {
	sort(array->data, 0, array->len - 1, compare);
}

int LgArrayFind(LgArrayP array, void *data) {
	int i;
	for (i = 0; i < array->len; i++) {
		if (array->data[i] == data) return i;
	}
	return -1;
}

void *LgArrayData(LgArrayP array) {
	return (void *)array->data;
}

void LgArrayClear(LgArrayP array) {
	int i;
	for (i = 0; i < LgArrayLen(array); i++) {
		if (array->destroyData) array->destroyData(array->data[i]);
	}
	LgFree(array->data);
}
