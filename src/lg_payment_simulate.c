#include "lg_payment.h"

static LgSlot slotPayment = {NULL};

LgBool LgPaymentOpen() {
	return LgTrue;
}

void LgPaymentClose() {
	LgSlotClear(&slotPayment);
}

LgBool LgPaymentCanMakePayments() {
	return LgTrue;
}

LgBool LgPaymentPurchase(const char *productId) {
	struct LgPaymentEvent evt;
	evt.type = LgPaymentEventPurchased;
	evt.productId = productId;
	evt.receipt = NULL;
	LgSlotCall(&slotPayment, &evt);
	if (evt.receipt) json_delete(evt.receipt);
	return LgTrue;
}

LgSlot *LgPaymentSlot() {
	return &slotPayment;
}
