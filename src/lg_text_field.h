﻿#ifndef _LG_TEXT_FIELD_H_
#define _LG_TEXT_FIELD_H_

#include "lg_node.h"

enum {
	LG_TEXTFIELD_EVENT_InsertText = LG_NODE_EVENT_LAST,
	LG_TEXTFIELD_EVENT_CompText,
	LG_TEXTFIELD_EVENT_DeleteChar,
	LG_TEXTFIELD_EVENT_SetFocus,
	LG_TEXTFIELD_EVENT_KillFocus,
	LG_TEXTFIELD_EVENT_SetFontName,
	LG_TEXTFIELD_EVENT_FontName,
	LG_TEXTFIELD_EVENT_SetFontSize,
	LG_TEXTFIELD_EVENT_FontSize,
	LG_TEXTFIELD_EVENT_SetColor,
	LG_TEXTFIELD_EVENT_Color,
	LG_TEXTFIELD_EVENT_SetText,
	LG_TEXTFIELD_EVENT_Text,
	LG_TEXTFIELD_EVENT_TextLength,
	LG_TEXTFIELD_EVENT_SetCompColor,
	LG_TEXTFIELD_EVENT_CompColor,
	LG_TEXTFIELD_EVENT_SetPlaceHolderColor,
	LG_TEXTFIELD_EVENT_PlaceHolderColor,
	LG_TEXTFIELD_EVENT_SetCursorColor,
	LG_TEXTFIELD_EVENT_CursorColor,
	LG_TEXTFIELD_EVENT_LAST,
};

typedef struct LgTextFieldEventInsertText {
	const wchar_t *	text;
	int				textLength;
}*LgTextFieldEventInsertTextP;

typedef struct LgTextFieldEventCompText {
	const wchar_t *	text;
	int				textLength;
}*LgTextFieldEventCompTextP;

typedef struct LgTextFieldEventGetText {
	char *	buf;
	int		bufLength;
}*LgTextFieldEventGetTextP;

LgBool LgNodeIsTextField(LgNodeP node);

LgNodeP LgTextFieldCreateR(const LgVector2P size, const char *text, const char *placeHolder);

void LgTextFieldInsertText(LgNodeP node, const wchar_t *text, int len);

void LgTextFieldCompText(LgNodeP node, const wchar_t *text, int len);

void LgTextFieldDeleteChar(LgNodeP node);

void LgTextFieldSetFocus(LgNodeP node);

void LgTextFieldKillFocus(LgNodeP node);

void LgTextFieldSetFontName(LgNodeP node, const char *fontname);

const char *LgTextFieldFontName(LgNodeP node);

void LgTextFieldSetFontSize(LgNodeP node, int fontsize);

int LgTextFieldFontSize(LgNodeP node);

void LgTextFieldSetColor(LgNodeP node, LgColor color);

LgColor LgTextFieldColor(LgNodeP node);

void LgTextFieldSetText(LgNodeP node, const char *text);

int LgTextFieldTextLength(LgNodeP node);

void LgTextFieldText(LgNodeP node, char *buf, int bufLen);

void LgTextFieldSetCompColor(LgNodeP node, LgColor color);

LgColor LgTextFieldCompColor(LgNodeP node);

void LgTextFieldSetPlaceHolderColor(LgNodeP node, LgColor color);

LgColor LgTextFieldPlaceHolderColor(LgNodeP node);

void LgTextFieldSetCursorColor(LgNodeP node, LgColor color);

LgColor LgTextFieldCursorColor(LgNodeP node);

LgSlot *LgTextFieldSlotChange(LgNodeP node);

#endif