#ifndef _LG_RTREE_H_
#define _LG_RTREE_H_

#include "lg_basetypes.h"
#include "lg_aabb.h"

typedef void *LgRTreeId;

typedef void *LgRTreeP;

LgRTreeP LgRTreeCreate();

void LgRTreeDestroy(LgRTreeP tree);

LgRTreeP LgRTreeInsertRect(LgRTreeP tree, const LgAabbP rt, LgRTreeId id);

LgRTreeP LgRTreeDelectRect(LgRTreeP tree, const LgAabbP rt, LgRTreeId id);

typedef LgBool (* LgRTreeSearchCallback)(LgRTreeId id, void* userdata);

int LgRTreeSearch(LgRTreeP tree, const LgAabbP rt, LgRTreeSearchCallback callback, void *userdata);

#endif