﻿#ifndef _LG_SPRITE_H
#define _LG_SPRITE_H

#include "lg_node.h"
#include "spine/Atlas.h"

typedef struct LgTexturePack {
	LG_REFCNT_HEAD
	spAtlas *	atlas;
}*LgTexturePackP;

LG_REFCNT_FUNCTIONS_DECL(LgTexturePack)

enum LgSpriteLocationType {
	LgSpriteInFile,
	LgSpriteInTexturePack,
};

typedef struct LgSpriteLocation {
	enum LgSpriteLocationType	type;
	const char *				filename;
	const char *				name;
}*LgSpriteLocationP;

LgTexturePackP LgTexturePackCreateR(const char *filename);

LgNodeP LgSpriteCreateR(const LgSpriteLocationP location, const LgVector2P size, const LgPointP insetLT, const LgPointP insetRB);

LgBool LgNodeIsSprite(LgNodeP node);

void LgSpriteSetSize(LgNodeP node, const LgVector2P size);

#endif