﻿#include "lg_window_android.h"
#include <jni.h>
#include "lg_memory.h"
#include "android/lg_jni_helper.h"
#include "lg_log.h"
#include "lg_meminputstream.h"
#include "lg_memoutputstream.h"
#include "lg_director.h"

extern struct android_app* g_androidApp;

LgBool LgWindowOpen(const LgSizeP size) {
	return LgTrue;
}

void LgWindowClose() {

}

void LgWindowSize(LgSizeP size) {
	size->width = ANativeWindow_getWidth(g_androidApp->window);
	size->height = ANativeWindow_getHeight(g_androidApp->window);
}

ANativeWindow *LgWindowAWindow() {
	return g_androidApp->window;
}

LgBool LgWindowIsHD() {
    return LgFalse;
}

void LgWindowShowKeyboard(LgBool show) {
	jmethodID methodShowSoftInput = 0;

 	LgJniAttachEnv();

    methodShowSoftInput = (*JNI_ENV)->GetStaticMethodID(JNI_ENV, LgJniLuaGameHelperClass(),
        show ? "showSoftInput" : "hideSoftInput", "()V");
    (*JNI_ENV)->CallStaticVoidMethod(JNI_ENV, LgJniLuaGameHelperClass(), methodShowSoftInput);
 
	LgJniReleaseEnv();
}

static wchar_t *toUtf16(const char *text, int *size) {
    LgInputStreamP input;
    LgOutputStreamP output;
    wchar_t c, *result;

    input = LgMemInputStreamCreate(text, strlen(text));
    output = LgMemOutputStreamCreate(NULL, 0);

    while(LgInputStreamReadRune(input, &c)) {
        LgOutputStreamWriteBytes(output, &c, sizeof(c));
    }

    result = (wchar_t *)LgMemOutputStreamDeatchData(output, size);
    LgInputStreamDestroy(input);
    LgOutputStreamDestroy(output);
    return result;
}

static void destroyKeyboardEvent(LgKeyboardEventP evt) {
    if (evt->type == LgKeyboardEventInputText || 
        evt->type == LgKeyboardEventCompText) {
        if (evt->text) LgFree((void *)evt->text);
    }
    LgFree(evt);
}

static LgBool handleKeyboardEvent(float delta, LgKeyboardEventP evt) {
    LgDirectorHandleKeyboardEvent(evt);
    return LgFalse;
}

// 所有jni的native方法函数处理都要避免重复attach jni环境，所以把键盘处理通过scheduler来处理
static void postKeyboardEvent(struct LgKeyboardEvent *evt) {
    LgKeyboardEventP evt2;

    evt2 = (LgKeyboardEventP)LgMalloc(sizeof(struct LgKeyboardEvent));
    memcpy(evt2, evt, sizeof(struct LgKeyboardEvent));
    LgSchedulerRun((LgStep)handleKeyboardEvent, (LgDestroy)destroyKeyboardEvent, evt2, 0, 0, 0, LgFalse);
}

JNIEXPORT void JNICALL Java_com_luagame_LuaGameHelper_eventKeyboardInputText(JNIEnv *env, jclass clazz, jstring text) {
    const char *textUtf8 = (*env)->GetStringUTFChars(env, text, NULL);
    int size;
    wchar_t *textWide = toUtf16(textUtf8, &size);
    struct LgKeyboardEvent evt;

    evt.type = LgKeyboardEventInputText;
    evt.text = textWide;
    evt.textLength = size / sizeof(wchar_t);
    postKeyboardEvent(&evt);

    (*env)->ReleaseStringUTFChars(env, text, textUtf8);
}

JNIEXPORT void JNICALL Java_com_luagame_LuaGameHelper_eventKeyboardCompText(JNIEnv *env, jclass clazz, jstring text) {
    const char *textUtf8 = (*env)->GetStringUTFChars(env, text, NULL);
    int size;
    wchar_t *textWide = toUtf16(textUtf8, &size);
    struct LgKeyboardEvent evt;

    evt.type = LgKeyboardEventCompText;
    evt.text = textWide;
    evt.textLength = size / sizeof(wchar_t);
    postKeyboardEvent(&evt);
    (*env)->ReleaseStringUTFChars(env, text, textUtf8);
}

JNIEXPORT void JNICALL Java_com_luagame_LuaGameHelper_eventKeyboardDeleteChar(JNIEnv *env, jclass clazz) {
    struct LgKeyboardEvent evt;
    evt.type = LgKeyboardEventDeleteChar;
    postKeyboardEvent(&evt);
}

JNIEXPORT void JNICALL Java_com_luagame_LuaGameHelper_eventKeyboardClose(JNIEnv *env, jclass clazz) {
    struct LgKeyboardEvent evt;
    evt.type = LgKeyboardEventCloseKeyboard;
    postKeyboardEvent(&evt);
}
