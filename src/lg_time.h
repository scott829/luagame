#ifndef _LG_TIME_H
#define _LG_TIME_H

#include "lg_basetypes.h"

typedef double LgTime;

#define LS_TIME_DAY (3600 * 24)

#define LS_TIME_HOUR 3600

#define LS_TIME_MINUTE 60

#define LS_TIME_SECONDS 1

#define LS_TIME_MILLISECOND 0.001

#define LS_TIME_MACROSECOND 0.000001

LgTime LgGetTimeOfDay();

typedef LgBool (* LgStep)(LgTime delta, void *userdata);

#endif