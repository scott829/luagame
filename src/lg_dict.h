#ifndef _LG_DICT_H
#define _LG_DICT_H

#include "lg_basetypes.h"

typedef struct LgDict *LgDictP;

LgDictP LgDictCreate(LgCompare compareKey, LgDestroy destroyKey, LgDestroy destroyData);

void LgDictDestroy(LgDictP dict);

LgBool LgDictInsert(LgDictP dict, void *key, void *value);

void LgDictReplace(LgDictP dict, void *key, void *value);

void *LgDictFind(LgDictP dict, void *key);

LgBool LgDictContains(LgDictP dict, void *key);

LgBool LgDictRemove(LgDictP dict, void *key);

void LgDictForeach(LgDictP dict, LgForeachPair foreach, void *userdata);

void LgDictClear(LgDictP dict);

int LgDictCount(LgDictP dict);

#endif