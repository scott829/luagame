#include "lg_fontsystem.h"
#include "lg_fontdriver.h"
#include "lg_ttffontdriver.h"
#include "lg_bmpfontdriver.h"
#include "lg_dict.h"
#include "lg_platform.h"
#include "lg_memory.h"
#include "jansson/jansson.h"
#include "lg_log.h"

LgFontDriverP g_fontDrivers[] = {
	&ttfFontDriver,
	&bmpFontDriver,
	NULL
};

struct LgFont {
	char *			alias;
	LgFontDataP		font;
	LgFontDriverP	driver;
};

LgDictP g_fonts = NULL;

LgFontDriverP LgFontGetDriver(LgFontP font) {
	return font->driver;
}

LgFontDataP LgFontGetData(LgFontP font) {
	return font->font;
}

const char *LgFontGetAlias(LgFontP font) {
	return font->alias;
}

static void destroyFont(LgFontP font) {
	font->driver->destroyFont(font->font);
	LgFree(font);
}

static void readFonts(json_t *config) {
	json_t *fontlist;

	fontlist = json_object_get(config, "fontlist");
	if (fontlist && json_is_array(fontlist)) {
		int i;
		
		for (i = 0; i < (int)json_array_size(fontlist); i++) {
			json_t *fontdef = json_array_get(fontlist, i);
			json_t *type, *alias, *filename;

			type = json_object_get(fontdef, "type");
			alias = json_object_get(fontdef, "alias");
			filename = json_object_get(fontdef, "filename");
			
			if (type && json_is_string(type) &&
				alias && json_is_string(alias) &&
				filename && json_is_string(filename)) {

				LgFontDriverP *p = g_fontDrivers;
				while(*p) {
					if (strcmp((*p)->name, json_string_value(type)) == 0) {
						LgFontDataP fontdata = (*p)->createFont(json_string_value(filename));
						if (fontdata) {
							LgFontP font = (LgFontP)LgMalloc(sizeof(struct LgFont));
							if (font) {
								font->alias = strdup(json_string_value(alias));
								font->driver = *p;
								font->font = fontdata;
								LgDictReplace(g_fonts, font->alias, font);
								LgLogInfo("load font: %s", json_string_value(alias));
								break;
							} else {
								LgLogError("failed to load font %s", json_string_value(alias));
							}
						}
					}
					p++;
				}
			}
		}
	}
}

LgBool LgFontSystemOpen() {
	int i;
	json_t *config = NULL;

	for (i = 0; g_fontDrivers[i]; i++)
		if (g_fontDrivers[i]->init) LG_CHECK_ERROR(g_fontDrivers[i]->init());

	config = json_loadp("fonts/fontlist.conf");
	LG_CHECK_ERROR(config);
	g_fonts = LgDictCreate((LgCompare)strnocasecmp, (LgDestroy)LgFree, (LgDestroy)destroyFont);
	LG_CHECK_ERROR(g_fonts);
	readFonts(config);
	json_delete(config);
	LgLogInfo("font system initialized.", NULL);
	return LgTrue;

_error:
	if (config) json_delete(config);
	LgFontSystemClose();
	LgLogError("failed to initialize font system.", NULL);
	return LgFalse;
}

void LgFontSystemClose() {
	if (g_fonts) {
		int i;

		LgDictDestroy(g_fonts);
		g_fonts = NULL;
		for (i = 0; g_fontDrivers[i]; i++)
			if (g_fontDrivers[i]->destroy) g_fontDrivers[i]->destroy();
	}
}

LgFontP LgFontSystemFindFont(const char *alias) {
	LgFontP font = (LgFontP)LgDictFind(g_fonts, (void *)alias);
	if (!font) {
		return (LgFontP)LgDictFind(g_fonts, "_default");
	} else {
		return font;
	}
}
