#ifndef _LG_OUTPUTSTREAM_H
#define _LG_OUTPUTSTREAM_H

#include "lg_basetypes.h"

typedef struct LgOutputStream *LgOutputStreamP;

struct LgOutputStreamImpl {
	LgDestroy destroy;
	int (* writeBytes)(LgOutputStreamP stream, const void *data, int size);
};

struct LgOutputStream {
	struct LgOutputStreamImpl *impl;
};

#define LgOutputStreamWriteBytes(stream, data, size) ((LgOutputStreamP)stream)->impl->writeBytes(((LgOutputStreamP)stream), data, size)

#define LgOutputStreamDestroy(stream) ((LgOutputStreamP)stream)->impl->destroy(((LgOutputStreamP)stream))

LgBool LgOutputStreamWriteByte(LgOutputStreamP stream, unsigned char c);

LgBool LgOutputStreamWriteRune(LgOutputStreamP stream, wchar_t wc);

#endif