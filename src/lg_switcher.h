#ifndef _LG_SWITCHER_H
#define _LG_SWITCHER_H

#include "lg_controlnode.h"

typedef struct LgSwitcherData {
	struct LgControlData	base;
	LgNodeP					buttonNode;
	LgBool					value;
	LgSlot					slotChange;
}*LgSwitcherDataP;

LgNodeP LgSwitcherCreateR(const LgVector2P size);

LgBool LgNodeIsSwitcher(LgNodeP node);

LgBool LgSwitcherValue(LgNodeP node);

void LgSwitcherSetValue(LgNodeP node, LgBool value);

void LgSwitcherSetButton(LgNodeP node, LgNodeP buttonNode);

LgSlot *LgSwitcherSlotChange(LgNodeP node);

#endif