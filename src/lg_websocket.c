#include "lg_websocket.h"
#include "lg_url.h"
#include "event2/event.h"
#include "event2/util.h"
#include "event2/bufferevent.h"
#include "event2/buffer.h"
#include "lg_init.h"
#include "lg_memory.h"
#include <math.h>
#include "lg_base64.h"
#include "regexp/pcre.h"
#include "lg_platform.h"
#include "lg_memoutputstream.h"
#include "lg_scheduler.h"

#pragma pack(1)
struct LgWebSocketFrameHeader {
	unsigned char opcode:4;
	unsigned char rsv:3;
	unsigned char fin:1;
	unsigned char payloadLength:7;
	unsigned char mask:1;
};
#pragma pack()

struct LgWebSocket {
	LG_REFCNT_HEAD
	evutil_socket_t			s;
	struct bufferevent *	bev;
	LgUrlP					urlComponent;
	enum LgWebSocketState	state;
	LgOutputStreamP			messageData;
	unsigned char			secKey[16];
	LgSlot					slotStateChanged, slotStateOpened, slotMessage, slotError;
};

LG_REFCNT_FUNCTIONS(LgWebSocket)

static void changeState(LgWebSocketP ws, enum LgWebSocketState newState) {
	if (ws->state != newState) {
		struct LgWebSocketChangedEvent event;
		event.newState = newState;
		event.oldState = ws->state;
		ws->state = newState;
		LgSlotCall(&ws->slotStateChanged, &event);
	}
}

static void close(LgWebSocketP ws, enum LgWebSocketErrorType err) {
	if (err != LgWebSocketErrorNone) LgSlotCall(&ws->slotError, (void *)err);
	
	if (ws->urlComponent) {
		LgUrlDestroy(ws->urlComponent);
		ws->urlComponent = NULL;
	}

	if (ws->bev) {
		bufferevent_free(ws->bev);
		ws->bev = NULL;
	}
	if (ws->s) {
		evutil_closesocket(ws->s);
		ws->s = 0;
	}

	changeState(ws, LgWebSocketStateDisconnected);
}

static void destroyWebSocket(LgWebSocketP ws) {
	close(ws, LgWebSocketErrorNone);
	LgSlotClear(&ws->slotError);
	LgSlotClear(&ws->slotMessage);
	LgSlotClear(&ws->slotStateOpened);
	LgSlotClear(&ws->slotStateChanged);
	if (ws->messageData) LgOutputStreamDestroy(ws->messageData);
	LgFree(ws);
}

LgWebSocketP LgWebSocketCreateR() {
	LgWebSocketP ws = (LgWebSocketP)LgMallocZ(sizeof(struct LgWebSocket));
	LG_CHECK_ERROR(ws);
	LG_REFCNT_INIT(ws, destroyWebSocket);
	ws->state = LgWebSocketStateDisconnected;
	ws->messageData = LgMemOutputStreamCreate(NULL, 0);
	return ws;
	
_error:
	if (ws) LgWebSocketRelease(ws);
	return NULL;
}

static void startHandshake(LgWebSocketP ws) {
	char temp[512];
	char *secKeyBase64;
	int i;
	const char *request =
		"GET /%s HTTP/1.1\r\n"
		"Host: %s:%d\r\n"
		"Upgrade: websocket\r\n"
		"Connection: Upgrade\r\n"
		"Sec-WebSocket-Key: %s\r\n"
		"Sec-WebSocket-Version: 13\r\n"
		"\r\n";
	
	for (i = 0; i < sizeof(ws->secKey); i++)
		ws->secKey[i] = (unsigned char)(rand() % 0xff);
	secKeyBase64 = LgBase64Encode((char *)ws->secKey, sizeof(ws->secKey));
	
	changeState(ws, LgWebSocketStateHandshake);
	
	sprintf(temp, request,
			ws->urlComponent->path,
			ws->urlComponent->host, ws->urlComponent->port,
			secKeyBase64);
	bufferevent_write(ws->bev, temp, strlen(temp));
	
	LgFree(secKeyBase64);
}

static void socketEvent(struct bufferevent *bev, short what, void *arg) {
	LgWebSocketP ws = (LgWebSocketP)arg;
	
	if (what == BEV_EVENT_CONNECTED) {
		// 连接成功，发送握手请求
		startHandshake(ws);
	} else if (what == BEV_EVENT_TIMEOUT) {
		// 超时了
		close(ws, LgWebSocketErrorTimeout);
	} else if (what == BEV_EVENT_EOF) {
		// 服务端断开了连接
		close(ws, LgWebSocketErrorClose);
	} else if (what & BEV_EVENT_ERROR) {
		// 出错了
		close(ws, LgWebSocketErrorReadData);
	}
}

static void socketWrite(struct bufferevent *bev, void *arg) {
}

static int getFrame(LgWebSocketP ws) {
	int len = LgMemOutputStreamLen(ws->messageData);

	if (len == 0) {
		return sizeof(struct LgWebSocketFrameHeader);
	} else if (len < sizeof(struct LgWebSocketFrameHeader)) {
		return len - sizeof(struct LgWebSocketFrameHeader);
	} else {
		const char *p = (const char *)LgMemOutputStreamData(ws->messageData);
		const struct LgWebSocketFrameHeader *header = (const struct LgWebSocketFrameHeader *)p;
		unsigned long long payloadLength;
		int lsz = 0;

		if (header->payloadLength <= 125) {
			payloadLength = header->payloadLength;
		} else if (header->payloadLength == 126) {
			lsz = sizeof(unsigned short);

			if (len < lsz + (int)(sizeof(struct LgWebSocketFrameHeader))) {
				return sizeof(struct LgWebSocketFrameHeader) + lsz - len;
			}

			payloadLength = LG_SWAP_SHORT(*((unsigned short *)(header + 1)));
		} else { // == 127
			lsz = sizeof(unsigned long long);

			if (len < lsz + (int)(sizeof(struct LgWebSocketFrameHeader))) {
				return sizeof(struct LgWebSocketFrameHeader) + lsz - len;
			}

			payloadLength = LG_SWAP_LONGLONG(*((unsigned long long *)(header + 1)));
		}

		if (len < sizeof(struct LgWebSocketFrameHeader) + lsz + payloadLength) {
			return (int)(sizeof(struct LgWebSocketFrameHeader) + lsz + payloadLength - len);
		}

		return 0;
	}
}

static const char *frameData(const char *data, int *len, enum LgWebSocketFrameOpCode *opcode) {
	struct LgWebSocketFrameHeader *header = (struct LgWebSocketFrameHeader *)data;
	unsigned long long payloadLength;
	const char *p;

	if (header->payloadLength <= 125) {
		payloadLength = header->payloadLength;
		p = (const char *)(header + 1);
	} else if (header->payloadLength == 126) {
		payloadLength = LG_SWAP_SHORT(*((unsigned short *)(header + 1)));
		p = (const char *)(header + 1) + sizeof(unsigned short);
	} else { // -- 127
		payloadLength = LG_SWAP_LONGLONG(*((unsigned long long *)(header + 1)));
		p = (const char *)(header + 1) + sizeof(unsigned long long);
	}

	*len = (int)payloadLength;
	*opcode = (enum LgWebSocketFrameOpCode)header->opcode;
	return p;
}

static void handlePackage(LgWebSocketP ws) {
	int len;
	enum LgWebSocketFrameOpCode opcode;
	const char *data = frameData((const char *)LgMemOutputStreamData(ws->messageData), &len, &opcode);
	struct LgWebSocketMessageEvent evt = {data, len};
	LgSlotCall(&ws->slotMessage, &evt);
}

static void socketRead(struct bufferevent *bev, void *arg) {
	LgWebSocketP ws = (LgWebSocketP)arg;
	struct evbuffer *input = bufferevent_get_input(bev);
	char *line;
	size_t lineSize;

	while(evbuffer_get_length(input) > 0) {
		if (ws->state == LgWebSocketStateHandshake) {
			// 读第一行
			if ((line = evbuffer_readln(input, &lineSize, EVBUFFER_EOL_CRLF))) {
				const char *error;
				int erroffset;
				pcre *re = pcre_compile("HTTP/.\\.. (?P<code>\\d+) .+", PCRE_EXTRA, &error, &erroffset, NULL);
				int vec[64], codeIdx;

				pcre_exec(re, NULL, line, lineSize, 0, 0, vec, sizeof(vec) / sizeof(int));
				codeIdx = pcre_get_stringnumber(re, "code");

				if (codeIdx >= 0) {
					int code = atoi(line + vec[codeIdx * 2]);
			
					pcre_free(re);
					free(line);

					if (code != 101) {
						// 握手失败
						if (code == 400) close(ws, LgWebSocketErrorBadRequest);
						else if (code == 404) close(ws, LgWebSocketErrorNotFound);
						else if (code == 403) close(ws, LgWebSocketErrorForbidden);
						else close(ws, LgWebSocketErrorBadResponse);
					} else {
						changeState(ws, LgWebSocketStateReadingHandshakeHeaders);
					}
				} else {
					// 错误的反馈头
					pcre_free(re);
					free(line);
					close(ws, LgWebSocketErrorBadResponse);
					break;
				}
			}
		} else if (ws->state == LgWebSocketStateReadingHandshakeHeaders) {
			if ((line = evbuffer_readln(input, &lineSize, EVBUFFER_EOL_CRLF))) {
				if (strlen(line) == 0) {
					free(line);
					changeState(ws, LgWebSocketStateConnected);
					LgSlotCall(&ws->slotStateOpened, NULL);
				} else {
					char *p = strchr(line, ':');
					char *name, *value;

					*p = 0;
					name = strdup(line);
					*p = ':';
					p++;
					while(*p == ' ') p++;
					value = strdup(p);

					free(line);

					if (strnocasecmp(name, "Connection") == 0) {
						if (strnocasecmp(value, "Upgrade") != 0) {
							close(ws, LgWebSocketErrorBadResponse);
						}
					} else if (strnocasecmp(name, "Upgrade") == 0) {
						if (strnocasecmp(value, "websocket") != 0) {
							close(ws, LgWebSocketErrorBadResponse);
						}
					}

					free(name);
					free(value);
				}
			}
		} else if (ws->state == LgWebSocketStateConnected) {
			int need = getFrame(ws);
			char temp[512];
			int rsz;

			rsz = evbuffer_remove(input, temp, LgMin(need, sizeof(temp)));
			LgOutputStreamWriteBytes(ws->messageData, temp, rsz);

			if (getFrame(ws) == 0) {
				handlePackage(ws);
				LgMemOutputStreamClear(ws->messageData);
			}
		}
	}
}

LgBool LgWebSocketOpen(LgWebSocketP ws, const char *url) {
	if (ws->s) {
		// 已经连接了
		return LgFalse;
	}
	
	ws->urlComponent = LgUrlParse(url);
	LG_CHECK_ERROR(ws->urlComponent);
	
	if (strcmp(ws->urlComponent->scheme, "ws") != 0) {
		return LgFalse;
	}
	
	// 创建socket连接
	ws->s = socket(AF_INET, SOCK_STREAM, 0);
	LG_CHECK_ERROR(ws->s);
	evutil_make_socket_nonblocking(ws->s);
	ws->bev = bufferevent_socket_new(g_evtbase, ws->s, BEV_OPT_CLOSE_ON_FREE);
	bufferevent_setcb(ws->bev, socketRead, socketWrite, socketEvent, ws);
	bufferevent_enable(ws->bev, EV_READ | EV_PERSIST);
	
	// 开始连接
	changeState(ws, LgWebSocketStateConnecting);
	LG_CHECK_ERROR(bufferevent_socket_connect_hostname(ws->bev, NULL, AF_UNSPEC, ws->urlComponent->host, ws->urlComponent->port) == 0);
	return LgTrue;
	
_error:
	close(ws, LgWebSocketErrorNone);
	return LgFalse;
}

void LgWebSocketClose(LgWebSocketP ws) {
	close(ws, LgWebSocketErrorNone);
}

void LgWebSocketSendMessage(LgWebSocketP ws, const char *data, size_t len, enum LgWebSocketFrameOpCode type) {
	struct LgWebSocketFrameHeader header;
	unsigned char mask[4];
	int i;
	char temp[512];

	LgZeroMemory(&header, sizeof(header));
	header.fin = LgTrue;
	header.rsv = 0;
	header.opcode = (unsigned int)type;
	header.mask = 1;

	if (len <= 125) {
		header.payloadLength = (unsigned int)len;
	} else if (len <= 0xffff) {
		header.payloadLength = 126;
	} else {
		header.payloadLength = 127;
	}

	// 写frame头
	bufferevent_write(ws->bev, &header, sizeof(header));

	// 写长度
	if (len > 125 && len < 0xffff) {
		unsigned short _len = (unsigned short)len;
		_len = LG_SWAP_SHORT(_len);
		bufferevent_write(ws->bev, &_len, sizeof(_len));
	} else if (len > 0xffff) {
		unsigned long long _len = (unsigned long long)len;
		_len = LG_SWAP_LONGLONG(_len);
		bufferevent_write(ws->bev, &_len, sizeof(_len));
	}

	// 写掩码
	bufferevent_write(ws->bev, mask, sizeof(mask));

	// 写数据
	for (i = 0; i < (int)len; i += sizeof(temp)) {
		int j, blockLen;

		blockLen = LgMin(sizeof(temp), len - i);
		for (j = i; j < i + blockLen; j++) {
			int maskIndex = j % 4;
			temp[j - i] = data[j] ^ mask[maskIndex];
		}
		bufferevent_write(ws->bev, temp, blockLen);
	}
}

LgSlot *LgWebSocketSlotOpen(LgWebSocketP ws) {
	return &ws->slotStateOpened;
}

LgSlot *LgWebSocketSlotMessage(LgWebSocketP ws) {
	return &ws->slotMessage;
}

LgSlot *LgWebSocketSlotError(LgWebSocketP ws) {
	return &ws->slotError;
}
