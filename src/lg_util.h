#ifndef _LG_UTIL_H
#define _LG_UTIL_H

#include <stdlib.h>

void LgRelPath(char *result, size_t size, const char *relpath, const char *filename);

wchar_t *LgUtf8ToWide(const char *text, int len);

char *LgWideToUtf8(const wchar_t *text, int len);

#endif