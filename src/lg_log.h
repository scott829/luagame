#ifndef _LG_LOG_H_
#define _LG_LOG_H_

#include "lg_basetypes.h"

enum LgLogType {
	LgLogINFO,
	LgLogWARN,
	LgLogERR
};

LgBool LgLogSystemOpen();

void LgLogSystemClose();

void LgLogSystemLog(enum LgLogType type, const char *filename, int line, const char *fmt, ...);

#define LgLogInfo(fmt, ...) LgLogSystemLog(LgLogINFO, __FILE__, __LINE__, fmt, __VA_ARGS__)

#define LgLogWarning(fmt, ...) LgLogSystemLog(LgLogWARN, __FILE__, __LINE__, fmt, __VA_ARGS__)

#define LgLogError(fmt, ...) LgLogSystemLog(LgLogERR, __FILE__, __LINE__, fmt, __VA_ARGS__)

#endif