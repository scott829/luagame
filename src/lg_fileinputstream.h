#ifndef _LG_MEMINPUTSTREAM_H
#define _LG_MEMINPUTSTREAM_H

#include "lg_inputstream.h"

LgInputStreamP LgFileInputStreamCreate(const char *filename, const LgRangeP range);

#endif