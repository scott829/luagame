#include "lg_scheduler.h"
#include "lg_memory.h"
#include "lg_time.h"
#include "lg_director.h"
#include <pthread.h>
#include "lg_avltree.h"

struct LgSchedulerItem {
	LG_REFCNT_HEAD
	LgSchedulerItemP	prev, next;
	LgStep				step;
	LgDestroy			destroyUserdata;
	void *				userdata;
	LgTime				prevTime;
	LgTime				interval, cinterval;
	int					repeat;
	LgTime				delay;
	LgBool				started, paused;
	LgListNodeP			newItemsNode; // 如果在新建项列表内，则此处保存新建项节点
};

LG_REFCNT_FUNCTIONS(LgSchedulerItem);

static LgSchedulerItemP g_first = NULL, g_last = NULL;
static int g_count = 0;
static LgBool g_init = LgFalse;
static pthread_mutex_t g_mutex;
static LgBool g_inUpdate = LgFalse;
static LgListP g_newItems = NULL;

LgBool LgSchedulerOpen() {
	pthread_mutexattr_t attr;

	g_first = g_last = NULL;

	pthread_mutexattr_init(&attr);
	pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE); 
	pthread_mutex_init(&g_mutex, &attr);
	pthread_mutexattr_destroy(&attr);
	g_init = LgTrue;
	return LgTrue;
}

void LgSchedulerClose() {
	if (g_init) {
		LgSchedulerClear();
		pthread_mutex_destroy(&g_mutex);
	}
}

static void destroyItem(LgSchedulerItemP item) {
	if (item->destroyUserdata) item->destroyUserdata(item->userdata);
	LgFree(item);
}

static void addToList(LgSchedulerItemP item) {
	if (g_last) {
		item->prev = g_last;
		g_last->next = item;
		g_last = item;
	} else {
		g_last = g_first = item;
	}
}

LgSchedulerItemP LgSchedulerRun(LgStep step, LgDestroy destroyUserdata, void *userdata, LgTime interval, int repeat, LgTime delay, LgBool paused) {
	LgSchedulerItemP item;

	item = (LgSchedulerItemP)LgMallocZ(sizeof(struct LgSchedulerItem));
	if (!item) return NULL;
	LG_REFCNT_INIT(item, destroyItem);
	item->step = step;
	item->userdata = userdata;
	item->destroyUserdata = destroyUserdata;
	item->prevTime = LgGetTimeOfDay();
	item->interval = interval;
	item->repeat = repeat;
	item->started = LgFalse;
	item->delay = delay;
	item->paused = paused;

	if (g_inUpdate) {
		if (!g_newItems) g_newItems = LgListCreate(NULL);
		item->newItemsNode = LgListAppend(g_newItems, item);
	} else {
		pthread_mutex_lock(&g_mutex);
		addToList(item);
		g_count++;
		pthread_mutex_unlock(&g_mutex);
	}

	return item;
}

void LgSchedulerUpdate() {
	LgTime time;
	LgSchedulerItemP item;

	pthread_mutex_lock(&g_mutex);
	g_inUpdate = LgTrue;

	time = LgGetTimeOfDay();
	item = g_first;

	while(item) {
		LgBool removed = LgFalse;
		LgSchedulerItemP next = item->next;

		if (!item->paused) {
			LgTime delta = (time - item->prevTime) * LgDirectorSpeed();

			if (!item->started) {
				item->delay -= delta;
				if (item->delay <= 0) {
					item->started = LgTrue;
					item->cinterval = item->interval;
				}
			}

			if (item->started) {
				item->cinterval -= delta;

				if (item->cinterval <= 0) {
					LgBool ret = LgTrue;

					if (item->step) ret = item->step(delta, item->userdata);
					item->cinterval = item->interval;

					if (!ret) removed = LgTrue;
					else {
						if (item->repeat != LG_LOOP_FOREVER) {
							--item->repeat;
							if (item->repeat <= 0) removed = LgTrue;
						}
					}
				}
			}

			if (!removed) item->prevTime = time;
		}

		if (removed) {
			if (item->destroyUserdata) {
				item->destroyUserdata(item->userdata);
				item->destroyUserdata = NULL;
			}
			LgSchedulerRemove(item);
		}
		
		item = next;
	}

	g_inUpdate = LgFalse;
	pthread_mutex_unlock(&g_mutex);
	
	if (g_newItems) {
		LgListNodeP it = LgListFirstNode(g_newItems);
		
		while(it) {
			LgSchedulerItemP item = (LgSchedulerItemP)LgListNodeValue(it);
			item->newItemsNode = NULL;
			addToList(item);
			g_count++;
			it = LgListNextNode(it);
		}
		
		LgListDestroy(g_newItems);
		g_newItems = NULL;
	}
}

void LgSchedulerPause(LgSchedulerItemP item) {
	if (!item->paused) {
		item->paused = LgTrue;
	}
}

void LgSchedulerResume(LgSchedulerItemP item) {
	if (item->paused) {
		item->paused = LgFalse;
	}
}

void LgSchedulerRemove(LgSchedulerItemP item) {
	if (item->newItemsNode) {
		LgListRemove(g_newItems, item->newItemsNode);
		item->newItemsNode = NULL;
		LgSchedulerItemRelease(item);
	} else {
		pthread_mutex_lock(&g_mutex);
		if (item->prev) item->prev->next = item->next;
		else g_first = item->next;
		if (item->next) item->next->prev = item->prev;
		else g_last = item->prev;
		pthread_mutex_unlock(&g_mutex);
		if (item->destroyUserdata) {
			item->destroyUserdata(item->userdata);
			item->destroyUserdata = NULL;
		}
		g_count--;
		LgSchedulerItemRelease(item);
	}
}

void LgSchedulerClear() {
	pthread_mutex_lock(&g_mutex);
	while(g_first) LgSchedulerRemove(g_first);
	g_count = 0;
	pthread_mutex_unlock(&g_mutex);
}

void LgSchedulerReleaseAtNextFrame(void *obj) {
	LgSchedulerRun(NULL, LgRelease, obj, 0, 1, 0, LgFalse);
}

int LgSchedulerCount() {
	return g_count;
}