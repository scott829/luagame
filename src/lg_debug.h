#ifndef _LG_DEBUG_H_
#define _LG_DEBUG_H_

#include "lg_basetypes.h"

LgBool LgDebugOpen();

void LgDebugClose();

LgBool LgCheckBreakpoint(const char *filename, int line);

void LgDebugProcessCommands();

#endif