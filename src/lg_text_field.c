﻿#include "lg_text_field.h"
#include "lg_label.h"
#include "lg_memoutputstream.h"
#include "lg_meminputstream.h"
#include "lg_drawnode.h"
#include "lg_action.h"
#include "lg_clipnode.h"

#define BLINK_SPEED 0.3f

typedef struct LgTextFieldData {
	struct LgVector2	size;
	LgNodeP				clipNode;
	LgNodeP				textNode;
	LgNodeP				compTextNode;
	LgNodeP				placeHolderNode;
	wchar_t *			text;
	int					textLength;
	LgNodeP				cursorNode;				// 光标图形
	LgColor				cursorColor;
	LgSlot				slotChange;
}*LgTextFieldDataP;

static float textOffset(LgTextFieldDataP td) {
	struct LgVector2 position;
	LgNodePosition(td->textNode, &position);
	return position.x;
}

static void redrawCursor(LgTextFieldDataP td) {
	LgFontP font;
	LgFontDataP fontdata;
	float height;
	
	font = LgLabelFont(td->textNode);
	fontdata = LgFontGetData(font);
	height = LgFontGetDriver(font)->height(fontdata, LgLabelFontSize(td->textNode));
	
	// 开始绘制光标
	LgDrawNodeSetColor(td->cursorNode, td->cursorColor);
	LgDrawNodeClear(td->cursorNode);
	LgDrawNodeRectangle(td->cursorNode, 0, 0, 4, (float)height);
}

static void updateCursorPosition(LgTextFieldDataP td) {
	if (td->cursorNode) {
		struct LgAabb textNodeBox, compTextNodeBox;
		struct LgVector2 vec;
		
		LgNodeContentBox(td->textNode, &textNodeBox);
		LgNodeContentBox(td->compTextNode, &compTextNodeBox);
		vec.x = textNodeBox.width + compTextNodeBox.width + textOffset(td), vec.y = 0;
		LgNodeSetPosition(td->cursorNode, &vec);
	}
}

static void buildTextNode(LgTextFieldDataP td) {
	if (td->textLength > 0) {
		LgOutputStreamP output;
		int i;
		
		output = LgMemOutputStreamCreate(NULL, 0);
		for (i = 0; i < td->textLength; i++) LgOutputStreamWriteRune(output, td->text[i]);
		LgOutputStreamWriteByte(output, 0);
		LgLabelSetText(td->textNode, (const char *)LgMemOutputStreamData(output));
		LgOutputStreamDestroy(output);
	} else {
		LgLabelSetText(td->textNode, NULL);
	}
}

static void setCompTextNode(LgTextFieldDataP td, const wchar_t *text, int len) {
	LgOutputStreamP output;
	int i;

	output = LgMemOutputStreamCreate(NULL, 0);
	for (i = 0; i < len; i++) LgOutputStreamWriteRune(output, text[i]);
	LgOutputStreamWriteByte(output, 0);
	LgLabelSetText(td->compTextNode, (const char *)LgMemOutputStreamData(output));
	LgOutputStreamDestroy(output);
}

static void relayoutCompTextNode(LgTextFieldDataP td) {
	struct LgAabb textNodeBox;
	struct LgVector2 vec;

	LgNodeContentBox(td->textNode, &textNodeBox);
	vec.x = textNodeBox.width + textOffset(td), vec.y = 0;
	LgNodeSetPosition(td->compTextNode, &vec);
}

static void setTextUtf8(LgNodeP node, LgTextFieldDataP td, const char *text) {
	if (td->text) {
		LgFree(td->text);
		td->text = NULL;
	}
	td->textLength = 0;

	if (text && strlen(text) > 0) {
		LgOutputStreamP output;
		LgInputStreamP input;
		wchar_t c;
		
		input = LgMemInputStreamCreate(text, strlen(text));
		output = LgMemOutputStreamCreate(NULL, 0);
		
		while(LgInputStreamReadRune(input, &c)) {
			LgOutputStreamWriteBytes(output, &c, sizeof(c));
		}
		
		LgTextFieldInsertText(node, (const wchar_t *)LgMemOutputStreamData(output), LgMemOutputStreamLen(output) / sizeof(c));
		LgInputStreamDestroy(input);
		LgOutputStreamDestroy(output);
		if (td->placeHolderNode) LgNodeSetVisible(td->placeHolderNode, LgFalse);
	} else {
		buildTextNode(td);
		if (td->placeHolderNode) LgNodeSetVisible(td->placeHolderNode, LgTrue);
	}
}

static void scrollRight(LgTextFieldDataP td) {
	struct LgAabb aabbText, aabbComp;
	LgNodeContentBox(td->textNode, &aabbText);
	LgNodeContentBox(td->compTextNode, &aabbComp);

	if (aabbText.width + aabbComp.width + textOffset(td) > td->size.x) {
		// 超出文本框范围了，需要向右滚动
		float offsetw = aabbText.width + aabbComp.width + textOffset(td) - td->size.x;
		struct LgVector2 position;
		LgNodePosition(td->textNode, &position);
		position.x -= offsetw + 10;
		LgNodeSetPosition(td->textNode, &position);
	}
}

static void scrollLeft(LgTextFieldDataP td) {
	struct LgAabb aabbText, aabbComp;
	LgNodeContentBox(td->textNode, &aabbText);
	LgNodeContentBox(td->compTextNode, &aabbComp);

	if (aabbText.width + aabbComp.width + textOffset(td) < td->size.x / 4) {
		// 超出文本框范围了，需要向右滚动
		float offsetw = aabbText.width + aabbComp.width + textOffset(td);
		struct LgVector2 position;
		LgNodePosition(td->textNode, &position);
		position.x -= offsetw - td->size.x / 2;
		position.x = LgMin(position.x, 0);
		LgNodeSetPosition(td->textNode, &position);
	}
}

static void textFieldEventProc(LgEventType type, LgNodeP node, void *param) {
	switch(type) {
	case LG_NODE_EVENT_Destroy:
		{
			LgTextFieldDataP td = (LgTextFieldDataP)LgNodeUserData(node);
			if (td->textNode) LgNodeRelease(td->textNode);
			if (td->compTextNode) LgNodeRelease(td->compTextNode);
			if (td->placeHolderNode) LgNodeRelease(td->placeHolderNode);
			if (td->cursorNode) LgNodeRelease(td->cursorNode);
			if (td->text) LgFree(td->text);
			LgSlotClear(&td->slotChange);
			LgFree(td);
		}
		break;
	case LG_TEXTFIELD_EVENT_InsertText:
		{
			LgTextFieldDataP td = (LgTextFieldDataP)LgNodeUserData(node);
			LgTextFieldEventInsertTextP insertText = (LgTextFieldEventInsertTextP)param;

			if (insertText->textLength > 0) {
				if (LgLabelTextLength(td->compTextNode) > 0) {
					// 有没有组合完成的文本，清除它
					LgLabelSetText(td->compTextNode, NULL);
				}

				td->text = (wchar_t *)LgRealloc(td->text, sizeof(wchar_t) * (td->textLength + insertText->textLength));
				LgCopyMemory(td->text + td->textLength, insertText->text, sizeof(wchar_t) * insertText->textLength);
				td->textLength += insertText->textLength;
				buildTextNode(td);
				if (td->placeHolderNode) LgNodeSetVisible(td->placeHolderNode, LgFalse);
				scrollRight(td);
				relayoutCompTextNode(td);
				updateCursorPosition(td);
				LgSlotCall(&td->slotChange, NULL);
			}
		}
		break;
	case LG_TEXTFIELD_EVENT_CompText:
		{
			LgTextFieldDataP td = (LgTextFieldDataP)LgNodeUserData(node);
			LgTextFieldEventCompTextP compText = (LgTextFieldEventCompTextP)param;
			int oldCompLen = LgLabelTextLength(td->compTextNode);
			setCompTextNode(td, compText->text, compText->textLength);
			if (td->placeHolderNode) LgNodeSetVisible(td->placeHolderNode, LgFalse);
			if (LgLabelTextLength(td->compTextNode) > oldCompLen) scrollRight(td);
			else scrollLeft(td);
			relayoutCompTextNode(td);
			updateCursorPosition(td);
		}
		break;
	case LG_TEXTFIELD_EVENT_DeleteChar:
		{
			LgTextFieldDataP td = (LgTextFieldDataP)LgNodeUserData(node);
			if (td->textLength > 0) {
				td->textLength--;
				buildTextNode(td);
				if (td->textLength == 0) {
					if (td->placeHolderNode) LgNodeSetVisible(td->placeHolderNode, LgTrue);
				}
				scrollLeft(td);
				relayoutCompTextNode(td);
				updateCursorPosition(td);
				LgSlotCall(&td->slotChange, NULL);
			}
		}
		break;
	case LG_TEXTFIELD_EVENT_SetFocus:
		{
			LgTextFieldDataP td = (LgTextFieldDataP)LgNodeUserData(node);
			LgActionP action, fade[2];
			
			updateCursorPosition(td);
			LgNodeSetVisible(td->cursorNode, LgTrue);
			
			fade[0] = LgActionFadeInCreate(BLINK_SPEED);
			fade[1] = LgActionFadeOutCreate(BLINK_SPEED);
			action = LgActionLoopForeverCreate(LgActionSequenceCreate(fade, 2));
			LgActionRelease(fade[0]);
			LgActionRelease(fade[1]);
			
			LgNodeDoAction(td->cursorNode, action);
			LgActionRelease(action);
		}
		break;
	case LG_TEXTFIELD_EVENT_KillFocus:
		{
			LgTextFieldDataP td = (LgTextFieldDataP)LgNodeUserData(node);
			LgNodeStopActions(td->cursorNode);
			LgNodeSetVisible(td->cursorNode, LgFalse);
		}
		break;
	case LG_TEXTFIELD_EVENT_SetFontName:
		{
			LgTextFieldDataP td = (LgTextFieldDataP)LgNodeUserData(node);
			LgLabelSetFontName(td->textNode, (const char *)param);
			LgLabelSetFontName(td->compTextNode, (const char *)param);
			if (td->placeHolderNode) LgLabelSetFontName(td->placeHolderNode, (const char *)param);
			redrawCursor(td);
			relayoutCompTextNode(td);
		}
		break;
	case LG_TEXTFIELD_EVENT_FontName:
		{
			LgTextFieldDataP td = (LgTextFieldDataP)LgNodeUserData(node);
			*((const char **)param) = LgLabelFontName(td->textNode);
		}
		break;
	case LG_TEXTFIELD_EVENT_SetFontSize:
		{
			LgTextFieldDataP td = (LgTextFieldDataP)LgNodeUserData(node);
			LgLabelSetFontSize(td->textNode, *((int *)param));
			LgLabelSetFontSize(td->compTextNode, *((int *)param));
			if (td->placeHolderNode) LgLabelSetFontSize(td->placeHolderNode, *((int *)param));
			redrawCursor(td);
			relayoutCompTextNode(td);
		}
		break;
	case LG_TEXTFIELD_EVENT_FontSize:
		{
			LgTextFieldDataP td = (LgTextFieldDataP)LgNodeUserData(node);
			*((int *)param) = LgLabelFontSize(td->textNode);
		}
		break;
	case LG_TEXTFIELD_EVENT_SetColor:
		{
			LgTextFieldDataP td = (LgTextFieldDataP)LgNodeUserData(node);
			LgLabelSetColor(td->textNode, *((LgColor *)param));
		}
		break;
	case LG_TEXTFIELD_EVENT_Color:
		{
			LgTextFieldDataP td = (LgTextFieldDataP)LgNodeUserData(node);
			*((LgColor *)param) = LgLabelColor(td->textNode);
		}
		break;
	case LG_TEXTFIELD_EVENT_SetText:
		{
			LgTextFieldDataP td = (LgTextFieldDataP)LgNodeUserData(node);
			setTextUtf8(node, td, (const char *)param);
		}
		break;
	case LG_TEXTFIELD_EVENT_Text:
		{
			LgTextFieldDataP td = (LgTextFieldDataP)LgNodeUserData(node);
			LgTextFieldEventGetTextP getText = (LgTextFieldEventGetTextP)param;
			LgLabelText(td->textNode, getText->buf, getText->bufLength);
		}
		break;
	case LG_TEXTFIELD_EVENT_TextLength:
		{
			LgTextFieldDataP td = (LgTextFieldDataP)LgNodeUserData(node);
			*((int *)param) = LgLabelTextLength(td->textNode);
		}
		break;
	case LG_TEXTFIELD_EVENT_SetCompColor:
		{
			LgTextFieldDataP td = (LgTextFieldDataP)LgNodeUserData(node);
			LgLabelSetColor(td->compTextNode, *((LgColor *)param));
		}
		break;
	case LG_TEXTFIELD_EVENT_CompColor:
		{
			LgTextFieldDataP td = (LgTextFieldDataP)LgNodeUserData(node);
			*((LgColor *)param) = LgLabelColor(td->compTextNode);
		}
		break;
	case LG_TEXTFIELD_EVENT_SetPlaceHolderColor:
		{
			LgTextFieldDataP td = (LgTextFieldDataP)LgNodeUserData(node);
			if (td->placeHolderNode) LgLabelSetColor(td->placeHolderNode, *((LgColor *)param));
		}
		break;
	case LG_TEXTFIELD_EVENT_PlaceHolderColor:
		{
			LgTextFieldDataP td = (LgTextFieldDataP)LgNodeUserData(node);
			*((LgColor *)param) = td->placeHolderNode ? LgLabelColor(td->placeHolderNode) : LgColorXRGB(255, 255, 255);
		}
		break;
	case LG_TEXTFIELD_EVENT_SetCursorColor:
		{
			LgTextFieldDataP td = (LgTextFieldDataP)LgNodeUserData(node);
			td->cursorColor = *((LgColor *)param);
			redrawCursor(td);
		}
		break;
	case LG_TEXTFIELD_EVENT_CursorColor:
		{
			LgTextFieldDataP td = (LgTextFieldDataP)LgNodeUserData(node);
			*((LgColor *)param) = td->cursorColor;
		}
		break;
	case LG_NODE_EVENT_ClearSlots:
		{
			LgTextFieldDataP td = (LgTextFieldDataP)LgNodeUserData(node);
			LgSlotClear(&td->slotChange);
		}
		break;
	case LG_NODE_EVENT_ContentBox:
		{
			LgTextFieldDataP td = (LgTextFieldDataP)LgNodeUserData(node);
			LgAabbP contentBox = (LgAabbP)param;
			contentBox->x = contentBox->y = 0;
			contentBox->width = td->size.x, contentBox->height = td->size.y;
		}
		return;
	}
	
	LgNodeDefEventProc(type, node, param);
}

LgNodeP LgTextFieldCreateR(const LgVector2P size, const char *text, const char *placeHolder) {
	LgNodeP node, clipMask;
	LgTextFieldDataP td;

	node = LgNodeCreateR((LgEvent)textFieldEventProc);
	LG_CHECK_ERROR(node);

	td = (LgTextFieldDataP)LgMallocZ(sizeof(struct LgTextFieldData));
	LG_CHECK_ERROR(td);
	LgCopyMemory(&td->size, size, sizeof(struct LgVector2));
	LgNodeSetUserData(node, td);

	td->cursorColor = LgColorXRGB(255, 0, 0);

	// 创建裁剪节点
	td->clipNode = LgClipNodeCreateR();
	LgNodeAddChild(node, td->clipNode);
	clipMask = LgDrawNodeCreateR();
	LgClipNodeSetStencil(td->clipNode, clipMask);
	LgNodeRelease(clipMask);
	LgDrawNodeRectangle(clipMask, 0, 0, size->x, size->y);
	
	// 创建文本节点
	td->textNode = LgLabelCreateR(NULL);
	td->compTextNode = LgLabelCreateR(NULL);
	LgNodeAddChild(td->clipNode , td->textNode);
	LgNodeAddChild(td->clipNode , td->compTextNode);
	
	if (placeHolder) {
		td->placeHolderNode = LgLabelCreateR(NULL);
		LgNodeAddChild(td->clipNode , td->placeHolderNode);
		LgLabelSetText(td->placeHolderNode, placeHolder);
	}

	setTextUtf8(node, td, text);

	// 创建光标节点
	td->cursorNode = LgDrawNodeCreateR();
	redrawCursor(td);
	LgNodeAddChild(td->clipNode , td->cursorNode);
	LgNodeSetVisible(td->cursorNode, LgFalse);

	LgNodeEnableHitTest(node, LgTrue);
	LgNodeCenterTransformAnchor(node);

	return node;

_error:
	if (node) LgNodeRelease(node);
	return NULL;
}

LgBool LgNodeIsTextField(LgNodeP node) {
	return LgNodeEventProc(node) == (LgEvent)textFieldEventProc;
}

void LgTextFieldInsertText(LgNodeP node, const wchar_t *text, int len) {
	struct LgTextFieldEventInsertText insertText;
	insertText.text = text;
	insertText.textLength = len;
	LgNodeDoEvent(LG_TEXTFIELD_EVENT_InsertText, node, &insertText);
}

void LgTextFieldCompText(LgNodeP node, const wchar_t *text, int len) {
	struct LgTextFieldEventCompText compText;
	compText.text = text;
	compText.textLength = len;
	LgNodeDoEvent(LG_TEXTFIELD_EVENT_CompText, node, &compText);
}

void LgTextFieldDeleteChar(LgNodeP node) {
	LgNodeDoEvent(LG_TEXTFIELD_EVENT_DeleteChar, node, NULL);
}

void LgTextFieldSetFocus(LgNodeP node) {
	LgNodeDoEvent(LG_TEXTFIELD_EVENT_SetFocus, node, NULL);
}

void LgTextFieldKillFocus(LgNodeP node) {
	LgNodeDoEvent(LG_TEXTFIELD_EVENT_KillFocus, node, NULL);
}

void LgTextFieldSetFontName(LgNodeP node, const char *fontname) {
	LgNodeDoEvent(LG_TEXTFIELD_EVENT_SetFontName, node, (void *)fontname);
}

const char *LgTextFieldFontName(LgNodeP node) {
	char *name;
	LgNodeDoEvent(LG_TEXTFIELD_EVENT_FontName, node, (void *)&name);
	return name;
}

void LgTextFieldSetFontSize(LgNodeP node, int fontsize) {
	LgNodeDoEvent(LG_TEXTFIELD_EVENT_SetFontSize, node, &fontsize);
}

int LgTextFieldFontSize(LgNodeP node) {
	int size;
	LgNodeDoEvent(LG_TEXTFIELD_EVENT_FontSize, node, &size);
	return size;
}

void LgTextFieldSetColor(LgNodeP node, LgColor color) {
	LgNodeDoEvent(LG_TEXTFIELD_EVENT_SetColor, node, &color);
}

LgColor LgTextFieldColor(LgNodeP node) {
	LgColor color;
	LgNodeDoEvent(LG_TEXTFIELD_EVENT_Color, node, &color);
	return color;
}

void LgTextFieldSetText(LgNodeP node, const char *text) {
	LgNodeDoEvent(LG_TEXTFIELD_EVENT_SetText, node, (void *)text);
}

int LgTextFieldTextLength(LgNodeP node) {
	int len;
	LgNodeDoEvent(LG_TEXTFIELD_EVENT_TextLength, node, &len);
	return len;
}

void LgTextFieldText(LgNodeP node, char *buf, int bufLen) {
	struct LgTextFieldEventGetText getText;
	getText.buf = buf;
	getText.bufLength = bufLen;
	LgNodeDoEvent(LG_TEXTFIELD_EVENT_Text, node, &getText);
}

void LgTextFieldSetCompColor(LgNodeP node, LgColor color) {
	LgNodeDoEvent(LG_TEXTFIELD_EVENT_SetCompColor, node, &color);
}

LgColor LgTextFieldCompColor(LgNodeP node) {
	LgColor color;
	LgNodeDoEvent(LG_TEXTFIELD_EVENT_CompColor, node, &color);
	return color;
}

void LgTextFieldSetPlaceHolderColor(LgNodeP node, LgColor color) {
	LgNodeDoEvent(LG_TEXTFIELD_EVENT_SetPlaceHolderColor, node, &color);
}

LgColor LgTextFieldPlaceHolderColor(LgNodeP node) {
	LgColor color;
	LgNodeDoEvent(LG_TEXTFIELD_EVENT_PlaceHolderColor, node, &color);
	return color;
}

void LgTextFieldSetCursorColor(LgNodeP node, LgColor color) {
	LgNodeDoEvent(LG_TEXTFIELD_EVENT_SetCursorColor, node, &color);
}

LgColor LgTextFieldCursorColor(LgNodeP node) {
	LgColor color;
	LgNodeDoEvent(LG_TEXTFIELD_EVENT_CursorColor, node, &color);
	return color;
}

LgSlot *LgTextFieldSlotChange(LgNodeP node) {
	LgTextFieldDataP td = (LgTextFieldDataP)LgNodeUserData(node);
	return &td->slotChange;
}
