﻿#ifndef _LG_SOUNDPLAYER_H
#define _LG_SOUNDPLAYER_H

#include "lg_basetypes.h"
#include "lg_refcnt.h"

typedef unsigned long LgSoundId;

typedef struct LgSoundData *LgSoundDataP;

LG_REFCNT_FUNCTIONS_DECL(LgSoundData)

LgSoundDataP LgSoundDataCreateR(const char *filename);

LgBool LgSoundPlayerOpen();

void LgSoundPlayerClose();

LgBool LgSoundPlayerPlayBackgroundMusic(const char *filename, SLOT_PARAMS);

void LgSoundPlayerStopBackgroundMusic();

LgSoundId LgSoundPlayerPlayEffect(const char *filename);

void LgSoundPlayerStopEffect(LgSoundId id);

void LgSoundPlayerStopAllEffects();

void LgSoundPlayerUpdate();

float LgSoundPlayerMusicGain();

void LgSoundPlayerSetMusicGain(float gain);

float LgSoundPlayerEffectGain();

void LgSoundPlayerSetEffectGain(float gain);

#endif