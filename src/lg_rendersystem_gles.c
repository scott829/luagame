﻿#include "lg_rendersystem.h"
#include "lg_memory.h"
#include "lg_blit.h"
#include "lg_imagecopy.h"
#if defined(ANDROID)
#include "lg_glescontext_android.h"
#elif defined(IOS)
#include "lg_glescontext_ios.h"
#endif
#include "shaders/gles_shaders.h"

enum LgOpenGLAttrib {
	LgOpenGLAttribPosition = 0,
	LgOpenGLAttribDiffuse,
	LgOpenGLAttribTexture,
	LgOpenGLAttribMAX,
};

enum LgOpenGLUniformId {
	LgOpenGLUniformMVPMatrix = 0,
	LgOpenGLUniformCxformMul,
	LgOpenGLUniformCxformAdd,
	LgOpenGLUniformTexture,
	LgOpenGLUniformMAX,
};

typedef struct LgOpenGLShader {
	GLuint fsh, vsh, prog;
	GLint uniformPosition[LgOpenGLUniformMAX];
	GLint attribPosition[LgOpenGLAttribMAX];
}*LgOpenGLShaderP;

struct LgVertexBufferGL {
	char *	data;
	int		size;
};

struct LgIndexBufferGL {
	char *	data;
	int		count;
};

struct LgTextureGL {
	LG_REFCNT_HEAD
	enum LgImageFormat_	format;
	GLuint				id;
	struct LgSize		size;
};

struct LgRenderTargetGL {
	GLuint				fbo, depth;
	struct LgTextureGL *texture;
};

static struct LgMatrix44 g_viewMatrix, g_worldMatrix, g_projMatrix, g_mvp;
static struct LgCxform g_cxform;
struct LgOpenGLShader g_shaderDiffuseCxform, g_shaderTextureCxform,
	g_shaderDiffuseTextureCxform, g_shaderCopyRenderTarget;
static LgOpenGLShaderP g_currentShader;
static LgBool g_renderInTexture;
static GLuint g_currentTexture;

static void openglSetCxform(const LgCxformP cxform);
static void openglVertexBufferDestroy(LgVertexBufferP vb);
static void openglIndexBufferDestroy(LgIndexBufferP ib);
static void openglRenderTargetDestroy(LgRenderTargetP target);

static void destroyShader(LgOpenGLShaderP shader) {
	glDeleteShader(shader->vsh);
	glDeleteShader(shader->fsh);
	glDeleteProgram(shader->prog);
}

static LgBool initShader(LgOpenGLShaderP shader, const char *vsh, const char *fsh) {
	GLint status;
	
	shader->prog = glCreateProgram();

	if (vsh) {
		shader->vsh = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(shader->vsh, 1, &vsh, NULL);
		glCompileShader(shader->vsh);
		glGetShaderiv(shader->vsh, GL_COMPILE_STATUS, &status);
		if (status == GL_FALSE) {
			return LgFalse;
		}
	}

	shader->fsh = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(shader->fsh, 1, &fsh, NULL);
	glCompileShader(shader->fsh);
	glGetShaderiv(shader->fsh, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE) {
		return LgFalse;
	}

	glAttachShader(shader->prog, shader->vsh);
	glAttachShader(shader->prog, shader->fsh);
	glLinkProgram(shader->prog);
	glGetProgramiv(shader->prog, GL_LINK_STATUS, &status);
	if (status == GL_FALSE) {
		return LgFalse;
	}

	glValidateProgram(shader->prog);
	glGetShaderiv(shader->prog, GL_VALIDATE_STATUS, &status);
	if (status == GL_FALSE) {
		return LgFalse;
	}

	shader->uniformPosition[LgOpenGLUniformMVPMatrix] = glGetUniformLocation(shader->prog, "dz_MVPMatrix");
	shader->uniformPosition[LgOpenGLUniformCxformMul] = glGetUniformLocation(shader->prog, "dz_cxformMul");
	shader->uniformPosition[LgOpenGLUniformCxformAdd] = glGetUniformLocation(shader->prog, "dz_cxformAdd");
	shader->uniformPosition[LgOpenGLUniformTexture] = glGetUniformLocation(shader->prog, "dz_texture");

	shader->attribPosition[LgOpenGLAttribPosition] = glGetAttribLocation(shader->prog, "dz_position");
	shader->attribPosition[LgOpenGLAttribDiffuse] = glGetAttribLocation(shader->prog, "dz_color");
	shader->attribPosition[LgOpenGLAttribTexture] = glGetAttribLocation(shader->prog, "dz_texcrood");
	return LgTrue;
}

static void openglClose() {
	destroyShader(&g_shaderDiffuseCxform);
	destroyShader(&g_shaderDiffuseTextureCxform);
	destroyShader(&g_shaderTextureCxform);
	destroyShader(&g_shaderCopyRenderTarget);
	LgGlContextClose();
}

static LgBool initShaders() {
	return initShader(&g_shaderDiffuseCxform, opengl_shaderDiffuseCxform_vsh, opengl_shaderDiffuseCxform_fsh) &&
		initShader(&g_shaderTextureCxform, opengl_shaderTextureCxform_vsh, opengl_shaderTextureCxform_fsh) &&
		initShader(&g_shaderDiffuseTextureCxform, opengl_shaderDiffuseTextureCxform_vsh, opengl_shaderDiffuseTextureCxform_fsh) &&
		initShader(&g_shaderCopyRenderTarget, opengl_shaderCopyRenderTarget_vsh, opengl_shaderCopyRenderTarget_fsh);
}

static LgBool openglOpen() {
	struct LgCxform defaultCxform;

	LG_CHECK_ERROR(LgGlContextOpen());
	LG_CHECK_ERROR(initShaders());

	g_renderInTexture = LgFalse;
	g_currentTexture = -1;

	// 关闭深度测试
	glDisable(GL_DEPTH_TEST);
	
	// 关闭模板测试
	glDisable(GL_STENCIL_TEST);

	// 设置混合方式
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// 关闭背面拣选
	glDisable(GL_CULL_FACE);

	// 初始化颜色变换矩阵
	LgCxformIndentity(&defaultCxform);
	openglSetCxform(&defaultCxform);

	LgMatrix44Indentity(&g_worldMatrix);
	LgMatrix44Indentity(&g_viewMatrix);
	LgMatrix44Indentity(&g_projMatrix);
	return LgTrue;

_error:
	openglClose();
	return LgFalse;
}

static void openglSetTransform(enum LgTransformType type, const LgMatrix44P m) {
	switch(type) {
	case LgTransformWorld:
		LgCopyMemory(&g_worldMatrix, m, sizeof(struct LgMatrix44));
		break;
	case LgTransformProjection:
		LgCopyMemory(&g_projMatrix, m, sizeof(struct LgMatrix44));
		break;
	case LgTransformView:
		LgCopyMemory(&g_viewMatrix, m, sizeof(struct LgMatrix44));
		break;
	}
}

static void openglGetTransform(enum LgTransformType type, LgMatrix44P m) {
	switch(type) {
	case LgTransformWorld:
		LgCopyMemory(m, &g_worldMatrix, sizeof(struct LgMatrix44));
		break;
	case LgTransformProjection:
		LgCopyMemory(m, &g_projMatrix, sizeof(struct LgMatrix44));
		break;
	case LgTransformView:
		LgCopyMemory(m, &g_viewMatrix, sizeof(struct LgMatrix44));
		break;
	}
}

static void openglSetCxform(const LgCxformP cxform) {
	LgCopyMemory(&g_cxform, cxform, sizeof(struct LgCxform));
}

static void openglGetCxform(LgCxformP cxform) {
	LgCopyMemory(cxform, &g_cxform, sizeof(struct LgCxform));
}

LgVertexBufferP openglCreateVertexBuffer(int size) {
	struct LgVertexBufferGL *vb = (struct LgVertexBufferGL *)LgMallocZ(sizeof(struct LgVertexBufferGL));
	LG_CHECK_ERROR(vb);
	vb->data = (char *)LgMalloc(size);
	LG_CHECK_ERROR(vb->data);
	vb->size = size;
	return vb;

_error:
	openglVertexBufferDestroy(vb);
	return NULL;
}

LgIndexBufferP openglCreateIndexBuffer(int count) {
	struct LgIndexBufferGL *ib = (struct LgIndexBufferGL *)LgMallocZ(sizeof(struct LgIndexBufferGL));
	LG_CHECK_ERROR(ib);
	ib->data = (char *)LgMalloc(count * sizeof(unsigned short));
	LG_CHECK_ERROR(ib->data);
	ib->count = count;
	return ib;

_error:
	openglIndexBufferDestroy(ib);
	return NULL;
}

static void destroyTexture(LgTextureP texture) {
	struct LgTextureGL *textureopengl = (struct LgTextureGL *)texture;
	if (textureopengl->id) glDeleteTextures(1, &textureopengl->id);
	LgFree(textureopengl);
}

static LgTextureP openglCreateTextureR(enum LgImageFormat_ format, const LgSizeP size) {
	struct LgTextureGL *texture;

	texture = (struct LgTextureGL *)LgMallocZ(sizeof(struct LgTextureGL));
	LG_CHECK_ERROR(texture);
	LG_REFCNT_INIT(texture, destroyTexture);
	texture->format = format;
	LgCopyMemory(&texture->size, size, sizeof(struct LgSize));

	glGenTextures(1, &texture->id);
	glBindTexture(GL_TEXTURE_2D, texture->id);

	switch(format) {
	case LgImageFormatL8:
		glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, size->width, size->height, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, NULL);
		break;
	case LgImageFormatA8L8:
		glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE_ALPHA, size->width, size->height, 0, GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, NULL);
		break;
	case LgImageFormatA8R8G8B8:
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size->width, size->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
		break;
	default:
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size->width, size->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
		break;
	}
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	return texture;

_error:
	if (texture) LgTextureRelease(texture);
	return NULL;
}

static void buildMvp() {
	LgCopyMemory(&g_mvp, &g_worldMatrix, sizeof(struct LgMatrix44));
	LgMatrix44Mul(&g_mvp, &g_viewMatrix);
	LgMatrix44Mul(&g_mvp, &g_projMatrix);
}

static void applyUniform() {
	if (g_currentShader) {
		buildMvp();
		glUniformMatrix4fv(g_currentShader->uniformPosition[LgOpenGLUniformMVPMatrix], 1, GL_FALSE, (const float *)&g_mvp);
		glUniform4f(g_currentShader->uniformPosition[LgOpenGLUniformCxformMul], g_cxform.mul[0], g_cxform.mul[1], g_cxform.mul[2], g_cxform.mul[3]);
		glUniform4f(g_currentShader->uniformPosition[LgOpenGLUniformCxformAdd], g_cxform.add[0], g_cxform.add[1], g_cxform.add[2], g_cxform.add[3]);

		if (g_currentTexture != -1) {
			glEnable(GL_TEXTURE_2D);
			glActiveTexture(0);
			glBindTexture(GL_TEXTURE_2D, g_currentTexture);
			glUniform1i(g_currentShader->uniformPosition[LgOpenGLUniformTexture], 0);
		} else {
			glBindTexture(GL_TEXTURE_2D, 0);
			glDisable(GL_TEXTURE_2D);
		}
	}
}

static int getStride(int fvf) {
	int size = 0;

	if (fvf & LgFvfXYZ) size += sizeof(float) * 3;
	if (fvf & LgFvfDiffuse) size += sizeof(LgColor);
	if (fvf & LgFvfTexture) size += sizeof(float) * 2;
	return size;
}

static void setGLArrays(int fvf, const char *data) {
	int offset = 0;
	int stride = getStride(fvf), p;

	if (!g_currentShader) return;

	p = g_currentShader->attribPosition[LgOpenGLAttribPosition];
	if (fvf & LgFvfXYZ) {
		glEnableVertexAttribArray(p);
		glVertexAttribPointer(p, 3, GL_FLOAT, GL_FALSE, stride, data + offset);
		offset += sizeof(float) * 3;
	} else {
		glDisableVertexAttribArray(p);
	}

	p = g_currentShader->attribPosition[LgOpenGLAttribDiffuse];
	if (fvf & LgFvfDiffuse) {
		glEnableVertexAttribArray(p);
		glVertexAttribPointer(p, 4, GL_UNSIGNED_BYTE, GL_TRUE, stride, data + offset);
		offset += sizeof(LgColor);
	} else {
		glDisableVertexAttribArray(p);
	}

	p = g_currentShader->attribPosition[LgOpenGLAttribTexture];
	if (fvf & LgFvfTexture) {
		glEnableVertexAttribArray(p);
		glVertexAttribPointer(p, 2, GL_FLOAT, GL_FALSE, stride, data + offset);
		offset += sizeof(float) * 2;
	} else {
		glDisableVertexAttribArray(p);
	}
}

static GLenum getPrimitiveType(enum LgPrimitiveType type) {
	switch(type) {
	case LgPrimitiveTriangleList: return GL_TRIANGLES;
	case LgPrimitiveTriangleStrip: return GL_TRIANGLE_STRIP;
	case LgPrimitiveTriangleFan: return GL_TRIANGLE_FAN;
	default: return GL_TRIANGLES;
	}
}

static GLenum getVertexCount(enum LgPrimitiveType type, int count) {
	switch(type) {
	case LgPrimitiveTriangleList: return count * 3;
	case LgPrimitiveTriangleStrip: return count + 2;
	case LgPrimitiveTriangleFan: return count + 2;
	default: return 0;
	}
}

static void openglDrawPrimitive(enum LgPrimitiveType type, int fvf, LgVertexBufferP vb, int primitiveCount) {
	applyUniform();
	setGLArrays(fvf, (const char *)((struct LgVertexBufferGL *)vb)->data);
	glDrawArrays(getPrimitiveType(type), 0, getVertexCount(type, primitiveCount));
}

static void openglDrawIndexPrimitive(enum LgPrimitiveType type, int fvf, LgVertexBufferP vb, LgIndexBufferP ib, int primitiveCount, int vertexCount) {
	applyUniform();
	setGLArrays(fvf, (const char *)((struct LgVertexBufferGL *)vb)->data);
	glDrawElements(getPrimitiveType(type), getVertexCount(type, primitiveCount), GL_UNSIGNED_SHORT, ((struct LgIndexBufferGL *)ib)->data);
}

static void openglBegin(LgRenderTargetP target) {
	if (target) {
		struct LgRenderTargetGL *targetopengl = (struct LgRenderTargetGL *)target;
		glBindFramebuffer(GL_FRAMEBUFFER, targetopengl->fbo);
		glViewport(0, 0, targetopengl->texture->size.width, targetopengl->texture->size.height);
		g_renderInTexture = LgTrue;
	} else {
		struct LgSize windowSize;
		
		LgGlContextBegin();
		LgWindowSize(&windowSize);
		glViewport(0, 0, windowSize.width, windowSize.height);
	}
}

static void openglEnd() {
	if (g_renderInTexture) {
		struct LgSize windowSize;
		
		LgWindowSize(&windowSize);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		g_renderInTexture = LgFalse;
		glViewport(0, 0, windowSize.width, windowSize.height);
	}
	else {
		glFlush();
		LgGlContextEnd();
	}
}

static void openglClear(enum LgClearFlag flags, LgColor color, int stencil) {
	GLbitfield glFlags = 0;

	if (flags & LgClearFlagColor) {
		glClearColor(LgColorRed(color) / 255.0f, LgColorGreen(color) / 255.0f, 
			LgColorBlue(color) / 255.0f, LgColorAlpha(color) / 255.0f);
		glFlags |= GL_COLOR_BUFFER_BIT;
	}
	
	if (flags & LgClearFlagStencil) {
		glClearStencil(stencil);
		glFlags |= GL_STENCIL_BUFFER_BIT;
	}
	
	if (flags & LgClearFlagDepth) {
		glClearDepthf(1024);
		glFlags |= GL_DEPTH_BUFFER_BIT;
	}

	glClear(glFlags);
}

static void openglSetTexture(LgTextureP texture) {
	g_currentTexture = texture ? ((struct LgTextureGL * )texture)->id : -1;
}

static void openglEnableStencil(LgBool value) {
	if (value) glEnable(GL_STENCIL_TEST);
	else glDisable(GL_STENCIL_TEST);
}

static GLenum openglCmp(enum LgStencilCompare cmp) {
	switch(cmp)
	{
	case LgStencilCompareNever: return GL_NEVER;
	case LgStencilCompareLess: return GL_LESS;
	case LgStencilCompareEqual: return GL_EQUAL;
	case LgStencilCompareLessEqual: return GL_LEQUAL;
	case LgStencilCompareGreater: return GL_GREATER;
	case LgStencilCompareNotEqual: return GL_NOTEQUAL;
	case LgStencilCompareGreaterEqual: return GL_GEQUAL;
	case LgStencilCompareAlways: return GL_ALWAYS;
	default: return GL_NEVER;
	}
}

static GLenum openglStencilOp(enum LgStencilOp op) {
	switch(op) {
	case LgStencilOpKeep: return GL_KEEP;
	case LgStencilOpZero: return GL_ZERO;
	case LgStencilOpReplace: return GL_REPLACE;
	case LgStencilOpIncrsat: return GL_INCR;
	case LgStencilOpDecrsat: return GL_DECR;
	case LgStencilOpInvert: return GL_INVERT;
	case LgStencilOpIncr: return GL_INCR_WRAP;
	case LgStencilOpDecr: return GL_DECR_WRAP;
	default: return GL_KEEP;
	}
}

static void openglApplyStencil(int reference, enum LgStencilCompare cmp, enum LgStencilOp passOp, enum LgStencilOp failOp) {
	glStencilFunc(openglCmp(cmp), reference, 0xffffffff);
	glStencilOp(0, openglStencilOp(failOp), openglStencilOp(passOp));
}

static LgRenderTargetP openglCreateRenderTarget(const LgSizeP size) {
	struct LgRenderTargetGL *target;

	target = (struct LgRenderTargetGL *)LgMallocZ(sizeof(struct LgRenderTargetGL));
	LG_CHECK_ERROR(target);

	glGenFramebuffers(1, &target->fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, target->fbo);

	target->texture = (struct LgTextureGL *)openglCreateTextureR(LgImageFormatA8R8G8B8, size);
	LG_CHECK_ERROR(target->texture);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, target->texture->id, 0);

	glGenRenderbuffers(1, &target->depth);
	glBindRenderbuffer(GL_RENDERBUFFER, target->depth);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8_OES, size->width, size->height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, target->depth);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, target->depth);
	return target;

_error:
	if (target) openglRenderTargetDestroy(target);
	return NULL;
}

static void openglSetPixelShader(enum LgPixelShaderType shaderType) {
	switch(shaderType) {
	case LgPixelShaderNone:
		glUseProgram(0);
		g_currentShader = NULL;
		break;
	case LgPixelShaderCxformDiffuse:
		glUseProgram(g_shaderDiffuseCxform.prog);
		g_currentShader = &g_shaderDiffuseCxform;
		break;
	case LgPixelShaderCxformTexture:
		glUseProgram(g_shaderTextureCxform.prog);
		g_currentShader = &g_shaderTextureCxform;
		break;
	case LgPixelShaderCxformDiffuseAndTexture:
		glUseProgram(g_shaderDiffuseTextureCxform.prog);
		g_currentShader = &g_shaderDiffuseTextureCxform;
		break;
	case LgPixelShaderCopyRenderTarget:
		glUseProgram(g_shaderCopyRenderTarget.prog);
		g_currentShader = &g_shaderCopyRenderTarget;
		break;
	}
}

static void openglVertexBufferDestroy(LgVertexBufferP vb) {
	struct LgVertexBufferGL *vbgl = (struct LgVertexBufferGL *)vb;
	if (vbgl->data) LgFree(vbgl->data);
	LgFree(vbgl);
}

static void *openglVertexBufferLock(LgVertexBufferP vb) {
	struct LgVertexBufferGL *vbgl = (struct LgVertexBufferGL *)vb;
	return vbgl->data;
}

static void openglVertexBufferUnlock(LgVertexBufferP vb) {
}

static int openglVertexBufferSize(LgVertexBufferP vb) {
	struct LgVertexBufferGL *vbgl = (struct LgVertexBufferGL *)vb;
	return vbgl->size;
}

static void openglIndexBufferDestroy(LgIndexBufferP ib) {
	struct LgIndexBufferGL *ibgl = (struct LgIndexBufferGL *)ib;
	if (ibgl->data) LgFree(ibgl->data);
	LgFree(ibgl);
}

static void *openglIndexBufferLock(LgIndexBufferP ib) {
	struct LgIndexBufferGL *ibgl = (struct LgIndexBufferGL *)ib;
	return ibgl->data;
}

static void openglIndexBufferUnlock(LgIndexBufferP ib) {
}

static int openglIndexBufferCount(LgIndexBufferP ib) {
	struct LgIndexBufferGL *ibgl = (struct LgIndexBufferGL *)ib;
	return ibgl->count;
}

static void openglTextureSize(LgTextureP texture, LgSizeP size) {
	struct LgTextureGL *textureGl = (struct LgTextureGL *)texture;
	LgCopyMemory(size, &textureGl->size, sizeof(struct LgSize));
}

static enum LgImageFormat_ openglTextureFormat(LgTextureP texture) {
	struct LgTextureGL *textureGl = (struct LgTextureGL *)texture;
	return textureGl->format;
}

static LgBool openglTextureUpdate(LgTextureP texture, int destX, int destY, int srcW, int srcH, LgImageP image) {
	struct LgTextureGL *textureGl = (struct LgTextureGL *)texture;

	glBindTexture(GL_TEXTURE_2D, textureGl->id);

	switch(textureGl->format) {
	case LgImageFormatL8:
		glTexSubImage2D(GL_TEXTURE_2D, 0, destX, destY, srcW, srcH, GL_LUMINANCE, GL_UNSIGNED_BYTE, LgImageData(image));
		break;
	case LgImageFormatA8L8:
		glTexSubImage2D(GL_TEXTURE_2D, 0, destX, destY, srcW, srcH, GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, LgImageData(image));
		break;
	case LgImageFormatA8R8G8B8:
		glTexSubImage2D(GL_TEXTURE_2D, 0, destX, destY, srcW, srcH, GL_RGBA, GL_UNSIGNED_BYTE, LgImageData(image));
		break;
	default:
		glTexSubImage2D(GL_TEXTURE_2D, 0, destX, destY, srcW, srcH, GL_RGBA, GL_UNSIGNED_BYTE, LgImageData(image));
		break;
	}

	return LgTrue;
}

static void openglRenderTargetDestroy(LgRenderTargetP target) {
	struct LgRenderTargetGL *targetgl = (struct LgRenderTargetGL *)target;
	if (targetgl->fbo) glDeleteFramebuffers(1, &targetgl->fbo);
	if (targetgl->depth) glDeleteRenderbuffers(1, &targetgl->depth);
	LgFree(target);
}

static void openglRenderTargetSize(LgRenderTargetP target, LgSizeP size) {
	struct LgRenderTargetGL *targetgl = (struct LgRenderTargetGL *)target;
	LgCopyMemory(size, &targetgl->texture->size, sizeof(struct LgSize));
}

static LgTextureP openglRenderTargetTexture(LgRenderTargetP target) {
	struct LgRenderTargetGL *targetgl = (struct LgRenderTargetGL *)target;
	return (LgTextureP)targetgl->texture;
}

struct LgRenderSystemDriver openglesDriver = {
	"OpenGLES",
	openglOpen,
	openglClose,
	openglSetTransform,
	openglGetTransform,
	openglSetCxform,
	openglGetCxform,
	openglCreateVertexBuffer,
	openglCreateIndexBuffer,
	openglCreateTextureR,
	openglDrawPrimitive,
	openglDrawIndexPrimitive,
	openglBegin,
	openglEnd,
	openglClear,
	openglSetTexture,
	openglEnableStencil,
	openglApplyStencil,
	openglCreateRenderTarget,
	openglSetPixelShader,

	openglVertexBufferDestroy,
	openglVertexBufferLock,
	openglVertexBufferUnlock,
	openglVertexBufferSize,

	openglIndexBufferDestroy,
	openglIndexBufferLock,
	openglIndexBufferUnlock,
	openglIndexBufferCount,

	openglTextureSize,
	openglTextureFormat,
	openglTextureUpdate,

	openglRenderTargetDestroy,
	openglRenderTargetSize,
	openglRenderTargetTexture,
};