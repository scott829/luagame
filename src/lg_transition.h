#ifndef _LG_TRANSITION_H_
#define _LG_TRANSITION_H_

#include "lg_scene.h"

void LgTransition(LgNodeP scene, LgActionP inAction, LgActionP outAction);

#endif
