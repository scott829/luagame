#ifndef _LG_REFCNT_H
#define _LG_REFCNT_H

#include "lg_basetypes.h"
#include <assert.h>

#define LG_REFCNT_HEAD int _refcnt; \
	LgDestroy _destroy;

struct LgBaseRefCnt {
	LG_REFCNT_HEAD
};

#define LG_REFCNT_FUNCTIONS_DECL(name) \
	name##P name##Retain(name##P obj); \
	name##P name##Release(name##P obj); \
	int name##RefCount(name##P obj);

#define LG_REFCNT_FUNCTIONS(name) \
	name##P name##Retain(name##P obj) { \
		if (obj) ((struct LgBaseRefCnt *)obj)->_refcnt++; \
		return obj; \
	} \
	name##P name##Release(name##P obj) { \
		if (obj) { \
			assert(((struct LgBaseRefCnt *)obj)->_refcnt > 0); \
			((struct LgBaseRefCnt *)obj)->_refcnt--; \
			if (((struct LgBaseRefCnt *)obj)->_refcnt == 0) ((struct LgBaseRefCnt *)obj)->_destroy(obj); \
		} \
		return obj; } \
	int name##RefCount(name##P obj) { \
		return ((struct LgBaseRefCnt *)obj)->_refcnt; \
	}

#define LG_REFCNT_INIT(obj, destroy) \
	((struct LgBaseRefCnt *)obj)->_refcnt = 1; \
	((struct LgBaseRefCnt *)obj)->_destroy = (LgDestroy)destroy;

void *LgRetain(void *p);

void LgRelease(void *p);

int LgRefCount(void *p);

#endif