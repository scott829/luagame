﻿#include "lg_jni_helper.h"
#include "android_native_app_glue.h"
#include <android/log.h>
#include "../lg_log.h"

#define JVM (g_androidApp->activity->vm)
JNIEnv *JNI_ENV = NULL;
static jobject g_classLoader = NULL;
static jmethodID g_findClass = NULL;
static jclass g_LuaGameHelperClass = NULL;
extern struct android_app* g_androidApp;
static int g_attachCount = 0;

// 当前线程附加到jni环境
LgBool LgJniAttachEnv() {
	if (g_attachCount == 0) {
		jint ret;
		jclass activityClass = NULL, classLoaderClass = NULL;
		jmethodID getClassLoader = NULL;
		JavaVMAttachArgs lJavaVMAttachArgs;
	    lJavaVMAttachArgs.version = JNI_VERSION_1_6;
	    lJavaVMAttachArgs.name = "NativeThread";
	    lJavaVMAttachArgs.group = NULL;

		ret = (*JVM)->AttachCurrentThread(JVM, &JNI_ENV, &lJavaVMAttachArgs);
		if (ret != JNI_OK) {
			LgLogInfo("unable attach jni!", 0);
			return LgFalse;
		}

		activityClass = (*JNI_ENV)->FindClass(JNI_ENV, "android/app/NativeActivity");
		getClassLoader = (*JNI_ENV)->GetMethodID(JNI_ENV, activityClass, "getClassLoader", "()Ljava/lang/ClassLoader;");
		g_classLoader = (*JNI_ENV)->CallObjectMethod(JNI_ENV, g_androidApp->activity->clazz, getClassLoader);

		classLoaderClass = (*JNI_ENV)->FindClass(JNI_ENV, "java/lang/ClassLoader");
		g_findClass = (*JNI_ENV)->GetMethodID(JNI_ENV, classLoaderClass, "loadClass", "(Ljava/lang/String;)Ljava/lang/Class;");

		(*JNI_ENV)->DeleteLocalRef(JNI_ENV, activityClass);
		(*JNI_ENV)->DeleteLocalRef(JNI_ENV, classLoaderClass);

		g_LuaGameHelperClass = LgJniFindClass("com/luagame/LuaGameHelper");
	} else {
		g_attachCount++;
	}

	return LgTrue;
}

// 当前线程释放jni环境
void LgJniReleaseEnv() {
	g_attachCount--;
	if (g_attachCount == 0) {
		(*JNI_ENV)->DeleteLocalRef(JNI_ENV, g_LuaGameHelperClass);
		g_LuaGameHelperClass = NULL;
		(*JNI_ENV)->DeleteLocalRef(JNI_ENV, g_classLoader);
		g_classLoader = NULL;
		(*JVM)->DetachCurrentThread(JVM);
	}
}

jclass LgJniLuaGameHelperClass() {
	return g_LuaGameHelperClass;
}

 jclass LgJniFindClass(const char *classname) {
 	jstring strClassName;
 	jclass ret;
 	strClassName = (*JNI_ENV)->NewStringUTF(JNI_ENV, classname);
	ret = (jclass)(*JNI_ENV)->CallObjectMethod(JNI_ENV, g_classLoader, g_findClass, strClassName);
 	(*JNI_ENV)->DeleteLocalRef(JNI_ENV, strClassName);
 	return ret;
 }
