package com.luagame;

import android.os.Looper;
import android.os.Message;

public class LuaGameHelper {
	public static void showSoftInput() {
		LuaGameHandler handler = new LuaGameHandler(Looper.getMainLooper());
		Message msg = handler.obtainMessage(LuaGameHandler.MSG_SHOWSOFTINPUT);
		handler.sendMessage(msg);
	}

	public static void hideSoftInput() {
		LuaGameHandler handler = new LuaGameHandler(Looper.getMainLooper());
		Message msg = handler.obtainMessage(LuaGameHandler.MSG_HIDESOFTINPUT);
		handler.sendMessage(msg);
	}
	
	public static native void eventKeyboardInputText(String text);
	
	public static native void eventKeyboardCompText(String text);
	
	public static native void eventKeyboardDeleteChar();
	
	public static native void eventKeyboardClose();
	
	static {
		System.loadLibrary("native-activity");
	}
}
