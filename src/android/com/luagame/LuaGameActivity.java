package com.luagame;

import android.app.NativeActivity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;

public class LuaGameActivity extends NativeActivity {
	private static LuaGameActivity current;
	private InputReceiverView inputReceiver;
	private boolean inputShown = false;

	public static LuaGameActivity getCurrent() {
		return current;
	}

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		current = this;

		inputReceiver = new InputReceiverView(this);
		addContentView(inputReceiver, new LayoutParams(0, 0));
	}
	
	public void showSoftInput() {
		// ��ʾ������
		if (!inputShown) {
			Log.i("jni", "showSoftInput");
			inputReceiver.setFocusable(true);
			inputReceiver.setFocusableInTouchMode(true);
			inputReceiver.requestFocus();
			InputMethodManager input = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			input.showSoftInput(inputReceiver, 0);
			inputShown = true;
		}
	}
	
	public void hideSoftInput() {
		// ����������
		if (inputShown) {
			Log.i("jni", "hideSoftInput");
			inputReceiver.setFocusable(false);
			inputReceiver.setFocusableInTouchMode(false);
			InputMethodManager input = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			input.hideSoftInputFromWindow(inputReceiver.getWindowToken(), 0);
			inputShown = false;
		}
	}
}
