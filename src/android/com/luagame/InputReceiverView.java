package com.luagame;

import android.content.Context;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.BaseInputConnection;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;

public class InputReceiverView extends View {

	// 输入法连接对象
	class MyInputConnection extends BaseInputConnection {
		public MyInputConnection(View targetView, boolean fullEditor) {
			super(targetView, fullEditor);
		}
		
		public boolean commitText(CharSequence text, int newCursorPosition) {
			if (text.charAt(0) == '\n') LuaGameHelper.eventKeyboardClose();
			else LuaGameHelper.eventKeyboardInputText(text.toString());
			return true;
		}
		
		public boolean sendKeyEvent(KeyEvent event) {
			if (event.getAction() == KeyEvent.ACTION_DOWN) {
				if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
					LuaGameHelper.eventKeyboardClose();
				} else if (event.getKeyCode() == KeyEvent.KEYCODE_DEL) {
					LuaGameHelper.eventKeyboardDeleteChar();
				}
			}
			return true;
		}
	}

	public InputReceiverView(Context context) {
		super(context);
	}

	public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
		return new MyInputConnection(this, false);
	}

	public boolean onCheckIsTextEditor() {
		return true;
	}

}
