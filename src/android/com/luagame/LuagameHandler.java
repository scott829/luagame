package com.luagame;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

public class LuaGameHandler extends Handler {
	public final static int MSG_SHOWSOFTINPUT = 1;
	
	public final static int MSG_HIDESOFTINPUT = 2;
	
	public LuaGameHandler(Looper looper){
        super(looper);
    }
	
	@Override
	public void handleMessage(Message msg) {
		if (msg.what == MSG_SHOWSOFTINPUT) {
			LuaGameActivity.getCurrent().showSoftInput();
		} else if (msg.what == MSG_HIDESOFTINPUT) {
			LuaGameActivity.getCurrent().hideSoftInput();
		}
	}
}
