﻿#ifndef _LG_JNI_HELPER_H
#define _LG_JNI_HELPER_H

#include <jni.h>
#include "../lg_basetypes.h"

extern JNIEnv *JNI_ENV;

LgBool LgJniInitEnv();

void LgJniReleaseEnv();

jclass LgJniLuaGameHelperClass();

jclass LgJniFindClass(const char *classname);

#endif