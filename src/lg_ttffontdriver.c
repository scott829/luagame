﻿#include "lg_ttffontdriver.h"
#include "lg_memory.h"
#include "libfreetype/include/ft2build.h"
#include FT_FREETYPE_H
#include "libfreetype/include/freetype/ftcache.h"
#include "lg_platform.h"
#include "lg_rendersystem.h"
#include "lg_imagecopy.h"
#include "lg_shapes.h"
#include "lg_meminputstream.h"
#include "lg_init.h"
#include <wchar.h>

struct LgTTFDrawBuffer {
	LgTextureP			texture;
	struct LgMatrix44	matrix;
	float				ascent;
};

struct FT_LibraryRec_ *g_ftLibrary = NULL;
struct FTC_ManagerRec_ *g_ftCache = NULL;

static FT_Error faceRequest(FTC_FaceID faceId, FT_Library library, FT_Pointer requestData, FT_Face *face) {
	char fontfile[LG_PATH_MAX];
	FT_Error err;

	strcpy(fontfile, LgResourceDirectory());
	strcat(fontfile, (const char *)faceId);
	err = FT_New_Face(library, fontfile, 0, face);
	FT_Select_Charmap(*face, FT_ENCODING_UNICODE);
	return err;
}

static FT_Face lookupFontFace(LgFontDataP font, int fontsize) {
	FTC_ScalerRec scaler;
	FT_Size ftsize;

	scaler.face_id = font;
	scaler.width = 0;
	scaler.height = fontsize;
	scaler.pixel = 1;
	scaler.x_res = 300;
	scaler.y_res = 300;

	if (FTC_Manager_LookupSize(g_ftCache, &scaler, &ftsize)) return NULL;
	return ftsize->face;
}

static LgBool init() {
	FT_Init_FreeType(&g_ftLibrary);
	FTC_Manager_New(g_ftLibrary, 0, 0, 0, faceRequest, NULL, &g_ftCache);
	return LgTrue;
}

static void destroy() {
	if (g_ftCache) FTC_Manager_Done(g_ftCache);
	if (g_ftLibrary) FT_Done_FreeType(g_ftLibrary);
}

static LgFontDataP createFont(const char *filename) {
	return strdup(filename);
}

static void destroyFont(LgFontDataP font) {
	free(font);
}

static void destroyStringBuffer(LgDrawBufferP buffer) {
	struct LgTTFDrawBuffer *db = (struct LgTTFDrawBuffer *)buffer;
	if (db->texture) LgTextureRelease(db->texture);
	LgFree(db);
}

static LgDrawBufferP createStringBuffer(LgFontDataP font, int fontsize, const wchar_t *text, int textLen) {
	FT_Face face = lookupFontFace(font, fontsize);
	FT_GlyphSlot slot;
	FT_Vector kerning;
	LgImageP img = NULL;
	struct LgSize size;
	int drawX, prevChar, firstCharOffset, imgWidth, i;
	LgTextureP texture = NULL;
	struct LgMatrix44 matrix;
	struct LgTTFDrawBuffer *db = NULL;
	wchar_t wc;

	if (!face) return NULL;
	
	if (textLen == -1) textLen = wcslen(text);

	slot = face->glyph;
	firstCharOffset = 0;
	imgWidth = 0;

	// 计算所需要的位图大小
	for (prevChar = -1, i = 0; i < textLen; i++) {
		FT_UInt glyphIndex;
		
		wc = text[i];
		glyphIndex = FT_Get_Char_Index(face, wc);

		if (FT_Load_Glyph(face, glyphIndex, FT_LOAD_DEFAULT)) continue;

		imgWidth += LgMax(slot->metrics.horiAdvance, slot->metrics.width) >> 6;

		if (prevChar != -1) {
			FT_Get_Kerning(face, prevChar, wc, FT_KERNING_DEFAULT, &kerning);
			imgWidth += kerning.x >> 6;
		}

		prevChar = wc;
	}

	// 创建位图
	size.width = imgWidth;
	size.height = fontsize - (face->size->metrics.descender >> 6);
	img = LgImageCreateR(&size, LgImageFormatA8L8);
	LG_CHECK_ERROR(img);

	for (drawX = 0, prevChar = -1, i = 0; i < textLen; i++) {
		FT_UInt glyphIndex;
		struct LgRect destRt, srcRt;
		
		wc = text[i];
		glyphIndex = FT_Get_Char_Index(face, wc);

		if (FT_Load_Glyph(face, glyphIndex, FT_LOAD_DEFAULT)) {
			continue;
		}

		FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL);

		if (prevChar == -1) {
			firstCharOffset = slot->bitmap_left;
			drawX -= slot->bitmap_left;
		}

		srcRt.x = 0, srcRt.y = 0, srcRt.width = slot->bitmap.width, srcRt.height = slot->bitmap.rows;
		destRt.x = drawX + slot->bitmap_left, destRt.y = fontsize - slot->bitmap_top,
			destRt.width = size.width, destRt.height = size.height;

		LgImageCopyData(LgImageData(img), LgImageLineSize(img), LgImageFormat(img), &destRt,
			slot->bitmap.buffer, slot->bitmap.pitch, LgImageFormatL8, &srcRt, LgBlit_L8_To_A8L8);

		if (prevChar != -1) {
			FT_Get_Kerning(face, prevChar, wc, FT_KERNING_DEFAULT, &kerning);
			drawX += kerning.x >> 6;
		}

		drawX += slot->advance.x >> 6;
		prevChar = wc;
	}

	texture = LgImageCreateTextureR(img);
	LgMatrix44InitScale(&matrix, (float)size.width, (float)size.height, 1.0f);
	LgMatrix44Translate(&matrix, (float)firstCharOffset, (float)-(face->size->metrics.ascender >> 6), 0);

	db = (struct LgTTFDrawBuffer *)LgMallocZ(sizeof(struct LgTTFDrawBuffer));
	LG_CHECK_ERROR(db);
	LgCopyMemory(&db->matrix, &matrix, sizeof(matrix));
	db->texture = texture;
	db->ascent = (float)(face->size->metrics.ascender >> 6);

	LgImageRelease(img);
	return db;

_error:
	if (img) LgImageRelease(img);
	if (db) destroyStringBuffer(db);
	return NULL;
}

static void metrics(LgFontDataP font, int fontsize, LgFontMetricsP metrics) {
	FT_Face face;

	LgZeroMemory(&metrics, sizeof(struct LgFontMetrics));

	face = lookupFontFace(font, fontsize);
	if (!face) return;

	metrics->ascent = (float)(face->size->metrics.ascender >> 6);
	metrics->descent = (float)(-face->size->metrics.descender >> 6);
}

static float stringWidth(LgFontDataP font, int fontsize, const wchar_t *text, int textLen) {
	FT_Face face = lookupFontFace(font, fontsize);
	FT_GlyphSlot slot;
	float width = 0;
	FT_Vector kerning;
	int prevChar, i;

	if (!face || !text) return 0;
	if (textLen == -1) textLen = wcslen(text);

	slot = face->glyph;

	for (prevChar = -1, i = 0; i < textLen; i++) {
		wchar_t wc = text[i];
		FT_UInt glyphIndex = FT_Get_Char_Index(face, wc);

		if (FT_Load_Glyph(face, glyphIndex, FT_LOAD_DEFAULT)) {
			continue;
		}

		if (prevChar != -1) {
			FT_Get_Kerning(face, prevChar, wc, FT_KERNING_DEFAULT, &kerning);
			width += kerning.x >> 6;
		}

		width += LgMax(slot->metrics.horiAdvance, slot->metrics.width) >> 6;
		prevChar = wc;
	}

	return width;
}

static void charsWidth(LgFontDataP font, int fontsize, const wchar_t *text, int textLen, int *width) {
	FT_Face face = lookupFontFace(font, fontsize);
	FT_GlyphSlot slot;
	int i;
	
	if (!face || !text) return;
	if (textLen == -1) textLen = wcslen(text);
	
	slot = face->glyph;
	
	for (i = 0; i < textLen; i++) {
		wchar_t wc = text[i];
		FT_UInt glyphIndex = FT_Get_Char_Index(face, wc);
		
		if (FT_Load_Glyph(face, glyphIndex, FT_LOAD_DEFAULT)) {
			width[i] = 0;
		} else {
			width[i] = LgMax(slot->metrics.horiAdvance, slot->metrics.width) >> 6;
		}
	}
}

static float kerning(LgFontDataP font, int fontsize, int leftChar, int rightChar) {
	FT_Face face = lookupFontFace(font, fontsize);
	FT_Vector kerning;
	if (!face) return 0;
	FT_Get_Kerning(face, leftChar, rightChar, FT_KERNING_DEFAULT, &kerning);
	return (float)(kerning.x >> 6);
}

static float height(LgFontDataP font, int fontsize) {
	FT_Face face = lookupFontFace(font, fontsize);
	if (!face) return 0;
	return (float)(fontsize - (face->size->metrics.descender >> 6));
}

static void draw(LgDrawBufferP buffer, float x, float y, LgColor color, enum LgDrawStringFlag flags) {
	struct LgTTFDrawBuffer *db = (struct LgTTFDrawBuffer *)buffer;
	struct LgMatrix44 oldMatrix, newMatrix;
	struct LgCxform oldCxform, textCxform, newCxform;

	LgRenderSystemGetTransform(LgTransformWorld, &oldMatrix);
	LgRenderSystemGetCxform(&oldCxform);

	LgZeroMemory(&textCxform, sizeof(textCxform));
	textCxform.mul[0] = LgColorRed(color) / 255.0f;
	textCxform.mul[1] = LgColorGreen(color) / 255.0f;
	textCxform.mul[2] = LgColorBlue(color) / 255.0f;
	textCxform.mul[3] = LgColorAlpha(color) / 255.0f;

	LgCopyMemory(&newCxform, &oldCxform, sizeof(oldCxform));
	LgCxformMul(&newCxform, &textCxform);
	LgRenderSystemSetCxform(&newCxform);
	
	LgCopyMemory(&newMatrix, &db->matrix, sizeof(struct LgMatrix44));
	LgMatrix44Translate(&newMatrix, x, y + ((flags & LgDrawStringBaseline) == 0 ? db->ascent : 0), 0);
	LgMatrix44Mul(&newMatrix, &oldMatrix);
	LgRenderSystemSetTransform(LgTransformWorld, &newMatrix);

	LgRenderSystemSetTexture(db->texture);
	LgRenderSystemSetPixelShader(LgPixelShaderCxformTexture);
	LgRenderSystemDrawPrimitive(LgPrimitiveTriangleStrip, LgFvfXYZ | LgFvfTexture, LgShapesRectangleTexcoord(), 2);
	LgRenderSystemSetTexture(NULL);

	LgRenderSystemSetTransform(LgTransformWorld, &oldMatrix);
	LgRenderSystemSetCxform(&oldCxform);
}

static int numberOfbatch(LgDrawBufferP buffer) {
	return 1;
}

struct LgFontDriver ttfFontDriver = {
	"truetype",
	init,
	destroy,
	createFont,
	destroyFont,
	createStringBuffer,
	destroyStringBuffer,
	metrics,
	stringWidth,
	charsWidth,
	kerning,
	height,
	draw,
	numberOfbatch
};
